#!/usr/bin/env bash




#this file should be run from a new shell


TPL_PATH=$(pwd)




#--------------solwcad build---------------------------------------
cd $TPL_PATH/solwcad
rm solwcad_p
rm solwcad_pT
./build



#------------gales mesh preprocessing building ---------------------
cd $TPL_PATH/gales_mesh_preprocessing
rm gales_mesh 
rm gales_fsi
rm gales_fhi
g++ -std=c++17 src/gales_mesh.cpp -o gales_mesh 
g++ -std=c++17 src/gales_fsi.cpp -o gales_fsi 
g++ -std=c++17 src/gales_fhi.cpp -o gales_fhi 




