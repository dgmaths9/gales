#ifndef READ_INPUT_HPP
#define READ_INPUT_HPP


#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <map>

#include <boost/algorithm/string.hpp>
#include "boost/lexical_cast.hpp"
#include <boost/algorithm/string/trim.hpp>



  using namespace std;

  class read_input
  {      
   
   public:
    
   read_input(std::string s)
   {
      read_data();
      set_vars();
      
      if(s=="p") print_vars_p();
      if(s=="pT") print_vars_pT();
   }    
      

    
    void read_data()
    {  
      ifstream file("input_solwcad.txt");
      if(!file.is_open())
        std::cerr<<"\n"<<__FILE__ << ":" << __LINE__ << ": error: Could not open 'input_solwcad.txt' \n\n";     
      
      if(file.is_open())
      {
        string s;
        vector<string> split_result;
        while(getline(file, s))
        {        
          boost::trim(s);
          if(!s.empty())
          {
             boost::split(split_result, s, boost::is_any_of(", "), boost::token_compress_on);

             if(split_result[0] == "oxides_wf_1") wfs(split_result);
             else if(split_result[0] == "oxides_wf_2") wfs(split_result);
             else if(split_result[0] == "oxides_wf_3") wfs(split_result);
             else if(split_result[0] == "oxides_wf_4") wfs(split_result);
             else if(split_result[0] == "oxides_wf_5") wfs(split_result);
             else
             {
               data_[split_result[0]] = split_result[1];               
             }    
          }
       }
       file.close();       
     }
     else
     { 
        cerr<<"\n"<<__FILE__ << ":" << __LINE__ << ": error: Could not open 'input_solwcad.txt' \n\n";     
        cerr << "Check if filename is correct i.e. 'input_solwcad.txt' "<<endl;
        exit(1);
     }   
   }      



  
   void wfs(const vector<string>& split_result)
   {
       vector<double> wf(10,0.0);
       for(int i=0; i<10; i++) 
          wf[i] = boost::lexical_cast<double>(split_result[1+i]);
       oxides_wf_.push_back(wf);     
   }




   void set_vars()
   {   
     for(auto s : data_)
     {
       if (s.first == "magma_1")            magma_name_.push_back(s.second);
       if (s.first == "h2o_1")              h2o_.push_back(boost::lexical_cast<double>(s.second));
       if (s.first == "co2_1")              co2_.push_back(boost::lexical_cast<double>(s.second));
                 
       if (s.first == "magma_2")            magma_name_.push_back(s.second);
       if (s.first == "h2o_2")              h2o_.push_back(boost::lexical_cast<double>(s.second));
       if (s.first == "co2_2")              co2_.push_back(boost::lexical_cast<double>(s.second));

       if (s.first == "magma_3")            magma_name_.push_back(s.second);
       if (s.first == "h2o_3")              h2o_.push_back(boost::lexical_cast<double>(s.second));
       if (s.first == "co2_3")              co2_.push_back(boost::lexical_cast<double>(s.second));

       if (s.first == "magma_4")            magma_name_.push_back(s.second);
       if (s.first == "h2o_4")              h2o_.push_back(boost::lexical_cast<double>(s.second));
       if (s.first == "co2_4")              co2_.push_back(boost::lexical_cast<double>(s.second));

       if (s.first == "magma_5")            magma_name_.push_back(s.second);
       if (s.first == "h2o_5")              h2o_.push_back(boost::lexical_cast<double>(s.second));
       if (s.first == "co2_5")              co2_.push_back(boost::lexical_cast<double>(s.second));

       if (s.first == "initial_T")         initial_T_ = boost::lexical_cast<double>(s.second);
       if (s.first == "final_T")           final_T_ = boost::lexical_cast<double>(s.second);
       if (s.first == "delta_T")           delta_T_ = boost::lexical_cast<double>(s.second);
       if (s.first == "fixed_T")           fixed_T_ = boost::lexical_cast<double>(s.second);

       if (s.first == "initial_p")         initial_p_ = boost::lexical_cast<double>(s.second);
       if (s.first == "final_p")           final_p_ = boost::lexical_cast<double>(s.second);
       if (s.first == "delta_p")           delta_p_ = boost::lexical_cast<double>(s.second);
       
     }
   }  





   void print_vars_p()
   {
      cout<<endl;
      cout<<"fixed_T_ = "<< fixed_T_<<endl<<endl;
      cout<<"initial_p_ = "<< initial_p_<<endl;
      cout<<"final_p_ = "<< final_p_<<endl;
      cout<<"delta_p_ = "<< delta_p_<<endl<<endl;

      for(std::size_t i=0; i<magma_name_.size(); i++)
      {
          cout<<magma_name_[i]<<"  ";
          for(int j=0; j<10; j++)
            cout<<oxides_wf_[i][j]<<" "; 
          cout<<endl;         
          cout<<"h2o_ = "<< h2o_[i]<<endl;
          cout<<"co2_ = "<< co2_[i]<<endl;
          cout<<endl;         
      }
   } 
 
 
 


   void print_vars_pT()
   {
      cout<<endl;
      cout<<"initial_T_ = "<< initial_T_<<endl;
      cout<<"final_T_ = "<< final_T_<<endl;
      cout<<"delta_T_ = "<<delta_T_ <<endl<<endl;
      cout<<"initial_p_ = "<< initial_p_<<endl;
      cout<<"final_p_ = "<< final_p_<<endl;
      cout<<"delta_p_ = "<< delta_p_<<endl<<endl;

      for(std::size_t i=0; i<magma_name_.size(); i++)
      {
          cout<<magma_name_[i]<<"  ";
          for(int j=0; j<10; j++)
            cout<<oxides_wf_[i][j]<<" "; 
          cout<<endl;         
          cout<<"h2o_ = "<< h2o_[i]<<endl;
          cout<<"co2_ = "<< co2_[i]<<endl;
          cout<<endl;         
      }
   } 
 
 
 
    map<string, string> data_;
    
    vector<string> magma_name_;
    vector<vector<double>> oxides_wf_;
    vector<double> h2o_, co2_;
    
    double initial_T_;
    double final_T_;
    double delta_T_;
    double fixed_T_;

    double initial_p_;
    double final_p_;
    double delta_p_;
  };









#endif

