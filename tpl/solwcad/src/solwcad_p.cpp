#include <numeric>
#include <algorithm>

#include <iostream>
#include <fstream>
#include <vector>

#include "Solwcad.hpp"
#include "read_input.hpp"


using namespace std;

int main () 
{
  read_input input("p");
  Solwcad solwcad;
  

  int n_row_p=int((input.final_p_ - input.initial_p_)/input.delta_p_)+1;
  vector<double> pressure(n_row_p);
  ofstream pressure_file("pressure.txt");
  pressure_file << n_row_p <<endl;
  for(int i_p=0; i_p<n_row_p;i_p++)
  {
    pressure[i_p] = input.initial_p_+(input.delta_p_*i_p);
    pressure_file << pressure[i_p] <<endl;      
  } 
  pressure_file.close();
  
  
  
  for(std::size_t l=0; l<input.magma_name_.size(); l++)
  {
    ofstream wf_l_h2o_onliq_file("wf_l_h2o_onliq_" + input.magma_name_[l] + ".txt");
    ofstream wf_l_co2_onliq_file("wf_l_co2_onliq_" + input.magma_name_[l] + ".txt");          
    for(int i_p=0; i_p<n_row_p;i_p++) 
    {
      solwcad.exsolution(pressure[i_p], input.fixed_T_, input.h2o_[l], input.co2_[l], input.oxides_wf_[l]);            
      double dissolved_h2o_onliq = solwcad.wf_h2o_l_onliq();
      double dissolved_co2_onliq = solwcad.wf_co2_l_onliq();
      wf_l_h2o_onliq_file<<dissolved_h2o_onliq<<endl;
      wf_l_co2_onliq_file<<dissolved_co2_onliq<<endl;
    }	  	  
    wf_l_h2o_onliq_file.close();
    wf_l_co2_onliq_file.close();
  }
  
         
  cerr<< "Done!!!!!"<< endl;
}
