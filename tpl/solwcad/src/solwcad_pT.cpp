#include <iostream>
#include <numeric>
#include <algorithm>
#include <fstream>
#include <vector>

#include "Solwcad.hpp"
#include "read_input.hpp"



using namespace std;

int main () 
{
  read_input input("pT");
  Solwcad solwcad;



  int n_col_p=int((input.final_p_ - input.initial_p_)/input.delta_p_)+1;
  vector<double> pressure(n_col_p);
  ofstream pressure_file("pressure.txt");
  pressure_file << n_col_p <<endl;
  for(int i_p=0; i_p<n_col_p;i_p++)
  {
    pressure[i_p] = input.initial_p_+(input.delta_p_*i_p);
    pressure_file << pressure[i_p] <<endl;      
  } 
  pressure_file.close();




  int n_row_T = int((input.final_T_ - input.initial_T_)/input.delta_T_)+1;
  vector<double> temperature(n_row_T);
  ofstream temperature_file("temperature.txt");
  temperature_file << n_row_T<<endl;  
  for(int i_T=0; i_T<n_row_T;i_T++) 
  {
    temperature[i_T] = input.initial_T_ + (input.delta_T_*i_T);
    temperature_file << temperature[i_T] <<endl;
  }  
  temperature_file.close();

  
  
  
    
  // lookup matrix will be stored as a matrix
  // with pressure representing columns, and temperature representing rows    

  for(std::size_t l=0; l<input.magma_name_.size(); l++)
  {
     ofstream wf_l_h2o_onliq_file("wf_l_h2o_onliq_" + input.magma_name_[l] + ".txt");
     ofstream wf_l_co2_onliq_file("wf_l_co2_onliq_" + input.magma_name_[l] + ".txt");
     for(int j=0; j<n_row_T; j++) 
     { 
        for(int i=0; i<n_col_p; i++)
        {
   	   solwcad.exsolution(pressure[i], temperature[j], input.h2o_[l], input.co2_[l], input.oxides_wf_[l]);     
   	   double dissolved_h2o_onliq = solwcad.wf_h2o_l_onliq();
	   double dissolved_co2_onliq = solwcad.wf_co2_l_onliq();
	   wf_l_h2o_onliq_file<<dissolved_h2o_onliq<<" ";
	   wf_l_co2_onliq_file<<dissolved_co2_onliq<<" ";
        }    
        wf_l_h2o_onliq_file<<std::endl;
        wf_l_co2_onliq_file<<std::endl;
     }  
     wf_l_h2o_onliq_file.close();
     wf_l_co2_onliq_file.close();
  }



  std::cerr<< "Done!!!!!"<< std::endl;
}
