#ifndef __SOLWCAD_HPP
#define __SOLWCAD_HPP

#include <vector>
#include "NumericalRecipes.hpp"


using namespace std;
typedef typename std::vector<double> vec;


class Solwcad
{

 private:

  NumericalRecipes nr_;

  boost::numeric::ublas::matrix<double> wo_, wij_;

  double p_, T_;  
  double wtfrex_; //! wtfrex = weight fraction of gas phase
  double whc_, p0_co2_, R_;
  double bm_, cm_, dm_, em_, vr_, xwo_;

  vec fol_, to_, par_;
  vec molecular_weight_, ghiorso_par_, weight_fraction_;
  vec h2o_co2_, gas_fraction_;
  vec ctrm_, xtrm_, rtrm_, scale_factor_, y_;

  //!weight_fraction_(1),(2) = weight fraction of H2O,CO2 on liquid phase
  //!weight_fraction_(3),(4) = weight fraction of H2O,CO2 on gas phase


public:


  Solwcad() :
  whc_(0.0), p0_co2_(1.01325e5), R_(8.31451),
  bm_(0.), cm_(0.), dm_(0.), em_(0.), vr_(0.), xwo_(0.),
  wo_(12,12,0.0),
  wij_(20,2,0.0),
  molecular_weight_(12,0.0), ghiorso_par_(12,0.0), weight_fraction_(4,0.0),
  fol_(2,0.0), to_(2,0.0), par_(10,0.0),
  gas_fraction_(2,0.0), h2o_co2_(2,0.0),
  ctrm_(2,0.0), xtrm_(2,0.0), rtrm_(2,0.0), scale_factor_(2,1.0), y_(2,0.0)
  {
    fol_ = {0.1979347855e+2, 0.2348447513e+2};
    to_ = {0.1e4, 0.14e4};
    par_ = {0.2418946564e-05, 0.1344868775e-07, 0.0, 0.0, 0.2106552365e-14, 0.0, 0.0, 0.0, 0.0, 0.0};
    molecular_weight_ =  {18.02, 44.01, 60.085, 79.899, 101.96, 159.69, 71.846, 70.937, 40.304, 56.079, 61.979, 94.195};
    ghiorso_par_ = {1., 1., 0.25, 0.25, 0.375, 0.375, 0.25, 0.25, 0.25, 0.25, 0.375, 0.375};

    wo_(2,3) = -122861.0680;
    wo_(2,4) = -328708.4288;
    wo_(2,5) = 11037.09912;
    wo_(2,6) = -40292.50576;
    wo_(2,7) = 23118.10624;
    wo_(2,8) = -126999.4624;
    wo_(2,9) = -268060.9304;
    wo_(2,10) = -308604.7272;
    wo_(2,11) = -366503.3376;

    wo_(3,4) = -281791.1448;
    wo_(3,5) = -28542.49488;
    wo_(3,6) = -19223.76456;
    wo_(3,7) = -8548.7488;
    wo_(3,8) = 53026.3424;
    wo_(3,9) = -428617.328;
    wo_(3,10) = -422893.616;
    wo_(3,11) = -170291.7288;

    wo_(4,5) = 5189.49888;
    wo_(4,6) = -249067.6624;
    wo_(4,7) = -8023.866;
    wo_(4,8) = -203655.3632;
    wo_(4,9) = -411824.0072;
    wo_(4,10) = -567413.16;
    wo_(4,11) = -733563.984;

    wo_(5,6) = 18930.34064;
    wo_(5,7) = 887.828064;
    wo_(5,8) = -5343.09352;
    wo_(5,9) = 6358.88504;
    wo_(5,10) = -15553.51792;
    wo_(5,11) = 1187.109584;

    wo_(6,7) = -2942.77456;
    wo_(6,8) = -242361.5472;
    wo_(6,9) = -248343.412;
    wo_(6,10) = -154666.5808;
    wo_(6,11) = -353880.628;

    wo_(7,8) = -11757.4584;
    wo_(7,9) = 2925.130632;
    wo_(7,10) = 3264.1476;
    wo_(7,11) = -254.0696344;

    wo_(8,9) = -330220.108;
    wo_(8,10) = -387486.0976;
    wo_(8,11) = -188961.5736;

    wo_(9,10) = -262671.1016;
    wo_(9,11) = -116767.072;

    wo_(10,11) = -75854.6648;

    for(int i=0; i<12; i++)
      for(int j = i+1; j <12; j++)
        wo_(j,i) = wo_(i,j);

    wij_(0,0) = -0.3409307328e5;
    wij_(1,0) =  0.0000000000e0;
    wij_(2,0) = -0.1891165069e6;
    wij_(3,0) =  0.1359353090e6;
    wij_(4,0) = -0.1957509655e6;
    wij_(5,0) =  0.0000000000e0;
    wij_(6,0) = -0.8641805096e5;
    wij_(7,0) = -0.2099966494e6;
    wij_(8,0) = -0.3222531515e6;
    wij_(9,0) = -0.3497975003e6;

    wij_(0,1) = -0.5996216205e5;
    wij_(1,1) =  0.0000000000e0;
    wij_(2,1) = -0.5909571939e6;
    wij_(3,1) =  0.4469622832e7;
    wij_(4,1) =  0.2166575511e5;
    wij_(5,1) =  0.0000000000e0;
    wij_(6,1) =  0.5286613028e5;
    wij_(7,1) = -0.3287921030e6;
    wij_(8,1) =  0.1400344548e6;
    wij_(9,1) = 0.3090699699e6;
    wij_(10,1) = 0.6049208301e4;
    wij_(11,1) = 0.0000000000e0;
    wij_(12,1) = 0.4139537240e5;
    wij_(13,1) = -0.5293012049e6;
    wij_(14,1) =  0.1213717202e4;
    wij_(15,1) =  0.0000000000e0;
    wij_(16,1) = -0.1344620202e5;
    wij_(17,1) =  0.1278883196e5;
    wij_(18,1) = -0.3521319385e5;
    wij_(19,1) = -0.5800953852e5;
  }



  inline const double wf_h2o_l_onliq()const {return weight_fraction_[0];}
  inline const double wf_co2_l_onliq()const {return weight_fraction_[1];}
  inline const double p()const {return p_;}
  inline const double R()const {return R_;}
  inline const double T()const {return T_;}
  



  //! returns the magma composition adjusted for ghiorso method from the oxides weight fraction.
  void reexpressComposition(const vec& oxide,  vec& new_oxide,  vec& x_fraction, vec& fmol)
  {
    new_oxide = {oxide[0], oxide[1]};
    double new_oxide_sum = oxide[0] + oxide[1];
    double oxide_sum = accumulate(&oxide[2], &oxide[12], 0.0);
    double use = (1.0-new_oxide_sum)/oxide_sum;

    for(int i=0; i<12; i++)
    {
      if(i>= 2)
      new_oxide[i] = oxide[i]*use;

      fmol[i] = new_oxide[i]/molecular_weight_[i];
    }

    fmol[2] = fmol[2] -0.5*(fmol[6]+fmol[7]+fmol[8]+fmol[9])-fmol[10]-fmol[11];

    for(int i=0; i<12; i++)
    x_fraction[i] = fmol[i]*ghiorso_par_[i];

    double fraction_sum = accumulate(&x_fraction[0], &x_fraction[12], 0.0);

    for(int i=0; i<12; i++)
    {
      x_fraction[i] = x_fraction[i]/fraction_sum;
      if(i>=2)
      {
        x_fraction[i] = x_fraction[i]/(1-x_fraction[0]-x_fraction[1]);
      }
    }
  }



  //! function for scaling the unknowns gas_est[i].
  void scalingUnknowns (vec& gas_est, vec& gas_scaling)
  {
    for(int i=0; i<2; i++)
    {
      const double tolscl = 0.9;
      int inot;
      if(i==0) inot=1;
      if(i==1) inot=0;
      if(gas_est[i]/gas_est[inot] < tolscl)
      {
        scale_factor_[i] = gas_est[i]/gas_est[inot];
        gas_scaling[i] = gas_scaling[i]/scale_factor_[i] ;
        gas_est[i] = gas_est[inot];
      }
    }
  }






  //! calculate the saturation content of H2O and CO2 in silicate liquids, and the composition of the co-existing gas phase. For any given silicate liquid composition in terms of 10 major oxides, pressure, temperature, and total H2O and CO2 in the sistem.
  void exsolution(const double &p, const double &T, const double &h2o, const double &co2, const vector<double> &magma);

  //! calculate the saturation content of CO2 in silicate liquids, and the composition of the co-existing gas phase. For any given silicate liquid composition in terms of 10 major oxides, pressure, temperature, and total CO2 in the sistem.
  void exsolutionCO2(const double &p, const double &T, const vec& composition);

  //! calculate the saturation content of H2O in silicate liquids, and the composition of the co-existing gas phase. For any given silicate liquid composition in terms of 10 major oxides, pressure, temperature, and total H2O in the sistem.
  void exsolutionH2O(const double &p, const double &T, const vec& composition);



  void vecfunc(vec& x, vec& fv);

  //! return the liquid components reference terms (ref_term_) and gas components fugacities (fugacity_) for CO2 only.
  void referenceTermsCO2 (vec& ref_term, vec& phi);

  //! return the liquid components reference terms (ref_term_) and gas components fugacities (fugacity_) for H2O only.
  void referenceTermsH2O (vec& ref_term, vec& phi);

};




class SolwcadFunction1 : public virtual Function
{
  public:
  SolwcadFunction1(Solwcad& solwcad, double &p1, double &p2, double &p3, double &p4, double &p5): p1_(p1), p2_(p2), p3_(p3), p4_(p4), p5_(p5), 
  p_(solwcad.p()), R_(solwcad.R()), T_(solwcad.T()){}

  double function(double &x)
  {  
    double a = p2_+ p3_/x + p4_/(x*x);
    p5_ = 0.25*p1_/x;
    double sol = p_ - R_*T_*(1. + p5_ + pow(p5_,2) - pow(p5_,3))/(x*pow(1.-p5_,3)) + a/(sqrt(T_)*x*(x+p1_));
    return sol;
  }

  double p5_;

  private:
  double p1_, p2_, p3_, p4_;
  double p_, R_, T_;
};




class SolwcadFunction2 : public virtual Function
{
  public:
  SolwcadFunction2(Solwcad& solwcad, double &p1, double &p2, double &p3): p1_(p1), p2_(p2), p3_(p3), p_(solwcad.p()) {}

  double function(double &x)
  {
    if(x <= 1e-15) x = 1e-6; 
    double sol = log(p1_*p_/x) - pow(1-x,2)*p2_ - p3_;
    return sol;
  }

  private:
  double p1_, p2_, p3_;
  double p_;
};




class SolwcadNewt : public virtual Function
{
  public:
  SolwcadNewt(Solwcad& solwcad)
  {
    solwcad_ = &solwcad;
  }

  void function2(vec& x, vec& y)
  {
    solwcad_->vecfunc(x,y);
  }

  private:
  Solwcad *solwcad_;
};








void Solwcad::exsolution(const double &p, const double &T, const double &h2o, const double &co2, const vector<double> &magma)
{
  vec composition(12,0.0);
  composition = {h2o, co2};
  for(int i=2; i<12; i++) composition[i] = magma[i-2];

  p_ = p;
  T_ = T;

  const double tresho = 5.e-5;

  vec oxide(12,0.0), x_fraction(12,0.0), fmol(12,0.0);
  vec gas_scaling(2,0.0), gas_oxide(2,0.0);
  
  reexpressComposition(composition, oxide, x_fraction, fmol);

  if(x_fraction[0] < tresho)
  {
    oxide[0] = 0.;
    exsolutionCO2(p,T,oxide);
    return;
  }
  if(x_fraction[1] < tresho)
  {
    oxide[1] = 0.;
    exsolutionH2O(p,T,oxide);
    return;
  }

  for(int i=0; i<2; i++)
  {
    gas_fraction_[i] = x_fraction[i];
    gas_scaling[i] = gas_fraction_[i];
    gas_oxide[i] = oxide[i];
  }
  double gas_tot = oxide[0]+oxide[1];
  double p_log = log(p_/p0_co2_);

  //set variables to zero
  for(int i=0; i<2; i++)  ctrm_[i] = 0;
  xwo_ = 0;
  
  for(int i=2; i<11; i++)
  {
    ctrm_[0] += x_fraction[i]*wij_(i-2,0);
    ctrm_[1] += x_fraction[i]*(wij_(i-2,1)+p_log*wij_(i+8,1));
    for(int j=i+1; j<12; j++) xwo_ += x_fraction[i]*x_fraction[j]*wo_(i,j);
  }
  ctrm_[0] += x_fraction[11]*wij_(9,0);
  ctrm_[1] += x_fraction[11]*(wij_(9,1)+p_log*wij_(19,1));

  for(int i=0; i<2; i++) xtrm_[i] = (ctrm_[i]-xwo_)/(R_*T_);

  vec ref_term(2,0.0), phi(2,0.0), v_ref(2,0.0);
  referenceTermsH2O(ref_term, phi);
  rtrm_[0] = fol_[0]+ref_term[0]/(R_*T_);
  v_ref[0] = vr_;

  referenceTermsCO2(ref_term, phi);
  rtrm_[1] = fol_[1]+ref_term[1]/(R_*T_);
  v_ref[1] = vr_;

  //set initial estimate for one gas
  vec oxide_1g(12,0.0), new_oxide_1g(12,0.0), x_fraction_1g(12,0.0);

  oxide_1g = {4.1e-6*pow(p_,0.5), 0.0};
  for(int i=2; i<12; i++) oxide_1g[i] = oxide[i];

  reexpressComposition(oxide_1g, new_oxide_1g, x_fraction_1g, fmol);
  vec gas_est = {x_fraction_1g[0], x_fraction_1g[0]*0.1};

  //start iteration
  int errore = 0;
  double ah, bh, yh, xg;
  double eps = 1.e-12;
  ah = 0.8*gas_est[0];
  bh = 1.2*gas_est[0];

  SolwcadFunction2 solwcad_function2(*this, phi[0], xtrm_[0], rtrm_[0]);
  do
  {
    if(bh > 1.) bh = 1.;
    errore = nr_.secantMethod( solwcad_function2, ah, bh, eps, xg);
    if(errore == 0)
    {
      ah = 0.5*ah;
      bh = 1.5*bh;
    }
    if(ah < 1.e-15 && bh > 1.)
    {
      cout << "error from Solwcad::exsolution" << endl;
      cout << "at P = " << p_ << " and T = "<< T_<< endl;
      cout << "the solution cannot be bracketed" << endl;
      exit(1);
    }
  }while(errore == 0);
  

  vec molar_fraction(4,0.0);
  for(int i=0; i<2; i++) x_fraction[i] = 0.;

  // set initial estimate for two gas
  gas_est = {xg, xg*0.1};

  for(int i=0; i<2; i++)
    if(gas_est[i]>= gas_fraction_[i])
      gas_est[i] = gas_fraction_[i]-1e-6;

  //calculate for two gas scaling unknowns
  int kspur = 0;
  int esci = 0;
  const double tol=1.e-8;
  bool check;
  vec xgg(2,0.0), xgg1(2,0.0);

  scalingUnknowns (gas_est, gas_scaling);
  for(int i=0; i<2; i++)
  {
    xgg[i] = gas_est[i];
    xgg1[i] = xgg[i];
  }
  SolwcadNewt solwcad_newt(*this);
  vec fvec(2,0.0);
  nr_.newt(xgg, check, solwcad_newt);
  vecfunc(xgg, fvec);
  if(check) cout << "convergency problems in newt" << endl;

  do
  {
    for(int i = 0; i< 2; i++)
    {
      if(fabs(fvec[i]) > tol)
      {
        kspur += 1;
        for(int j=0; j<2; j++)  xgg[j] = xgg1[j]*0.8/kspur;
        if(kspur < 3)
        {
          scalingUnknowns (gas_est, gas_scaling);
          nr_.newt(xgg, check, solwcad_newt);
          vecfunc(xgg, fvec);
          if(check) cout << "convergency problems in newt" << endl;
        }
        if(kspur >= 3)
        {
          for(int ispur=0; ispur<2; ispur++)  xgg[ispur] = gas_fraction_[ispur];
        }
      }
      if(fabs(fvec[i]) > tol && kspur < 3)
      {
        esci = 0;
        break;
      }
      xgg[i] = xgg[i]*scale_factor_[i];
      x_fraction[i] = xgg[i];
      molar_fraction[i] = xgg[i];
      esci = 1;
    }
  }while( esci == 0);

  molar_fraction[2] = y_[0];
  molar_fraction[3] = 1 - molar_fraction[2];

  double xyws = molar_fraction[2]*molecular_weight_[0]+ molar_fraction[3]*molecular_weight_[1];
  for(int i=0; i<12; i++)
  {
    if(i >= 2) x_fraction[i] *= 1.-(x_fraction[0]+x_fraction[1]);;
    fmol[i] = x_fraction[i]/ghiorso_par_[i];
  }
  fmol[2] = fmol[2]+0.5*(fmol[6]+fmol[7]+fmol[8]+fmol[9]) +fmol[10]+fmol[11];

  double ox_sum = 0.;
  for(int i=0; i < 12; i++)
  {
    double oxn = fmol[i]*molecular_weight_[i];
    ox_sum += oxn;
    if(i <= 1) weight_fraction_[i] = oxn;
  }
  
  weight_fraction_[0] = weight_fraction_[0]/ox_sum;
  weight_fraction_[1] = weight_fraction_[1]/ox_sum;
  weight_fraction_[2] = molar_fraction[2]*molecular_weight_[0]/xyws;
  weight_fraction_[3] = 1.-weight_fraction_[2];

  //check mass conservation
  wtfrex_ = (gas_tot-weight_fraction_[0]-weight_fraction_[1])/ (1.-weight_fraction_[0]-weight_fraction_[1]);

  //undersatured conditions
  if(wtfrex_ <= 1.e-16)
  {
    wtfrex_ = 0.;
    weight_fraction_[0] = gas_oxide[0];
    weight_fraction_[1] = gas_oxide[1];
    weight_fraction_[2] = 0.;
    weight_fraction_[3] = 0.;
  }
}





void Solwcad::exsolutionCO2(const double &p, const double &T, const vec& composition)
{
  p_ = p;
  T_ = T;

  vec oxide(12,0.0), x_fraction(12,0.0), fmol(12,0.0);

  reexpressComposition(composition, oxide, x_fraction, fmol);

  for(int i=0; i<2; i++) gas_fraction_[i] = x_fraction[i];

  double gas_tot = oxide[2];
  double p_log = log(p_/p0_co2_);

  //set to zero variables
  for(int i = 0; i<2; i++)  ctrm_[i] = 0;
  xwo_ = 0;

  for(int i = 2; i<11; i++)
  {
    ctrm_[0] += x_fraction[i]*wij_(i-2,0);
    ctrm_[1] += x_fraction[i]*(wij_(i-2,1)+p_log*wij_(i+8,1));
    for(int j=i+1; j<12; j++)  xwo_ += x_fraction[i]*x_fraction[j]*wo_(i,j);
  }
  ctrm_[0] += x_fraction[11]*wij_(9,0);
  ctrm_[1] += x_fraction[11]*(wij_(9,1)+p_log*wij_(19,1));

  for(int i=0; i<2; i++) xtrm_[i] = (ctrm_[i]-xwo_)/(R_*T_);

  vec ref_term(2,0.0), phi(2,0.0), v_ref(2,0.0);

  referenceTermsCO2(ref_term, phi);
  rtrm_[1] = fol_[1] + ref_term[1]/(R_*T_);
  v_ref[1] = vr_;

  vec oxide_1g(12,0.0), new_oxide_1g(12,0.0), x_fraction_1g(12,0.0);

  oxide_1g = {4.1e-6*pow(p_,0.5), 0.0};
  for(int i=2; i<12; i++) oxide_1g[i] = oxide[i];

  reexpressComposition(oxide_1g, new_oxide_1g, x_fraction_1g, fmol);
  vec gas_est = {x_fraction_1g[0], x_fraction_1g[0]*0.1};

  //start iteration
  int errore = 0;
  double ah, bh, yh, xg;
  double eps = 1.e-12;
  ah = 0.8*gas_est[1];
  bh = 1.2*gas_est[1];

  SolwcadFunction2 solwcad_function2(*this, phi[1], xtrm_[1], rtrm_[1]);
  do
  {
    if(bh >= 1.) bh = 1.;
    errore = nr_.secantMethod( solwcad_function2, ah, bh, eps, xg);
    if(errore == 0)
    {
      ah = 0.5*ah;
      bh = 1.5*bh;
    }
    if(ah < 1.e-15 && bh > 1.)
    {
      cout << "error from Solwcad::exsolution" << endl;
      cout << "at P = " << p_ << " and T = "<< T_<< endl;
      cout << "in solwcad.hpp line 794; ah = "<<ah<< "   bh = "<<bh<<endl;
      cout << "the solution cannot be bracketed" << endl;
      exit(1);
    }
  }while(errore == 0);


  vec molar_fraction(4,0.0);
  if(xg < gas_fraction_[1])
  {
    x_fraction[1] = xg;
    molar_fraction[1] = xg;
    molar_fraction[3] = 1;
  }
  else
  {
    x_fraction[1] = gas_fraction_[1];
    molar_fraction[1] = gas_fraction_[1];
  }

  double xyws = molar_fraction[2]*molecular_weight_[0]+ molar_fraction[3]*molecular_weight_[1];
  for(int i=0; i<12; i++)
  {
    if(i >= 2) x_fraction[i] *=  1.-(x_fraction[0]+x_fraction[1]);
    fmol[i] = x_fraction[i]/ghiorso_par_[i];
  }
  fmol[2] = fmol[2] + 0.5*(fmol[6]+fmol[7]+fmol[8]+fmol[9]) + fmol[10]+fmol[11];

  double ox_sum = 0.;
  for(int i=0; i < 12; i++)
  {
    double oxn = fmol[i]*molecular_weight_[i];
    ox_sum += oxn;
    if(i <= 1) weight_fraction_[i] = oxn;
  }

  weight_fraction_[0] = weight_fraction_[0]/ox_sum;
  weight_fraction_[1] = weight_fraction_[1]/ox_sum;
  weight_fraction_[2] = molar_fraction[2]*molecular_weight_[0]/xyws;
  weight_fraction_[3] = 1.-weight_fraction_[2];

  //check mass conservation
  wtfrex_ = (gas_tot-weight_fraction_[0]-weight_fraction_[1])/(1.-weight_fraction_[0]-weight_fraction_[1]);

  //undersatured conditions
  if(wtfrex_ <= 1.e-16) wtfrex_ = 0.;
}





void Solwcad::exsolutionH2O(const double &p, const double &T, const vec& composition)
{
  p_ = p;
  T_ = T;

  vec oxide(12,0.0), x_fraction(12,0.0), fmol(12,0.0);
  reexpressComposition(composition, oxide, x_fraction, fmol);

  for(int i=0; i<2; i++)  gas_fraction_[i] = x_fraction[i];
  double gas_tot = oxide[0];
  double p_log = log(p_/p0_co2_);

  //set to zero variables
  for(int i = 0; i<2; i++) ctrm_[i] = 0;
  xwo_ = 0;

  for(int i = 2; i<11; i++)
  {
    ctrm_[0] += x_fraction[i]*wij_(i-2,0);
    ctrm_[1] += x_fraction[i]*(wij_(i-2,1)+p_log*wij_(i+8,1));
    for(int j=i+1; j<12; j++) xwo_ += x_fraction[i]*x_fraction[j]*wo_(i,j);
  }
  ctrm_[0] += x_fraction[11]*wij_(9,0);
  ctrm_[1] += x_fraction[11]*(wij_(9,1)+p_log*wij_(19,1));
  for(int i=0; i<2; i++) xtrm_[i] = (ctrm_[i]-xwo_)/(R_*T_);

  vec ref_term(2,0.0), phi(2,0.0), v_ref(2,0.0);

  referenceTermsH2O(ref_term, phi);
  rtrm_[0] = fol_[0] + ref_term[0]/(R_*T_);
  v_ref[0] = vr_;

  //set initial estimate for one gas
  vec oxide_1g(12,0.0), new_oxide_1g(12,0.0), x_fraction_1g(12,0.0);

  oxide_1g = {4.1e-6*pow(p_,0.5), 0.0};
  for(int i=2; i<12; i++) oxide_1g[i] = oxide[i];

  reexpressComposition(oxide_1g, new_oxide_1g, x_fraction_1g, fmol);
  vec gas_est = {x_fraction_1g[0], x_fraction_1g[0]*0.1};

  //start iteration
  int errore = 0;
  double ah, bh, yh, xg;
  double eps = 1.e-12;
  ah = 0.8*gas_est[0];
  bh = 1.2*gas_est[0];

  SolwcadFunction2 solwcad_function2(*this, phi[0], xtrm_[0], rtrm_[0]);
  do
  {
    if(bh >= 1.) bh = 1.;
    errore = nr_.secantMethod( solwcad_function2, ah, bh, eps, xg);
    if(errore == 0)
    {
      ah = 0.5*ah;
      bh = 1.5*bh;
    }
    if(ah < 1.e-15 && bh > 1.)
    {
      cout << "error from Solwcad::exsolution" << endl;
      cout << "at P = " << p_ << " and T = "<< T_<< endl;
      cout << "in solwcad.hpp line 950; ah = "<<ah<< "   bh = "<<bh<<endl;
      cout << "the solution cannot be bracketed" << endl;
      exit(1);
    }
  }while(errore == 0);


  vec molar_fraction(4,0.0);
  if(xg < gas_fraction_[0])
  {
    x_fraction[0] = xg;
    molar_fraction[0] = xg;
    molar_fraction[2] = 1;
  }
  else
  {
    x_fraction[0] = gas_fraction_[0];
    molar_fraction[0] = gas_fraction_[0];
  }

  double xyws = molar_fraction[2]*molecular_weight_[0]+ molar_fraction[3]*molecular_weight_[1];
  for(int i=0; i<12; i++)
  {
    if(i >= 2) x_fraction[i] *=  1.-(x_fraction[0]+x_fraction[1]);
    fmol[i] = x_fraction[i]/ghiorso_par_[i];
  }
  fmol[2]=fmol[2]+0.5*(fmol[6]+fmol[7]+fmol[8]+fmol[9]) +fmol[10]+fmol[11];

  double ox_sum = 0.;
  for(int i=0; i < 12; i++)
  {
    double oxn = fmol[i]*molecular_weight_[i];
    ox_sum += oxn;
    if(i <= 1) weight_fraction_[i] = oxn;
  }
  weight_fraction_[0] = weight_fraction_[0]/ox_sum;
  weight_fraction_[1] = weight_fraction_[1]/ox_sum;
  weight_fraction_[2] = molar_fraction[2]*molecular_weight_[0]/xyws;
  weight_fraction_[3] =1.-weight_fraction_[2];

  //check mass conservation
  wtfrex_ = (gas_tot-weight_fraction_[0]-weight_fraction_[1])/ (1.-weight_fraction_[0]-weight_fraction_[1]);

  //undersatured conditions
  if(wtfrex_ <= 1.e-16) wtfrex_ = 0.;

}








void Solwcad::referenceTermsCO2(vec& ref_term, vec& phi)
{
  //ref_term = liquid components reference terms
  //phi = gas components fugacities

  double T2(T_*T_), T3(T2*T_), p2(p_*p_), p3(p2*p_), p4(p3*p_);
  vr_ = par_[0] + par_[1]*T_ + par_[2]*T2 + par_[3]*T_*T_*T_ + p_*(par_[4] + par_[5]*T_ + par_[6]*T2) + p2*(par_[7] + par_[8]*T_) + p3*par_[9];

  double bm = 5.8e-5;
  double cm = (28.31+0.10721*T_-8.81e-6*T2)*1e-1;
  double dm = (9380.-8.53*T_+1.189e-3*T2)*1e-7;
  double em = (-368654.+715.9*T_+0.1534*T2)*1e-13;
  double ah = 2.e-5;
  double ym, bh, v;

  //start iteration
  int errore = 0;
  int counter = 0;
  int counter2 = 0;
  double ah_initial = ah;
  double ahfac = 0.1;
  double eps = 1.e-16;
  double fac = 0.2;
  SolwcadFunction1 solwcad_function1(*this, bm, cm, dm, em, ym);
  do
  {
    bh = ah*(1.+fac);
    errore = nr_.secantMethod( solwcad_function1, ah, bh, eps, v);
    //change boundaries
    if(errore == 0)
    {
      counter++;
      if(counter == 20)
      {
        counter = 0;
        counter2++;
        if(counter2 == 100)
        {
          counter2 = 0;
          ah = ah_initial;
          ahfac = -ahfac;
        }
        ah = ah*(1+ahfac);
        fac = 0.1;
      }
      fac = 2*fac;
    }
  }    while(errore == 0);
  ym = solwcad_function1.p5_;

  double vln = log((v+bm)/v);
  double t1 = 2. - T_/to_[1];
  double t2 = 2.*T_ - to_[1];
  double t3 = 2.*T2 - (to_[1]*to_[1]);
  double tp = pow(T_,1.5);
  double pb1(R_*tp*bm), pb2(pb1*bm), pb3(pb2*bm);
  double pv1(R_*tp*v), pv2(pv1*v);
  double bv2 = 2.*pb1*v*v;
  double vb = v + bm;
  double ym2(ym*ym), ym3(ym2*ym);
  double ym_menus3 = pow(1.-ym,3);

  double phexp = (8.*ym - 9.*ym2 + 3.*ym3)/ym_menus3 - cm/(R_*tp*vb) - dm/(pv1*vb) - em/(pv2*vb) - cm/pb1*vln - dm/(pv1*bm) + dm/pb2*vln - em/bv2+em/(pb2*v) - em/pb3*vln - log(p_*v/(R_*T_));
  phi[1] = exp(phexp);
  ref_term[1] = p_*(par_[0]*t1 + par_[1]*T_ + par_[2]*T_*t2 + par_[3]*T_*t3) + p2*(par_[4]*t1 + par_[5]*T_ + par_[6]*T_*t2)*0.5 + p3*(par_[7]*t1 + par_[8]*T_)/3. + p4*par_[9]*t1*0.25;
}





void Solwcad::referenceTermsH2O(vec& ref_term, vec& phi)
{
  //ref_term = liquid components reference terms
  //phi = gas components fugacities

  double T2(T_*T_), T3(T2*T_), p2(p_*p_), p3(p2*p_), p4(p3*p_);
  vec bd = {9.144e-6, 3.685e-9, 1.168e-11, -1.99e-15, 1.22e-16, -1.945e-17, -1.58e-21, 4.68e-24, 1.144e-26, -3.96e-33} ;
  vr_ = bd[0] + bd[1]*T_ + bd[2]*T2 + bd[3]*T3 + p_*(bd[4]+ bd[5]*T_ + bd[6]*T2) + p2*(bd[7] + bd[8]*T_) + p3*bd[9];

  double bm = 2.9e-5;
  double cm = (290.78 - 0.30276*T_ + 1.4774e-4*T2)*1e-1;
  double dm = (-8374. + 19.437*T_ - 8.148e-3*T2)*1e-7;
  double em = (76600. - 133.9*T_ + 0.1071*T2)*1e-13;
  double ah = 1.9901e-5 - 4.6125e-15*p_ + 5.5523e-25*p2;
  double ym, bh, v;

  //start iteration
  int errore = 0;
  int counter = 0;
  int counter2 = 0;
  double ah_initial = ah;
  double ahfac = 0.1;
  double eps = 1.e-16;
  double fac = 0.2;
  SolwcadFunction1 solwcad_function1(*this, bm, cm, dm, em, ym);
  do
  {
    bh = ah*(1.+fac);
    errore = nr_.secantMethod( solwcad_function1, ah, bh, eps, v);
    //change boundaries
    if(errore == 0)
    {
      counter++;
      if(counter == 20)
      {
        counter = 0;
        counter2++;
        if(counter2 == 100)
        {
          counter2 = 0;
          ah = ah_initial;
          ahfac = -ahfac;
        }
        ah = ah*(1+ahfac);
        fac = 0.1;
      }
      fac = 2*fac;
    }
  }    while(errore == 0);
  ym = solwcad_function1.p5_;

  double vln = log((v+bm)/v);
  double t1 = 2. - T_/to_[0];
  double t2 = 2.*T_ - to_[0];
  double t3 = 2.*T2 - (to_[0]*to_[0]);
  double tp = pow(T_,1.5);
  double pb1(R_*tp*bm), pb2(pb1*bm), pb3(pb2*bm);
  double pv1(R_*tp*v), pv2(pv1*v);
  double bv2 = 2.*pb1*v*v;
  double vb = v + bm;
  double ym2(ym*ym), ym3(ym2*ym);
  double ym_menus3 = pow(1.-ym,3);

  double phexp = (8.*ym - 9.*ym2 + 3.*ym3)/ym_menus3 - cm/(R_*tp*vb) - dm/(pv1*vb) - em/(pv2*vb) - cm/pb1*vln - dm/(pv1*bm) + dm/pb2*vln - em/bv2 + em/(pb2*v) - em/pb3*vln - log(p_*v/(R_*T_));
  phi[0] = exp(phexp);
  ref_term[0] = p_*(bd[0]*t1 + bd[1]*T_ + bd[2]*T_*t2 + bd[3]*T_*t3) + p2*(bd[4]*t1 + bd[5]*T_ + bd[6]*T_*t2)*0.5 + p3*(bd[7]*t1 + bd[8]*T_)/3. + p4*bd[9]*t1*0.25;
}






void Solwcad::vecfunc(vec& x, vec& fv)
{
  const double tin = 1.e-16;
  vec x2(2,0.0);

  for(int i=0; i<2; i++)
  {
    if(x[i] <= 0.) x[i] = tin;
    x2[i] = x[i]*scale_factor_[i];
  }

  y_[0] = (gas_fraction_[0]*(1.-x2[1])-x2[0]*(1.-gas_fraction_[1]))/(gas_fraction_[1]-x2[1]+gas_fraction_[0]-x2[0]);

  if(y_[0] <= tin) y_[0] = tin;
  if(y_[0] >= (1.-tin)) y_[0] = 1.-tin;

  y_[1] = 1-y_[0];

  int inot;
  for(int i=0; i<2; i++)
  {
    if(i == 1) inot = 0;
    if(i == 0) inot = 1;

    if(y_[i] <= 0.)
    {
      y_[i] = tin;
      y_[inot] = 1.-tin;
    }
    if(y_[i] >= 1.)
    {
      y_[i] = 1.-tin;
      y_[inot] = 1.-y_[i];
    }
  }

  vec phi(2,0.0), gtrm(2);
  //phi = gas components fugacities

  vr_ = 0.0 ;

  double T2=T_*T_;
  vec xl = {1.-y_[1], y_[1]};
  vec b = {2.9e-5, 5.8e-5};
  vec c(3, 0.0), d(3, 0.0), e(3, 0.0);
  c = {(290.78 - 0.30276*T_ + 1.4774e-4*T2)*1e-1,  (28.31 + 0.10721*T_ - 8.81e-6*T2)*1e-1};
  d = {(-8374. + 19.437*T_ - 8.148e-3*T2)*1e-7,  (9380. - 8.53*T_ + 1.189e-3*T2)*1e-7};
  e = {(76600. - 133.9*T_ + 0.1071*T2)*1e-13,  (-368654. + 715.9*T_ + 0.1534*T2)*1e-13};

  if(c[0]*c[1] <= 0.)  c[2] = 0.;
  else c[2] = sqrt(c[0]*c[1]);

  if(d[0]*d[1] <= 0.)  d[2] = 0.;
  else d[2] = sqrt(d[0]*d[1]);

  if(e[0]*e[1] <= 0.)  e[2] = 0.;
  else e[2] = sqrt(e[0]*e[1]);

  double bm = xl[0]*b[0] + xl[1]*b[1];
  double cm = pow(xl[0],2)*c[0] + pow(xl[1],2)*c[1] + 2*xl[0]*xl[1]*c[2];
  double dm = pow(xl[0],2)*d[0] + pow(xl[1],2)*d[1] + 2*xl[0]*xl[1]*d[2];
  double em = pow(xl[0],2)*e[0] + pow(xl[1],2)*e[1] + 2*xl[0]*xl[1]*e[2];

  double ah = 1.9901e-5 - 4.6125e-15*p_ + 5.5523e-25*p_*p_ + 1.3539e-5*y_[1];
  if(T_ <= 1300.) ah = ah*0.8;
  double ym, bh, v;

  //start iteration
  int errore = 0;
  int counter = 0;
  int counter2 = 0;
  double ah_initial = ah;
  double ahfac = 0.1;
  double eps = 1.e-16;
  double fac = 0.2;
  SolwcadFunction1 solwcad_function1(*this, bm, cm, dm, em, ym);

  do
  {
    bh = ah*(1.+fac);
    errore = nr_.secantMethod( solwcad_function1, ah, bh, eps, v);
    if(errore == 0)
    {
      counter++;
      if(counter == 20)
      {
        counter = 0;
        counter2++;
        if(counter2 == 100)
        {
          counter2 = 0;
          ah = ah_initial;
          ahfac = -ahfac;
        }
        ah = ah*(1+ahfac);
        fac = 0.1;
      }
      fac = 2*fac;
    }

  }    while(errore == 0);
  ym = solwcad_function1.p5_;

  double vln = log((v+bm)/v);
  double tp = pow(T_,1.5);
  double pb1(R_*tp*bm), pb2(pb1*bm), pb3(pb2*bm), pb4(pb3*bm);
  double pv1(R_*tp*v), pv2(pv1*v);
  double bv2 = 2.*pb1*v*v;
  double vb = v+bm;
  double ym2(ym*ym), ym3(ym2*ym);
  double ym_menus2(pow(1.-ym,2)), ym_menus3(ym_menus2*(1.-ym));

  for(int i=0; i<2; i++)
  {
    double bc1 = b[i]*cm;
    double bd1 = b[i]*dm;
    double be1 = b[i]*em;
    double c1 = 2.*c[i]*xl[i] + 2.*(1.-xl[i])*c[2];
    double d1 = 2.*d[i]*xl[i] + 2.*(1.-xl[i])*d[2] + dm;
    double e1 = 2.*(e[i]*xl[i] + e[2]*(1.-xl[i]) + em);

    double phexp = (4.*ym - 3.*ym2)/(ym_menus2) + b[i]/bm*(4.*ym - 2.*ym2)/(ym_menus3) - c1/pb1*vln - bc1/(pb1*vb) + bc1/pb2*vln - d1/(pb1*v) + d1/pb2*vln + bd1/(pb1*v*vb)
                   + 2.*bd1/(pb2*vb) - 2.*bd1/pb3*vln - e1/bv2 + e1/(pb2*v) - e1/pb3*vln + be1/(bv2*vb) - 3.*be1/(2.*pb2*v*vb) + 3.*be1/pb4*vln - 3.*be1/(pb3*vb) - log(p_*v/(R_*T_));
    phi[i] = exp(phexp);
  }

  for(int i=0; i<2; i++)
  {
    if(i == 1) inot = 0;
    if(i == 0) inot = 1;
    gtrm[i] = log(phi[i]*y_[i]*p_/x2[i]);
    xtrm_[i]= (1.-x2[0]-x2[1])*((1.-x2[i])*ctrm_[i]-x2[inot]*ctrm_[inot])-(1.-x2[0]-x2[1])*(1.-x2[0]-x2[1])*xwo_+(1.-x2[i])*x2[inot]*whc_;
    xtrm_[i] = xtrm_[i]/(R_*T_);
    fv[i] = gtrm[i]-xtrm_[i]-rtrm_[i];
  }
}








#endif
