#!/usr/bin/python3





import matplotlib.pyplot as plt
import numpy as np
import os
import math
import csv
import mu_nu 





#---------------------------------Mogi----------------------------------------------
  
print ('')
print ('          ============ Mogi model===============')
print ('')
print ('')
print (' _________________________(x,0,z)________________________     _____________________ +x')
print ('                           /|                                 |')
print ('                          / |                                 |')
print ('                         /  |                                 |')
print ('                    (R) /   |                                 |')
print ('                       /    |                                 |')
print ('                      /     |                                 |')
print ('                     /      |(d)                              |')
print ('                    /       |                                 |')
print ('                 - /        |                                 | -y')
print ('              -   /-        |     ')
print ('            -    /   -      |     ')
print ('           (  (0,-d,0) )    |     ')
print ('            -        -            ')
print ('              -    -              ')
print ('                -                 ')
print ('                -------           ')
print ('                  (r)             ')
print ('')
print ('')
print ('            | u1 | = dp * r^3 * (1-nu) *  | x/R^3 | ')
print ('            | u2 |        ------------    | d/R^3 | ')
print ('            | u3 |             mu         | z/R^3 | ')
print ('')
print ('')
print ('')


print('------- In the following to chose default values press Enter key when asked to insert custom values -------------')
print ('')


print('Default chamber radius:   r = 1000.0 m')
r = float(input("Enter custom r: ") or '1.e3')
print('r = ', r)
print()



print('Default depth of chamber:   d = 4000 m')
d = float(input("Enter custom d: ") or '4000.0')
print('d = ', d)
print()



mu, nu = mu_nu.mu_nu()



print ('Default overpressure:   dp = 1.e7 Pa')
dp = float(input("Enter custom dp: ") or '1.e7')
print('dp = ', dp)
print()


coefficient = r*r*r*dp*(1-nu)/mu
print()
print()



print('-------------- we plot for x and y axis ---------------------')
print()
print()


print('Default semi_x_length:    x = 50000 m')
semi_x_length = float(input("Enter custom x: ") or '50.e3')
print('semi_x_length = ', semi_x_length)
print()



x = np.arange(-semi_x_length, semi_x_length, 1.0)
h = 0.0;  # flat surface




u1, u2 = [], []
for i in range(len(x)):
#  h = 1500.0*math.exp(-x[i]*x[i]/(2.0*1.e3*1.e3))
  R = math.sqrt(x[i]*x[i] + (d+h)*(d+h))
  u1.append(coefficient*x[i]/(R*R*R))
  u2.append(coefficient*d/(R*R*R))



f = open('Mogi_u.txt', 'w')
f.write("Points_X,u_X,u_Y" + os.linesep )
for i in range(len(x)):
    f.write("{},{},{}".format(str(x[i]), str(u1[i]), str(u2[i])) + os.linesep )
f.close()




plt.figure()

plt.subplot(121)
plt.xlabel('x(m)')
plt.ylabel('ux')
plt.plot(x, u1, label="Mogi")
plt.grid(True)
plt.legend()


plt.subplot(122)
plt.xlabel('x(m)')
plt.ylabel('uy')
plt.plot(x, u2, label="Mogi")
plt.grid(True)
plt.legend()

plt.show()

