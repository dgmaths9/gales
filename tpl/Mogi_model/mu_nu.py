#!/usr/bin/python3

import math






def get_E():
          print('Default Elasticity modulus:    E = 25.e9')
          E = float(input("Enter custom E: ") or '25.e9')
          print('E = ', E)
          print()
          return E


def get_mu():
          print('Default shear modulus:    mu = 1.e10')
          mu = float(input("Enter custom mu: ") or '1.e10')
          print('mu = ', mu)
          print()
          return mu

def get_nu():
          print('Default Poisson ratio:    nu = 0.25')
          nu = float(input("Enter custom nu: ") or '0.25')
          print('nu = ', nu)
          print()
          return nu

def get_lam():
          print('Default Lambda value:    lambda = 1.e10')
          lam = float(input("Enter custom lambda: ") or '1.e10')
          print('lambda = ', lam)
          print()
          return lam



def mu_nu():
      print('Which pair of following parameters are known?')
      print()
      print('1   Shear modulus(mu) and Poisson ratio(nu)')
      print('2   Elasticity modulus (E) and Shear modulus(mu)')
      print('3   Elasticity modulus (E) and Poisson ratio(nu)')
      print('4   Lambda (lam) and Shear modulus(mu)')
      print('5   Lambda (lam) and Elasticity modulus(E)')
      print('6   Lambda (lam) and Poisson ratio(nu)')
      print()
      num = input('Please insert the number corresponding to the known pair: ')

      if(num == '1'):
          mu = get_mu() 
          nu = get_nu() 
          return mu, nu

      elif(num == '2'): 
          E = get_E() 
          mu = get_mu()
          nu = 0.5*E/mu - 1
          print('Computed nu: ', nu)
          print()
          return mu, nu

      elif(num == '3'): 
          E = get_E() 
          nu = get_nu()
          mu = 0.5*E/(1.0+nu)
          print('Computed mu: ', mu)
          print()
          return mu, nu 

      elif(num == '4'): 
          lam = get_lam() 
          mu = get_mu() 
          nu = 0.5*lam/(lam+mu)
          print('Computed nu: ', nu)
          print()
          return mu, nu 
          
      elif(num == '5'): 
          lam = get_lam()
          E = get_E() 
          R = math.sqrt(E**2 + 9*(lam**2) + 2*E*lam)
          mu = (E - 3*lam + R)/4
          nu = 2*lam/(E+lam+R)
          print('Computed mu: ', mu)
          print('Computed nu: ', nu)
          print()
          return mu, nu 

      elif(num == '6'): 
          lam = get_lam() 
          nu = get_nu() 
          mu = 0.5*lam*(1.0-2*nu)/nu
          print('Computed mu: ', mu)
          print()
          return mu, nu 



      

