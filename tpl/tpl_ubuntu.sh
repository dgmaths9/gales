#!/bin/bash

# exit script on error
set -e

##### PARSE ARGUMENTS  ###############################################

# prints usage message
print_usage () {
    echo "Usage: $(basename $0) [OPTION...] [TARGET]..."
}

# prints usage message with option descriptions
print_help () {
cat <<- EOF
This script extracts and builds all the dependencies required to compile and run Gales. 

Available options:
  -f, --force-install      force build and istall / creation of symlink to
                           binaries for specified target(s)
  -x, --force-extraction   force extraction and installation of specified
                           target tar(s)
  -j, --parallel=          set build parallelism level
  -h, --help               show this message

Packages are only built if their installation folder is not found automatically by the
script, or if a rebuild is forced with -f.
If one or more specific packages need to be (re)built (without touching the others), 
they can be passed as targets to the script.

Available targets:
  cmake, gmsh, trilinos

EOF
}


#---- PARSE COMMAND LINE  -------------------------------------------# 

# empty targets dictionary
declare -A TARGETS

# parse command line
while [[ "$#" -gt 0 ]]; do
    case "$1" in
    cmake|gmsh|trilinos)
        # append target to list of targets
        TARGETS["$1"]=true
        shift
        ;;
    -f|--force-build)
        FORCE_BUILD=true
        shift
        ;;
    -x|--force-extract)
        # force tar (re) extraction and rebuild
        FORCE_EXTRACT=true
        shift
        ;;
    -j)
        # level of parallelism for the build process
        NPARALLEL=$2
        shift 2
        ;;
    --parallel=*)
        # same as -j, different syntax
        NPARALLEL=$(echo $1 | cut -d '=' -f 2)
        shift
        ;;
    -h|--help)
        # shows help message 
        print_usage
        print_help
        exit 0
        ;;
    *)
        echo "Invalid target or option: ${target}"
        print_usage
        exit 1
        ;;
    esac
done

# handle default case with no targets (equal to all)
if [[ "${#TARGETS[@]}" -eq 0 ]]; then
    TARGETS[cmake]=true
    TARGETS[gmsh]=true
    TARGETS[trilinos]=true
    INSTALL_SYS_PKGS=true
fi

# set default parallelism level to 1
if [[ "${NPARALLEL}" = "" ]]; then
    NPARALLEL=1
fi



###### SCRIPT VARIABLES CONFIGURATION ################################

# cd in script directory (tpl directory)
cd $(dirname $0)

# global directories
TPL_ROOT_DIR="$(pwd)"
TPL_TAR_DIR="${TPL_ROOT_DIR}/tars"
TPL_SRC_DIR="${TPL_ROOT_DIR}/src"
TPL_LIB_DIR="${TPL_ROOT_DIR}/tpl"
LOGFILE="${TPL_ROOT_DIR}/log.txt"

# check existence of src directory
if [[ ! -d "${TPL_SRC_DIR}" ]]; then
    mkdir "${TPL_SRC_DIR}"
fi

# check existence of tpl directory
if [[ ! -d "${TPL_LIB_DIR}" ]]; then
    mkdir "${TPL_LIB_DIR}"
fi

# delete and recreate logfile if already existing
rm -f "${LOGFILE}" && touch "${LOGFILE}"


#---- TARGET DIRECTORIES --------------------------------------------#

CMAKE_TARBALL="${TPL_TAR_DIR}/cmake-3.26.2-linux-x86_64.tar.gz"
CMAKE_EXTRACT_DIR="${TPL_SRC_DIR}/cmake-3.26.2-linux-x86_64"
CMAKE_INSTALL_DIR="${TPL_LIB_DIR}/cmake-3.26.2"

GMSH_TARBALL="${TPL_TAR_DIR}/gmsh-4.11.1-Linux64-sdk.tgz"
GMSH_EXTRACT_DIR="${TPL_SRC_DIR}/gmsh-4.11.1-Linux64-sdk"
GMSH_INSTALL_DIR="${TPL_LIB_DIR}/gmsh-4.11.1"

TRILINOS_TARBALL="${TPL_TAR_DIR}/Trilinos-14.tar.gz"
TRILINOS_EXTRACT_DIR="${TPL_SRC_DIR}/Trilinos-trilinos-release-14-0-0"
TRILINOS_INSTALL_DIR="${TPL_LIB_DIR}/trilinos-14.0.0"



##### SET DEPENDENCIES FOR TARGET BUILDS #############################

# function that sets cmake as target if not already targeted or built
require_cmake () {
    if [[ ! "${TARGETS[cmake]}" = true ]] && \
       [[ ! -d "${CMAKE_INSTALL_DIR}" ]]; then
        TARGETS[cmake]=true
        DEP_cmake=1
    fi
}


#---- CHECK DEPENDENCIES AND PRINT CONFIG ---------------------------#

# print initial target configuration
echo -e "\n########## GALES THIRD PARTY LIBRARIES INSTALLER ##########\n" | \
    tee -a "${LOGFILE}"
echo -e "The following targets were requested:\n  " | \
    tee -a "${LOGFILE}"
for key in "${!TARGETS[@]}"; do
    printf ' %s,' $key | tee -a "${LOGFILE}"
done
echo  -e "\n" | tee -a "${LOGFILE}"

# check trilinos dependencies
if [[ "${TARGETS[trilinos]}" = true ]]; then
    require_cmake
fi

# if dependencies were added, print new target configuration
NDEPS=$((DEP_cmake + DEP_freet + DEP_occt + DEP_lapack + DEP_boost))
if [[ ! "$NDEPS" = 0 ]]; then
    echo -e "Missing target dependencies, ${NDEPS} target(s) were added" | \
        tee -a "${LOGFILE}"
    echo -e "The following targets will be processed:\n" | \
        tee -a "${LOGFILE}"
    for key in "${!TARGETS[@]}"; do
        printf ' %s,' $key | tee -a "${LOGFILE}"
    done
    echo -e "\n" | tee -a "${LOGFILE}"
fi 



##### EXTRACT AND BUILD PACKAGES #####################################

# this function checks if extraction is required, then
# removes previous extraction and extracts tar archive 
extract_tar_checked () {
    echo "[TPL_INSTALL_LOG] extracting $(basename $1)..." | \
        tee -a "${LOGFILE}"
    # check if extraction is required
    if [[ ! -d "${2}" ]] || \
       [[ "${FORCE_EXTRACT}" = true ]]
    then
        # extract (or re-extract)
        rm -rf "${2}"                       # remove extraction dir if present
        tar -C "${TPL_SRC_DIR}" -xf "${1}"  # extract package
    else 
        echo "[TPL_INSTALL_LOG] $(basename $1) extraction found" | \
            tee -a "${LOGFILE}"
    fi
}


# first log message
echo "[TPL_INSTALL_LOG] start tpl installation"


#---- APT DEPENDENCIES ----------------------------------------------#

if [[ "${INSTALL_SYS_PKGS}" = true ]]; then

echo "[TPL_INSTALL_LOG] installing build tools..."
sudo apt install -y build-essential gfortran | \
    tee -a "${LOGFILE}"

echo "[TPL_INSTALL_LOG] installing openmpi..."
sudo apt install -y libopenmpi-dev openmpi-bin | \
    tee -a "${LOGFILE}"

echo "[TPL_INSTALL_LOG] installing hd5 utilities..."
sudo apt install -y libhdf5-openmpi-dev hdf5-tools | \
    tee -a "${LOGFILE}"

echo "[TPL_INSTALL_LOG] installing blas and lapack..."
sudo apt install -y libblas-dev liblapack-dev | \
    tee -a "${LOGFILE}"

echo "[TPL_INSTALL_LOG] installing boost..."
sudo apt install -y libboost-all-dev | \
    tee -a "${LOGFILE}"

echo "[TPL_INSTALL_LOG] installing metis..."
sudo apt install -y metis | \
    tee -a "${LOGFILE}"

# needed for gmsh dependencies 
echo "[TPL_INSTALL_LOG] installing gmsh..."
sudo apt install -y gmsh | \
    tee -a "${LOGFILE}"

fi


#---- CMAKE ---------------------------------------------------------#
if [[ "${TARGETS[cmake]}" = true ]]; then
    
    extract_tar_checked "${CMAKE_TARBALL}" "${CMAKE_EXTRACT_DIR}"

    echo "[TPL_INSTALL_LOG] creating symlink to cmake binaries..." | \
        tee -a "${LOGFILE}" 
    # check if build is required
    if [[ ! -d "${CMAKE_INSTALL_DIR}" ]] || \
       [[ "${FORCE_BUILD}" = true ]]
    then
        # create (or recreate) symlink to binaries
        rm -rf "${CMAKE_INSTALL_DIR}" # clear old symlink
        ln -Ts "${CMAKE_EXTRACT_DIR}" "${CMAKE_INSTALL_DIR}" | \
            tee -a "${LOGFILE}"       # recreate symlink
    else
        echo "[TPL_INSTALL_LOG] symlink to cmake binaries found" | \
            tee -a "${LOGFILE}"
    fi
    
fi # end cmake block

# use the cmake we just built for the rest of the script
PATH="${CMAKE_INSTALL_DIR}/bin:$PATH"



#---- GMSH ----------------------------------------------------------#
if [[ "${TARGETS[gmsh]}" = true ]]; then

    extract_tar_checked "${GMSH_TARBALL}" "${GMSH_EXTRACT_DIR}"

    echo "[TPL_INSTALL_LOG] creating symlink to gmsh binaries..." | \
        tee -a "${LOGFILE}"
    # check if build is required
    if [[ ! -d "${GMSH_INSTALL_DIR}" ]] || \
       [[ "${FORCE_BUILD}" = true ]]
    then
        # create (or recreate) symlink to binaries
        rm -rf "${GMSH_INSTALL_DIR}" # clear old symlink
        ln -vTs "${GMSH_EXTRACT_DIR}" "${GMSH_INSTALL_DIR}" | \
            tee -a "${LOGFILE}"      # recreate symlink
    else
        echo "[TPL_INSTALL_LOG] symlink to gmsh binaries found" | \
            tee -a "${LOGFILE}"
    fi

fi # end gmsh block


#---- TRILINOS ------------------------------------------------------#
if [[ "${TARGETS[trilinos]}" = true ]]; then

    extract_tar_checked "${TRILINOS_TARBALL}" "${TRILINOS_EXTRACT_DIR}"

    echo "[TPL_INSTALL_LOG] building trilinos..." | \
        tee -a "${LOGFILE}"
    # check if build is required
    if [[ ! -d "${TRILINOS_INSTALL_DIR}" ]] || \
       [[ "${FORCE_BUILD}" = true ]]
    then
        # build (or rebuild)
        cd "${TRILINOS_EXTRACT_DIR}"
        rm -rf "${TRILINOS_INSTALL_DIR}"  # clean previous install
        rm -rf build                      # clean previous build
        mkdir -p build && cd build        # create build directory
       
        cmake \
            -DTrilinos_ENABLE_Belos=ON \
            -DTrilinos_ENABLE_EpetraExt=ON \
            -DTrilinos_ENABLE_Epetra=ON \
            -DTrilinos_ENABLE_Ifpack=ON \
            -DTrilinos_ENABLE_Ifpack2=ON \
            -DTrilinos_ENABLE_Kokkos=ON \
            -DTrilinos_ENABLE_KokkosKernels=ON \
            -DTrilinos_ENABLE_ML=ON \
            -DTrilinos_ENABLE_MueLu=ON \
            -DTrilinos_ENABLE_Teuchos=ON \
            -DTrilinos_ENABLE_Tpetra=ON \
              -DTpetra_ENABLE_CUDA=OFF \
              -DTpetra_INST_SERIAL=ON \
              -DTpetra_INST_OPENMP=OFF \
              -DTpetra_INST_DOUBLE=ON \
              -DTpetra_ASSUME_CUDA_AWARE_MPI=OFF \
              -DTpetra_ENABLE_EXAMPLES=OFF \
            -DTrilinos_ENABLE_ALL_OPTIONAL_PACKAGES=ON \
            -DTrilinos_ENABLE_EXPLICIT_INSTANTIATION=ON \
            -DTrilinos_ENABLE_DEBUG_SYMBOLS=ON \
            -DTrilinos_ENABLE_TESTS=ON \
            -DTPL_ENABLE_MPI=ON \
            -DTPL_ENABLE_Boost=ON \
            -DTPL_ENABLE_HDF5=ON \
            -DCMAKE_BUILD_TYPE=RELEASE \
            -DCMAKE_CXX_STANDARD=17 \
            -DBUILD_SHARED_LIBS=ON \
            -DCMAKE_INSTALL_PREFIX="${TRILINOS_INSTALL_DIR}" \
            .. | \
            tee -a "${LOGFILE}"

        cmake --build . -j "${NPARALLEL}" --target install | \
            tee -a "${LOGFILE}"

        echo -e "\n\n" | tee -a "${LOGFILE}"
    else
        echo "[TPL_INSTALL_LOG] trilinos installation found" | \
            tee -a "${LOGFILE}"
    fi
   
fi # end trilinos block



##### GENERATE ENVIRONMENT FILE #####################################

# set envfile name
ENVFILE="${TPL_ROOT_DIR}/setenv_gales"
MISSING=()

# remove old env file
rm -rf "${ENVFILE}"

# add extracted cmake to path
if [[ -d "${CMAKE_INSTALL_DIR}" ]]; then
cat <<- EOF >> "${ENVFILE}"
# add cmake directories to path
export PATH=${CMAKE_INSTALL_DIR}/bin:\$PATH
EOF
echo -e "\n" >> "${ENVFILE}"
else 
MISSING+=('cmake')
fi

# add built gmsh to path and libpath
if [[ -d "${GMSH_INSTALL_DIR}" ]]; then 
cat <<- EOF >> "${ENVFILE}"
# add gmsh directories to paths
export PATH=${GMSH_INSTALL_DIR}/bin:\$PATH
export LIBRARY_PATH=${GMSH_INSTALL_DIR}/lib:\$LIBRARY_PATH
export LD_LIBRARY_PATH=${GMSH_INSTALL_DIR}/lib:\$LD_LIBRARY_PATH
export PYTHONPATH=${GMSH_EXTRACT_DIR}/lib:\$PYTHONPATH
export GMSH_ROOT=${GMSH_INSTALL_DIR}
export GMSH_INCLUDE=${GMSH_INSTALL_DIR}/include
export GMSH_INC=${GMSH_INSTALL_DIR}/include
export GMSH_LIB=${GMSH_INSTALL_DIR}/lib
EOF
echo -e "\n" >> "${ENVFILE}"
else 
MISSING+=('gmsh')
fi

# add built trilinos directories to path
if [[ -d "${TRILINOS_INSTALL_DIR}" ]]; then
cat <<- EOF >> "${ENVFILE}"
# adding trilinos directories to paths
export LIBRARY_PATH=${TRILINOS_INSTALL_DIR}/lib:\$LIBRARY_PATH
export LD_LIBRARY_PATH=${TRILINOS_INSTALL_DIR}/lib:\$LD_LIBRARY_PATH
export TRILINOS_ROOT=${TRILINOS_INSTALL_DIR}
export TRILINOS_INCLUDE=${TRILINOS_INSTALL_DIR}/include
export TRILINOS_INC=${TRILINOS_INSTALL_DIR}/include
export TRILINOS_LIB=${TRILINOS_INSTALL_DIR}/lib
export Trilinos_DIR=${TRILINOS_INSTALL_DIR}/lib/cmake
EOF
echo -e "\n" >> "${ENVFILE}"
else 
MISSING+=('trilinos')
fi 

# export gales directories
cat <<- EOF >> "${ENVFILE}"
# adding gales utilities to path
export TPL_PATH=${TPL_ROOT_DIR} 
export PATH=${TPL_ROOT_DIR}/gales_mesh_preprocessing:\$PATH
export PATH=${TPL_ROOT_DIR}/gales_results_postprocessing:\$PATH
export PATH=${TPL_ROOT_DIR}/Mogi_model:\$PATH
export PATH=${TPL_ROOT_DIR}/solwcad:\$PATH 
export PYTHONPATH=${TPL_ROOT_DIR}/gales_results_postprocessing:\$PYTHONPATH
export PYTHONPATH=${TPL_ROOT_DIR}/Mogi_model:\$PYTHONPATH
EOF

# echo final message
echo -e "\nEnvironment file generated at: ${ENVFILE}\n"
if [[ ! "${#MISSING[@]}" -eq 0 ]]; then
    echo -e "WARNING: incomplete environment file generated, missing targets:\n"
    for target in "${MISSING[@]}"; do
        echo -n " $target,"
    done
    echo -e "\n\nBuild the missing targets before sourcing the environment file\n"
fi
