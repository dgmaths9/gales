# Installing Gales dependencies
In order to correctly compile, Gales needs a few third party libraries, together with a working C++14 compiler and an MPI distribution (e.g. openmpi, mpich, Intel mpi).

A list of all the software required to build Gales is provided below:

- A C++14 compiler
- An MPI distribution
- [CMake](https://cmake.org/)
- [METIS](http://glaros.dtc.umn.edu/gkhome/metis/metis/overview/)
- [Gmsh](https://gmsh.info/)
    - [Freetype](http://freetype.org/)
    - [OpenCascade Technology (OCCT)](https://dev.opencascade.org/)
- [Trilinos](https://trilinos.github.io/)
    - A BLAS implementation (e.g. openblas, MKL, Netlib BLAS)
    - A LAPACK implementation (e.g. openblas, MKL, Netlib LAPACK)
    - [Boost](https://www.boost.org/)


In the `tpl` folder (this folder) you can find a selection of scripts that will help you installing the required third party libraries on some commonly-used systems. You can use the menu in the next section to navigate to the section relative to the system you are using.

After running the desired installation script, an environment file will be generated inside the `tpl` directory. Sourcing this file will add the installed libraries to the enviroment, allowing you to compile the examples in the Gales `sim` directory, or creating your own.

In order to source the file you can simply run the following command in your shell:
```bash
. /path/to/setenv_gales
```

If you are running gales on a Mac (experimental), using zsh as your shell, the relative path to the file must be specified starting from the directory you are in, thus the command becomes
```zsh
. ./path/to/setenv_gales 
```

## Navigation menu
- [Summary](#installing-gales-dependencies)
- [Navigation](#navigation-menu)
- [Installing on Ubuntu](#installing-on-ubuntu)
- [Installing on macOS (experimental)](#installing-on-macos)
- [Installing on CINECA's clusters](#installing-on-cineca-clusters)
    - [Building with Intel toolchain](#building-with-intel-toolchain)
- [Installing on a generic Linux machine](#installing-on-a-generic-linux-machine)
- [Reinstalling a dependency](#reinstalling-a-dependency)

## Installing on Ubuntu
This section covers the installation of Gales third party libraries on systems running Ubuntu (only tested on 22.04). In order to install the required software you can simply run the `tpl_ubuntu.sh` script. Where possible, the script will install the required packages via the system's package manager. The remaining dependencies will be built from source.

To run the script, you can simply type the following command in your shell:
```bash
./path/to/tpl_ubuntu.sh
```

## Installing on macOS
This section covers how to install Gales dependencies on a system running macOS. Before installing, remember that Gales is not fully tested on macOS, thus unforeseen problems may arise. 

In this case, the script to use is `tpl_mac.sh`. This script relies on the Homebrew package manager to install the majority of the required software, thus you must have it installed on your system for the script to work correctly. For a guide on how to install Homebrew you can check the [project's website](https://brew.sh/).

The only required package that is not installed vie Homebrew is Trilinos, which is compiled from source using the system's compiler (which on macOS is a customized version of `clang`). For compatibility, we suggest to also compile your models using the same compiler. Be aware that this compiler defaults to an old C++ standard (C++98), thus some errors may arise during the compilation of a model, if you use modern C++ features but forget to compile specifying an adequate standard (minimun for GALES is C++14).

To run the installation script, simply type the following command in your shell:
```bash
./path/to/tpl_mac.sh
```

## Installing on CINECA clusters
This section covers the installation of Gales on CINECA's clusters (Galileo100 and Leonardo). For this purpose you can use either `tpl_g100.sh` or `tpl_leo.sh`, depending on the system you are using.
These scripts make use software modules available on the cluster where possible (i.e. for compiler, BLAS, LAPACK, MPI, Boost), and build the remaining dependencies from source.

In order to run the installation script, you can simply run the following commands
```bash
module purge
./path/to/tpl_g100.sh
```
or the alternative version with `tpl_leonardo.sh`.

*NOTE*: the `module purge` command cleans the module environment before running the script. This is required since the script loads some modules in order to build the required software from source. If you already have other modules loaded in your environment, there could be conflicts and the script may fail to execute correctly.


### Building with Intel toolchain
By default the scripts use the GNU compilers to build the third party libraries from source, and the [netlib-LAPACK](https://www.netlib.org/lapack/) module for the numerical libraries (BLAS and LAPACK). It is also possible to select an alternative build toolchain based on Intel compilers and the MKL library from the Intel OneAPI toolkit. 

To build Gales using this alternative toolchain, you can run the script as follows:
```bash
module purge
./path/to/tpl_g100.sh --use-intel-toolchain
```

It is possible to keep third party libraries compiled with both toolchains installed at the same time (they will be installed in differently-named folders), then switch between the two toolchains simply by running the script with or without the `--use-intel-toolchain`flag. This will simply regenerate the `setenv_gales` environment file including the selected toolchain modules and libraries. Simply exiting the cluster, re-entering and sourcing the file will make sure the correct toolchain is loaded.


## Installing on a generic Linux machine
This section covers how to install Gales third party libraries on a generic Linux system (typically a cluster). In this case, the relevant script is `tpl_generic.sh`. This script does not assume anything about the system, except the availability of the C++14 compiler and the MPI distribution. All the remaining libraries will be built from source (even BLAS and LAPACK, thus they will probably not  not be the optimal ones for your system).

In order to run the script. type the following command in your shell:
```bash
tpl_generic.sh
```


## Reinstalling a dependency
This section only applies to libraries that are built from source using the provided installation scripts. The list of libraries that are compiled from source with a specific `tpl_xxx.sh` script can be seen by running the `tpl_xxx.sh -h`, and checking the available targets for the script.

In case of one or more of the installed libraries becoming corrupted (for whatever reason), or if one were to change the compilation flags for one or more of the libraries, it is possible to forcibly rebuild and reinstall those libraries from their sources.

In order to force the rebuild of a specific target, it is sufficent to provide the name of target in command line, together with the `-f` flag, which forces the rebuild. As an example, if one were to reinstall `gmsh`, the command would be the following:

```bash
module purge # only on g100/leonardo
./path/to/tpl_xxx.sh -f gmsh
```

When rebuilding, the script uses the sources already extracted from the tar archives (if available), which are stored in the `src` folder. If also these sources were to become corrupted, it is possible to re-extract them from their compressed archives and rebuild the targets from the newly extracted sources. In order to do this, it suffices to invoke the installation script and pass it the `-x` flag together with the name of the corrupted target. 

Once again, if we were to re-extract and rebuild `gmsh`, the required command would be:
```bash
module purge # only on g100/leonardo
./path/to/tpl_xxx.sh -x gmsh
```
