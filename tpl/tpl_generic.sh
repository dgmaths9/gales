#!/bin/bash

# exit script on error
set -e

##### PARSE ARGUMENTS  ###############################################

# prints usage message
print_usage () {
    echo "Usage: $(basename $0) [OPTION...] [TARGET]..."
}

# prints usage message with option descriptions
print_help () {
cat <<- EOF
This script extracts and builds all the dependencies required to compile and run Gales. 

Available options:
  -f, --force-build        force build of specified target(s)
  -x, --force-extraction   force extraction and build of specified
                           target tar(s)
  -j, --parallel=          build parallelism level
  -h, --help               shows this message
  --with-extras            includes git in targets

Packages are only built if their installation folder is not found automatically by the
script, or if a rebuild is forced with -f.
If one or more specific packages need to be (re)built (without touching the others), 
they can be passed as targets to the script.

Available targets:
  cmake, boost, freetype, git, gmsh, lapack, metis, occt, trilinos

EOF
}


#---- PARSE COMMAND LINE  -------------------------------------------# 

# empty targets dictionary
declare -A TARGETS

# parse command line
while [[ "$#" -gt 0 ]]; do
    case "$1" in
    cmake|boost|freetype|git|gmsh|lapack|metis|occt|trilinos)
        # append target list of targets
        TARGETS["$1"]=true
        shift
        ;;
    --with-extras)
        # incude extra targets
        TARGETS[git]=true
        shift
        ;;
    -f|--force-build)
        FORCE_BUILD=true
        shift
        ;;
    -x|--force-extract)
        # force tar (re) extraction and rebuild
        FORCE_EXTRACT=true
        shift
        ;;
    -j)
        # level of parallelism for the build process
        NPARALLEL=$2
        shift 2
        ;;
    --parallel=*)
        # same as -j, different syntax
        NPARALLEL=$(echo $1 | cut -d '=' -f 2)
        shift
        ;;
    -h|--help)
        # shows help message 
        print_usage
        print_help
        exit 0
        ;;
    *)
        echo "Invalid target name: ${target}"
        print_usage
        exit 1
        ;;
    esac
done

# handle default case with no targets (equal to all)
if [[ "${#TARGETS[@]}" -eq 0 ]]; then
    TARGETS[cmake]=true
    TARGETS[freetype]=true
    TARGETS[occt]=true
    TARGETS[gmsh]=true
    TARGETS[lapack]=true
    TARGETS[metis]=true
    TARGETS[boost]=true
    TARGETS[trilinos]=true
fi

# set default parallelism level to 1
if [[ "${NPARALLEL}" = "" ]]; then
    NPARALLEL=1
fi



###### SCRIPT VARIABLES CONFIGURATION ################################

# cd in script directory (tpl directory)
cd $(dirname $0)

# global directories
TPL_ROOT_DIR="$(pwd)"
TPL_TAR_DIR="${TPL_ROOT_DIR}/tars"
TPL_SRC_DIR="${TPL_ROOT_DIR}/src"
TPL_LIB_DIR="${TPL_ROOT_DIR}/tpl"
LOGFILE="${TPL_ROOT_DIR}/log.txt"

# check existence of src directory
if [[ ! -d "${TPL_SRC_DIR}" ]]; then
    mkdir "${TPL_SRC_DIR}"
fi

# check existence of tpl directory
if [[ ! -d "${TPL_LIB_DIR}" ]]; then
    mkdir "${TPL_LIB_DIR}"
fi

# delete and recreate logfile if already existing
rm -f "${LOGFILE}" && touch "${LOGFILE}"


#---- TARGET DIRECTORIES --------------------------------------------#

CMAKE_TARBALL="${TPL_TAR_DIR}/cmake-3.26.2.tar.gz"
CMAKE_EXTRACT_DIR="${TPL_SRC_DIR}/cmake-3.26.2"
CMAKE_INSTALL_DIR="${TPL_LIB_DIR}/cmake-3.26.2"

GIT_TARBALL="${TPL_TAR_DIR}/git-2.25.0.tar.gz"
GIT_EXTRACT_DIR="${TPL_SRC_DIR}/git-2.25.0"
GIT_INSTALL_DIR="${TPL_LIB_DIR}/git-2.25.0"

FREETYPE_TARBALL="${TPL_TAR_DIR}/freetype-2.13.0.tar.gz"
FREETYPE_EXTRACT_DIR="${TPL_SRC_DIR}/freetype-2.13.0"
FREETYPE_INSTALL_DIR="${TPL_LIB_DIR}/freetype-2.13.0"

OCCT_TARBALL="${TPL_TAR_DIR}/OCCT-7_6_3.tar.gz"
OCCT_EXTRACT_DIR="${TPL_SRC_DIR}/OCCT-7_6_3"
OCCT_INSTALL_DIR="${TPL_LIB_DIR}/occt-7.6.3"

GMSH_TARBALL="${TPL_TAR_DIR}/gmsh-4.11.1-source.tgz"
GMSH_EXTRACT_DIR="${TPL_SRC_DIR}/gmsh-4.11.1-source"
GMSH_INSTALL_DIR="${TPL_LIB_DIR}/gmsh-4.11.1"

LAPACK_TARBALL="${TPL_TAR_DIR}/lapack-3.11.tar.gz"
LAPACK_EXTRACT_DIR="${TPL_SRC_DIR}/lapack-3.11"
LAPACK_INSTALL_DIR="${TPL_LIB_DIR}/lapack-3.11"

METIS_TARBALL="${TPL_TAR_DIR}/metis-5.1.0.tar.gz"
METIS_EXTRACT_DIR="${TPL_SRC_DIR}/metis-5.1.0"
METIS_INSTALL_DIR="${TPL_LIB_DIR}/metis-5.1.0"

BOOST_TARBALL="${TPL_TAR_DIR}/boost_1_81_0.tar.gz"
BOOST_EXTRACT_DIR="${TPL_SRC_DIR}/boost_1_81_0"
BOOST_INSTALL_DIR="${TPL_LIB_DIR}/boost-1.81.0"

TRILINOS_TARBALL="${TPL_TAR_DIR}/Trilinos-14.tar.gz"
TRILINOS_EXTRACT_DIR="${TPL_SRC_DIR}/Trilinos-trilinos-release-14-0-0"
TRILINOS_INSTALL_DIR="${TPL_LIB_DIR}/trilinos-14.0.0"



##### SET DEPENDENCIES FOR TARGET BUILDS #############################

# function that sets cmake as target if not already targeted or built
require_cmake () {
    if [[ ! "${TARGETS[cmake]}" = true ]] && \
       [[ ! -d "${CMAKE_INSTALL_DIR}" ]]; then
        TARGETS[cmake]=true
        DEP_cmake=1
    fi
}

# function that sets freetype as target if not already targeted or built
require_freetype () {
    if [[ ! "${TARGETS[freetype]}" = true ]] && \
       [[ ! -d "${FREETYPE_INSTALL_DIR}" ]]; then
        TARGETS[freetype]=true
        DEP_freet=1
    fi
}

# function that sets occt as target if not already targeted or built
require_occt () {
    if [[ ! "${TARGETS[occt]}" = true ]] && \
       [[ ! -d "${OCCT_INSTALL_DIR}" ]]; then
        TARGETS[occt]=true
        DEP_occt=1
    fi
}

# function that sets lapack as target if not already targeted or built
require_lapack () {
    if [[ ! "${TARGETS[lapack]}" = true ]] && \
       [[ ! -d "${LAPACK_INSTALL_DIR}" ]]; then
        TARGETS[lapack]=true
        DEP_lapack=1
    fi
}

# function that sets boost as target if not already targeted or build
require_boost () {
    if [[ ! "${TARGETS[boost]}" = true ]] && \
       [[ ! -d "${BOOST_INSTALL_DIR}" ]]; then
        TARGETS[boost]=true
        DEP_boost=1
    fi
}


#---- CHECK DEPENDENCIES AND PRINT CONFIG ---------------------------#

# print initial target configuration
echo -e "\n########## GALES THIRD PARTY LIBRARIES INSTALLER ##########\n" | \
    tee -a "${LOGFILE}"
echo -e "The following targets were requested:\n  " | \
    tee -a "${LOGFILE}"
for key in "${!TARGETS[@]}"; do
    printf ' %s,' $key | tee -a "${LOGFILE}"
done
echo  -e "\n" | tee -a "${LOGFILE}"

# check lapack dependencies
if [[ "$TARGETS[lapack]}" = true ]]; then
    require_cmake
fi

# check occt dependencies
if [[ "${TARGETS[occt]}" = true ]]; then
    require_cmake
    require_freetype
fi

# check gmsh dependencies
if [[ "${TARGETS[gmsh]}" = true ]]; then
    require_cmake
    require_freetype
    require_occt
fi
    
# check trilinos dependencies
if [[ "${TARGETS[trilinos]}" = true ]]; then
    require_cmake
    require_lapack
    require_boost
fi

# if dependencies were added, print new target configuration
NDEPS=$((DEP_cmake + DEP_freet + DEP_occt + DEP_lapack + DEP_boost))
if [[ ! "$NDEPS" = 0 ]]; then
    echo -e "Missing target dependencies, ${NDEPS} target(s) were added" | \
        tee -a "${LOGFILE}"
    echo -e "The following targets will be processed:\n" | \
        tee -a "${LOGFILE}"
    for key in "${!TARGETS[@]}"; do
        printf ' %s,' $key | tee -a "${LOGFILE}"
    done
    echo -e "\n" | tee -a "${LOGFILE}"
fi 



##### EXTRACT AND BUILD PACKAGES #####################################

# this function checks if extraction is required, then
# removes previous extraction and extracts tar archive 
extract_tar_checked () {
    echo "[TPL_INSTALL_LOG] extracting $(basename $1)..." | \
        tee -a "${LOGFILE}"
    # check if extraction is required
    if [[ ! -d "${2}" ]] || \
       [[ "${FORCE_EXTRACT}" = true ]]
    then
        # extract (or re-extract)
        rm -rf "${2}"                       # remove extraction dir if present
        tar -C "${TPL_SRC_DIR}" -xf "${1}"  # extract package
    else 
        echo "[TPL_INSTALL_LOG] $(basename $1) extraction found" | \
            tee -a "${LOGFILE}"
    fi
}


# first log message
echo "[TPL_INSTALL_LOG] start tpl installation"


#---- CMAKE ---------------------------------------------------------#
if [[ "${TARGETS[cmake]}" = true ]]; then
    
    extract_tar_checked "${CMAKE_TARBALL}" "${CMAKE_EXTRACT_DIR}"

    echo "[TPL_INSTALL_LOG] building cmake..." | \
        tee -a "${LOGFILE}" 
    # check if build is required
    if [[ ! -d "${CMAKE_INSTALL_DIR}" ]] || \
       [[ "${FORCE_BUILD}" = true ]]
    then
        # build (or rebuild)
        cd "${CMAKE_EXTRACT_DIR}"
        rm -rf "${CMAKE_INSTALL_DIR}"  # clean previous install
        make clean || true             # clean previous build

        ./bootstrap \
            --prefix="${CMAKE_INSTALL_DIR}" | \
            tee -a "${LOGFILE}" 

        make install -j "${NPARALLEL}" | \
            tee -a "${LOGFILE}"

        echo -e "\n\n" | tee -a "${LOGFILE}"
    else
        echo "[TPL_INSTALL_LOG] cmake installation found" | \
            tee -a "${LOGFILE}"
    fi
    
fi # end cmake block

# use the cmake we just built for the rest of the script
PATH="${CMAKE_INSTALL_DIR}/bin:$PATH"


#---- GIT -----------------------------------------------------
if [[ "${TARGETS[git]}" = true ]]; then

    extract_tar_checked "${GIT_TARBALL}" "${GIT_EXTRACT_DIR}"

    echo "[TPL_INSTALL_LOG] building git..." | \
        tee -a "${LOGFILE}"
    # check if build is required
    if [[ ! -d  "${GIT_INSTALL_DIR}" ]] || \
       [[ "${FORCE_BUILD}" = true ]]
    then
        # build (or rebuild)
        cd "${GIT_EXTRACT_DIR}"
        rm -rf "${GIT_INSTALL_DIR}"  # clean previous install
        make -i clean                # clean previous build

        ./configure \
            --prefix="${GIT_INSTALL_DIR}" | \
            tee -a "${LOGFILE}"

        make install -j "${NPARALLEL}" | \
            tee -a "${LOGFILE}"

        echo -e "\n\n" | tee -a "${LOGFILE}"
    else
        echo "[TPL_INSTALL_LOG] git installation found" | \
            tee -a "${LOGFILE}"
    fi

fi # end git block


#---- LAPACK --------------------------------------------------------#
if [[ "${TARGETS[lapack]}" = true ]]; then

    extract_tar_checked "${LAPACK_TARBALL}" "${LAPACK_EXTRACT_DIR}"

    echo "[TPL_INSTALL_LOG] building lapack..." | \
        tee -a "${LOGFILE}"
    # check if build is required
    if [[ ! -d "${LAPACK_INSTALL_DIR}" ]] || \
       [[ "${FORCE_BUILD}" = true ]]
    then
        # build (or rebuild)
        cd "${LAPACK_EXTRACT_DIR}"
        rm -rf "${METIS_INSTALL_DIR}"  # clean previous install
        rm -rf build                    # clean previous build
        mkdir -p build && cd build      # create build dir

        cmake \
            -DCMAKE_INSTALL_PREFIX="${LAPACK_INSTALL_DIR}" \
            -DCMAKE_BUILD_TYPE=RELEASE \
            -DBUILD_SHARED_LIBS=ON \
            .. | \
            tee -a "${LOGFILE}"

        cmake --build . -j "${NPARALLEL}" --target install | \
            tee -a "${LOGFILE}"

        echo -e "\n\n" | tee -a "${LOGFILE}"
    else
        echo "[TPL_INSTALL_LOG] lapack installation found" | \
            tee -a "${LOGFILE}"
    fi

fi # end lapack block


#---- METIS ---------------------------------------------------------#
if [[ "${TARGETS[metis]}" = true ]]; then

    extract_tar_checked "${METIS_TARBALL}" "${METIS_EXTRACT_DIR}"

    echo "[TPL_INSTALL_LOG] building metis..." | \
        tee -a "${LOGFILE}"
    # check if build is required
    if [[ ! -d "${METIS_INSTALL_DIR}" ]] || \
       [[ "${FORCE_BUILD}" = true ]]
    then
        # build (or rebuild)
        cd "${METIS_EXTRACT_DIR}" 
        rm -rf "${METIS_INSTALL_DIR}"  # clean previous install
        make -i distclean              # clean previous build

        make config prefix="${METIS_INSTALL_DIR}" | \
            tee -a "${LOGFILE}"

        make install -j "${NPARALLEL}" | \
            tee -a "${LOGFILE}"

        echo -e "\n\n" | tee -a "${LOGFILE}"
    else
        echo "[TPL_INSTALL_LOG] metis installation found" | \
            tee -a "${LOGFILE}"
    fi

fi # end metis block


#---- FREETYPE ------------------------------------------------------#
if [[ "${TARGETS[freetype]}" = true ]]; then
    
    extract_tar_checked "${FREETYPE_TARBALL}" "${FREETYPE_EXTRACT_DIR}"

    echo "[TPL_INSTALL_LOG] building freetype..." | \
        tee -a "${LOGFILE}"
    # check if build is required
    if [[ ! -d "${FREETYPE_INSTALL_DIR}" ]] || \
       [[ "${FORCE_BUILD}" = true ]]
    then
        # build (or rebuild)
        cd "${FREETYPE_EXTRACT_DIR}"
        rm -rf "${FREETYPE_INSTALL_DIR}"  # clean previous install
        make clean || true                # clean previous build

        ./configure \
            --prefix="${FREETYPE_INSTALL_DIR}" | \
            tee -a "${LOGFILE}"

        make install -j "${NPARALLEL}" | \
            tee -a "${LOGFILE}"

        echo -e "\n\n" | tee -a "${LOGFILE}"
    else
        echo "[TPL_INSTALL_LOG] freetype installation found" | \
            tee -a "${LOGFILE}"
    fi

fi # end freetype block


#---- OCCT ----------------------------------------------------------#
if [[ "${TARGETS[occt]}" = true ]]; then

    extract_tar_checked "${OCCT_TARBALL}" "${OCCT_EXTRACT_DIR}"

    echo "[TPL_INSTALL_LOG] building occt..." | \
        tee -a "${LOGFILE}"
    # check if build is required 
    if [[ ! -d "${OCCT_INSTALL_DIR}" ]] || \
       [[ "${FORCE_BUILD}" = true ]]
    then 
        # build (or rebuild)
        cd "${OCCT_EXTRACT_DIR}"
        rm -rf "${OCCT_INSTALL_DIR}"  # clean previous install
        rm -rf build                  # clean previous build
        mkdir -p build && cd build    # create build dir

        cmake \
            -DBUILD_MODULE_ApplicationFramework=0 \
            -DBUILD_MODULE_Visualization=0 \
            -DBUILD_MODULE_Draw=0 \
            -DCMAKE_BUILD_TYPE=Release \
            -DBUILD_LIBRARY_TYPE=Shared \
            -DCMAKE_PREFIX_PATH="${FREETYPE_INSTALL_DIR}" \
            -DCMAKE_INSTALL_PREFIX="${OCCT_INSTALL_DIR}" \
            .. | \
            tee -a "${LOGFILE}"

        cmake --build . -j "${NPARALLEL}" --target install | \
            tee -a "${LOGFILE}"

        echo -e "\n\n" | tee -a "${LOGFILE}"
    else
        echo "[TPL_INSTALL_LOG] occt installation found" | \
            tee -a "${LOGFILE}"
    fi

fi # end occt block


#---- GMSH ----------------------------------------------------------#
if [[ "${TARGETS[gmsh]}" = true ]]; then

    extract_tar_checked "${GMSH_TARBALL}" "${GMSH_EXTRACT_DIR}"

    echo "[TPL_INSTALL_LOG] building gmsh..." | \
        tee -a "${LOGFILE}"
    # check if build is required
    if [[ ! -d "${GMSH_INSTALL_DIR}" ]] || \
       [[ "${FORCE_BUILD}" = true ]]
    then
        # build (or rebuild)
        cd "${GMSH_EXTRACT_DIR}" 
        rm -rf "${GMSH_INSTALL_DIR}"  # clean previous install
        rm -rf build                  # clean previous build
        mkdir -p build && cd build    # create build dir

        cmake \
            -DCMAKE_INSTALL_PREFIX="${GMSH_INSTALL_DIR}" \
            -DCMAKE_PREFIX_PATH="${OCCT_INSTALL_DIR};${FREETYPE_INSTALL_DIR}" \
            -DENABLE_BUILD_DYNAMIC=1 \
            .. | \
            tee -a "${LOGFILE}"

        cmake --build . -j "${NPARALLEL}" --target install | \
            tee -a "${LOGFILE}"

        echo -e "\n\n" | tee -a "${LOGFILE}"
    else
        echo "[TPL_INSTALL_LOG] gmsh installation found" | \
            tee -a "${LOGFILE}"
    fi

fi # end gmsh block
         

#---- BOOST ---------------------------------------------------------#
if [[ "${TARGETS[boost]}" = true ]]; then

    extract_tar_checked "${BOOST_TARBALL}" "${BOOST_EXTRACT_DIR}"

    echo "[TPL_INSTALL_LOG] building boost..." | \
        tee -a "${LOGFILE}"
    # check if build is required
    if [[ ! -d "${BOOST_INSTALL_DIR}" ]] || \
       [[ "${FORCE_BUILD}" = true ]]
    then
        # build (or rebuild)
        cd "${BOOST_EXTRACT_DIR}"
        rm -rf "${BOOST_INSTALL_DIR}"  # clean previous install
        if [[ -f ./b2 ]]; then
            ./b2 --clean-all           # clean previous build
        fi

        ./bootstrap.sh --prefix="${BOOST_INSTALL_DIR}" | \
            tee -a "${LOGFILE}"

        ./b2 install -j "${NPARALLEL}" | \
            tee -a "${LOGFILE}"

        echo -e "\n\n" | tee -a "${LOGFILE}"
    else
        echo "[TPL_INSTALL_LOG] boost installation found" | \
            tee -a "${LOGFILE}"
    fi

fi # end boost block


#---- TRILINOS ------------------------------------------------------#
if [[ "${TARGETS[trilinos]}" = true ]]; then

    extract_tar_checked "${TRILINOS_TARBALL}" "${TRILINOS_EXTRACT_DIR}"

    echo "[TPL_INSTALL_LOG] building trilinos..." | \
        tee -a "${LOGFILE}"
    # check if build is required
    if [[ ! -d "${TRILINOS_INSTALL_DIR}" ]] || \
       [[ "${FORCE_BUILD}" = true ]]
    then
        # build (or rebuild)
        cd "${TRILINOS_EXTRACT_DIR}"
        rm -rf "${TRILINOS_INSTALL_DIR}"  # clean previous install
        rm -rf build                      # clean previous build
        mkdir -p build && cd build        # create build directory
       
        cmake \
            -DTrilinos_ENABLE_EpetraExt=ON \
            -DTrilinos_ENABLE_Epetra=ON \
            -DTrilinos_ENABLE_Belos=ON \
            -DTrilinos_ENABLE_Teuchos=ON \
            -DTrilinos_ENABLE_Ifpack=ON \
            -DTrilinos_ENABLE_Kokkos=OFF \
            -DTrilinos_ENABLE_ALL_OPTIONAL_PACKAGES=ON \
            -DTrilinos_ENABLE_TESTS=ON \
            -DTrilinos_ENABLE_DEBUG_SYMBOLS=ON \
            -DTPL_ENABLE_MPI=ON \
            -DBLAS_LIBRARY_DIRS="${LAPACK_INSTALL_DIR}/lib64" \
            -DLAPACK_LIBRARY_DIRS="${LAPACK_INSTALL_DIR}/lib64" \
            -DTPL_ENABLE_Boost=ON \
            -DBoost_INCLUDE_DIRS="${BOOST_INSTALL_DIR}/include" \
            -DCMAKE_BUILD_TYPE=RELEASE \
            -DCMAKE_CXX_STANDARD=17 \
            -DBUILD_SHARED_LIBS==ON \
            -DCMAKE_INSTALL_PREFIX="${TRILINOS_INSTALL_DIR}" \
            .. | \
            tee -a "${LOGFILE}"

        cmake --build . -j "${NPARALLEL}" --target install | \
            tee -a "${LOGFILE}"

        echo -e "\n\n" | tee -a "${LOGFILE}"
    else
        echo "[TPL_INSTALL_LOG] trilinos installation found" | \
            tee -a "${LOGFILE}"
    fi
   
fi # end trilinos block



##### GENERATE ENVIRONMENT FILE #####################################


# set envfile name
ENVFILE="${TPL_ROOT_DIR}/setenv_gales"
MISSING=()

# remove old env file
rm -rf "${ENVFILE}"

# add built cmake to path
if [[ -d "${CMAKE_INSTALL_DIR}" ]]; then
cat <<- EOF >> "${ENVFILE}"
# add cmake directories to path
export PATH=${CMAKE_INSTALL_DIR}/bin:\$PATH
EOF
echo -e "\n" >> "${ENVFILE}"
else 
MISSING+=('cmake')
fi

# if required, add built git to path
if [[ "${TARGETS[git]}" = true ]]; then
if [[ -d "${GIT_INSTALL_DIR}" ]]; then
cat <<- EOF >> "${ENVFILE}"
# add git directories to path
export PATH=${GIT_INSTALL_DIR}/bin:\$PATH
EOF
echo -e "\n" >> "${ENVFILE}"
else
MISSING+=('git')
fi
fi

# add built freetype libs to libpath
if [[ -d "${FREETYPE_INSTALL_DIR}" ]]; then
cat <<- EOF >> "${ENVFILE}"
# adding freetype directories to paths
export LIBRARY_PATH=${FREETYPE_INSTALL_DIR}/lib:\$LIBRARY_PATH
export LD_LIBRARY_PATH=${FREETYPE_INSTALL_DIR}/lib:\$LD_LIBRARY_PATH}
export FREETYPE_ROOT=${FREETYPE_INSTALL_DIR}
export FREETYPE_INCLUDE=${FREETYPE_INSTALL_DIR}/include
export FREETYPE_INC=${FREETYPE_INSTALL_DIR}/include
export FREETYPE_LIB=${FREETYPE_INSTALL_DIR}/lib
EOF
echo -e "\n" >> "${ENVFILE}"
else 
MISSING+=('freetype')
fi

# add built occt libs to libpath
if [[ -d "${GMSH_INSTALL_DIR}" ]]; then
cat <<- EOF >> "${ENVFILE}"
# add opencascade directories to paths
export LIBRARY_PATH=${OCCT_INSTALL_DIR}/lib:\$LIBRARY_PATH
export LD_LIBRARY_PATH=${OCCT_INSTALL_DIR}/lib:\$LD_LIBRARY_PATH
export OCCT_ROOT=${OCCT_INSTALL_DIR}
export OCCT_INCLUDE=${OCCT_INSTALL_DIR}/include
export OCCT_INC=${OCCT_INSTALL_DIR}/include 
export OCCT_LIB=${OCCT_INSTALL_DIR}/lib
EOF
echo -e "\n" >> "${ENVFILE}"
else 
MISSING+=('occt')
fi

# add built gmsh to path and libpath
if [[ -d "${GMSH_INSTALL_DIR}" ]]; then 
cat <<- EOF >> "${ENVFILE}"
# add gmsh directories to paths
export PATH=${GMSH_INSTALL_DIR}/bin:\$PATH
export LIBRARY_PATH=${GMSH_INSTALL_DIR}/lib64:\$LIBRARY_PATH
export LD_LIBRARY_PATH=${GMSH_INSTALL_DIR}/lib64:\$LD_LIBRARY_PATH
export PYTHONPATH=${GMSH_INSTALL_DIR}/lib64:\$PYTHONPATH
export GMSH_ROOT=${GMSH_INSTALL_DIR}
export GMSH_INCLUDE=${GMSH_INSTALL_DIR}/include
export GMSH_INC=${GMSH_INSTALL_DIR}/include
export GMSH_LIB=${GMSH_INSTALL_DIR}/lib64
EOF
echo -e "\n" >> "${ENVFILE}"
else 
MISSING+=('gmsh')
fi

# add built lapack directories to path
if [[ -d "${LAPACK_INSTALL_DIR}" ]]; then
cat <<- EOF >> "${ENVFILE}"
# add lapack directories to paths
export LIBRARY_PATH=${LAPACK_INSTALL_DIR}/lib64:\$LIBRARY_PATH
export LD_LIBRARY_PATH=${LAPACK_INSTALL_DIR}/lib64:\$LD_LIBRARY_PATH
export LAPACK_ROOT=${LAPACK_INSTALL_DIR}
export LAPACK_LIB=${LAPACK_INSTALL_DIR}/lib64
export Lapack_DIR=${LAPACK_INSTALL_DIR}/lib64/cmake
EOF
echo -e "\n" >> "${ENVFILE}"
else
MISSING+=('lapack')
fi


# add built metis directories to path
if [[ -d "${METIS_INSTALL_DIR}" ]]; then
cat <<- EOF >> "${ENVFILE}"
# add metis directories to paths
export PATH=${METIS_INSTALL_DIR}/bin:\$PATH
EOF
echo -e "\n" >> "${ENVFILE}"
else
MISSING+=('metis')
fi

# add built boost directories to path
if [[ -d "${BOOST_INSTALL_DIR}" ]]; then
cat <<- EOF >> "${ENVFILE}"
# add boost directories to paths
export LIBRARY_PATH=${BOOST_INSTALL_DIR}/lib64:\$LIBRARY_PATH
export LD_LIBRARY_PATH=${BOOST_INSTALL_DIR}/lib64:\$LD_LIBRARY_PATH
export BOOST_ROOT=${BOOST_INSTALL_DIR}
export BOOST_INCLUDE=${BOOST_INSTALL_DIR}/include
export BOOST_INC=${BOOST_INSTALL_DIR}/include
export BOOST_LIB=${BOOST_INSTALL_DIR}/lib
export Boost_DIR=${BOOST_INSTALL_DIR}/lib/cmake
EOF
echo -e "\n" >> "${ENVFILE}"
else
MISSING+=('boost')
fi

# add built trilinos directories to path
if [[ -d "${TRILINOS_INSTALL_DIR}" ]]; then
cat <<- EOF >> "${ENVFILE}"
# adding trilinos directories to paths
export LIBRARY_PATH=${TRILINOS_INSTALL_DIR}/lib:\$LIBRARY_PATH
export LD_LIBRARY_PATH=${TRILINOS_INSTALL_DIR}/lib:\$LD_LIBRARY_PATH
export TRILINOS_ROOT=${TRILINOS_INSTALL_DIR}
export TRILINOS_INCLUDE=${TRILINOS_INSTALL_DIR}/include
export TRILINOS_INC=${TRILINOS_INSTALL_DIR}/include
export TRILINOS_LIB=${TRILINOS_INSTALL_DIR}/lib
export Trilinos_DIR=${TRILINOS_INSTALL_DIR}/lib/cmake
EOF
echo -e "\n" >> "${ENVFILE}"
else 
MISSING+=('trilinos')
fi 

# export gales directories
cat <<- EOF >> "${ENVFILE}"
# adding gales utilities to path
export TPL_PATH=${TPL_ROOT_DIR} 
export PATH=${TPL_ROOT_DIR}/gales_mesh_preprocessing:\$PATH
export PATH=${TPL_ROOT_DIR}/gales_results_postprocessing:\$PATH
export PATH=${TPL_ROOT_DIR}/Mogi_model:\$PATH
export PATH=${TPL_ROOT_DIR}/solwcad:\$PATH 
export PYTHONPATH=${TPL_ROOT_DIR}/gales_results_postprocessing:\$PYTHONPATH
export PYTHONPATH=${TPL_ROOT_DIR}/Mogi_model:\$PYTHONPATH
EOF

# echo final message
echo -e "\nEnvironment file generated at: ${ENVFILE}\n"
if [[ ! "${#MISSING[@]}" -eq 0 ]]; then
    echo -e "WARNING: incomplete environment file generated, missing targets:\n"
    for target in "${MISSING[@]}"; do
        echo -n " $target,"
    done
    echo -e "\n\nBuild the missing targets before sourcing the environment file\n"
fi
