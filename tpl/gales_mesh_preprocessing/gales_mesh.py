#!/usr/bin/python3

import os
from gmsh_to_gales import *
import sys


parts = 1
mesh_name = 'mesh.msh'

if(len(sys.argv)==2):  
   parts = int(sys.argv[1])
elif(len(sys.argv)==3):  
   parts = int(sys.argv[1])
   mesh_name = sys.argv[2]

mesh = gmsh_to_gales(mesh_name, parts,  mesh_name[:-4]+'_'+str(parts)+'core.txt') 


print ("------------------------- FINISHED!!!!!! --------------------------")        
print('');print(''); print('');   

