#!/usr/bin/python3

import os
from gmsh_to_gales import *
import sys


'''
    This is to be used for fluid and heat equation coupling
'''

parts = 1

if(len(sys.argv)==2):  
   parts = int(sys.argv[1])


f_mesh = gmsh_to_gales('f.msh', parts, 'f_mesh_'+str(parts)+'core.txt') 
h_mesh = gmsh_to_gales('h.msh', parts, 'h_mesh_'+str(parts)+'core.txt') 

matching_interface_nodes(f_mesh, h_mesh, 'fhi_nd.txt')


print ("------------------------- FINISHED!!!!!! --------------------------")        
print('');print(''); print('');   



