#!/usr/bin/python3

import os
from gmsh_to_gales import *
import sys


parts = 1

if(len(sys.argv)==2):  
   parts = int(sys.argv[1])


f_mesh = gmsh_to_gales('f.msh', parts, 'f_mesh_'+str(parts)+'core.txt') 
s_mesh = gmsh_to_gales('s.msh', parts, 's_mesh_'+str(parts)+'core.txt') 

matching_interface_nodes(f_mesh, s_mesh, 'fsi_nd.txt')


print ("------------------------- FINISHED!!!!!! --------------------------")        
print('');print(''); print('');   



