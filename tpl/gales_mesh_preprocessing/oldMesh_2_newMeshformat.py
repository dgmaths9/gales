#!/usr/bin/python3


import os



mesh_name = 'mesh_192core.m'



    
''' 
   This script is to convert meshes written in the old format to the new format.
   This deals with only 2D quadrangle elements but can be easily generalised to other types also.
   
   mesh_192core.m    ---->  mesh_192core.txt 
                            (this includes sides info also;  boundary flag of each side is fixed to 5 correspomding to boundary nd_flag = 5 ) 
'''  






f = open(mesh_name, "r")
# ----------------------------------read header----------------------------------------------------------
split_result = ((f.readline()).strip()).split()     #  MESH! 2D

split_result = ((f.readline()).strip()).split()   # nodes 9331
n_nds = int(split_result[1])

split_result = ((f.readline()).strip()).split()  # elements 9000
n_els = int(split_result[1])

x_range = f.readline()     #  X -80.0 80.0

y_range = f.readline()     #  Y 0.0 12.0
# ---------------------------------------------------------------------------------------------------------


# ----------------------------------read nodes----------------------------------------------------------
nd_flag, x, y = [],[],[]
for i in range (n_nds):
       split_result = ((f.readline()).strip()).split()      #  Node 0 80.0 10.01622179 6
       x.append(float(split_result[2]))
       y.append(float(split_result[3]))
       nd_flag.append(int(split_result[4]))
# ---------------------------------------------------------------------------------------------------------



# ----------------------------------read els ----------------------------------------------------------
el_pid, el, el_flag = [], [], []
for i in range (n_els):
   split_result = ((f.readline()).strip()).split()       # Element  0  159  quad  4  1  36573  37628  37632  37631  0
   el_pid.append(int(split_result[2]))
   el.append([int(split_result[6]), int(split_result[7]), int(split_result[8]), int(split_result[9])]) 
   el_flag.append(int(split_result[10]))
# ---------------------------------------------------------------------------------------------------------
f.close()
   

   
   
   
#--------------------------------compute sides info from els and nd_flag info --------------------------
side, side_pid, side_flag = [], [], []
for i in range (n_els):                                # 4 sides for each element  
  if(el_flag[i]==1):
    if(nd_flag[el[i][0]]>0 and nd_flag[el[i][1]]>0):     # 1 side 
      side.append([el[i][0], el[i][1]])
      side_pid.append(el_pid[i])
      side_flag.append(5)    
    if(nd_flag[el[i][1]]>0 and nd_flag[el[i][2]]>0):     # 2 side 
      side.append([el[i][1], el[i][2]])
      side_pid.append(el_pid[i])
      side_flag.append(5)
    if(nd_flag[el[i][2]]>0 and nd_flag[el[i][3]]>0):     # 3 side 
      side.append([el[i][2], el[i][3]])
      side_pid.append(el_pid[i])
      side_flag.append(5)
    if(nd_flag[el[i][3]]>0 and nd_flag[el[i][0]]>0):     # 4 side 
      side.append([el[i][3], el[i][0]])                  
      side_pid.append(el_pid[i])
      side_flag.append(5)
# ---------------------------------------------------------------------------------------------------------



  
#--------------------------------write mesh in new format --------------------------
f = open(mesh_name[:-1]+'txt', "w")                 
f.write("MESH! 2D" + os.linesep)
f.write("nodes " + str(n_nds) + os.linesep)
f.write("elements " + str(n_els) + os.linesep)
f.write("sides " + str(len(side)) + os.linesep)
f.write(x_range)
f.write(y_range)
for i in range (n_nds):  
  f.write("Node " + str(i) + " "+ str(x[i]) + " "+ str(y[i]) + " "+ str(nd_flag[i]) + os.linesep)
for i in range (n_els):
  f.write("Element " + str(i) + " "+ str(el_pid[i]) + " 4 " + str(el[i][0]) + " " + str(el[i][1]) + " " + str(el[i][2]) + " " + str(el[i][3]) + " " + str(el_flag[i]) + os.linesep)
for i in range(len(side)):
  f.write("Side " + str(i) + " "+ str(side_pid[i]) + " 2 " + str(side[i][0]) + " " + str(side[i][1]) + " " + str(side_flag[i]) + os.linesep)
f.close()     
# ---------------------------------------------------------------------------------------------------------
        
         
         
         
         
         
  
         
         
         
         
         
         
         
         
         
             
