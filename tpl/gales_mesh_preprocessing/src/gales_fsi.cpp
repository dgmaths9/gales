#include<iostream>
#include<fstream>
#include<sstream>
#include<vector>

#include "gmsh_to_gales.hpp"


  
int main(int argc, char** argv)
{
   int parts = 1;
   
   if(argc==2)  
      parts = std::stoi(argv[1]);   
            
   gmsh_to_gales f_mesh("f.msh", parts, std::string("f_mesh_") + std::to_string(parts) + "core.txt");
   gmsh_to_gales s_mesh("s.msh", parts, std::string("s_mesh_") + std::to_string(parts) + "core.txt");


   matching_interface_nodes(f_mesh, s_mesh, "fsi_nd.txt");
   
   std::cerr<<"------------------------- FINISHED!!!!!! --------------------------\n\n\n";
   
   return 0;
}  
  

