#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <chrono>
#include <map>
#include <set>
#include <algorithm>
#include <cmath>
#include <iomanip>



    
  using namespace std;    



  class gmsh_to_gales
  {

    public:   
               
    gmsh_to_gales(const string& read_mesh_name, int parts, const string& write_mesh_name)
    {
         ifstream infile(read_mesh_name);
         if(!infile.is_open())
         {
            cerr<<"ERROR: "<<__FILE__<<":"<<__LINE__<<":"<<" unable to open and read mesh file  \n";
            exit(0); 
         }
         vector<string> result;
         
         
         
         
         
                  
         //----------------------------------------------------------- Reading GMSH(.msh) file ------------------------------------
         read_one_line(infile, result);          //$MeshFormat
         read_one_line(infile, result);          //2.2 0 8    or   4.1 0 8

         if(result[0] == "2.2")
         {                     
            mesh_version_ = 2;   
            cout<< "\n------------------gmsh mesh version is 2.2------------------"<< endl<<endl;            
         }
         else if(result[0] == "4.1")
         {
            mesh_version_ = 4;   
            cout<< "\n------------------gmsh mesh version is 4.1------------------"<< endl<<endl;            
         }
         read_one_line(infile, result);          //$EndMeshFormat
         
         
         
         
         
         
         
         
         if(mesh_version_ == 4)
         {
            read_one_line(infile, result);          //$Entities
            read_one_line(infile, result);          
            int numPoints(stoi(result[0])), numCurves(stoi(result[1])), numSurfaces(stoi(result[2])), numVolumes(stoi(result[3])); 
            for(int i=0; i<numPoints; i++)   
            {
               read_one_line(infile, result);
               int entityTag = stoi(result[0]);
               int numPhysicalTags = stoi(result[4]);
               if(numPhysicalTags == 1)
               {
                  int physicalTag = stoi(result[5]);            
                  if(physicalTag == 0)
                  {
                      cerr<<"ERROR: "<<__FILE__<<":"<<__LINE__<<"\n";
                      cerr<<"-----------------Error: gmsh did not write the right physical tag for"<<i<<"th point in "<< read_mesh_name <<" file ----------\n"
                          <<"-----------------To do: check and edit manualy for the the right physical tag for the point in the Entities block ------------------\n"
                          <<"-----------------Generate mesh file again ------------------\n\n";               
                      exit(0);         
                  }
                  pt_map_[entityTag] = physicalTag;               
               }
               else if(numPhysicalTags > 1)
               {
                   cerr<<"-----------------Error: numPhysicalTags for the "<<i<<" th point is greater than 1; it should be 1 ----------"<<endl<<endl;
                   exit(0);    
               }  
            }
            for(int i=0; i<numCurves; i++)   
            {
               read_one_line(infile, result);
               int entityTag = stoi(result[0]);
               int numPhysicalTags = stoi(result[7]);
               if(numPhysicalTags == 1)
               {
                   int physicalTag = stoi(result[8]);            
                   line_map_[entityTag] = physicalTag;               
               }
               else if(numPhysicalTags > 1)
               {
                   cerr<<"-----------------Error: numPhysicalTags for the "<<i<<" th point is greater than 1; it should be 1 ----------"<<endl<<endl;
                   exit(0);    
               }  
            }   
            for(int i=0; i<numSurfaces; i++)   
            {
               read_one_line(infile, result);
               int entityTag = stoi(result[0]);
               int numPhysicalTags = stoi(result[7]);
               if(numPhysicalTags == 1)
               {
                   int physicalTag = stoi(result[8]);            
                   surf_map_[entityTag] = physicalTag;               
               }
               else if(numPhysicalTags > 1)
               {
                   cerr<<"-----------------Error: numPhysicalTags for the "<<i<<" th point is greater than 1; it should be 1 ----------"<<endl<<endl;
                   exit(0);    
               }  
            }   
            for(int i=0; i<numVolumes; i++)   
            {
               read_one_line(infile, result);
               int entityTag = stoi(result[0]);
               int numPhysicalTags = stoi(result[7]);
               if(numPhysicalTags == 1)
               {
                   int physicalTag = stoi(result[8]);            
                   vol_map_[entityTag] = physicalTag;               
               }
               else if(numPhysicalTags > 1)
               {
                   cerr<<"-----------------Error: numPhysicalTags for the "<<i<<" th point is greater than 1; it should be 1 ----------"<<endl<<endl;
                   exit(0);    
               }  
            }   
            read_one_line(infile, result);          //$EndEntities            
         }
         














         if(mesh_version_ == 4)
         {
              auto position = infile.tellg();               // get the position of the file pointer 
              read_one_line(infile, result);          
              if(result[0] == "$PartitionedEntities")           // $PartitionedEntities
              {
                 read_one_line(infile, result);                        
                 int numPartitions = stoi(result[0]);

                 read_one_line(infile, result);                                         
                 int numGhostEntities = stoi(result[0]);

                 if(numGhostEntities > 0)
                 {
                    cerr <<"-----------Error: numGhostEntities is greater than 0; it should be zero-------------------"<<endl;
                    cerr <<"-----------We do not read ghost entities-------------------"<< endl<<endl;
                    exit(0);
                 }  
                 read_one_line(infile, result);                                         
                 int numPoints(stoi(result[0])), numCurves(stoi(result[1])), numSurfaces(stoi(result[2])), numVolumes(stoi(result[3]));


                 for(int i=0; i<numPoints; i++)
                 {
                    read_one_line(infile, result);                                         
                    int pointTag(stoi(result[0])), parentDim(stoi(result[1])), parentTag(stoi(result[2])), numPartitions(stoi(result[3]));           
                    
                    if(numPartitions == 1)      
                        pt_pid_map_[pointTag] = stoi(result[4])-1;
                    
                    vector<int> partitionTag(numPartitions);
                    for(int j=0; j<numPartitions; j++)
                        partitionTag[j] = stoi(result[4+j]);
                    
                    int numPhysicalTags = stoi(result[3+numPartitions+4]);
                    
                    if(numPhysicalTags == 1)
                    {
                       int physicalTag = stoi(result[3+numPartitions+5]);
                       if(physicalTag == 0)
                       {
                           cerr<<"ERROR: "<<__FILE__<<":"<<__LINE__<<"\n";
                           cerr<<"-----------------Error: gmsh did not write the right physical tag for"<<i<<"th point in "<< read_mesh_name <<" file ----------\n"
                               <<"-----------------To do: check and edit manualy for the the right physical tag for the point in the Entities block ------------------\n"
                               <<"-----------------Generate mesh file again ------------------\n\n";               
                           exit(0);         
                       }
                       pt_tag_map_[pointTag] = physicalTag;
                    }   
                    else if(numPhysicalTags > 1)
                    {
                        cerr << "-----------------Error: numPhysicalTags for the " << i << "th point is greater than 1 in PartitionedEntities; it should be 1 or 0 ----------"<<endl<<endl;
                        exit(0);
                    }    
                 }       

                 for(int i=0; i<numCurves; i++)
                 {
                    read_one_line(infile, result);                                         
                    int curveTag(stoi(result[0])), parentDim(stoi(result[1])), parentTag(stoi(result[2])), numPartitions(stoi(result[3]));           
                    
                    if(numPartitions == 1)      
                        line_pid_map_[curveTag] = stoi(result[4])-1;
                    
                    vector<int> partitionTag(numPartitions);
                    for(int j=0; j<numPartitions; j++)
                        partitionTag[j] = stoi(result[4+j]);
                    
                    int numPhysicalTags = stoi(result[3+numPartitions+7]);
                    
                    if(numPhysicalTags == 1)
                    {
                       int physicalTag = stoi(result[3+numPartitions+8]);
                       line_tag_map_[curveTag] = physicalTag;
                    }   
                    else if(numPhysicalTags > 1)
                    {
                        cerr << "-----------------Error: numPhysicalTags for the " << i << "th curve is greater than 1 in PartitionedEntities; it should be 1 or 0 ----------"<<endl<<endl;
                        exit(0);
                    }    
                 }       
              
                 for(int i=0; i<numSurfaces; i++)
                 {
                    read_one_line(infile, result);                                         
                    int surfaceTag(stoi(result[0])), parentDim(stoi(result[1])), parentTag(stoi(result[2])), numPartitions(stoi(result[3]));           
                    
                    if(numPartitions == 1)      
                        surf_pid_map_[surfaceTag] = stoi(result[4])-1;
                    
                    vector<int> partitionTag(numPartitions);
                    for(int j=0; j<numPartitions; j++)
                        partitionTag[j] = stoi(result[4+j]);
                    
                    int numPhysicalTags = stoi(result[3+numPartitions+7]);
                    
                    if(numPhysicalTags == 1)
                    {
                       int physicalTag = stoi(result[3+numPartitions+8]);
                       surf_tag_map_[surfaceTag] = physicalTag;
                    }   
                    else if(numPhysicalTags > 1)
                    {
                        cerr << "-----------------Error: numPhysicalTags for the " << i << "th surface is greater than 1 in PartitionedEntities; it should be 1 or 0 ----------"<<endl<<endl;
                        exit(0);
                    }    
                 }       

                 for(int i=0; i<numVolumes; i++)
                 {
                    read_one_line(infile, result);                                         
                    int volumeTag(stoi(result[0])), parentDim(stoi(result[1])), parentTag(stoi(result[2])), numPartitions(stoi(result[3]));           
                    
                    if(numPartitions == 1)      
                        vol_pid_map_[volumeTag] = stoi(result[4])-1;
                    
                    vector<int> partitionTag(numPartitions);
                    for(int j=0; j<numPartitions; j++)
                        partitionTag[j] = stoi(result[4+j]);
                    
                    int numPhysicalTags = stoi(result[3+numPartitions+7]);
                    
                    if(numPhysicalTags == 1)
                    {
                       int physicalTag = stoi(result[3+numPartitions+8]);
                       vol_tag_map_[volumeTag] = physicalTag;
                    }   
                    else if(numPhysicalTags > 1)
                    {
                        cerr << "-----------------Error: numPhysicalTags for the " << i << "th volume is greater than 1 in PartitionedEntities; it should be 1 or 0 ----------"<<endl<<endl;
                        exit(0);
                    }    
                 }
                 read_one_line(infile, result);          // $EndPartitionedEntities                       
              }        
              else
              {
                  infile.seekg(position);     //put file pointer at the position of the previous line
              }
              
              if(pt_pid_map_.size() !=0 or line_pid_map_.size() !=0 or surf_pid_map_.size() !=0 or vol_pid_map_.size() !=0)
                 gmsh_partitioned_mesh_ = true;                               
         }







         
         
         
         
         
         auto beg = chrono::steady_clock::now();
         read_one_line(infile, result);          //$Nodes
         if(mesh_version_ == 2)
         {
            read_one_line(infile, result);          
            n_nds_ = stoi(result[0]);         
            x_.resize(n_nds_);
            y_.resize(n_nds_);
            z_.resize(n_nds_);
            for(int i=0; i<n_nds_; i++)
            {
               read_one_line(infile, result);          
               x_[i] = stod(result[1]);
               y_[i] = stod(result[2]);
               z_[i] = stod(result[3]);
            }
         }   
         else if(mesh_version_ == 4)
         {
            read_one_line(infile, result);          
            n_nds_ = stoi(result[1]);
            int numEntityBlocks(stoi(result[0])), minNodeTag(stoi(result[2])), maxNodeTag(stoi(result[3]));              
            x_.resize(n_nds_);  
            y_.resize(n_nds_);  
            z_.resize(n_nds_);  
            for(int i=0; i<numEntityBlocks; i++)
            {
               read_one_line(infile, result);           
               int entityDim(stoi(result[0])), entityTag(stoi(result[1])), parametric(stoi(result[2])), numNodesInBlock(stoi(result[3]));        
               vector<int> nd_tag(numNodesInBlock);
               for(int j=0; j<numNodesInBlock; j++)
               {
                 read_one_line(infile, result);                             
                 nd_tag[j] = stoi(result[0])-1;
               }
               for(int j=0; j<numNodesInBlock; j++)
               {
                 read_one_line(infile, result);   
                 x_[nd_tag[j]] = stod(result[0]);
                 y_[nd_tag[j]] = stod(result[1]);
                 z_[nd_tag[j]] = stod(result[2]);
               }
            }
         }
         read_one_line(infile, result);          //$EndNodes
         auto end = chrono::steady_clock::now();
         auto duration = chrono::duration<double>(end - beg);
         cerr<<"------------------mesh nodes reading done in "<< duration.count() <<" milli seconds -------------\n"<<endl;  
           
           
           
                     
           
           


           
         beg = chrono::steady_clock::now();
         read_one_line(infile, result);          //$Elements         
         if(mesh_version_ == 2)
         {         
            read_one_line(infile, result);          
            int n_els_tot = stoi(result[0]);                  
            for(int i=0; i<n_els_tot; i++)
            {
               read_one_line(infile, result);
               int elm_number(stoi(result[0])), elm_type(stoi(result[1])), number_of_tags(stoi(result[2]));

               std::vector<int> tags(number_of_tags);
               for(int j=0; j<number_of_tags; j++) 
                  tags[j] = stoi(result[3+j]);
                  
               int physical_tag = tags[0];
               int geometrical_entity = tags[1];
               int pid = get_pid(tags);

               switch(elm_type)
               {
                  case 15:        //point
                  {
                     if(physical_tag==0)
                     {
                       cerr<<"ERROR: "<<__FILE__<<":"<<__LINE__<<"\n";
                       cerr<<"-----------------Error: gmsh did not write the right physical tag for"<<i<<"th element (a point) in "<< read_mesh_name <<" file ----------\n"
                                <<"-----------------To do: check and edit manualy for the the right physical tag ------------------\n"
                                <<"-----------------Generate mesh file again ------------------\n\n";
                       exit(0);         
                     }
                     pt_tag_.push_back(physical_tag);                  
                     pt_.push_back(stoi(result[2+number_of_tags+1]));
                     if(pid >= 0)
                         pt_pid_.push_back(pid);                     
                     break;
                  }
                  case 1:         //line
                  {
                     line_tag_.push_back(physical_tag);
                     vector<int> v = {stoi(result[2+number_of_tags+1]), stoi(result[2+number_of_tags+2])}; 
                     line_.push_back(v);
                     if(pid >= 0)
                         line_pid_.push_back(pid);                     
                     break;               
                  }
                  case 2:         //tri
                  {
                     surf_tag_.push_back(physical_tag);
                     vector<int> v = {stoi(result[2+number_of_tags+1]), stoi(result[2+number_of_tags+2]), stoi(result[2+number_of_tags+3])}; 
                     surf_.push_back(v);               
                     if(pid >= 0)
                         surf_pid_.push_back(pid);                     
                     break;               
                  }
                  case 3:        //quad
                  {
                     surf_tag_.push_back(physical_tag);
                     vector<int> v = {stoi(result[2+number_of_tags+1]), stoi(result[2+number_of_tags+2]), stoi(result[2+number_of_tags+3]), stoi(result[2+number_of_tags+4])}; 
                     surf_.push_back(v);                              
                     if(pid >= 0)
                         surf_pid_.push_back(pid);                     
                     break;               
                  }
                  case 4:        //tetra
                  {
                     vol_tag_.push_back(physical_tag);
                     vector<int> v = {stoi(result[2+number_of_tags+1]), stoi(result[2+number_of_tags+2]), stoi(result[2+number_of_tags+3]), stoi(result[2+number_of_tags+4])}; 
                     vol_.push_back(v);                              
                     if(pid >= 0)
                         vol_pid_.push_back(pid);                     
                     break;               
                  }
                  case 5:        //hexa
                  {
                     vol_tag_.push_back(physical_tag);
                     vector<int> v = {stoi(result[2+number_of_tags+1]), stoi(result[2+number_of_tags+2]), stoi(result[2+number_of_tags+3]), stoi(result[2+number_of_tags+4]), 
                                      stoi(result[2+number_of_tags+5]), stoi(result[2+number_of_tags+6]), stoi(result[2+number_of_tags+7]), stoi(result[2+number_of_tags+8])}; 
                     vol_.push_back(v);                                             
                     if(pid >= 0)
                         vol_pid_.push_back(pid);                     
                     break;               
                  }
                  case 6:        //prism
                  {
                     vol_tag_.push_back(physical_tag);
                     vector<int> v = {stoi(result[2+number_of_tags+1]), stoi(result[2+number_of_tags+2]), stoi(result[2+number_of_tags+3]), stoi(result[2+number_of_tags+4]), 
                                      stoi(result[2+number_of_tags+5]), stoi(result[2+number_of_tags+6])}; 
                     vol_.push_back(v);                                             
                     if(pid >= 0)
                         vol_pid_.push_back(pid);                     
                     break;               
                  }
                  default:
                  {
                     cerr<< "elm_type: "<< elm_type<<endl;
                     cerr << "-----------------Error: We do not read this type of element ----------"<<endl<<endl;
                     exit(0);                    
                  }                   
               }            
            }

            if(pt_pid_.size() !=0 or line_pid_.size() !=0 or surf_pid_.size() !=0 or vol_pid_.size() !=0)
                 gmsh_partitioned_mesh_ = true;                               
         }
         else if(mesh_version_ == 4)
         {
             read_one_line(infile, result);
             int numEntityBlocks(stoi(result[0])), n_els_tot(stoi(result[1])), minElementTag(stoi(result[2])), maxElementTag(stoi(result[3]));
             for(int i=0; i<numEntityBlocks; i++)
             {
                read_one_line(infile, result);
                int entityDim(stoi(result[0])), entityTag(stoi(result[1])), elementType(stoi(result[2])), numElementsInBlock(stoi(result[3]));              
                if(entityDim == 0)                  //point
                {
                   for(int j=0; j<numElementsInBlock; j++)
                   {
                      read_one_line(infile, result);
                      pt_.push_back(stoi(result[1]));                      
                      if(gmsh_partitioned_mesh_)
                      {                  
                         pt_tag_.push_back(pt_tag_map_.at(entityTag));
                         pt_pid_.push_back(pt_pid_map_.at(entityTag));
                      }   
                      else
                         pt_tag_.push_back(pt_map_.at(entityTag));                                               
                   }
                }
                else if(entityDim == 1)             //line
                {
                   for(int j=0; j<numElementsInBlock; j++)
                   {
                      read_one_line(infile, result);
                      vector<int> v = {stoi(result[1]), stoi(result[2])};
                      line_.push_back(v);
                      if(gmsh_partitioned_mesh_)
                      {                  
                         line_tag_.push_back(line_tag_map_.at(entityTag));
                         line_pid_.push_back(line_pid_map_.at(entityTag));
                      }   
                      else
                         line_tag_.push_back(line_map_.at(entityTag));                                               
                   }
                }
                else if(entityDim == 2 && elementType == 2)      //tri
                {
                   for(int j=0; j<numElementsInBlock; j++)
                   {
                      read_one_line(infile, result);
                      vector<int> v = {stoi(result[1]), stoi(result[2]), stoi(result[3])};
                      surf_.push_back(v);
                      if(gmsh_partitioned_mesh_)
                      {                  
                         surf_tag_.push_back(surf_tag_map_.at(entityTag));
                         surf_pid_.push_back(surf_pid_map_.at(entityTag));
                      }   
                      else
                         surf_tag_.push_back(surf_map_.at(entityTag));                                               
                   }
                }
                else if(entityDim == 2 && elementType == 3)     //quad
                {
                   for(int j=0; j<numElementsInBlock; j++)
                   {
                      read_one_line(infile, result);
                      vector<int> v = {stoi(result[1]), stoi(result[2]), stoi(result[3]), stoi(result[4])};
                      surf_.push_back(v);
                      if(gmsh_partitioned_mesh_)
                      {                  
                         surf_tag_.push_back(surf_tag_map_.at(entityTag));
                         surf_pid_.push_back(surf_pid_map_.at(entityTag));
                      }   
                      else
                         surf_tag_.push_back(surf_map_.at(entityTag));                                               
                   }
                }
                else if(entityDim == 3 && elementType == 4)      //tetra
                {
                   for(int j=0; j<numElementsInBlock; j++)
                   {
                      read_one_line(infile, result);
                      vector<int> v = {stoi(result[1]), stoi(result[2]), stoi(result[3]), stoi(result[4])};
                      vol_.push_back(v);
                      if(gmsh_partitioned_mesh_)
                      {                  
                         vol_tag_.push_back(vol_tag_map_.at(entityTag));
                         vol_pid_.push_back(vol_pid_map_.at(entityTag));
                      }   
                      else
                         vol_tag_.push_back(vol_map_.at(entityTag));                                               
                   }
                }
                else if(entityDim == 3 && elementType == 5)       //hexa
                {
                   for(int j=0; j<numElementsInBlock; j++)
                   {
                      read_one_line(infile, result);
                      vector<int> v = {stoi(result[1]), stoi(result[2]), stoi(result[3]), stoi(result[4]),
                                       stoi(result[5]), stoi(result[6]), stoi(result[7]), stoi(result[8])};
                      vol_.push_back(v);
                      if(gmsh_partitioned_mesh_)
                      {                  
                         vol_tag_.push_back(vol_tag_map_.at(entityTag));
                         vol_pid_.push_back(vol_pid_map_.at(entityTag));
                      }   
                      else
                         vol_tag_.push_back(vol_map_.at(entityTag));                                               
                   }
                }
                else if(entityDim == 3 && elementType == 6)       //prism
                {
                   for(int j=0; j<numElementsInBlock; j++)
                   {
                      read_one_line(infile, result);
                      vector<int> v = {stoi(result[1]), stoi(result[2]), stoi(result[3]), stoi(result[4]),
                                       stoi(result[5]), stoi(result[6])};
                      vol_.push_back(v);
                      if(gmsh_partitioned_mesh_)
                      {                  
                         vol_tag_.push_back(vol_tag_map_.at(entityTag));
                         vol_pid_.push_back(vol_pid_map_.at(entityTag));
                      }   
                      else
                         vol_tag_.push_back(vol_map_.at(entityTag));                                               
                   }
                }
                else
                {
                     cerr<< "elementType: "<< elementType<<endl;
                     cerr << "-----------------Error: We do not read this type of element ----------"<<endl<<endl;
                     exit(0);                    
                }  
               
                
             }
         }         
         read_one_line(infile, result);          //$EndElements
         infile.close();
         //--------------------------------------------------------------------------------------------------------------------------------
         
         


         





         
         
         
         
         
         
         
         
         //-----------------------------------------mesh info-----------------------------------------------------------------------
         if(vol_.size()==0)
         {
             is_mesh_3d_ = false;
             
             els_ = surf_;
             vector<int> els_size(els_.size());
             for(int i=0; i<els_.size(); i++)  els_size[i] = els_[i].size();             
             set<int> els_size_set(els_size.begin(), els_size.end());
 
             if(els_size_set.size()==1)
             {                
               if(els_size[0]==3)  el_type_ = "tri";
               else if (els_size[0]==4) el_type_ = "quad";
             }  
             else
               el_type_ = "2dhybrid";
                         
             sides_ = line_;
             sd_tag_ = line_tag_;

             el_pid_ = surf_pid_;
             sd_pid_ = line_pid_;
         }
         else
         {
             is_mesh_3d_ = true;
             
             els_ = vol_;
             vector<int> els_size(els_.size());
             for(int i=0; i<els_.size(); i++)  els_size[i] = els_[i].size(); 
             set<int> els_size_set(els_size.begin(), els_size.end());

             if(els_size_set.size()==1)
             {                
                if(els_size[0]==4)         el_type_ = "tetra";   
                else if (els_size[0]==8)   el_type_ = "hexa";   
                else if (els_size[0]==6)   el_type_ = "prism";   
             }   
             else
               el_type_ = "3dhybrid";   
                         
             sides_ = surf_;
             sd_tag_ = surf_tag_;

             el_pid_ = vol_pid_;
             sd_pid_ = surf_pid_;                    
         }
         end = chrono::steady_clock::now();
         duration = chrono::duration<double>(end - beg);
         cerr<<"------------------mesh elements ("<< el_type_ <<") reading done in "<< duration.count() <<" milli seconds-------------\n"<<endl;  
         //-------------------------------------------------------------------------------------------------------------------------
         
         
         
         
         
         
         
         //-----------------------------------------node flag-----------------------------------------------------------------------         
         beg = chrono::steady_clock::now();
         nd_flag_.resize(n_nds_);                // first we assign nd_flag = 0 for all nodes
         
         if(is_mesh_3d_)
         {
           for(int i=0; i<surf_.size(); i++)    // Check if mesh is 3D then loop over all surf elements and overwrite flags of nodes of an element equal to the flag of the element
             for(auto j : surf_[i])
               nd_flag_[j-1] = surf_tag_[i];           
         }     
                    
         for(int i=0; i<line_.size(); i++)       // Then we loop over all line elements(2D and 3D) and overwrite flags of nodes of an element equal to the flag of the line
           for(auto j : line_[i])
             nd_flag_[j-1] = line_tag_[i];           

         for(int i=0; i<pt_.size(); i++)           //Then we loop over all points(2D and 3D) and overwrite flags of nodes equal to the flag of the pt
           nd_flag_[pt_[i]-1] = pt_tag_[i];                        

         end = chrono::steady_clock::now();
         duration = chrono::duration<double>(end - beg);
         cerr<<"------------------nodes boundary flag is set in "<< duration.count() <<" milli seconds-------------\n"<<endl;  
         //-------------------------------------------------------------------------------------------------------------------------
         
         
         
         
         
                  
         
         
         
         
         //-----------------------------------------element flag-----------------------------------------------------------------------         
         beg = chrono::steady_clock::now();
         el_flag_.resize(els_.size());      
         
         if(el_type_=="hexa")
         {               
           for(int i=0; i<els_.size(); i++)
             el_flag_[i] = el_flag_hexa(i);
         }    

         else if(el_type_=="tetra" || el_type_=="prism")         
         {
           for(int i=0; i<els_.size(); i++)
             el_flag_[i] = el_flag_tetra_prism(i);
         }
          
         else if(el_type_=="quad" || el_type_=="tri")  
         {
           for(int i=0; i<els_.size(); i++)
             el_flag_[i] = el_flag_tri_quad(i);
         }    

         else if(el_type_=="3dhybrid")  
         {
           for(int i=0; i<els_.size(); i++)
             if(els_[i].size() == 8)                                   el_flag_[i] = el_flag_hexa(i);
             else if(els_[i].size() == 4 || els_[i].size() == 6)       el_flag_[i] = el_flag_tetra_prism(i);
         }    

         else if(el_type_=="2dhybrid")  
         {
           for(int i=0; i<els_.size(); i++)
             el_flag_[i] = el_flag_tri_quad(i);
         }    
             
         end = chrono::steady_clock::now();
         duration = chrono::duration<double>(end - beg);
         cerr<<"------------------element boundary flag is set in "<< duration.count() <<" milli seconds-------------\n"<<endl;  
         //-----------------------------------------------------------------------------------------------------------------------         
            
         
         
         
         
         
         if(!gmsh_partitioned_mesh_)
         {
             cout<<"------------------Gmsh mesh is not partitioned; therefore doing partition by metis------------------------------"<<endl<<endl;
         
             //----------------------------------------- mesh partitioning-----------------------------------------------------------------------         
             beg = chrono::steady_clock::now();
             el_pid_.resize(els_.size());
             nd_pid_.resize(n_nds_);         
          
             if(parts != 1)
             {            
                ofstream ofile("metis_input.txt");
                if(!ofile.is_open())
                {
                  cerr<<"ERROR: "<<__FILE__<<":"<<__LINE__<<":"<<" file is not opened  \n";
                  exit(0);
                }                           
                ofile << els_.size()<< "\n";
                for(int i=0; i<els_.size(); i++)
                { 
                  for(auto j : els_[i])
                    ofile << j << " ";
                  ofile << "\n"; 
                }
                ofile.close();
    
                if(el_type_=="hexa")
                {
                   auto s = "mpmetis -ncommon=4 metis_input.txt " + to_string(parts);             
                   system(s.c_str());
                }   
                else if(el_type_=="tetra" || el_type_=="prism")
                {
                   auto s = "mpmetis -ncommon=3 metis_input.txt " + to_string(parts);             
                   system(s.c_str());            
                }  
                else if(el_type_=="quad" || el_type_=="tri" || el_type_=="2dhybrid")
                {
                   auto s = "mpmetis -ncommon=2 metis_input.txt " + to_string(parts);                           
                   system(s.c_str());              
                }
                else if(el_type_=="3dhybrid")
                {
                   auto s = "mpmetis -ncommon=3 metis_input.txt " + to_string(parts);                           
                   system(s.c_str());              
                }
                else
                {
                   cerr<<"el_type: "<<el_type_<<endl;
                   cerr<<"is_mesh_3d_: "<<is_mesh_3d_<<endl;                   
                   cerr<<"ERROR: we got in strange condition in metis partitioning \n";
                   exit(0);
                }
                auto el_pid_file = "metis_input.txt.epart." + to_string(parts);
                auto nd_pid_file = "metis_input.txt.npart." + to_string(parts);             
                ifstream infile1(el_pid_file);
                if(!infile1.is_open())
                {
                  cerr<<"ERROR: "<<__FILE__<<":"<<__LINE__<<":"<<" file is not opened  \n";
                  exit(0);
                }                           
                for(int i=0; i<els_.size(); i++)
                   infile1 >> el_pid_[i];
                infile1.close();             
                
                ifstream infile2(nd_pid_file);
                if(!infile2.is_open())
                {
                   cerr<<"ERROR: "<<__FILE__<<":"<<__LINE__<<":"<<" file is not opened  \n";
                   exit(0);
                }                           
                for(int i=0; i<n_nds_; i++)
                   infile2 >> nd_pid_[i];
                infile2.close();       
                
                remove(el_pid_file.c_str());                  
                remove(nd_pid_file.c_str());                  
                remove("metis_input.txt");                  
             }                  
             end = chrono::steady_clock::now();
             duration = chrono::duration<double>(end - beg);
             cerr<<"------------------mesh partition by metis took "<< duration.count() <<" milli seconds-------------\n"<<endl;  
             //-----------------------------------------------------------------------------------------------------------------------         
    
    
             
             
             
             
             
             //-------------------------------------- side pid -----------------------------------------------------------------------
             beg = chrono::steady_clock::now();
             sd_pid_.resize(sides_.size());
    
             map<vector<int>, int> bd_els_sides_dict;     
             for(int i=0; i<els_.size(); i++)
             {
               if(el_flag_[i]==1)
               { 
                 auto el_sides = get_el_sides(i); 
                 for(int j=0; j<el_sides.size(); j++)
                 {
                   vector<int> side;
                   for(int k=0; k<el_sides[j].size(); k++)
                      side.push_back(els_[i][el_sides[j][k]]);
                   sort(side.begin(), side.end());            
                   bd_els_sides_dict[side] = i;                   
                 }
               }    
             }   
    
             for(int i=0; i<sides_.size(); i++)
             {
                auto side = sides_[i];
                sort(side.begin(), side.end());
                sd_pid_[i] = el_pid_[bd_els_sides_dict[side]];
             }         
             end = chrono::steady_clock::now();
             duration = chrono::duration<double>(end - beg);
             cerr<<"------------------side pid is set in "<< duration.count() <<" milli seconds-------------\n"<<endl;  
             //-----------------------------------------------------------------------------------------------------------------------         
         }
         else
            cout<<"------------------Gmsh mesh is partitioned--------------------------------"<<endl<<endl;
         
         
         
         
         
       
         //-------------------------------------- mesh writing -----------------------------------------------------------------------
         beg = chrono::steady_clock::now();         
         if(is_mesh_3d_)
         {
            auto minmax_x = minmax_element(x_.begin(), x_.end());
            auto minmax_y = minmax_element(y_.begin(), y_.end());
            auto minmax_z = minmax_element(z_.begin(), z_.end());

            ofstream ofile(write_mesh_name);             // mesh_72.txt
            ofile << "MESH! 3D \n";
            ofile << "nodes " << n_nds_<< "\n";
            ofile << "elements " << els_.size()<< "\n";
            ofile << "sides " << sides_.size()<< "\n";
            ofile << "X "  << *minmax_x.first << " " << *minmax_x.second<< "\n";
            ofile << "Y "  << *minmax_y.first << " " << *minmax_y.second<< "\n";
            ofile << "Z "  << *minmax_z.first << " " << *minmax_z.second<< "\n";
            for(int i=0; i<n_nds_; i++)
               ofile << fixed << setprecision(10) <<"Node " << i << " " << x_[i] << " " <<  y_[i] << " " << z_[i] << " " << nd_flag_[i]<< "\n";
           
            if(el_type_ == "tetra")
            {
              for(int i=0; i<els_.size(); i++)        write_tetra(i, ofile);
              for(int i=0; i<sides_.size(); i++)      write_tri_sd(i, ofile);
            }
            
            else if(el_type_ == "hexa")
            {   
              for(int i=0; i<els_.size(); i++)        write_hexa(i, ofile);
              for(int i=0; i<sides_.size(); i++)      write_quad_sd(i, ofile);
            }
            
            else if(el_type_ == "prism")
            {   
              for(int i=0; i<els_.size(); i++)        write_prism(i, ofile);
              
              for(int i=0; i<sides_.size(); i++)
              {
                 if(sides_[i].size()==3)              write_tri_sd(i, ofile);
                 else if(sides_[i].size()==4)         write_quad_sd(i, ofile);
              }   
            }

            else if(el_type_ == "3dhybrid")
            {
              for(int i=0; i<els_.size(); i++)    
              {
                 if(els_[i].size() == 8)              write_hexa(i, ofile);
                 else if(els_[i].size() == 4)         write_tetra(i, ofile);
                 else if(els_[i].size() == 6)         write_prism(i, ofile);
              }   
            
              for(int i=0; i<sides_.size(); i++)
              {
                 if(sides_[i].size()==3)              write_tri_sd(i, ofile);
                 else if(sides_[i].size()==4)         write_quad_sd(i, ofile);
              }   
            }  
                         
            ofile.close();
         }
         else
         {
            auto minmax_x = minmax_element(x_.begin(), x_.end());
            auto minmax_y = minmax_element(y_.begin(), y_.end());

            ofstream ofile(write_mesh_name);               // mesh_72.txt
            ofile << "MESH! 2D \n";
            ofile << "nodes " << n_nds_<< "\n";
            ofile << "elements " << els_.size()<< "\n";
            ofile << "sides " << sides_.size()<< "\n";
            ofile << "X "  << *minmax_x.first << " " << *minmax_x.second<< "\n";
            ofile << "Y "  << *minmax_y.first << " " << *minmax_y.second<< "\n";
            for(int i=0; i<n_nds_; i++)
               ofile << setprecision(12) << "Node " << i << " " << x_[i] << " " <<  y_[i] << " " << nd_flag_[i]<< "\n";
               
                           
            if(el_type_ == "tri")
              for(int i=0; i<els_.size(); i++)         write_tri(i, ofile);

            else if(el_type_ == "quad")   
              for(int i=0; i<els_.size(); i++)         write_quad(i, ofile);

            else if(el_type_ == "hybrid")   
              for(int i=0; i<els_.size(); i++)    
              {
                 if(els_[i].size() == 4)           write_quad(i, ofile);
                 else if(els_[i].size() == 3)      write_tri(i, ofile);
              }   
           
            for(int i=0; i<sides_.size(); i++)     write_line_sd(i, ofile);

            ofile.close();                  
         }    
         end = chrono::steady_clock::now();
         duration = chrono::duration<double>(end - beg);
         cerr<<"------------------mesh writing took "<< duration.count() <<" milli seconds-------------\n"<<endl;  
    }
    
    
    
    
    
        














    vector<vector<int>> get_el_sides(int i)
    {
       vector<vector<int>> el_sides;
       if(el_type_=="tri")   el_sides = { {1,2}, {2,0}, {0,1} };
       else if(el_type_=="quad")  el_sides = { {0,1}, {1,2}, {2,3}, {3,0} };
       else if(el_type_=="tetra")   el_sides = { {1,2,3}, {0,3,2}, {0,1,3}, {0,2,1} };
       else if(el_type_=="hexa")    el_sides = { {4,5,6,7}, {0,3,2,1}, {0,4,7,3}, {1,2,6,5}, {2,3,7,6}, {1,5,4,0} };
       else if(el_type_=="prism")    el_sides = { {1,3,2}, {4,5,6}, {2,3,6,5}, {1,2,5,4}, {1,4,6,3}  };
       else if(el_type_=="2dhybrid")
       {
         if(els_[i].size() == 3)             el_sides = { {1,2}, {2,0}, {0,1} };
         else if(els_[i].size() == 4)        el_sides = { {0,1}, {1,2}, {2,3}, {3,0} };         
       }
       else if(el_type_=="3dhybrid")
       {
         if(els_[i].size() == 4 )            el_sides = { {1,2,3}, {0,3,2}, {0,1,3}, {0,2,1} };        
         else if(els_[i].size() == 8)        el_sides = { {4,5,6,7}, {0,3,2,1}, {0,4,7,3}, {1,2,6,5}, {2,3,7,6}, {1,5,4,0} };
         else if(els_[i].size() == 6)        el_sides = { {1,3,2}, {4,5,6}, {2,3,6,5}, {1,2,5,4}, {1,4,6,3}  };                    
       }       
       return el_sides;
    }
    
    
    
    

         
         
    int el_flag_hexa(int i)
    {
       int flag = 0; 
       auto tot = 0;
       for(auto j : els_[i])
          if(nd_flag_[j-1]>0)  
              tot += 1;
       if(tot>=4)                //for hexa el we have quad as side
           flag = 1;
       return flag;
    }
             
             
             
             
             
             
             
    int el_flag_tetra_prism(int i)
    {
       int flag = 0; 
       auto tot = 0;
       for(auto j : els_[i])
          if(nd_flag_[j-1]>0)  
              tot += 1;
       if(tot>=3)                //for tetra el we have tri as side  # for prism we have both tri and quad as sides
           flag = 1;
       return flag;
    }





    int el_flag_tri_quad(int i)
    {
       int flag = 0; 
       auto tot = 0;
       for(auto j : els_[i])
          if(nd_flag_[j-1]>0)  
              tot += 1;
       if(tot>=2)                //for tri and quad el we have line as side
           flag = 1;
       return flag;
    }





    void write_tetra(int i, ofstream& ofile)
    {
        ofile << "Element " << i << " "  << el_pid_[i] << " 4 " << els_[i][0]-1 << " " << els_[i][1]-1 << " " << els_[i][2]-1 << " "<< els_[i][3]-1 << " " << el_flag_[i]<< "\n";         
    }
    





    void write_hexa(int i, ofstream& ofile)
    {
        ofile << "Element " << i << " "  << el_pid_[i] << " 8 " << els_[i][0]-1 << " " << els_[i][1]-1 << " " << els_[i][2]-1 << " "<< els_[i][3]-1 << " " << 
                                                                els_[i][4]-1 << " " << els_[i][5]-1 << " " << els_[i][6]-1 << " "<< els_[i][7]-1 << " " << el_flag_[i]<< "\n";
    }





    void write_prism(int i, ofstream& ofile)
    {
        ofile << "Element " << i << " "  << el_pid_[i] << " 6 " << els_[i][0]-1 << " " << els_[i][1]-1 << " " << els_[i][2]-1 << " "<< els_[i][3]-1 << " " << 
                                                                els_[i][4]-1 << " " << els_[i][5]-1 << " " << el_flag_[i]<< "\n";
    }
    
        
        
        
    
    void write_tri_sd(int i, ofstream& ofile)
    {
        ofile << "Side " << i << " "  <<  sd_pid_[i] << " 3 " << sides_[i][0]-1 << " " << sides_[i][1]-1 << " " << sides_[i][2]-1 << " " << sd_tag_[i]<< "\n";
    }
    
    



    void write_quad_sd(int i, ofstream& ofile)
    {
        ofile << "Side " << i << " "  <<  sd_pid_[i] << " 4 " << sides_[i][0]-1 << " " << sides_[i][1]-1 << " " << sides_[i][2]-1 << " " << sides_[i][3]-1 << " " << sd_tag_[i]<< "\n";
    }





    void write_tri(int i, ofstream& ofile)
    {
       if(IsTriAntiClockwise(i))
           ofile << "Element " << i <<" "<< el_pid_[i] <<" 3 "<< els_[i][0]-1 << " " << els_[i][1]-1 << " " << els_[i][2]-1 << " " << el_flag_[i]<< "\n";         
       else
           ofile << "Element " << i <<" "<< el_pid_[i] <<" 3 "<< els_[i][0]-1 << " " << els_[i][2]-1 << " " << els_[i][1]-1 << " " << el_flag_[i]<< "\n";                
    }
    
    
    
    
    void write_quad(int i, ofstream& ofile)
    {
       if(IsQuadAntiClockwise(i))
         ofile << "Element " << i <<" " << el_pid_[i] <<" 4 "<< els_[i][0]-1 << " " << els_[i][1]-1 << " " << els_[i][2]-1 << " "<< els_[i][3]-1 << " " << el_flag_[i]<< "\n";
       else  
         ofile << "Element " << i <<" " << el_pid_[i] <<" 4 "<< els_[i][0]-1 << " " << els_[i][3]-1 << " " << els_[i][2]-1 << " "<< els_[i][1]-1 << " " << el_flag_[i]<< "\n";
    }
    
    
    
    
    void write_line_sd(int i, ofstream& ofile)
    {
      ofile << "Side " << i <<" " <<  sd_pid_[i] <<" 2 "<< sides_[i][0]-1 << " " << sides_[i][1]-1 << " " << sd_tag_[i]<< "\n";
    }
    
    
    
    
    
    int get_pid(const vector<int>& tags)
    {
       int pid = -1;
       if(tags.size() > 2)
       {
          int num_mesh_partitions_of_el = tags[2];
          if(num_mesh_partitions_of_el == 1 && tags[3] >= 0)
                pid = tags[3]-1;
          else
          {
            cerr<<"num_mesh_partitions_of_el: "<< num_mesh_partitions_of_el << endl;
            cerr<<"Partition Id: "<< tags[3] << endl<<endl;
            cerr<<"-----------------Error----------------------------------------------------------------"<<endl; 
            cerr<<" 1) Either the number of mesh partitions to which the element belong is more than 1 "<< endl;
            cerr<<" 2) Or Partition Id is negative means ghost cell "<< endl;                                
            exit(0);
          }      
        }        
        return pid;       
    }
    






    
    bool IsTriAntiClockwise(int el_index)
    {
      vector<double> el_nd_x = {x_[els_[el_index][0]-1], x_[els_[el_index][1]-1], x_[els_[el_index][2]-1]};
      vector<double> el_nd_y = {y_[els_[el_index][0]-1], y_[els_[el_index][1]-1], y_[els_[el_index][2]-1]};
      auto var = 0.0;
      for (int j=0; j<el_nd_x.size(); j++)
      {
        auto x1 = el_nd_x[j];
        auto y1 = el_nd_y[j];
        auto x2 = el_nd_x[(j+1)%el_nd_x.size()];
        auto y2 = el_nd_y[(j+1)%el_nd_x.size()];
        var += (x2-x1)*(y2+y1);          
      }          
      return (var < 0.0);
    }
    
    
    
    
    
    

    bool IsQuadAntiClockwise(int el_index)
    {
      vector<double> el_nd_x = {x_[els_[el_index][0]-1], x_[els_[el_index][1]-1], x_[els_[el_index][2]-1], x_[els_[el_index][3]-1]};
      vector<double> el_nd_y = {y_[els_[el_index][0]-1], y_[els_[el_index][1]-1], y_[els_[el_index][2]-1], y_[els_[el_index][3]-1]};
      auto var = 0.0;
      for (int j=0; j<el_nd_x.size(); j++)
      {
        auto x1 = el_nd_x[j];
        auto y1 = el_nd_y[j];
        auto x2 = el_nd_x[(j+1)%el_nd_x.size()];
        auto y2 = el_nd_y[(j+1)%el_nd_x.size()];
        var += (x2-x1)*(y2+y1);          
      }          
      return (var < 0.0);
    }
    
    
    
    

    ///----------------------------------------------------------------------------------------- 
    /// This function reads only one line from the file (input) and return a vector of string as output
    void read_one_line(ifstream& file, vector<string>& split_result)
    {
        split_result.clear();
        
        string s;
        getline(file,s);
  
        stringstream ss(s);  
        string word;
        while (ss >> word) 
          split_result.push_back(word);    
    }
    ///-----------------------------------------------------------------------------------------
    





    ///----------------------------------------------------------------------------------------- 
    /// This function reads and skips only one line from the file (input)
    void skip_one_line(ifstream& file)
    {        
        string s;
        getline(file,s);  
    }
    ///-----------------------------------------------------------------------------------------
    
  
  
      




    public:
     int mesh_version_;
     int n_nds_;
     vector<double> x_, y_, z_;
     vector<int> pt_, pt_tag_, line_tag_, surf_tag_, vol_tag_;
     vector<vector<int>> line_, surf_, vol_;       
     vector<int> pt_pid_, line_pid_, surf_pid_, vol_pid_;


     bool is_mesh_3d_;
     vector<vector<int>> els_, sides_ ;
     vector<int> sd_tag_;
     string el_type_;

     vector<int> nd_flag_, el_flag_;
     vector<int> el_pid_, nd_pid_, sd_pid_;
     map<int,int> pt_map_, line_map_, surf_map_, vol_map_;        
     map<int,int> pt_pid_map_, line_pid_map_, surf_pid_map_, vol_pid_map_;     
     map<int,int> pt_tag_map_, line_tag_map_, surf_tag_map_, vol_tag_map_;     
     bool gmsh_partitioned_mesh_ = false;               
  };
















 void matching_interface_nodes(const gmsh_to_gales& f_mesh, const gmsh_to_gales& s_mesh, const string& out_file)
 {
       vector<int> f_nodes;
       for(int i=0; i<f_mesh.sides_.size(); i++)
         if(f_mesh.sd_tag_[i] == 1)
           for(auto nd : f_mesh.sides_[i])
              f_nodes.push_back(nd-1);
       sort(f_nodes.begin(), f_nodes.end());
       f_nodes.erase( unique(f_nodes.begin(), f_nodes.end()), f_nodes.end() );
                

       vector<int> s_nodes;
       for(int i=0; i<s_mesh.sides_.size(); i++)
         if(s_mesh.sd_tag_[i] == 1)
           for(auto nd : s_mesh.sides_[i])
              s_nodes.push_back(nd-1);
       sort(s_nodes.begin(), s_nodes.end());
       s_nodes.erase( unique(s_nodes.begin(), s_nodes.end()), s_nodes.end() );

            
       if(f_nodes.size() != s_nodes.size())
       {
           cerr<<"\n ------------------Error: fluid and solid have different number of interface nodes i.e. meshes are not correct------------------ \n";
           cerr<<"------------------f_interface_nodes: "<<f_nodes.size()<<"------------------\n";
           cerr<<"------------------s_interface_nodes: "<<s_nodes.size()<<"------------------\n\n";
           cerr<<"------------------ERROR!!!!  ------------------\n\n";
       }
       else     
           cerr<<"\n------------------Good: fluid and solid have same number of interface nodes: "<<f_nodes.size()<<"------------------\n";            
            
    
       auto beg = chrono::steady_clock::now();         
       vector<int> f_I_nd, s_I_nd;
       for(auto i : f_nodes)
       { 
          for(auto j : s_nodes)
          {
             if(f_mesh.x_[i]==s_mesh.x_[j] && f_mesh.y_[i]==s_mesh.y_[j] && f_mesh.z_[i]==s_mesh.z_[j])
             {
                      f_I_nd.push_back(i);
                      s_I_nd.push_back(j);             
                      break;
             }
          }              
       }
       
       vector<int> uncovered_f;
       if(f_nodes.size() != f_I_nd.size())
       {
           cerr<<"\n ------------------Error: All fluid nodes on fluid-solid interface are not covered in f_I_nd ------------------ \n";
           cerr<<"------------------f_nodes: "<<f_nodes.size()<<"------------------\n";
           cerr<<"------------------f_I_nd: "<<f_I_nd.size()<<"------------------\n";
           cerr<<"------------------check uncovered_nodes.txt for nodes not covered ------------------\n";

           for(auto i : f_nodes) 
             if( find(f_I_nd.begin(), f_I_nd.end(), i) != f_I_nd.end() )
               uncovered_f.push_back(i);
                    
           remove("uncovered_nodes.txt"); 
           ofstream ofile("uncovered_nodes.txt");
           ofile<<"-------------fluid-----------------\n";           
           for(auto i : uncovered_f)
              ofile<<i<<" "<<f_mesh.x_[i]<<" "<<f_mesh.y_[i]<<" "<<f_mesh.z_[i]<<"\n\n";
           ofile.close();      
       }
    
    


       vector<int> uncovered_s;
       if(s_nodes.size() != s_I_nd.size())
       {
           cerr<<"\n ------------------Error: All solid nodes on fluid-solid interface are not covered in s_I_nd ------------------ \n";
           cerr<<"------------------s_nodes: "<<s_nodes.size()<<"------------------\n";
           cerr<<"------------------s_I_nd: "<<s_I_nd.size()<<"------------------\n";
           cerr<<"------------------check uncovered_nodes.txt for nodes not covered ------------------\n";

           for(auto i : s_nodes) 
             if( find(s_I_nd.begin(), s_I_nd.end(), i) != s_I_nd.end() )
               uncovered_s.push_back(i);
                    
           ofstream ofile("uncovered_nodes.txt", ios_base::app);
           ofile<<"-------------solid-----------------\n";           
           for(auto i : uncovered_s)
              ofile<<i<<" "<<s_mesh.x_[i]<<" "<<s_mesh.y_[i]<<" "<<s_mesh.z_[i]<<"\n";
           ofile.close();      
       }

        
       auto end = chrono::steady_clock::now();
       auto duration = chrono::duration<double>(end - beg);
       cerr<<"------------------Interface matching nodes took "<< duration.count() <<" milli seconds-------------\n"<<endl;  

    
       ofstream ofile(out_file);
       ofile<<f_I_nd.size()<<"\n";
       for(int i=0; i<f_I_nd.size(); i++)
          ofile<<f_I_nd[i]<<" "<<s_I_nd[i]<<"\n";
       ofile.close();
  }

  
