#include<iostream>
#include<fstream>
#include<sstream>
#include<vector>
#include<string>

#include "gmsh_to_gales.hpp"


  
int main(int argc, char** argv)
{
   int parts = 1;
   std::string mesh_name = "mesh.msh";
   
   if(argc==2)  
   {
      parts = std::stoi(argv[1]);      
   }
   else if(argc==3) 
   {
      parts = std::stoi(argv[1]);      
      mesh_name = argv[2];         
   }
   
   std::string ofile = mesh_name.substr(0, mesh_name.size()-4) + "_" + std::to_string(parts) + "core.txt";
            
   gmsh_to_gales mesh(mesh_name, parts, ofile);

   
   std::cerr<<"------------------------- FINISHED!!!!!! --------------------------\n\n\n";
   
   return 0;
}  
  

