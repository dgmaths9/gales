#!/usr/bin/python3

import os
import sys
from timeit import default_timer as timer





class gmsh_to_gales:

    def __init__(self, read_mesh_name, parts, write_mesh_name):
       self.read_mesh_name = read_mesh_name
       self.parts = parts
       self.write_mesh_name = write_mesh_name       

       #---------Reading GMSH(.msh) file--------------------
       with open(self.read_mesh_name, 'r') as f:
          self.read_version(f)
          self.gmsh_partitioned_mesh = False
          if(self.mesh_version == 4):
             self.read_entities(f)
             self.read_partitioned_entities(f)
          self.read_nds(f)
          self.read_els(f)    
    
    
    
       #------ write mesh----------------------------
       self.node_flag()
       self.element_flag()   
            
       if(self.gmsh_partitioned_mesh == False):
         print('------------------Gmsh mesh is not partitioned; therefore doing partition by metis------------------\n')
         self.metis_partition()
       else:
         print('------------------Gmsh mesh is partitioned--------------------------------\n')
                  
       start = timer()
       if(self.is_mesh_3d):         
           self.mesh_writing_3d()                                                                                     
       else: 
           self.mesh_writing_2d()
       end = timer()
       print ('\n------------------mesh writing took {s1} s------------------\n\n'.format(s1 = end-start))



    
    
      
    
    
    def read_version(self, f):
      f.readline()                                    # $MeshFormat
      version = f.readline().strip().split()          # 2.2 0 8    or   4.1 0 8
      if(version[0] == "2.2"):
         self.mesh_version = 2
         print('\n------------------gmsh mesh version is 2.2--------------------')
      elif(version[0] == "4.1"):
         self.mesh_version = 4
         print('\n------------------gmsh mesh version is 4.1--------------------')
      else:
         print(' ')
         print('-----------------Error: gmsh mesh version is neither 2.2 nor 4.1  ----------')
         print('----------------- We support only these two versions------------------------')
         sys.exit('\n')            
      f.readline()                                   # $EndMeshFormat
    
    



    
    
    
    
    def read_entities(self, f):
        f.readline()                                 # $Entities
        result = f.readline().strip().split()
        numPoints, numCurves, numSurfaces, numVolumes = int(result[0]), int(result[1]), int(result[2]), int(result[3])          
        pt_map, line_map, surf_map, vol_map = {}, {}, {}, {}         
        for i in range(numPoints):
            result = f.readline().strip().split()          
            entityTag = int(result[0]) 
            numPhysicalTags = int(result[4])
            if(numPhysicalTags == 1):
               physicalTag = int(result[5])            
               if(physicalTag == 0 ):
                    print(' ')
                    print('-----------------Error: gmsh did not write the right physical tag for {s}th point in {s2} file ----------'.format(s=i, s2=self.read_mesh_name))
                    print('-----------------To do: check and edit manualy for the the right physical tag for the point in the Entities block ------------------')
                    print('-----------------Generate mesh file again ------------------\n')
                    sys.exit('\n')                    
               pt_map[entityTag] = physicalTag           
            if(numPhysicalTags > 1):
                 print('-----------------Error: numPhysicalTags for the {}th point is greater than 1; it should be 1 ----------'.format(s=i))
                 sys.exit('\n')       
        for i in range(numCurves):
            result = f.readline().strip().split()
            entityTag = int(result[0]) 
            numPhysicalTags = int(result[7])
            if(numPhysicalTags == 1):
               physicalTag = int(result[8])                        
               line_map[entityTag] = physicalTag
            if(numPhysicalTags > 1):
                print('-----------------Error: numPhysicalTags for the {}th curve is greater than 1; it should be 1 ----------'.format(s=i))
                sys.exit('\n')    
        for i in range(numSurfaces):
            result = f.readline().strip().split()
            entityTag = int(result[0]) 
            numPhysicalTags = int(result[7])
            if(numPhysicalTags == 1):
               physicalTag = int(result[8])                        
               surf_map[entityTag] = physicalTag
            if(numPhysicalTags > 1):
                print('-----------------Error: numPhysicalTags for the {}th surface is greater than 1; it should be 1 ----------'.format(s=i))
                sys.exit('\n')    
        for i in range(numVolumes):
            result = f.readline().strip().split()
            entityTag = int(result[0]) 
            numPhysicalTags = int(result[7])
            if(numPhysicalTags == 1):
               physicalTag = int(result[8])                            
               vol_map[entityTag] = physicalTag
            if(numPhysicalTags > 1):
                print('-----------------Error: numPhysicalTags for the {}th volume is greater than 1; it should be 1 ----------'.format(s=i))
                sys.exit('\n')    
        f.readline()                                                        # $EndEntities
        self.pt_map, self.line_map, self.surf_map, self.vol_map = pt_map, line_map, surf_map, vol_map
      
      
      






    
    
    def read_partitioned_entities(self, f):
         position = f.tell()          # get the position of the file pointer
         pt_pid_map, line_pid_map, surf_pid_map, vol_pid_map = {}, {}, {}, {}         
         pt_tag_map, line_tag_map, surf_tag_map, vol_tag_map = {}, {}, {}, {}         
         result = f.readline().strip().split()
         if(result[0] == "$PartitionedEntities"):           # $PartitionedEntities
             numPartitions = int(f.readline())
             numGhostEntities = int(f.readline())
             if(numGhostEntities > 0):
                 print('-----------Error: numGhostEntities is greater than 0; it should be zero-------------------')
                 print('-----------We do not read ghost entities-------------------')
                 sys.exit('\n')    
             result = f.readline().strip().split()
             numPoints, numCurves, numSurfaces, numVolumes = int(result[0]), int(result[1]), int(result[2]), int(result[3])   
    
             for i in range(numPoints):
                result = f.readline().strip().split()          
                pointTag, parentDim, parentTag, numPartitions = int(result[0]), int(result[1]), int(result[2]), int(result[3])            
                if(numPartitions == 1):
                   pt_pid_map[pointTag] = int(result[4])-1
                partitionTag = [int(result[4+j]) for j in range(numPartitions)]            
                numPhysicalTags = int(result[3+numPartitions+4])
                if(numPhysicalTags == 1):
                   physicalTag = int(result[3+numPartitions+5])
                   if(physicalTag == 0 ):
                        print(' ')
                        print('-----------------Error: gmsh did not write the right physical tag for {s}th point in {s2} file ----------'.format(s=i, s2=self.read_mesh_name))
                        print('-----------------To do: check and edit manualy for the the right physical tag for the point in the Entities block ------------------')
                        print('-----------------Generate mesh file again ------------------\n')
                        sys.exit('\n')                    
                   
                   
                   pt_tag_map[pointTag] = physicalTag
                elif(numPhysicalTags > 1):
                    print('-----------------Error: numPhysicalTags for the {}th point is greater than 1 in PartitionedEntities; it should be 1 or 0 ----------'.format(s=i))
                    sys.exit('\n')
                                    
             for i in range(numCurves):
                result = f.readline().strip().split()          
                curveTag, parentDim, parentTag, numPartitions = int(result[0]), int(result[1]), int(result[2]), int(result[3])            
                if(numPartitions == 1):
                   line_pid_map[curveTag] = int(result[4])-1
                partitionTag = [int(result[4+j]) for j in range(numPartitions)]            
                numPhysicalTags = int(result[3+numPartitions+7])
                if(numPhysicalTags == 1):
                   physicalTag = int(result[3+numPartitions+8])
                   line_tag_map[curveTag] = physicalTag
                elif(numPhysicalTags > 1):
                    print('-----------------Error: numPhysicalTags for the {}th curve is greater than 1 in PartitionedEntities; it should be 1 or 0----------'.format(s=i))
                    sys.exit('\n')
                         
             for i in range(numSurfaces):
                result = f.readline().strip().split()          
                surfaceTag, parentDim, parentTag, numPartitions = int(result[0]), int(result[1]), int(result[2]), int(result[3])            
                if(numPartitions == 1):
                   surf_pid_map[surfaceTag] = int(result[4])-1
                partitionTag = [int(result[4+j]) for j in range(numPartitions)]            
                numPhysicalTags = int(result[3+numPartitions+7])
                if(numPhysicalTags == 1):
                   physicalTag = int(result[3+numPartitions+8])
                   surf_tag_map[surfaceTag] = physicalTag
                elif(numPhysicalTags > 1):
                    print('-----------------Error: numPhysicalTags for the {}th surface is greater than 1 in PartitionedEntities; it should be 1 or 0----------'.format(s=i))
                    sys.exit('\n')
        
             for i in range(numVolumes):
                result = f.readline().strip().split()          
                volumeTag, parentDim, parentTag, numPartitions = int(result[0]), int(result[1]), int(result[2]), int(result[3])            
                if(numPartitions == 1):
                   vol_pid_map[volumeTag] = int(result[4])-1
                partitionTag = [int(result[4+j]) for j in range(numPartitions)]            
                numPhysicalTags = int(result[3+numPartitions+7])
                if(numPhysicalTags == 1):
                   physicalTag = int(result[3+numPartitions+8])
                   vol_tag_map[volumeTag] = physicalTag
                elif(numPhysicalTags > 1):
                    print('-----------------Error: numPhysicalTags for the {}th volume is greater than 1 in PartitionedEntities; it should be 1 or 0----------'.format(s=i))
                    sys.exit('\n')
             
             f.readline() # $EndPartitionedEntities
         else:
             f.seek(position)   # put file pointer at the position of the previous line 
    
         if(len(pt_pid_map)!=0 or len(line_pid_map)!=0 or len(surf_pid_map)!=0 or len(vol_pid_map)!=0):
             self.gmsh_partitioned_mesh = True
    
         self.pt_pid_map, self.line_pid_map, self.surf_pid_map, self.vol_pid_map = pt_pid_map, line_pid_map, surf_pid_map, vol_pid_map
         self.pt_tag_map, self.line_tag_map, self.surf_tag_map, self.vol_tag_map = pt_tag_map, line_tag_map, surf_tag_map, vol_tag_map







      


    def read_nds(self, f):
      f.readline()                           # $Nodes
      x, y, z = [], [], []
      start = timer()
      if(self.mesh_version == 2):
         n_nds = int(f.readline())
         for i in range (n_nds):
           result = f.readline().strip().split()
           x.append(float(result[1]))
           y.append(float(result[2]))
           z.append(float(result[3]))
      elif(self.mesh_version == 4):
         result = f.readline().strip().split()
         numEntityBlocks, n_nds, minNodeTag, maxNodeTag = int(result[0]), int(result[1]), int(result[2]), int(result[3]) 
         x, y, z = [None]*n_nds, [None]*n_nds, [None]*n_nds
         
         for i in range(numEntityBlocks):
           result = f.readline().strip().split()         
           entityDim, entityTag, parametric, numNodesInBlock = int(result[0]), int(result[1]), int(result[2]), int(result[3])
         
           nodeTag = []  
           for j in range(numNodesInBlock):
             nodeTag.append(int(f.readline())-1)             
           for j in range(numNodesInBlock):
             result = f.readline().strip().split()                    
             x[nodeTag[j]] = float(result[0])
             y[nodeTag[j]] = float(result[1])
             z[nodeTag[j]] = float(result[2])
      end = timer()
      print(' ')
      print('------------------mesh nodes reading done in {s1} s-------------'.format(s1 = end-start))
      f.readline()                    # $EndNodes
      self.n_nds = n_nds
      self.x, self.y, self.z = x, y, z
             
           
                  

         
         
         
         


    def get_pid(self,tags):
          pid = -1
          if(len(tags) > 2):
             num_mesh_partitions_of_el = tags[2]
             if(num_mesh_partitions_of_el == 1 and tags[3] >= 0):
                pid = tags[3]-1
             else:
                print('num_mesh_partitions_of_el: ', num_mesh_partitions_of_el)
                print('Partition Id: ', tags[3])
                print(' ')
                print('-----------------Error----------------------------------------------------------------') 
                print('1) Either the number of mesh partitions to which the element belong is more than 1 ----------')
                print('2) Or Partition Id is negative means ghost cell ----------')                                
                sys.exit('\n')
          return pid       








    def read_els(self,f):
      f.readline()                   # $Elements
      pt, line, surf, vol, pt_tag, line_tag, surf_tag, vol_tag = [], [], [], [], [], [], [], []
      pt_pid, line_pid, surf_pid, vol_pid = [], [], [], []
      el_pid, sd_pid = [], []
      start = timer()

      if(self.mesh_version == 2):
          n_els_tot = int(f.readline())        
          for i in range(n_els_tot):
              result = f.readline().strip().split()
              elm_number, elm_type, number_of_tags = int(result[0]), int(result[1]), int(result[2])
              tags = [int(result[3+j])  for j in range(number_of_tags)]          
              physical_tag = tags[0]
              geometrical_entity = tags[1]
              if(elm_type==15):                  #point
                   pt.append(int(result[2+number_of_tags+1]))
                   if(physical_tag==0):
                      print(' ')
                      print('-----------------Error: gmsh did not write the right physical tag for {s}th element (a point) in {s2} file ----------'.format(s=i, s2=self.read_mesh_name))
                      print('-----------------To do: check and edit manualy for the the right physical tag ------------------')
                      print('-----------------Generate mesh file again ------------------\n')
                      sys.exit('\n')
                   pt_tag.append(physical_tag)
                   if(self.get_pid(tags) >= 0):   
                      pt_pid.append(self.get_pid(tags))
              elif(elm_type==1):                 #line
                   line.append([int(result[2+number_of_tags+1]), int(result[2+number_of_tags+2])])           
                   line_tag.append(physical_tag)
                   if(self.get_pid(tags) >= 0):   
                      line_pid.append(self.get_pid(tags))
              elif(elm_type==2):                 #tri
                   surf.append([int(result[2+number_of_tags+1]), int(result[2+number_of_tags+2]), int(result[2+number_of_tags+3])])           
                   surf_tag.append(physical_tag)       
                   if(self.get_pid(tags) >= 0):   
                      surf_pid.append(self.get_pid(tags))
              elif(elm_type==3):                 #quad
                   surf.append([int(result[2+number_of_tags+1]), int(result[2+number_of_tags+2]), int(result[2+number_of_tags+3]), int(result[2+number_of_tags+4])])           
                   surf_tag.append(physical_tag)              
                   if(self.get_pid(tags) >= 0):   
                      surf_pid.append(self.get_pid(tags))
              elif(elm_type==4):                 #tetra
                   vol.append([int(result[2+number_of_tags+1]), int(result[2+number_of_tags+2]), int(result[2+number_of_tags+3]), int(result[2+number_of_tags+4])])           
                   vol_tag.append(physical_tag)              
                   if(self.get_pid(tags) >= 0):   
                      vol_pid.append(self.get_pid(tags))
              elif(elm_type==5):                 #hexa
                   vol.append([int(result[2+number_of_tags+1]), int(result[2+number_of_tags+2]), int(result[2+number_of_tags+3]), int(result[2+number_of_tags+4]), 
                               int(result[2+number_of_tags+5]), int(result[2+number_of_tags+6]), int(result[2+number_of_tags+7]), int(result[2+number_of_tags+8])])           
                   vol_tag.append(physical_tag)                     
                   if(self.get_pid(tags) >= 0):   
                    vol_pid.append(self.get_pid(tags))  
              elif(elm_type==6):                 #prism
                   vol.append([int(result[2+number_of_tags+1]), int(result[2+number_of_tags+2]), int(result[2+number_of_tags+3]), int(result[2+number_of_tags+4]), 
                               int(result[2+number_of_tags+5]), int(result[2+number_of_tags+6])])           
                   vol_tag.append(physical_tag)                     
                   if(self.get_pid(tags) >= 0):   
                    vol_pid.append(self.get_pid(tags))  
              else:                 
                print('-----------------Error----------------------------------------------------------------') 
                print('elm_type: ', elm_type)
                print('We do not read this type of element')
                sys.exit('\n')


          
          if(len(pt_pid)!=0 or len(line_pid)!=0 or len(surf_pid)!=0 or len(vol_pid)!=0):
             self.gmsh_partitioned_mesh = True
                          
      elif(self.mesh_version == 4):
          result = f.readline().strip().split()         
          numEntityBlocks, n_els_tot, minElementTag, maxElementTag = int(result[0]), int(result[1]), int(result[2]), int(result[3])  
          for i in range(numEntityBlocks):
              result = f.readline().strip().split()         
              entityDim, entityTag, elementType, numElementsInBlock = int(result[0]), int(result[1]), int(result[2]), int(result[3])  
              if(entityDim == 0):
                for j in range(numElementsInBlock):
                  result = f.readline().strip().split()
                  pt.append(int(result[1]))
                  if(self.gmsh_partitioned_mesh):                  
                     pt_tag.append(self.pt_tag_map[entityTag])
                     pt_pid.append(self.pt_pid_map[entityTag])
                  else:
                     pt_tag.append(self.pt_map[entityTag])                                               
              elif(entityDim == 1):
                for j in range(numElementsInBlock):
                  result = f.readline().strip().split()
                  line.append([int(result[1]),int(result[2])])
                  if(self.gmsh_partitioned_mesh):                  
                     line_tag.append(self.line_tag_map[entityTag])
                     line_pid.append(self.line_pid_map[entityTag])
                  else:
                     line_tag.append(self.line_map[entityTag])                                               
              elif(entityDim == 2 and elementType == 2):
                for j in range(numElementsInBlock):
                  result = f.readline().strip().split()
                  surf.append([int(result[1]),int(result[2]),int(result[3])])
                  if(self.gmsh_partitioned_mesh):                  
                     surf_tag.append(self.surf_tag_map[entityTag])
                     surf_pid.append(self.surf_pid_map[entityTag])
                  else:
                     surf_tag.append(self.surf_map[entityTag])                                               
              elif(entityDim == 2 and elementType == 3):
                for j in range(numElementsInBlock):
                  result = f.readline().strip().split()
                  surf.append([int(result[1]),int(result[2]),int(result[3]),int(result[4])])
                  if(self.gmsh_partitioned_mesh):                  
                     surf_tag.append(self.surf_tag_map[entityTag])
                     surf_pid.append(self.surf_pid_map[entityTag])
                  else:
                     surf_tag.append(self.surf_map[entityTag])                                               
              elif(entityDim == 3 and elementType == 4):
                for j in range(numElementsInBlock):
                  result = f.readline().strip().split()
                  vol.append([int(result[1]),int(result[2]),int(result[3]),int(result[4])])
                  if(self.gmsh_partitioned_mesh):                  
                     vol_tag.append(self.vol_tag_map[entityTag])
                     vol_pid.append(self.vol_pid_map[entityTag])
                  else:
                     vol_tag.append(self.vol_map[entityTag])                                               
              elif(entityDim == 3 and elementType == 5):
                for j in range(numElementsInBlock):
                  result = f.readline().strip().split()
                  vol.append([int(result[1]),int(result[2]),int(result[3]),int(result[4]),
                              int(result[5]), int(result[6]), int(result[7]), int(result[8])])
                  if(self.gmsh_partitioned_mesh):                  
                     vol_tag.append(self.vol_tag_map[entityTag])
                     vol_pid.append(self.vol_pid_map[entityTag])
                  else:
                     vol_tag.append(self.vol_map[entityTag])                                                                                        
              elif(entityDim == 3 and elementType == 6):
                for j in range(numElementsInBlock):
                  result = f.readline().strip().split()
                  vol.append([int(result[1]),int(result[2]),int(result[3]),int(result[4]),
                              int(result[5]), int(result[6])])
                  if(self.gmsh_partitioned_mesh):                  
                     vol_tag.append(self.vol_tag_map[entityTag])
                     vol_pid.append(self.vol_pid_map[entityTag])
                  else:
                     vol_tag.append(self.vol_map[entityTag])                                                                                        

              else:                 
                print('-----------------Error----------------------------------------------------------------') 
                print('elementType: ', elementType)
                print('We do not read this type of element')
                sys.exit('\n')



      end = timer()
      f.readline()              # $EndElements

      if(len(vol)==0):
          is_mesh_3d = False  
          els = surf
          n_els = len(surf)

          el_tag = surf_tag          
          els_size = [len(el) for el in els]
                    
          if(len(set(els_size)) == 1):
              if(els_size[0] == 3): 
                   el_type = 'tri'              
              elif(els_size[0] == 4): 
                   el_type = 'quad'                 
          else:               
              el_type = '2dhybrid'                                          
                          
          sides = line
          n_sides = len(line)
          sd_tag = line_tag
          
          el_pid = surf_pid
          sd_pid = line_pid
          
      else:  
          is_mesh_3d = True
          els = vol
          n_els = len(vol)        
          el_tag = vol_tag
          els_size = [len(el) for el in els]
          
          sides = surf
          n_sides = len(surf)
          sd_tag = surf_tag
          
          if(len(set(els_size)) == 1): 
             if(els_size[0] == 4):        
                el_type = 'tetra'
             elif(els_size[0] == 8):
                el_type = 'hexa'              
             elif(els_size[0] == 6):
                el_type = 'prism'
          else:
             el_type = '3dhybrid'  
          

          el_pid = vol_pid
          sd_pid = surf_pid

      self.pt, self.line, self.surf, self.vol = pt, line, surf, vol  
      self.pt_tag, self.line_tag, self.surf_tag, self.vol_tag = pt_tag, line_tag, surf_tag, vol_tag  
      self.is_mesh_3d = is_mesh_3d  
      self.els, self.n_els, self.el_tag, self.el_type = els, n_els, el_tag, el_type
      self.sides, self.n_sides, self.sd_tag = sides, n_sides, sd_tag  
      self.el_pid, self.sd_pid = el_pid, sd_pid

      print('')
      print ('------------------mesh elements ({s}) reading done in {s1} s------------------'.format(s = el_type, s1 = end-start))
      




      
      
      
      
     
    


    def node_flag(self):
      start = timer()
      nd_flag = [0]*self.n_nds        # first we assign nd_flag = 0 for all nodes


      # Check if mesh is 3D then loop over all surf elements and overwrite flags of nodes of an element equal to the flag of the element
      if(self.is_mesh_3d):           
        for i in range(len(self.surf)):
           for j in self.surf[i]:
              nd_flag[j-1] = self.surf_tag[i]           
                    
      # Then we loop over all line elements(2D and 3D) and overwrite flags of nodes of an element equal to the flag of the line
      for i in range(len(self.line)):
         for j in self.line[i]:
            nd_flag[j-1] = self.line_tag[i]           
      
      # Then we loop over all points(2D and 3D) and overwrite flags of nodes equal to the flag of the pt
      for i in range(len(self.pt)):
          nd_flag[self.pt[i]-1] = self.pt_tag[i]                        

      end = timer()
      print('')
      print ('------------------nodes boundary flag is set in {s1} s------------------'.format(s1 = end-start))
      self.nd_flag = nd_flag 











    def el_flag_hexa(self, i):
      flag = 0 
      tot = 0
      for j in self.els[i]:
         if(self.nd_flag[j-1]>0):  
             tot += 1
      if(tot>=4):                    # for hexa el we have quad as side
          flag = 1
      return flag     





    def el_flag_tetra_prism(self, i):
      flag = 0 
      tot = 0
      for j in self.els[i]:
         if(self.nd_flag[j-1]>0):  
             tot += 1
      if(tot>=3):                    # for tetra el we have tri as side  # for prism we have both tri and quad as sides
          flag = 1
      return flag     




    def el_flag_tri_quad(self, i):
      flag = 0 
      tot = 0
      for j in self.els[i]:
         if(self.nd_flag[j-1]>0):  
             tot += 1
      if(tot>=2):                     # for tri and quad el we have line as side
          flag = 1
      return flag     









    def element_flag(self):
      start = timer()
      bd_els = []
      el_flag = [0]*self.n_els
      if(self.el_type=='hexa'):
        for i in range(self.n_els):
           el_flag[i] = self.el_flag_hexa(i)

      elif(self.el_type=='tetra'):
        for i in range(self.n_els):
           el_flag[i] = self.el_flag_tetra_prism(i)

      elif(self.el_type=='tri' or self.el_type=='quad'):
        for i in range(self.n_els):
           el_flag[i] = self.el_flag_tri_quad(i)

      elif(self.el_type=='3dhybrid'):
        for i in range(self.n_els):
          if(len(self.els[i]) == 8):                                   el_flag[i] = self.el_flag_hexa(i) 
          elif(len(self.els[i]) == 4 or len(self.els[i]) == 6):        el_flag[i] = self.el_flag_tetra_prism(i) 
             
      elif(self.el_type=='2dhybrid'):
        for i in range(self.n_els):
          el_flag[i] = self.el_flag_tri_quad(i)               

      end = timer()
      print('')
      print ('------------------element boundary flag is set in {s1} s------------------'.format(s1 = end-start))
      print('')
      self.el_flag = el_flag 









    def metis_partition(self):
      #----------------------------------------- mesh partitioning-----------------------------------------------------------------------         
      start = timer()
      el_pid = []
      nd_pid = []
      self.createFolder('metis')
      if(self.parts != 1):
        with open('metis/input.txt', 'w') as f:
           f.write(str(self.n_els) + os.linesep)
           for i in range (self.n_els):
              for j in self.els[i]: 
                  f.write(str(j) + ' ')
              f.write(os.linesep)

        os.chdir('metis/')
        if(self.el_type=='hexa'):
               os.system('mpmetis -ncommon=4 input.txt {s}'.format(s=self.parts))
        elif(self.el_type=='tetra' or self.el_type=='prism'):
               os.system('mpmetis -ncommon=3 input.txt {s}'.format(s=self.parts))
        elif(self.el_type=='tri' or self.el_type=='quad' or self.el_type=='2dhybrid'):
               os.system('mpmetis -ncommon=2 input.txt {s}'.format(s=self.parts))              
        elif(self.el_type=='3dhybrid'):
               os.system('mpmetis -ncommon=3 input.txt {s}'.format(s=self.parts))
        else:        
            print('Error: we got in strange condition in metis partitioning')                                
            sys.exit('\n')
                   
        os.chdir('../')    
    
        with open('metis/input.txt.epart.'+str(self.parts), 'r') as f:
           for i in range (self.n_els):
              result = f.readline().strip().split()
              el_pid.append(int(result[0]))      

        with open('metis/input.txt.npart.'+str(self.parts), 'r') as f:
           for i in range (self.n_nds):
              result = f.readline().strip().split()
              nd_pid.append(int(result[0]))      

      else:
        for i in range (self.n_els):
           el_pid.append(0)
        for i in range (self.n_nds):
           nd_pid.append(0)
      os.system('rm -rf metis')     	
      end = timer()
      print ('\n------------------mesh partition by metis took {s1} s------------------'.format(s1 = end-start))
      self.el_pid = el_pid
      self.nd_pid = nd_pid
      #-----------------------------------------------------------------------------------------------------------------------         
    
    
             
             
             
             
             
      #-------------------------------------- side pid -----------------------------------------------------------------------
      start = timer()
      bd_els_sides_dict = {}     #empty dictionary
      for i in range(self.n_els):
         if(self.el_flag[i]==1):
            el_sides = self.get_el_sides(i)
            for j in range(len(el_sides)):            
               side = []
               for k in range(len(el_sides[j])):
                  side.append(self.els[i][el_sides[j][k]])                                                
               side.sort()
               bd_els_sides_dict[str(side)] = i              #generate a dictionary where key is str(list(side_nodes)) and value is element number
            
      sd_pid = []
      for i in range(self.n_sides):
        sd_pid.append(self.el_pid[bd_els_sides_dict[str(sorted(self.sides[i]))]])

      end = timer()
      print ('\n------------------side pid is set in {s1} s------------------'.format(s1 = end-start))
      self.sd_pid = sd_pid    
                           






    def get_el_sides(self, i):
      if(self.el_type=='tri'):    
          el_sides = [ [1,2], [2,0], [0,1] ]
      elif(self.el_type=='quad'):
          el_sides = [ [0,1], [1,2], [2,3], [3,0] ]
      elif(self.el_type=='tetra'):
          el_sides = [ [1,2,3], [0,3,2], [0,1,3], [0,2,1] ]
      elif(self.el_type=='hexa'):
          el_sides = [ [4,5,6,7], [0,3,2,1], [0,4,7,3], [1,2,6,5], [2,3,7,6], [1,5,4,0] ]
      elif(self.el_type=='prism'):
          el_sides = [ [1,3,2], [4,5,6], [2,3,6,5], [1,2,5,4], [1,4,6,3] ]
      elif(self.el_type=='2dhybrid'):
          if(len(self.els[i]) == 3):           el_sides = [ [1,2], [2,0], [0,1] ]
          elif(len(self.els[i]) == 4):         el_sides = [ [0,1], [1,2], [2,3], [3,0] ]          
      elif(self.el_type=='3dhybrid'):
          if(len(self.els[i]) == 4):           el_sides = [ [1,2,3], [0,3,2], [0,1,3], [0,2,1] ]          
          elif(len(self.els[i]) == 8):         el_sides = [ [4,5,6,7], [0,3,2,1], [0,4,7,3], [1,2,6,5], [2,3,7,6], [1,5,4,0] ]
          elif(len(self.els[i]) == 6):         el_sides = [ [1,3,2], [4,5,6], [2,3,6,5], [1,2,5,4], [1,4,6,3] ]
      return el_sides









    def mesh_writing_3d(self):
        f = open(self.write_mesh_name, 'w')
        f.write("MESH! 3D" + os.linesep)
        f.write("nodes " + str(self.n_nds) + os.linesep)
        f.write("elements " + str(self.n_els) + os.linesep)
        f.write("sides " + str(len(self.surf)) + os.linesep)
        f.write("X " + str(min(self.x)) + " " + str(max(self.x)) + os.linesep)
        f.write("Y " + str(min(self.y)) + " " + str(max(self.y)) + os.linesep)
        f.write("Z " + str(min(self.z)) + " " + str(max(self.z)) + os.linesep)
        for i in range (self.n_nds):  
           f.write("Node " + str(i) + " "+ str(self.x[i]) + " "+ str(self.y[i]) + " "+ str(self.z[i]) + " "+ str(self.nd_flag[i]) + os.linesep)

        if(self.el_type=='tetra'):
          for i in range (self.n_els):         self.write_tetra(i, f)
          for i in range (len(self.sides)):    self.write_tri_sd(i, f)


        elif(self.el_type=='hexa'): 
          for i in range (self.n_els):         self.write_hexa(i, f)
          for i in range (len(self.sides)):    self.write_quad_sd(i, f)


        elif(self.el_type=='prism'): 
          for i in range (self.n_els):         self.write_prism(i, f)
          for i in range (len(self.sides)):          
            if(len(self.sides[i])==4):         self.write_quad_sd(i, f)
            elif(len(self.sides[i])==3):       self.write_tri_sd(i, f)


        elif(self.el_type=='3dhybrid'):  
          for i in range (self.n_els):
            if(len(self.els[i])==8):         self.write_hexa(i, f)
            elif(len(self.els[i])==4):       self.write_tetra(i, f)
            elif(len(self.els[i])==6):       self.write_prism(i, f)
          for i in range (len(self.sides)):
            if(len(self.sides[i])==4):         self.write_quad_sd(i, f)
            elif(len(self.sides[i])==3):       self.write_tri_sd(i, f)
         
        f.close()                                









    def mesh_writing_2d(self):
        f = open(self.write_mesh_name, 'w')
        f.write("MESH! 2D" + os.linesep)
        f.write("nodes " + str(self.n_nds) + os.linesep)
        f.write("elements " + str(self.n_els) + os.linesep)
        f.write("sides " + str(len(self.line)) + os.linesep)
        f.write("X " + str(min(self.x)) + " " + str(max(self.x)) + os.linesep)
        f.write("Y " + str(min(self.y)) + " " + str(max(self.y)) + os.linesep)       
        for i in range (self.n_nds):  
           f.write("Node " + str(i) + " "+ str(self.x[i]) + " "+ str(self.y[i]) + " "+ str(self.nd_flag[i]) + os.linesep)           

        if(self.el_type=='tri'):
          for i in range (self.n_els):     self.write_tri(i, f)
          
        elif(self.el_type=='quad'): 
          for i in range (self.n_els):     self.write_quad(i, f)
        
        elif(self.el_type=='2dhybrid'):
          for i in range (self.n_els):
            if(len(self.els[i])==3):       self.write_tri(i, f)
            elif(len(self.els[i])==4):     self.write_quad(i, f)

        for i in range(len(self.sides)):   self.write_line_sd(i, f)

        f.close()                                







    def IsQuadAntiClockwise(self, el_index):
      el_nd_x = [ self.x[self.els[el_index][0]-1], self.x[self.els[el_index][1]-1], self.x[self.els[el_index][2]-1], self.x[self.els[el_index][3]-1] ]
      el_nd_y = [ self.y[self.els[el_index][0]-1], self.y[self.els[el_index][1]-1], self.y[self.els[el_index][2]-1], self.y[self.els[el_index][3]-1] ]
      var = 0.0
      for i in range(len(el_nd_x)):
        x1 = el_nd_x[i]
        y1 = el_nd_y[i]
        x2 = el_nd_x[(i+1)%len(el_nd_x)]
        y2 = el_nd_y[(i+1)%len(el_nd_x)]
        var = var + (x2-x1)*(y2+y1)
      return (var < 0.0)





    def IsTriAntiClockwise(self, el_index):
      el_nd_x = [ self.x[self.els[el_index][0]-1], self.x[self.els[el_index][1]-1], self.x[self.els[el_index][2]-1] ]
      el_nd_y = [ self.y[self.els[el_index][0]-1], self.y[self.els[el_index][1]-1], self.y[self.els[el_index][2]-1] ]                          
      var = 0.0
      for i in range(len(el_nd_x)):
        x1 = el_nd_x[i]
        y1 = el_nd_y[i]
        x2 = el_nd_x[(i+1)%len(el_nd_x)]
        y2 = el_nd_y[(i+1)%len(el_nd_x)]
        var = var + (x2-x1)*(y2+y1)
      return (var < 0.0)









    def write_tri(self, i, f):
       nds = self.els[i]
       if(self.IsTriAntiClockwise(i)):
            f.write( "Element {a} {b} 3 {c} {d} {e} {f}".format(a=i, b=self.el_pid[i], 
                                                               c=nds[0]-1, d=nds[1]-1, e=nds[2]-1, 
                                                               f=self.el_flag[i]) + os.linesep )
       else:
            f.write( "Element {a} {b} 3 {c} {d} {e} {f}".format(a=i, b=self.el_pid[i], 
                                                               c=nds[0]-1, d=nds[2]-1, e=nds[1]-1, 
                                                               f=self.el_flag[i]) + os.linesep )
       return f                                                        
    








    def write_quad(self, i, f):
       nds = self.els[i]
       if(self.IsQuadAntiClockwise(i)):
            f.write( "Element {a} {b} 4 {c} {d} {e} {f} {g}".format(a=i, b=self.el_pid[i], 
                                                                   c=nds[0]-1, d=nds[1]-1, e=nds[2]-1, f=nds[3]-1, 
                                                                   g=self.el_flag[i]) + os.linesep )
       else:
            f.write( "Element {a} {b} 4 {c} {d} {e} {f} {g}".format(a=i, b=self.el_pid[i], 
                                                                   c=nds[0]-1, d=nds[3]-1, e=nds[2]-1, f=nds[1]-1, 
                                                                   g=self.el_flag[i]) + os.linesep )
       return f                                                        










    def write_line_sd(self, i, f):
       nds = self.sides[i]
       f.write("Side {a} {b} {c} {d} {e} {f}".format(a=i, b=self.sd_pid[i], c=2, d=nds[0]-1, e=nds[1]-1, f=self.line_tag[i])  + os.linesep)        
       return f











    def write_tetra(self, i, f):
       nds = self.els[i]
       f.write( "Element {a} {b} 4 {c} {d} {e} {f} {g}".format(a=i, b=self.el_pid[i], 
                                                               c=nds[0]-1, d=nds[1]-1, e=nds[2]-1, f=nds[3]-1, 
                                                               g=self.el_flag[i]) + os.linesep)
       return f










    def write_hexa(self, i, f):
       nds = self.els[i]
       f.write( "Element {a} {b} 8 {c} {d} {e} {f} {g} {h} {i} {j} {k}".format(a=i, b=self.el_pid[i], 
                                                                               c=nds[0]-1, d=nds[1]-1, e=nds[2]-1, f=nds[3]-1, 
                                                                               g=nds[4]-1, h=nds[5]-1, i=nds[6]-1, j=nds[7]-1, 
                                                                               k=self.el_flag[i]) + os.linesep )
       return f









    def write_prism(self, i, f):
       nds = self.els[i]
       f.write( "Element {a} {b} 6 {c} {d} {e} {f} {g} {h} {i} {j} {k}".format(a=i, b=self.el_pid[i], 
                                                                               c=nds[0]-1, d=nds[1]-1, e=nds[2]-1, f=nds[3]-1, 
                                                                               g=nds[4]-1, h=nds[5]-1,  
                                                                               k=self.el_flag[i]) + os.linesep )
       return f











    def write_tri_sd(self, i, f):
       nds = self.sides[i]
       f.write("Side {a} {b} {c} {d} {e} {f} {g}".format(a=i, b=self.sd_pid[i], c=3,  d=nds[0]-1, e=nds[1]-1, f=nds[2]-1, g=self.surf_tag[i])  + os.linesep)
       return f










    def write_quad_sd(self, i, f):
       nds = self.sides[i]
       f.write("Side {a} {b} {c} {d} {e} {f} {g} {h}".format(a=i, b=self.sd_pid[i], c=3,  d=nds[0]-1, e=nds[1]-1, f=nds[2]-1, g=nds[3]-1, h=self.surf_tag[i])  + os.linesep)
       return f











    @staticmethod
    def createFolder(directory):
      try:
        if not os.path.exists(directory):
            os.makedirs(directory)
      except OSError:
        print (' ')
        print ('       Error: Creating directory. ' + directory)



    @staticmethod 
    def isclose(a, b, rel_tol=1e-09, abs_tol=0.0):
       return abs(a-b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)





    
        
         
                  





def matching_interface_nodes(f_mesh, s_mesh, out_file):
       f_nodes = []
       for i in range(f_mesh.n_sides):
         if(f_mesh.sd_tag[i] == 1):
           for nd in f_mesh.sides[i]:
              f_nodes.append(nd-1)   
       f_nodes = sorted(set(f_nodes))

       s_nodes = []
       for i in range(s_mesh.n_sides):
         if(s_mesh.sd_tag[i] == 1):
           for nd in s_mesh.sides[i]:
              s_nodes.append(nd-1)   
       s_nodes = sorted(set(s_nodes))
            
       if(len(f_nodes) != len(s_nodes)):
           print('')
           print('------------------Error: fluid and solid have different number of interface nodes i.e. meshes are not correct------------------')            
           print('------------------f_interface_nodes: {x}------------------'.format(x=len(f_nodes)))
           print('------------------s_interface_nodes: {x}------------------'.format(x=len(s_nodes)))
           print('')
           print('------------------  ERROR!!!!  ------------------')
           sys.exit('\n')            
       else:     
           print('')
           print('------------------Good: fluid and solid have same number of interface nodes:  {x} ------------------'.format(x=len(f_nodes)))            
    
    
       
       start = timer()
       f_I_nd, s_I_nd = [], []   
       for i in f_nodes: 
           for j in s_nodes:
               if(f_mesh.x[i]==s_mesh.x[j] and f_mesh.y[i]==s_mesh.y[j] and f_mesh.z[i]==s_mesh.z[j]):
                  f_I_nd.append(i)
                  s_I_nd.append(j)
                  break             
                              
       
       
       uncovered_f = []
       if(len(f_nodes) != len(f_I_nd)):
           print('')
           print('------------------Error: All fluid nodes on fluid-solid interface are not covered in f_I_nd ------------------')            
           print('------------------f_nodes: {x}------------------'.format(x=len(f_nodes)))
           print('------------------f_I_nd: {x}------------------'.format(x=len(f_I_nd)))
           print('------------------check uncovered_nodes.txt for nodes not covered ------------------')
           uncovered_f = [i for i in f_nodes if i not in f_I_nd]
           os.system('rm uncovered_nodes.txt') 
           with open('uncovered_nodes.txt', 'w') as f:
              f.write("-------------fluid-----------------" + os.linesep)       
              for i in uncovered_f:
                  f.write(str(i) + " " + str(f_mesh.x[i]) + " " + str(f_mesh.y[i]) + " " + str(f_mesh.z[i]) + os.linesep)
              f.write(os.linesep)    
    
    
    
       uncovered_s = []
       if(len(s_nodes) != len(s_I_nd)):
           print('')
           print('------------------Error: All solid nodes on fluid-solid interface are not covered in s_I_nd ------------------')            
           print('------------------s_nodes: {x}------------------'.format(x=len(s_nodes)))
           print('------------------s_I_nd: {x}------------------'.format(x=len(s_I_nd)))
           print('------------------check uncovered_nodes.txt for nodes not covered ------------------')
           uncovered_s = [i for i in s_nodes if i not in s_I_nd]       
           with open('uncovered_nodes.txt', 'a+') as f:
              f.write("-------------solid-----------------" + os.linesep)               
              for i in uncovered_s:
                  f.write(str(i) + " " + str(s_mesh.x[i]) + " " + str(s_mesh.y[i]) + " " + str(s_mesh.z[i]) + os.linesep)
                    
    
       end = timer()
       print('')
       print ('------------------ Interface matching nodes took {s1} s------------------'.format(s1 = end-start))
    
    
       with open(out_file, 'w') as f:
          f.write(str(len(f_I_nd)) + os.linesep)
          for i in range (len(f_I_nd)):
             f.write(str(f_I_nd[i]) + " " + str(s_I_nd[i]) + os.linesep)


