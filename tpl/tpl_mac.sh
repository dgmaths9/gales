#!/bin/zsh

# exit script on error
set -e

##### PARSE ARGUMENTS  ###############################################

# prints usage message
print_usage () {
    echo "Usage: $(basename $0) [OPTION...] [TARGET]..."
}

# prints usage message with option descriptions
print_help () {
cat <<- EOF
This script extracts and builds all the dependencies required to compile and run Gales. 

Available options:
  -f, --force-install      force build and istall / creation of symlink to
                           binaries for specified target(s)
  -x, --force-extraction   force extraction and installation of specified
                           target tar(s)
  -j, --parallel=          set build parallelism level
  -h, --help               show this message

Packages are only built if their installation folder is not found automatically by the
script, or if a rebuild is forced with -f.
If one or more specific packages need to be (re)built (without touching the others), 
they can be passed as targets to the script.

Available targets:
  trilinos

EOF
}


#---- PARSE COMMAND LINE  -------------------------------------------# 

# empty targets dictionary
declare -A TARGETS

# parse command line
while [[ "$#" -gt 0 ]]; do
    case "$1" in
    trilinos)
        # append target to list of targets
        TARGETS[trilinos]=true
        shift
        ;;
    -f|--force-build)
        FORCE_BUILD=true
        shift
        ;;
    -x|--force-extract)
        # force tar (re) extraction and rebuild
        FORCE_EXTRACT=true
        shift
        ;;
    -j)
        # level of parallelism for the build process
        NPARALLEL=$2
        shift 2
        ;;
    --parallel=*)
        # same as -j, different syntax
        NPARALLEL=$(echo $1 | cut -d '=' -f 2)
        shift
        ;;
    -h|--help)
        # shows help message 
        print_usage
        print_help
        exit 0
        ;;
    *)
        echo "Invalid target or option: ${target}"
        print_usage
        exit 1
        ;;
    esac
done

# handle default case with no targets (equal to all)
if [[ "${#TARGETS[@]}" -eq 0 ]]; then
    TARGETS[trilinos]=true
    INSTALL_SYS_PKGS=true
fi

# set default parallelism level to 1
if [[ "${NPARALLEL}" = "" ]]; then
    NPARALLEL=1
fi



###### SCRIPT VARIABLES CONFIGURATION ################################

# cd in script directory (tpl directory)
cd $(dirname $0)

# global directories
TPL_ROOT_DIR="$(pwd)"
TPL_TAR_DIR="${TPL_ROOT_DIR}/tars"
TPL_SRC_DIR="${TPL_ROOT_DIR}/src"
TPL_LIB_DIR="${TPL_ROOT_DIR}/tpl"
LOGFILE="${TPL_ROOT_DIR}/log.txt"

# check existence of src directory
if [[ ! -d "${TPL_SRC_DIR}" ]]; then
    mkdir "${TPL_SRC_DIR}"
fi

# check existence of tpl directory
if [[ ! -d "${TPL_LIB_DIR}" ]]; then
    mkdir "${TPL_LIB_DIR}"
fi

# delete and recreate logfile if already existing
rm -f "${LOGFILE}" && touch "${LOGFILE}"


#---- TARGET DIRECTORIES --------------------------------------------#

TRILINOS_TARBALL="${TPL_TAR_DIR}/Trilinos-14.tar.gz"
TRILINOS_EXTRACT_DIR="${TPL_SRC_DIR}/Trilinos-trilinos-release-14-0-0"
TRILINOS_INSTALL_DIR="${TPL_LIB_DIR}/trilinos-14.0.0"



##### EXTRACT AND BUILD PACKAGES #####################################

# this function checks if extraction is required, then
# removes previous extraction and extracts tar archive 
extract_tar_checked () {
    echo "[TPL_INSTALL_LOG] extracting $(basename $1)..." | \
        tee -a "${LOGFILE}"
    # check if extraction is required
    if [[ ! -d "${2}" ]] || \
       [[ "${FORCE_EXTRACT}" = true ]]
    then
        # extract (or re-extract)
        rm -rf "${2}"                       # remove extraction dir if present
        tar -C "${TPL_SRC_DIR}" -xf "${1}"  # extract package
    else 
        echo "[TPL_INSTALL_LOG] $(basename $1) extraction found" | \
            tee -a "${LOGFILE}"
    fi
}


# first log message
echo "[TPL_INSTALL_LOG] start tpl installation"


#---- BREW DEPENDENCIES ---------------------------------------------#

if [[ "${INSTALL_SYS_PKGS}" = true ]]; then

echo "[TPL_INSTALL_LOG] installing gcc..."
brew install gcc | \
    tee -a "${LOGFILE}"

echo "[TPL_INSTALL_LOG] installing cmake..."
brew install cmake | \
    tee -a "${LOGFILE}"

echo "[TPL_INSTALL_LOG] installing openmpi..."
brew install open-mpi | \
    tee -a "${LOGFILE}"
 
echo "[TPL_INSTALL_LOG] installing openblas..."
brew install openblas | \
    tee -a "${LOGFILE}"

echo "[TPL_INSTALL_LOG] installing metis..."
brew install metis | \
    tee -a "${LOGFILE}"

echo "[TPL_INSTALL_LOG] installing gmsh..."
brew install gmsh | \
    tee -a "${LOGFILE}"

echo "[TPL_INSTALL_LOG] installing boost..."
brew install boost | \
    tee -a "${LOGFILE}"

fi


#---- SET NONSTANDARD LIBRARY PATHS FOR COMPILATION------------------#

# set path to installed openblas
# run "brew info openblas" to get
# the installation path for the libs
OPENBLAS_ROOT="/usr/local/opt/openblas"


#---- TRILINOS ------------------------------------------------------#
if [[ "${TARGETS[trilinos]}" = true ]]; then

    extract_tar_checked "${TRILINOS_TARBALL}" "${TRILINOS_EXTRACT_DIR}"

    echo "[TPL_INSTALL_LOG] building trilinos..." | \
        tee -a "${LOGFILE}"
    # check if build is required
    if [[ ! -d "${TRILINOS_INSTALL_DIR}" ]] || \
       [[ "${FORCE_BUILD}" = true ]]
    then
        # build (or rebuild)
        cd "${TRILINOS_EXTRACT_DIR}"
        rm -rf "${TRILINOS_INSTALL_DIR}"  # clean previous install
        rm -rf build                      # clean previous build
        mkdir -p build && cd build        # create build directory
       
        cmake \
            -DTrilinos_ENABLE_EpetraExt=ON \
            -DTrilinos_ENABLE_Epetra=ON \
            -DTrilinos_ENABLE_Belos=ON \
            -DTrilinos_ENABLE_Teuchos=ON \
            -DTrilinos_ENABLE_Ifpack=ON \
            -DTrilinos_ENABLE_Kokkos=OFF \
            -DTrilinos_ENABLE_ALL_OPTIONAL_PACKAGES=ON \
            -DTrilinos_ENABLE_EXPLICIT_INSTANTIATION=ON \
            -DTrilinos_ENABLE_DEBUG_SYMBOLS=ON \
            -DTrilinos_ENABLE_TESTS=ON \
            -DTPL_ENABLE_MPI=ON \
            -DTPL_ENABLE_Boost=ON \
            -DBLAS_LIBRARY_NAMES="openblas" \
            -DBLAS_LIBRARY_DIRS="${OPENBLAS_ROOT}/lib" \
            -DLAPACK_LIBRARY_NAMES="" \
            -DLAPACK_LIBRARY_DIRS="${OPENBLAS_ROOT}/lib" \
            -DCMAKE_BUILD_TYPE=RELEASE \
            -DCMAKE_CXX_STANDARD=17 \
            -DBUILD_SHARED_LIBS=ON \
            -DCMAKE_BUILD_WITH_INSTALL_NAME_DIR=TRUE \
            -DCMAKE_INSTALL_NAME_DIR="${TRILINOS_INSTALL_DIR}/lib" \
            -DCMAKE_INSTALL_PREFIX="${TRILINOS_INSTALL_DIR}" \
            .. | \
            tee -a "${LOGFILE}"

        cmake --build . -j "${NPARALLEL}" --target install | \
            tee -a "${LOGFILE}"

        echo -e "\n\n" | tee -a "${LOGFILE}"
    else
        echo "[TPL_INSTALL_LOG] trilinos installation found" | \
            tee -a "${LOGFILE}"
    fi
   
fi # end trilinos block



##### GENERATE ENVIRONMENT FILE #####################################

# set envfile name
ENVFILE="${TPL_ROOT_DIR}/setenv_gales"
MISSING=()

# remove old env file
rm -rf "${ENVFILE}"

# add built trilinos directories to path
if [[ -d "${TRILINOS_INSTALL_DIR}" ]]; then
cat <<- EOF >> "${ENVFILE}"
# adding trilinos directories to paths
export TRILINOS_ROOT=${TRILINOS_INSTALL_DIR}
export TRILINOS_INCLUDE=${TRILINOS_INSTALL_DIR}/include
export TRILINOS_INC=${TRILINOS_INSTALL_DIR}/include
export TRILINOS_LIB=${TRILINOS_INSTALL_DIR}/lib
export Trilinos_DIR=${TRILINOS_INSTALL_DIR}/lib/cmake
EOF
echo -e "\n" >> "${ENVFILE}"
else 
MISSING+=('trilinos')
fi 

# export gales directories
cat <<- EOF >> "${ENVFILE}"
# adding gales utilities to path
export TPL_PATH=${TPL_ROOT_DIR} 
export PATH=${TPL_ROOT_DIR}/gales_mesh_preprocessing:\$PATH
export PATH=${TPL_ROOT_DIR}/gales_results_postprocessing:\$PATH
export PATH=${TPL_ROOT_DIR}/Mogi_model:\$PATH
export PATH=${TPL_ROOT_DIR}/solwcad:\$PATH 
export PYTHONPATH=${TPL_ROOT_DIR}/gales_results_postprocessing:\$PYTHONPATH
export PYTHONPATH=${TPL_ROOT_DIR}/Mogi_model:\$PYTHONPATH
EOF

# echo final message
echo -e "\nEnvironment file generated at: ${ENVFILE}\n"
if [[ ! "${#MISSING[@]}" -eq 0 ]]; then
    echo -e "WARNING: incomplete environment file generated, missing targets:\n"
    for target in "${MISSING[@]}"; do
        echo -n " $target,"
    done
    echo -e "\n\nBuild the missing targets before sourcing the environment file\n"
fi
