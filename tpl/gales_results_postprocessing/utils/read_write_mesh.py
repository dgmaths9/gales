

import os




# Functions to read GALES mesh format
#------------------------------------------------------------------------------------------------
def read_header(f):
    dim = 2
    split_result = ((f.readline()).strip()).split()
    if(split_result[1]=='3D'):
       dim = 3
    split_result = ((f.readline()).strip()).split()
    n_nodes = int(split_result[1])
    split_result = ((f.readline()).strip()).split()
    n_els = int(split_result[1])
    split_result = ((f.readline()).strip()).split()
    n_sides = int(split_result[1])
    f.readline() 
    f.readline()
    if(dim==3):
       f.readline()     
    return n_nodes, n_els, dim










def read_nodes(f, n_nodes, dim):
    x, y, z = [], [], []    
    if(dim==2):
        for i in range (n_nodes):
           split_result = ((f.readline()).strip()).split()
           x.append(float(split_result[2]))  
           y.append(float(split_result[3]))
           z.append(0)
    else:
        for i in range (n_nodes):
           split_result = ((f.readline()).strip()).split()
           x.append(float(split_result[2]))  
           y.append(float(split_result[3]))
           z.append(float(split_result[4]))
    return x, y, z










def read_cells(f, n_els, dim):
    surf, vol = [], []
    if(dim==2):
        for i in range (n_els):
           split_result = ((f.readline()).strip()).split()
           if(split_result[3]=='3'):
              surf.append([int(split_result[4]), int(split_result[5]), int(split_result[6])])
           elif(split_result[3]=='4'):   
              surf.append([int(split_result[4]), int(split_result[5]), int(split_result[6]), int(split_result[7])])
           else:
              print('-----------------Error----------------------------------------------------------------') 
              print('2D mesh el has ', split_result[3], ' num of nodes')
              sys.exit('\n')   
    else:
        for i in range (n_els):
           split_result = ((f.readline()).strip()).split()
           if(split_result[3]=='4'):
              vol.append([int(split_result[4]), int(split_result[5]), int(split_result[6]), int(split_result[7])])
           elif(split_result[3]=='8'):   
              vol.append([int(split_result[4]), int(split_result[5]), int(split_result[6]), int(split_result[7]), 
                          int(split_result[8]), int(split_result[9]), int(split_result[10]), int(split_result[11])])           
           elif(split_result[3]=='6'):   
              vol.append([int(split_result[4]), int(split_result[5]), int(split_result[6]), int(split_result[7]), 
                          int(split_result[8]), int(split_result[9])])
           else:
              print('-----------------Error----------------------------------------------------------------') 
              print('£D mesh el has ', split_result[3], ' num of nodes')
              sys.exit('\n')   
                                     
    return surf, vol

#------------------------------------------------------------------------------------------------






















# Functions to write in vtk mesh format

#------------------------------------------------------------------------------------------------

def write_header(f, n_nodes):
    f.write('# vtk DataFile Version 2.0' + os.linesep)
    f.write("mesh in vtk" + os.linesep)
    f.write('ASCII' + os.linesep)
    f.write('DATASET UNSTRUCTURED_GRID' + os.linesep)
    f.write('POINTS {s1} double'.format(s1=n_nodes) + os.linesep)
 







def write_nodes(f, n_nodes, x, y, z):
    for i, j, k in zip(x, y, z):
        f.write('%.6f  %.6f  %.6f\n' % (i, j, k))
    f.write(os.linesep)










def write_cells(f, n_els, dim, surf, vol):
    
    n = len(surf)
    for x in surf:
       n += len(x)     
    
    if(dim==2):    
           f.write('CELLS {s1} {s2}'.format(s1=n_els, s2=n) + os.linesep)

           for x in surf:
             if(len(x)==3):
                f.write("{} {} {} {}".format(str(3), str(x[0]), str(x[1]), str(x[2])) + os.linesep )              
             elif(len(x)==4):
                f.write("{} {} {} {} {}".format(str(4), str(x[0]), str(x[1]), str(x[2]), str(x[3])) + os.linesep )              
           f.write(os.linesep)   

           f.write('CELL_TYPES {s1}'.format(s1=n_els) + os.linesep)           
           for x in surf:
             if(len(x)==3):
               f.write(str(5)+os.linesep)
             elif(len(x)==4): 
               f.write(str(9)+os.linesep)

    else:
           f.write('CELLS {s1} {s2}'.format(s1=n_els, s2=5*n_els) + os.linesep)

           for x in vol:
             if(len(x)==4):
               f.write("{} {} {} {} {}".format(str(4), str(x[0]), str(x[1]), str(x[2]), str(x[3])) + os.linesep )              
             elif(len(x)==8):
               f.write("{} {} {} {} {} {} {} {} {}".format(str(8), str(x[0]), str(x[1]), str(x[2]), str(x[3]), str(x[4]), str(x[5]), str(x[6]), str(x[7])) + os.linesep )              
             elif(len(x)==6):
               f.write("{} {} {} {} {} {} {} {} {}".format(str(6), str(x[0]), str(x[1]), str(x[2]), str(x[3]), str(x[4]), str(x[5])) + os.linesep )              
           f.write(os.linesep)   
           
           f.write('CELL_TYPES {s1}'.format(s1=n_els) + os.linesep)           
           for x in vol:
             if(len(x)==4):
               f.write(str(10)+os.linesep)
             elif(len(x)==8):
               f.write(str(12)+os.linesep)
             elif(len(x)==6):
               f.write(str(13)+os.linesep)
#------------------------------------------------------------------------------------------------
            





