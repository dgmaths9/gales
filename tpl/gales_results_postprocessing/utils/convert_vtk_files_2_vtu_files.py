#!/usr/bin/python3


import os
import sys
import numpy as np
import time
import glob
import concurrent.futures
import pp_utils_ex as ex




# This file is to covert .vtk files to .vtu files and to write a data.pvd file
# This file must be run from  vtk_data directory



def rename_file(name):
    if(len(name)-9 == 8 and name[0:9] == 'file_0000'):
        os.system('mv {} {}'.format(name, str(int(name[9:13]))+'.vtk'))
    elif(len(name)-9 == 9 and name[0:10] == 'file_00000'):
        os.system('mv {} {}'.format(name, str(int(name[10:14]))+'.vtk'))






# This converts vtk files into binary compressed vtu format and writes a pvd file for paraview.
def write_pvd_file():     
           
    # ------ list of times to be processed-------------------------------- 
    f_list = os.listdir('../fluid_dofs/')
    
    files_list = [i[11:] for i in f_list if(i[0:11] == 'fluid_dofs_')]
    files_list_float = [float(i) for i in files_list]
    files_list_float.sort()
    times = [ex.formatted_time(i) for i in files_list_float]
    print('times = ', times)
    


    #--------------convert vtk to vtu files multi processing----------------
    vtk_old_files_list = [f for f in glob.glob('*.vtk')]            
    if(len(times) != len(vtk_old_files_list)):
       print("length of times  !=  length of vtk_files")
       sys.exit()
        
    for f in vtk_old_files_list:
         rename_file(f)    
    vtk_files = [f for f in glob.glob('*.vtk')]        
    print('vtk_files = ', vtk_files)
    

    with concurrent.futures.ProcessPoolExecutor() as executor:
        executor.map(ex.vtk2vtu_convert, vtk_files)
                        
    os.system('rm *.vtk')
       
    vtufiles_index_list = [int(f[:-4]) for f in glob.glob('*.vtu')]    
    vtufiles_index_list.sort()
    vtufiles = [str(i)+'.vtu' for i in vtufiles_index_list]   


    #--------------generating .pvd file for paraview----------------    
    with open('data.pvd', 'w') as f:
       f.write('<VTKFile type="Collection" version="0.1" byte_order="LittleEndian">' + os.linesep)
       f.write('    <Collection>' + os.linesep)
       for i in range(len(times)):
          f.write('      <DataSet timestep="{s1}"         file="{s2}"/>'.format(s1=times[i], s2=vtufiles[i]) + os.linesep)    
       f.write('    </Collection>' + os.linesep)
       f.write('</VTKFile>' + os.linesep)


   


write_pvd_file()
os.system('rm -rf ../data')       #removing un needed folders
#os.system('paraview data.pvd')



