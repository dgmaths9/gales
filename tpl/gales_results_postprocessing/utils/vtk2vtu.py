

import sys
from paraview.simple import *





def execute(f):	
  a1vtk = LegacyVTKReader(FileNames=[f])
  renderView1 = GetActiveViewOrCreate('RenderView')
  a1vtkDisplay = Show(a1vtk, renderView1, 'UnstructuredGridRepresentation')
  renderView1.Update()
  SaveData(f[:-1]+'u', proxy=a1vtk, CompressorType='ZLib')





execute(sys.argv[1])        




