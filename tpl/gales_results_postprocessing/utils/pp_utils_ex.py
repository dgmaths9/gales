#!/usr/bin/python3


import os
import numpy as np
import glob
import concurrent.futures
import utils.read_write_mesh as reader_writer
import json
import decimal




def createFolder(directory):
    try:
        if not os.path.exists(directory):
            os.makedirs(directory)
    except OSError:
        print('Error: Creating {s1}'.format(s1=directory))







#  This formats the file name for floas and integers
def formatted_time(f):
    f_name = '%d' if f.is_integer() else '%s'
    return (f_name % f)

 
 
 

def check_file_path(file_path):
  if(os.path.isfile(file_path) == False):
    print('Error: file {s1} does not exist'.format(s1=file_path))
    exit()




def check_dir_path(dir_path):
  if(os.path.isdir(dir_path) == False):
    print('Error: directory {s1} does not exist'.format(s1=dir_path))
    exit()




#def float_to_decimal(f):
#    # http://docs.python.org/library/decimal.html#decimal-faq
#    "Convert a floating point number to a Decimal with no loss of information"
#    n, d = f.as_integer_ratio()
#    numerator, denominator = decimal.Decimal(n), decimal.Decimal(d)
#    ctx = decimal.Context(prec=60)
#    result = ctx.divide(numerator, denominator)
#    while ctx.flags[decimal.Inexact]:
#        ctx.flags[decimal.Inexact] = False
#        ctx.prec *= 2
#        result = ctx.divide(numerator, denominator)
#    return result 




#def f(number, sigfig):
#    # http://stackoverflow.com/questions/2663612/nicely-representing-a-floating-point-number-in-python/2663623#2663623
#    assert(sigfig>0)
#    try:
#        d=decimal.Decimal(number)
#    except TypeError:
#        d=float_to_decimal(float(number))
#    sign,digits,exponent=d.as_tuple()
#    if len(digits) < sigfig:
#        digits = list(digits)
#        digits.extend([0] * (sigfig - len(digits)))    
#    shift=d.adjusted()
#    result=int(''.join(map(str,digits[:sigfig])))
#    # Round the result
#    if len(digits)>sigfig and digits[sigfig]>=5: result+=1
#    result=list(str(result))
#    # Rounding can change the length of result
#    # If so, adjust shift
#    shift+=len(result)-sigfig
#    # reset len of result to sigfig
#    result=result[:sigfig]
#    if shift >= sigfig-1:
#        # Tack more zeros on the end
#        result+=['0']*(shift-sigfig+1)
#    elif 0<=shift:
#        # Place the decimal point in between digits
#        result.insert(shift+1,'.')
#    else:
#        # Tack zeros on the front
#        assert(shift<0)
#        result=['0.']+['0']*(-shift-1)+result
#    if sign:
#        result.insert(0,'-')
#    return ''.join(result)






# This gets mesh name from mesh folder
def get_mesh_name():
    mesh_dir_path = 'mesh/'
    check_dir_path(mesh_dir_path)    
    
    files_list = os.listdir(mesh_dir_path)
    if(len(files_list)==0):
       print('Error: mesh directory is empty')
       exit()
       
    files_list_txt = [i for i in files_list if '.txt' in i]
    if(len(files_list_txt)==0):
       print('Error: in mesh directory there is no file with ".txt" prefix')
       exit()
    
    mesh_name = files_list_txt[0]
    print('----------------------------------')
    print('mesh name: ', mesh_name)
    print('---------------------------------')
    return mesh_name






# This gets mesh name from mesh folder starting with s = {'f_mesh', 's_mesh', 'h_mesh', ...}
def get_mesh_name_by_prefix(s):
    mesh_dir_path = 'mesh/'
    check_dir_path(mesh_dir_path)    

    files_list = os.listdir(mesh_dir_path)
    if(len(files_list)==0):
       print('Error: mesh directory is empty')
       exit()

    files_list_txt = [i for i in files_list if '.txt' in i]
    files_list_txt = [i for i in files_list_txt if s in i]     
    if(len(files_list_txt)==0):
       print('Error: in mesh directory there is no file with ".txt" prefix')
       exit()

    mesh_name = files_list_txt[0]
    print('----------------------------------')
    print('mesh name: ', mesh_name)
    print('---------------------------------')
    return mesh_name







# This reads the GALES mesh format and write it in VTK format
def read_write_mesh(mesh_name, out_mesh_name = 'mesh.vtk'):
    #------------------reading part---------------------------------------
    with open('mesh/{s1}'.format(s1=mesh_name),'r') as f:
       n_nodes, n_els, dim = reader_writer.read_header(f)
       x, y, z = reader_writer.read_nodes(f, n_nodes, dim)
       surf, vol = reader_writer.read_cells(f, n_els, dim)
    
    #------------------writing part---------------------------------------
    if os.path.isfile('mesh/' + out_mesh_name): 
       print(out_mesh_name + ' exists') 
    else:   
       with open('mesh/' + out_mesh_name, 'w') as f:
          reader_writer.write_header(f, n_nodes)
          reader_writer.write_nodes(f, n_nodes, x, y, z)
          reader_writer.write_cells(f, n_els, dim, surf, vol)            
    return n_nodes








def mesh_n_nodes(mesh_name):
    with open('mesh/{s1}'.format(s1=mesh_name),'r') as f:
       n_nodes, n_els, dim = reader_writer.read_header(f)
    return  n_nodes




def mesh_n_els(mesh_name):
    with open('mesh/{s1}'.format(s1=mesh_name),'r') as f:
       n_nodes, n_els, dim = reader_writer.read_header(f)
    return  n_els






# This reads the GALES mesh format and write it in VTK format for moving meshes; It does not read(write) the mesh nodes.
def read_write_mesh_for_moving_mesh(mesh_name):
    #-----------reading---------------------------------
    with open('mesh/{s1}'.format(s1=mesh_name),'r') as f:
       n_nodes, n_els, dim = reader_writer.read_header(f)
       for i in range (n_nodes):
           f.readline()         
       surf, vol = reader_writer.read_cells(f, n_els, dim)

    #-----------writing---------------------------------
    with open('f_data/mesh_pre1', 'w') as f:
       reader_writer.write_header(f, n_nodes)

    with open('f_data/mesh_pre2', 'w') as f:
       reader_writer.write_cells(f, n_els, dim, surf, vol)

    return n_nodes












#----------------------variable prefixes-----------------------------
def prefix(file_name, n_nodes, variable, s, WRITE_POINT_DATA=False):    
    with open(file_name, 'w') as f:
       f.write(os.linesep)
       if(WRITE_POINT_DATA):
          f.write('POINT_DATA {s1}'.format(s1=n_nodes) + os.linesep)
       if(s=='SCALAR'):
          f.write('SCALARS {s1} float'.format(s1=variable) + os.linesep)
          f.write('LOOKUP_TABLE default' + os.linesep)  
       else:
          f.write('VECTORS {s1} float'.format(s1=variable) + os.linesep)











#----------------get last postprocessed time---------------
def get_last_pp_time(s):
  if(os.path.isfile(s)):
    with open(s) as f:
      if(s[-3:] == "txt"):
         split_result = f.readline().strip().split()
         last_pp_time = float(split_result[0])
      else:        
         vtk_data = json.load(f)     
         last_pp_time = vtk_data['files'][-1]['time'] 

  else:
      last_pp_time = 0
  print('last_pp_time = ', last_pp_time)    
  return  last_pp_time






#--------------list of times to be postprocessed----------------
def list_of_times_to_pp(data_path, last_pp_time, start, end, step, adaptive, gap):
    print("start: ", start, "  end: ", end, " step: ", step)
    print("adaptive: ", adaptive)    

    if(adaptive == 'y'):
        files_list = os.listdir(data_path)        
        files_list_float = [float(i) for i in files_list if float(i) >= last_pp_time]
        files_list_float.sort()
        times = [formatted_time(i) for i in files_list_float] 
        print("time: ", times[::gap])
        return times[::gap]
    else:
        times = []        
        for i in range(int((end-start)/step + 1)):
             time = start + i*step
             time = round(time, 9)
             time = formatted_time(time)
             if os.path.isfile(data_path + time):
               times.append(time)
        print("time: ", times)                
        return times
          












#------------------nb of dofs---------------------------------
def nb_of_dofs(data_path, n_nodes):
    with open(data_path, 'rb') as fid:
        load_var = np.fromfile(fid, dtype=float)
    nb_dofs = int(round(len(load_var)/n_nodes))
    return nb_dofs












#--------------convert vtk to vtu file format; This invokes vtk2vtu.py file with pvpython----------------    
def vtk2vtu_convert(f):
    path_of_vtk2vtu_file = os.path.abspath(os.path.dirname(__file__))    
    os.system('pvpython {s1}/vtk2vtu.py {s2}'.format(s1=path_of_vtk2vtu_file, s2=f))
    print('wrote {s1}u'.format(s1=str(f[:-1]))) 











# This converts vtk files into binary compressed vtu format and writes a pvd file for paraview.
def write_pvd_file(times):     

           
    #--------------convert vtk to vtu files multi processing----------------
    vtk_files = [f for f in glob.glob('*.vtk')]

    if(len(vtk_files)==0):
       print('Error: There are no vtk files to convert to vtu format in vtk_data directory')
       exit()
            
    with concurrent.futures.ProcessPoolExecutor() as executor:
        executor.map(vtk2vtu_convert, vtk_files)
                        

    os.system('rm *.vtk')


    #--------------write last processed time in a file for record of already postprocessed data--------------
    with open('prev.txt', 'w') as f:
       f.write(times[-1] + os.linesep)
    
           


    #--------------generating .pvd file for paraview----------------    
    with open('part1.txt', 'w') as f:
       f.write('<VTKFile type="Collection" version="0.1" byte_order="LittleEndian">' + os.linesep)
       f.write('    <Collection>' + os.linesep)
   
    with open('part2.txt', 'a+') as f:
       for i in range(len(times)):
          f.write('      <DataSet timestep="{s1}"         file="sol_{s1}.vtu"/>'.format(s1=times[i]) + os.linesep)
    
    with open('part3.txt', 'w') as f:
       f.write('    </Collection>' + os.linesep)
       f.write('</VTKFile>' + os.linesep)
       f.close()      
    
    if(os.path.isfile('data.pvd')): 
       os.system('rm data.pvd')
       
    os.system('cat part1.txt  part2.txt part3.txt > data.pvd')     
    os.system('rm part1.txt part3.txt')
    os.chdir('..')











# This vtk series file for paraview.
def write_vtk_series(times):                       
    
    with open('part1.txt', 'w') as f:
       f.write('{' + os.linesep)
       f.write('   "file-series-version" : "1.0",' + os.linesep)
       f.write('   "files" : [' + os.linesep)

    if(os.path.isfile('part2.txt')):     
      with open('part2.txt', 'a+') as f:
        f.write(',')
        f.write(os.linesep)                      
        for i in range(len(times)-1):
          f.write('{ "name" : "sol_' + times[i] + '.vtk", "time" : ' + times[i] + ' },' + os.linesep)
        f.write('{ "name" : "sol_' + times[-1] + '.vtk", "time" : ' + times[-1] + ' }')
    else:
      with open('part2.txt', 'w') as f:
        for i in range(len(times)-1):
          f.write('{ "name" : "sol_' + times[i] + '.vtk", "time" : ' + times[i] + ' },' + os.linesep)
        f.write('{ "name" : "sol_' + times[-1] + '.vtk", "time" : ' + times[-1] + ' }')
    


    with open('part3.txt', 'w') as f:
       f.write(os.linesep)
       f.write('   ]' + os.linesep)
       f.write('}' + os.linesep)
       f.close()      

    
    if(os.path.isfile('data.vtk.series')): 
       os.system('rm data.vtk.series')
       
    os.system('cat part1.txt  part2.txt part3.txt > data.vtk.series')     
    os.system('rm part1.txt part3.txt')
    os.chdir('..')






