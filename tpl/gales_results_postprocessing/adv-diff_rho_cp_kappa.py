#!/usr/bin/python3

import os
import sys
import numpy as np
import time
import concurrent.futures
import utils.pp_utils_ex as ex



start, end, step, adaptive, gap, vtk_type = float(sys.argv[1]), float(sys.argv[2]), float(sys.argv[3]), sys.argv[4], int(sys.argv[5]), sys.argv[6]


ex.createFolder('Y_data')
ex.createFolder('vtk_data')



data_path = 'Y/'


mesh_name = ex.get_mesh_name()
n_nodes = ex.read_write_mesh(mesh_name)

ex.prefix('Y_data/Y_prefix', n_nodes, 'Y', 'SCALAR', True)
ex.prefix('Y_data/rho_prefix', n_nodes, 'rho', 'SCALAR')
ex.prefix('Y_data/cp_prefix', n_nodes, 'cp', 'SCALAR')
ex.prefix('Y_data/kappa_prefix', n_nodes, 'kappa', 'SCALAR')


if(vtk_type == "vtu"):   last_pp_time = ex.get_last_pp_time('vtk_data/prev.txt')
else:     last_pp_time = ex.get_last_pp_time('vtk_data/data.vtk.series')


times = ex.list_of_times_to_pp(data_path, last_pp_time, start, end, step, adaptive, gap)



#--------------writing data in vtk file format----------------
def vtk_file_write(t):
      file_name = data_path + t
      with open(file_name,'rb') as fid:
         Y = np.fromfile(fid, dtype=float)

      with open('sec_dofs/rho/' + t, 'rb') as fid:
         rho = np.fromfile(fid, dtype=float)
   
      with open('sec_dofs/cp/' + t, 'rb') as fid:
         cp = np.fromfile(fid, dtype=float)

      with open('sec_dofs/kappa/' + t, 'rb') as fid:
         kappa = np.fromfile(fid, dtype=float)

      print ('Processing at ', t)
      np.savetxt('Y_data/Y_{s1}'.format(s1=t), Y)
      np.savetxt('Y_data/rho_{s1}'.format(s1=t), rho)
      np.savetxt('Y_data/cp_{s1}'.format(s1=t), cp)
      np.savetxt('Y_data/kappa_{s1}'.format(s1=t), kappa)
      s = 'mesh/mesh.vtk Y_data/Y_prefix Y_data/Y_{s2} '.format(s2=t)
      s += 'Y_data/rho_prefix Y_data/rho_{s2} '.format(s2=t)
      s += 'Y_data/cp_prefix Y_data/cp_{s2} '.format(s2=t)
      s += 'Y_data/kappa_prefix Y_data/kappa_{s2} '.format(s2=t)

      vtk_file = 'vtk_data/sol_{}.vtk'.format(t)
      os.system('cat {s1} > {s2}'.format(s1=s, s2=vtk_file))




#------------------execute multi processing--------------------------
with concurrent.futures.ProcessPoolExecutor() as executor:
    executor.map(vtk_file_write, times)




os.chdir('vtk_data/')

if(vtk_type == "vtu"):  
   ex.write_pvd_file(times)
   os.system('rm -rf Y_data')       #removing un needed folders
   os.system('paraview vtk_data/data.pvd')
else: 
   ex.write_vtk_series(times)
   os.system('rm -rf Y_data')       #removing un needed folders
   os.system('paraview vtk_data/data.vtk.series')


