#!/usr/bin/python3

import os
import sys
import numpy as np
import time
import concurrent.futures
import utils.pp_utils_ex as ex




start, end, step, adaptive, gap, isothermal, vtk_type = float(sys.argv[1]), float(sys.argv[2]), float(sys.argv[3]), sys.argv[4], int(sys.argv[5]), sys.argv[6], sys.argv[7]

ex.createFolder('f_data')
ex.createFolder('s_data')

f_data_path = 'fluid_dofs/'
f_mesh_path = 'fluid_mesh/'
s_data_path_u = 'solid/u/'
s_data_path_v = 'solid/v/'
s_data_path_a = 'solid/a/'


if(vtk_type == "vtu"):   last_pp_time = ex.get_last_pp_time('f_vtk_data/prev.txt')
else:     last_pp_time = ex.get_last_pp_time('f_vtk_data/data.vtk.series')



times = ex.list_of_times_to_pp(f_data_path, last_pp_time, start, end, step, adaptive, gap)





#--------------------------------------------------------fluid----------------------------------------------------
f_mesh_name = ex.get_mesh_name_by_prefix('f_mesh')
f_n_nodes = ex.read_write_mesh_for_moving_mesh(f_mesh_name)
f_nb_dofs = ex.nb_of_dofs(f_data_path + times[0], f_n_nodes)
ex.prefix('f_data/p_prefix', f_n_nodes, 'p', 'SCALAR', True)
ex.prefix('f_data/dp_prefix', f_n_nodes, 'dp', 'SCALAR')
ex.prefix('f_data/T_prefix', f_n_nodes, 'T', 'SCALAR')
ex.prefix('f_data/v_prefix', f_n_nodes, 'v', 'VECTOR')
ex.createFolder('f_vtk_data')



with open(f_data_path + '0','rb') as fid:
    load_var = np.fromfile(fid, dtype=float)
p0 = load_var[0::f_nb_dofs]


def f_var_files(load_var, xyz_data, t):
      p = load_var[0::f_nb_dofs]
      dp = p-p0
      v1 = load_var[1::f_nb_dofs]
      v2 = load_var[2::f_nb_dofs]
      np.savetxt('f_data/p_{s1}'.format(s1=t), p)
      np.savetxt('f_data/dp_{s1}'.format(s1=t), dp)


      if(isothermal == 'y'):
         if(f_nb_dofs==3): 
            v3 = np.zeros(f_n_nodes)
         elif(f_nb_dofs==4):
            v3 = load_var[3::f_nb_dofs]

      elif(isothermal == 'n'):
         if(f_nb_dofs==4): 
            v3 = np.zeros(f_n_nodes)
            T = load_var[3::f_nb_dofs]
         elif(f_nb_dofs==5):
            v3 = load_var[3::f_nb_dofs]
            T = load_var[4::f_nb_dofs]            
         np.savetxt('f_data/T_{s1}'.format(s1=t), T)
               
      v = np.array([v1, v2, v3])
      np.savetxt('f_data/v_{s1}'.format(s1=t), v.T)


      if(nb_dofs==3):
            x = xyz_data[0::2]
            y = xyz_data[1::2]   
            z = np.zeros(n_nodes)
      elif(nb_dofs==4):
         if(isothermal == 'y'):
            x = xyz_data[0::3]
            y = xyz_data[1::3]
            z = xyz_data[2::3]
         else:
            x = xyz_data[0::2]
            y = xyz_data[1::2]   
            z = np.zeros(n_nodes)
      elif(nb_dofs==5):
            x = xyz_data[0::3]
            y = xyz_data[1::3]
            z = xyz_data[2::3]


      xyz = np.array([x, y, z])
      np.savetxt('f_data/mesh_{s1}'.format(s1=t), xyz.T)

      s = 'f_data/mesh_pre1  f_data/mesh_{s1} f_data/mesh_pre2 '.format(s1=t)
      s += 'f_data/{s1}_prefix f_data/{s1}_{s2} '.format(s1='p', s2=t)
      s += 'f_data/{s1}_prefix f_data/{s1}_{s2} '.format(s1='dp', s2=t)
      if(isothermal == 'n'):  s += 'f_data/{s1}_prefix f_data/{s1}_{s2} '.format(s1='T', s2=t)
      s += 'f_data/{s1}_prefix f_data/{s1}_{s2}'.format(s1='v', s2=t)
      return s


#--------------writing data in vtk file format----------------    
def f_vtk_file_write(t):
      with open(f_data_path + t, 'rb') as fid:
         load_var = np.fromfile(fid, dtype=float)
      with open(f_mesh_path + t, 'rb') as fid:
         xyz_data = np.fromfile(fid, dtype=float)
   
      print ('Processing f at ', t)
      s = f_var_files(load_var, xyz_data, t)      

      vtk_file = 'f_vtk_data/sol_{}.vtk'.format(t)
      os.system('cat {s1} > {s2}'.format(s1=s, s2=vtk_file))


#------------------execute multi processing--------------------------
with concurrent.futures.ProcessPoolExecutor() as executor:
    executor.map(f_vtk_file_write, times)
    
os.chdir('f_vtk_data/')

if(vtk_type == "vtu"):     ex.write_pvd_file(times)
else:    ex.write_vtk_series(times)

    
    
    



print ('')
print ('')
print ('')
print ('')
print ('')



#--------------------------------------------------------solid----------------------------------------------------
s_mesh_name = ex.get_mesh_name_by_prefix('s_mesh')
s_n_nodes = ex.read_write_mesh(s_mesh_name, 's_mesh.vtk')
s_nb_dofs = ex.nb_of_dofs(s_data_path_u + times[0], s_n_nodes)
ex.prefix('s_data/u_prefix', s_n_nodes, 'u', 'VECTOR', True)
ex.prefix('s_data/v_prefix', s_n_nodes, 'v', 'VECTOR')
ex.prefix('s_data/a_prefix', s_n_nodes, 'a', 'VECTOR')
ex.createFolder('s_vtk_data')


def s_var_files(u, v, a, t):
      u1 = u[0::s_nb_dofs]  
      u2 = u[1::s_nb_dofs]         
      v1 = v[0::s_nb_dofs]         
      v2 = v[1::s_nb_dofs]         
      a1 = a[0::s_nb_dofs]         
      a2 = a[1::s_nb_dofs]         

      if(s_nb_dofs==2):
         u3 = np.zeros(s_n_nodes)
         v3 = np.zeros(s_n_nodes)
         a3 = np.zeros(s_n_nodes)
      else:   
         u3 = u[2::s_nb_dofs]         
         v3 = v[2::s_nb_dofs]         
         a3 = a[2::s_nb_dofs]         

      u_vec = np.array([u1, u2, u3])  
      np.savetxt('s_data/u_{s1}'.format(s1=t), u_vec.T)
      v_vec = np.array([v1, v2, v3])  
      np.savetxt('s_data/v_{s1}'.format(s1=t), v_vec.T)
      a_vec = np.array([a1, a2, a3])  
      np.savetxt('s_data/a_{s1}'.format(s1=t), a_vec.T)

      s = 'mesh/s_mesh.vtk '
      s += 's_data/{s1}_prefix s_data/{s1}_{s2} '.format(s1='u', s2=t)
      s += 's_data/{s1}_prefix s_data/{s1}_{s2} '.format(s1='v', s2=t)
      s += 's_data/{s1}_prefix s_data/{s1}_{s2}'.format(s1='a', s2=t)
      return s


def s_vtk_file_write(t):
      with open(s_data_path_u + t, 'rb') as fid:
         u = np.fromfile(fid, dtype=float)
      with open(s_data_path_v + t, 'rb') as fid:
         v = np.fromfile(fid, dtype=float)
      with open(s_data_path_a + t, 'rb') as fid:
         a = np.fromfile(fid, dtype=float)
   
      print ('Processing s at ', t)
      s = s_var_files(u, v, a, t)      

      vtk_file = 's_vtk_data/sol_{}.vtk'.format(t)
      os.system('cat {s1} > {s2}'.format(s1=s, s2=vtk_file))


#------------------execute multi processing--------------------------
with concurrent.futures.ProcessPoolExecutor() as executor:
    executor.map(s_vtk_file_write, times)

os.chdir('s_vtk_data/')

if(vtk_type == "vtu"):     ex.write_pvd_file(times)
else:    ex.write_vtk_series(times)

    




os.system('rm -rf f_data s_data')

if(vtk_type == "vtu"): os.system('paraview f_vtk_data/data.pvd s_vtk_data/data.pvd')
else: os.system('paraview f_vtk_data/data.vtk.series s_vtk_data/data.vtk.series')

