#!/usr/bin/python3

import os
import sys
import numpy as np
import time
import concurrent.futures
import utils.pp_utils_ex as ex




start, end, step, adaptive, gap, mm, isothermal, var, vtk_type = float(sys.argv[1]), float(sys.argv[2]), float(sys.argv[3]), sys.argv[4], int(sys.argv[5]), sys.argv[6], sys.argv[7], sys.argv[8], sys.argv[9]

var = var.split(',')



data_path = 'sec_dofs/rho/'



mesh_name = ex.get_mesh_name()

if(mm == 'y'): n_nodes = ex.read_write_mesh_for_moving_mesh(mesh_name) 
else: n_nodes = ex.read_write_mesh(mesh_name)

ex.createFolder('f_data')
ex.createFolder('vtk_data_pp')


if(vtk_type == "vtu"):   last_pp_time = ex.get_last_pp_time('vtk_data/prev.txt')
else:     last_pp_time = ex.get_last_pp_time('vtk_data/data.vtk.series')


times = ex.list_of_times_to_pp(data_path, last_pp_time, start, end, step, adaptive, gap)


   
#-------------------data prefix-----------------------------   
for x in var:
   if(x=='rho'):  ex.prefix('f_data/{}_prefix'.format(x), n_nodes, x, 'SCALAR', True)
   else: ex.prefix('f_data/{}_prefix'.format(x),n_nodes, x, 'SCALAR')




#--------------writing data in vtk file format----------------    
def vtk_file_write(t):
   print ('Processing at ', t)
   s = 'mesh/mesh.vtk '
   for x in var:
      file_name = 'sec_dofs/{s1}/'.format(s1=x) + t
      with open(file_name,'rb') as fid:
         data = np.fromfile(fid, dtype=float)
         
      np.savetxt('f_data/{s1}_{s2}'.format(s1=x, s2=t), data)
      s += 'f_data/{s1}_prefix f_data/{s1}_{s2} '.format(s1=x, s2=t)
      
   vtk_file = 'vtk_data_pp/sol_{}.vtk'.format(t)

   os.system('cat {s1} > {s2}'.format(s1=s, s2=vtk_file))



           
#------------------execute multi processing--------------------------
with concurrent.futures.ProcessPoolExecutor() as executor:
    executor.map(vtk_file_write, times)



    
os.chdir('vtk_data_pp/')   

if(vtk_type == "vtu"):  
   ex.write_pvd_file(times)
   os.system('rm -rf data')       #removing un needed folders
   os.system('paraview vtk_data_pp/data.pvd')
else: 
   ex.write_vtk_series(times)
   os.system('rm -rf data')       #removing un needed folders
   os.system('paraview vtk_data_pp/data.vtk.series')


