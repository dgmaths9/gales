#!/usr/bin/python3

import os
import sys
import numpy as np
import time
import concurrent.futures
import utils.pp_utils_ex as ex




start, end, step, adaptive, gap, mm, isothermal, vtk_type = float(sys.argv[1]), float(sys.argv[2]), float(sys.argv[3]), sys.argv[4], int(sys.argv[5]), sys.argv[6], sys.argv[7], sys.argv[8]


ex.createFolder('f_data')
ex.createFolder('vtk_data')


data_path = 'fluid_dofs/'

mesh_name = ex.get_mesh_name()

if(mm == 'y'): n_nodes = ex.read_write_mesh_for_moving_mesh(mesh_name) 
else: n_nodes = ex.read_write_mesh(mesh_name)

ex.prefix('f_data/p_prefix', n_nodes, 'p', 'SCALAR', True)
ex.prefix('f_data/dp_prefix', n_nodes, 'dp', 'SCALAR')
ex.prefix('f_data/v_prefix', n_nodes, 'v', 'VECTOR')
if (isothermal == 'n'): ex.prefix('f_data/T_prefix', n_nodes, 'T', 'SCALAR')



if(vtk_type == "vtu"):   last_pp_time = ex.get_last_pp_time('vtk_data/prev.txt')
else:     last_pp_time = ex.get_last_pp_time('vtk_data/data.vtk.series')



times = ex.list_of_times_to_pp(data_path, last_pp_time, start, end, step, adaptive, gap)
nb_dofs = ex.nb_of_dofs(data_path + times[0], n_nodes)
print("nb_dofs: ", nb_dofs)


with open(data_path + '0','rb') as fid:
    load_var = np.fromfile(fid, dtype=float)
p0 = load_var[0::nb_dofs]







def var_files(load_var,t):
      p = load_var[0::nb_dofs]
      dp = p-p0
      v1 = load_var[1::nb_dofs]
      v2 = load_var[2::nb_dofs]
      np.savetxt('f_data/p_{s1}'.format(s1=t), p)
      np.savetxt('f_data/dp_{s1}'.format(s1=t), dp)

      
      if(isothermal == 'y'):
         if(nb_dofs==3): 
            v3 = np.zeros(n_nodes)
         elif(nb_dofs==4): 
            v3 = load_var[3::nb_dofs]

      elif(isothermal == 'n'):
         if(nb_dofs==4): 
            v3 = np.zeros(n_nodes)
            T = load_var[3::nb_dofs]
         elif(nb_dofs==5):
            v3 = load_var[3::nb_dofs]
            T = load_var[4::nb_dofs]

         np.savetxt('f_data/T_{s1}'.format(s1=t), T)


      v = np.array([v1, v2, v3])
      np.savetxt('f_data/v_{s1}'.format(s1=t), v.T)

      s = 'mesh/mesh.vtk '
      s += 'f_data/{s1}_prefix f_data/{s1}_{s2} '.format(s1='p', s2=t)
      s += 'f_data/{s1}_prefix f_data/{s1}_{s2} '.format(s1='dp', s2=t)
      if(isothermal == 'n'):  s += 'f_data/{s1}_prefix f_data/{s1}_{s2} '.format(s1='T', s2=t)
      s += 'f_data/{s1}_prefix f_data/{s1}_{s2} '.format(s1='v', s2=t)
      return s





def var_files_mm(load_var, xyz_data, t):

      s = var_files(load_var,t)

      if(nb_dofs==3):
            x = xyz_data[0::2]
            y = xyz_data[1::2]   
            z = np.zeros(n_nodes)
      elif(nb_dofs==4):
         if(isothermal == 'y'):
            x = xyz_data[0::3]
            y = xyz_data[1::3]
            z = xyz_data[2::3]
         else:
            x = xyz_data[0::2]
            y = xyz_data[1::2]   
            z = np.zeros(n_nodes)
      elif(nb_dofs==5):
            x = xyz_data[0::3]
            y = xyz_data[1::3]
            z = xyz_data[2::3]
                     
      xyz = np.array([x, y, z])
      np.savetxt('f_data/mesh_{s1}'.format(s1=t), xyz.T)

      s1 = s.strip().split()
      s1[0] = 'f_data/mesh_pre1  f_data/mesh_{s1} f_data/mesh_pre2 '.format(s1=t)
      s =' '.join(s1)
      return s







#--------------writing data in vtk file format----------------
def vtk_file_write(t):
      if(mm == 'y'):
         with open(data_path + t, 'rb') as fid:
            load_var = np.fromfile(fid, dtype=float)         
         with open('fluid_mesh/' + t, 'rb') as fid:
            xyz_data = np.fromfile(fid, dtype=float)
         
         print ('Processing at ', t)
         s = var_files_mm(load_var, xyz_data, t)      

      else:
         with open(data_path + t, 'rb') as fid:
            load_var = np.fromfile(fid, dtype=float)
         
         print ('Processing at ', t)
         s = var_files(load_var, t)

      vtk_file = 'vtk_data/sol_{}.vtk'.format(t)
      os.system('cat {s1} > {s2}'.format(s1=s, s2=vtk_file))



#------------------execute multi processing--------------------------
with concurrent.futures.ProcessPoolExecutor() as executor:
    executor.map(vtk_file_write, times)




os.chdir('vtk_data/')

if(vtk_type == "vtu"):  
   ex.write_pvd_file(times)
   os.system('rm -rf f_data')       #removing un needed folders
   os.system('paraview vtk_data/data.pvd')
else: 
   ex.write_vtk_series(times)
   os.system('rm -rf f_data')       #removing un needed folders
   os.system('paraview vtk_data/data.vtk.series')


