#!/usr/bin/python3


import os
import sys
import numpy as np
import time 
import concurrent.futures
import utils.pp_utils_ex as ex





start, end, step, adaptive, gap, vtk_type = float(sys.argv[1]), float(sys.argv[2]), float(sys.argv[3]), sys.argv[4], int(sys.argv[5]), sys.argv[6]

ex.createFolder('s_data')
ex.createFolder('vtk_data')


data_path_u = 'solid/u/'


mesh_name = ex.get_mesh_name()
n_nodes = ex.read_write_mesh(mesh_name)
ex.prefix('s_data/u_prefix', n_nodes, 'u', 'VECTOR', True)


if(vtk_type == "vtu"):   last_pp_time = ex.get_last_pp_time('vtk_data/prev.txt')
else:     last_pp_time = ex.get_last_pp_time('vtk_data/data.vtk.series')

times = ex.list_of_times_to_pp(data_path_u, last_pp_time, start, end, step, adaptive, gap)
nb_dofs = ex.nb_of_dofs(data_path_u + times[0], n_nodes)



def var_files(u, t):
      u1 = u[0::nb_dofs]  
      u2 = u[1::nb_dofs]         

      if(nb_dofs==2):
         u3 = np.zeros(n_nodes)
      else:   
         u3 = u[2::nb_dofs]         
          

      u_vec = np.array([u1, u2, u3])  
      np.savetxt('s_data/u_{s1}'.format(s1=t), u_vec.T)

      s = 'mesh/mesh.vtk '
      s += 's_data/{s1}_prefix s_data/{s1}_{s2} '.format(s1='u', s2=t)
      return s




#--------------writing data in vtk file format----------------    
def vtk_file_write(t):
      with open(data_path_u + t, 'rb') as fid:
         u = np.fromfile(fid, dtype=float)
   
      print ('Processing at ', t)
      s = var_files(u, t)      

      vtk_file = 'vtk_data/sol_{}.vtk'.format(t)
      os.system('cat {s1} > {s2}'.format(s1=s, s2=vtk_file))

    



#------------------execute multi processing--------------------------
with concurrent.futures.ProcessPoolExecutor() as executor:
    executor.map(vtk_file_write, times)
                                              

os.chdir('vtk_data/')

if(vtk_type == "vtu"):  
   ex.write_pvd_file(times)
   os.system('rm -rf s_data')       #removing un needed folders
   os.system('paraview vtk_data/data.pvd')
else: 
   ex.write_vtk_series(times)
   os.system('rm -rf s_data')       #removing un needed folders
   os.system('paraview vtk_data/data.vtk.series')
   
     
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
    
