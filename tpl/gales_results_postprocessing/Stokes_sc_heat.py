#!/usr/bin/python3

import os
import sys
import numpy as np
import time
import concurrent.futures
import pp_utils_ex as ex
import json
import decimal





start, end, step, adaptive, gap, vtk_type = float(sys.argv[1]), float(sys.argv[2]), float(sys.argv[3]), sys.argv[4], int(sys.argv[5]), sys.argv[6]



ex.createFolder('f_data')
ex.createFolder('vtk_data')


data_path = 'fluid_dofs/'
T_data_path = 'heat_eq/T/'

mesh_name = ex.get_mesh_name()

n_nodes = ex.read_write_mesh(mesh_name)

ex.prefix('f_data/p_prefix', n_nodes, 'p', 'SCALAR', True)
ex.prefix('f_data/dp_prefix', n_nodes, 'dp', 'SCALAR')
ex.prefix('f_data/T_prefix', n_nodes, 'T', 'SCALAR')
ex.prefix('f_data/v_prefix', n_nodes, 'v', 'VECTOR')



if(vtk_type == "vtu"):   last_pp_time = ex.get_last_pp_time('vtk_data/prev.txt')
else:     last_pp_time = ex.get_last_pp_time('vtk_data/data.vtk.series')



times = ex.list_of_times_to_pp(data_path, last_pp_time, start, end, step, adaptive, gap)
nb_dofs = ex.nb_of_dofs(data_path + times[0], n_nodes)
print("nb_dofs: ", nb_dofs)


with open(data_path + '0','rb') as fid:
    load_var = np.fromfile(fid, dtype=float)
p0 = load_var[0::nb_dofs]







def var_files(load_var, load_var_T, t):
      p = load_var[0::nb_dofs]
      dp = p-p0
      T = load_var_T
      v1 = load_var[1::nb_dofs]
      v2 = load_var[2::nb_dofs]
      np.savetxt('f_data/p_{s1}'.format(s1=t), p)
      np.savetxt('f_data/dp_{s1}'.format(s1=t), dp)
      np.savetxt('f_data/T_{s1}'.format(s1=t), T)

      
      if(nb_dofs==3): 
            v3 = np.zeros(n_nodes)
      elif(nb_dofs==4):
            v3 = load_var[3::nb_dofs]


      v = np.array([v1, v2, v3])
      np.savetxt('f_data/v_{s1}'.format(s1=t), v.T)

      s = 'mesh/mesh.vtk '
      s += 'f_data/{s1}_prefix f_data/{s1}_{s2} '.format(s1='p', s2=t)
      s += 'f_data/{s1}_prefix f_data/{s1}_{s2} '.format(s1='dp', s2=t)
      s += 'f_data/{s1}_prefix f_data/{s1}_{s2} '.format(s1='T', s2=t)
      s += 'f_data/{s1}_prefix f_data/{s1}_{s2} '.format(s1='v', s2=t)
      return s









#--------------writing data in vtk file format----------------
def vtk_file_write(t):
       with open(data_path + t, 'rb') as fid:
            load_var = np.fromfile(fid, dtype=float)
       with open(T_data_path + t, 'rb') as fid:
            load_var_T = np.fromfile(fid, dtype=float)
         
       print ('Processing at ', t)
       s = var_files(load_var, load_var_T, t)

       vtk_file = 'vtk_data/sol_{}.vtk'.format(t)
       os.system('cat {s1} > {s2}'.format(s1=s, s2=vtk_file))



#------------------execute multi processing--------------------------
with concurrent.futures.ProcessPoolExecutor() as executor:
    executor.map(vtk_file_write, times)




os.chdir('vtk_data/')

if(vtk_type == "vtu"):  
   ex.write_pvd_file(times)
#   os.system('rm -rf f_data')       #removing un needed folders
   os.system('paraview vtk_data/data.pvd')
else: 
   ex.write_vtk_series(times)
#   os.system('rm -rf f_data')       #removing un needed folders
   os.system('paraview vtk_data/data.vtk.series')


