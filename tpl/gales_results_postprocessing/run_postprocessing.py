#!/usr/bin/python3

import math
import os
import sys
import matplotlib.pyplot as plt
import numpy as np
import utils.pp_utils_ex as ex



init_msg = '''

 ------------------------------------------------------
  Steps:
           
   1. create a directory 
      (e.g. mkdir <sim_postproc>)
           
   2. download results there 
      (e.g. mv <sim>/results/*  <sim_postproc>/)
           
   3. create directory mesh 
      (e.g. mkdir <sim_postproc>/mesh) 
                      
   4. download mesh files in mesh directory 
      (e.g. mv <sim>/input/*.txt  <sim_postproc>/mesh/ )

   5. launch this script from sim_postproc directory
      (e.g. cd <sim_postproc> && run_postprocessing.py)
   
   use appropriate paths for <> 
 ------------------------------------------------------


'''






dirs = [name for name in os.listdir(".") if os.path.isdir(name)]



def check_dir(dir_name, err_msg):
   if dir_name not in dirs:
      print(err_msg)
      print('\n\n')
      print(init_msg)
      print('\n\n')
      exit()
    
    




def check_input(prompt, input_range, err_msg):
   for trial in range(4):
      if(trial > 2):
         print('')
         print('---------Maximum trials exceeded!!!----------')
         print('')
         print('')
         exit()         
      x = input(prompt)     
      if x not in input_range:  
        print('')  
        print('------ ' + err_msg + ' ----------')
        print('')
        continue
      else:  
        break
   return x








def check_postprocessing_directory():
   cwd = os.getcwd()
   dirname = cwd[-7:]
   if(dirname != "results"):
      print('')  
      print('--------------------------------------------------')
      print('')
      print('Erorr: Please run this script from results directory!!!')
      print('')
      print('')
      exit()
      
         





def simultion_type():
  print('')
  print('------- Simulation types : -------------')
  print(' 1: Fluid')
  print(' 2: Solid')
  print(' 3: Heat equation')
  print(' 4: FSI (Fluid-Solid interaction)')
  print(' 5: FHI (Fluid-heat interaction)')
  print(' 6: Adv-Diff (scaler Advection-Diffusion equation)')
  print('--------------------------------------')
  print('')
  x = check_input("Choose simulation type (1,2,3, ...): ", ('1','2','3','4','5','6'), "Error: Simulation type is unknown;")  
  print('')  
  return x
  
    





def run_selection(x):
  ex.createFolder("./mesh")
  os.system("cp ../input/*core* ./mesh/")


  mesh_dir = os.listdir("./mesh/")   
  if len(mesh_dir) == 0:
      print("Error: Mesh directory is empty")
      print('')
      exit()


  adaptive = 'y'
  print('')
  data_type = int(input('adaptive(0) or timeRange(1)?  [default = adaptive(0)] : ') or '0')  
  if(data_type == 0):
     print('Info: chosen data type is adaptive')  
  else:
     print('Info: chosen data type is timeRange')  
     
  print('')
  if(data_type != 0): adaptive = 'n'
  
  
  print('')
  gap = 1
  start, end, step = 0.0, 0.0, 0.0
  if(adaptive == 'y'):
    gap = int(input('insert desired gap (int) between two results (default gap = 1) : ') or '1')  
    print('Info: gap = ', gap)
  else:
    user_input = input("Enter 3 numbers for startTime endTime step: ").split()
    start, end, step = float(user_input[0]), float(user_input[1]), float(user_input[2])   
    print("Info: start: ", start, "  end: ", end, " step: ", step)
  print('')


  
  print('')  
  vtk_file_type = str(input('vtk or vtu? [default = vtu] : ') or 'vtu')    
  print('Info: file_type = ', vtk_file_type)
  print('')  
  

  
  if(x=='1'):
    check_dir('fluid_dofs', 'Error: you have not put the fluid_dofs directory here')
    
    print('')
    print('------- Fluid simulation type : -------------')
    print('    1: Fluid_MC:              p, v, T, y1')
    print('    2: Fluid_MC_Isothermal:   p, v,    y1')
    print('    3: Fluid_SC:              p, v, T    ')
    print('    4: Fluid_SC_Isothermal:   p, v,      ')
    print('    5: Fluid_secondary_dofs:  rho, mu, cp, cv, kappa, sound_speed, alpha, beta, gas_vf, ...')
    print('--------------------------------------')
    print('')
    v = check_input("Choose Fluid simulation type (1,2,3, ...): ", ('1','2','3','4','5'), "Error: Fluid Simulation type is unknown;")  
    print('')


    isothermal = 'n'
    if(v=='2' or v=='4'): isothermal = 'y'


    print('')
    mm = 'n'
    if(v == '1' or v=='2' or v == '3' or v=='4'):
      mm = str(input('Is mesh moving? (y/n)  [default = n] : ') or 'n')    
      print('Info: mm = ', mm)        
      if(mm=='y'):  
        check_dir('fluid_mesh', 'Error: you have not put the fluid_mesh directory here')
        check_dir('elastostatic_dofs', 'Error: you have not put the elastostatic_dofs directory here')
      print('')
              
              
              
    if(v == '1' or v=='2'):   os.system('fluid_mc.py {} {} {} {} {} {} {} {}'.format(start, end, step, adaptive, gap, mm, isothermal, vtk_file_type)) 
    elif(v == '3' or v=='4'):   os.system('fluid_sc.py {} {} {} {} {} {} {} {}'.format(start, end, step, adaptive, gap, mm, isothermal, vtk_file_type)) 
    elif(v == '5'):
       sec_vars = input("Enter secondary variables with a space (rho, mu, cp, cv, ...): ").split()
       sec_vars = ",".join([str(a) for a in sec_vars])
       os.system('fluid_secondary_dofs.py {} {} {} {} {} {} {} {} {}'.format(start, end, step, adaptive, gap, mm, isothermal, sec_vars, vtk_file_type)) 
    print('')




  elif(x=='2'):
    check_dir('solid', 'Error: you have not put the solid directory here')
    print('')
    print('------- Solid simulation type : -------------')
    print('    1: Solid_ED:              u, v, a')
    print('    2: Solid_ES:              u      ')
    print('    3: Solid_ES_hetero:       u  +  heterogeneous properties  ')
    print('---------------------------------------------')
    print('')
    v = check_input("Choose Solid simulation type (1,2,3): ", ('1','2','3'), "Error: Solid Simulation type is unknown;")  
    print('')              
    if(v=='1'):      os.system('solid_ed.py {} {} {} {} {} {}'.format(start, end, step, adaptive, gap, vtk_file_type)) 
    elif(v=='2'):    os.system('solid_es.py {} {} {} {} {} {}'.format(start, end, step, adaptive, gap, vtk_file_type))
    elif(v=='3'):    os.system('solid_es_hetero.py {} {} {} {} {} {}'.format(start, end, step, adaptive, gap, vtk_file_type))
    print('')  
    
    
    
    
  elif(x=='3'):
    check_dir('heat_eq', 'Error: you have not put the heat_eq directory here')
    print('')
    os.system('heat_eq.py {} {} {} {} {} {}'.format(start, end, step, adaptive, gap, vtk_file_type))   
    print('')  
    
    

    

  elif(x=='4'):
    check_dir('fluid_dofs', 'Error: you have not put the fluid_dofs directory here')
    check_dir('fluid_mesh', 'Error: you have not put the fluid_mesh directory here')
    check_dir('elastostatic_dofs', 'Error: you have not put the elastostatic_dofs directory here')    
    check_dir('solid', 'Error: you have not put the solid directory here')
    print('')
    print('------- FSI simulation type : -------------')
    print('    1: Fluid: p, v, T, y1       Solid: u, v, a')
    print('    2: Fluid: p, v,    y1       Solid: u, v, a')
    print('    3: Fluid: p, v, T           Solid: u, v, a')
    print('    4: Fluid: p, v              Solid: u, v, a')
    print('')
    v = check_input("Choose FSI simulation type (1,2,3, ...): ", ('1','2','3','4'), "Error: FSI Simulation type is unknown;")  
    print('')              

    isothermal = 'n'
    if(v=='2' or v=='4'): isothermal = 'y'

    if(v=='1' or v=='2'):    os.system('FSI_mc_ed.py {} {} {} {} {} {} {}'.format(start, end, step, adaptive, gap, isothermal, vtk_file_type))
    elif(v=='3' or v=='4'):   os.system('FSI_sc_ed.py {} {} {} {} {} {} {}'.format(start, end, step, adaptive, gap, isothermal, vtk_file_type))
    print('')  




  elif(x=='5'):
    check_dir('fluid_dofs', 'Error: you have not put the fluid_dofs directory here')
    check_dir('heat_eq', 'Error: you have not put the heat_eq directory here')
    print('')
    print('------- FHI simulation type : -------------')
    print('    1: Fluid: p, v, T, y1       Heat equation: T')
    print('    2: Fluid: p, v,    y1       Heat equation: T')
    print('    3: Fluid: p, v, T           Heat equation: T')
    print('    4: Fluid: p, v              Heat equation: T')
    print('')
    v = check_input("Choose FHI simulation type (1,2,3, ...): ", ('1','2','3','4'), "Error: FHI Simulation type is unknown;")  
    print('')              

    isothermal = 'n'
    if(v=='2' or v=='4'): isothermal = 'y'

    if(v=='1' or v=='2'):    os.system('FHI_mc_heat.py {} {} {} {} {} {} {}'.format(start, end, step, adaptive, gap, isothermal, vtk_file_type))
    elif(v=='3' or v=='4'):   os.system('FHI_sc_heat.py {} {} {} {} {} {} {}'.format(start, end, step, adaptive, gap, isothermal, vtk_file_type))
    print('')  
        
    

  elif(x=='6'):
    check_dir('Y', 'Error: you have not put the Y directory here')
    print('')
    print('------- adv-diff simulation type : -------------')
    print('    1: adv-diff              ')
    print('    2: adv-diff with rho, cp, kappa              ')
    print('---------------------------------------------')
    print('')
    v = check_input("Choose adv-diff simulation type (1,2): ", ('1','2'), "Error: adv-diff Simulation type is unknown;")  
    print('')              
    if(v=='1'):      os.system('adv-diff.py {} {} {} {} {} {}'.format(start, end, step, adaptive, gap, vtk_file_type))   
    elif(v=='2'):    os.system('adv-diff_rho_cp_kappa.py {} {} {} {} {} {}'.format(start, end, step, adaptive, gap, vtk_file_type))
    print('')  




if __name__ == "__main__":
   check_postprocessing_directory()
   x = simultion_type()
   run_selection(x)
    
    

