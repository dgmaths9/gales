#!/usr/bin/python3

import os
import sys
import numpy as np
import time
import concurrent.futures
import utils.pp_utils_ex as ex



start, end, step, adaptive, gap, isothermal, vtk_type = float(sys.argv[1]), float(sys.argv[2]), float(sys.argv[3]), sys.argv[4], int(sys.argv[5]), sys.argv[6], sys.argv[7]


ex.createFolder('f_data')
ex.createFolder('h_data')

f_data_path = 'fluid_dofs/'
h_data_path = 'heat_eq/T/'



if(vtk_type == "vtu"):   last_pp_time = ex.get_last_pp_time('f_vtk_data/prev.txt')
else:     last_pp_time = ex.get_last_pp_time('f_vtk_data/data.vtk.series')


times = ex.list_of_times_to_pp(f_data_path, last_pp_time, start, end, step, adaptive, gap)





#--------------------------------------------------------fluid----------------------------------------------------
f_mesh_name = ex.get_mesh_name_by_prefix('f_mesh')
f_n_nodes = ex.read_write_mesh(f_mesh_name, 'f_mesh.vtk')
f_nb_dofs = ex.nb_of_dofs(f_data_path + times[0], f_n_nodes)
ex.prefix('f_data/p_prefix', f_n_nodes, 'p', 'SCALAR', True)
ex.prefix('f_data/dp_prefix', f_n_nodes, 'dp', 'SCALAR')
ex.prefix('f_data/v_prefix', f_n_nodes, 'v', 'VECTOR')
ex.prefix('f_data/T_prefix', f_n_nodes, 'T', 'SCALAR')
ex.createFolder('f_vtk_data')



with open(f_data_path + '0','rb') as fid:
    load_var = np.fromfile(fid, dtype=float)
p0 = load_var[0::f_nb_dofs]



def f_var_files(load_var, t):
      p = load_var[0::f_nb_dofs]
      dp = p-p0
      v1 = load_var[1::f_nb_dofs]
      v2 = load_var[2::f_nb_dofs]
      np.savetxt('f_data/p_{s1}'.format(s1=t), p)
      np.savetxt('f_data/dp_{s1}'.format(s1=t), dp)

      if(isothermal == 'y'):
         if(f_nb_dofs==3): 
            v3 = np.zeros(f_n_nodes)
         elif(f_nb_dofs==4):
            v3 = load_var[3::f_nb_dofs]

      elif(isothermal == 'n'):
         if(f_nb_dofs==4): 
            v3 = np.zeros(f_n_nodes)
            T = load_var[3::f_nb_dofs]
         elif(f_nb_dofs==5):
            v3 = load_var[3::f_nb_dofs]
            T = load_var[4::f_nb_dofs]
            
         np.savetxt('f_data/T_{s1}'.format(s1=t), T)


      v = np.array([v1, v2, v3])
      np.savetxt('f_data/v_{s1}'.format(s1=t), v.T)

      s = 'mesh/f_mesh.vtk '
      s += 'f_data/{s1}_prefix f_data/{s1}_{s2} '.format(s1='p', s2=t)
      s += 'f_data/{s1}_prefix f_data/{s1}_{s2} '.format(s1='dp', s2=t)
      if(isothermal == 'n'):  s += 'f_data/{s1}_prefix f_data/{s1}_{s2} '.format(s1='T', s2=t)
      s += 'f_data/{s1}_prefix f_data/{s1}_{s2}'.format(s1='v', s2=t)
      return s


#--------------writing data in vtk file format----------------    
def f_vtk_file_write(t):
      with open(f_data_path + t, 'rb') as fid:
         load_var = np.fromfile(fid, dtype=float)
   
      print ('Processing f at ', t)
      s = f_var_files(load_var, t)      

      vtk_file = 'f_vtk_data/sol_{}.vtk'.format(t)
      os.system('cat {s1} > {s2}'.format(s1=s, s2=vtk_file))


#------------------execute multi processing--------------------------
with concurrent.futures.ProcessPoolExecutor() as executor:
    executor.map(f_vtk_file_write, times)
    
os.chdir('f_vtk_data/')

if(vtk_type == "vtu"):     ex.write_pvd_file(times)
else:    ex.write_vtk_series(times)

    
    
    



print ('')
print ('')
print ('')
print ('')
print ('')


#--------------------------------------------------------heat equation----------------------------------------------------
h_mesh_name = ex.get_mesh_name_by_prefix('h_mesh')
h_n_nodes = ex.read_write_mesh(h_mesh_name, 'h_mesh.vtk')
ex.prefix('h_data/T_prefix', h_n_nodes, 'T', 'SCALAR', True)
ex.createFolder('h_vtk_data')


#--------------writing data in vtk file format----------------
def h_vtk_file_write(t):
      file_name = h_data_path + t
      with open(file_name,'rb') as fid:
         T = np.fromfile(fid, dtype=float)

      print ('Processing at ', t)
      np.savetxt('h_data/T_{s1}'.format(s1=t), T)
      s = 'mesh/h_mesh.vtk h_data/T_prefix h_data/T_{s2}'.format(s2=t)

      vtk_file = 'h_vtk_data/sol_{}.vtk'.format(t)
      os.system('cat {s1} > {s2}'.format(s1=s, s2=vtk_file))




#------------------execute multi processing--------------------------
with concurrent.futures.ProcessPoolExecutor() as executor:
    executor.map(h_vtk_file_write, times)




os.chdir('h_vtk_data/')

if(vtk_type == "vtu"):     ex.write_pvd_file(times)
else:    ex.write_vtk_series(times)
    




os.system('rm -rf f_data h_data')
if(vtk_type == "vtu"):  os.system('paraview f_vtk_data/data.pvd h_vtk_data/data.pvd')
else:  os.system('paraview f_vtk_data/data.vtk.series h_vtk_data/data.vtk.series')

