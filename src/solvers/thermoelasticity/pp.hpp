#include <mpi.h>
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <iterator>
#include <vector>


#include "../../fem/fem.hpp"
#include "thermoelasticity.hpp"



using namespace GALES;

   void print_sigma(const std::string& f, const std::vector<double>& v)
   {
      const int rank = get_rank();      
        if(rank==0)
        {     
          std::stringstream ss;
          ss <<"results/sec_dofs/sigma/"<<f<<"/"<<time::get().t();
          auto s = ss.str();
          std::ofstream os(s, std::ios::out);
          for (int i=0;i<v.size();i++)
          os<< v[i] <<std::endl;
          os.close();
        }  
   }
   
      void print_sigma(const std::string& f, const std::vector<int>& v)
   {
      const int rank = get_rank();      
        if(rank==0)
        {     
          std::stringstream ss;
          ss <<"results/sec_dofs/sigma/"<<f<<"/"<<time::get().t();
          auto s = ss.str();
          std::ofstream os(s, std::ios::out);
          for (int i=0;i<v.size();i++)
          os<< v[i] <<std::endl;
          os.close();
        }  
   }

template<int dim> 
void solver(read_setup& setup)
{
  #include "constructors.hpp"

  for(auto t = setup.pp_start_time(); t <= setup.pp_final_time(); t += setup.pp_delta_t())
  {    
    double t_iteration(MPI_Wtime());    
    print_only_pid<0>(std::cerr)<<"postprocessing at time = "<<t<<" sec.\n";

    time::get().t(t);
    solid_io.read("solid/u/", *solid_u_dof_state); 
    heat_eq_io.read("heat_eq/T/", *heat_eq_dof_state);     

    std::map<int, std::vector<double> > s_nd_sgm;
    sigma<dim> sigma;
    sigma.sgm(solid_u_model, heat_eq_model, solid_props, s_nd_sgm);
    
    std::vector<double> sgm11, sgm22, sgm33, sgm12, sgm23, sgm13;

    if (dim == 2) {    
    make_dirs("results/sec_dofs/sigma/sigma11");
    make_dirs("results/sec_dofs/sigma/sigma22");
    make_dirs("results/sec_dofs/sigma/sigma12");
    for(auto v : s_nd_sgm) 
    {
      sgm11.push_back(v.second[0]);
      sgm22.push_back(v.second[1]);
      sgm12.push_back(v.second[2]);     
    }
    print_sigma("sigma11",sgm11);
    print_sigma("sigma22",sgm22);
    print_sigma("sigma12",sgm12); 
    }
    else if (dim == 3) {
    make_dirs("results/sec_dofs/sigma/sigma11");
    make_dirs("results/sec_dofs/sigma/sigma22");
    make_dirs("results/sec_dofs/sigma/sigma33");
    make_dirs("results/sec_dofs/sigma/sigma12");
    make_dirs("results/sec_dofs/sigma/sigma23");
    make_dirs("results/sec_dofs/sigma/sigma13");
    for(auto v : s_nd_sgm) 
    {
      sgm11.push_back(v.second[0]);
      sgm22.push_back(v.second[1]);
      sgm33.push_back(v.second[2]);
      sgm12.push_back(v.second[3]); 
      sgm23.push_back(v.second[4]); 
      sgm13.push_back(v.second[5]);    
    }
      print_sigma("sigma11",sgm11);
      print_sigma("sigma22",sgm22);
      print_sigma("sigma33",sgm33);   
      print_sigma("sigma12",sgm12);
      print_sigma("sigma23",sgm23);
      print_sigma("sigma13",sgm13);
    } 

   
    print_only_pid<0>(std::cerr)<<"Time taken for time step = "<<MPI_Wtime()-t_iteration<<"\n"<<"\n";    
  }
}






int main(int argc, char* argv[])
{

  MPI_Init(&argc, &argv);
      


  //---------------- read setup file -------------------------------
  read_setup setup;
    

  if(setup.dim()==2) solver<2>(setup);
  else if(setup.dim()==3) solver<3>(setup);
  
  MPI_Barrier(MPI_COMM_WORLD);
  MPI_Finalize();
  return 0;
}
