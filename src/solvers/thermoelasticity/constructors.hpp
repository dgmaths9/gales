

  const int nb_dofs_s = dim;  
  

  //------------------ mesh building --------------------------------------------------------------------
  print_only_pid<0>(std::cerr)<< "SOLID MESH\n";
  double mesh_read_start_s = MPI_Wtime();
  Mesh<dim> solid_mesh(setup.solid_mesh_file(), nb_dofs_s, setup);
  print_only_pid<0>(std::cerr)<<"Mesh reading took: "<<std::setprecision(setup.precision()) << MPI_Wtime()-mesh_read_start_s<<" s\n\n";



  //-----------------solid props------------------------------------------
  solid_properties solid_props;

  //-----------------------------------------maps-------------------------------------------
  epetra_maps solid_maps(solid_mesh);
  
  //----------------------------------------dofs state-----------------------------------------------------  
  auto solid_u_dof_state = std::make_unique<dof_state>(solid_maps.state_map()->NumMyElements(), 1);
  
  //---------------------------------------model--------------------------------------------
  model<dim> solid_u_model(solid_mesh, *solid_u_dof_state, solid_maps, setup);  

  //----------------------------------------ic bc-------------------------------------------
  using solid_ic_bc_type = solid_ic_bc<dim>;
  solid_ic_bc_type solid_ic_bc;
  
  //----------------------------------------dofs ic bc------------------------------------------
  using solid_dofs_ic_bc_type = solid_dofs_ic_bc<solid_ic_bc_type, dim>;
  solid_dofs_ic_bc_type solid_dofs_ic_bc(solid_u_model, solid_ic_bc, setup);
  
  //---------------------updater------------------------------------------------------
  dofs_updater solid_updater(setup);
      
  //--------------------linear system-------------------------------------------------
  linear_system solid_lp(solid_u_model);
  
        
  //--------------------linear solver----------------------------------------------------
  belos_linear_solver solid_ls_solver(setup); 

  //---------------------non linear residual check-------------------------------------------
  residual_check solid_res_check;
  
  //------------------------integral---------------------------------------------------------  
  using solid_integral_type = solid<solid_ic_bc_type, dim>;
  solid_integral_type solid_integral(solid_ic_bc, solid_props, solid_u_model); 

  //-------------------------assembly--------------------------------------------------------
  using solid_assembly_type = solid_assembly<solid_integral_type, solid_dofs_ic_bc_type, dim>;
  solid_assembly_type solid_assembly;

  // ------------------parallel I/O---------------------------------------------------  
  IO solid_io(*solid_maps.state_map(), *solid_maps.dof_map());

  // ------------------parallel printing of rho, E, nu ------------------------------------
  Rho_E_Nu<dim> sec_dofs(*solid_maps.shared_node_map(), setup);  

















 //------------------ mesh building --------------------------------------------------------------------
  print_only_pid<0>(std::cerr)<< "HEAT EQUATION MESH\n";
  double mesh_read_start_T = MPI_Wtime();
  Mesh<dim> heat_eq_mesh(setup.heat_eq_mesh_file(), 1, setup);
  print_only_pid<0>(std::cerr)<<"Mesh reading took: "<<std::setprecision(setup.precision()) << MPI_Wtime()-mesh_read_start_T<<" s\n\n";
  

  //-----------------props------------------------------------------
  heat_eq_properties heat_eq_props;

  
  //----------------------maps-------------------------------------------
  epetra_maps heat_eq_maps(heat_eq_mesh);
  
  
  //-------------------- dofs state------------------------------------------------------
  auto heat_eq_dof_state = std::make_unique<dof_state>(heat_eq_maps.state_map()->NumMyElements(), 2);
  auto heat_eq_dot_dof_state = std::make_unique<dof_state>(heat_eq_maps.state_map()->NumMyElements(), 2);
  
  
  //-------------------- models  ----------------------------------------------------------------
  model<dim> heat_eq_model(heat_eq_mesh, *heat_eq_dof_state, heat_eq_maps, setup);
  model<dim> heat_eq_dot_model(heat_eq_mesh, *heat_eq_dot_dof_state, heat_eq_maps, setup);
  
  
  //-------------------- ic bc  -------------------------------------------------------------------
  using heat_eq_ic_bc_type = heat_eq_ic_bc<dim>;
  heat_eq_ic_bc_type heat_eq_ic_bc;
  
  //----------------------------------------dofs ic bc--------------------------------------------
  using heat_eq_dofs_ic_bc_type = heat_eq_dofs_ic_bc<heat_eq_ic_bc_type, dim>;
  heat_eq_dofs_ic_bc_type heat_eq_dofs_ic_bc(heat_eq_model, heat_eq_ic_bc, heat_eq_dot_model);
  
  
  //------------------- updater  --------------------------------------------------------------
  dofs_updater heat_eq_updater(setup);
  
  
  //---------------- linear system ---------------------------------------------------------
  linear_system heat_eq_lp(heat_eq_model);
  
  
  //---------------- linear solver -------------------------------------------------------
  belos_linear_solver heat_eq_ls_solver(setup);
  
  
  //--------------non linear residual check-------------------------------------------------
  residual_check heat_eq_res_check;
  
  
  //--------------------integral  ----------------------------------------------------------
  using heat_eq_integral_type = heat_eq<heat_eq_ic_bc_type, dim>;
  heat_eq_integral_type heat_eq_integral(heat_eq_ic_bc, heat_eq_props, heat_eq_model);
  
  
  //------------------ assembly  -----------------------------------------------------------
  heat_eq_assembly<heat_eq_integral_type, heat_eq_dofs_ic_bc_type, dim> heat_eq_assembly;
  
    
  // ------------------parallel I/O--------------------------------------------------------
  IO heat_eq_io(*heat_eq_maps.state_map(), *heat_eq_maps.dof_map());
  


