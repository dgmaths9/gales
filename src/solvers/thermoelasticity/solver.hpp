#include <mpi.h>
#include <iostream>
 

#include "../../fem/fem.hpp"
#include "thermoelasticity.hpp"





using namespace GALES;




template<int dim>
void solver(read_setup& setup)
{
  #include "constructors.hpp"

  
  
  

  time::get().t(0.0);
  time::get().delta_t(setup.delta_t());
    
  double restart_or_t_zero = MPI_Wtime();  
  if(!setup.restart())
  {
    //--------- setting up dofs for time = 0 ----------------
    solid_dofs_ic_bc.dirichlet_bc();
    heat_eq_dofs_ic_bc.dirichlet_bc();
    solid_io.write("solid/u/", *solid_u_dof_state);
    heat_eq_io.write("heat_eq/T/", *heat_eq_dof_state);
    if(setup.print_rho_E_nu())  sec_dofs.execute(solid_u_model, solid_props);
    if(setup.Gen_alpha()) heat_eq_io.write("heat_eq/T_dot/", *heat_eq_dot_dof_state); 
    print_only_pid<0>(std::cerr)<<"Dofs writing for time 0 took: "<<std::setprecision(setup.precision()) << MPI_Wtime()-restart_or_t_zero<<" s\n\n";       
  }
  else
  {
    //--------------read restart-----------------------------
    time::get().t(setup.restart_time());
    heat_eq_io.read("heat_eq/T/", *heat_eq_dof_state);
    if(setup.Gen_alpha()) heat_eq_io.read("heat_eq/T_dot/", *heat_eq_dot_dof_state);
    print_only_pid<0>(std::cerr)<<"Dofs reading at restart time " << time::get().t() << " took: "<<std::setprecision(setup.precision()) << MPI_Wtime()-restart_or_t_zero<<" s\n\n";       
  }
  time::get().tick();




  // --------------------------------TIME LOOP-----------------------------------------------------------
  double time_loop_start = MPI_Wtime();
  double non_linear_res(0.0);
  int tot_time_iterations(0);

  for(; time::get().t()<=setup.end_time(); time::get().tick())
  {
    double t_iteration(MPI_Wtime());
    
    print_only_pid<0>(std::cerr)<<"simulation time:  "<<std::setprecision(setup.precision()) << time::get().t()<<" s\n";

    if(setup.Gen_alpha()) heat_eq_updater.predictor(*heat_eq_dof_state, *heat_eq_dot_dof_state);
    else heat_eq_updater.predictor(*heat_eq_dof_state);
    
    solid_dofs_ic_bc.dirichlet_bc();
    heat_eq_dofs_ic_bc.dirichlet_bc();


    //------------------------ SOLUTION For solid ------------------------
    double fill_time = solid_assembly.execute(solid_integral, solid_dofs_ic_bc, solid_u_model, heat_eq_model, solid_lp);   
    double solver_time = solid_ls_solver.execute(solid_lp);
        

    solid_updater.solution(*solid_u_dof_state, solid_io.state_vec(*solid_ls_solver.solution()));
 

    print_solver_info("SOLID  ", fill_time, solver_time, solid_ls_solver);        

    // -----------------------end of solid solution---------------------------------------------------------
       





    for(int it_count=0; it_count < setup.n_max_it(); it_count++)   // iterations loop
    {
        //------------------------ SOLUTION For heat_eq ------------------------        
	double fill_time(0.0);
	if(setup.Gen_alpha()) fill_time = heat_eq_assembly.execute(heat_eq_integral, heat_eq_dofs_ic_bc, heat_eq_model, heat_eq_lp, heat_eq_dot_model); 
	else fill_time = heat_eq_assembly.execute(heat_eq_integral, heat_eq_dofs_ic_bc, heat_eq_model, heat_eq_lp);   
        
        
        bool residual_converged = heat_eq_res_check.execute(*heat_eq_lp.rhs(), it_count, non_linear_res);      
        if(residual_converged)
        {
          print_solver_info("HEAT   ", it_count, fill_time, non_linear_res);
      	  break;
        } 	      
        double solver_time = heat_eq_ls_solver.execute(heat_eq_lp);


        if(setup.Gen_alpha()) heat_eq_updater.corrector(*heat_eq_dof_state, *heat_eq_dot_dof_state, heat_eq_io.state_vec(*heat_eq_ls_solver.solution()));
     	else heat_eq_updater.corrector(*heat_eq_dof_state, heat_eq_io.state_vec(*heat_eq_ls_solver.solution()));


        print_solver_info("HEAT   ", it_count, fill_time, non_linear_res, solver_time, heat_eq_ls_solver);
     

    // -----------------------end of heat_eq solution---------------------------------------------------------
    }


    if((tot_time_iterations+1)%(setup.print_freq()) == 0)
    {
      solid_io.write("solid/u/", *solid_u_dof_state);
      if(setup.print_rho_E_nu())  sec_dofs.execute(solid_u_model, solid_props);
      heat_eq_io.write("heat_eq/T/", *heat_eq_dof_state);
      if(setup.Gen_alpha()) heat_eq_io.write("heat_eq/T_dot/", *heat_eq_dot_dof_state);
    }






    print_only_pid<0>(std::cerr)<<"Time step took: "<<MPI_Wtime()-t_iteration<<" s\n\n";
    tot_time_iterations++;    	
  }

  
  print_only_pid<0>(std::cerr)<<"End time reached!!!! "<<"\n\n";
  print_only_pid<0>(std::cerr)<<"Total run time: "<<MPI_Wtime()-time_loop_start<<" s\n\n\n";

}













int main(int argc, char* argv[])
{

  MPI_Init(&argc, &argv);



  //---------------- read setup file -------------------------------
  read_setup setup;



  //---------- creating directories ----------------------------------------------------------------------
  make_dirs("results/solid/u");
  make_dirs("results/heat_eq/T");




 
  if(setup.dim()==2)  solver<2>(setup);
  else if(setup.dim()==3)  solver<3>(setup);

  MPI_Barrier(MPI_COMM_WORLD);
  MPI_Finalize();
  return 0;
}
