#ifndef _GALES_THERMOELASTICITY_HPP_
#define _GALES_THERMOELASTICITY_HPP_


#include "assembly.hpp"
#include "dofs_ic_bc.hpp"
#include "solid_2d.hpp"
#include "heat_eq_2d.hpp"
#include "solid_3d.hpp"
#include "heat_eq_3d.hpp"

#endif 
