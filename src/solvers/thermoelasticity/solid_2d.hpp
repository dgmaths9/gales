#ifndef SOLID_2D_HPP
#define SOLID_2D_HPP




#include "../../fem/fem.hpp"



namespace GALES{
 
   
  /*
      Below we write t in place of theta, it is just a symbol to denote theta; it is not time.
      
      In this solver we implement 3 kinds of stress-strain element:
      1) plain stress     size along x1 & x2  >>> size long x3 (very thin) 
      2) plain strain     size along x1 & x2  <<< size long x3 (very thick) 
      3) axisymmetric(r,t,z)     symmetric along z-axis      (torsionless axisymmetric)
        
          x1 = r          x2 = z            x3 = t
          u1 = u_r        u2 = u_z          u3 = u_t = 0, 
             
          In axisymmetric formulation we do not multiply by 2*pi as it is common in all integrals and cancels out.
          
          tau_11 = sigma_rr       tau_22 = sigma_zz       tau_12 = sigma_rz         tau_33 = sigma_tt                         tau_13 = sigma_rt = 0        tau_23 = sigma_zt = 0
          eps_11 = eps_rr         eps_22 = eps_zz         2*eps_12 = 2*eps_rz       eps_33 = eps_tt = u_r/r = u1/x            eps_13 = eps_rt = 0          eps_23 = eps_zt = 0 

          tau_11 = sigma_rr = (lambda + 2*mu)*eps_11 + lambda*eps_22 + lambda*eps_33
          tau_22 = sigma_zz = lambda*eps_11 + (lambda + 2*mu)*eps_22 + lambda*eps_33
          tau_12 = sigma_rz = mu*(2*eps_12)
          tau_33 = sigma_tt = lambda*eps_11 + lambda*eps_22 + (lambda + 2*mu)*eps_33                    
  */        
   


  template<typename ic_bc_type, int dim> class solid{};

   
  template<typename ic_bc_type>
  class solid<ic_bc_type, 2>
  {
    using element_type = element<2>;
    using vec = boost::numeric::ublas::vector<double>;
    using mat = boost::numeric::ublas::matrix<double>;


   public :  

    solid
    (
     ic_bc_type& ic_bc,
     solid_properties& props,
     model<2>& model
    ):

      ic_bc_(ic_bc),
      props_(props),
      setup_(model.setup()),     

      JNxW_(2,0.0),
      JxW_(0.0),
      
      I_T_(0.0),
      
      condition_type_(props.condition_type()),      

      rho_(props.rho()),
      nu_(props.nu()),
      E_(props.E()),
      alpha_(props.alpha()),
      T_ref_(props.T_ref()),

      D_(3,3,0.0),
      
      gravity_(2,0.0)
     {      
       ic_bc_.body_force(gravity_);         
       if(condition_type_ == "Axisymmetric")  
          D_.resize(4,4);
     }

  
     void interpolation()
     {     
          quad_ptr_->interpolate(I_orig_T_, I_T_);
     }  
  



    void execute(const element_type& el, const vec& dofs_T, mat& m, vec& r)
    {
      nb_el_nodes_ = el.nb_nodes();      
      N_.resize(2,2*nb_el_nodes_, false);
      
      I_orig_T_ = dofs_T;

      if(condition_type_ == "Axisymmetric")   B_.resize(4,nb_el_nodes_*2,0.0);
      else B_.resize(3,nb_el_nodes_*2,0.0);

      auto nds = el.el_node_vec();
      for(int i=0; i<el.quad_i().size(); i++)
      {
        quad_ptr_ = el.quad_i()[i];
        JxW_ = quad_ptr_->JxW();
        interpolation();
        N();
 
        if(props_.heterogeneous())
        {
          props_.properties(nds[i]->get_x(), nds[i]->get_y());
          rho_ = props_.rho();
          E_ = props_.E();
          nu_ = props_.nu();
          alpha_ = props_.alpha();
          T_ref_ = props_.T_ref();
        }

    
         //------------------------------Mat2---------------------------------------------------------------------------------------------
          B();
          D();
          const mat use1 = prod(D_,B_);         
          mat Mat2 = prod(boost::numeric::ublas::trans(B_),use1)*JxW_;
          
          if(condition_type_ == "Axisymmetric") 
            Mat2 = quad_ptr_->x()*Mat2;
    
          m += Mat2;
    
    
          //----------------------------vect4------------------------------------------------------------------------------------------------
          vec Vect4 = prod(boost::numeric::ublas::trans(N_),gravity_)*rho_*JxW_;
//---------------------------vect5-------------------------------------------------------------------------------                    
          const mat use2 = prod(boost::numeric::ublas::trans(B_),D_);
          vec thermal_strain(3,0.0);
          thermal_strain[0] = alpha_*(I_T_-T_ref_);
          thermal_strain[1] = alpha_*(I_T_-T_ref_); 
          vec Vect5 = prod(use2,thermal_strain)*JxW_;
          
          if(condition_type_ == "Axisymmetric") {
            Vect4 = quad_ptr_->x()*Vect4;
            Vect5 = quad_ptr_->x()*Vect5;
            }
         
          r += Vect4+Vect5; 
      }



      if(el.on_boundary())
      {
         for(int i=0; i<el.nb_sides(); i++)
         {
           if(el.is_side_on_boundary(i))
           {
             auto bd_nodes = el.side_nodes(i);
             auto side_flag = el.side_flag(i);
             
             // vector of pointers to nodes of the side
             std::vector<std::shared_ptr<node<2>>> nds_side;
             // compare gid of element nodes and gid of side nodes to fill the vector of pointers to side nodes
             for (int i=0; i<bd_nodes.size(); i++) 
               for (int j=0; j<nds.size(); j++) 
                 if(bd_nodes[i] == nds[j]->gid())
                   nds_side.push_back(nds[j]);
   
             if(side_flag==1)
             {            
               std::vector<std::vector<double>> f_tr;
               ic_bc_.get_fluid_tr(bd_nodes, f_tr);            
   
               for(int j=0; j<el.nb_side_gp(i); j++)
               {
                 quad_ptr_ = el.quad_b(i,j);
                 
                 N();
                 vec tr(2);
                 for(int i=0; i<2; i++)
                  tr[i] = -f_tr[j][i];
                        
                 r += prod(boost::numeric::ublas::trans(N_), tr)*quad_ptr_->W_bd();
                 
               }
             }
             else
             {
                double tau11(0.0), tau22(0.0);
                double tau12(0.0), tau21(0.0);
                double p(0.0);
           
                if(ic_bc_.neumann_tau11(bd_nodes,side_flag).first)      tau11 = ic_bc_.neumann_tau11(bd_nodes,side_flag).second;
                if(ic_bc_.neumann_tau22(bd_nodes,side_flag).first)      tau22 = ic_bc_.neumann_tau22(bd_nodes,side_flag).second;
                if(ic_bc_.neumann_tau12(bd_nodes,side_flag).first)      tau12 = ic_bc_.neumann_tau12(bd_nodes,side_flag).second;
                //if(ic_bc_.neumann_tau11(nds_side,side_flag).first)      tau11 = ic_bc_.neumann_tau11(nds_side,side_flag).second;
                //if(ic_bc_.neumann_tau22(nds_side,side_flag).first)      tau22 = ic_bc_.neumann_tau22(nds_side,side_flag).second;
                //if(ic_bc_.neumann_tau12(nds_side,side_flag).first)      tau12 = ic_bc_.neumann_tau12(nds_side,side_flag).second;
                if(ic_bc_.neumann_pressure(bd_nodes,side_flag).first)     p = ic_bc_.neumann_pressure(bd_nodes,side_flag).second; 
                tau21 = tau12;
   
                for(const auto& quad_ptr: el.quad_b(i))
                {
                 quad_ptr_ = quad_ptr;               
                 JNxW_ = quad_ptr_->JNxW();
   
                 vec traction(2,0.0);        
                 traction[0] = (tau11-p)*JNxW_[0] + tau12*JNxW_[1];
                 traction[1] = tau21*JNxW_[0] + (tau22-p)*JNxW_[1];
           
                 N();      
                 vec Vect5 = prod(boost::numeric::ublas::trans(N_),traction);
                 if(condition_type_ == "Axisymmetric")
                   Vect5 = quad_ptr_->x()*Vect5;
                 
                 r += Vect5;
               }
             }
           }
         }            
      }
    }











  void D()
  {
    D_.clear();
    
//    //--------------radially changing parameters--------------------------------
//    const double x = quad_ptr_->x();
//    const double y = quad_ptr_->y();
//    const double r = sqrt(x*x + (y+2000.0)*(y+2000.0));  // distance from point (0, -2000)
//    const double T = 900.0*1000.0/r + 100.0; 
//    E_ = 10.e9*(1.0-0.5*(exp(T/1000.0)-1.0));
//    nu_ = (1.0-E_/10.e9)*0.15 + 0.25; 
//    //------------------------------------------------------------

    const double mu = 0.5*E_/(1.0 + nu_);    
    double lambda(0.0);
    if(condition_type_ == "Plain_Strain" || condition_type_ == "Axisymmetric")  lambda = E_*nu_/((1.0+nu_)*(1.0-2.0*nu_));
    else if(condition_type_ == "Plain_Stress")  lambda = E_*nu_/(1.0-nu_*nu_);
    
    D_(0,0) = lambda + 2.0*mu;
    D_(0,1) = lambda;
    D_(1,0) = lambda;
    D_(1,1) = lambda + 2.0*mu;
    D_(2,2) = mu;        

    if(condition_type_ == "Axisymmetric")
    {         
      D_(3,0) = lambda;
      D_(0,3) = lambda;
      D_(3,1) = lambda;
      D_(1,3) = lambda;
      D_(3,3) = lambda + 2.0*mu;
    } 
  }




   void N()
   {
    N_.clear();
    for(int i=0; i<nb_el_nodes_; i++)
    {
       N_(0,2*i) = quad_ptr_->sh(i);
       N_(1,2*i+1) = quad_ptr_->sh(i);
    }   
   }



   
   void B()
   {
    B_.clear();        
    for(int i=0; i<nb_el_nodes_; i++)
    {
       B_(0,2*i) = quad_ptr_->dsh_dx(i);
       B_(1,2*i+1) = quad_ptr_->dsh_dy(i);
       B_(2,2*i)   = quad_ptr_->dsh_dy(i);
       B_(2,2*i+1) = quad_ptr_->dsh_dx(i);
    }   
    if(condition_type_ == "Axisymmetric")
    {         
      for(int i=0; i<nb_el_nodes_; i++)
         B_(3,2*i) = quad_ptr_->sh(i)/quad_ptr_->x();
    }
   }





   private:

    int nb_el_nodes_;
    
    ic_bc_type& ic_bc_;
    solid_properties& props_;
    read_setup& setup_;

    double JxW_; 
    vec JNxW_;

    std::string condition_type_;
    double rho_, nu_, E_, alpha_;

    mat N_;
    mat D_;      
    mat B_;

    vec gravity_;   
    
    vec I_orig_T_;
    double I_T_;
    double T_ref_;

    std::shared_ptr<quad> quad_ptr_ = nullptr;
    point<2> dummy_;    
  }; 






 
} 

#endif

