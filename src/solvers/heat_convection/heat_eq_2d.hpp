#ifndef HEAT_EQ_2D_HPP
#define HEAT_EQ_2D_HPP




#include "../../fem/fem.hpp"



namespace GALES{
 
   
   
      
   


  template<typename ic_bc_type, int dim> class heat_eq{};


   
  template<typename ic_bc_type>
  class heat_eq<ic_bc_type, 2>
  {
    using element_type = element<2>;
    using vec = boost::numeric::ublas::vector<double>;
    using mat = boost::numeric::ublas::matrix<double>;


   public :  
 
      heat_eq(ic_bc_type& ic_bc, heat_eq_properties& props, model<2>& model)
      :
      ic_bc_(ic_bc),
      props_(props),
      setup_(model.setup()),

      JxW_(0.0),
      JNxW_(2,0.0),
      dt_(0.0), 

      rho_(0.0),
      cp_(0.0),
      cv_(0.0),
      mu_(0.0),
      kappa_(0.0),
      alpha_(0.0),
      
      P_(0.0),
      I_(0.0),
      I_dot_(0.0),

      dI_dx_(0.0),
      dI_dy_(0.0),      
           
      tol_(std::numeric_limits<double>::epsilon())
      {
        alpha_m_ = 0.5*(3.0-setup_.rho_inf())/(1.0+setup_.rho_inf());
        alpha_f_ = 1.0/(1.0+setup_.rho_inf());
        gamma_ = 0.5 + alpha_m_ - alpha_f_;
        T_ref_ = ic_bc_.T_ref(); 

        if(props_.get_material_type() == "custom")
        {
          props_.properties();
          rho_ = props_.rho();
          cp_ = props_.cp();
          kappa_ = props_.kappa();
          alpha_ = props_.alpha();
        }                
      } 

  
          
  


    void get_properties(double x, double y, double p,  double T)
    {
       if(props_.get_material_type() == "y-dependent-class")
       {
          props_.properties(x,y,p,T);
          rho_ = props_.rho();
          cp_ = props_.cp();
          mu_ = props_.mu();
          kappa_ = props_.kappa();
          alpha_ = props_.alpha();        
       }        
    }

    
    


    vec get_v_el(const element_type& el)
    {
      vec v_el(2*nb_el_nodes_,0.0);          
      for(int i=0; i<nb_el_nodes_; i++)
      {
        vec v(2, 0.0);
        ic_bc_.get_v(el.nd_gid(i), v);       
        for(int j=0; j<2; j++)
          v_el[2*i+j] = v[j];
      }
      return v_el;    
    }
     




    vec get_p_el(const element_type& el)
    {
      vec p_el(nb_el_nodes_,0.0);          

      for(int i=0; i<nb_el_nodes_; i++)
        ic_bc_.get_p(el.nd_gid(i), p_el[i]);

      return p_el;    
    }





     // This is for Euler-backward method
    void execute(const element_type& el, const std::vector<vec>& dofs_T, mat& m, vec& r)
    {
      nb_el_nodes_ = el.nb_nodes();
      dt_= time::get().delta_t();

      vec P_dofs = dofs_T[1];
      vec I_dofs = dofs_T[0];
      
      auto v_el = get_v_el(el);
      auto p_el = get_p_el(el);
      double p(0.0), dp_dx(0.0), dp_dy(0.0);

      vec v(2, 0.0);
      vec dv_dx(2,0.0);
      vec dv_dy(2,0.0);
      
           

      for(int i=0; i<el.quad_i().size(); i++)
      {
          quad_ptr_ = el.quad_i()[i];
          JxW_ = quad_ptr_->JxW();
          quad_ptr_->interpolate(p_el, p);
          quad_ptr_->x_derivative_interpolate(p_el, dp_dx);
          quad_ptr_->y_derivative_interpolate(p_el, dp_dy);

          quad_ptr_->interpolate(v_el, v);         
          quad_ptr_->x_derivative_interpolate(v_el, dv_dx);
          quad_ptr_->y_derivative_interpolate(v_el, dv_dy);
         
          double vx = v[0];
          double vy = v[1];
          const double v1_1(dv_dx[0]), v1_2(dv_dy[0]);
          const double v2_1(dv_dx[1]), v2_2(dv_dy[1]);      

          const double lambda = -2.0*mu_/3.0;
          double tau11 = lambda*(v1_1 + v2_2) + 2.0*mu_*v1_1;
          double tau22 = lambda*(v1_1 + v2_2) + 2.0*mu_*v2_2;
          double tau12 = mu_*(v1_2 + v2_1);
          
          quad_ptr_->interpolate(P_dofs, P_);                    
          quad_ptr_->interpolate(I_dofs, I_);
          quad_ptr_->x_derivative_interpolate(I_dofs, dI_dx_);
          quad_ptr_->y_derivative_interpolate(I_dofs, dI_dy_);
          get_properties(el.get_x(i), el.get_y(i), p, I_);                

                                                    
          for(int b=0; b<nb_el_nodes_; b++)
          {
               r[b] += quad_ptr_->sh(b)*rho_*cp_*(I_ - P_)/dt_*JxW_;
               r[b] += quad_ptr_->sh(b)*rho_*cp_*vx*dI_dx_*JxW_;
               r[b] += quad_ptr_->sh(b)*rho_*cp_*vy*dI_dy_*JxW_;
               r[b] += quad_ptr_->dsh_dx(b)*kappa_*dI_dx_*JxW_;
               r[b] += quad_ptr_->dsh_dy(b)*kappa_*dI_dy_*JxW_;
               r[b] -= quad_ptr_->sh(b)*vx*dp_dx*alpha_*I_*JxW_;
               r[b] -= quad_ptr_->sh(b)*vy*dp_dy*alpha_*I_*JxW_;
               r[b] -= quad_ptr_->sh(b)*(tau11*v1_1 + tau12*v2_1 + tau12*v1_2 + tau22*v2_2)*JxW_;
          }
  

          for(int b=0; b<nb_el_nodes_; b++)
           for(int a=0; a<nb_el_nodes_; a++)
           {
               m(b,a) += quad_ptr_->sh(b)*rho_*cp_*quad_ptr_->sh(a)/dt_*JxW_;
               m(b,a) += quad_ptr_->sh(b)*rho_*cp_*vx *quad_ptr_->dsh_dx(a)*JxW_;
               m(b,a) += quad_ptr_->sh(b)*rho_*cp_*vy *quad_ptr_->dsh_dy(a)*JxW_; 
               m(b,a) += quad_ptr_->dsh_dx(b)* kappa_ *quad_ptr_->dsh_dx(a)*JxW_;
               m(b,a) += quad_ptr_->dsh_dy(b)* kappa_ *quad_ptr_->dsh_dy(a)*JxW_; 
               m(b,a) -= quad_ptr_->sh(b)*vx*dp_dx*alpha_*quad_ptr_->sh(a)*JxW_;
               m(b,a) -= quad_ptr_->sh(b)*vy*dp_dy*alpha_*quad_ptr_->sh(a)*JxW_;                        
           }                              




         //--------------------------------------------R_LeastSquares--------------------------------------------------------------------------        
         const double tau = tau_stab(vx, vy, v_el);
         double Res = rho_*cp_*((I_ - P_)/dt_ +  vx*dI_dx_ + vy*dI_dy_) - (vx*dp_dx + vy*dp_dy)*alpha_*I_ - (tau11*v1_1 + tau12*v2_1 + tau12*v1_2 + tau22*v2_2);                              
                          
         for(int b=0; b<nb_el_nodes_; b++)
            r[b] += (vx*quad_ptr_->dsh_dx(b) + vy*quad_ptr_->dsh_dy(b))*tau*Res*JxW_;


         //--------------------------------------------M_LeastSquares--------------------------------------------------------------------------
         for(int a=0; a<nb_el_nodes_; a++)
         {
             auto dRes_dT = rho_*cp_*(quad_ptr_->sh(a)/dt_ + vx*quad_ptr_->dsh_dx(a) + vy*quad_ptr_->dsh_dy(a)) - (vx*dp_dx + vy*dp_dy)*alpha_*quad_ptr_->sh(a);                                        
                                            
             for(int b=0; b<nb_el_nodes_; b++)
                 m(b,a) += (vx*quad_ptr_->dsh_dx(b) + vy*quad_ptr_->dsh_dy(b))*tau*dRes_dT*JxW_;
         }



         //-----------------dc------------------------
         double dc(0.0);
         if(setup_.dc_2006()) dc = dc_stab(Res);   

         for(int b=0;b<nb_el_nodes_;b++)
         {
             r[b] += quad_ptr_->dsh_dx(b)*dc*dI_dx_*JxW_;
             r[b] += quad_ptr_->dsh_dy(b)*dc*dI_dy_*JxW_;
         }
         for(int b=0;b<nb_el_nodes_;b++)
         for(int a=0;a<nb_el_nodes_;a++)
         {
             m(b,a) += dc*quad_ptr_->dsh_dx(b)*quad_ptr_->dsh_dx(a)*JxW_;
             m(b,a) += dc*quad_ptr_->dsh_dy(b)*quad_ptr_->dsh_dy(a)*JxW_;
         }
      }                     

     
     
      if(el.on_boundary())
      {
         for(int i=0; i<el.nb_sides(); i++)
         {
           if(el.is_side_on_boundary(i))
           {
             auto bd_nodes = el.side_nodes(i);
             auto side_flag = el.side_flag(i);
   
             if(side_flag==1)
             {            
               std::vector<std::vector<double>> f_heat_flux;
               ic_bc_.get_fluid_heat_flux(bd_nodes, f_heat_flux);            
               
               for(int j=0; j<el.nb_side_gp(i); j++)
               {
                 quad_ptr_ = el.quad_b(i,j);
                 
                 vec q(2,0.0);
                 q[0] = -f_heat_flux[j][0];
                 q[1] = -f_heat_flux[j][1];
               
                 for (int b=0; b<nb_el_nodes_; b++)
                   r[b] += quad_ptr_->sh(b)*(q[0]+q[1])*quad_ptr_->W_bd();
               }
             }
             else
             {             
               for(int j=0; j<el.quad_b(i).size(); j++)
               {
                   quad_ptr_ = el.quad_b(i,j);               
                   JNxW_ = quad_ptr_->JNxW();
                   quad_ptr_->interpolate(p_el, p);
                   quad_ptr_->interpolate(I_dofs, I_);
                   quad_ptr_->x_derivative_interpolate(I_dofs, dI_dx_);
                   quad_ptr_->y_derivative_interpolate(I_dofs, dI_dy_);
                   get_properties(el.get_side_x(i,j), el.get_side_x(i,j), p, I_);                
                  
                   vec q(2, 0.0), K(2, -kappa_);
                   q[0] = -kappa_*dI_dx_;
                   q[1] = -kappa_*dI_dy_;
                                                 
                   if(ic_bc_.neumann_q1(bd_nodes, side_flag).first){  q[0] = ic_bc_.neumann_q1(bd_nodes, side_flag).second;  K[0] = 0.0;}
                   if(ic_bc_.neumann_q2(bd_nodes, side_flag).first){  q[1] = ic_bc_.neumann_q2(bd_nodes, side_flag).second;  K[1] = 0.0;}
                      
                   for (int b=0; b<nb_el_nodes_; b++)
                     r[b] += quad_ptr_->sh(b)*(q[0]*JNxW_[0] + q[1]*JNxW_[1]);
                   
                   for(int b=0; b<nb_el_nodes_; b++)
                   for(int a=0; a<nb_el_nodes_; a++)
                     m(b,a) += quad_ptr_->sh(b)*(K[0]*quad_ptr_->dsh_dx(a)*JNxW_[0] + K[1]*quad_ptr_->dsh_dy(a)*JNxW_[1]);
                     
               }
             }                   
           }
         }            
      }


      r *= -1.0;      
    }








      double tau_stab(double vx,  double vy, const vec& v_el)
      {
         double tau(0.0);
         const double v_nrm = sqrt(vx*vx + vy*vy);
         if(v_nrm < tol_) return tau;
         vec v(2,0.0);
         v[0] = vx;    v[1] = vy;  
                        
                  
         if(setup_.tau_non_diag_comp_2019())
         {         
              vec dv_dx(2, 0.0), dv_dy(2, 0.0);
              quad_ptr_->x_derivative_interpolate(v_el, dv_dx);
              quad_ptr_->y_derivative_interpolate(v_el, dv_dy);
     
              mat del_v(2,2,0.0);
              for(int i=0; i<2; i++)
              {
                  del_v(i,0) = dv_dx[i];       del_v(i,1) = dv_dy[i];     
              }
              const auto alpha = quad_ptr_->alpha(v, del_v, dummy_);
              const auto h = quad_ptr_->compute_h(alpha, dummy_);
              
              const double use =  std::min(dt_*0.5, h/(2.0*v_nrm));
              tau = std::min(use, h*h*rho_*cp_/(12.0*kappa_));
              
//              const double Pe = v_nrm*h*rho_*cp_/(2.0*kappa_); 
//              tau = h/(2*v_nrm)*std::min(1.0, Pe/3.0);                
         }
         
         if(setup_.tau_diag_incomp_2007())
         {
              const auto G = quad_ptr_->G();
              double use(0.0);
              for(int j=0; j<2; j++)
               for(int k=0; k<2; k++)
                 use += G(j,k)*G(j,k);
                
              double C_t(0.0);
              if(!setup_.steady_state()) C_t = 4.0;
               
              tau = sqrt(C_t/(dt_*dt_) + boost::numeric::ublas::inner_prod(v, prod(G, v)) + 9.0*use*kappa_*kappa_/(rho_*rho_*cp_*cp_));
              if(tau > tol_) tau = 1.0/tau;              
         }              
         return tau;
      }
  







      double dc_stab(double Res)
      {           
           if(setup_.dc_2006())
           {  
             const double del_T_nrm = sqrt(dI_dx_*dI_dx_ + dI_dy_*dI_dy_);
             if(del_T_nrm < tol_) return 0.0;
             vec J(2,0.0);
             J[0] = dI_dx_/del_T_nrm;
             J[1] = dI_dy_/del_T_nrm;
             double h(0.0);
             for(int i=0; i<nb_el_nodes_; i++)
             {
                 h += abs(J[0]*quad_ptr_->dsh_dx(i) + J[1]*quad_ptr_->dsh_dy(i));
             }
             if(h > tol_) h = 1.0/h;
             else return 0.0;
             

             const double T_ref_inv = 1.0/T_ref_;
             
             const double T_ref_inv_Res_nrm = abs(T_ref_inv*Res);
             const double a = abs(T_ref_inv*dI_dx_);
             const double b = abs(T_ref_inv*dI_dy_);

             const double s = setup_.dc_sharp();
             if(s == 1.0)
             {
               if(a*a+b*b < tol_) return 0.0;
               else return setup_.dc_scale_fact()*T_ref_inv_Res_nrm*h*1.0/sqrt(a*a+b*b);
             }  
             else if(s == 2.0) 
             {               
               return setup_.dc_scale_fact()*T_ref_inv_Res_nrm*h*h;
             }  
           }
           return 0.0;              
      }
  


    void execute(const element_type& el, const std::vector<vec>& dofs_T, mat& m, vec& r, const std::vector<vec>& dofs_T_dot)
    {
    }





   private:

    int nb_el_nodes_;
    
    ic_bc_type& ic_bc_;
    heat_eq_properties& props_;
    read_setup& setup_;

    double JxW_; 
    vec JNxW_;
    double dt_;


    double rho_, cp_, cv_, kappa_, alpha_, mu_;
    double P_, I_, I_dot_;
    double dI_dx_, dI_dy_;
    double T_ref_;
            

    double tol_;
    std::shared_ptr<quad> quad_ptr_ = nullptr;
    point<2> dummy_;    
    double alpha_f_, alpha_m_, gamma_;
  }; 






 
} 

#endif

