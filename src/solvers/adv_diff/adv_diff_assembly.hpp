#ifndef ADV_DIFF_ASSEMBLY_HPP_
#define ADV_DIFF_ASSEMBLY_HPP_


#include "../../fem/fem.hpp"



namespace GALES {





  template<typename integral_type, typename dofs_ic_bc_type, int dim>
  class adv_diff_assembly
  {
      using model_type = model<dim>;
      using vec = boost::numeric::ublas::vector<double>;
      using mat = boost::numeric::ublas::matrix<double>;

    public:
    

     // This is for Euler-backward method
    double execute(integral_type& integral, dofs_ic_bc_type& dofs_ic_bc, model_type& model, linear_system& lp)
    {
      double start(MPI_Wtime());      
      
      std::vector<vec> dofs_adv_diff(model.state().num_slot());

      lp.clear();
      for(const auto& el: model.mesh().elements())
      {        
	 model.extract_element_dofs(*el, dofs_adv_diff);
         const int nb_dofs = dofs_adv_diff[0].size();
      
         vec r(nb_dofs, 0.0);
         mat m(nb_dofs, nb_dofs, 0.0);

	 integral.execute(*el, dofs_adv_diff, m, r);
	 lp.assemble(el->dofs_gids(), el->dofs_constraints(dofs_ic_bc), m, r);
      }

      lp.assembled();       
      return MPI_Wtime()-start;                  
    }




     // this is for generalized-alpha method
    double execute(integral_type& integral, dofs_ic_bc_type& dofs_ic_bc, model_type& model, linear_system& lp, model_type& dot_model)
    {
      double start(MPI_Wtime());      
      
      std::vector<vec> dofs_adv_diff(model.state().num_slot());
      std::vector<vec> dofs_adv_diff_dot(dot_model.state().num_slot());

      lp.clear();
      for(const auto& el: model.mesh().elements())
      {
	 model.extract_element_dofs(*el, dofs_adv_diff);
	 dot_model.extract_element_dofs(*el, dofs_adv_diff_dot);
      
         const int nb_dofs = dofs_adv_diff[0].size();
         vec r(nb_dofs, 0.0);
         mat m(nb_dofs, nb_dofs, 0.0);

	 integral.execute(*el, dofs_adv_diff, m, r, dofs_adv_diff_dot);
	 lp.assemble(el->dofs_gids(), el->dofs_constraints(dofs_ic_bc), m, r);
      }

      lp.assembled();       
      return MPI_Wtime()-start;                  
    }
   
  };



}//namespace GALES
#endif
