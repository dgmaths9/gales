#include <mpi.h>
#include <iostream>
 

#include "../../fem/fem.hpp"
#include "adv_diff.hpp"





using namespace GALES;




template<int dim>
void solver(read_setup& setup)
{
  #include "constructors.hpp"

  
  
  

  time::get().t(0.0);
  time::get().delta_t(setup.delta_t());
    
  double restart_or_t_zero = MPI_Wtime();  
  if(!setup.restart())
  {
    //--------- setting up dofs for time = 0 ----------------
    adv_diff_dofs_ic_bc.dirichlet_bc();
    adv_diff_io.write("Y/", *adv_diff_dof_state);
    if(setup.Gen_alpha()) adv_diff_io.write("Y_dot/", *adv_diff_dot_dof_state);
    print_only_pid<0>(std::cerr)<<"Dofs writing for time 0 took: "<<std::setprecision(setup.precision()) << MPI_Wtime()-restart_or_t_zero<<" s\n\n";       
  }
  else
  {
    //--------------read restart-----------------------------
    time::get().t(setup.restart_time());
    adv_diff_io.read("Y/", *adv_diff_dof_state);
    if(setup.Gen_alpha()) adv_diff_io.read("Y_dot/", *adv_diff_dot_dof_state);
    print_only_pid<0>(std::cerr)<<"Dofs reading at restart time " << time::get().t() << " took: "<<std::setprecision(setup.precision()) << MPI_Wtime()-restart_or_t_zero<<" s\n\n";       
  }
  time::get().tick();




  // --------------------------------TIME LOOP-----------------------------------------------------------
  double time_loop_start = MPI_Wtime();
  double non_linear_res(0.0);
  int tot_time_iterations(0);


  for(; time::get().t()<=setup.end_time(); time::get().tick())
  {
      double t_iteration(MPI_Wtime());
        
      print_only_pid<0>(std::cerr)<<"simulation time:  "<<std::setprecision(setup.precision()) << time::get().t()<<" s\n";


      if(setup.Gen_alpha())  adv_diff_updater.predictor(*adv_diff_dof_state, *adv_diff_dot_dof_state);
      else  adv_diff_updater.predictor(*adv_diff_dof_state);


      adv_diff_dofs_ic_bc.dirichlet_bc();




      for(int it_count=0; it_count < setup.n_max_it(); it_count++)   //iterations loop
      {
        //------------------------ SOLUTION For adv_diff ------------------------

        double fill_time(0.0); 
        if(setup.Gen_alpha())  fill_time = adv_diff_assembly.execute(adv_diff_integral, adv_diff_dofs_ic_bc, adv_diff_model, adv_diff_lp, adv_diff_dot_model);   
        else fill_time = adv_diff_assembly.execute(adv_diff_integral, adv_diff_dofs_ic_bc, adv_diff_model, adv_diff_lp);


        bool residual_converged = adv_diff_res_check.execute(*adv_diff_lp.rhs(), it_count, non_linear_res);      
        if(residual_converged)
        {
          print_solver_info("Adv_Diff  ", it_count, fill_time, non_linear_res);
          break;
        } 	              
        double solver_time = adv_diff_ls_solver.execute(adv_diff_lp);


        if(setup.Gen_alpha())  adv_diff_updater.corrector(*adv_diff_dof_state, *adv_diff_dot_dof_state, adv_diff_io.state_vec(*adv_diff_ls_solver.solution()));
	else adv_diff_updater.corrector(*adv_diff_dof_state, adv_diff_io.state_vec(*adv_diff_ls_solver.solution()));

        
        print_solver_info("Adv_Diff  ", it_count, fill_time, non_linear_res, solver_time, adv_diff_ls_solver);        

        // -----------------------end of adv_diff solution---------------------------------------------------------
       }
       



    if((tot_time_iterations+1)%(setup.print_freq()) == 0)
    {
       adv_diff_io.write("Y/", *adv_diff_dof_state);
       if(setup.Gen_alpha())  adv_diff_io.write("Y_dot/", *adv_diff_dot_dof_state);
    }       


    print_only_pid<0>(std::cerr)<<"Time step took: "<<MPI_Wtime()-t_iteration<<" s\n\n";
    tot_time_iterations++;    	
  }

  
  print_only_pid<0>(std::cerr)<<"End time reached!!!! "<<"\n\n";
  print_only_pid<0>(std::cerr)<<"Total run time: "<<MPI_Wtime()-time_loop_start<<" s\n\n\n";

}







int main(int argc, char* argv[])
{

  MPI_Init(&argc, &argv);



  //---------------- read setup file -------------------------------
  read_setup setup;



  //---------- creating directories ----------------------------------------------------------------------
  make_dirs("results/Y");
  if(setup.Gen_alpha()) make_dirs("results/Y_dot");




  if(setup.dim()==2)  solver<2>(setup);
//  else if(setup.dim()==3)  solver<3>(setup);


  MPI_Barrier(MPI_COMM_WORLD);
  MPI_Finalize();
  return 0;
}
