#ifndef _GALES_ADV_DIFF_HPP_
#define _GALES_ADV_DIFF_HPP_


#include "adv_diff_assembly.hpp"
#include "adv_diff_dofs_ic_bc.hpp"
#include "adv_diff_2d.hpp"

#endif 
