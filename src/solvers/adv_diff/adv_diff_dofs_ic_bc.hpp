#ifndef _GALES_ADV_DIFF_DOFS_IC_BC_HPP_
#define _GALES_ADV_DIFF_DOFS_IC_BC_HPP_


#include"../../fem/fem.hpp"



namespace GALES {






  template<typename ic_bc_type, int dim>
  class adv_diff_dofs_ic_bc
  {
      using model_type = model<dim>;
      
      public:
      
      adv_diff_dofs_ic_bc(model<dim>& model, ic_bc_type& ic_bc, model_type& dot_model)
      :  
      model_(model), ic_bc_(ic_bc), dot_model_(dot_model)
      {
        ic();
        const double alpha_m = 0.5*(3.0-model.setup().rho_inf())/(1.0+model.setup().rho_inf());
        const double alpha_f = 1.0/(1.0+model.setup().rho_inf());
        gamma_ = 0.5 + alpha_m - alpha_f;
      }


      void ic()
      {
        for(const auto& nd : model_.mesh().nodes())
          model_.state().set_dof(nd->first_dof_lid(), ic_bc_.initial_Y(*nd));
      }



      void dirichlet_bc()
      {
        const double dt(time::get().delta_t());  
        for(const auto& nd : model_.mesh().nodes())
        {
          auto result = ic_bc_.dirichlet_Y(*nd);
          if (result.first)          
          {
             model_.state().set_dof(nd->first_dof_lid(), result.second);          
             if(model_.setup().Gen_alpha())
             {             
               auto n_1 = result.second;
               auto n = model_.extract_node_dof(nd->lid(), 0, 1);
               auto dot_n = dot_model_.extract_node_dof(nd->lid(), 0, 1);
               dot_model_.state().set_dof(nd->first_dof_lid(), (n_1 - n)/(gamma_*dt) - (1.0-gamma_)/gamma_*dot_n);
             }
          } 
        }
      }



    auto dofs_constraints(const node<dim> &nd)const  
    {
      std::vector<std::pair<bool,double>> nd_dofs_constraints(nd.nb_dofs(), std::make_pair(false, 0.0));              
      if(ic_bc_.dirichlet_Y(nd).first) nd_dofs_constraints[0] = std::make_pair(true, 0.0);
      return nd_dofs_constraints;
    }


    private:
    model_type& model_;
    model_type& dot_model_;
    ic_bc_type& ic_bc_;
    double gamma_;
  };



} /* namespace GALES */

#endif

