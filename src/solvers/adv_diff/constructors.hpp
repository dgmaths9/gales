
  //------------------ mesh building --------------------------------------------------------------------
  print_only_pid<0>(std::cerr)<< "adv_diff MESH\n";
  double mesh_read_start = MPI_Wtime();
  Mesh<dim> adv_diff_mesh(setup.adv_diff_mesh_file(), 1, setup);
  print_only_pid<0>(std::cerr)<<"Mesh reading took: "<<std::setprecision(setup.precision()) << MPI_Wtime()-mesh_read_start<<" s\n\n";



  //-----------------adv_diff props------------------------------------------
  adv_diff_properties adv_diff_props;

  //-----------------------------------------maps------------------------------------------------------------
  epetra_maps adv_diff_maps(adv_diff_mesh);
  
  //----------------------------------------dofs state-----------------------------------------------------  
  auto adv_diff_dof_state = std::make_unique<dof_state>(adv_diff_maps.state_map()->NumMyElements(), 2);
  auto adv_diff_dot_dof_state = std::make_unique<dof_state>(adv_diff_maps.state_map()->NumMyElements(), 2);
  
  //---------------------------------------model---------------------------------------------------
  model<dim> adv_diff_model(adv_diff_mesh, *adv_diff_dof_state, adv_diff_maps, setup);  
  model<dim> adv_diff_dot_model(adv_diff_mesh, *adv_diff_dot_dof_state, adv_diff_maps, setup);  

  //----------------------------------------ic bc---------------------------------------------------------
  using adv_diff_ic_bc_type = adv_diff_ic_bc<dim>;
  adv_diff_ic_bc_type adv_diff_ic_bc;
  
  //----------------------------------------dofs ic bc---------------------------------------------------------
  using adv_diff_dofs_ic_bc_type = adv_diff_dofs_ic_bc<adv_diff_ic_bc_type, dim>;
  adv_diff_dofs_ic_bc_type adv_diff_dofs_ic_bc(adv_diff_model, adv_diff_ic_bc, adv_diff_dot_model);
      
  //---------------------updater------------------------------------------------------
  dofs_updater adv_diff_updater(setup);
      
  //--------------------linear system-------------------------------------------------
  linear_system adv_diff_lp(adv_diff_model);
  
        
  //--------------------linear solver----------------------------------------------------
  belos_linear_solver adv_diff_ls_solver(setup); 

  //---------------------non linear residual check-------------------------------------------
  residual_check adv_diff_res_check;
  
  //------------------------integral---------------------------------------------------------  
  using adv_diff_integral_type = adv_diff<adv_diff_ic_bc_type, dim>;
  adv_diff_integral_type adv_diff_integral(adv_diff_ic_bc, adv_diff_props, adv_diff_model); 

  //-------------------------assembly--------------------------------------------------------
  using adv_diff_assembly_type = adv_diff_assembly<adv_diff_integral_type, adv_diff_dofs_ic_bc_type, dim>;
  adv_diff_assembly_type adv_diff_assembly;

  // ------------------parallel I/O---------------------------------------------------  
  IO adv_diff_io(*adv_diff_maps.state_map(), *adv_diff_maps.dof_map());


