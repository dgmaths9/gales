#ifndef _ADV_DIFF_2D_HPP_
#define _ADV_DIFF_2D_HPP_



#include "../../fem/fem.hpp"




namespace GALES
{


    

  template<typename ic_bc_type, int dim>
  class adv_diff{};



  template<typename ic_bc_type>
  class adv_diff<ic_bc_type, 2>
  {
      using element_type = element<2>;
      using vec = boost::numeric::ublas::vector<double>;
      using mat = boost::numeric::ublas::matrix<double>;
  
      public :
  
  
      adv_diff(ic_bc_type& ic_bc, adv_diff_properties& props, model<2>& model)
      : 
      ic_bc_(ic_bc),
      setup_(model.setup()),
      props_(props),
 
      JNxW_(2,0.0),
      JxW_(0.0),
      dt_(0.0),
 
      P_(0.0),
      I_(0.0),
      dI_dx_(0.0),
      dI_dy_(0.0),
 
      tau_(0.0),        
      g_(2, 0.0),
 
      tol_(std::numeric_limits<double>::epsilon())
      { 
        nu_ = props_.nu();    
                 
        ic_bc_.body_force(g_);

        alpha_m_ = 0.5*(3.0-setup_.rho_inf())/(1.0+setup_.rho_inf());
        alpha_f_ = 1.0/(1.0+setup_.rho_inf());
        gamma_ = 0.5 + alpha_m_ - alpha_f_;
      }
 
   


 
     // This is for Euler-backward method
      void execute(const element_type& el, const std::vector<vec>& dofs_Y, mat& m, vec& r)
      {
          nb_el_nodes_ = el.nb_nodes();
          
          // need to define a function in adv_diff_ic_bc to obtain the velocity vector

          dt_= time::get().delta_t(); 
          vec P_orig = dofs_Y[1];
          vec I_orig = dofs_Y[0];
          
          vec v_el(2*nb_el_nodes_,0.0);
          vec v_nd(2, 0.0);
          
          for(int i=0; i<nb_el_nodes_; i++)
          {
            ic_bc_.get_v(el.nd_gid(i), v_nd);
            for(int j=0; j<2; j++)
                v_el[2*i+j] = v_nd[j];
          }  
          
           
          for(int i=0; i<el.quad_i().size(); i++)
          {
              quad_ptr_ = el.quad_i()[i];
              JxW_ = quad_ptr_->JxW();
 
              quad_ptr_->interpolate(P_orig,P_);       
              quad_ptr_->interpolate(I_orig,I_);
              quad_ptr_->x_derivative_interpolate(I_orig,dI_dx_);
              quad_ptr_->y_derivative_interpolate(I_orig,dI_dy_);

              vec v(2,0.0);
              quad_ptr_->interpolate(v_el, v);
     
              //------------------tau -------------------------------------------------------------------------                            
              double vx = v[0];
              double vy = v[1];
              const double tau = tau_stab(vx, vy, v_el);
              //------------------ -------------------------------------------------------------------------


              //------------------dc -------------------------------------------------------------------------                            
              double dc(0.0);
              if(setup_.dc_2006()) dc = dc_stab(vx, vy);   
              //-------------------------------------------------------------------------------------------                            

              
              //------------------body force -------------------------------------------------------------------------                            
              double bf = 0.0;             
              //-------------------------------------------------------------------------------------------                            
                                                     
     
              for(int b=0; b<nb_el_nodes_; b++)
              {
                   r[b] += (quad_ptr_->sh(b)*(I_ - P_)/dt_
                          + quad_ptr_->sh(b)*vx*dI_dx_
                          + quad_ptr_->sh(b)*vy*dI_dy_
                          + quad_ptr_->dsh_dx(b)*nu_*dI_dx_
                          + quad_ptr_->dsh_dy(b)*nu_*dI_dy_
                          - quad_ptr_->sh(b)*bf
                          + (vx*quad_ptr_->dsh_dx(b) + vy*quad_ptr_->dsh_dy(b))*tau*(vx*dI_dx_ + vy*dI_dy_ - bf)
                          + dc*(quad_ptr_->dsh_dx(b)*dI_dx_ + quad_ptr_->dsh_dy(b)*dI_dy_) )*JxW_;
              }
      
              for(int b=0; b<nb_el_nodes_; b++)
               for(int a=0; a<nb_el_nodes_; a++)
               {
                   m(b,a) += (quad_ptr_->sh(b)*quad_ptr_->sh(a)/dt_
                            + quad_ptr_->sh(b)*vx *quad_ptr_->dsh_dx(a)
                            + quad_ptr_->sh(b)*vy *quad_ptr_->dsh_dy(a) 
                            + quad_ptr_->dsh_dx(b)* nu_ *quad_ptr_->dsh_dx(a)
                            + quad_ptr_->dsh_dy(b)* nu_ *quad_ptr_->dsh_dy(a) 
                            + (vx*quad_ptr_->dsh_dx(b) + vy*quad_ptr_->dsh_dy(b))*tau*(vx*quad_ptr_->dsh_dx(a) + vy*quad_ptr_->dsh_dy(a))
                            + dc*(quad_ptr_->dsh_dx(b)*quad_ptr_->dsh_dx(a) + quad_ptr_->dsh_dy(b)*quad_ptr_->dsh_dy(a)) )*JxW_;
               }                              
          }                     
          
          
          if(el.on_boundary())
          {
             for(int i=0; i<el.nb_sides(); i++)
             {
               if(el.is_side_on_boundary(i))
               {
                 auto bd_nodes = el.side_nodes(i);
                 auto side_flag = el.side_flag(i);
           
                 for(const auto& quad_ptr: el.quad_b(i))
                 {
                     quad_ptr_ = quad_ptr;               
                     JNxW_ = quad_ptr_->JNxW();
                     
                     vec h(2, 0.0);
           
                     if(ic_bc_.neumann_J1(bd_nodes, side_flag).first)
                         h[0] = ic_bc_.neumann_J1(bd_nodes, side_flag).second;
                         
                     if(ic_bc_.neumann_J2(bd_nodes, side_flag).first)    
                         h[1] = ic_bc_.neumann_J2(bd_nodes, side_flag).second;                     
     
     
                     for (int b=0; b<nb_el_nodes_; b++)
                       r[b] += - quad_ptr_->sh(b)*(h[0]*JNxW_[0] + h[1]*JNxW_[1]);                                                            
                 }                                         
               }
             }            
          }
                    
          r *= -1.0;
      }
  
 
  
  
  
 



     // this is for generalized-alpha method
      void execute(const element_type& el, const std::vector<vec>& dofs_Y, mat& m, vec& r, const std::vector<vec>& dofs_Y_dot)
      {
      }



  
  
      double tau_stab(double vx,  double vy, const vec& v_el)
      {
         double tau(0.0);
         const double v_nrm = sqrt(vx*vx + vy*vy);
         if(v_nrm < tol_) return tau;
         vec v(2,0.0);
         v[0] = vx;    v[1] = vy;  
                  
         if(setup_.tau_non_diag_comp_2019())
         {         
              vec dv_dx(2, 0.0), dv_dy(2, 0.0);
              quad_ptr_->x_derivative_interpolate(v_el, dv_dx);
              quad_ptr_->y_derivative_interpolate(v_el, dv_dy);
     
              mat del_v(2,2,0.0);
              for(int i=0; i<2; i++)
              {
                  del_v(i,0) = dv_dx[i];       del_v(i,1) = dv_dy[i];     
              }
              const auto alpha = quad_ptr_->alpha(v, del_v, dummy_);
              const auto h = quad_ptr_->compute_h(alpha, dummy_);
              
//              const double use =  std::min(dt_*0.5, h/(2.0*v_nrm));
//              tau = std::min(use, h*h/(12.0*nu_));
              
              const double Pe = v_nrm*h/(2.0*nu_); 
              tau = h/(2*v_nrm)*std::min(1.0, Pe/3.0);                
         }
         
         if(setup_.tau_diag_incomp_2007())
         {
              const auto G = quad_ptr_->G();
              double use(0.0);
              for(int j=0; j<2; j++)
               for(int k=0; k<2; k++)
                 use += G(j,k)*G(j,k);
                
              double C_t(0.0);
              if(!setup_.steady_state()) C_t = 4.0;
               
              tau = sqrt(C_t/(dt_*dt_) + boost::numeric::ublas::inner_prod(v, prod(G, v)) + 9.0*use*nu_*nu_);
              if(tau > tol_) tau = 1.0/tau;
              
         }              
         return tau;
      }
  
  
  
  
  
      
      double dc_stab(double vx,  double vy)
      {
             const double del_Y_nrm = sqrt(pow(dI_dx_,2) + pow(dI_dy_,2));
             if(del_Y_nrm < tol_) return 0.0;
             vec J(2,0.0);
             J[0] = dI_dx_/del_Y_nrm;
             J[1] = dI_dy_/del_Y_nrm;
             double h(0.0);
             for(int i=0; i<nb_el_nodes_; i++)
             {
                 h += abs(J[0]*quad_ptr_->dsh_dx(i) + J[1]*quad_ptr_->dsh_dy(i));
             }
             if(h > tol_) h = 1.0/h;
             else return 0.0;


             const double Res = vx*dI_dx_ + vy*dI_dy_;
             const double Y_ref = 1.0;
             const double Y_ref_inv = 1.0/Y_ref;
             
             const double Y_ref_inv_Res_nrm = abs(Y_ref_inv*Res);
             const double a = abs(Y_ref_inv*dI_dx_);
             const double b = abs(Y_ref_inv*dI_dy_);

             const double s = setup_.dc_sharp();
             double nu_shoc(0.0);
             if(s == 1.0)
             {
               if(a*a+b*b < tol_) return 0.0;
               nu_shoc = Y_ref_inv_Res_nrm*h*1.0/sqrt(a*a+b*b);
             }  
             else if(s == 2.0) 
             {
               nu_shoc = Y_ref_inv_Res_nrm*h*h;
             }  
             else if(s == 1.5) 
             {
               if(a*a+b*b < tol_) nu_shoc = Y_ref_inv_Res_nrm*h*h;
               else nu_shoc = Y_ref_inv_Res_nrm*h*(1.0/sqrt(a*a+b*b) + h)/2.0;
             }
             return nu_shoc*setup_.dc_scale_fact();      
      }
  
  
 
      
      
      
  private:
  
    int nb_el_nodes_;

    ic_bc_type& ic_bc_;
    read_setup& setup_;
    adv_diff_properties& props_;
    
    double nu_;
    double rho_;
    double cv_;
    double kappa_;

    double JxW_; 
    vec JNxW_;
    double dt_;

    double I_dot_;
    
    double P_;
    double I_;
    double dI_dx_;
    double dI_dy_;

    double tau_;

    vec  g_;

    double tol_;
    std::shared_ptr<quad> quad_ptr_ = nullptr;
    point<2> dummy_;
    double alpha_f_, alpha_m_, gamma_;


  };
  
  
  
}/* namespace GALES */
#endif
