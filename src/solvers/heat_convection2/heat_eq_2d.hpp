#ifndef HEAT_EQ_2D_HPP
#define HEAT_EQ_2D_HPP




#include "../../fem/fem.hpp"



namespace GALES{
 
   
   
      
   


  template<typename ic_bc_type, int dim> class heat_eq{};


   
  template<typename ic_bc_type>
  class heat_eq<ic_bc_type, 2>
  {
    using element_type = element<2>;
    using vec = boost::numeric::ublas::vector<double>;
    using mat = boost::numeric::ublas::matrix<double>;


   public :  
 
      heat_eq(ic_bc_type& ic_bc, heat_eq_properties& props, model<2>& model)
      :
      ic_bc_(ic_bc),
      props_(props),
      setup_(model.setup()),

      JxW_(0.0),
      JNxW_(2,0.0),
      dt_(0.0), 

      rho_(0.0),
      cp_(0.0),
      cv_(0.0),
      kappa_(0.0),
      alpha_(0.0),
      mu_ (0.0),
      
      P_(0.0),
      I_(0.0),

      dI_dx_(0.0),
      dI_dy_(0.0),      
           
      tol_(std::numeric_limits<double>::epsilon())
      {
        alpha_m_ = 0.5*(3.0-setup_.rho_inf())/(1.0+setup_.rho_inf());
        alpha_f_ = 1.0/(1.0+setup_.rho_inf());
        gamma_ = 0.5 + alpha_m_ - alpha_f_;

        if(props_.get_material_type() == "custom")
        {
          props_.properties();
          rho_ = props_.rho();
          cp_ = props_.cp();
          cv_ = props_.cv();
          mu_ = props_.mu();
          kappa_ = props_.kappa();
          alpha_ = props_.alpha();
        }                
      } 

  
          


     void u_ref_inv()
     {
         get_properties(0.0, -2103.98, 1.e6, 313.15);
         double u_ref(0.0);
         U(313.15, u_ref);

         if(u_ref < tol_)  u_ref_inv_ = 0.0;
         else u_ref_inv_ = 1.0/u_ref;
     }



     
    
    vec get_v_el(const element_type& el)
    {
      vec v_el(2*nb_el_nodes_,0.0);          
      for(int i=0; i<nb_el_nodes_; i++)
      {
        vec v(2, 0.0);
        ic_bc_.get_v(el.nd_gid(i), v);       
        for(int j=0; j<2; j++)
          v_el[2*i+j] = v[j];
      }
      return v_el;    
    }
     




    vec get_p_el(const element_type& el)
    {
      vec p_el(nb_el_nodes_,0.0);          

      for(int i=0; i<nb_el_nodes_; i++)
        ic_bc_.get_p(el.nd_gid(i), p_el[i]);

      return p_el;    
    }
  




    void get_properties(double x, double y, double p,  double T)
    {
       if(props_.get_material_type() == "y-dependent-class")
       {
          props_.properties(x,y,p,T);
          rho_ = props_.rho();
          cp_ = props_.cp();
          cv_ = props_.cv();
          mu_ = props_.mu();
          kappa_ = props_.kappa();
          alpha_ = props_.alpha();        
       }        
    }





     // This is for Euler-backward method
    void execute(const element_type& el, const std::vector<vec>& dofs_T, mat& m, vec& r)
    {
      nb_el_nodes_ = el.nb_nodes();
      r_.resize(nb_el_nodes_, false);
      m_.resize(nb_el_nodes_, nb_el_nodes_, false);
      
            
      dt_= time::get().delta_t();

      vec P_dofs = dofs_T[1];
      vec I_dofs = dofs_T[0];
      
      r_.clear();
      m_.clear();      

      
      auto v_el = get_v_el(el);
      auto p_el = get_p_el(el);
      double p(0.0);
      vec v(2, 0.0);
           
      for(int i=0; i<el.quad_i().size(); i++)
      {
          quad_ptr_ = el.quad_i()[i];
          JxW_ = quad_ptr_->JxW();
          quad_ptr_->interpolate(p_el, p);
          quad_ptr_->interpolate(v_el, v);
            
          quad_ptr_->interpolate(P_dofs, P_);
          get_properties(el.get_x(i), el.get_y(i), p, P_);         
          U(P_, U_P_);                    
                              
          quad_ptr_->interpolate(I_dofs, I_);
          quad_ptr_->x_derivative_interpolate(I_dofs, dI_dx_);
          quad_ptr_->y_derivative_interpolate(I_dofs, dI_dy_);                    
          get_properties(el.get_x(i), el.get_y(i), p, I_);         
          flux_matrices_and_vectors(p, I_, v[0], v[1], v_el); 
          tau_stab(v[0], v[1], v_el);                             
          RM_i();                                 
      }                     

     
      if(el.on_boundary())
      {
         for(int i=0; i<el.nb_sides(); i++)
         {
           if(el.is_side_on_boundary(i))
           {
              auto bd_nodes = el.side_nodes(i);
              auto side_flag = el.side_flag(i);
      
               for(int j=0; j<el.quad_b(i).size(); j++)
               {
                   quad_ptr_ = el.quad_b(i,j);               
                   JNxW_ = quad_ptr_->JNxW();   
                   quad_ptr_->interpolate(p_el, p);
                   quad_ptr_->interpolate(v_el, v);
                   quad_ptr_->interpolate(I_dofs, I_);
                   quad_ptr_->x_derivative_interpolate(I_dofs, dI_dx_);
                   quad_ptr_->y_derivative_interpolate(I_dofs, dI_dy_);
                   get_properties(el.get_side_x(i,j), el.get_side_x(i,j), p, I_); 
                   flux_matrices_and_vectors(p, I_, v[0], v[1], v_el, bd_nodes, side_flag);
                   RM_b();               
               }
           }
         }            
      }

      m = m_;
      r = -r_;      
    }











      void RM_i()
      {      
         const double dU_dx = DUDY_*dI_dx_;
         const double dU_dy = DUDY_*dI_dy_;
         const double dU_dt = DUDY_*(I_-P_)/dt_;      

          for(int b=0; b<nb_el_nodes_; b++)
          {
             r_[b] += quad_ptr_->sh(b)*dU_dt*JxW_;
             r_[b] += quad_ptr_->dsh_dx(b)*(F1_dif_ - F1_adv_)*JxW_;
             r_[b] += quad_ptr_->dsh_dy(b)*(F2_dif_ - F2_adv_)*JxW_;
             r_[b] += quad_ptr_->sh(b)*F_sp_*JxW_;
          }


          for(int b=0; b<nb_el_nodes_; b++)
           for(int a=0; a<nb_el_nodes_; a++)
           {
               m_(b,a) += quad_ptr_->sh(b)*DUDY_*quad_ptr_->sh(a)/dt_*JxW_;               
               m_(b,a) -= quad_ptr_->dsh_dx(b)*A1_*quad_ptr_->sh(a)*JxW_;  
               m_(b,a) -= quad_ptr_->dsh_dy(b)*A2_*quad_ptr_->sh(a)*JxW_;                 
               m_(b,a) += quad_ptr_->dsh_dx(b)*K11_*quad_ptr_->dsh_dx(a)*JxW_;
               m_(b,a) += quad_ptr_->dsh_dy(b)*K22_*quad_ptr_->dsh_dy(a)*JxW_;
           }                              
         



         //--------------------------------------------R_LeastSquares--------------------------------------------------------------------------        
         double Res = A1_*dI_dx_ + A2_*dI_dy_;                              
                          
         for(int b=0; b<nb_el_nodes_; b++)
            r_[b] += (A1_*quad_ptr_->dsh_dx(b) + A2_*quad_ptr_->dsh_dy(b))*tau_*Res*JxW_;


         //--------------------------------------------M_LeastSquares--------------------------------------------------------------------------
         for(int a=0; a<nb_el_nodes_; a++)
         {
             auto Res = A1_*quad_ptr_->dsh_dx(a) + A2_*quad_ptr_->dsh_dy(a);                                        
                                            
             for(int b=0; b<nb_el_nodes_; b++)
                 m_(b,a) += (A1_*quad_ptr_->dsh_dx(b) + A2_*quad_ptr_->dsh_dy(b))*tau_*Res*JxW_;
         }



         //-----------------dc------------------------
         dc(dU_dx, dU_dy);

         if(setup_.dc_2006())
         {
             for(int b=0;b<nb_el_nodes_;b++)
             {
                 r_[b] += quad_ptr_->dsh_dx(b)*shock_matrix_*dI_dx_*JxW_;
                 r_[b] += quad_ptr_->dsh_dy(b)*shock_matrix_*dI_dy_*JxW_;
             }

             for(int b=0;b<nb_el_nodes_;b++)
             for(int a=0;a<nb_el_nodes_;a++)
             {
                 m_(b,a) += shock_matrix_*quad_ptr_->dsh_dx(b)*quad_ptr_->dsh_dx(a)*JxW_;
                 m_(b,a) += shock_matrix_*quad_ptr_->dsh_dy(b)*quad_ptr_->dsh_dy(a)*JxW_;
             }
         }
         
      }









      void RM_b()
      {      
         for(int b=0;b<nb_el_nodes_;b++)
         {
             r_[b] += quad_ptr_->sh(b)*(F1_adv_ - F1_dif_)*JNxW_[0];
             r_[b] += quad_ptr_->sh(b)*(F2_adv_ - F2_dif_)*JNxW_[1];
         }

         for(int b=0; b<nb_el_nodes_; b++)
         for(int a=0; a<nb_el_nodes_; a++)
         {
            m_(b,a) += quad_ptr_->sh(b)*A1_*quad_ptr_->sh(a)*JNxW_[0]; 
            m_(b,a) += quad_ptr_->sh(b)*A2_*quad_ptr_->sh(a)*JNxW_[1]; 
            m_(b,a) -= quad_ptr_->sh(b)*K11_*quad_ptr_->dsh_dx(a)*JNxW_[0]; 
            m_(b,a) -= quad_ptr_->sh(b)*K22_*quad_ptr_->dsh_dy(a)*JNxW_[1]; 
         }
      }







     void U(double T, double& u)
     {
         double rho =  props_.rho();
         double cv = props_.cv();
         double e = cv*T;
         u = rho*e;
     }







     void flux_matrices_and_vectors(double p, double T, double vx, double vy, const vec& v_el)
     {
         const double e = cv_*T;
         const double drho_dT = -rho_*alpha_;
         const double de_dT =  cv_;
         
         U(T,U_);
         DUDY_ = drho_dT*e + rho_*de_dT;

         
         F1_adv_ = vx*rho_*e;
         A1_  = vx*(drho_dT*e + rho_*de_dT);


         F2_adv_ = vy*rho_*e;
         A2_  = vy*(drho_dT*e + rho_*de_dT);                    

         vec dv_dx(2,0.0);
         vec dv_dy(2,0.0);
         quad_ptr_->x_derivative_interpolate(v_el, dv_dx);
         quad_ptr_->y_derivative_interpolate(v_el, dv_dy);
         
         const double v1_1(dv_dx[0]), v1_2(dv_dy[0]);
         const double v2_1(dv_dx[1]), v2_2(dv_dy[1]);      

         const double lambda = -2.0*mu_/3.0;
         double tau11 = lambda*(v1_1 + v2_2) + 2.0*mu_*v1_1;
         double tau22 = lambda*(v1_1 + v2_2) + 2.0*mu_*v2_2;
         double tau12 = mu_*(v1_2 + v2_1);

         F_sp_ = ((p-tau11)*v1_1 - tau12*v2_1 - tau12*v1_2 + (p-tau22)*v2_2);
         

         K11_ =  kappa_;
         K22_ =  kappa_;

         F1_dif_ =  K11_*dI_dx_;
         F2_dif_ =  K22_*dI_dy_;
     }






     void flux_matrices_and_vectors(double p, double T, double vx, double vy, const vec& v_el, const std::vector<int>& bd_nodes, int side_flag)
     {
         flux_matrices_and_vectors(p, T, vx, vy, v_el);

         if(ic_bc_.neumann_q1(bd_nodes, side_flag).first){  F1_dif_ = -ic_bc_.neumann_q1(bd_nodes, side_flag).second;   K11_ = 0.0;}
         if(ic_bc_.neumann_q2(bd_nodes, side_flag).first){  F2_dif_ = -ic_bc_.neumann_q2(bd_nodes, side_flag).second;   K22_ = 0.0;}
     }







      void tau_stab(double vx,  double vy, const vec& v_el)
      {
         tau_ = 0.0;
         const double v_nrm = sqrt(vx*vx + vy*vy);
         if(v_nrm < tol_) return;
         vec v(2,0.0);
         v[0] = vx;    v[1] = vy;  
                        
                  
         if(setup_.tau_non_diag_comp_2019())
         {         
              vec dv_dx(2, 0.0), dv_dy(2, 0.0);
              quad_ptr_->x_derivative_interpolate(v_el, dv_dx);
              quad_ptr_->y_derivative_interpolate(v_el, dv_dy);
     
              mat del_v(2,2,0.0);
              for(int i=0; i<2; i++)
              {
                  del_v(i,0) = dv_dx[i];       del_v(i,1) = dv_dy[i];     
              }
              const auto alpha = quad_ptr_->alpha(v, del_v, dummy_);
              const auto h = quad_ptr_->compute_h(alpha, dummy_);
              
              const double use =  std::min(dt_*0.5, h/(2.0*v_nrm));
              tau_ = std::min(use/(rho_*cv_), h*h/(kappa_*12));             
              
         }
         
         else if(setup_.tau_non_diag_comp_2001())
         {
             const double v_nrm = sqrt(vx*vx + vy*vy);
             if(v_nrm < tol_) return;
             vec J(2,0.0);
             J[0] = vx/v_nrm;
             J[1] = vy/v_nrm;
             double h(0.0);
             for(int i=0; i<nb_el_nodes_; i++)
             {
                 h += abs(J[0]*quad_ptr_->dsh_dx(i) + J[1]*quad_ptr_->dsh_dy(i) );
             }
             if(h > tol_) h = 2.0/h;
             else return;
             
             tau_ = (dt_*h*h)/(4*kappa_/(rho_*cv_)*dt_ + 2*v_nrm*h*dt_ + h*h);            
         }
         
         else if(setup_.tau_diag_incomp_2007())
         {
              const auto G = quad_ptr_->G();
              double use(0.0);
              for(int j=0; j<2; j++)
               for(int k=0; k<2; k++)
                 use += G(j,k)*G(j,k);
                
              double C_t(0.0);
              if(!setup_.steady_state()) C_t = 4.0;
               
              tau_ = sqrt(C_t/(dt_*dt_) + boost::numeric::ublas::inner_prod(v, prod(G, v)) + 9.0*use*kappa_*kappa_/(rho_*rho_*cp_*cp_));
              if(tau_ > tol_) tau_ = 1.0/(DUDY_*tau_);
              else tau_ = 0.0;              
         }              
      }
  







      void dc(double dU_dx, double dU_dy)
      {           
           shock_matrix_ = 0.0;
           
           if(setup_.dc_2006())
           {               
             const double del_rho_nrm = sqrt(dU_dx*dU_dx + dU_dy*dU_dy);
             if(del_rho_nrm < tol_) return;
             vec J(2,0.0);
             J[0] = dU_dx/del_rho_nrm;
             J[1] = dU_dy/del_rho_nrm;
             double h(0.0);
             for(int i=0; i<nb_el_nodes_; i++)
             {
                 h += abs(J[0]*quad_ptr_->dsh_dx(i) + J[1]*quad_ptr_->dsh_dy(i) );
             }
             if(h > tol_) h = 1.0/h;
             else return;

             double Res = A1_*dI_dx_ + A2_*dI_dy_;
             const double u_ref_inv_Res_nrm = std::abs(u_ref_inv_*Res);
             const double a = std::abs(u_ref_inv_*dU_dx);
             const double b = std::abs(u_ref_inv_*dU_dy);

             const double s = setup_.dc_sharp();
             double nu_shoc(0.0);
             if(s == 1.0)
             {
               if(a*a+b*b < tol_) return;
               nu_shoc = u_ref_inv_Res_nrm*h*1.0/sqrt(a*a+b*b);
             }  

             shock_matrix_ = setup_.dc_scale_fact()*nu_shoc*DUDY_;
             
             
             
//             const double del_T_nrm = sqrt(dI_dx_*dI_dx_ + dI_dy_*dI_dy_);
//             if(del_T_nrm < tol_) return;
//             vec J(2,0.0);
//             J[0] = dI_dx_/del_T_nrm;
//             J[1] = dI_dy_/del_T_nrm;
//             double h(0.0);
//             for(int i=0; i<nb_el_nodes_; i++)
//             {
//                 h += abs(J[0]*quad_ptr_->dsh_dx(i) + J[1]*quad_ptr_->dsh_dy(i));
//             }
//             if(h > tol_) h = 1.0/h;
//             else return;
//                       
//             const double Res = A1_*dI_dx_ + A2_*dI_dy_;
//             const double T_ref_inv = 1.0/700.0;
//             
//             const double T_ref_inv_Res_nrm = abs(T_ref_inv*Res);
//             const double a = abs(T_ref_inv*dI_dx_);
//             const double b = abs(T_ref_inv*dI_dy_);

//             const double s = setup_.dc_sharp();
//             if(s == 1.0)
//             {
//               if(a*a+b*b < tol_) return;
//               else shock_matrix_ = setup_.dc_scale_fact()*T_ref_inv_Res_nrm*h*1.0/sqrt(a*a+b*b);
//             }  
             
             
           }
      }
  





    // gen-alpha method
    void execute(const element_type& el, const std::vector<vec>& dofs_T, mat& m, vec& r, const std::vector<vec>& dofs_T_dot)
    {
    }





   private:

    int nb_el_nodes_;
    
    ic_bc_type& ic_bc_;
    heat_eq_properties& props_;
    read_setup& setup_;

    double JxW_; 
    vec JNxW_;
    double dt_;


    double rho_, cp_, cv_, kappa_, alpha_, mu_;
    double P_, I_;
    double dI_dx_, dI_dy_;
    
    double U_P_;
    double U_;
    double F1_adv_;
    double F2_adv_;
    double F_sp_;
    double F1_dif_;
    double F2_dif_;
    
    double DUDY_;
    double A1_,A2_;
    double K11_, K22_;
    
    double tau_;
    double shock_matrix_;
    double u_ref_inv_;
    double dc_hm_;

    vec r_;
    mat m_;




    double tol_;
    std::shared_ptr<quad> quad_ptr_ = nullptr;
    point<2> dummy_;    
    double alpha_f_, alpha_m_, gamma_;
  }; 






 
} 

#endif

