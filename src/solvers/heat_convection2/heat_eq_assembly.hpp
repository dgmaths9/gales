#ifndef HEAT_EQ_ASSEMBLY_HPP
#define HEAT_EQ_ASSEMBLY_HPP




#include "../../fem/fem.hpp"



namespace GALES{



  template<typename integral_type, typename dofs_ic_bc_type, int dim>
  class heat_eq_assembly
  {
      using model_type = model<dim>;
      using vec = boost::numeric::ublas::vector<double>;
      using mat = boost::numeric::ublas::matrix<double>;
    
    public:
    


     // This is for Euler-backward method
    double execute(integral_type& integral, dofs_ic_bc_type& dofs_ic_bc, model_type& model, linear_system& lp)
    {
      double start(MPI_Wtime());      
      
      std::vector<vec> dofs_T(model.state().num_slot());

      lp.clear();
      for(const auto& el : model.mesh().elements())
      {
	model.extract_element_dofs(*el, dofs_T);
	
        const int nb_dofs = dofs_T[0].size();
        vec r(nb_dofs, 0.0);
        mat m(nb_dofs, nb_dofs, 0.0);

	integral.execute(*el, dofs_T, m, r);
	lp.assemble(el->dofs_gids(), el->dofs_constraints(dofs_ic_bc), m, r);
      }

      lp.assembled();       
      return MPI_Wtime()-start;                  
    }




     // This is for Generalized-alpha method 
    double execute(integral_type& integral, dofs_ic_bc_type& dofs_ic_bc, model_type& model, linear_system& lp, model_type& dot_model)
    {
      double start(MPI_Wtime());      
      
      std::vector<vec> dofs_T(model.state().num_slot());
      std::vector<vec> dofs_T_dot(dot_model.state().num_slot());

      lp.clear();
      for(const auto& el : model.mesh().elements())
      {
	model.extract_element_dofs(*el, dofs_T);
	dot_model.extract_element_dofs(*el, dofs_T_dot);
	
        const int nb_dofs = dofs_T[0].size();
        vec r(nb_dofs, 0.0);
        mat m(nb_dofs, nb_dofs, 0.0);

	integral.execute(*el, dofs_T, m, r, dofs_T_dot);
	lp.assemble(el->dofs_gids(), el->dofs_constraints(dofs_ic_bc), m, r);
      }

      lp.assembled();       
      return MPI_Wtime()-start;                  
    }

  };



}

#endif
