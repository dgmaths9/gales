#ifndef HEAT_EQ_2D_HPP
#define HEAT_EQ_2D_HPP




#include "../../fem/fem.hpp"



namespace GALES{
 
   
   
      
   


  template<typename ic_bc_type, int dim> class heat_eq{};


   
  template<typename ic_bc_type>
  class heat_eq<ic_bc_type, 2>
  {
    using element_type = element<2>;
    using vec = boost::numeric::ublas::vector<double>;
    using mat = boost::numeric::ublas::matrix<double>;


   public :  
 
      heat_eq(ic_bc_type& ic_bc, heat_eq_properties& props, model<2>& model)
      :
      ic_bc_(ic_bc),
      props_(props),
      setup_(model.setup()),

      JxW_(0.0),
      JNxW_(2,0.0),
      dt_(0.0), 

      rho_(0.0),
      cp_(0.0),
      cv_(0.0),
      kappa_(0.0),
      alpha_(0.0),
      rhoe_p_(0.0),
      
      P_(0.0),
      I_(0.0),

      dI_dx_(0.0),
      dI_dy_(0.0),      
           
      tol_(std::numeric_limits<double>::epsilon())
      {
        alpha_m_ = 0.5*(3.0-setup_.rho_inf())/(1.0+setup_.rho_inf());
        alpha_f_ = 1.0/(1.0+setup_.rho_inf());
        gamma_ = 0.5 + alpha_m_ - alpha_f_;
        T_ref_ = ic_bc_.T_ref(); 

        if(props_.get_material_type() == "custom")
        {
          props_.properties();
          rho_ = props_.rho();
          cp_ = props_.cp();
          cv_ = props_.cv();
          mu_ = props_.mu();
          kappa_ = props_.kappa();
          alpha_ = props_.alpha();
        }                
      } 

  
          

     
    
    vec get_v_el(const element_type& el)
    {
      vec v_el(2*nb_el_nodes_,0.0);          
      for(int i=0; i<nb_el_nodes_; i++)
      {
        vec v(2, 0.0);
        ic_bc_.get_v(el.nd_gid(i), v);       
        for(int j=0; j<2; j++)
          v_el[2*i+j] = v[j];
      }
      return v_el;    
    }
     




    vec get_p_el(const element_type& el)
    {
      vec p_el(nb_el_nodes_,0.0);          

      for(int i=0; i<nb_el_nodes_; i++)
        ic_bc_.get_p(el.nd_gid(i), p_el[i]);

      return p_el;    
    }
  




    void get_properties(double x, double y, double p,  double T)
    {
       if(props_.get_material_type() == "y-dependent-class")
       {
          props_.properties(x,y,p,T);
          rho_ = props_.rho();
          cp_ = props_.cp();
          cv_ = props_.cv();
          mu_ = props_.mu();
          kappa_ = props_.kappa();
          alpha_ = props_.alpha();        
       }        
    }



     // This is for Euler-backward method
    void execute(const element_type& el, const std::vector<vec>& dofs_T, mat& m, vec& r)
    {
      nb_el_nodes_ = el.nb_nodes();
      r_.resize(nb_el_nodes_, false);
      m_.resize(nb_el_nodes_, nb_el_nodes_, false);

      dt_= time::get().delta_t();

      vec P_dofs = dofs_T[1];
      vec I_dofs = dofs_T[0];
      
      r_.clear();
      m_.clear();      

      
      auto v_el = get_v_el(el);
      auto p_el = get_p_el(el);
      double p(0.0);
           
      for(int i=0; i<el.quad_i().size(); i++)
      {
          quad_ptr_ = el.quad_i()[i];
          JxW_ = quad_ptr_->JxW();
          quad_ptr_->interpolate(p_el, p);
            
          quad_ptr_->interpolate(P_dofs, P_);
          get_properties(el.get_x(i), el.get_y(i), p, P_);         
          rhoe_p_ = rho_*cv_*P_;                    
                              
          quad_ptr_->interpolate(I_dofs, I_);
          quad_ptr_->x_derivative_interpolate(I_dofs, dI_dx_);
          quad_ptr_->y_derivative_interpolate(I_dofs, dI_dy_);                    
          get_properties(el.get_x(i), el.get_y(i), p, I_);         
          RM_i(p, v_el);                                 
      }                     

     
      if(el.on_boundary())
      {
         for(int i=0; i<el.nb_sides(); i++)
         {
           if(el.is_side_on_boundary(i))
           {
              auto bd_nodes = el.side_nodes(i);
              auto side_flag = el.side_flag(i);
      
               for(const auto& quad_ptr: el.quad_b(i))
               {
                   quad_ptr_ = quad_ptr;               
                   JNxW_ = quad_ptr_->JNxW();   
                   quad_ptr_->interpolate(p_el, p);
                   quad_ptr_->interpolate(I_dofs, I_);
                   quad_ptr_->x_derivative_interpolate(I_dofs, dI_dx_);
                   quad_ptr_->y_derivative_interpolate(I_dofs, dI_dy_);
                   get_properties(el.get_x(i), el.get_y(i), p, I_); 
                   RM_b(v_el, bd_nodes, side_flag);               
               }
           }
         }            
      }

      m = m_;
      r = -r_;      
    }











      void RM_i(double p, const vec& v_el)
      {      
         vec v(2,0.0);
         quad_ptr_->interpolate(v_el, v);
         double vx = v[0];
         double vy = v[1];


         vec dv_dx(2,0.0);
         vec dv_dy(2,0.0);
         
         quad_ptr_->x_derivative_interpolate(v_el, dv_dx);
         quad_ptr_->y_derivative_interpolate(v_el, dv_dy);
         
         const double v1_1(dv_dx[0]), v1_2(dv_dy[0]);
         const double v2_1(dv_dx[1]), v2_2(dv_dy[1]);      

         const double lambda = -2.0*mu_/3.0;
         double tau11 = lambda*(v1_1 + v2_2) + 2.0*mu_*v1_1;
         double tau22 = lambda*(v1_1 + v2_2) + 2.0*mu_*v2_2;
         double tau12 = mu_*(v1_2 + v2_1);
                   
          //------------------tau -------------------------------------------------------------------------                            
          const double tau = tau_stab(vx, vy, v_el);
          //------------------ -------------------------------------------------------------------------

          //------------------dc -------------------------------------------------------------------------                            
          double dc(0.0);
          if(setup_.dc_2006()) dc = dc_stab(vx, vy);   
          //-------------------------------------------------------------------------------------------                            

          double e = cv_*I_;
          double rhoe = rho_*e;
          double drho_dT = -rho_*alpha_;
          double de_dT = cv_;
          double drhoe_dT = rho_*de_dT + e*drho_dT;

          for(int b=0; b<nb_el_nodes_; b++)
          {
               r_[b] += quad_ptr_->sh(b)*(rhoe - rhoe_p_)/dt_*JxW_;
               r_[b] -= quad_ptr_->dsh_dx(b)*rhoe*vx*JxW_;
               r_[b] -= quad_ptr_->dsh_dy(b)*rhoe*vy*JxW_;
               r_[b] += quad_ptr_->dsh_dx(b)*kappa_*dI_dx_*JxW_;
               r_[b] += quad_ptr_->dsh_dy(b)*kappa_*dI_dy_*JxW_;                              
               r_[b] += quad_ptr_->sh(b)*((p-tau11)*v1_1 - tau12*v2_1 - tau12*v1_2 + (p-tau22)*v2_2)*JxW_;               
               r_[b] += (vx*quad_ptr_->dsh_dx(b) + vy*quad_ptr_->dsh_dy(b))*tau*(vx*dI_dx_ + vy*dI_dy_)*JxW_;
               r_[b] += quad_ptr_->dsh_dx(b)*dc*dI_dx_ + quad_ptr_->dsh_dy(b)*dc*dI_dy_*JxW_;
          }


          for(int b=0; b<nb_el_nodes_; b++)
           for(int a=0; a<nb_el_nodes_; a++)
           {
               m_(b,a) += quad_ptr_->sh(b)*drhoe_dT*quad_ptr_->sh(a)/dt_*JxW_;               
               m_(b,a) -= quad_ptr_->dsh_dx(b)*drhoe_dT*vx*quad_ptr_->sh(a)*JxW_;  
               m_(b,a) -= quad_ptr_->dsh_dy(b)*drhoe_dT*vy*quad_ptr_->sh(a)*JxW_;                 
               m_(b,a) += quad_ptr_->dsh_dx(b)*kappa_*quad_ptr_->dsh_dx(a)*JxW_;
               m_(b,a) += quad_ptr_->dsh_dy(b)*kappa_*quad_ptr_->dsh_dy(a)*JxW_;

               m_(b,a) += (vx*quad_ptr_->dsh_dx(b) + vy*quad_ptr_->dsh_dy(b))
                          *tau*(vx*quad_ptr_->dsh_dx(a) + vy*quad_ptr_->dsh_dy(a))*JxW_;

               m_(b,a) += quad_ptr_->dsh_dx(b)*dc*quad_ptr_->dsh_dx(a) + quad_ptr_->dsh_dy(b)*dc*quad_ptr_->dsh_dy(a)*JxW_;
           }                              
         
      }









      void RM_b(const vec& v_el, const std::vector<int>& bd_nodes, int side_flag)
      {      
         vec v(2,0.0);
         quad_ptr_->interpolate(v_el, v);
         double vx = v[0];
         double vy = v[1];

         double q1 = -kappa_*dI_dx_;
         double q2 = -kappa_*dI_dy_;
         
         double k1 = -kappa_;
         double k2 = -kappa_;
 
         if(ic_bc_.neumann_q1(bd_nodes, side_flag).first){ q1 = ic_bc_.neumann_q1(bd_nodes, side_flag).second;  k1=0.0;}
         if(ic_bc_.neumann_q2(bd_nodes, side_flag).first){ q2 = ic_bc_.neumann_q2(bd_nodes, side_flag).second;  k2=0.0;}
          

          double e = cv_*I_;
          double rhoe = rho_*e;
          double drho_dT = -rho_*alpha_;
          double de_dT = cv_;
          double drhoe_dT = rho_*de_dT + e*drho_dT;

          for(int b=0; b<nb_el_nodes_; b++)
          {
               r_[b] += quad_ptr_->sh(b)*rhoe*(vx*JNxW_[0] + vy*JNxW_[1]);
               r_[b] += quad_ptr_->sh(b)*(q1*JNxW_[0] + q2*JNxW_[1]);
          }


          for(int b=0; b<nb_el_nodes_; b++)
           for(int a=0; a<nb_el_nodes_; a++)
           {
               m_(b,a) += quad_ptr_->sh(b)*drhoe_dT*quad_ptr_->sh(a)*(vx*JNxW_[0] + vy*JNxW_[1]);               
               m_(b,a) += quad_ptr_->sh(b)*(k1*quad_ptr_->dsh_dx(a)*JNxW_[0] + k2*quad_ptr_->dsh_dy(a)*JNxW_[1]);               
           }                              
      }








      double tau_stab(double vx,  double vy, const vec& v_el)
      {
         double tau(0.0);
         const double v_nrm = sqrt(vx*vx + vy*vy);
         if(v_nrm < tol_) return tau;
         vec v(2,0.0);
         v[0] = vx;    v[1] = vy;  
                        
                  
         if(setup_.tau_non_diag_comp_2019())
         {         
              vec dv_dx(2, 0.0), dv_dy(2, 0.0);
              quad_ptr_->x_derivative_interpolate(v_el, dv_dx);
              quad_ptr_->y_derivative_interpolate(v_el, dv_dy);
     
              mat del_v(2,2,0.0);
              for(int i=0; i<2; i++)
              {
                  del_v(i,0) = dv_dx[i];       del_v(i,1) = dv_dy[i];     
              }
              const auto alpha = quad_ptr_->alpha(v, del_v, dummy_);
              const auto h = quad_ptr_->compute_h(alpha, dummy_);
              
              const double use =  std::min(dt_*0.5, h/(2.0*v_nrm));
              tau = std::min(use, h*h*rho_*cp_/(12.0*kappa_));
              
//              const double Pe = v_nrm*h*rho_*cp_/(2.0*kappa_); 
//              tau = h/(2*v_nrm)*std::min(1.0, Pe/3.0);                
         }
         
         if(setup_.tau_diag_incomp_2007())
         {
              const auto G = quad_ptr_->G();
              double use(0.0);
              for(int j=0; j<2; j++)
               for(int k=0; k<2; k++)
                 use += G(j,k)*G(j,k);
                
              double C_t(0.0);
              if(!setup_.steady_state()) C_t = 4.0;
               
              tau = sqrt(C_t/(dt_*dt_) + boost::numeric::ublas::inner_prod(v, prod(G, v)) + 9.0*use*kappa_*kappa_/(rho_*rho_*cp_*cp_));
              if(tau > tol_) tau = 1.0/tau;              
         }              
         return tau;
      }
  







      double dc_stab(double vx,  double vy)
      {           
           if(setup_.dc_2006())
           {  
             const double del_T_nrm = sqrt(dI_dx_*dI_dx_ + dI_dy_*dI_dy_);
             if(del_T_nrm < tol_) return 0.0;
             vec J(2,0.0);
             J[0] = dI_dx_/del_T_nrm;
             J[1] = dI_dy_/del_T_nrm;
             double h(0.0);
             for(int i=0; i<nb_el_nodes_; i++)
             {
                 h += abs(J[0]*quad_ptr_->dsh_dx(i) + J[1]*quad_ptr_->dsh_dy(i));
             }
             if(h > tol_) h = 1.0/h;
             else return 0.0;
                       
             const double Res = vx*dI_dx_ + vy*dI_dy_;
             const double T_ref_inv = 1.0/T_ref_;
             
             const double T_ref_inv_Res_nrm = abs(T_ref_inv*Res);
             const double a = abs(T_ref_inv*dI_dx_);
             const double b = abs(T_ref_inv*dI_dy_);

             const double s = setup_.dc_sharp();
             if(s == 1.0)
             {
               if(a*a+b*b < tol_) return 0.0;
               else return setup_.dc_scale_fact()*T_ref_inv_Res_nrm*h*1.0/sqrt(a*a+b*b);
             }  
             else if(s == 2.0) 
             {               
               return setup_.dc_scale_fact()*T_ref_inv_Res_nrm*h*h;
             }  
           }
           return 0.0;              
      }
  





    // gen-alpha method
    void execute(const element_type& el, const std::vector<vec>& dofs_T, mat& m, vec& r, const std::vector<vec>& dofs_T_dot)
    {
    }





   private:

    int nb_el_nodes_;
    
    ic_bc_type& ic_bc_;
    heat_eq_properties& props_;
    read_setup& setup_;

    double JxW_; 
    vec JNxW_;
    double dt_;


    double rho_, cp_, cv_, kappa_, alpha_, mu_;
    double P_, I_;
    double dI_dx_, dI_dy_;
    double T_ref_;
    
    double rhoe_p_;
    vec r_;
    mat m_;


    double tol_;
    std::shared_ptr<quad> quad_ptr_ = nullptr;
    point<2> dummy_;    
    double alpha_f_, alpha_m_, gamma_;
  }; 






 
} 

#endif

