#ifndef SOLID_2D_HPP
#define SOLID_2D_HPP




#include "../../fem/fem.hpp"



namespace GALES{
 
   
      
   
  template<typename ic_bc_type, int dim> class solid{};


   
  template<typename ic_bc_type>
  class solid<ic_bc_type, 2>
  {
    using element_type = element<2>;
    using vec = boost::numeric::ublas::vector<double>;
    using mat = boost::numeric::ublas::matrix<double>;


   public :  

    solid
    (
     ic_bc_type& ic_bc,
     solid_properties& props,
     model<2>& model
    ):

      ic_bc_(ic_bc),
      props_(props),
      setup_(model.setup()),     

      JNxW_(2,0.0),
      JxW_(0.0),
      dt_(0.0), 

      material_type_(props.material_type()),
      condition_type_(props.condition_type()),      

      rho_(props.rho()),
      nu_(props.nu()),
      E_(props.E()),

      a_damping_(props.a_damping()), 
      b_damping_(props.b_damping()),

      D_(3,3,0.0),
      P_(2,2,0.0),
            
      gravity_(2,0.0)
     {      
       alpha_m_ = 0.5*(3.0-setup_.rho_inf())/(1.0+setup_.rho_inf());
       alpha_f_ = 1.0/(1.0+setup_.rho_inf());
       gamma_ = 0.5 + alpha_m_ - alpha_f_;
       beta_ = 0.25*(1.0 + alpha_m_ - alpha_f_)*(1.0 + alpha_m_ - alpha_f_);      
              
       ic_bc_.body_force(gravity_);         
     }

  




     void set_el_data_size()
     {
        N_.resize(2,2*nb_el_nodes_,false);
        B_.resize(3,nb_el_nodes_*2,false);

        I_dofs_solid_u_.resize(2*nb_el_nodes_,false);
        I_dofs_solid_v_.resize(2*nb_el_nodes_,false);
        I_dofs_solid_a_.resize(2*nb_el_nodes_,false);

        r_.resize(2*nb_el_nodes_,false);
        m_.resize(2*nb_el_nodes_,2*nb_el_nodes_,false);
     }
    
  
  



    void execute(const element_type& el, const std::vector<vec>& dofs_u, const std::vector<vec>& dofs_v, const std::vector<vec>& dofs_a, mat& m, vec& r)
    {
      nb_el_nodes_ = el.nb_nodes();      
      set_el_data_size();

      dt_= time::get().delta_t();
      
      I_dofs_solid_u_ = alpha_f_*dofs_u[0] + (1.0-alpha_f_)*dofs_u[1];
      I_dofs_solid_v_ = alpha_f_*dofs_v[0] + (1.0-alpha_f_)*dofs_v[1];
      I_dofs_solid_a_ = alpha_m_*dofs_a[0] + (1.0-alpha_m_)*dofs_a[1];

      r_.clear();
      m_.clear();      

      for(const auto& quad_ptr : el.quad_i())
      {
        quad_ptr_ = quad_ptr;
        JxW_ = quad_ptr_->JxW();

        N();
    
        //------------------------------Mat1---------------------------------------------------------------------------------------------
        const mat Mat1 = prod(boost::numeric::ublas::trans(N_),N_)*rho_*JxW_;
    
    
        //------------------------------Mat2---------------------------------------------------------------------------------------------
        mat Mat2(2*nb_el_nodes_, 2*nb_el_nodes_, 0.0);
    
        if(material_type_ == "Hookes")
        {
          B();
          D();
          mat use1 = prod(D_,B_);
          Mat2 = prod(boost::numeric::ublas::trans(B_),use1)*JxW_;
        }        
        else if(material_type_ == "St_V_Kirchhoff" || material_type_ == "Neo_Hookean")
        {
          double D_arr[2][2][2][2] = {}; 
          finite_def(E_, D_arr);
    
          for(int a=0; a<nb_el_nodes_; a++)
           for(int i=0; i<2; i++)
             for(int b=0; b<nb_el_nodes_; b++)
              for(int k=0; k<2; k++)    
                  Mat2(2*a+i, 2*b+k) += (quad_ptr_->dsh_dx(a)*D_arr[i][0][k][0]*quad_ptr_->dsh_dx(b) + 
                                         quad_ptr_->dsh_dx(a)*D_arr[i][0][k][1]*quad_ptr_->dsh_dy(b) +
                                         quad_ptr_->dsh_dy(a)*D_arr[i][1][k][0]*quad_ptr_->dsh_dx(b) + 
                                         quad_ptr_->dsh_dy(a)*D_arr[i][1][k][1]*quad_ptr_->dsh_dy(b))*JxW_;
        }
    
        //------------------------------Mat3---------------------------------------------------------------------------------------------
        const mat Mat3 = (a_damping_*Mat1) + (b_damping_*Mat2);
    
        m_ += (alpha_m_*Mat1) + (alpha_f_* beta_*dt_*dt_*Mat2) + (alpha_f_* gamma_*dt_*Mat3);
    
    
    
    
    
    
        //----------------------------vect1------------------------------------------------------------------------------------------------
        const vec Vect1 = prod(Mat1, I_dofs_solid_a_);
    
        //----------------------------vect2------------------------------------------------------------------------------------------------
        vec Vect2(2*nb_el_nodes_, 0.0);
    
        if(material_type_ == "Hookes")
          Vect2 = prod(Mat2, I_dofs_solid_u_);
    
        else if(material_type_ == "St_V_Kirchhoff" || material_type_ == "Neo_Hookean")
        {
          for(int b=0; b<nb_el_nodes_; b++)
            for(int jb=0; jb<2; jb++)    
              Vect2[2*b+jb] = (quad_ptr_->dsh_dx(b)*P_(jb,0) + quad_ptr_->dsh_dy(b)*P_(jb,1) )*JxW_;
        }
        
        //----------------------------vect3------------------------------------------------------------------------------------------------
        const vec Vect3 = prod(Mat3, I_dofs_solid_v_);
    
        //----------------------------vect4------------------------------------------------------------------------------------------------
        const vec Vect4 = prod(boost::numeric::ublas::trans(N_),gravity_)*rho_*JxW_;
         
        r_ += Vect1 + Vect2 + Vect3 - Vect4;   
      }



      if(el.on_boundary())
      {
         for(int i=0; i<el.nb_sides(); i++)
         {
           if(el.is_side_on_boundary(i))
           {
             auto bd_nodes = el.side_nodes(i);
             auto side_flag = el.side_flag(i);
   
             if(side_flag==1)
             {            
               std::vector<std::vector<double>> f_tr;
               ic_bc_.get_fluid_tr(bd_nodes, f_tr);            
               
               for(int j=0; j<el.nb_side_gp(i); j++)
               {
                 quad_ptr_ = el.quad_b(i,j);
   
                 N();
                 vec tr(2);
                 for(int i=0; i<2; i++)
                  tr[i] = -f_tr[j][i];   // this is to make traction outward for solid domain
                        
                 r_ -= prod(boost::numeric::ublas::trans(N_), tr)*quad_ptr_->W_bd();
               }
             }
             else
             {
                vec traction(2,0.0);         
                double tau11(0.0), tau22(0.0);
                double tau12(0.0), tau21(0.0);
          
                if(ic_bc_.neumann_tau11(bd_nodes,side_flag).first)      tau11 = ic_bc_.neumann_tau11(bd_nodes,side_flag).second;
                if(ic_bc_.neumann_tau22(bd_nodes,side_flag).first)      tau22 = ic_bc_.neumann_tau22(bd_nodes,side_flag).second;
                if(ic_bc_.neumann_tau12(bd_nodes,side_flag).first)      tau12 = ic_bc_.neumann_tau12(bd_nodes,side_flag).second;
                tau21 = tau12;
   
                for(const auto& quad_ptr: el.quad_b(i))
                {
                  quad_ptr_ = quad_ptr;               
                  JNxW_ = quad_ptr_->JNxW();
            
                  //    This needs to be defined for hyperelastic material because unlike tau, P is not symmetric
                  //    if(ic_bc_.neumann_tau21(bd_nodes,side_flag).first)       tau21 = ic_bc_.neumann_tau21(bd_nodes,side_flag).second;
                  
                  
                  traction[0] = tau11*JNxW_[0] + tau12*JNxW_[1];
                  traction[1] = tau21*JNxW_[0] + tau22*JNxW_[1];
            
                  N();
                  r_ -= prod(boost::numeric::ublas::trans(N_),traction);
               }
             }
           }
         }      
      }
            
      m = m_;
      r = -r_;      
    }














      

  void finite_def(double E, double (&D_arr)[2][2][2][2])
  {
    vec dI_dx(2,0.0), dI_dy(2,0.0);    
    quad_ptr_->x_derivative_interpolate(I_dofs_solid_u_,dI_dx);
    quad_ptr_->y_derivative_interpolate(I_dofs_solid_u_,dI_dy);

    mat del_u(2,2,0.0);    
    for(int i=0; i<2; i++)
    {    
     del_u(i,0) = dI_dx[i];   
     del_u(i,1) = dI_dy[i];
    }

    const boost::numeric::ublas::identity_matrix<double> I_mat(2,2);       
    const mat F = I_mat + del_u;
    const mat C = prod(boost::numeric::ublas::trans(F),F);
    const mat E_strain = 0.5*(C-I_mat); 

    const double mu = 0.5*E/(1.0 + nu_);    
    double lambda(0.0);
    if(condition_type_ == "Plain_Strain")  lambda = E*nu_/((1.0+nu_)*(1.0-2.0*nu_));
    else if(condition_type_ == "Plain_Stress")  lambda = E*nu_/(1.0-nu_*nu_);

    double D[2][2][2][2] = {};
    mat S(2,2,0.0);

    if(material_type_ == "St_V_Kirchhoff")
    { 
       for(int I=0; I<2; I++)
        for(int J=0; J<2; J++)
         for(int K=0; K<2; K++)
          for(int L=0; L<2; L++)
              D[I][J][K][L] = lambda*I_mat(I,J)*I_mat(K,L) + mu*(I_mat(I,K)*I_mat(J,L) + I_mat(I,L)*I_mat(J,K));
                         
       for(int I=0; I<2; I++)
        for(int J=0; J<2; J++)
         for(int K=0; K<2; K++)
          for(int L=0; L<2; L++)
           S(I,J) += D[I][J][K][L] * E_strain(K,L);
    }

    else if(material_type_ == "Neo_Hookean")
    { 
      const double J = F(0,0)*F(1,1) - F(0,1)*F(1,0);        
      const double trC = C(0,0) + C(1,1) + 1.0;
      const mat C_inv = inverse(C);
      const double kappa = lambda + 2.0*mu/3.0;
      const double use = mu*std::pow(J, -2.0/3.0);      
      const double use1 = 2.0/9.0*use*trC + kappa*J*J;
      const double use2 = 2.0/3.0*use*trC - kappa*(J*J-1.0);
      const double use3 = 2.0/3.0*use;
    
       for(int I=0; I<2; I++)
        for(int J=0; J<2; J++)
         for(int K=0; K<2; K++)
          for(int L=0; L<2; L++)
            D[I][J][K][L] = use1*C_inv(I,J)*C_inv(K,L) + use2*0.5*(C_inv(I,K)*C_inv(J,L) + C_inv(I,L)*C_inv(J,K)) - use3*(I_mat(I,J)*C_inv(K,L) + C_inv(I,J)*I_mat(K,L));
                         
       S = use*(I_mat - trC/3.0*C_inv) + 0.5*kappa*(J*J - 1.0)*C_inv;
    }
    
    P_ = prod(F,S);    
    
    for(int i=0; i<2; i++)
     for(int J=0; J<2; J++)
      for(int k=0; k<2; k++)
       for(int L=0; L<2; L++)
       {
            D_arr[i][J][k][L] += S(J,L)*I_mat(i,k);
            
            for(int I=0; I<2; I++)
             for(int K=0; K<2; K++)
               D_arr[i][J][k][L] += F(i,I)*D[I][J][K][L]*F(k,K);   
       }
  }





  void D()
  {
    D_.clear();

    const double mu = 0.5*E_/(1.0 + nu_);    
    double lambda(0.0);
    if(condition_type_ == "Plain_Strain")  lambda = E_*nu_/((1.0+nu_)*(1.0-2.0*nu_));
    else if(condition_type_ == "Plain_Stress")  lambda = E_*nu_/(1.0-nu_*nu_);
    
    D_(0,0) = lambda + 2.0*mu;
    D_(0,1) = lambda;
    D_(1,0) = lambda;
    D_(1,1) = lambda + 2.0*mu;
    D_(2,2) = mu;        
  }




   void N()
   {
    N_.clear();
    for(int i=0; i<nb_el_nodes_; i++)
    {
       N_(0,2*i) = quad_ptr_->sh(i);
       N_(1,2*i+1) = quad_ptr_->sh(i);
    }   
   }



   
   void B()
   {
    B_.clear();
    for(int i=0; i<nb_el_nodes_; i++)
    {
       B_(0,2*i) = quad_ptr_->dsh_dx(i);
       B_(1,2*i+1) = quad_ptr_->dsh_dy(i);
       B_(2,2*i)   = quad_ptr_->dsh_dy(i);
       B_(2,2*i+1) = quad_ptr_->dsh_dx(i);
    }   
   }





   private:

    int nb_el_nodes_;
    
    ic_bc_type& ic_bc_;
    solid_properties& props_;
    read_setup& setup_;

    double JxW_; 
    vec JNxW_;
    double dt_;

    std::string material_type_;
    std::string condition_type_;

    double rho_, nu_, E_;
    double a_damping_, b_damping_;

    mat N_;
    mat D_;      
    mat B_;

    mat P_;

    vec I_dofs_solid_u_;
    vec I_dofs_solid_v_;
    vec I_dofs_solid_a_;

    vec r_;
    mat m_;

    vec gravity_;   

    double alpha_m_, alpha_f_, gamma_, beta_;
    std::shared_ptr<quad> quad_ptr_ = nullptr;

    point<2> dummy_;    
  }; 






 
} 

#endif

