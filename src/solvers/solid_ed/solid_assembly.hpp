#ifndef SOLID_ASSEMBLY_HPP
#define SOLID_ASSEMBLY_HPP




#include "../../fem/fem.hpp"



namespace GALES{



  template<typename integral_type, typename dofs_ic_bc_type, int dim>
  class solid_assembly
  {
      using model_type = model<dim>;
      using vec = boost::numeric::ublas::vector<double>;
      using mat = boost::numeric::ublas::matrix<double>;
    
    public:


    double execute(integral_type& integral, dofs_ic_bc_type& dofs_ic_bc, model_type& u_model, model_type& v_model, model_type& a_model, linear_system& lp)
    {
      double start(MPI_Wtime());      
      
      std::vector<vec> dofs_solid_u, dofs_solid_v, dofs_solid_a;

      lp.clear();
      for(const auto& el : u_model.mesh().elements())
      {
	 u_model.extract_element_dofs(*el, dofs_solid_u);
	 v_model.extract_element_dofs(*el, dofs_solid_v);
	 a_model.extract_element_dofs(*el, dofs_solid_a);
	
        const int nb_dofs = dofs_solid_u[0].size();
        vec r(nb_dofs, 0.0);
        mat m(nb_dofs, nb_dofs, 0.0);

	integral.execute(*el, dofs_solid_u, dofs_solid_v, dofs_solid_a, m, r);
	lp.assemble(el->dofs_gids(), el->dofs_constraints(dofs_ic_bc), m, r);
      }

      lp.assembled();       
      return MPI_Wtime()-start;                  
    }

  };



}

#endif
