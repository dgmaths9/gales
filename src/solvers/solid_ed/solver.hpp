#include <mpi.h>
#include <iostream>
 
 
#include "../../fem/fem.hpp"
#include "solid.hpp"





using namespace GALES;




template<int dim>
void solver(read_setup& setup)
{
  #include "constructors.hpp"
  
 


  time::get().t(0.0); 
  time::get().delta_t(setup.delta_t());	

  double restart_or_t_zero = MPI_Wtime();  
  if(!setup.restart())
  {
    //--------- setting up dofs for time = 0 ----------------
    solid_dofs_ic_bc.dirichlet_bc();
    solid_io.write("solid/u/", *solid_u_dof_state);
    solid_io.write("solid/v/", *solid_v_dof_state);
    solid_io.write("solid/a/", *solid_a_dof_state);
    print_only_pid<0>(std::cerr)<<"Dofs writing for time 0 took: "<<std::setprecision(setup.precision()) << MPI_Wtime()-restart_or_t_zero<<" s\n\n";       
  } 
  else
  {
    //--------------read restart-----------------------------    
     time::get().t(setup.restart_time());
     solid_io.read("solid/u/", *solid_u_dof_state);
     solid_io.read("solid/v/", *solid_v_dof_state);
     solid_io.read("solid/a/", *solid_a_dof_state);
    print_only_pid<0>(std::cerr)<<"Dofs reading at restart time " << time::get().t() << " took: "<<std::setprecision(setup.precision()) << MPI_Wtime()-restart_or_t_zero<<" s\n\n";       
  }
  time::get().tick();




  //---------- starting time of time loop ---------------------
  double time_loop_start = MPI_Wtime();
  double non_linear_res(0.0);
  int tot_time_iterations(0);

  
  for(; time::get().t()<=setup.end_time(); time::get().tick())
  {
    double t_iteration(MPI_Wtime());
  
    print_only_pid<0>(std::cerr)<<"simulation time:  "<<std::setprecision(setup.precision()) << time::get().t()<<" s.\n";


    solid_updater.predictor(*solid_u_dof_state, *solid_v_dof_state, *solid_a_dof_state);


    solid_dofs_ic_bc.dirichlet_bc();
        


    for(int it_count=0; it_count < setup.n_max_it(); it_count++)   //iterations loop
    {
        //------------------------ SOLUTION For solid ------------------------
	double fill_time = solid_assembly.execute(solid_integral, solid_dofs_ic_bc, solid_u_model, solid_v_model, solid_a_model, solid_lp);   
        
        
        bool residual_converged = solid_res_check.execute(*solid_lp.rhs(), it_count, non_linear_res);      
        if(residual_converged)
        {
          print_solver_info("SOLID  ", it_count, fill_time, non_linear_res);
      	  break;
        } 	      
        double solver_time = solid_ls_solver.execute(solid_lp);


     	solid_updater.corrector(*solid_u_dof_state, *solid_v_dof_state, *solid_a_dof_state, solid_io.state_vec(*solid_ls_solver.solution()));
 
        print_solver_info("SOLID  ", it_count, fill_time, non_linear_res, solver_time, solid_ls_solver);        

        // -----------------------end of solid solution---------------------------------------------------------
    }




    if((tot_time_iterations+1)%(setup.print_freq()) == 0)
    {
      solid_io.write("solid/u/", *solid_u_dof_state);
      solid_io.write("solid/v/", *solid_v_dof_state);
      solid_io.write("solid/a/", *solid_a_dof_state);
    }



     //-------- adaptive time step-----------------
     if(setup.adaptive_time_step() == true && tot_time_iterations > setup.N())
     {
        adaptive_time_solid_ed(solid_u_model, solid_props, setup);
     }


    print_only_pid<0>(std::cerr)<<"Time step took: "<<MPI_Wtime()-t_iteration<<" s\n\n";
    tot_time_iterations++;    	  
  }


  print_only_pid<0>(std::cerr)<<"End time reached!!!! "<<"\n\n";
  print_only_pid<0>(std::cerr)<<"Total run time: "<<MPI_Wtime()-time_loop_start<<" s\n\n\n";

}









int main(int argc, char* argv[])
{

  MPI_Init(&argc, &argv);


  //---------- creating directories -----------------------------
  make_dirs("results/solid/u results/solid/v results/solid/a");
  

  //---------------- read setup file -------------------------------
  read_setup setup;  


  if(setup.dim()==2)  solver<2>(setup);
  else if(setup.dim()==3)  solver<3>(setup);


  MPI_Barrier(MPI_COMM_WORLD);
  MPI_Finalize();
  return 0;
}
