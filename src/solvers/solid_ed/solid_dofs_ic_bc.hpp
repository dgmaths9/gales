#ifndef SOLID_DOFS_IC_BC_HPP
#define SOLID_DOFS_IC_BC_HPP




#include "../../fem/fem.hpp"



namespace GALES{

 

  template<typename ic_bc_type, int dim>
  class solid_dofs_ic_bc{};

 
 
 
  template<typename ic_bc_type>
  class solid_dofs_ic_bc <ic_bc_type, 2>
  {
    using model_type = model<2>;

   public :

   solid_dofs_ic_bc(model_type& u_model, model_type& v_model, model_type& a_model, ic_bc_type& ic_bc)
   : 
   u_model_(u_model), v_model_(v_model), a_model_(a_model), ic_bc_(ic_bc)
   {
        const double alpha_m = 0.5*(3.0-u_model_.setup().rho_inf())/(1.0+u_model_.setup().rho_inf());
        const double alpha_f = 1.0/(1.0+u_model_.setup().rho_inf());
        gamma_ = 0.5 + alpha_m - alpha_f;
        beta_ = 0.25*std::pow(1.0 + alpha_m - alpha_f, 2);        
        ic();
   }





    void ic() 
    {
      for(const auto& nd : u_model_.mesh().nodes()) 
      {
         const int first_dof_lid (nd->first_dof_lid());
         
         u_model_.state().set_dof(first_dof_lid,   ic_bc_.initial_ux(*nd));
         u_model_.state().set_dof(first_dof_lid+1, ic_bc_.initial_uy(*nd));
         
         v_model_.state().set_dof(first_dof_lid,   ic_bc_.initial_vx(*nd));
         v_model_.state().set_dof(first_dof_lid+1, ic_bc_.initial_vy(*nd));
      }     
    }
  
  
  
  

    void update_models(const node<2>& nd, int dof_indx, double x)
    {
        const double dt(time::get().delta_t());  
        u_model_.state().set_dof(nd.first_dof_lid()+dof_indx, x);
        auto u_n_1 = x;
        auto u_n = u_model_.extract_node_dof(nd.lid(), dof_indx, 1);
        auto v_n = v_model_.extract_node_dof(nd.lid(), dof_indx, 1);
        auto a_n = a_model_.extract_node_dof(nd.lid(), dof_indx, 1);
        auto a_n_1 = (u_n_1-u_n-dt*v_n)/(beta_*dt*dt) - (1-2*beta_)/(2*beta_)*a_n;
        auto v_n_1 = v_n + dt*(1.0-gamma_)*a_n + dt*gamma_*a_n_1;
        a_model_.state().set_dof(nd.first_dof_lid()+dof_indx, a_n_1);
        v_model_.state().set_dof(nd.first_dof_lid()+dof_indx, v_n_1);                      
    }




    void dirichlet_bc() 
    {
      std::pair<bool,double> result;

      for(const auto& nd : u_model_.mesh().nodes()) 
      {
         const int first_dof_lid (nd->first_dof_lid());

         result = ic_bc_.dirichlet_ux(*nd);
         if (result.first)  update_models(*nd, 0, result.second);        

         result = ic_bc_.dirichlet_uy(*nd);
         if (result.first)  update_models(*nd, 1, result.second);      
      }
    }  





    auto dofs_constraints(const node<2>& nd)const  
    {
      std::vector<std::pair<bool,double>> nd_dofs_constraints(nd.nb_dofs(), std::make_pair(false, 0.0));              
      if(ic_bc_.dirichlet_ux(nd).first) nd_dofs_constraints[0] = std::make_pair(true, 0.0);
      if(ic_bc_.dirichlet_uy(nd).first) nd_dofs_constraints[1] = std::make_pair(true, 0.0);      
      return nd_dofs_constraints;
    }




    private:
    model_type& u_model_;
    model_type& v_model_;
    model_type& a_model_;
    ic_bc_type& ic_bc_;
    double gamma_, beta_;
  };
  
  
  
  
  
  
  
  
  
  
  
  
  template<typename ic_bc_type>
  class solid_dofs_ic_bc <ic_bc_type, 3>
  {
    using model_type = model<3>;

   public :

   solid_dofs_ic_bc(model_type& u_model, model_type& v_model, model_type& a_model, ic_bc_type& ic_bc)
   : 
   u_model_(u_model), v_model_(v_model), a_model_(a_model), ic_bc_(ic_bc)
   {
        const double alpha_m = 0.5*(3.0-u_model_.setup().rho_inf())/(1.0+u_model_.setup().rho_inf());
        const double alpha_f = 1.0/(1.0+u_model_.setup().rho_inf());
        gamma_ = 0.5 + alpha_m - alpha_f;
        beta_ = 0.25*std::pow(1.0 + alpha_m - alpha_f, 2);                 
        ic();
   }
   

    void ic() 
    {
      for(const auto& nd : u_model_.mesh().nodes()) 
      {
         const int first_dof_lid (nd->first_dof_lid());
         
         u_model_.state().set_dof(first_dof_lid,   ic_bc_.initial_ux(*nd));
         u_model_.state().set_dof(first_dof_lid+1, ic_bc_.initial_uy(*nd));
         u_model_.state().set_dof(first_dof_lid+2, ic_bc_.initial_uz(*nd));
         
         v_model_.state().set_dof(first_dof_lid,   ic_bc_.initial_vx(*nd));
         v_model_.state().set_dof(first_dof_lid+1, ic_bc_.initial_vy(*nd));
         v_model_.state().set_dof(first_dof_lid+2, ic_bc_.initial_vz(*nd));
      }     
    }




    void update_models(const node<3>& nd, int dof_indx, double x)
    {
         const double dt(time::get().delta_t());  
         u_model_.state().set_dof(nd.first_dof_lid()+dof_indx, x);
         auto u_n_1 = x;
         auto u_n = u_model_.extract_node_dof(nd.lid(), dof_indx, 1);
         auto v_n = v_model_.extract_node_dof(nd.lid(), dof_indx, 1);
         auto a_n = a_model_.extract_node_dof(nd.lid(), dof_indx, 1);
         auto a_n_1 = (u_n_1-u_n-dt*v_n)/(beta_*dt*dt) - (1-2*beta_)/(2*beta_)*a_n;
         auto v_n_1 = v_n + dt*(1.0-gamma_)*a_n + dt*gamma_*a_n_1;
         a_model_.state().set_dof(nd.first_dof_lid()+dof_indx, a_n_1);
         v_model_.state().set_dof(nd.first_dof_lid()+dof_indx, v_n_1);                      
    }

     


    void dirichlet_bc() 
    {
      std::pair<bool,double> result;

      for(const auto& nd : u_model_.mesh().nodes()) 
      {
         const int first_dof_lid (nd->first_dof_lid());

         result = ic_bc_.dirichlet_ux(*nd);
         if (result.first) update_models(*nd, 0, result.second);         

         result = ic_bc_.dirichlet_uy(*nd);
         if (result.first) update_models(*nd, 1, result.second);         

         result = ic_bc_.dirichlet_uz(*nd);
         if (result.first) update_models(*nd, 2, result.second);            
      }
    }  



    auto dofs_constraints(const node<3>& nd)const  
    {
      std::vector<std::pair<bool,double>> nd_dofs_constraints(nd.nb_dofs(), std::make_pair(false, 0.0));              
      if(ic_bc_.dirichlet_ux(nd).first) nd_dofs_constraints[0] = std::make_pair(true, 0.0);
      if(ic_bc_.dirichlet_uy(nd).first) nd_dofs_constraints[1] = std::make_pair(true, 0.0);      
      if(ic_bc_.dirichlet_uz(nd).first) nd_dofs_constraints[2] = std::make_pair(true, 0.0);      
      return nd_dofs_constraints;
    }
   
  
    private:
    model_type& u_model_;
    model_type& v_model_;
    model_type& a_model_;
    ic_bc_type& ic_bc_;
    double gamma_, beta_;
  };

}

#endif



