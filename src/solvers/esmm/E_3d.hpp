#ifndef E_3D_HPP_
#define E_3D_HPP_



#include "../../fem/fem.hpp"




namespace GALES {





  template<>
  class elastostatic<3>
  {
    using element_type = element<3>;
    using mat = boost::numeric::ublas::matrix<double>;

    public :

    elastostatic(model<3>& model)
    :
    setup_(model.setup())
    {}




    void execute(const element_type& el, mat& m)
    {
      nb_el_nodes_ = el.nb_nodes();      
      for(const auto& quad_ptr : el.quad_i())
      {
          quad_ptr_ = quad_ptr;

          const double E  = 1000.0; 
          const double nu = 0.3; 
    
          const double mu= E/(2*(1+nu)); 
          const double lambda = (E*nu)/((1+nu)*(1-2*nu)); 
     
          mat K(6,6,0.0);
    
          K(0,0) = lambda + 2*mu;
          K(0,1) = lambda;
          K(0,2) = lambda;
          K(1,0) = lambda;
          K(1,1) = lambda + 2*mu;
          K(1,2) = lambda;
          K(2,0) = lambda;
          K(2,1) = lambda;
          K(2,2) = lambda + 2*mu;
          K(3,3) = mu;
          K(4,4) = mu;
          K(5,5) = mu;
    
          mat B(6,3*nb_el_nodes_,0.0);
    
          for(int i=0; i<nb_el_nodes_; i++)
          {
            B(0,3*i)   = quad_ptr_->dsh_dx(i);
            B(1,3*i+1) = quad_ptr_->dsh_dy(i);
            B(2,3*i+2) = quad_ptr_->dsh_dz(i);
            
            B(3,3*i)   = quad_ptr_->dsh_dy(i);
            B(3,3*i+1) = quad_ptr_->dsh_dx(i);
            
            B(4,3*i+1) = quad_ptr_->dsh_dz(i);          
            B(4,3*i+2) = quad_ptr_->dsh_dy(i);
              
            B(5,3*i)   = quad_ptr_->dsh_dz(i);
            B(5,3*i+2) = quad_ptr_->dsh_dx(i);
          }
    
          const mat KB = prod(K,B);
          const mat Btrn= boost::numeric::ublas::trans(B);
          const mat BtrnKB = prod(Btrn,KB);
          m += BtrnKB * std::pow(quad_ptr_->J(), 1.0-setup_.mesh_stiffness()) * quad_ptr_->W_in();
      }
    }
  

 

  private:
    int nb_el_nodes_;
    read_setup& setup_;
    std::shared_ptr<quad> quad_ptr_ = nullptr;

  };
  

}

#endif

