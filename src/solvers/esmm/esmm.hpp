#ifndef _GALES_ELASTOSTATIC_HPP_
#define _GALES_ELASTOSTATIC_HPP_

#include "E_dofs_ic_bc.hpp"
#include "E_assembly.hpp"
#include "E_2d.hpp"
#include "E_3d.hpp"

#endif 
