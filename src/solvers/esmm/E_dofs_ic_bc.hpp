#ifndef E_DOFS_IC_BC_HPP_
#define E_DOFS_IC_BC_HPP_


#include"../../fem/fem.hpp"


namespace GALES {


  
  template<typename ic_bc_type, int dim>
  class elastostatic_dofs_ic_bc{};





  template<typename ic_bc_type>
  class elastostatic_dofs_ic_bc<ic_bc_type, 2>
  {
    public :
         
     elastostatic_dofs_ic_bc(model<2>& e_model, ic_bc_type& ic_bc): 
     state_(e_model.state()), mesh_(e_model.mesh()), ic_bc_(ic_bc)
     {
       ic();
     }



    void ic()
    {
      for(const auto& nd : mesh_.nodes())
      {
        const int first_dof_lid (nd->first_dof_lid());
        state_.set_dof(first_dof_lid+0, ic_bc_.initial_ux(*nd));
        state_.set_dof(first_dof_lid+1, ic_bc_.initial_uy(*nd));
      }
    }
     


    void dirichlet_bc()
    {
      std::pair<bool,double> result;

      for(const auto& nd : mesh_.nodes())
      {
        const int first_dof_lid (nd->first_dof_lid());

        result = ic_bc_.dirichlet_ux(*nd);
        if (result.first)        state_.set_dof(first_dof_lid+0, result.second);

        result = ic_bc_.dirichlet_uy(*nd);
        if (result.first)        state_.set_dof(first_dof_lid+1, result.second);
      }
    }





    auto dofs_constraints(const node<2> &nd) const
    {
      std::vector<std::pair<bool,double>> nd_dofs_constraints(nd.nb_dofs(), std::make_pair(false, 0.0));              
      if(ic_bc_.dirichlet_ux(nd).first) nd_dofs_constraints[0] = std::make_pair(true, ic_bc_.dirichlet_ux(nd).second);
      if(ic_bc_.dirichlet_uy(nd).first) nd_dofs_constraints[1] = std::make_pair(true, ic_bc_.dirichlet_uy(nd).second);      
      return nd_dofs_constraints;
    }



    private:
    dof_state& state_;
    Mesh<2>& mesh_;
    ic_bc_type& ic_bc_;
    
  };













  template<typename ic_bc_type>
  class elastostatic_dofs_ic_bc<ic_bc_type, 3>
  {
    public :
         
     elastostatic_dofs_ic_bc(model<3>& e_model, ic_bc_type& ic_bc): 
     state_(e_model.state()), mesh_(e_model.mesh()), ic_bc_(ic_bc)
     {
       ic();
     }


     
    void ic()
    {
      for(const auto& nd : mesh_.nodes())
      {
        const int first_dof_lid (nd->first_dof_lid());
        state_.set_dof(first_dof_lid+0, ic_bc_.initial_ux(*nd));
        state_.set_dof(first_dof_lid+1, ic_bc_.initial_uy(*nd));
        state_.set_dof(first_dof_lid+2, ic_bc_.initial_uz(*nd));
      }
    }



    void dirichlet_bc()
    {
      std::pair<bool,double> result;

      for(const auto& nd : mesh_.nodes())
      {
        const int first_dof_lid (nd->first_dof_lid());

        result= ic_bc_.dirichlet_ux(*nd);
        if (result.first)        state_.set_dof(first_dof_lid+0, result.second);

        result= ic_bc_.dirichlet_uy(*nd);
        if (result.first)        state_.set_dof(first_dof_lid+1, result.second);

        result= ic_bc_.dirichlet_uz(*nd);
        if (result.first)        state_.set_dof(first_dof_lid+2, result.second);
      }
    }





    auto dofs_constraints(const node<3> &nd) const
    {
      std::vector<std::pair<bool,double>> nd_dofs_constraints(nd.nb_dofs(), std::make_pair(false, 0.0));              
      if(ic_bc_.dirichlet_ux(nd).first) nd_dofs_constraints[0] = std::make_pair(true, ic_bc_.dirichlet_ux(nd).second);
      if(ic_bc_.dirichlet_uy(nd).first) nd_dofs_constraints[1] = std::make_pair(true, ic_bc_.dirichlet_uy(nd).second);
      if(ic_bc_.dirichlet_uz(nd).first) nd_dofs_constraints[2] = std::make_pair(true, ic_bc_.dirichlet_uz(nd).second);      
      return nd_dofs_constraints;
    }



    private:
    dof_state& state_;
    Mesh<3>& mesh_;
    ic_bc_type& ic_bc_;
    
  };

  
} 

#endif

