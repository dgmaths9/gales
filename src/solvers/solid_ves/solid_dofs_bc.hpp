#ifndef SOLID_DOFS_BC_HPP
#define SOLID_DOFS_BC_HPP



#include "../../fem/fem.hpp"



namespace GALES{


 
  template <int dim, typename ic_bc_type>
  class solid_u_dofs_bc
  {

    using model_type = model<dim>;
    
    public :

     solid_u_dofs_bc(model_type& s_model, ic_bc_type& ic_bc): 
     state_(s_model.state()), mesh_(s_model.mesh()), ic_bc_(ic_bc)
     {}
 
    
    
    void dirichlet_bc(point_2d& dummy) 
    {
      std::pair<bool,double> result;

      for(const auto& nd : mesh_.nodes()) 
      {
        const int first_dof_lid (nd->first_dof_lid());
	
        result = ic_bc_.dirichlet_ux(*nd);
        if (result.first)         state_.set_dof(first_dof_lid+0, result.second);

        result = ic_bc_.dirichlet_uy(*nd);
        if (result.first)         state_.set_dof(first_dof_lid+1, result.second);		

      }
    } 
  



    void dirichlet_bc(point_3d& dummy) 
    {
      std::pair<bool,double> result;

      for(const auto& nd : mesh_.nodes()) 
      {
        const int first_dof_lid (nd->first_dof_lid());
	
        result = ic_bc_.dirichlet_ux(*nd);
        if (result.first)         state_.set_dof(first_dof_lid+0, result.second);

        result = ic_bc_.dirichlet_uy(*nd);
        if (result.first)         state_.set_dof(first_dof_lid+1, result.second);		

        result = ic_bc_.dirichlet_uz(*nd);
        if (result.first)         state_.set_dof(first_dof_lid+2, result.second);		
      }
    } 
  



   //--------------------------------- These are for fixing dofs in scatter -----------------------------------------
    template<typename nd_type>
    auto dirichlet(int dof, const nd_type &nd, const point_2d& dummy) const
    {
      const int nb_dofs = 2;
      const int n = dof%nb_dofs;
      std::pair<bool,double> result(false, 0.0);
      std::pair<bool,double> value(false, 0.0);
      
      switch(n)
      {
          case 0:
              result = ic_bc_.dirichlet_ux(nd);
              if(result.first == true) value = std::make_pair(true, result.second);
              break;
          case 1:
              result = ic_bc_.dirichlet_uy(nd);
              if(result.first == true) value = std::make_pair(true, result.second);
              break;
      }
      return value;
    }




    template<typename nd_type>
    auto dirichlet(int dof, const nd_type &nd, const point_3d& dummy) const
    {
      const int nb_dofs = 3;
      const int n = dof%nb_dofs;
      std::pair<bool,double> result(false, 0.0);
      std::pair<bool,double> value(false, 0.0);

      switch(n)
      {
          case 0:
              result = ic_bc_.dirichlet_ux(nd);
              if(result.first == true) value = std::make_pair(true, result.second);
              break;
          case 1:
              result = ic_bc_.dirichlet_uy(nd);
              if(result.first == true) value = std::make_pair(true, result.second);
              break;
          case 2:
              result = ic_bc_.dirichlet_uz(nd);
              if(result.first == true) value = std::make_pair(true, result.second);
              break;
      }
      return value;
    }





    private:
    dof_state& state_;
    simple_mesh<dim>& mesh_;
    ic_bc_type& ic_bc_;    

  };
  


}

#endif



