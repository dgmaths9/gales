#ifndef SOLID_UPDATER_HPP
#define SOLID_UPDATER_HPP



namespace GALES{





  class solid_updater
  {

    using vec = boost::numeric::ublas::vector<double>;

    public:

    solid_updater(dof_state& state): state_(state) {}


    void predictor()
    {
      state_.dofs(1) = state_.dofs(0); //P = last corrected
      state_.dofs(0) = state_.dofs(1); //I  = last corrected
    }


    void corrector(const vec& correction)
    {
      state_.dofs(0) = correction;            
    }


    private:
    dof_state& state_;

  };


}

#endif


