#ifndef HISTORY_SYSTEM_STATE_HPP
#define HISTORY_SYSTEM_STATE_HPP



#include "boost/numeric/ublas/vector.hpp"



namespace GALES{



class dofs_history_state 
{

  public:

  typedef typename boost::numeric::ublas::vector<double> v_type;


  template<typename mesh_type>
  dofs_history_state(const mesh_type& mesh, physical_properties& props, int n_slot):  state_(n_slot),  n_slot_(n_slot)
  {
    std::vector<int> gid_list;

    const int dim = mesh.dimension();    
    for(const auto& el : mesh.elements())
    {
        const int el_gid = el->gid();
        if(dim == 2)
        {
          const int a = el->nb_nodes()*props.s_nb_Maxwell_el_*3;
          for(int j=0; j<a; j++)
            gid_list.push_back(el_gid*a + j);
        }
        else if(dim == 3)
        {
          const int a = el->nb_nodes()*props.s_nb_Maxwell_el_*6;
          for(int j=0; j<a; j++)
            gid_list.push_back(el_gid*a + j);
        } 
    }
    sort_unique(gid_list);
    
    history_map_ = Epetra_Map(-1, gid_list.size(), &(gid_list[0]), 0, Epetra_MpiComm(MPI_COMM_WORLD));

    for(int i=0; i<n_slot_; i++)
       state_[i] = std::make_unique<vec>(history_map_.NumMyElements(),0.0);
  }


    
    auto num_slot()const { return n_slot_; }
    auto& map()const { return history_map_; }
    auto& dofs(int index)const {return *state_[index];}
    auto nb_dofs()const { return history_map_.NumMyElements(); }
    auto global_size()const { return history_map_.MaxAllGID()+1; }
    auto get_dof(int lid, int slot = 0)const {return (*state_[slot])[lid];}
    void set_dof(int lid, double v, int slot = 0) { (*state_[slot])[lid] = v;} 



  
  private:
  std::vector<std::shared_ptr<v_type>> state_;
  int n_slot_;
  Epetra_Map history_map_;
  
};




}

#endif
