#ifndef SOLID_3D_HPP
#define SOLID_3D_HPP




#include "../../fem/fem.hpp"



namespace GALES{


  
   
  template<typename ic_bc_type, int dim> class solid{};



  template<typename ic_bc_type>
  class solid<ic_bc_type, 3>
  {
    using element_type = element<3>;
    using vec = boost::numeric::ublas::vector<double>;
    using mat = boost::numeric::ublas::matrix<double>;
    using v_vec = boost::numeric::ublas::vector<vec>;
    using vv_vec = boost::numeric::ublas::vector<v_vec>;

   public :  

    solid
    (
     ic_bc_type& ic_bc,
     physical_properties& props,
     read_setup& setup
    ):

      nb_el_nodes_(setup.nb_el_nodes()),

      ic_bc_(ic_bc),
      material_(material),      
      setup_(setup),
            
      JNxW_(3,0.0),
      JxW_(0.0),
      dt_(0.0), 

      rho_(props.s_rho_),
      nu_(props.s_nu_),
      E_inf_(props.s_E_),
      
      
      nb_Maxwell_elements_(props.s_nb_Maxwell_el_),
      E_(props.s_Max_el_E_),
      eta_(props.s_Max_el_eta_),            
      relaxation_time_(nb_Maxwell_elements_, 0.0),
      exponential_(nb_Maxwell_elements_,0.0),
      A_(nb_Maxwell_elements_,0.0),
      

      a_damping_(props.s_a_damping_), 
      b_damping_(props.s_b_damping_),

      N_(3,3*nb_el_nodes_,0.0),
      D_(6,6,0.0),
      B_(6,nb_el_nodes_*3,0.0),
      P_history_(nb_el_nodes_*nb_Maxwell_elements_*6,0.0),
      PP_history_(nb_el_nodes_*nb_Maxwell_elements_*6,0.0),
      P_history_gn_(nb_el_nodes_*nb_Maxwell_elements_*6,0.0),
      
      P_dofs_solid_u_(3*nb_el_nodes_,0.0),
      PP_dofs_solid_u_(3*nb_el_nodes_,0.0),

      r_(3*nb_el_nodes_,0.0),
      m_(3*nb_el_nodes_,3*nb_el_nodes_,0.0),
      
      gravity_(3,0.0)
     {                    
       for(int i=0;i<nb_Maxwell_elements_;i++) 
           relaxation_time_[i] = eta_[i]/E_[i];          

       ic_bc_.body_force(gravity_, dummy_);         
       quad_ = std::make_unique<quad<3>>(setup_);
     }

  
  
  



    void set_prony() 
    {
       vec plus_exponent(nb_Maxwell_elements_, 0.0);
       for(int i=0; i<nb_Maxwell_elements_; i++) 
       {
	   plus_exponent[i] = dt_/relaxation_time_[i];
           exponential_[i] = exp(-plus_exponent[i]);
	   A_[i] = (1.0-exponential_[i])/plus_exponent[i];
       }    
    }





    void execute_i(const element_type& el, const std::vector<vec>& dofs_u, const vec& PP_history, mat& m, vec& r, vec& P_history )
    {
      dt_ = time::get().delta_t();
      
      P_dofs_solid_u_ = dofs_u[1];
      PP_dofs_solid_u_ = dofs_u[2];
      PP_history_ = PP_history;

      set_prony();

      r_.clear();
      m_.clear();      
      P_history_.clear();
      
      for (const auto& gp : quad_->gp())
      {
         quad_->calc(el, gp);
         JxW_ = quad_->JxW();      
         RM_i(gp);
         P_history_ += P_history_gn_;      
      }
      m = m_;
      r = r_;
      P_history =  P_history_;
    }






    void execute_b(const element_type& el, vec& r)
    {
      r_.clear();                  
      
      for(int i=0; i<el.nb_sides(); i++)
      {
        if(el.is_side_on_boundary(i))
        {
          auto bd_nodes = el.side_nodes(i);
          auto side_flag = el.side_flag(i);

          for(const auto& gp: quad_->side_gn(i))
          {
              quad_->calc(el, gp);
              quad_->compute_JN(gp);
              JNxW_ = quad_->JNxW();
              R_b(bd_nodes, side_flag);
          }
        }
      }
      r = r_;
    } 






  void RM_i(int g)
  {
    N();
    B();

  //------------------------------Mat2---------------------------------------------------------------------------------------------
    D(E_inf_);
    const mat use1 = prod(D_,B_);
    mat Mat2 = prod(boost::numeric::ublas::trans(B_),use1)*JxW_;

  //--------------------------------Mat_h1-----------------------------------------------------------------------
    mat tmp(6,6,0.0);
    for(int i=0;i<nb_Maxwell_elements_;i++) 
    {
        D(E_[i]);
        tmp += D_*A_[i];
    }
    const use1 = prod(tmp,B_);

    const mat h_mat = prod(boost::numeric::ublas::trans(B_),use1)*JxW_;
    Mat2 += h_mat;
           
    m_ += Mat2;




   //----------------------------vect2------------------------------------------------------------------------------------------------

        //--------------------------------Vect_h1---------------------------------
        vec tmp1(6,0.0);
        P_history(g);     

        int l(0);
        for(int j=0;j<6;j++)
        {
           int k(0);
           for(int i=0;i<nb_Maxwell_elements_;i++)
            {
              tmp1[j] += exponential_[i] * P_history_gn_[g*nb_Maxwell_elements_*6 + k + l];
              k += 6; 
            }
          l++;
        }
       
       vec Vect2 = -prod(boost::numeric::ublas::trans(B_),tmp1)*JxW_;
  
       Vect2 += prod(Mat2, P_dofs_solid_u_);  
   
     //----------------------------vect3------------------------------------------------------------------------------------------------
     const vec Vect3 = prod(boost::numeric::ublas::trans(N_),gravity_)*rho_*JxW_;

     r_ += Vect2 + Vect3;   

  }





  void R_b(const std::vector<int>& bd_nodes, int side_flag)
  {
      N();
      vec traction(3,0.0);
      double sigma11(0.0), sigma22(0.0), sigma33(0.0);
      double sigma12(0.0), sigma13(0.0), sigma23(0.0);
    
      if(ic_bc_.neumann_sigma11(bd_nodes,side_flag).first)         sigma11 = ic_bc_.neumann_sigma11(bd_nodes,side_flag).second; 
      if(ic_bc_.neumann_sigma22(bd_nodes,side_flag).first)         sigma22 = ic_bc_.neumann_sigma22(bd_nodes,side_flag).second; 
      if(ic_bc_.neumann_sigma33(bd_nodes,side_flag).first)         sigma33 = ic_bc_.neumann_sigma33(bd_nodes,side_flag).second;   
      if(ic_bc_.neumann_sigma12(bd_nodes,side_flag).first)         sigma12 = ic_bc_.neumann_sigma12(bd_nodes,side_flag).second;   
      if(ic_bc_.neumann_sigma13(bd_nodes,side_flag).first)         sigma13 = ic_bc_.neumann_sigma13(bd_nodes,side_flag).second;   
      if(ic_bc_.neumann_sigma23(bd_nodes,side_flag).first)         sigma23 = ic_bc_.neumann_sigma23(bd_nodes,side_flag).second; 

      traction[0] = sigma11*JNxW_[0] + sigma12*JNxW_[1] + sigma13*JNxW_[2];
      traction[1] = sigma12*JNxW_[0] + sigma22*JNxW_[1] + sigma23*JNxW_[2];
      traction[2] = sigma13*JNxW_[0] + sigma23*JNxW_[1] + sigma33*JNxW_[2];

      N();
      r_ += prod(boost::numeric::ublas::trans(N_),traction);
  }






  void P_history(int g)
  {
    P_history_gn_.clear();
    const vec tmp1 = P_dofs_solid_u_ - PP_dofs_solid_u_;
    const vec tmp2 = prod(B_,tmp1);

    vv_vec PP_history_tmp(nb_el_nodes_,v_vec(nb_Maxwell_elements_, vec (6,0.0)));
    int k(0);
    for(int i=0;i<nb_Maxwell_elements_;i++)
      for(int j=0;j<6;j++)
      {
        PP_history_tmp[g][i][j] = PP_history_[g*nb_Maxwell_elements_*6 + k];
        k++ ; 
      }


    vv_vec P_history_tmp(nb_el_nodes_,v_vec(nb_Maxwell_elements_, vec (6,0.0)));
    for(int i=0;i<nb_Maxwell_elements_;i++)
    {
      D(E_[i]);
      P_history_tmp[g][i] = PP_history_tmp[g][i]*exponential_[i] + prod(D_,tmp2)*A_[i];
    }


    k = 0;
    for(int i=0;i<nb_Maxwell_elements_;i++)
      for(int j=0;j<6;j++)
      {
	  P_history_gn_[g*nb_Maxwell_elements_*6 +k] = P_history_tmp[g][i][j];             
          k++;
      }

   }


      




  void D(double E)
  {
    D_.clear();

    const double mu = 0.5*E/(1.0 + nu_);    
    const double lambda = E*nu_/((1.0+nu_)*(1.0-2.0*nu_));
    
    D_(0,0) = lambda + 2.0*mu;
    D_(0,1) = lambda;
    D_(0,2) = lambda;
    D_(1,0) = lambda;
    D_(1,1) = lambda + 2.0*mu;
    D_(1,2) = lambda;
    D_(2,0) = lambda;
    D_(2,1) = lambda;
    D_(2,2) = lambda + 2.0*mu;        
    D_(3,3) = mu;
    D_(4,4) = mu;
    D_(5,5) = mu;
  }





   void N()
   {
    N_.clear();
    for(int i=0; i<nb_el_nodes_; i++)
    {
       N_(0,3*i) = quad_->sh(i);
       N_(1,3*i+1) = quad_->sh(i);
       N_(2,3*i+2) = quad_->sh(i);
    }   
   }



   
   void B()
   {
    B_.clear();
    for(int i=0; i<nb_el_nodes_; i++)
    {
       B_(0,3*i) = quad_->dsh_dx(i);
       B_(1,3*i+1) = quad_->dsh_dy(i);
       B_(2,3*i+2) = quad_->dsh_dz(i);          
       B_(3,3*i)   = quad_->dsh_dy(i);
       B_(3,3*i+1) = quad_->dsh_dx(i);
       B_(4,3*i+1) = quad_->dsh_dz(i);
       B_(4,3*i+2) = quad_->dsh_dy(i);     
       B_(5,3*i)   = quad_->dsh_dz(i);
       B_(5,3*i+2) = quad_->dsh_dx(i);
    }   
   }





   private:
   
    int nb_el_nodes_;

    ic_bc_type& ic_bc_;
    physical_properties& props;
    read_setup& setup_;

    double JxW_; 
    vec JNxW_;
    double dt_;
     
    int nb_Maxwell_elements_;
    std::vector<double> E_, eta_;
    vec relaxation_time_;
    vec exponential_;
    vec A_;

    double rho_;
    double nu_;
    double E_inf_;

    double a_damping_, b_damping_;

    mat N_;
    mat D_;      
    mat B_;
    vec P_history_;
    vec PP_history_;
    vec P_history_gn_;
    
    vec P_dofs_solid_u_;
    vec PP_dofs_solid_u_;
    
    vec r_;
    mat m_;

    vec gravity_;   

    point<3> dummy_;
    std::unique_ptr<quad<3>> quad_;
  }; 

 
}

#endif

