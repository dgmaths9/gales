  
  //-----------------fluid props------------------------------------------
  fluid_properties fluid_props;



  const int nb_comp = fluid_props.nb_comp();
  int nb_dofs_f = 2 + dim + nb_comp - 1;  
  if(setup.isothermal())   nb_dofs_f = 1+dim + nb_comp - 1;


  

  //------------------ mesh building --------------------------------------------------------------------
  print_only_pid<0>(std::cerr)<< "FLUID MESH\n";
  double mesh_read_start = MPI_Wtime();
  Mesh<dim> fluid_mesh(setup.fluid_mesh_file(), nb_dofs_f, setup);
  print_only_pid<0>(std::cerr)<<"Mesh reading took: "<<std::setprecision(setup.precision()) << MPI_Wtime()-mesh_read_start<<" s\n\n";



  
  //-----------------------------------------maps------------------------------------------
  epetra_maps fluid_maps(fluid_mesh);
  
  //----------------------------------------dofs state-----------------------------------------------------  
  auto fluid_dof_state = std::make_unique<dof_state>(fluid_maps.state_map()->NumMyElements(), 2);
  auto fluid_dot_dof_state = std::make_unique<dof_state>(fluid_maps.state_map()->NumMyElements(), 2);
  
  //---------------------------------------model---------------------------------------------------
  model<dim> fluid_model(fluid_mesh, *fluid_dof_state, fluid_maps, setup);  
  model<dim> fluid_dot_model(fluid_mesh, *fluid_dot_dof_state, fluid_maps, setup);  

  //----------------------------------------ic bc---------------------------------------------
  using fluid_ic_bc_type = fluid_ic_bc<dim>;  
  fluid_ic_bc_type fluid_ic_bc;

  //----------------------------------------dofs ic bc-----------------------------
  using fluid_dofs_ic_bc_type = fluid_dofs_ic_bc<fluid_ic_bc_type, dim>;
  fluid_dofs_ic_bc_type fluid_dofs_ic_bc(fluid_model, fluid_ic_bc, nb_comp, fluid_dot_model);
  
  //--------------pressure profile-----------------------------------------------------
  using pressure_profile_type = pressure_profile_mc<fluid_ic_bc_type, dim>;
  pressure_profile_type pressure_profile(fluid_ic_bc, fluid_model, fluid_props);

  if(fluid_ic_bc.custom_pressure_profile())
     fluid_ic_bc.set_custom_pressure_profile(fluid_model, fluid_props);
    
  
  //---------------------updater------------------------------------------------------
  dofs_updater fluid_updater(setup);

      
  //--------------------linear system-------------------------------------------------
  linear_system fluid_lp(fluid_model);
  
        
  //--------------------linear solver----------------------------------------------------
  belos_linear_solver fluid_ls_solver(setup); 


  //---------------------non linear residual check-------------------------------------------
  residual_check fluid_res_check;
  
  //------------------------integral---------------------------------------------------------  
  using fluid_integral_type = fluid<fluid_ic_bc_type, dim>;
  fluid_integral_type fluid_integral(fluid_ic_bc, fluid_props, fluid_model); 

  //-------------------------assembly--------------------------------------------------------
  fluid_assembly<fluid_integral_type, fluid_dofs_ic_bc_type, dim> fluid_assembly;


  // ------------------parallel I/O---------------------------------------------------  
  IO fluid_io(*fluid_maps.state_map(), *fluid_maps.dof_map());


  // ------------------dof trimmer---------------------------------------------------  
  dof_trimmer<dim> trimmer(fluid_model, fluid_dot_model);

