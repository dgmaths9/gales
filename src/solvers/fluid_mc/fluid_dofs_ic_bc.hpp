#ifndef _GALES_FLUID_DOFS_IC_BC_HPP_
#define _GALES_FLUID_DOFS_IC_BC_HPP_


#include"../../fem/fem.hpp"



namespace GALES {


  template<typename ic_bc_type, int dim>
  class fluid_dofs_ic_bc{};






  template<typename ic_bc_type>
  class fluid_dofs_ic_bc <ic_bc_type, 2>
  {
      using model_type = model<2>;
 
      public:
      
      fluid_dofs_ic_bc(model_type& model, ic_bc_type& ic_bc, int nb_comp, model_type& dot_model)
      :  
      model_(model), ic_bc_(ic_bc), nb_comp_(nb_comp), dot_model_(dot_model)
      {
        ic();
        const double alpha_m = 0.5*(3.0-model.setup().rho_inf())/(1.0+model.setup().rho_inf());
        const double alpha_f = 1.0/(1.0+model.setup().rho_inf());
        gamma_ = 0.5 + alpha_m - alpha_f;
      }



      void ic()
      {
        boost::numeric::ublas::vector<double> Y(nb_comp_-1,0.0);
        for(const auto& nd : model_.mesh().nodes())
        {
          model_.state().set_dof(nd->first_dof_lid(), ic_bc_.initial_p(*nd));
          model_.state().set_dof(nd->first_dof_lid()+1, ic_bc_.initial_vx(*nd));
          model_.state().set_dof(nd->first_dof_lid()+2, ic_bc_.initial_vy(*nd));
        }
              
        if(!model_.setup().isothermal())
        {  
          for(const auto& nd : model_.mesh().nodes())
          {
            model_.state().set_dof(nd->first_dof_lid()+3, ic_bc_.initial_T(*nd));
            ic_bc_.initial_Y(*nd,Y);
            for(int i=0; i<nb_comp_-1; i++)
              model_.state().set_dof(nd->first_dof_lid()+4+i, Y[i]);
          } 
        }
        else
        {
          for(const auto& nd : model_.mesh().nodes())
          {
            ic_bc_.initial_Y(*nd,Y);
            for(int i=0; i<nb_comp_-1; i++)
              model_.state().set_dof(nd->first_dof_lid()+3+i, Y[i]);
          }       
        }
      }
  




     void update_models(const node<2>& nd, int dof_indx, double x)
     {
        model_.state().set_dof(nd.first_dof_lid()+dof_indx, x);
        if(model_.setup().Gen_alpha())
        {             
          const auto n_1 = x;
          const auto n = model_.extract_node_dof(nd.lid(), dof_indx, 1);
          const auto dot_n = dot_model_.extract_node_dof(nd.lid(), dof_indx, 1);
          const auto dot_n_1 = (n_1 - n)/(gamma_*time::get().delta_t()) - (1.0-gamma_)/gamma_*dot_n;
          dot_model_.state().set_dof(nd.first_dof_lid()+dof_indx, dot_n_1);
        }
     }
     



  
  
      void dirichlet_bc()
      {
        std::pair<bool,double> result;

        for(const auto& nd : model_.mesh().nodes())
        {
          result = ic_bc_.dirichlet_p(*nd);
          if (result.first) update_models(*nd, 0, result.second);          
           
          result= ic_bc_.dirichlet_vx(*nd);
          if (result.first) update_models(*nd, 1, result.second);          

          result= ic_bc_.dirichlet_vy(*nd);
          if (result.first) update_models(*nd, 2, result.second);          
        }
        
        
        if(!model_.setup().isothermal())
        {  
          for(const auto& nd : model_.mesh().nodes())
          {
             result= ic_bc_.dirichlet_T(*nd);
             if (result.first) update_models(*nd, 3, result.second);
               
             for(int i=0; i<nb_comp_-1; i++)
             {
               result = ic_bc_.dirichlet_Y(*nd, i);
               if(result.first) update_models(*nd, 4+i, result.second);         
             }
          }
        }
        else
        {
          for(const auto& nd : model_.mesh().nodes())
          {
            for(int i=0; i<nb_comp_-1; i++)
            {
               result = ic_bc_.dirichlet_Y(*nd, i);
               if(result.first) update_models(*nd, 3+i, result.second);        
            }
          }        
        }         
      }




    auto dofs_constraints(const node<2>& nd)const  
    {
      std::vector<std::pair<bool,double>> nd_dofs_constraints(nd.nb_dofs(), std::make_pair(false, 0.0));              

      if(ic_bc_.dirichlet_p(nd).first) nd_dofs_constraints[0] = std::make_pair(true, 0.0);
      if(ic_bc_.dirichlet_vx(nd).first) nd_dofs_constraints[1] = std::make_pair(true, 0.0);
      if(ic_bc_.dirichlet_vy(nd).first) nd_dofs_constraints[2] = std::make_pair(true, 0.0);
      
      if(!model_.setup().isothermal())
      {
        if(ic_bc_.dirichlet_T(nd).first) nd_dofs_constraints[3] = std::make_pair(true, 0.0);
      
        for(int i=0; i<nb_comp_-1; i++)
          if(ic_bc_.dirichlet_Y(nd, i).first) nd_dofs_constraints[4+i] = std::make_pair(true, 0.0);      
      }
      else
      {
        for(int i=0; i<nb_comp_-1; i++)
          if(ic_bc_.dirichlet_Y(nd, i).first) nd_dofs_constraints[3+i] = std::make_pair(true, 0.0);            
      }
            
      return nd_dofs_constraints;
    }
 

    private:
    model_type& model_;
    ic_bc_type& ic_bc_;
    int nb_comp_;
    model_type& dot_model_;
    double gamma_;
  };











  template<typename ic_bc_type>
  class fluid_dofs_ic_bc <ic_bc_type, 3>
  {
      using model_type = model<3>;
      
      public:
      
      fluid_dofs_ic_bc(model_type& model, ic_bc_type& ic_bc, int nb_comp, model_type& dot_model)
      :  
      model_(model), ic_bc_(ic_bc), nb_comp_(nb_comp), dot_model_(dot_model)
      {
        ic();
        const double alpha_m = 0.5*(3.0-model.setup().rho_inf())/(1.0+model.setup().rho_inf());
        const double alpha_f = 1.0/(1.0+model.setup().rho_inf());
        gamma_ = 0.5 + alpha_m - alpha_f;
      }


      void ic()
      {
        boost::numeric::ublas::vector<double> Y(nb_comp_-1,0.0);
        for(const auto& nd : model_.mesh().nodes())
        {
          model_.state().set_dof(nd->first_dof_lid(), ic_bc_.initial_p(*nd));
          model_.state().set_dof(nd->first_dof_lid()+1, ic_bc_.initial_vx(*nd));
          model_.state().set_dof(nd->first_dof_lid()+2, ic_bc_.initial_vy(*nd));
          model_.state().set_dof(nd->first_dof_lid()+3, ic_bc_.initial_vz(*nd));
        }
         
        if(!model_.setup().isothermal())
        {  
          for(const auto& nd : model_.mesh().nodes())
          {
            model_.state().set_dof(nd->first_dof_lid()+4, ic_bc_.initial_T(*nd));
            ic_bc_.initial_Y(*nd,Y);
            for(int i=0; i<nb_comp_-1; i++)
              model_.state().set_dof(nd->first_dof_lid()+5+i, Y[i]);
          } 
        }
        else
        {
          for(const auto& nd : model_.mesh().nodes())
          {
            ic_bc_.initial_Y(*nd,Y);
            for(int i=0; i<nb_comp_-1; i++)
              model_.state().set_dof(nd->first_dof_lid()+4+i, Y[i]);
          }       
        }
      }

   



     void update_models(const node<3>& nd, int dof_indx, double x)
     {
        model_.state().set_dof(nd.first_dof_lid()+dof_indx, x);
        if(model_.setup().Gen_alpha())
        {             
          const auto n_1 = x;
          const auto n = model_.extract_node_dof(nd.lid(), dof_indx, 1);
          const auto dot_n = dot_model_.extract_node_dof(nd.lid(), dof_indx, 1);
          const auto dot_n_1 = (n_1 - n)/(gamma_*time::get().delta_t()) - (1.0-gamma_)/gamma_*dot_n;
          dot_model_.state().set_dof(nd.first_dof_lid()+dof_indx, dot_n_1);
        }
     }





      void dirichlet_bc()
      {
        std::pair<bool,double> result;

        for(const auto& nd : model_.mesh().nodes())
        {
          result = ic_bc_.dirichlet_p(*nd);
          if (result.first) update_models(*nd, 0, result.second);           
           
          result= ic_bc_.dirichlet_vx(*nd);
          if (result.first) update_models(*nd, 1, result.second);           

          result= ic_bc_.dirichlet_vy(*nd);
          if (result.first) update_models(*nd, 2, result.second);                   
        
          result= ic_bc_.dirichlet_vz(*nd);
          if (result.first) update_models(*nd, 3, result.second);           
        }

        if(!model_.setup().isothermal())
        {  
          for(const auto& nd : model_.mesh().nodes())
          {
             result= ic_bc_.dirichlet_T(*nd);
             if (result.first) update_models(*nd, 4, result.second); 
                
             for(int i=0; i<nb_comp_-1; i++)
             {
               result = ic_bc_.dirichlet_Y(*nd, i);
               if(result.first) update_models(*nd, 5+i, result.second);          
             }
          }
        }
        else
        {
          for(const auto& nd : model_.mesh().nodes())
          {
            for(int i=0; i<nb_comp_-1; i++)
            {
               result = ic_bc_.dirichlet_Y(*nd, i);
               if(result.first) update_models(*nd, 4+i, result.second);         
            }
          }        
        }         
      }








    auto dofs_constraints(const node<3> &nd)const  
    {
      std::vector<std::pair<bool,double>> nd_dofs_constraints(nd.nb_dofs(), std::make_pair(false, 0.0));              

      if(ic_bc_.dirichlet_p(nd).first) nd_dofs_constraints[0] = std::make_pair(true, 0.0);
      if(ic_bc_.dirichlet_vx(nd).first) nd_dofs_constraints[1] = std::make_pair(true, 0.0);
      if(ic_bc_.dirichlet_vy(nd).first) nd_dofs_constraints[2] = std::make_pair(true, 0.0);
      if(ic_bc_.dirichlet_vz(nd).first) nd_dofs_constraints[3] = std::make_pair(true, 0.0);

      if(!model_.setup().isothermal())
      {
        if(ic_bc_.dirichlet_T(nd).first) nd_dofs_constraints[4] = std::make_pair(true, 0.0);
      
        for(int i=0; i<nb_comp_-1; i++)
          if(ic_bc_.dirichlet_Y(nd, i).first) nd_dofs_constraints[5+i] = std::make_pair(true, 0.0);      
      }
      else
      {
        for(int i=0; i<nb_comp_-1; i++)
          if(ic_bc_.dirichlet_Y(nd, i).first) nd_dofs_constraints[4+i] = std::make_pair(true, 0.0);            
      }
            
      return nd_dofs_constraints;
    }




    private:
    model_type& model_;
    ic_bc_type& ic_bc_;
    int nb_comp_;
    model_type& dot_model_;
    double gamma_;
  };


} /* namespace GALES */

#endif

