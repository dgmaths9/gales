#include <mpi.h>
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <iterator>
#include <vector>


#include "../../fem/fem.hpp"
#include "fluid.hpp"



using namespace GALES;



template<int dim>
void solver(read_setup& setup)
{
  #include "constructors.hpp"
  secondary_dofs<dim> sec_dofs(*fluid_maps.shared_node_map(), fluid_props, setup);  






//  std::vector<std::string> files;
//  list_dir("results/fluid_dofs", files);

//  for(auto s: files)
//  {
//    auto t = stod(s);
//    double t_iteration(MPI_Wtime());    
//    print_only_pid<0>(std::cerr)<<"postprocessing at time = "<<t<<" sec.\n";
//   
//    time::get().t(t);
//    fluid_io.read("fluid_dofs/", *fluid_dof_state);     
//   
//    sec_dofs.execute(fluid_model, fluid_props);
//   
//    print_only_pid<0>(std::cerr)<<"Time taken for time step = "<<MPI_Wtime()-t_iteration<<"\n"<<"\n";
//  }






  for(auto t = setup.pp_start_time(); t <= setup.pp_final_time(); t += setup.pp_delta_t())
  {    
    double t_iteration(MPI_Wtime());    
    print_only_pid<0>(std::cerr)<<"postprocessing at time = "<<t<<" sec.\n";

    time::get().t(t);
    fluid_io.read("fluid_dofs/", *fluid_dof_state);     

    sec_dofs.execute(fluid_model, fluid_props);
   
    print_only_pid<0>(std::cerr)<<"Time taken for time step = "<<MPI_Wtime()-t_iteration<<"\n"<<"\n";    
  }
}






int main(int argc, char* argv[])
{

  MPI_Init(&argc, &argv);
      


  //---------------- read setup file -------------------------------
  read_setup setup;
    

  if(setup.dim()==2) solver<2>(setup);
  else if(setup.dim()==3) solver<3>(setup);
      
  
  MPI_Barrier(MPI_COMM_WORLD);
  MPI_Finalize();
  return 0;
}
