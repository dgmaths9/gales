#ifndef _FLUID_2D_HPP_
#define _FLUID_2D_HPP_



#include "../../fem/fem.hpp"




namespace GALES
{



  template<typename ic_bc_type, int dim>
  class fluid{};

    



  template<typename ic_bc_type>
  class fluid<ic_bc_type, 2>
  {
    using element_type = element<2>;
    using vec = boost::numeric::ublas::vector<double>;
    using mat = boost::numeric::ublas::matrix<double>;

    public :

    fluid(ic_bc_type& ic_bc, fluid_properties& props, model<2>& model)
    :
    nb_dof_fluid_(model.mesh().nb_nd_dofs()),
    nb_comp_(props.nb_comp()),

    ic_bc_(ic_bc),
    props_(props),
    setup_(model.setup()),

    JNxW_(2,0.0),
    JxW_(0.0),
    dt_(0.0),

    I_dot_(nb_dof_fluid_,0.0),
    P_(nb_dof_fluid_,0.0),
    I_(nb_dof_fluid_,0.0),
    dI_dx_(nb_dof_fluid_,0.0),
    dI_dy_(nb_dof_fluid_,0.0),

    mesh_v_(2,0.0),

    alpha_(0.0),
    beta_(0.0),
    rho_(0.0),
    mu_(0.0),
    cp_(0.0),
    cv_(0.0),
    kappa_(0.0),
    sound_speed_(0.0),
    qL_dp_(0.0), 
    qL_dT_(0.0),

    rho_comp_(nb_comp_,0.0),
    internal_energy_comp_(nb_comp_,0.0),
    chemical_diffusivity_comp_(nb_comp_,0.0),

    U_P_(nb_dof_fluid_,0.0),
    U_(nb_dof_fluid_,0.0),
    F1_adv_(nb_dof_fluid_,0.0),
    F2_adv_(nb_dof_fluid_,0.0),
    F1_dif_(nb_dof_fluid_,0.0),
    F2_dif_(nb_dof_fluid_,0.0),
    S_(nb_dof_fluid_,0.0),

    DUDY_(nb_dof_fluid_,nb_dof_fluid_,0.0),
    A1_(nb_dof_fluid_,nb_dof_fluid_,0.0),
    A2_(nb_dof_fluid_,nb_dof_fluid_,0.0),
    K11_(nb_dof_fluid_,nb_dof_fluid_,0.0),
    K12_(nb_dof_fluid_,nb_dof_fluid_,0.0),
    K21_(nb_dof_fluid_,nb_dof_fluid_,0.0),
    K22_(nb_dof_fluid_,nb_dof_fluid_,0.0),
    S0_(nb_dof_fluid_,nb_dof_fluid_,0.0),

    tau_(nb_dof_fluid_,nb_dof_fluid_,0.0),
    shock_matrix_(nb_dof_fluid_,nb_dof_fluid_,0.0),
    u_ref_inv_(nb_dof_fluid_, nb_dof_fluid_, 0.0),

    gravity_(2,0.0),

    tol_(std::numeric_limits<double>::epsilon())
    {
       ic_bc_.body_force(gravity_);
       if(setup_.dc_2006())  u_ref_inv();   

       alpha_m_ = 0.5*(3.0-setup_.rho_inf())/(1.0+setup_.rho_inf());
       alpha_f_ = 1.0/(1.0+setup_.rho_inf());
       gamma_ = 0.5 + alpha_m_ - alpha_f_;
    }





     void assign(const vec &v, double &p, double &vx, double &vy, double &T, vec &Y)
     {
        p =  v[0];        vx = v[1];        vy = v[2];        T =  v[3];

        for(int i=0; i<nb_comp_-1;i++)
          Y[i] = v[4+i];
        Y[nb_comp_-1] = 1.0 - std::accumulate(&Y[0], &Y[nb_comp_-1], 0.0);
     }



     void assign(const vec &v, double &p, double &vx, double &vy, vec &Y)
     {
        p =  v[0];        vx = v[1];        vy = v[2];       

        for(int i=0; i<nb_comp_-1;i++)
          Y[i] = v[3+i];
        Y[nb_comp_-1] = 1.0 - std::accumulate(&Y[0], &Y[nb_comp_-1], 0.0);
     }



     void u_ref_inv()
     {
         vec u_ref(nb_dof_fluid_,0.0);
         vec Y_ref(nb_comp_, 0.5);

         if(setup_.isothermal())
         {
            props_.properties(ic_bc_.p_ref(), Y_ref, 0.0);         
            U(ic_bc_.p_ref(), ic_bc_.v1_ref(), ic_bc_.v2_ref(), Y_ref, u_ref);
         }
         else
         {
            props_.properties(ic_bc_.p_ref(), ic_bc_.T_ref(), Y_ref, 0.0);         
            U(ic_bc_.p_ref(), ic_bc_.v1_ref(), ic_bc_.v2_ref(), ic_bc_.T_ref(), Y_ref, u_ref);
         }

         for(int i=0; i<nb_dof_fluid_; i++)
         {
             if(u_ref[i] < tol_)  u_ref_inv_(i,i) = 0.0;
             else u_ref_inv_(i,i) = 1.0/u_ref[i];
         }
     }
 
 
 


    // This is for Euler-backward method
     void get_dofs(const std::vector<vec>& dofs_fluid)
     {
         dt_= time::get().delta_t();
 
         P_orig_fluid_ = dofs_fluid[1];
         I_orig_fluid_ = dofs_fluid[0];
     }
 



    // This is for Euler-backward method with moving mesh
     void get_dofs_mm(const std::vector<vec>& dofs_elastostatic, const std::vector<vec>& dofs_fluid)
     {
         get_dofs(dofs_fluid);
         const vec I_orig_elastostatic = dofs_elastostatic[0];
 
         for(int i=0; i<nb_el_nodes_; i++)
         {
             I_ux_e_[i] = I_orig_elastostatic[2*i];
             I_uy_e_[i] = I_orig_elastostatic[2*i+1];
         }
     }
 
 




     // this is for generalized-alpha method
     void get_dofs(const std::vector<vec>& dofs_fluid, const std::vector<vec>& dofs_fluid_dot)
     {
         dt_= time::get().delta_t();
 
         P_orig_fluid_ = dofs_fluid[1];
         I_orig_fluid_ = dofs_fluid[1] + alpha_f_*(dofs_fluid[0]-dofs_fluid[1]); 
         I_orig_fluid_dot_ = dofs_fluid_dot[1] + alpha_m_*(dofs_fluid_dot[0]-dofs_fluid_dot[1]); 
     }







    // This is for generalized-alpha method with moving mesh
     void get_dofs_mm(const std::vector<vec>& dofs_elastostatic, const std::vector<vec>& dofs_fluid, const std::vector<vec>& dofs_fluid_dot)
     {
         get_dofs(dofs_fluid, dofs_fluid_dot);
         const vec I_orig_elastostatic = dofs_elastostatic[0];
 
         for(int i=0; i<nb_el_nodes_; i++)
         {
             I_ux_e_[i] = I_orig_elastostatic[2*i];
             I_uy_e_[i] = I_orig_elastostatic[2*i+1];
         }
     }




 
     void get_properties()
     {
         rho_ = props_.rho();
         mu_ = props_.mu();
         kappa_ = props_.kappa();
         sound_speed_ = props_.sound_speed();
         cv_ = props_.cv();
         cp_ = props_.cp();
         alpha_ = props_.alpha();
         beta_ = props_.beta();

         rho_comp_ = props_.rho_comp();
         chemical_diffusivity_comp_ = props_.chemical_diffusivity_comp();
         internal_energy_comp_ = props_.internal_energy_comp();
        
         if(setup_.print_props())
         {
            if(setup_.isothermal()) props_.print_props_mc_isothermal();
            else props_.print_props_mc();
         }   
     }
 




     void interpolation()
     {
          quad_ptr_->interpolate(I_orig_fluid_,I_);
          quad_ptr_->x_derivative_interpolate(I_orig_fluid_,dI_dx_);
          quad_ptr_->y_derivative_interpolate(I_orig_fluid_,dI_dy_);     
     }
 
 
 
 
 
    
     void U_P()
     {
              double p,vx,vy,T;
              vec Y(nb_comp_);
              quad_ptr_->interpolate(P_orig_fluid_,P_);
              assign(P_,p,vx,vy,T,Y);
              props_.properties(p, T, Y, 0.0);           
              U(p,vx,vy,T,Y, U_P_);     
     }
     



     void U_P_isothermal()
     {
              double p,vx,vy;
              vec Y(nb_comp_);
              quad_ptr_->interpolate(P_orig_fluid_,P_);
              assign(P_,p,vx,vy,Y);
              props_.properties(p,Y, 0.0);           
              U(p,vx,vy,Y, U_P_);     
     }
     


     
     //This is for element interior (isothermal case)
     void flux_matrices_and_vectors_isothermal()
     {
              double p,vx,vy;
              vec Y(nb_comp_);
              assign(I_,p,vx,vy,Y);
              const double shear_rate = compute_shear_rate(dI_dx_, dI_dy_);
              props_.properties(p,Y, shear_rate);           
              get_properties();
              flux_matrices_and_vectors(p,vx,vy,Y);
              tau_matrix(vx,vy);     
     }
 



     //This is for element interior
     void flux_matrices_and_vectors()
     {
              double p,vx,vy,T;
              vec Y(nb_comp_);
              assign(I_,p,vx,vy,T,Y);
              const double shear_rate = compute_shear_rate(dI_dx_, dI_dy_);
              props_.properties(p,T,Y, shear_rate);           
              get_properties();
              flux_matrices_and_vectors(p,vx,vy,T,Y);
              tau_matrix(vx,vy);     
     }
 




     //This is for boundary elements (isothermal case) 
     void flux_matrices_and_vectors_isothermal(const std::vector<int>& bd_nodes, int side_flag)
     {
              double p,vx,vy;
              vec Y(nb_comp_);
              assign(I_,p,vx,vy,Y);
              const double shear_rate = compute_shear_rate(dI_dx_, dI_dy_);
              props_.properties(p,Y, shear_rate);           
              get_properties();
              flux_matrices_and_vectors(p,vx,vy,Y,bd_nodes, side_flag);
     }





     //This is for boundary elements      
     void flux_matrices_and_vectors(const std::vector<int>& bd_nodes, int side_flag)
     {
              double p,vx,vy,T;
              vec Y(nb_comp_);
              assign(I_,p,vx,vy,T,Y);
              const double shear_rate = compute_shear_rate(dI_dx_, dI_dy_);
              props_.properties(p,T,Y, shear_rate);           
              get_properties();
              flux_matrices_and_vectors(p,vx,vy,T,Y,bd_nodes, side_flag);
     }
 

 
 

     void set_el_data_size()
     {
         I_orig_fluid_dot_.resize(nb_dof_fluid_*nb_el_nodes_, false);     
         P_orig_fluid_.resize(nb_dof_fluid_*nb_el_nodes_, false);
         I_orig_fluid_.resize(nb_dof_fluid_*nb_el_nodes_, false);

         I_ux_e_.resize(nb_el_nodes_, false);
         I_uy_e_.resize(nb_el_nodes_, false);

         r_.resize(nb_dof_fluid_*nb_el_nodes_, false);
         m_.resize(nb_dof_fluid_*nb_el_nodes_, nb_dof_fluid_*nb_el_nodes_, false);
     }
    




     // This is for Euler-backward method
     void execute(const element_type& el, const std::vector<vec>& dofs_fluid, mat& m, vec& r)
     {
         nb_el_nodes_ = el.nb_nodes();      
         set_el_data_size();

         get_dofs(dofs_fluid);
         r_.clear();
         m_.clear();

         for(const auto& quad_ptr : el.quad_i())
         {
           quad_ptr_ = quad_ptr;
           JxW_ = quad_ptr_->JxW();   
           interpolation();
           if(setup_.isothermal())
           {
             U_P_isothermal();
             flux_matrices_and_vectors_isothermal();   
           }
           else
           {
             U_P();
             flux_matrices_and_vectors();                            
           }  
           RM_i();
         }

         if(el.on_boundary())
         {
            for(int i=0; i<el.nb_sides(); i++)
            {
              if(el.is_side_on_boundary(i))
              {
                auto bd_nodes = el.side_nodes(i);
                auto side_flag = el.side_flag(i);
                
                for(const auto& quad_ptr: el.quad_b(i))
                {
                  quad_ptr_ = quad_ptr;               
                  JNxW_ = quad_ptr_->JNxW();      
                  interpolation();
                  if(setup_.isothermal()) flux_matrices_and_vectors_isothermal(bd_nodes, side_flag);
                  else flux_matrices_and_vectors(bd_nodes, side_flag);
                  RM_b();
                }
              }
            }            
         }
         m = m_;
         r = -r_;
     }
 
 
 






     // This is for Euler-backward method with moving mesh (ALE)
     void execute(const element_type& el, const std::vector<vec>& dofs_elastostatic, const std::vector<vec>& dofs_fluid, mat& m, vec& r)
     {
         nb_el_nodes_ = el.nb_nodes();      
         set_el_data_size();

         get_dofs_mm(dofs_elastostatic, dofs_fluid);
         r_.clear();
         m_.clear();

         for(int i=0; i<el.nb_gp(); i++)
         {
           quad_ptr_ = std::make_unique<quad>(el, el.gp(i), el.W_in(i));
           JxW_ = quad_ptr_->JxW();
           mesh_v_ = quad_ptr_->mesh_v(I_ux_e_, I_uy_e_, dt_);          
           interpolation();
           if(setup_.isothermal())
           {
             U_P_isothermal();
             flux_matrices_and_vectors_isothermal();   
           }
           else
           {
             U_P();
             flux_matrices_and_vectors();                            
           }  
           RM_i();
         }
      
         if(el.on_boundary())
         {
            for(int i=0; i<el.nb_sides(); i++)
            {
              if(el.is_side_on_boundary(i))
              {
                auto bd_nodes = el.side_nodes(i);
                auto side_flag = el.side_flag(i);
                
                for(int j=0; j<el.nb_side_gp(i); j++)
                {
                  quad_ptr_ = std::make_unique<quad>(el, el.side_gp(i,j), dummy_, el.W_bd(i,j));               
                  JNxW_ = quad_ptr_->JNxW();   
                  interpolation();
                  if(setup_.isothermal()) flux_matrices_and_vectors_isothermal(bd_nodes, side_flag);
                  else flux_matrices_and_vectors(bd_nodes, side_flag);
                  RM_b();
                }
              }
            }            
         }         
         m = m_;
         r = -r_;
     }








     // This is for Euler-backward method (ALE); element interior calculation 
     void RM_i()
     {
         const vec dU_dx = prod(DUDY_, dI_dx_);
         const vec dU_dy = prod(DUDY_, dI_dy_);
         const vec dU_dt = (U_ - U_P_)/dt_;          //(U_ - U_P_)/dt_;

     
         for (int b=0; b<nb_el_nodes_; b++)
         for (int jb=0; jb<nb_dof_fluid_; jb++)
         {
             r_[nb_dof_fluid_*b+jb] += quad_ptr_->sh(b)*dU_dt[jb]*JxW_;
             r_[nb_dof_fluid_*b+jb] += quad_ptr_->dsh_dx(b)*(F1_dif_[jb]-F1_adv_[jb])*JxW_;
             r_[nb_dof_fluid_*b+jb] += quad_ptr_->dsh_dy(b)*(F2_dif_[jb]-F2_adv_[jb])*JxW_;
             r_[nb_dof_fluid_*b+jb] -= quad_ptr_->sh(b)*S_[jb]*JxW_;
             r_[nb_dof_fluid_*b+jb] -= quad_ptr_->sh(b)*(mesh_v_[0]*dU_dx[jb] + mesh_v_[1]*dU_dy[jb])*JxW_;
         }

         for(int b=0; b<nb_el_nodes_; b++)
         for(int jb=0; jb<nb_dof_fluid_; jb++)
         for(int a=0; a<nb_el_nodes_; a++)
         for(int ja=0; ja<nb_dof_fluid_; ja++)
         {
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += quad_ptr_->sh(b)* DUDY_(jb,ja) * quad_ptr_->sh(a)/dt_*JxW_;
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) -= quad_ptr_->dsh_dx(b)* A1_(jb,ja) * quad_ptr_->sh(a)*JxW_;
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) -= quad_ptr_->dsh_dy(b)* A2_(jb,ja) * quad_ptr_->sh(a)*JxW_;
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += quad_ptr_->dsh_dx(b)* K11_(jb,ja) * quad_ptr_->dsh_dx(a)*JxW_;
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += quad_ptr_->dsh_dx(b)* K12_(jb,ja) * quad_ptr_->dsh_dy(a)*JxW_;
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += quad_ptr_->dsh_dy(b)* K21_(jb,ja) * quad_ptr_->dsh_dx(a)*JxW_;
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += quad_ptr_->dsh_dy(b)* K22_(jb,ja) * quad_ptr_->dsh_dy(a)*JxW_;
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) -= quad_ptr_->sh(b)* S0_(jb,ja) * quad_ptr_->sh(a)*JxW_;
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) -= quad_ptr_->sh(b)* DUDY_(jb,ja) * (mesh_v_[0]*quad_ptr_->dsh_dx(a) 
                                                                                          + mesh_v_[1]*quad_ptr_->dsh_dy(a))*JxW_;
         }

         //--------------------------------------------R_LeastSquares--------------------------------------------------------------------------        
         vec Res_vec = prod(A1_-mesh_v_[0]*DUDY_,dI_dx_) 
                     + prod(A2_-mesh_v_[1]*DUDY_,dI_dy_) 
                     - S_;
                           
//         if(!setup_.steady_state())   Res_vec += dU_dt;
                           
         for(int b=0; b<nb_el_nodes_; b++)
         {
             const mat first_part = (A1_-mesh_v_[0]*DUDY_)*quad_ptr_->dsh_dx(b) 
                                  + (A2_-mesh_v_[1]*DUDY_)*quad_ptr_->dsh_dy(b) 
                                  - S0_*quad_ptr_->sh(b);
             const mat first_part_tau = prod(first_part, tau_);
             const vec use = prod(first_part_tau, Res_vec);
             for(int jb=0; jb<nb_dof_fluid_; jb++)
             r_[nb_dof_fluid_*b+jb] += use[jb]*JxW_;
         }

         //--------------------------------------------M_LeastSquares--------------------------------------------------------------------------
         for(int a=0; a<nb_el_nodes_; a++)
         {
             mat Res_mat = (A1_-mesh_v_[0]*DUDY_)*quad_ptr_->dsh_dx(a) 
                         + (A2_-mesh_v_[1]*DUDY_)*quad_ptr_->dsh_dy(a) 
                         - S0_*quad_ptr_->sh(a);
 
//             if(!setup_.steady_state())     Res_mat += DUDY_*quad_ptr_->sh(a)/dt_;                         
                               
             for(int b=0; b<nb_el_nodes_; b++)
             {
                 const mat first_part = (A1_-mesh_v_[0]*DUDY_)*quad_ptr_->dsh_dx(b) 
                                      + (A2_-mesh_v_[1]*DUDY_)*quad_ptr_->dsh_dy(b) 
                                      - S0_*quad_ptr_->sh(b);
                 const mat first_part_tau = prod(first_part, tau_);
                 const mat use = prod(first_part_tau, Res_mat);
                 for(int jb=0; jb<nb_dof_fluid_; jb++)
                 for(int ja=0; ja<nb_dof_fluid_; ja++)
                 m_(nb_dof_fluid_*b+jb, nb_dof_fluid_*a+ja) += use(jb,ja)*JxW_;
             }
         }


         //-----------------dc------------------------
         dc(dU_dx, dU_dy);

         if(setup_.dc_2006())
         {
             const vec app_1 = prod(shock_matrix_,dI_dx_);
             const vec app_2 = prod(shock_matrix_,dI_dy_);

             for(int b=0;b<nb_el_nodes_;b++)
             for(int jb=0;jb<nb_dof_fluid_;jb++)
             {
                 r_[nb_dof_fluid_*b+jb] += quad_ptr_->dsh_dx(b)*app_1[jb]*JxW_;
                 r_[nb_dof_fluid_*b+jb] += quad_ptr_->dsh_dy(b)*app_2[jb]*JxW_;
             }

             for(int b=0;b<nb_el_nodes_;b++)
             for(int jb=0;jb<nb_dof_fluid_;jb++)
             for(int a=0;a<nb_el_nodes_;a++)
             for(int ja=0; ja<nb_dof_fluid_;ja++)
             {
                 m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += shock_matrix_(jb,ja)*quad_ptr_->dsh_dx(b)*quad_ptr_->dsh_dx(a)*JxW_;
                 m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += shock_matrix_(jb,ja)*quad_ptr_->dsh_dy(b)*quad_ptr_->dsh_dy(a)*JxW_;
             }
         }

     }







     // This is for Euler-backward method (ALE); bd element calculation
     void RM_b()
     {
         for(int b=0;b<nb_el_nodes_;b++)
         for(int j=0;j<nb_dof_fluid_;j++)
         {
             r_[nb_dof_fluid_*b+j] += quad_ptr_->sh(b)*(F1_adv_[j] - F1_dif_[j])*JNxW_[0];
             r_[nb_dof_fluid_*b+j] += quad_ptr_->sh(b)*(F2_adv_[j] - F2_dif_[j])*JNxW_[1];
         }

         for(int b=0; b<nb_el_nodes_; b++)
         for(int jb=0;jb<nb_dof_fluid_;jb++)
         for(int a=0; a<nb_el_nodes_; a++)
         for(int ja=0;ja<nb_dof_fluid_;ja++)
         {
            m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += quad_ptr_->sh(b)*A1_(jb,ja)*quad_ptr_->sh(a)*JNxW_[0]; 
            m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += quad_ptr_->sh(b)*A2_(jb,ja)*quad_ptr_->sh(a)*JNxW_[1]; 
            m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) -= quad_ptr_->sh(b)*K11_(jb,ja)*quad_ptr_->dsh_dx(a)*JNxW_[0]; 
            m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) -= quad_ptr_->sh(b)*K12_(jb,ja)*quad_ptr_->dsh_dy(a)*JNxW_[0]; 
            m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) -= quad_ptr_->sh(b)*K21_(jb,ja)*quad_ptr_->dsh_dx(a)*JNxW_[1]; 
            m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) -= quad_ptr_->sh(b)*K22_(jb,ja)*quad_ptr_->dsh_dy(a)*JNxW_[1]; 
         }
     }












    // This is for Generalized-alpha method
     void execute(const element_type& el, const std::vector<vec>& dofs_fluid, mat& m, vec& r, const std::vector<vec>& dofs_fluid_dot)
     {
         nb_el_nodes_ = el.nb_nodes();      
         set_el_data_size();

         get_dofs(dofs_fluid, dofs_fluid_dot);
         r_.clear();
         m_.clear();

         for(const auto& quad_ptr : el.quad_i())
         {
           quad_ptr_ = quad_ptr;
           JxW_ = quad_ptr_->JxW();   
           interpolation();
           quad_ptr_->interpolate(I_orig_fluid_dot_,I_dot_);
           if(setup_.isothermal())
           {
             U_P_isothermal();
             flux_matrices_and_vectors_isothermal();   
           }
           else
           {
             U_P();
             flux_matrices_and_vectors();                            
           }  
           RM_i_gen_alpha();
         }

         if(el.on_boundary())
         {
            for(int i=0; i<el.nb_sides(); i++)
            {
              if(el.is_side_on_boundary(i))
              {
                auto bd_nodes = el.side_nodes(i);
                auto side_flag = el.side_flag(i);
                
                for(const auto& quad_ptr: el.quad_b(i))
                {
                  quad_ptr_ = quad_ptr;               
                  JNxW_ = quad_ptr_->JNxW();      
                  interpolation();
                  if(setup_.isothermal()) flux_matrices_and_vectors_isothermal(bd_nodes, side_flag);
                  else flux_matrices_and_vectors(bd_nodes, side_flag);
                  RM_b_gen_alpha();
                }
              }
            }            
         }
         m = m_;
         r = -r_;
     }
 












     // This is for Generalized-alpha method with moving mesh (ALE)
     void execute(const element_type& el, const std::vector<vec>& dofs_elastostatic, const std::vector<vec>& dofs_fluid, mat& m, vec& r, const std::vector<vec>& dofs_fluid_dot)
     {
         nb_el_nodes_ = el.nb_nodes();      
         set_el_data_size();

         get_dofs_mm(dofs_elastostatic, dofs_fluid, dofs_fluid_dot);
         r_.clear();
         m_.clear();

         for(int i=0; i<el.nb_gp(); i++)
         {
           quad_ptr_ = std::make_unique<quad>(el, el.gp(i), alpha_f_, el.W_in(i));
           JxW_ = quad_ptr_->JxW();
           mesh_v_ = quad_ptr_->mesh_v(I_ux_e_, I_uy_e_, dt_);          
           interpolation();
           quad_ptr_->interpolate(I_orig_fluid_dot_,I_dot_);
           if(setup_.isothermal())
           {
             U_P_isothermal();
             flux_matrices_and_vectors_isothermal();   
           }
           else
           {
             U_P();
             flux_matrices_and_vectors();                            
           }  
           RM_i_gen_alpha();
         }
      
         if(el.on_boundary())
         {
            for(int i=0; i<el.nb_sides(); i++)
            {
              if(el.is_side_on_boundary(i))
              {
                auto bd_nodes = el.side_nodes(i);
                auto side_flag = el.side_flag(i);
                
                for(int j=0; j<el.nb_side_gp(i); j++)
                {
                  quad_ptr_ = std::make_unique<quad>(el, el.side_gp(i,j), dummy_, alpha_f_, el.W_bd(i,j));               
                  JNxW_ = quad_ptr_->JNxW();   
                  interpolation();
                  if(setup_.isothermal()) flux_matrices_and_vectors_isothermal(bd_nodes, side_flag);
                  else flux_matrices_and_vectors(bd_nodes, side_flag);
                  RM_b_gen_alpha();
                }
              }
            }            
         }         
         m = m_;
         r = -r_;
     }








    // This is for Generalized-alpha method (ALE); element interior calculation 
    void RM_i_gen_alpha()
    {
         const vec dU_dx = prod(DUDY_, dI_dx_);
         const vec dU_dy = prod(DUDY_, dI_dy_);
         const vec dU_dt = prod(DUDY_, I_dot_);
         const double coeff = alpha_f_* gamma_*dt_;
     
         for (int b=0; b<nb_el_nodes_; b++)
         for (int jb=0; jb<nb_dof_fluid_; jb++)
         {
             r_[nb_dof_fluid_*b+jb] += quad_ptr_->sh(b)*dU_dt[jb]*JxW_;
             r_[nb_dof_fluid_*b+jb] += quad_ptr_->dsh_dx(b)*(F1_dif_[jb]-F1_adv_[jb])*JxW_;
             r_[nb_dof_fluid_*b+jb] += quad_ptr_->dsh_dy(b)*(F2_dif_[jb]-F2_adv_[jb])*JxW_;
             r_[nb_dof_fluid_*b+jb] -= quad_ptr_->sh(b)*S_[jb]*JxW_;
             r_[nb_dof_fluid_*b+jb] -= quad_ptr_->sh(b)*(mesh_v_[0]*dU_dx[jb] + mesh_v_[1]*dU_dy[jb])*JxW_;
         }

         for(int b=0; b<nb_el_nodes_; b++)
         for(int jb=0; jb<nb_dof_fluid_; jb++)
         for(int a=0; a<nb_el_nodes_; a++)
         for(int ja=0; ja<nb_dof_fluid_; ja++)
         {
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += alpha_m_*quad_ptr_->sh(b)* DUDY_(jb,ja) * quad_ptr_->sh(a)*JxW_;
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) -= coeff*quad_ptr_->dsh_dx(b)* A1_(jb,ja) * quad_ptr_->sh(a)*JxW_;
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) -= coeff*quad_ptr_->dsh_dy(b)* A2_(jb,ja) * quad_ptr_->sh(a)*JxW_;
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += coeff*quad_ptr_->dsh_dx(b)* K11_(jb,ja) * quad_ptr_->dsh_dx(a)*JxW_;
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += coeff*quad_ptr_->dsh_dx(b)* K12_(jb,ja) * quad_ptr_->dsh_dy(a)*JxW_;
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += coeff*quad_ptr_->dsh_dy(b)* K21_(jb,ja) * quad_ptr_->dsh_dx(a)*JxW_;
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += coeff*quad_ptr_->dsh_dy(b)* K22_(jb,ja) * quad_ptr_->dsh_dy(a)*JxW_;
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) -= coeff*quad_ptr_->sh(b)* S0_(jb,ja) * quad_ptr_->sh(a)*JxW_;
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) -= coeff*quad_ptr_->sh(b)* DUDY_(jb,ja) * (mesh_v_[0]*quad_ptr_->dsh_dx(a) 
                                                                                                + mesh_v_[1]*quad_ptr_->dsh_dy(a))*JxW_;
         }

         //--------------------------------------------R_LeastSquares--------------------------------------------------------------------------        
         vec Res_vec = prod(A1_-mesh_v_[0]*DUDY_,dI_dx_) 
                     + prod(A2_-mesh_v_[1]*DUDY_,dI_dy_) 
                     - S_;
                           
//         if(!setup_.steady_state())   Res_vec += dU_dt;
                           
         for(int b=0; b<nb_el_nodes_; b++)
         {
             const mat first_part = (A1_-mesh_v_[0]*DUDY_)*quad_ptr_->dsh_dx(b) 
                                  + (A2_-mesh_v_[1]*DUDY_)*quad_ptr_->dsh_dy(b) 
                                  - S0_*quad_ptr_->sh(b);
             const mat first_part_tau = prod(first_part, tau_);
             const vec use = prod(first_part_tau, Res_vec);
             for(int jb=0; jb<nb_dof_fluid_; jb++)
             r_[nb_dof_fluid_*b+jb] += use[jb]*JxW_;
         }

         //--------------------------------------------M_LeastSquares--------------------------------------------------------------------------
         for(int a=0; a<nb_el_nodes_; a++)
         {
             mat Res_mat = coeff*(A1_-mesh_v_[0]*DUDY_)*quad_ptr_->dsh_dx(a) 
                         + coeff*(A2_-mesh_v_[1]*DUDY_)*quad_ptr_->dsh_dy(a) 
                         - coeff*S0_*quad_ptr_->sh(a);
 
//             if(!setup_.steady_state())     Res_mat += alpha_m_*DUDY_*quad_ptr_->sh(a);                         
                               
             for(int b=0; b<nb_el_nodes_; b++)
             {
                 const mat first_part = (A1_-mesh_v_[0]*DUDY_)*quad_ptr_->dsh_dx(b) 
                                      + (A2_-mesh_v_[1]*DUDY_)*quad_ptr_->dsh_dy(b) 
                                      - S0_*quad_ptr_->sh(b);
                 const mat first_part_tau = prod(first_part, tau_);
                 const mat use = prod(first_part_tau, Res_mat);
                 for(int jb=0; jb<nb_dof_fluid_; jb++)
                 for(int ja=0; ja<nb_dof_fluid_; ja++)
                 m_(nb_dof_fluid_*b+jb, nb_dof_fluid_*a+ja) += use(jb,ja)*JxW_;
             }
         }




         //-----------------dc------------------------
         dc(dU_dx, dU_dy);

         if(setup_.dc_2006())
         {
             const vec app_1 = prod(shock_matrix_,dI_dx_);
             const vec app_2 = prod(shock_matrix_,dI_dy_);

             for(int b=0;b<nb_el_nodes_;b++)
             for(int jb=0;jb<nb_dof_fluid_;jb++)
             {
                 r_[nb_dof_fluid_*b+jb] += quad_ptr_->dsh_dx(b)*app_1[jb]*JxW_;
                 r_[nb_dof_fluid_*b+jb] += quad_ptr_->dsh_dy(b)*app_2[jb]*JxW_;
             }

             for(int b=0;b<nb_el_nodes_;b++)
             for(int jb=0;jb<nb_dof_fluid_;jb++)
             for(int a=0;a<nb_el_nodes_;a++)
             for(int ja=0; ja<nb_dof_fluid_;ja++)
             {
                 m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += coeff*shock_matrix_(jb,ja)*quad_ptr_->dsh_dx(b)*quad_ptr_->dsh_dx(a)*JxW_;
                 m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += coeff*shock_matrix_(jb,ja)*quad_ptr_->dsh_dy(b)*quad_ptr_->dsh_dy(a)*JxW_;
             }
         }

     }








     // This is for Generalized-alpha method (ALE); bd element calculation
     void RM_b_gen_alpha()
     {
         const double coeff = alpha_f_* gamma_*dt_;

         for(int b=0;b<nb_el_nodes_;b++)
         for(int j=0;j<nb_dof_fluid_;j++)
         {
             r_[nb_dof_fluid_*b+j] += quad_ptr_->sh(b)*(F1_adv_[j] - F1_dif_[j])*JNxW_[0];
             r_[nb_dof_fluid_*b+j] += quad_ptr_->sh(b)*(F2_adv_[j] - F2_dif_[j])*JNxW_[1];
         }

         for(int b=0; b<nb_el_nodes_; b++)
         for(int jb=0;jb<nb_dof_fluid_;jb++)
         for(int a=0; a<nb_el_nodes_; a++)
         for(int ja=0;ja<nb_dof_fluid_;ja++)
         {
            m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += coeff*quad_ptr_->sh(b)*A1_(jb,ja)*quad_ptr_->sh(a)*JNxW_[0]; 
            m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += coeff*quad_ptr_->sh(b)*A2_(jb,ja)*quad_ptr_->sh(a)*JNxW_[1]; 
            m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) -= coeff*quad_ptr_->sh(b)*K11_(jb,ja)*quad_ptr_->dsh_dx(a)*JNxW_[0]; 
            m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) -= coeff*quad_ptr_->sh(b)*K12_(jb,ja)*quad_ptr_->dsh_dy(a)*JNxW_[0]; 
            m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) -= coeff*quad_ptr_->sh(b)*K21_(jb,ja)*quad_ptr_->dsh_dx(a)*JNxW_[1]; 
            m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) -= coeff*quad_ptr_->sh(b)*K22_(jb,ja)*quad_ptr_->dsh_dy(a)*JNxW_[1]; 
         }
     }











     //this is called to generate U_P_, U_ and U_ref_
     void U(double p,  double vx,  double vy, double T, const vec &Y, vec& u)
     {
         double rho =  props_.rho();
         double cv = props_.cv();
         double E = cv*T + 0.5*(vx*vx+vy*vy);

         u[0] = rho;
         u[1] = rho*vx;
         u[2] = rho*vy;
         u[3] = rho*E - qL_dp_*p - qL_dT_*T;

         for(int i=0; i<nb_comp_-1; i++)
           u[4+i] = rho*Y[i];
     }





     void flux_matrices_and_vectors(double p, double vx, double vy, double T, const vec &Y)
     {
         const double E = cv_*T + 0.5*(vx*vx + vy*vy);
         const double drho_dp = rho_*beta_;
         const double drho_dT = -rho_*alpha_;
         const double de_dp =  (beta_*p - alpha_*T)/rho_;
         const double de_dT =  cp_ - p*alpha_/rho_;
         
         vec drho_dY(nb_comp_-1,0.0), dE_dY(nb_comp_-1,0.0);
         for(int i=0; i<nb_comp_-1; i++)
         {
           drho_dY[i] = -rho_*rho_* (1./rho_comp_[i]-1/rho_comp_[nb_comp_-1]);
           dE_dY[i] = internal_energy_comp_[i]-internal_energy_comp_[nb_comp_-1];
         }  

         //------------------------------------------------------------------------
         DUDY_.clear();
         DUDY_(0,0) = drho_dp;
         DUDY_(0,3) = drho_dT;
         for(int c=0; c<nb_comp_-1; c++)
           DUDY_(0, 4+c) = drho_dY[c];            

         DUDY_(1,0) = drho_dp*vx;
         DUDY_(1,1) = rho_;
         DUDY_(1,3) = drho_dT*vx;
         for(int c=0; c<nb_comp_-1; c++)
           DUDY_(1, 4+c) = vx*drho_dY[c];

         DUDY_(2,0) = drho_dp*vy;
         DUDY_(2,2) = rho_;
         DUDY_(2,3) = drho_dT*vy;
         for(int c=0; c<nb_comp_-1; c++)
           DUDY_(2, 4+c) = vy*drho_dY[c];

         DUDY_(3,0) = drho_dp*E + rho_*de_dp - qL_dp_;
         DUDY_(3,1) = rho_*vx;
         DUDY_(3,2) = rho_*vy;
         DUDY_(3,3) = drho_dT*E + rho_*de_dT - qL_dT_;
         for(int c=0; c<nb_comp_-1; c++)
           DUDY_(3, 4+c) = E*drho_dY[c] + rho_*dE_dY[c];

         for(int i=0; i<nb_comp_-1; i++)
         {
           DUDY_(4+i, 0) = drho_dp*Y[i];
           DUDY_(4+i, 3) = drho_dT*Y[i];
           for(int c=0; c<nb_comp_-1; c++)
             DUDY_(4+i, 4+c) = Y[i]*drho_dY[c];
         }

         for(int i=0; i<nb_comp_-1; i++)
           DUDY_(4+i, 4+i) += rho_;


         //------------------------------------------------------------------------
         A1_.clear();
         A1_(0,0)  = drho_dp*vx;
         A1_(0,1) = rho_;
         A1_(0,3)  = drho_dT*vx;
         for(int c=0; c<nb_comp_-1; c++)
           A1_(0, 4+c) = drho_dY[c]*vx;

         A1_(1,0)  = drho_dp*vx*vx+1.0;
         A1_(1,1) = 2.0*rho_*vx;
         A1_(1,3)  = drho_dT*vx*vx;
         for(int c=0; c<nb_comp_-1; c++)
           A1_(1, 4+c) = drho_dY[c]*vx*vx;

         A1_(2,0)  = drho_dp*vy*vx;
         A1_(2,1) = rho_*vy;
         A1_(2,2) = rho_*vx;
         A1_(2,3)  = drho_dT*vy*vx;
         for(int c=0; c<nb_comp_-1; c++)
           A1_(2, 4+c) = drho_dY[c]*vy*vx;

         A1_(3,0)  = vx + vx*(drho_dp*E + rho_*de_dp);
         A1_(3,1) = p + rho_*E + rho_*vx*vx;
         A1_(3,2) = rho_*vx*vy;
         A1_(3,3)  = vx*(drho_dT*E + rho_*de_dT);
         for(int c=0; c<nb_comp_-1; c++)
           A1_(3, 4+c) = (E*drho_dY[c] + rho_*dE_dY[c])*vx;

         for(int i=0; i<nb_comp_-1; i++)
         {
           A1_(4+i, 0)  = drho_dp*Y[i]*vx;
           A1_(4+i, 1) = rho_*Y[i];
           A1_(4+i, 3)  = drho_dT*Y[i]*vx;
           for(int c=0; c<nb_comp_-1; c++)
             A1_(4+i, 4+c) = Y[i]*drho_dY[c]*vx;
         }

         for(int i=0; i<nb_comp_-1; i++)
           A1_(4+i, 4+i) += rho_*vx;



         //------------------------------------------------------------------------
         A2_.clear();
         A2_(0,0)  = drho_dp*vy;
         A2_(0,2) = rho_;
         A2_(0,3)  = drho_dT*vy;
         for(int c=0; c<nb_comp_-1; c++)
           A2_(0, 4+c) = drho_dY[c]*vy;

         A2_(1,0)  = drho_dp*vx*vy;
         A2_(1,1) = rho_*vy;
         A2_(1,2) = rho_*vx;
         A2_(1,3)  = drho_dT*vx*vy;
         for(int c=0; c<nb_comp_-1; c++)
           A2_(1, 4+c) = drho_dY[c]*vx*vy;

         A2_(2,0)  = drho_dp*vy*vy+1.0;
         A2_(2,2) = 2.0*rho_*vy;
         A2_(2,3)  = drho_dT*vy*vy;
         for(int c=0; c<nb_comp_-1; c++)
           A2_(2, 4+c) = drho_dY[c]*vy*vy;

         A2_(3,0)  = vy + vy*(drho_dp*E + rho_*de_dp);
         A2_(3,1) = rho_*vx*vy;
         A2_(3,2) = p + rho_*E + rho_*vy*vy;
         A2_(3,3)  = vy*(drho_dT*E + rho_*de_dT);
         for(int c=0; c<nb_comp_-1; c++)
           A2_(3, 4+c) = (E*drho_dY[c] + rho_*dE_dY[c])*vy;

         for(int i=0; i<nb_comp_-1; i++)
         {
           A2_(4+i, 0)  = drho_dp*Y[i]*vy;
           A2_(4+i, 2) = rho_*Y[i];
           A2_(4+i, 3)  = drho_dT*Y[i]*vy;
           for(int c=0; c<nb_comp_-1; c++)
             A2_(4+i, 4+c) = Y[i]*drho_dY[c]*vy;
         }

         for(int i=0; i<nb_comp_-1; i++)
           A2_(4+i, 4+i) += rho_*vy;
       
       
       


         //------------------------------------------------------------------------
         K11_.clear(); K12_.clear(); 
         K21_.clear(); K22_.clear(); 

         vec H(nb_comp_,0.0);
         for(int i=0; i<nb_comp_; i++)
           H[i] = internal_energy_comp_[i] + p/rho_comp_[i];

         const double lambda = -2.0*mu_/3.0;
         auto D = chemical_diffusivity_comp_;

         K11_(1,1) = lambda + 2*mu_;
         K11_(2,2) = mu_;
         K11_(3,1) = (lambda + 2*mu_)*vx;
         K11_(3,2) = mu_*vy;
         K11_(3,3) =  kappa_;
         for(int i=0; i<nb_comp_-1; i++)
         {
           K11_(4+i, 4+i) = rho_*D[i];
           K11_(3, 4+i) = rho_*D[i]*H[i];
         }  

         K12_(1,2) = lambda;
         K12_(2,1) =  mu_;
         K12_(3,1) = mu_*vy;
         K12_(3,2) = lambda*vx;

         K21_(1,2) = mu_;
         K21_(2,1) = lambda;
         K21_(3,1) = lambda*vy;
         K21_(3,2) = mu_*vx;

         K22_(1,1) = mu_;
         K22_(2,2) = lambda + 2*mu_;
         K22_(3,1) = mu_*vx;
         K22_(3,2) = (lambda + 2*mu_)*vy;
         K22_(3,3) =  kappa_;
         for(int i=0; i<nb_comp_-1; i++)
         {
           K22_(4+i, 4+i) = rho_*D[i];
           K22_(3, 4+i) = rho_*D[i]*H[i];
         }  

         S0_.clear();
         S0_(3,1) = rho_*gravity_[0];
         S0_(3,2) = rho_*gravity_[1];

         U(p,vx,vy,T,Y,U_);
         
                  

         F1_adv_[0] = rho_*vx;
         F1_adv_[1] = rho_*vx*vx + p;
         F1_adv_[2] = rho_*vx*vy;
         F1_adv_[3] = vx*(rho_*E+p);

         F2_adv_[0] = rho_*vy;
         F2_adv_[1] = rho_*vy*vx;
         F2_adv_[2] = rho_*vy*vy + p;
         F2_adv_[3] = vy*(rho_*E+p);

         for(int i=0; i<nb_comp_-1; i++)
         {
           F1_adv_[4+i] = rho_*vx*Y[i];
           F2_adv_[4+i] = rho_*vy*Y[i];
         }

         S_[1] = rho_*gravity_[0];
         S_[2] = rho_*gravity_[1];
         S_[3] = rho_*(gravity_[0]*vx + gravity_[1]*vy);

         F1_dif_ =  prod(K11_,dI_dx_) + prod(K12_,dI_dy_);
         F2_dif_ =  prod(K21_,dI_dx_) + prod(K22_,dI_dy_);
     }






     void flux_matrices_and_vectors(double p, double vx, double vy, double T, const vec &Y, const std::vector<int>& bd_nodes, int side_flag)
     {
         flux_matrices_and_vectors(p,vx,vy,T,Y);

         const double E = cv_*T + 0.5*(vx*vx + vy*vy);
         const double de_dp =  (beta_*p - alpha_*T)/rho_;
         const double de_dT =  cp_ - p*alpha_/rho_;

         vec dE_dY(nb_comp_-1,0.0);
         for(int i=0; i<nb_comp_-1; i++)
         {
           dE_dY[i] = internal_energy_comp_[i]-internal_energy_comp_[nb_comp_-1];
         }  

         if(ic_bc_.mass_flux1(bd_nodes, side_flag).first)
         {
            const double mass_flux1 = ic_bc_.mass_flux1(bd_nodes, side_flag).second;
         
            F1_adv_.clear();
            F1_adv_[0] = mass_flux1;
            F1_adv_[1] = mass_flux1*vx + p;
            F1_adv_[2] = mass_flux1*vy;
            F1_adv_[3] = mass_flux1*E + p*vx;         
            for(int i=0; i<nb_comp_-1; i++)
              F1_adv_[4+i] = mass_flux1*Y[i];

            A1_.clear();
            A1_(1,0)  = 1.0;
            A1_(1,1) = mass_flux1;
            A1_(2,2) = mass_flux1;
            A1_(3,0)  = vx + mass_flux1*de_dp;
            A1_(3,1) = p + mass_flux1*vx;
            A1_(3,2) = mass_flux1*vy;
            A1_(3,3)  = mass_flux1*de_dT;
            for(int c=0; c<nb_comp_-1; c++)
              A1_(3, 4+c) = mass_flux1*dE_dY[c];

            for(int i=0; i<nb_comp_-1; i++)
              A1_(4+i, 4+i) = mass_flux1;
         }    


         if(ic_bc_.mass_flux2(bd_nodes, side_flag).first)
         {
            const double mass_flux2 = ic_bc_.mass_flux2(bd_nodes, side_flag).second;
         
            F2_adv_.clear();
            F2_adv_[0] = mass_flux2;
            F2_adv_[1] = mass_flux2*vx;
            F2_adv_[2] = mass_flux2*vy + p;
            F2_adv_[3] = mass_flux2*E + p*vy;         
            for(int i=0; i<nb_comp_-1; i++)
              F2_adv_[4+i] = mass_flux2*Y[i];

            A2_.clear();
            A2_(1,1) = mass_flux2;
            A2_(2,0)  = 1.0;
            A2_(2,2) = mass_flux2;
            A2_(3,0)  = vy + mass_flux2*de_dp;
            A2_(3,1) = mass_flux2*vx;
            A2_(3,2) = p + mass_flux2*vy;
            A2_(3,3)  = mass_flux2*de_dT;
            for(int c=0; c<nb_comp_-1; c++)
              A2_(3, 4+c) = mass_flux2*dE_dY[c];

            for(int i=0; i<nb_comp_-1; i++)
              A2_(4+i, 4+i) = mass_flux2;
         }    


         const double vx_x(dI_dx_[1]), vx_y(dI_dy_[1]);
         const double vy_x(dI_dx_[2]), vy_y(dI_dy_[2]);
         const double T_x(dI_dx_[3]),  T_y(dI_dy_[3]);
         vec Y_x(nb_comp_-1,0.0), Y_y(nb_comp_-1,0.0);    
         for(int i=0; i<nb_comp_-1; i++)
         {
            Y_x[i] = dI_dx_[4+i];     Y_y[i] = dI_dy_[4+i]; 
         }

         const double lambda = -2.0*mu_/3.0;
         double tau11 = lambda*(vx_x + vy_y) + 2.0*mu_*vx_x;
         double tau22 = lambda*(vx_x + vy_y) + 2.0*mu_*vy_y;
         double tau12 = mu_*(vx_y + vy_x);
         double q1 = -kappa_*T_x;
         double q2 = -kappa_*T_y;
         vec J1(nb_comp_-1,0.0), J2(nb_comp_-1,0.0);
         auto d = chemical_diffusivity_comp_;
         for(int i=0; i<nb_comp_-1; i++)
         {
           J1[i] = rho_*d[i]*Y_x[i];    J2[i] = rho_*d[i]*Y_y[i];        
         }  
 

         if(ic_bc_.neumann_tau11(bd_nodes, side_flag).first)    tau11 = ic_bc_.neumann_tau11(bd_nodes, side_flag).second;
         if(ic_bc_.neumann_tau22(bd_nodes, side_flag).first)    tau22 = ic_bc_.neumann_tau22(bd_nodes, side_flag).second;
         if(ic_bc_.neumann_tau12(bd_nodes, side_flag).first)    tau12 = ic_bc_.neumann_tau12(bd_nodes, side_flag).second;
         if(ic_bc_.neumann_q1(bd_nodes, side_flag).first)       q1 = ic_bc_.neumann_q1(bd_nodes, side_flag).second;
         if(ic_bc_.neumann_q2(bd_nodes, side_flag).first)       q2 = ic_bc_.neumann_q2(bd_nodes, side_flag).second;
         
         for(int i=0; i<nb_comp_-1; i++)
         {
           if(ic_bc_.neumann_J1(i,bd_nodes, side_flag).first)   J1[i] = ic_bc_.neumann_J1(i,bd_nodes, side_flag).second;
           if(ic_bc_.neumann_J2(i,bd_nodes, side_flag).first)   J2[i] = ic_bc_.neumann_J2(i,bd_nodes, side_flag).second;
         }

         vec H(nb_comp_-1,0.0);
         for(int i=0; i<nb_comp_-1; i++)
           H[i] = internal_energy_comp_[i] + p/rho_comp_[i];

         double H_J1(0.0), H_J2(0.0);
         for(int i=0; i<nb_comp_-1; i++)
         {
           H_J1 += H[i]*J1[i];        H_J2 += H[i]*J2[i];    
         }

         F1_dif_[1] = tau11;
         F1_dif_[2] = tau12;
         F1_dif_[3] = tau11*vx + tau12*vy - q1 - H_J1;

         F2_dif_[1] = tau12;
         F2_dif_[2] = tau22;
         F2_dif_[3] = tau12*vx + tau22*vy - q2 - H_J2;

         for(int i=0; i<nb_comp_-1; i++)
         {
           F1_dif_[4+i] = J1[i];
           F2_dif_[4+i] = J2[i];
         }

         if(ic_bc_.neumann_tau11(bd_nodes, side_flag).first)
         {
           K11_(1, 1) = 0.0;
           K12_(1, 2) = 0.0;
           K11_(3, 1) = 0.0;
           K12_(3, 2) = 0.0;
         }

         if(ic_bc_.neumann_tau22(bd_nodes, side_flag).first)
         {
           K21_(2, 1) = 0.0;
           K22_(2, 2) = 0.0;
           K21_(3, 1) = 0.0;
           K22_(3, 2) = 0.0;
         }

         if(ic_bc_.neumann_tau12(bd_nodes, side_flag).first)
         {
           K11_(2, 2) = 0.0;
           K12_(2, 1) = 0.0;
           K21_(1, 2) = 0.0;
           K22_(1, 1) = 0.0;
           K11_(3, 2) = 0.0;
           K12_(3, 1) = 0.0;
           K21_(3, 2) = 0.0;
           K22_(3, 1) = 0.0;
         }

         if(ic_bc_.neumann_q1(bd_nodes, side_flag).first)
           K11_(3, 3) =  0.0;

         if(ic_bc_.neumann_q2(bd_nodes, side_flag).first)
           K22_(3, 3) =  0.0;      

         for(int i=0; i<nb_comp_-1; i++)
         {
            if(ic_bc_.neumann_J1(i, bd_nodes, side_flag).first)
            {         
              K11_(4+i, 4+i) = 0.0;
              K11_(3, 4+i) = 0.0;
            }

            if(ic_bc_.neumann_J2(i, bd_nodes, side_flag).first)
            {         
              K22_(4+i, 4+i) = 0.0;
              K22_(3, 4+i) = 0.0;
            }
         }
     }














     //this is called to generate U_P_, U_ and U_ref_
     void U(double p,  double vx,  double vy, const vec &Y, vec& u)
     {
         double rho =  props_.rho();

         u[0] = rho;
         u[1] = rho*vx;
         u[2] = rho*vy;

         for(int i=0; i<nb_comp_-1; i++)
           u[3+i] = rho*Y[i];
     }





     void flux_matrices_and_vectors(double p, double vx, double vy, const vec &Y)
     {
         const double drho_dp = rho_*beta_;
         
         vec drho_dY(nb_comp_-1,0.0);
         for(int i=0; i<nb_comp_-1; i++)
         {
           drho_dY[i] = -rho_*rho_* (1./rho_comp_[i]-1/rho_comp_[nb_comp_-1]);
         }  

         //------------------------------------------------------------------------
         DUDY_.clear();
         DUDY_(0,0) = drho_dp;
         for(int c=0; c<nb_comp_-1; c++)
           DUDY_(0, 3+c) = drho_dY[c];            

         DUDY_(1,0) = drho_dp*vx;
         DUDY_(1,1) = rho_;
         for(int c=0; c<nb_comp_-1; c++)
           DUDY_(1, 3+c) = vx*drho_dY[c];

         DUDY_(2,0) = drho_dp*vy;
         DUDY_(2,2) = rho_;
         for(int c=0; c<nb_comp_-1; c++)
           DUDY_(2, 3+c) = vy*drho_dY[c];

         for(int i=0; i<nb_comp_-1; i++)
         {
           DUDY_(3+i, 0) = drho_dp*Y[i];
           for(int c=0; c<nb_comp_-1; c++)
             DUDY_(3+i, 3+c) = Y[i]*drho_dY[c];
         }

         for(int i=0; i<nb_comp_-1; i++)
           DUDY_(3+i, 3+i) += rho_;


         //------------------------------------------------------------------------
         A1_.clear();
         A1_(0,0)  = drho_dp*vx;
         A1_(0,1) = rho_;
         for(int c=0; c<nb_comp_-1; c++)
           A1_(0, 3+c) = drho_dY[c]*vx;

         A1_(1,0)  = drho_dp*vx*vx+1.0;
         A1_(1,1) = 2.0*rho_*vx;
         for(int c=0; c<nb_comp_-1; c++)
           A1_(1, 3+c) = drho_dY[c]*vx*vx;

         A1_(2,0)  = drho_dp*vy*vx;
         A1_(2,1) = rho_*vy;
         A1_(2,2) = rho_*vx;
         for(int c=0; c<nb_comp_-1; c++)
           A1_(2, 3+c) = drho_dY[c]*vy*vx;

         for(int i=0; i<nb_comp_-1; i++)
         {
           A1_(3+i, 0)  = drho_dp*Y[i]*vx;
           A1_(3+i, 1) = rho_*Y[i];
           for(int c=0; c<nb_comp_-1; c++)
             A1_(3+i, 3+c) = Y[i]*drho_dY[c]*vx;
         }

         for(int i=0; i<nb_comp_-1; i++)
           A1_(3+i, 3+i) += rho_*vx;



         //------------------------------------------------------------------------
         A2_.clear();
         A2_(0,0)  = drho_dp*vy;
         A2_(0,2) = rho_;
         for(int c=0; c<nb_comp_-1; c++)
           A2_(0, 3+c) = drho_dY[c]*vy;

         A2_(1,0)  = drho_dp*vx*vy;
         A2_(1,1) = rho_*vy;
         A2_(1,2) = rho_*vx;
         for(int c=0; c<nb_comp_-1; c++)
           A2_(1, 3+c) = drho_dY[c]*vx*vy;

         A2_(2,0)  = drho_dp*vy*vy+1.0;
         A2_(2,2) = 2.0*rho_*vy;
         for(int c=0; c<nb_comp_-1; c++)
           A2_(2, 3+c) = drho_dY[c]*vy*vy;

         for(int i=0; i<nb_comp_-1; i++)
         {
           A2_(3+i, 0)  = drho_dp*Y[i]*vy;
           A2_(3+i, 2) = rho_*Y[i];
           for(int c=0; c<nb_comp_-1; c++)
             A2_(3+i, 3+c) = Y[i]*drho_dY[c]*vy;
         }

         for(int i=0; i<nb_comp_-1; i++)
           A2_(3+i, 3+i) += rho_*vy;
       
       
       


         //------------------------------------------------------------------------
         K11_.clear(); K12_.clear(); 
         K21_.clear(); K22_.clear(); 

         vec H(nb_comp_,0.0);
         for(int i=0; i<nb_comp_; i++)
           H[i] = internal_energy_comp_[i] + p/rho_comp_[i];

         const double lambda = -2.0*mu_/3.0;
         auto D = chemical_diffusivity_comp_;

         K11_(1,1) = lambda + 2*mu_;
         K11_(2,2) = mu_;
         for(int i=0; i<nb_comp_-1; i++)
         {
           K11_(3+i, 3+i) = rho_*D[i];
         }  

         K12_(1,2) = lambda;
         K12_(2,1) =  mu_;

         K21_(1,2) = mu_;
         K21_(2,1) = lambda;

         K22_(1,1) = mu_;
         K22_(2,2) = lambda + 2*mu_;         
         for(int i=0; i<nb_comp_-1; i++)
         {
           K22_(3+i, 3+i) = rho_*D[i];
         }  

         S0_.clear();

         U(p,vx,vy,Y,U_);
         
                  

         F1_adv_[0] = rho_*vx;
         F1_adv_[1] = rho_*vx*vx + p;
         F1_adv_[2] = rho_*vx*vy;

         F2_adv_[0] = rho_*vy;
         F2_adv_[1] = rho_*vy*vx;
         F2_adv_[2] = rho_*vy*vy + p;

         for(int i=0; i<nb_comp_-1; i++)
         {
           F1_adv_[3+i] = rho_*vx*Y[i];
           F2_adv_[3+i] = rho_*vy*Y[i];
         }

         S_[1] = rho_*gravity_[0];
         S_[2] = rho_*gravity_[1];

         F1_dif_ =  prod(K11_,dI_dx_) + prod(K12_,dI_dy_);
         F2_dif_ =  prod(K21_,dI_dx_) + prod(K22_,dI_dy_);
     }






     void flux_matrices_and_vectors(double p, double vx, double vy, const vec &Y, const std::vector<int>& bd_nodes, int side_flag)
     {
         flux_matrices_and_vectors(p,vx,vy,Y);


         if(ic_bc_.mass_flux1(bd_nodes, side_flag).first)
         {
            const double mass_flux1 = ic_bc_.mass_flux1(bd_nodes, side_flag).second;
         
            F1_adv_.clear();
            F1_adv_[0] = mass_flux1;
            F1_adv_[1] = mass_flux1*vx + p;
            F1_adv_[2] = mass_flux1*vy;
            for(int i=0; i<nb_comp_-1; i++)
              F1_adv_[3+i] = mass_flux1*Y[i];

            A1_.clear();
            A1_(1,0)  = 1.0;
            A1_(1,1) = mass_flux1;
            A1_(2,2) = mass_flux1;

            for(int i=0; i<nb_comp_-1; i++)
              A1_(3+i, 3+i) = mass_flux1;
         }    


         if(ic_bc_.mass_flux2(bd_nodes, side_flag).first)
         {
            const double mass_flux2 = ic_bc_.mass_flux2(bd_nodes, side_flag).second;
         
            F2_adv_.clear();
            F2_adv_[0] = mass_flux2;
            F2_adv_[1] = mass_flux2*vx;
            F2_adv_[2] = mass_flux2*vy + p;
            for(int i=0; i<nb_comp_-1; i++)
              F2_adv_[3+i] = mass_flux2*Y[i];

            A2_.clear();
            A2_(1,1) = mass_flux2;
            A2_(2,0)  = 1.0;
            A2_(2,2) = mass_flux2;

            for(int i=0; i<nb_comp_-1; i++)
              A2_(3+i, 3+i) = mass_flux2;
         }    


         const double vx_x(dI_dx_[1]), vx_y(dI_dy_[1]);
         const double vy_x(dI_dx_[2]), vy_y(dI_dy_[2]);
         vec Y_x(nb_comp_-1,0.0), Y_y(nb_comp_-1,0.0);    
         for(int i=0; i<nb_comp_-1; i++)
         {
            Y_x[i] = dI_dx_[3+i];     Y_y[i] = dI_dy_[3+i]; 
         }

         const double lambda = -2.0*mu_/3.0;
         double tau11 = lambda*(vx_x + vy_y) + 2.0*mu_*vx_x;
         double tau22 = lambda*(vx_x + vy_y) + 2.0*mu_*vy_y;
         double tau12 = mu_*(vx_y + vy_x);
         vec J1(nb_comp_-1,0.0), J2(nb_comp_-1,0.0);
         auto d = chemical_diffusivity_comp_;
         for(int i=0; i<nb_comp_-1; i++)
         {
           J1[i] = rho_*d[i]*Y_x[i];    J2[i] = rho_*d[i]*Y_y[i];        
         }  
 

         if(ic_bc_.neumann_tau11(bd_nodes, side_flag).first)   tau11 = ic_bc_.neumann_tau11(bd_nodes, side_flag).second;
         if(ic_bc_.neumann_tau22(bd_nodes, side_flag).first)   tau22 = ic_bc_.neumann_tau22(bd_nodes, side_flag).second;
         if(ic_bc_.neumann_tau12(bd_nodes, side_flag).first)   tau12 = ic_bc_.neumann_tau12(bd_nodes, side_flag).second;
         
         for(int i=0; i<nb_comp_-1; i++)
         {
           if(ic_bc_.neumann_J1(i,bd_nodes, side_flag).first)  J1[i] = ic_bc_.neumann_J1(i,bd_nodes, side_flag).second;
           if(ic_bc_.neumann_J2(i,bd_nodes, side_flag).first)  J2[i] = ic_bc_.neumann_J2(i,bd_nodes, side_flag).second;
         }

         F1_dif_[1] = tau11;
         F1_dif_[2] = tau12;

         F2_dif_[1] = tau12;
         F2_dif_[2] = tau22;

         for(int i=0; i<nb_comp_-1; i++)
         {
           F1_dif_[3+i] = J1[i];
           F2_dif_[3+i] = J2[i];
         }

         if(ic_bc_.neumann_tau11(bd_nodes, side_flag).first)
         {
           K11_(1, 1) = 0.0;
           K12_(1, 2) = 0.0;
         }

         if(ic_bc_.neumann_tau22(bd_nodes, side_flag).first)
         {
           K21_(2, 1) = 0.0;
           K22_(2, 2) = 0.0;
         }

         if(ic_bc_.neumann_tau12(bd_nodes, side_flag).first)
         {
           K11_(2, 2) = 0.0;
           K12_(2, 1) = 0.0;
           K21_(1, 2) = 0.0;
           K22_(1, 1) = 0.0;
         }

         for(int i=0; i<nb_comp_-1; i++)
         {
            if(ic_bc_.neumann_J1(i, bd_nodes, side_flag).first)
            {         
              K11_(3+i, 3+i) = 0.0;
            }

            if(ic_bc_.neumann_J2(i, bd_nodes, side_flag).first)
            {         
              K22_(3+i, 3+i) = 0.0;
            }
         }
     }









     void tau_matrix(double vx,  double vy)
     {
         tau_.clear();

         const double v_nrm = sqrt(vx*vx + vy*vy);
         if(v_nrm < tol_) return;

         const double c(sound_speed_);
         vec v(2,0.0);
         v[0] = vx;    v[1] = vy;  

         mat del_v(2,2,0.0);
         for(int i=0; i<2; i++)
         {
             del_v(i,0) = dI_dx_[1+i];       del_v(i,1) = dI_dy_[1+i];     
         }
         auto alpha = quad_ptr_->alpha(v, del_v, dummy_);
         auto h = quad_ptr_->compute_h(alpha, dummy_);

         if(setup_.tau_non_diag_comp_2001())
         {
             double lambda_sqr = alpha[0]*(v_nrm*v_nrm + c*c) + alpha[1]*0.5*c*c
                        + 0.5*c*sqrt(c*c*alpha[1]*alpha[1]+ 16*alpha[0]*alpha[0]*v_nrm*v_nrm);

             if(!setup_.steady_state()) lambda_sqr += 4/(dt_*dt_);
             const double lambda = 1.0/sqrt(lambda_sqr) + h/(2*v_nrm);

             double use(0.0);
             if(setup_.steady_state())  use = lambda;
             else  use = std::min(dt_*0.5, lambda);

             tau_(0, 0) = use;
             tau_(1, 1) = std::min(use, rho_*h*h/(mu_*12));
             tau_(2, 2) = tau_(1, 1);

             if(!setup_.isothermal()) 
             {
               tau_(3, 3) = std::min(use, rho_*h*h*cv_/(kappa_*12));

               for(int i=0; i<nb_comp_-1; i++)
                 tau_(4+i, 4+i) = std::min(use, h*h/(chemical_diffusivity_comp_[i]*12));
             }  
             else
             {
               for(int i=0; i<nb_comp_-1; i++)
                 tau_(3+i, 3+i) = std::min(use, h*h/(chemical_diffusivity_comp_[i]*12));             
             }

             const mat DYDU = inverse(DUDY_);
             tau_= prod(DYDU,tau_);

             // tau correction for weak compressibility (if M < 0.1)
             if(setup_.incompressibility_correction() && v_nrm/c < 0.1)
             {
               const auto G = quad_ptr_->G();
               const auto tr_G = quad_ptr_->trace_G();
               tau_(0, 0) = 1.0/( 1.0/tau_(0,0) + rho_*tau_(1,1)*tr_G ); 
               tau_(0, 1) = 0.0;
               tau_(0, 2) = 0.0;
               if(!setup_.isothermal()) 
               { 
                 tau_(0, 3) = 0.0;
                 for(int i=0; i<nb_comp_-1; i++)
                   tau_(0, 4+i) = 0.0;
               }
               else
               {
                 for(int i=0; i<nb_comp_-1; i++)
                   tau_(0, 3+i) = 0.0;               
               }    
             }
         }


         else if(setup_.tau_non_diag_comp_2019())
         {
             double use(0.0);
             if(setup_.steady_state())  use = h/(2.0*(v_nrm + c)) + h/(2.0*v_nrm);
             else  use = std::min(dt_*0.5, h/(2.0*(v_nrm + c)) + h/(2.0*v_nrm));

             tau_(0, 0) = use;
             tau_(1, 1) = std::min(use, h*h*rho_/(12.0*mu_));
             tau_(2, 2) = tau_(1, 1);

             if(!setup_.isothermal()) 
             {
               tau_(3, 3) = std::min(use, rho_*h*h*cv_/(kappa_*12));

               for(int i=0; i<nb_comp_-1; i++)
                 tau_(4+i, 4+i) = std::min(use, h*h/(chemical_diffusivity_comp_[i]*12));
             }  
             else
             {
               for(int i=0; i<nb_comp_-1; i++)
                 tau_(3+i, 3+i) = std::min(use, h*h/(chemical_diffusivity_comp_[i]*12));             
             }

             const mat DYDU = inverse(DUDY_);
             tau_= prod(DYDU,tau_);            

             // tau correction for weak compressibility (if M < 0.1)
             if(setup_.incompressibility_correction() && v_nrm/c < 0.1)
             {
               const auto G = quad_ptr_->G();
               const auto tr_G = quad_ptr_->trace_G();
               tau_(0, 0) = 1.0/( 1.0/tau_(0,0) + rho_*tau_(1,1)*tr_G ); 
               tau_(0, 1) = 0.0;
               tau_(0, 2) = 0.0;
               if(!setup_.isothermal()) 
               { 
                 tau_(0, 3) = 0.0;
                 for(int i=0; i<nb_comp_-1; i++)
                   tau_(0, 4+i) = 0.0;
               }
               else
               {
                 for(int i=0; i<nb_comp_-1; i++)
                   tau_(0, 3+i) = 0.0;               
               }    
             }
         }


         else if(setup_.tau_diag_incomp_2007())
         {
          const auto G = quad_ptr_->G();
          double use(0.0);
          for(int i=0; i<2; i++)
           for(int j=0; j<2; j++)
            use += G(i,j)*G(i,j);
           
          double tau_m = boost::numeric::ublas::inner_prod(v, prod(G, v)) + 9.0*use*mu_*mu_/(rho_*rho_);
          if(!setup_.steady_state()) tau_m += 4.0/(dt_*dt_);
          tau_m = 1.0/sqrt(tau_m); 

          const auto g = quad_ptr_->g();
          const double tau_c = 1.0/(rho_*tau_m*boost::numeric::ublas::inner_prod(g,g));
          
          tau_(0, 0) = tau_c;
          tau_(1, 1) = tau_m/rho_;
          tau_(2, 2) = tau_m/rho_;
          if(!setup_.isothermal())
          {
            double tau_e = boost::numeric::ublas::inner_prod(v, prod(G, v)) + 9.0*use*kappa_*kappa_/(rho_*cv_*rho_*cv_);
            if(!setup_.steady_state()) tau_e += 4.0/(dt_*dt_);
            tau_e = 1.0/sqrt(tau_e); 

            tau_(3, 3) = tau_e/(rho_*cv_);

            for(int i=0; i<nb_comp_-1; i++)
              tau_(4+i, 4+i) = tau_m;
          }
          else
          {
            for(int i=0; i<nb_comp_-1; i++)
              tau_(3+i, 3+i) = tau_m;          
          }    
         }


         else if(setup_.tau_diag_2014())
         {
            auto A0 = diag_mat_of_mat(DUDY_);
            auto A1 = diag_mat_of_mat(A1_);
            auto A2 = diag_mat_of_mat(A2_);
            auto K11 = diag_mat_of_mat(K11_);
            auto K12 = diag_mat_of_mat(K12_);
            auto K21 = diag_mat_of_mat(K21_);
            auto K22 = diag_mat_of_mat(K22_);
                        
            const auto G = quad_ptr_->G();
            mat tau_t_inv_sqr = 4.0/(dt_*dt_)*prod(A0,A0); 
            mat tau_a_inv_sqr = G(0,0)*prod(A1,A1) + G(0,1)*prod(A1,A2) 
                              + G(1,0)*prod(A2,A1) + G(1,1)*prod(A2,A2);
            mat tau_d_inv_sqr = G(0,0)*G(0,0)*prod(K11,K11) + G(0,1)*G(0,1)*prod(K12,K12) 
                              + G(1,0)*G(1,0)*prod(K21,K21) + G(1,1)*G(1,1)*prod(K22,K22);
            
            vec tau_t_inv(nb_dof_fluid_, 0.0);
            vec tau_a_inv(nb_dof_fluid_, 0.0); 
            vec tau_d_inv(nb_dof_fluid_, 0.0);
            for(int i=0;i<nb_dof_fluid_;i++)
            {
              tau_t_inv[i] = sqrt(tau_t_inv_sqr(i,i));
              tau_a_inv[i] = sqrt(tau_a_inv_sqr(i,i));
              tau_d_inv[i] = sqrt(tau_d_inv_sqr(i,i));
            }
                              
            for(int i=0;i<nb_dof_fluid_;i++)
              if(tau_t_inv[i] + tau_a_inv[i] + tau_d_inv[i] > tol_) 
                tau_(i,i) = 1.0/(tau_t_inv[i] + tau_a_inv[i] + tau_d_inv[i] + 1.e-8);

            if(setup_.incompressibility_correction() && v_nrm/c < 0.1)
            {
              const auto tr_G = quad_ptr_->trace_G();
              tau_(0, 0) = 1.0/( 1.0/tau_(0,0) + rho_*tau_(1,1)*tr_G ); 
            }  
         }
     }




     void dc(const vec& dU_dx, const vec& dU_dy)
     {
         if (setup_.dc_2006())
         {
             shock_matrix_.clear();
             const double del_rho_nrm = sqrt(dU_dx[0]*dU_dx[0] + dU_dy[0]*dU_dy[0]);
             if(del_rho_nrm < tol_) return;
             vec J(2,0.0);
             J[0] = dU_dx[0]/del_rho_nrm;
             J[1] = dU_dy[0]/del_rho_nrm;
             double h(0.0);
             for(int i=0; i<nb_el_nodes_; i++)
             {
                 h += abs(J[0]*quad_ptr_->dsh_dx(i) + J[1]*quad_ptr_->dsh_dy(i) );
             }
             if(h > tol_) h = 1.0/h;
             else return;

             vec Res = prod(A1_,dI_dx_) + prod(A2_,dI_dy_) - S_;
             const double u_ref_inv_Res_nrm = norm_2(prod(u_ref_inv_,Res));
             const double u_ref_inv_U_nrm = norm_2(prod(u_ref_inv_,U_));
             const double a = norm_2(prod(u_ref_inv_, dU_dx));
             const double b = norm_2(prod(u_ref_inv_, dU_dy));

             const double s = setup_.dc_sharp();
             double nu_shoc(0.0);
             if(s == 1.0)
             {
               if(a*a+b*b < tol_) return;
               nu_shoc = u_ref_inv_Res_nrm*h*1.0/sqrt(a*a+b*b);
             }  
             else if(s == 2.0) 
             {
               if(u_ref_inv_U_nrm < tol_) return;
               nu_shoc = u_ref_inv_Res_nrm*h*h/u_ref_inv_U_nrm;
             }  
             else if(s == 1.5) 
             {
               if(a*a+b*b < tol_) nu_shoc = u_ref_inv_Res_nrm*h*h/u_ref_inv_U_nrm;
               else nu_shoc = u_ref_inv_Res_nrm*h*(1.0/sqrt(a*a+b*b) + h/u_ref_inv_U_nrm)/2.0;
             }

             mat K(nb_dof_fluid_,nb_dof_fluid_,0.0);
             for(int i=0; i<nb_dof_fluid_; i++)
             K(i,i) = nu_shoc;

             shock_matrix_ = setup_.dc_scale_fact()*prod(K, DUDY_);
         }
     }




 private:

     int nb_el_nodes_;
     int nb_dof_fluid_;
     int nb_comp_;

     ic_bc_type& ic_bc_;
     fluid_properties& props_;
     read_setup& setup_;

     double JxW_; 
     vec JNxW_;
     double dt_;

     vec I_orig_fluid_dot_;
     vec I_dot_;

     vec P_orig_fluid_;
     vec I_orig_fluid_;
     vec P_;
     vec I_;
     vec dI_dx_;
     vec dI_dy_;

     vec I_ux_e_;
     vec I_uy_e_;
     vec mesh_v_;

     double rho_;
     double mu_;
     double cp_, cv_;
     double kappa_;
     double sound_speed_;
     double beta_, alpha_;
     double qL_dp_, qL_dT_;


     vec rho_comp_;
     vec internal_energy_comp_;
     vec chemical_diffusivity_comp_;

     vec U_P_;
     vec U_;
     vec F1_adv_;
     vec F2_adv_;
     vec F1_dif_;
     vec F2_dif_;
     vec S_;

     mat DUDY_;
     mat A1_,A2_;
     mat K11_,K12_,K21_,K22_;
     mat S0_;

     mat tau_;
     mat shock_matrix_;
     mat u_ref_inv_;

     vec gravity_;

     vec  r_;
     mat  m_;

     double tol_;
     std::shared_ptr<quad> quad_ptr_ = nullptr;
     point<2> dummy_;
     double alpha_f_, alpha_m_, gamma_;


  };



}/* namespace GALES */
#endif
