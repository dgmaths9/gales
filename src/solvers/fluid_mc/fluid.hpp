#ifndef _GALES_FLUID_HPP_
#define _GALES_FLUID_HPP_


#include "fluid_assembly.hpp"
#include "fluid_dofs_ic_bc.hpp"
#include "fluid_2d.hpp"
#include "fluid_3d.hpp"

#endif 
