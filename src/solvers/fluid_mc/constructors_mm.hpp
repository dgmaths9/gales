
  int nb_dofs_f = 2 + dim;  
  if(setup.isothermal()) nb_dofs_f = 1 + dim;
  
  const int nb_dofs_e = dim;  
  

  //------------------ mesh building --------------------------------------------------------------------
  print_only_pid<0>(std::cerr)<< "FLUID MESH\n";
  double mesh_read_start = MPI_Wtime();
  Mesh<dim> fluid_mesh(setup.fluid_mesh_file(), nb_dofs_f, setup);
  print_only_pid<0>(std::cerr)<<"Mesh reading took: "<<std::setprecision(setup.precision()) << MPI_Wtime()-mesh_read_start<<" s\n\n";

  print_only_pid<0>(std::cerr)<< "ELASTOSTATIC MESH\n";
  mesh_read_start = MPI_Wtime();
  Mesh<dim> elastostatic_mesh(setup.fluid_mesh_file(), nb_dofs_e, setup);
  print_only_pid<0>(std::cerr)<<"Mesh reading took: "<<std::setprecision(setup.precision()) << MPI_Wtime()-mesh_read_start<<" s\n\n";


  
  //---------------- mesh motion -----------------------------------------------------------------------
  Mesh_Motion<dim> mesh_motion;
  
  
  
  if(setup.restart())
  {
    std::stringstream ss;
    ss << "results/fluid_mesh/" << setup.restart_time();

    if(!does_file_exist(ss.str()))   Error(ss.str() + "   file is not opened");
    
    mesh_motion.read_updated_node_coords(ss.str(), fluid_mesh);
    mesh_motion.read_updated_node_coords(ss.str(), elastostatic_mesh);
  }



  //-----------------fluid props------------------------------------------
  fluid_properties fluid_props;

  //-----------------------------------------maps------------------------------------------------------------
  epetra_maps fluid_maps(fluid_mesh);
  epetra_maps elastostatic_maps(elastostatic_mesh);
  
  //----------------------------------------dofs state-----------------------------------------------------  
  auto fluid_dof_state = std::make_unique<dof_state>(fluid_maps.state_map()->NumMyElements(), 2);
  auto fluid_dot_dof_state = std::make_unique<dof_state>(fluid_maps.state_map()->NumMyElements(), 2);
  auto elastostatic_dof_state = std::make_unique<dof_state>(elastostatic_maps.state_map()->NumMyElements(), 2);
  
  
  //---------------------------------------model---------------------------------------------------
  model<dim> fluid_model(fluid_mesh, *fluid_dof_state, fluid_maps, setup);  
  model<dim> fluid_dot_model(fluid_mesh, *fluid_dot_dof_state, fluid_maps, setup);  
  model<dim> elastostatic_model(elastostatic_mesh, *elastostatic_dof_state, elastostatic_maps, setup);

  //----------------------------------------ic bc---------------------------------------------------------
  using fluid_ic_bc_type = fluid_ic_bc<dim>;
  fluid_ic_bc_type fluid_ic_bc;

  using elastostatic_ic_bc_type = elastostatic_ic_bc<dim>;
  elastostatic_ic_bc_type elastostatic_ic_bc(fluid_model);
  
  //----------------------------------------dofs ic bc---------------------------------------------------------
  using fluid_dofs_ic_bc_type = fluid_dofs_ic_bc<fluid_ic_bc_type, dim>;
  fluid_dofs_ic_bc_type fluid_dofs_ic_bc(fluid_model, fluid_ic_bc, fluid_dot_model);

  using elastostatic_dofs_ic_bc_type = elastostatic_dofs_ic_bc<elastostatic_ic_bc_type, dim>;
  elastostatic_dofs_ic_bc_type elastostatic_dofs_ic_bc(elastostatic_model, elastostatic_ic_bc);
  
  //--------------pressure profile-----------------------------------------------------
  using pressure_profile_type = pressure_profile_mc<fluid_ic_bc_type, dim>;
  pressure_profile_type pressure_profile(fluid_ic_bc, fluid_model, fluid_props);

  if(fluid_ic_bc.custom_pressure_profile())
     fluid_ic_bc.set_custom_pressure_profile(fluid_model, fluid_props);
    
  //---------------------updater------------------------------------------------------
  dofs_updater fluid_updater(setup);
  dofs_updater elastostatic_updater(setup);
      
  //--------------------linear system-------------------------------------------------
  linear_system fluid_lp(fluid_model);
  linear_system elastostatic_lp(elastostatic_model);
  
        
  //--------------------linear solver----------------------------------------------------
  belos_linear_solver fluid_ls_solver(setup); 
  belos_linear_solver elastostatic_ls_solver(setup); 


  //---------------------non linear residual check-------------------------------------------
  residual_check fluid_res_check;
  
  //------------------------integral---------------------------------------------------------  
  using fluid_integral_type = fluid<fluid_ic_bc_type, dim>;
  fluid_integral_type fluid_integral(fluid_ic_bc, fluid_props, fluid_model); 

  using elastostatic_integral_type = elastostatic<dim>;
  elastostatic_integral_type elastostatic_integral(elastostatic_model); 

  //-------------------------assembly--------------------------------------------------------
  using fluid_assembly_type = fluid_assembly<fluid_integral_type, fluid_dofs_ic_bc_type, dim>;
  fluid_assembly_type fluid_assembly;

  using elastostatic_assembly_type = elastostatic_assembly<elastostatic_integral_type, elastostatic_dofs_ic_bc_type, dim>;
  elastostatic_assembly_type elastostatic_assembly;

  // ------------------parallel I/O---------------------------------------------------  
  IO fluid_io(*fluid_maps.state_map(), *fluid_maps.dof_map());
  IO elastostatic_io(*elastostatic_maps.state_map(), *elastostatic_maps.dof_map());
  IO parallel_mesh_writer(*fluid_maps.shared_node_map());


  // ------------------dof trimmer---------------------------------------------------  
  dof_trimmer<dim> trimmer(fluid_model, fluid_dot_model);

  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  

