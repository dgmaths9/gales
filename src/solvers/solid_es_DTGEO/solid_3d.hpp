#ifndef SOLID_3D_HPP
#define SOLID_3D_HPP




#include "../../fem/fem.hpp"



namespace GALES{
 
   
      
   


   
  template<typename ic_bc_type>
  class solid<ic_bc_type, 3>
  {
    using element_type = element<3>;
    using vec = boost::numeric::ublas::vector<double>;
    using mat = boost::numeric::ublas::matrix<double>;


   public :  

    solid
    (
     ic_bc_type& ic_bc,
     solid_properties& props,
     model<3>& model
    ):

      ic_bc_(ic_bc),
      props_(props),
      setup_(model.setup()),     

      JNxW_(3,0.0),
      JxW_(0.0),
            
      rho_(props.rho()),
      nu_(props.nu()),
      E_(props.E()),

      D_(6,6,0.0)
      {}

  
  
  



    void execute(const element_type& el, mat& m, vec& r)
    {
      nb_el_nodes_ = el.nb_nodes();      
      N_.resize(3,3*nb_el_nodes_,false);
      B_.resize(6,nb_el_nodes_*3,false);
  
      for(int i=0; i<el.nb_gp(); i++)
      {
        quad_ptr_ = std::make_unique<quad>(el, el.gp(i), el.W_in(i));
        JxW_ = quad_ptr_->JxW();
        N();
    
        if(props_.heterogeneous())
        {
          props_.properties(el.el_node_vec(i)->get_x(), el.el_node_vec(i)->get_y(), el.el_node_vec(i)->get_z());
          rho_ = props_.rho();
          E_ = props_.E();
          nu_ = props_.nu();
        }


        //------------------------------Mat2---------------------------------------------------------------------------------------------
        B();
        D();
        const mat use1 = prod(D_,B_);
        const mat Mat2 = prod(boost::numeric::ublas::trans(B_),use1)*JxW_;
    
        m += Mat2;
      }
      
      

      if(el.on_boundary() && el.el_flag()== 4)
      {
         for(int i=0; i<el.nb_sides(); i++)
         {
           if(el.is_side_on_boundary(i))
           {
                 auto side_flag = el.side_flag(i);    
                 double p(0.0);
             
                 if(side_flag == 4) p = 1.e6;   // on dike boundary fix 1 MPa pressure
    
                 for(int j=0; j<el.nb_side_gp(i); j++)
                 {
                   quad_ptr_ = std::make_unique<quad>(el, el.side_gp(i,j), dummy_, el.W_bd(i,j));                
                   JNxW_ = quad_ptr_->JNxW();
    
                   vec traction = -p*JNxW_;                                    
                   N();
                   r += prod(boost::numeric::ublas::trans(N_),traction);
                 }
           }
         }         
      }      
    }










  void D()
  {
    D_.clear();

    const double mu = 0.5*E_/(1.0 + nu_);    
    const double lambda = E_*nu_/((1.0+nu_)*(1.0-2.0*nu_));
    
    D_(0,0) = lambda + 2.0*mu;
    D_(0,1) = lambda;
    D_(0,2) = lambda;
    D_(1,0) = lambda;
    D_(1,1) = lambda + 2.0*mu;
    D_(1,2) = lambda;
    D_(2,0) = lambda;
    D_(2,1) = lambda;
    D_(2,2) = lambda + 2.0*mu;        
    D_(3,3) = mu;
    D_(4,4) = mu;
    D_(5,5) = mu;
  }




   void N()
   {
    N_.clear();
    for(int i=0; i<nb_el_nodes_; i++)
    {
       N_(0,3*i) = quad_ptr_->sh(i);
       N_(1,3*i+1) = quad_ptr_->sh(i);
       N_(2,3*i+2) = quad_ptr_->sh(i);
    }   
   }



   
   void B()
   {
    B_.clear();
    for(int i=0; i<nb_el_nodes_; i++)
    {
       B_(0,3*i) = quad_ptr_->dsh_dx(i);
       B_(1,3*i+1) = quad_ptr_->dsh_dy(i);
       B_(2,3*i+2) = quad_ptr_->dsh_dz(i);          
       B_(3,3*i)   = quad_ptr_->dsh_dy(i);
       B_(3,3*i+1) = quad_ptr_->dsh_dx(i);
       B_(4,3*i+1) = quad_ptr_->dsh_dz(i);
       B_(4,3*i+2) = quad_ptr_->dsh_dy(i);     
       B_(5,3*i)   = quad_ptr_->dsh_dz(i);
       B_(5,3*i+2) = quad_ptr_->dsh_dx(i);
    }   
   }





   private:

    int nb_el_nodes_;
    
    ic_bc_type& ic_bc_;
    solid_properties& props_;
    read_setup& setup_;

    double JxW_; 
    vec JNxW_;

    double rho_, nu_, E_;

    mat N_;
    mat D_;      
    mat B_;

    std::shared_ptr<quad> quad_ptr_ = nullptr;
    point<3> dummy_;    
  }; 






 
} 

#endif

