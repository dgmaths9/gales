
  const int nb_dofs_s = dim;
    
  
  //------------------ mesh building --------------------------------------------------------------------
  print_only_pid<0>(std::cerr)<< "SOLID MESH\n";
  double mesh_read_start_s = MPI_Wtime();
  Mesh<dim> solid_mesh(nb_dofs_s, setup, num);
  print_only_pid<0>(std::cerr)<<"Mesh reading took: "<<std::setprecision(setup.precision()) << MPI_Wtime()-mesh_read_start_s<<" s\n\n";
  
  
  

  //-----------------props------------------------------------------
  solid_properties solid_props;

  
  //----------------------maps--------------------------------------------------------------------------
  epetra_maps solid_maps(solid_mesh);
  
  
  //-------------------- dofs state---------------------------------------------------------------------
  auto solid_u_dof_state = std::make_unique<dof_state>(solid_maps.state_map()->NumMyElements(), 1);
  
  
  //-------------------- models  ----------------------------------------------------------------
  model<dim> solid_u_model(solid_mesh, *solid_u_dof_state, solid_maps, setup);
  
  
  //-------------------- ic bc  -------------------------------------------------------------------
  using solid_ic_bc_type = ic_bc<dim>;
  solid_ic_bc_type solid_ic_bc;
  
  
  //----------------------dofs ic bc--------------------------------------------------------------------
  using solid_dofs_ic_bc_type = solid_dofs_ic_bc<solid_ic_bc_type, dim>;
  solid_dofs_ic_bc_type solid_dofs_ic_bc(solid_u_model, solid_ic_bc, setup);
  

  //------------------- updater  --------------------------------------------------------------
  dofs_updater solid_updater(setup);
  
  
  //---------------- linear system ---------------------------------------------------------
  linear_system solid_lp(solid_u_model);
  
  
  //---------------- linear solver -------------------------------------------------------
  belos_linear_solver solid_ls_solver(setup);
  
  
  //--------------non linear residual check-------------------------------------------------
  residual_check solid_res_check;
  
  
  //--------------------integral  ----------------------------------------------------------
  using solid_integral_type = solid<solid_ic_bc_type, dim>;
  solid_integral_type solid_integral(solid_ic_bc, solid_props, solid_u_model);
  
  
  //------------------ assembly  -----------------------------------------------------------
  using solid_assembly_type = solid_assembly<solid_integral_type, solid_dofs_ic_bc_type, dim>;
  solid_assembly_type solid_assembly;
  
    
  // ------------------parallel I/O--------------------------------------------------------
  IO solid_io(*solid_maps.state_map(), *solid_maps.dof_map());

  // ------------------parallel printing of rho, E, nu ------------------------------------
  Rho_E_Nu<dim> sec_dofs(*solid_maps.shared_node_map(), setup);  


