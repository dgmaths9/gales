#include <mpi.h>
#include <iostream>
#include <fstream>
 
 
#include "../../fem/fem.hpp"
#include "solid.hpp"





using namespace GALES;




template<int dim>
void print_data(int num, const std::string & fname, int size, model<dim>& solid_u_model, const std::string & dir)
{
  std::ifstream ifile("input/" + fname); 
  if(!ifile.is_open())
  {
     Error("unable to open file");
  }
  std::vector<double> x(size), y(size), z(size);
  std::string line;
  for(int i=0; i<size; i++)
  {
    std::getline(ifile, line);
    if(line.size()==0)
       Error("line is empty or line is not present in the file"); 
    std::stringstream ss(line);
    ss >> x[i] >> y[i]>> z[i];    
  }  
  ifile.close();
  
  int rank = get_rank();
  std::ofstream ofile(dir + "/" + std::to_string(num)+"_"+ std::to_string(rank)+ ".txt"); 
  if(!ofile.is_open())
  {
     Error("unable to open file");
  }

  std::vector<double> u;
  for(const auto& nd : solid_u_model.mesh().nodes())
    if(nd->flag() == 2)
      for(int i=0; i<size; i++)
        if(nd->get_x()==x[i] && nd->get_y()==y[i])
        {
          solid_u_model.extract_node_dofs(*nd, u);
          ofile << std::fixed << std::setprecision(8) << nd->gid() << " " << nd->get_x() << " " << nd->get_y() << " " << nd->get_z() << " " << u[0] << " " << u[1] << " " << u[2] << std::endl;
        }         
  
  ofile.close();
}










/*----------free surface data where u > 50.e-6 --------*/
template<int dim>
void print_all_data(int num, model<dim>& solid_u_model, const std::string & dir)
{
  int rank = get_rank();
  std::ofstream ofile(dir + "/" + std::to_string(num)+"_"+ std::to_string(rank)+".txt"); 
  if(!ofile.is_open())
  {
     Error("unable to open and read mesh file");
  }

  std::vector<double> u;
  for(const auto& nd : solid_u_model.mesh().nodes())
    if(nd->flag() == 2)
    {
       solid_u_model.extract_node_dofs(*nd, u);
       double u_mag = sqrt(u[0]*u[0] + u[1]*u[1] + u[2]*u[2]);
       if(u_mag >= 5.e-5)
          ofile << nd->gid() << " " << nd->get_x() << " " << nd->get_y() << " " << nd->get_z() << " " << u[0] << " " << u[1] << " " << u[2] << std::endl;
    }         

  ofile.close();
}








template<int dim>
void solver(read_setup& setup, int num, const std::string& d1, const std::string& d2, const std::string& d3)
{
  #include "constructors.hpp"
  

  time::get().t(0.0); 
  time::get().delta_t(setup.delta_t());	
  time::get().tick();
  

  //------------------------ SOLUTION For solid ------------------------
  double fill_time = solid_assembly.execute(solid_integral, solid_dofs_ic_bc, solid_u_model, solid_lp);   
  double solver_time = solid_ls_solver.execute(solid_lp);
        
  solid_updater.solution(*solid_u_dof_state, solid_io.state_vec(*solid_ls_solver.solution()));
 
  print_solver_info("SOLID  ", fill_time, solver_time, solid_ls_solver);        
  // -----------------------end of solid solution---------------------------------------------------------
   
   
  /*------printing of whole solution--------*/
//  solid_io.write("solid/u/", *solid_u_dof_state);   // all dofs

  print_data(num, "AI_points_elevation.txt", 1024, solid_u_model, d1);
  print_data(num, "GNSS_stations_elevation.txt",  35, solid_u_model, d2);
  print_all_data(num, solid_u_model, d3);  
}















int main(int argc, char* argv[])
{

  MPI_Init(&argc, &argv);
  int size = get_size(); 

  double start = MPI_Wtime();
  
  if(argc != 2)
  {
        std::cerr<<"argc: "<<argc<<std::endl;
        for(int i=0; i<argc; i++)
          std::cerr<<"argv["<<i<<"]: "<<argv[i]<<" ";
        std::cerr<<std::endl;  
        
        Error("case number' is missing in the command line");
  }

  int num = std::stoi(argv[1]);
  
  if(get_rank()==0)
  {
    std::cout<<"num: "<<num<<std::endl;  
  }
  
  
  //---------- creating directories -----------------------------
   make_dirs("results/AI results/GNSS results/All mesh");

   int l = floor(num/1000)*1000;
   int u = l+999;
   std::string d1 = "results/AI/AI_" + std::to_string(l) + "_" + std::to_string(u);
   make_dirs(d1);

   std::string d2 = "results/GNSS/GNSS_" + std::to_string(l) + "_" + std::to_string(u);
   make_dirs(d2);

   std::string d3 = "results/All/All_" + std::to_string(l) + "_" + std::to_string(u);
   make_dirs(d3);

//  make_dirs("results/solid/u");



  //---------------- read setup file -------------------------------
  read_setup setup;  


  solver<3>(setup, num, d1, d2, d3);

  print_only_pid<0>(std::cerr)<<"Total run time: "<<MPI_Wtime()-start<<" s\n\n\n";
 
  MPI_Finalize();
  return 0;
}
