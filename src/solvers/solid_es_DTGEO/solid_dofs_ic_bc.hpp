#ifndef SOLID_DOFS_IC_BC_HPP
#define SOLID_DOFS_IC_BC_HPP




#include "../../fem/fem.hpp"



namespace GALES{

 
  template<typename ic_bc_type, int dim>
  class solid_dofs_ic_bc{};
  
  
  
 
  template<typename ic_bc_type>
  class solid_dofs_ic_bc <ic_bc_type, 2>
  {

   public :

   solid_dofs_ic_bc(model<2>& u_model, ic_bc_type& ic_bc, read_setup& setup)
   : 
   u_state_(u_model.state()), mesh_(u_model.mesh()), ic_bc_(ic_bc)
   {}




    auto dofs_constraints(const node<2>& nd)const  
    {
      std::vector<std::pair<bool,double>> nd_dofs_constraints(nd.nb_dofs(), std::make_pair(false, 0.0)); 
      
      if(nd.flag() == 5)
      {
        for(int i=0; i<2; i++)
          nd_dofs_constraints[i] = std::make_pair(true, 0.0);
      }

      return nd_dofs_constraints;
    }


    private:
    dof_state& u_state_;
    Mesh<2>& mesh_;
    ic_bc_type& ic_bc_;    
  };
  








  template<typename ic_bc_type>
  class solid_dofs_ic_bc <ic_bc_type, 3>
  {

   public :

   solid_dofs_ic_bc(model<3>& u_model, ic_bc_type& ic_bc, read_setup& setup)
   : 
   u_state_(u_model.state()), mesh_(u_model.mesh()), ic_bc_(ic_bc)
   {}


    auto dofs_constraints(const node<3>& nd)const  
    {
      std::vector<std::pair<bool,double>> nd_dofs_constraints(nd.nb_dofs(), std::make_pair(false, 0.0));              

      if(nd.flag() == 5)
      {
        for(int i=0; i<3; i++)
          nd_dofs_constraints[i] = std::make_pair(true, 0.0);
      }
      
      return nd_dofs_constraints;
    }



    private:
    dof_state& u_state_;
    Mesh<3>& mesh_;
    ic_bc_type& ic_bc_;    
  };



}

#endif



