#ifndef SOLID_2D_HPP
#define SOLID_2D_HPP




#include "../../fem/fem.hpp"



namespace GALES{
 
   
  /*
      Below we write t in place of theta, it is just a symbol to denote theta; it is not time.
      
      In this solver we implement 3 kinds of stress-strain element:
      1) plain stress     size along x1 & x2  >>> size long x3 (very thin) 
      2) plain strain     size along x1 & x2  <<< size long x3 (very thick) 
      3) axisymmetric(r,t,z)     symmetric along z-axis      (torsionless axisymmetric)
        
          x1 = r          x2 = z            x3 = t
          u1 = u_r        u2 = u_z          u3 = u_t = 0, 
             
          In axisymmetric formulation we do not multiply by 2*pi as it is common in all integrals and cancels out.
          
          tau_11 = sigma_rr        tau_22 = sigma_zz       tau_12 = sigma_rz         tau_33 = sigma_tt                         tau_13 = sigma_rt = 0        tau_23 = sigma_zt = 0
          eps_11 = eps_rr          eps_22 = eps_zz         2*eps_12 = 2*eps_rz       eps_33 = eps_tt = u_r/r = u1/x            eps_13 = eps_rt = 0          eps_23 = eps_zt = 0 

          tau_11 = sigma_rr = (lambda + 2*mu)*eps_11 + lambda*eps_22 + lambda*eps_33
          tau_22 = sigma_zz = lambda*eps_11 + (lambda + 2*mu)*eps_22 + lambda*eps_33
          tau_12 = sigma_rz = mu*(2*eps_12)
          tau_33 = sigma_tt = lambda*eps_11 + lambda*eps_22 + (lambda + 2*mu)*eps_33                    
  */        
   


  template<typename ic_bc_type, int dim> class solid{};

   
  template<typename ic_bc_type>
  class solid<ic_bc_type, 2>
  {
    using element_type = element<2>;
    using vec = boost::numeric::ublas::vector<double>;
    using mat = boost::numeric::ublas::matrix<double>;


   public :  

    solid
    (
     ic_bc_type& ic_bc,
     solid_properties& props,
     model<2>& model
    ):

      ic_bc_(ic_bc),
      props_(props),
      setup_(model.setup()),     

      JNxW_(2,0.0),
      JxW_(0.0),

      condition_type_(props.condition_type()),      

      rho_(props.rho()),
      nu_(props.nu()),
      E_(props.E()),

      D_(3,3,0.0)
     {      
       if(condition_type_ == "Axisymmetric")  
          D_.resize(4,4);
     }

  
  
  



    void execute(const element_type& el, mat& m, vec& r)
    {}











  void D()
  {
    D_.clear();
    
//    //--------------radially changing parameters--------------------------------
//    const double x = quad_ptr_->x();
//    const double y = quad_ptr_->y();
//    const double r = sqrt(x*x + (y+2000.0)*(y+2000.0));  // distance from point (0, -2000)
//    const double T = 900.0*1000.0/r + 100.0; 
//    E_ = 10.e9*(1.0-0.5*(exp(T/1000.0)-1.0));
//    nu_ = (1.0-E_/10.e9)*0.15 + 0.25; 
//    //------------------------------------------------------------

    const double mu = 0.5*E_/(1.0 + nu_);    
    double lambda(0.0);
    if(condition_type_ == "Plain_Strain" || condition_type_ == "Axisymmetric")  lambda = E_*nu_/((1.0+nu_)*(1.0-2.0*nu_));
    else if(condition_type_ == "Plain_Stress")  lambda = E_*nu_/(1.0-nu_*nu_);
    
    D_(0,0) = lambda + 2.0*mu;
    D_(0,1) = lambda;
    D_(1,0) = lambda;
    D_(1,1) = lambda + 2.0*mu;
    D_(2,2) = mu;        

    if(condition_type_ == "Axisymmetric")
    {         
      D_(3,0) = lambda;
      D_(0,3) = lambda;
      D_(3,1) = lambda;
      D_(1,3) = lambda;
      D_(3,3) = lambda + 2.0*mu;
    } 
  }




   void N()
   {
    N_.clear();
    for(int i=0; i<nb_el_nodes_; i++)
    {
       N_(0,2*i) = quad_ptr_->sh(i);
       N_(1,2*i+1) = quad_ptr_->sh(i);
    }   
   }



   
   void B()
   {
    B_.clear();        
    for(int i=0; i<nb_el_nodes_; i++)
    {
       B_(0,2*i) = quad_ptr_->dsh_dx(i);
       B_(1,2*i+1) = quad_ptr_->dsh_dy(i);
       B_(2,2*i)   = quad_ptr_->dsh_dy(i);
       B_(2,2*i+1) = quad_ptr_->dsh_dx(i);
    }   
    if(condition_type_ == "Axisymmetric")
    {         
      for(int i=0; i<nb_el_nodes_; i++)
         B_(3,2*i) = quad_ptr_->sh(i)/quad_ptr_->x();
    }
   }





   private:

    int nb_el_nodes_;
    
    ic_bc_type& ic_bc_;
    solid_properties& props_;
    read_setup& setup_;

    double JxW_; 
    vec JNxW_;

    std::string condition_type_;
    double rho_, nu_, E_;

    mat N_;
    mat D_;      
    mat B_;

    std::shared_ptr<quad> quad_ptr_ = nullptr;
    point<2> dummy_;    
  }; 






 
} 

#endif

