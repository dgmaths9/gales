#include <mpi.h>
#include <iostream>


#include "../../fem/fem.hpp"
#include "../fluid_sc/fluid.hpp"
#include "../heat_equation/heat_eq.hpp"




using namespace GALES;




template<int dim>
void solver(read_setup& setup)
{
  #include "../fluid_sc/constructors.hpp"
  #include "../heat_equation/constructors.hpp"



  //-------------fluid - heat equation coupling -------------------------------------------------
  heat_eq_T<dim> heat_eq_T;
  f_heat_flux<dim> f_heat_flux;







  time::get().t(0.0);
  time::get().delta_t(setup.delta_t());

  double restart_or_t_zero = MPI_Wtime();  
  if(!setup.restart())
  { 
    //--------- setting up dofs for time = 0 ---------------- 

    // ------------assigning T from heat equation to fluid at the interface --------
    double coupling_heat_eq_start = MPI_Wtime();
    std::map<int, double> heat_eq_nd_T;             
    heat_eq_T.T(heat_eq_model, heat_eq_nd_T);
    fluid_ic_bc.set_heat_eq_T(heat_eq_nd_T);
    double coupling_heat_eq = MPI_Wtime()-coupling_heat_eq_start;
    //------------------------------------------------------

    fluid_dofs_ic_bc.dirichlet_bc();
    fluid_io.write("fluid_dofs/", *fluid_dof_state);
    
    heat_eq_dofs_ic_bc.dirichlet_bc();
    heat_eq_io.write("heat_eq/T/", *heat_eq_dof_state);
    
    if(setup.Gen_alpha()) 
    {
      fluid_io.write("fluid_dot_dofs/", *fluid_dot_dof_state);
      heat_eq_io.write("heat_eq/T_dot/", *heat_eq_dot_dof_state);    
    }
    
    print_only_pid<0>(std::cerr)<<"Dofs writing for time 0 took: "<<std::setprecision(setup.precision()) << MPI_Wtime()-restart_or_t_zero<<" s\n\n";       
  }
  else
  {
     //--------------read restart-----------------------------
    time::get().t(setup.restart_time());
    fluid_io.read("fluid_dofs/", *fluid_dof_state);
    heat_eq_io.read("heat_eq/T/", *heat_eq_dof_state);

    if(setup.Gen_alpha()) 
    {
      fluid_io.read("fluid_dot_dofs/", *fluid_dot_dof_state);
      heat_eq_io.read("heat_eq/T_dot/", *heat_eq_dot_dof_state);
    }

    print_only_pid<0>(std::cerr)<<"Dofs reading at restart time " << time::get().t() << " took: "<<std::setprecision(setup.precision()) << MPI_Wtime()-restart_or_t_zero<<" s\n\n";       
  }
  time::get().tick();




  // ---------------------TIME LOOP---------------------------
  double time_loop_start = MPI_Wtime();
  double non_linear_res_fluid(0.0), non_linear_res_heat_eq(0.0);
  int tot_time_iterations(0);

  for(; time::get().t()<=setup.end_time(); time::get().tick())
  {

    double t_iteration(MPI_Wtime());

    print_only_pid<0>(std::cerr)<<"simulation time: "<<std::setprecision(setup.precision()) << time::get().t()<<" s\n";






    //----------------predictors--------------------------------------------------------------------------------
    if(setup.Gen_alpha()) 
    {
      fluid_updater.predictor(*fluid_dof_state, *fluid_dot_dof_state);
      heat_eq_updater.predictor(*heat_eq_dof_state, *heat_eq_dot_dof_state);
    }    
    else
    { 
      fluid_updater.predictor(*fluid_dof_state);
      heat_eq_updater.predictor(*heat_eq_dof_state);
    }
    //----------------end of predictors--------------------------------------------------------------------------------
        






      //----------------------------------- SOLUTION For fluid -------------------------------------------

      // ------------assigning T from heat equation to fluid at the interface --------
      double coupling_heat_eq_start = MPI_Wtime();
      std::map<int, double> heat_eq_nd_T;             
      heat_eq_T.T(heat_eq_model, heat_eq_nd_T);
      fluid_ic_bc.set_heat_eq_T(heat_eq_nd_T);
      double coupling_heat_eq = MPI_Wtime()-coupling_heat_eq_start;
      //------------------------------------------------------

      fluid_dofs_ic_bc.dirichlet_bc();

      for(int f_it_count=0; f_it_count < setup.n_max_it(); f_it_count++)   //fluid iterations loop
      {

	double fill_time(0.0);
	if(setup.Gen_alpha()) fill_time = fluid_assembly.execute(fluid_integral, fluid_dofs_ic_bc, fluid_model, fluid_lp, fluid_dot_model); 
	else fill_time = fluid_assembly.execute(fluid_integral, fluid_dofs_ic_bc, fluid_model, fluid_lp);   


        bool residual_converged = fluid_res_check.execute(*fluid_lp.rhs(), f_it_count, non_linear_res_fluid);      
        if(residual_converged)
        {
          print_solver_info("FLUID  ", f_it_count, fill_time, non_linear_res_fluid);        
          break;
        } 	      
        double solver_time = fluid_ls_solver.execute(fluid_lp);


        if(setup.Gen_alpha()) fluid_updater.corrector(*fluid_dof_state, *fluid_dot_dof_state, fluid_io.state_vec(*fluid_ls_solver.solution()));
        else fluid_updater.corrector(*fluid_dof_state, fluid_io.state_vec(*fluid_ls_solver.solution()));


        print_solver_info("FLUID  ", f_it_count, fill_time, non_linear_res_fluid, solver_time, fluid_ls_solver);        
      }
      // -----------------------end of fluid solution---------------------------------------------------------







      //------------------------ SOLUTION For heat_eq ------------------------------------------------------

      // ------------assigning fluid heat flux to heat equation at the interface---------
      double coupling_fluid_heat_flux_start = MPI_Wtime();      
      std::map<int, std::vector<double> > f_nd_q;       
      f_heat_flux.heat_flux(fluid_model, fluid_props, f_nd_q);
      heat_eq_ic_bc.set_fluid_heat_flux(f_nd_q);	       
      double coupling_fluid_heat_flux = MPI_Wtime()-coupling_fluid_heat_flux_start;
      //-------------------------------------------------------------

      heat_eq_dofs_ic_bc.dirichlet_bc();


      for(int it_count=0; it_count < setup.n_max_it(); it_count++)   // heat equation iterations loop
      {
        
	double fill_time(0.0);
	if(setup.Gen_alpha()) fill_time = heat_eq_assembly.execute(heat_eq_integral, heat_eq_dofs_ic_bc, heat_eq_model, heat_eq_lp, heat_eq_dot_model);
	else fill_time = heat_eq_assembly.execute(heat_eq_integral, heat_eq_dofs_ic_bc, heat_eq_model, heat_eq_lp);   
                
                
        bool residual_converged = heat_eq_res_check.execute(*heat_eq_lp.rhs(), it_count, non_linear_res_heat_eq);      
        if(residual_converged)
        {
          print_solver_info("HEAT   ", it_count, fill_time, non_linear_res_heat_eq);
      	  break;
        } 	      
        double solver_time = heat_eq_ls_solver.execute(heat_eq_lp);


     	if(setup.Gen_alpha()) heat_eq_updater.corrector(*heat_eq_dof_state, *heat_eq_dot_dof_state, heat_eq_io.state_vec(*heat_eq_ls_solver.solution()));
     	else heat_eq_updater.corrector(*heat_eq_dof_state, heat_eq_io.state_vec(*heat_eq_ls_solver.solution()));
     	
     	
        print_solver_info("HEAT   ", it_count, fill_time, non_linear_res_heat_eq, solver_time, heat_eq_ls_solver);    
        // -----------------------end of heat_eq solution---------------------------------------------------------
      }
	      
      
      


    if((tot_time_iterations+1)% (setup.print_freq()) == 0)
    {
      fluid_io.write("fluid_dofs/", *fluid_dof_state);
      heat_eq_io.write("heat_eq/T/", *heat_eq_dof_state);
      if(setup.Gen_alpha()) 
      {
        fluid_io.write("fluid_dot_dofs/", *fluid_dot_dof_state);
        heat_eq_io.write("heat_eq/T_dot/", *heat_eq_dot_dof_state);      
      }
    }



    //-------- adaptive time step-----------------
    if(setup.adaptive_time_step() == true && tot_time_iterations > setup.N())
    {
       adaptive_time_heat_eq(heat_eq_model, heat_eq_props, setup);
       adaptive_time_f_sc_mc<dim>(fluid_model, fluid_props, setup);
    }



    print_only_pid<0>(std::cerr)<<"Time step took: "<<MPI_Wtime()-t_iteration<<" s\n\n";
    tot_time_iterations++;

  }
  

  print_only_pid<0>(std::cerr)<<"End time reached!!!! "<<"\n\n";
  print_only_pid<0>(std::cerr)<<"Total run time: "<<MPI_Wtime()-time_loop_start<<" s\n\n\n";

}










int main(int argc, char* argv[])
{

  MPI_Init(&argc, &argv);


  //---------------- read setup file -------------------------------
  read_setup setup;



  //---------- creating directories -----------------------------
  make_dirs("results/fluid_dofs results/heat_eq/T");
  if(setup.Gen_alpha()) make_dirs("results/fluid_dot_dofs results/heat_eq/T_dot");





  if(setup.dim()==2) solver<2>(setup);
  else if(setup.dim()==3) solver<3>(setup);
 

  MPI_Barrier(MPI_COMM_WORLD);
  MPI_Finalize();
  return 0;
}

