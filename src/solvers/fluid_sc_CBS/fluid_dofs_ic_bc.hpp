#ifndef _GALES_FLUID_DOFS_IC_BC_HPP_
#define _GALES_FLUID_DOFS_IC_BC_HPP_


#include"../../fem/fem.hpp"



namespace GALES {



  template<typename ic_bc_type, int dim>
  class fluid_dofs_ic_bc{};






  template<typename ic_bc_type>
  class fluid_dofs_ic_bc <ic_bc_type, 2>
  {
      using model_type = model<2>;
      
      public:
      
      
      
      fluid_dofs_ic_bc(model_type& p_model, model_type& T_model, model_type& v1_model, model_type& v2_model, ic_bc_type& ic_bc)
      {
         set_ic_pvT(p_model, T_model, v1_model, v2_model, ic_bc);
      }
      


      void set_ic_pvT(model_type& p_model, model_type& T_model, model_type& v1_model, model_type& v2_model, ic_bc_type& ic_bc)
      {
           set_ic_p(p_model, ic_bc);       
           set_ic_T(T_model, ic_bc);       
           set_ic_v1(v1_model, ic_bc);       
           set_ic_v2(v2_model, ic_bc);       
      }



      void set_ic_p(model_type& p_model, ic_bc_type& ic_bc)
      {
         for(const auto& nd : p_model.mesh().nodes())
            p_model.state().set_dof(nd->first_dof_lid(), ic_bc_.initial_p(*nd));
      }
      

      void set_ic_T(model_type& T_model, ic_bc_type& ic_bc)
      {
         for(const auto& nd : T_model.mesh().nodes())
            T_model.state().set_dof(nd->first_dof_lid(), ic_bc_.initial_T(*nd));
      }


      void set_ic_v1(model_type& v1_model, ic_bc_type& ic_bc)
      {
         for(const auto& nd : v1_model.mesh().nodes())
            v1_model.state().set_dof(nd->first_dof_lid(), ic_bc_.initial_vx(*nd));
      }

      void set_ic_v2(model_type& v2_model, ic_bc_type& ic_bc)
      {
         for(const auto& nd : v2_model.mesh().nodes())
            v2_model.state().set_dof(nd->first_dof_lid(), ic_bc_.initial_vy(*nd));
      }

      







      void set_dirichlet_p(model_type& p_model, ic_bc_type& ic_bc)
      {
         std::pair<bool,double> result;
         for(const auto& nd : p_model.mesh().nodes())
         {
           result = ic_bc_.dirichlet_p(*nd);
           if(result.first)   p_model_.state().set_dof(nd->first_dof_lid(), result.second);   
         }      
      }



      void set_dirichlet_T(model_type& T_model, ic_bc_type& ic_bc)
      {
         std::pair<bool,double> result;
         for(const auto& nd : T_model.mesh().nodes())
         {
           result = ic_bc_.dirichlet_T(*nd);
           if(result.first)   T_model_.state().set_dof(nd->first_dof_lid(), result.second);   
         }      
      }



      void set_dirichlet_v1(model_type& v1_model, ic_bc_type& ic_bc)
      {
         std::pair<bool,double> result;
         for(const auto& nd : v1_model.mesh().nodes())
         {
           result = ic_bc_.dirichlet_vx(*nd);
           if(result.first)   v1_model_.state().set_dof(nd->first_dof_lid(), result.second);   
         }      
      }


      void set_dirichlet_v2(model_type& v2_model, ic_bc_type& ic_bc)
      {
         std::pair<bool,double> result;
         for(const auto& nd : v2_model.mesh().nodes())
         {
           result = ic_bc_.dirichlet_vy(*nd);
           if(result.first)   v2_model_.state().set_dof(nd->first_dof_lid(), result.second);   
         }      
      }



      void set_dirichlet_pvT(model_type& p_model, model_type& T_model, model_type& v1_model, model_type& v2_model, ic_bc_type& ic_bc)
      {
           set_dirichlet_p(p_model, ic_bc);       
           set_dirichlet_T(T_model, ic_bc);       
           set_dirichlet_v1(v1_model, ic_bc);       
           set_dirichlet_v2(v2_model, ic_bc);       
      }







    auto dofs_constraints(const node<2>& nd)const  
    {
      std::vector<std::pair<bool,double>> nd_dofs_constraints(nd.nb_dofs(), std::make_pair(false, 0.0));              

      if(ic_bc_.dirichlet_p(nd).first) nd_dofs_constraints[0] = std::make_pair(true, 0.0);
      if(ic_bc_.dirichlet_vx(nd).first) nd_dofs_constraints[1] = std::make_pair(true, 0.0);
      if(ic_bc_.dirichlet_vy(nd).first) nd_dofs_constraints[2] = std::make_pair(true, 0.0);
      if(!model_.setup().isothermal() && ic_bc_.dirichlet_T(nd).first) nd_dofs_constraints[3] = std::make_pair(true, 0.0);
      
      return nd_dofs_constraints;
    }

 
    private:
    model_type& model_;
    ic_bc_type& ic_bc_;
  };












  template<typename ic_bc_type>
  class fluid_dofs_ic_bc <ic_bc_type, 3>
  {
      using model_type = model<3>;

      public:
      

      fluid_dofs_ic_bc(model_type& p_model, model_type& T_model, model_type& v1_model, model_type& v2_model, model_type& v3_model, ic_bc_type& ic_bc)
      {
         set_ic_pvT(p_model, T_model, v1_model, v2_model, v3_model, ic_bc);
      }
      


      void set_ic_pvT(model_type& p_model, model_type& T_model, model_type& v1_model, model_type& v2_model, model_type& v3_model, ic_bc_type& ic_bc)
      {
           set_ic_p(p_model, ic_bc);       
           set_ic_T(T_model, ic_bc);       
           set_ic_v1(v1_model, ic_bc);       
           set_ic_v2(v2_model, ic_bc);       
           set_ic_v3(v3_model, ic_bc);       
      }


      void set_ic_p(model_type& p_model, ic_bc_type& ic_bc)
      {
         for(const auto& nd : p_model.mesh().nodes())
            p_model.state().set_dof(nd->first_dof_lid(), ic_bc_.initial_p(*nd));
      }
      

      void set_ic_T(model_type& T_model, ic_bc_type& ic_bc)
      {
         for(const auto& nd : T_model.mesh().nodes())
            T_model.state().set_dof(nd->first_dof_lid(), ic_bc_.initial_T(*nd));
      }


      void set_ic_v1(model_type& v1_model, ic_bc_type& ic_bc)
      {
         for(const auto& nd : v1_model.mesh().nodes())
            v1_model.state().set_dof(nd->first_dof_lid(), ic_bc_.initial_vx(*nd));
      }


      void set_ic_v2(model_type& v2_model, ic_bc_type& ic_bc)
      {
         for(const auto& nd : v2_model.mesh().nodes())
            v2_model.state().set_dof(nd->first_dof_lid(), ic_bc_.initial_vy(*nd));
      }


      void set_ic_v3(model_type& v3_model, ic_bc_type& ic_bc)
      {
         for(const auto& nd : v3_model.mesh().nodes())
            v3_model.state().set_dof(nd->first_dof_lid(), ic_bc_.initial_vz(*nd));
      }









      void set_dirichlet_p(model_type& p_model, ic_bc_type& ic_bc)
      {
         std::pair<bool,double> result;
         for(const auto& nd : p_model.mesh().nodes())
         {
           result = ic_bc_.dirichlet_p(*nd);
           if(result.first)   p_model_.state().set_dof(nd->first_dof_lid(), result.second);   
         }      
      }



      void set_dirichlet_T(model_type& T_model, ic_bc_type& ic_bc)
      {
         std::pair<bool,double> result;
         for(const auto& nd : T_model.mesh().nodes())
         {
           result = ic_bc_.dirichlet_T(*nd);
           if(result.first)   T_model_.state().set_dof(nd->first_dof_lid(), result.second);   
         }      
      }




      void set_dirichlet_v1(model_type& v1_model, ic_bc_type& ic_bc)
      {
         std::pair<bool,double> result;
         for(const auto& nd : v1_model.mesh().nodes())
         {
           result = ic_bc_.dirichlet_vx(*nd);
           if(result.first)   v1_model_.state().set_dof(nd->first_dof_lid(), result.second);   
         }      
      }


      void set_dirichlet_v2(model_type& v2_model, ic_bc_type& ic_bc)
      {
         std::pair<bool,double> result;
         for(const auto& nd : v2_model.mesh().nodes())
         {
           result = ic_bc_.dirichlet_vy(*nd);
           if(result.first)   v2_model_.state().set_dof(nd->first_dof_lid(), result.second);   
         }      
      }


      void set_dirichlet_v3(model_type& v3_model, ic_bc_type& ic_bc)
      {
         std::pair<bool,double> result;
         for(const auto& nd : v3_model.mesh().nodes())
         {
           result = ic_bc_.dirichlet_vz(*nd);
           if(result.first)   v3_model_.state().set_dof(nd->first_dof_lid(), result.second);   
         }      
      }


      void set_dirichlet_pvT(model_type& p_model, model_type& T_model, model_type& v1_model, model_type& v2_model, model_type& v3_model, ic_bc_type& ic_bc)
      {
           set_dirichlet_p(p_model, ic_bc);       
           set_dirichlet_T(T_model, ic_bc);       
           set_dirichlet_v1(v1_model, ic_bc);       
           set_dirichlet_v2(v2_model, ic_bc);       
           set_dirichlet_v3(v3_model, ic_bc);       
      }














    auto dofs_constraints(const node<3>& nd)const  
    {
      std::vector<std::pair<bool,double>> nd_dofs_constraints(nd.nb_dofs(), std::make_pair(false, 0.0));              

      if(ic_bc_.dirichlet_p(nd).first) nd_dofs_constraints[0] = std::make_pair(true, 0.0);
      if(ic_bc_.dirichlet_vx(nd).first) nd_dofs_constraints[1] = std::make_pair(true, 0.0);
      if(ic_bc_.dirichlet_vy(nd).first) nd_dofs_constraints[2] = std::make_pair(true, 0.0);
      if(ic_bc_.dirichlet_vz(nd).first) nd_dofs_constraints[3] = std::make_pair(true, 0.0);
      if(!model_.setup().isothermal() && ic_bc_.dirichlet_T(nd).first) nd_dofs_constraints[4] = std::make_pair(true, 0.0);

      return nd_dofs_constraints;
    }


    private:
    model_type& model_;
    ic_bc_type& ic_bc_;
  };




} /* namespace GALES */

#endif

