#ifndef _FLUID_2D_HPP_
#define _FLUID_2D_HPP_



#include "../../fem/fem.hpp"




namespace GALES
{



  template<typename ic_bc_type, int dim>
  class fluid{};

    



  template<typename ic_bc_type>
  class fluid<ic_bc_type, 2>
  {
    using element_type = element<2>;
    using vec = boost::numeric::ublas::vector<double>;
    using mat = boost::numeric::ublas::matrix<double>;

    public :

    fluid(ic_bc_type& ic_bc, fluid_properties& props, model<2>& model)
    :
    nb_dof_fluid_(model.mesh().nb_nd_dofs()),

    ic_bc_(ic_bc),
    props_(props),
    setup_(model.setup()),

    JNxW_(2,0.0),
    JxW_(0.0),
    dt_(0.0),

    P_(nb_dof_fluid_,0.0),
    I_(nb_dof_fluid_,0.0),
    dI_dx_(nb_dof_fluid_,0.0),
    dI_dy_(nb_dof_fluid_,0.0),


    alpha_(0.0),
    beta_(0.0),
    rho_(0.0),
    mu_(0.0),
    cp_(0.0),
    cv_(0.0),
    kappa_(0.0),
    sound_speed_(0.0),
    qL_dp_(0.0), 
    qL_dT_(0.0),

    U_P_(nb_dof_fluid_,0.0),
    U_(nb_dof_fluid_,0.0),
    F1_adv_(nb_dof_fluid_,0.0),
    F2_adv_(nb_dof_fluid_,0.0),
    F1_dif_(nb_dof_fluid_,0.0),
    F2_dif_(nb_dof_fluid_,0.0),
    S_(nb_dof_fluid_,0.0),

    gravity_(2,0.0),

    tol_(std::numeric_limits<double>::epsilon())
    {
       ic_bc_.body_force(gravity_);
    }






    // This is for Euler-backward method
     void get_dofs(const std::vector<vec>& dofs_fluid)
     {
         dt_= time::get().delta_t();
 
         P_orig_fluid_ = dofs_fluid[1];
         I_orig_fluid_ = dofs_fluid[0];
     }
 





 
     void get_properties()
     {
         rho_ = props_.rho();
         mu_ = props_.mu();
         kappa_ = props_.kappa();
         sound_speed_ = props_.sound_speed();
         cv_ = props_.cv();
         cp_ = props_.cp();
         alpha_ = props_.alpha();
         beta_ = props_.beta();
        
         if(setup_.print_props())
         {
            if(setup_.isothermal()) props_.print_props_sc_isothermal();
            else props_.print_props_sc();
         }   
     }
 




     void interpolation()
     {
          quad_ptr_->interpolate(I_orig_fluid_,I_);
          quad_ptr_->x_derivative_interpolate(I_orig_fluid_,dI_dx_);
          quad_ptr_->y_derivative_interpolate(I_orig_fluid_,dI_dy_);     
     }
 
 
 
 
 
    
     void U_P()
     {
              double p,vx,vy,T;
              quad_ptr_->interpolate(P_orig_fluid_,P_);
              assign(P_,p,vx,vy,T);
              props_.properties(p, T, 0.0);           
              U(p,vx,vy,T, U_P_);     
     }
     



     //This is for element interior
     void flux_matrices_and_vectors()
     {
              double p,vx,vy,T;
              assign(I_,p,vx,vy,T);
              const double shear_rate = compute_shear_rate(dI_dx_, dI_dy_);
              props_.properties(p,T, shear_rate);           
              get_properties();
              flux_matrices_and_vectors(p,vx,vy,T);
              tau_matrix(vx,vy);     
     }
 



     //This is for boundary elements      
     void flux_matrices_and_vectors(const std::vector<int>& bd_nodes, int side_flag)
     {
              double p,vx,vy,T;
              assign(I_,p,vx,vy,T);
              const double shear_rate = compute_shear_rate(dI_dx_, dI_dy_);
              props_.properties(p,T, shear_rate);           
              get_properties();
              flux_matrices_and_vectors(p,vx,vy,T,bd_nodes, side_flag);
     }
 




     void set_el_data_size()
     {
         P_orig_fluid_.resize(nb_dof_fluid_*nb_el_nodes_, false);
         I_orig_fluid_.resize(nb_dof_fluid_*nb_el_nodes_, false);

         r_.resize(nb_dof_fluid_*nb_el_nodes_, false);
         m_.resize(nb_dof_fluid_*nb_el_nodes_, nb_dof_fluid_*nb_el_nodes_, false);
     }

 
 
 


     void get_el_data(const std::vector<vec>& dofs_p, const std::vector<vec>& dofs_v1, const std::vector<vec>& dofs_v2, const std::vector<vec>& dofs_T,
                      vec& el_p, vec& el_v1, vec& el_v2, vec& el_T, 
                      vec& el_rho, vec& el_mu, vec& el_U1, vec& el_U2)
     {
         vec el_p = dofs_p[1];
         vec el_v1 = dofs_v1[1];
         vec el_v2 = dofs_v2[1];
         vec el_T = dofs_T[1];
         
         
         vec el_rho(nb_el_nodes_, 0.0), el_mu(nb_el_nodes_, 0.0), el_U1(nb_el_nodes_, 0.0), el_U2(nb_el_nodes_, 0.0);
         for(int i=0; i<nb_el_nodes_; i++)
         {
            props_.properties(el_p[i], el_T[i], 0.0);
            el_rho[i] = props_.rho();
            el_U1[i] = el_rho[i]*el_v1[i];
            el_U2[i] = el_rho[i]*el_v2[i];
            el_mu[i] = props_.mu();
         }     
     }
     
     
     
     




     void execute_U1_star(const element_type& el, 
                          const std::vector<vec>& dofs_p, 
                          const std::vector<vec>& dofs_v1, 
                          const std::vector<vec>& dofs_v2, 
                          const std::vector<vec>& dofs_T,                         
                          mat& m, vec& r)
     {
         nb_el_nodes_ = el.nb_nodes();      
         set_el_data_size();

         vec el_p(nb_el_nodes_, 0.0), el_v1(nb_el_nodes_, 0.0), el_v2(nb_el_nodes_, 0.0), el_T(nb_el_nodes_, 0.0);
         vec el_rho(nb_el_nodes_, 0.0), el_mu(nb_el_nodes_, 0.0), el_U1(nb_el_nodes_, 0.0), el_U2(nb_el_nodes_, 0.0);   
                                          
         
         r_.clear();
         m_.clear();

         for(const auto& quad_ptr : el.quad_i())
         {
            quad_ptr_ = quad_ptr;
            JxW_ = quad_ptr_->JxW();                                                              

            double U1(0.0), dU1_dx(0.0), dU1_dy(0.0), p(0.0), v1(0.0), v2(0.0), v1_1(0.0), v1_2(0.0), v2_1(0.0), v2_2(0.0), mu(0.0); 
            quad_ptr_->interpolate(el_U1, U1);
            quad_ptr_->x_derivative_interpolate(el_U1, dU1_dx);
            quad_ptr_->y_derivative_interpolate(el_U1, dU1_dy);
            quad_ptr_->interpolate(el_p, p);
            quad_ptr_->interpolate(el_v1, v1);
            quad_ptr_->interpolate(el_v2, v2);
            quad_ptr_->x_derivative_interpolate(el_v1, v1_1);
            quad_ptr_->y_derivative_interpolate(el_v1, v1_2);
            quad_ptr_->x_derivative_interpolate(el_v2, v2_1);
            quad_ptr_->y_derivative_interpolate(el_v2, v2_2);
            quad_ptr_->interpolate(el_mu, mu);
            
            double tau11 = 2.0/3.0*mu(2*v1_1 - v2_2);                        
            double tau12 = mu(v1_2 + v2_1);                        
                                    
            RM_i_U_star(U1, dU1_dx, dU1_dy, tau11, tau12, gravity_[0], v1, v2, v1_1, v2_2); 
         }

         if(el.on_boundary())
         {
            for(int i=0; i<el.nb_sides(); i++)
            {
              if(el.is_side_on_boundary(i))
              {
                auto bd_nodes = el.side_nodes(i);
                auto side_flag = el.side_flag(i);
                
                for(const auto& quad_ptr: el.quad_b(i))
                {
                   quad_ptr_ = quad_ptr;               
                   JNxW_ = quad_ptr_->JNxW();
                        
                   double v1_1(0.0), v1_2(0.0), v2_1(0.0), v2_2(0.0), mu(0.0); 
                   quad_ptr_->x_derivative_interpolate(el_v1, v1_1);
                   quad_ptr_->y_derivative_interpolate(el_v1, v1_2);
                   quad_ptr_->x_derivative_interpolate(el_v2, v2_1);
                   quad_ptr_->y_derivative_interpolate(el_v2, v2_2);
                   quad_ptr_->interpolate(el_mu, mu);
              
                   double tau11 = 2.0/3.0*mu(2*v1_1 - v2_2);                        
                   double tau12 = mu(v1_2 + v2_1);                        
                        
                   RM_b_U_star(tau11, tau12);
                }
              }
            }            
         }
         m = m_;
         r = -r_;
     }
 
 
 







     void execute_U2_star(const element_type& el, 
                          const std::vector<vec>& dofs_p, 
                          const std::vector<vec>& dofs_v1, 
                          const std::vector<vec>& dofs_v2, 
                          const std::vector<vec>& dofs_T,                         
                          mat& m, vec& r)
     {
         nb_el_nodes_ = el.nb_nodes();      
         set_el_data_size();

         vec el_p = dofs_p[1];
         vec el_v1 = dofs_v1[1];
         vec el_v2 = dofs_v2[1];
         vec el_T = dofs_T[1];
         
         
         vec el_rho(nb_el_nodes_, 0.0), el_mu(nb_el_nodes_, 0.0), el_U1(nb_el_nodes_, 0.0), el_U2(nb_el_nodes_, 0.0);
         for(int i=0; i<nb_el_nodes_; i++)
         {
            props_.properties(el_p[i], el_T[i], 0.0);
            el_rho[i] = props_.rho();
            el_U1[i] = el_rho[i]*el_v1[i];
            el_U2[i] = el_rho[i]*el_v2[i];
            el_mu[i] = props_.mu();
         }
                                    
         
         r_.clear();
         m_.clear();

         for(const auto& quad_ptr : el.quad_i())
         {
            quad_ptr_ = quad_ptr;
            JxW_ = quad_ptr_->JxW();                                                              

            double U1(0.0), dU1_dx(0.0), dU1_dy(0.0), p(0.0), v1(0.0), v2(0.0), v1_1(0.0), v1_2(0.0), v2_1(0.0), v2_2(0.0), mu(0.0); 
            quad_ptr_->interpolate(el_U1, U1);
            quad_ptr_->x_derivative_interpolate(el_U1, dU1_dx);
            quad_ptr_->y_derivative_interpolate(el_U1, dU1_dy);
            quad_ptr_->interpolate(el_p, p);
            quad_ptr_->interpolate(el_v1, v1);
            quad_ptr_->interpolate(el_v2, v2);
            quad_ptr_->x_derivative_interpolate(el_v1, v1_1);
            quad_ptr_->y_derivative_interpolate(el_v1, v1_2);
            quad_ptr_->x_derivative_interpolate(el_v2, v2_1);
            quad_ptr_->y_derivative_interpolate(el_v2, v2_2);
            quad_ptr_->interpolate(el_mu, mu);
            
            double tau11 = 2.0/3.0*mu(2*v1_1 - v2_2);                        
            double tau12 = mu(v1_2 + v2_1);                        
                                    
            RM_i_U_star(U1, dU1_dx, dU1_dy, tau11, tau12, gravity_[0], v1, v2, v1_1, v2_2); 
         }

         if(el.on_boundary())
         {
            for(int i=0; i<el.nb_sides(); i++)
            {
              if(el.is_side_on_boundary(i))
              {
                auto bd_nodes = el.side_nodes(i);
                auto side_flag = el.side_flag(i);
                
                for(const auto& quad_ptr: el.quad_b(i))
                {
                   quad_ptr_ = quad_ptr;               
                   JNxW_ = quad_ptr_->JNxW();
                        
                   double v1_1(0.0), v1_2(0.0), v2_1(0.0), v2_2(0.0), mu(0.0); 
                   quad_ptr_->x_derivative_interpolate(el_v1, v1_1);
                   quad_ptr_->y_derivative_interpolate(el_v1, v1_2);
                   quad_ptr_->x_derivative_interpolate(el_v2, v2_1);
                   quad_ptr_->y_derivative_interpolate(el_v2, v2_2);
                   quad_ptr_->interpolate(el_mu, mu);
              
                   double tau11 = 2.0/3.0*mu(2*v1_1 - v2_2);                        
                   double tau12 = mu(v1_2 + v2_1);                        
                        
                   RM_b_U_star(tau11, tau12);
                }
              }
            }            
         }
         m = m_;
         r = -r_;
     }
 



     
     
     
     


     //----------------------------------------U_star-----------------------------
     void RM_i_U_star(double U, double dU_dx, double dU_dy, double tau1, double tau2, double g, double v1, double v2, double v1_1, double v2_2)
     {               
         for (int b=0; b<nb_el_nodes_; b++)
         {
             r_[b] += -dt_*quad_ptr_->sh(b)*(v1*dU_dx + v1_1*U + v2*dU_dy + v2_2*U)*JxW_;
             r_[b] += -dt_*(quad_ptr_->dsh_dx(b)*tau1 + quad_ptr_->dsh_dy(b)*tau2)*JxW_;
             r_[b] +=  dt_*(quad_ptr_->sh(b)*rho_*g)*JxW_;
             
             r_[b] += 0.5*dt_*dt_* (v1*quad_ptr_->dsh_dx(b) + v1_1*quad_ptr_->sh(b) + v2*quad_ptr_->dsh_dy(b) + v2_2*quad_ptr_->sh(b))
                                 * (-(v1*dU_dx + v1_1*U + v2*dU_dy + v2_2*U) + rho_*g)*JxW_;                                 
         }
         
         for(int b=0; b<nb_el_nodes_; b++)
         for(int a=0; a<nb_el_nodes_; a++)
         {
             m_(b,a) += quad_ptr_->sh(b)*quad_ptr_->sh(a)*JxW_;
         }
     }


     void RM_b_U_star(double tau1, double tau2)
     {
         for(int b=0; b<nb_el_nodes_; b++)
             r_[b] += dt_*quad_ptr_->sh(b)*(tau1*JNxW_[0] + tau2*JNxW_[1]);
     }



     //------------U1_star------------------------------------
     RM_i_U_star(U1, dU1_dx, dU1_dy, tau11, tau12, gravity_[0]);
     RM_b_U_star(tau11, tau12);

     //------------U2_star------------------------------------
     RM_i_U_star(U2, dU2_dx, dU2_dy, tau12, tau22, gravity_[1]);
     RM_b_U_star(tau11, tau12);













     //----------------------------------------delta_p-----------------------------
     void RM_i_p()
     {               
         for (int b=0; b<nb_el_nodes_; b++)
         {
             r_[b] +=  dt_*(quad_ptr_->dsh_dx(b)*U1 + quad_ptr_->dsh_dy(b)*U2) *JxW_;
             r_[b] +=  dt_*theta1*(quad_ptr_->dsh_dx(b)*U1_star + quad_ptr_->dsh_dy(b)*U2_star) *JxW_;
             r_[b] += -dt_*dt_*theta1*(quad_ptr_->dsh_dx(b)*dp_dx + quad_ptr_->dsh_dy(b)*dp_dy)         
         }
         
         for(int b=0; b<nb_el_nodes_; b++)
         for(int a=0; a<nb_el_nodes_; a++)
         {
             m_(b,a) += quad_ptr_->sh(b)*quad_ptr_->sh(a)/(sound_speed_*sound_speed_)*JxW_;
             m_(b,a) += dt_*dt*theta1*theta2*(quad_ptr_->dsh_dx(b)*quad_ptr_->dsh_dx(a) +  quad_ptr_->dsh_dy(b)*quad_ptr_->dsh_dy(a))*JxW_;
         }
     }




     void RM_b_p()
     {
         for (int b=0; b<nb_el_nodes_; b++)
         {
             r_[b] +=  -dt_*quad_ptr_->sh(b)*(U1*JNxW_[0] + U2*JNxW_[1]);
             r_[b] +=  -dt_*theta1*quad_ptr_->sh(b)*(U1_star*JNxW_[0] + U2_star*JNxW_[1]);
             r_[b] +=  dt_*dt_*theta1*quad_ptr_->sh(b)*(dp_dx*JNxW_[0] + dp_dy*JNxW_[1]);
         }            

         for(int b=0; b<nb_el_nodes_; b++)
         for(int a=0; a<nb_el_nodes_; a++)
         {
             m_(b,a) -= dt_*dt*theta1*theta2*quad_ptr_->sh(b)*(quad_ptr_->dsh_dx(a)*JNxW_[0] + quad_ptr_->dsh_dy(a)*JNxW_[1]);
         }
     }








      

     //----------------------------------------U1_star_star-----------------------------
     void RM_i_U_star_star(double dp_dx, double ddelta_p_dx)
     {               
         for (int b=0; b<nb_el_nodes_; b++)
         {
             r_[b] += -dt_*quad_ptr_->sh(b)*(dp_dx + theta2*ddelta_p_dx)*JxW_;
             r_[b] -= 0.5*dt_*dt_*(v1*quad_ptr_->dsh_dx(b) + v1_1*quad_ptr_->sh(b) + v2*quad_ptr_->dsh_dy(b) + v2_2*quad_ptr_->sh(b))*dp_dx*JxW_;
         }
         
         for(int b=0; b<nb_el_nodes_; b++)
         for(int a=0; a<nb_el_nodes_; a++)
         {
             m_(b,a) += quad_ptr_->sh(b)*quad_ptr_->sh(a)*JxW_;
         }
     }

     //------------U1_star------------------------------------
      RM_i_U_star_star(dp_dx, ddelta_p_dx);
     //------------U2_star------------------------------------
      RM_i_U_star_star(dp_dy, ddelta_p_dy);







    
    vec N_u()
    {
       for(int i=0; i<nb_el_nodes_; i++)
         N_u_[i] = quad_ptr_->sh(i);
    }


    mat M_u()
    {
       using boost::numeric::ublas;
       M_u_ = outer_prod(N_u_,N_u_);
    }

    mat B(const vec& u)
    {
       for(int i=0; i<nb_el_nodes_; i++)
       {
          B_(0, 2*i) = quad_ptr_->dsh_dx(i);
          B_(1, 2*i+1) = quad_ptr_->dsh_dy(i);
          B_(2, 2*i) = quad_ptr_->dsh_dy(i);
          B_(2, 2*i+1) = quad_ptr_->dsh_dx(i);
       }       
    }

    mat I0_mmTrans()
    {
         I0_mmTrans_(0,0) = 4.0/3.0;  I0_mmTrans_(0,1) = -2.0/3.0;
         I0_mmTrans_(1,0) = -2.0/3.0;  I0_mmTrans_(1,1) = 4.0/3.0;
         I0_mmTrans_(2,2) = 1.0;
    }


    


    mat C_u(const vec& u)
    {
       using boost::numeric::ublas;
       C_u_ = outer_prod(u,N_u_);
    }





 private:

     mat I0_mmTrans_(3,3,0.0);

     vec N_u_(nb_el_nodes_, 0.0);
     mat M_u_(nb_el_nodes_, nb_el_nodes_, 0.0);
     mat B_(3,2*nb_el_nodes_,0.0);
     mat K_tau_()
     
     mat C_u_();


     int nb_el_nodes_;
     int nb_dof_fluid_;

     ic_bc_type& ic_bc_;
     fluid_properties& props_;
     read_setup& setup_;

     double JxW_; 
     vec JNxW_;
     double dt_;

     vec P_orig_fluid_;
     vec I_orig_fluid_;
     vec P_;
     vec I_;
     vec dI_dx_;
     vec dI_dy_;

     double rho_;
     double mu_;
     double cp_, cv_;
     double kappa_;
     double sound_speed_;
     double beta_, alpha_;
     double qL_dp_, qL_dT_;

     vec U_P_;
     vec U_;
     vec F1_adv_;
     vec F2_adv_;
     vec F1_dif_;
     vec F2_dif_;
     vec S_;

     vec N_v_;
     mat M_v_;
     mat C_v_;
     mat B_;
     mat I0_;


     vec gravity_;

     vec  r_;
     mat  m_;

     double tol_;
     std::shared_ptr<quad> quad_ptr_ = nullptr;
     point<2> dummy_;


  };



}/* namespace GALES */
#endif
