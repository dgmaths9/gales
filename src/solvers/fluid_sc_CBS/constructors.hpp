

  

  //------------------ mesh building --------------------------------------------------------------------
  print_only_pid<0>(std::cerr)<< "FLUID MESH\n";
  double mesh_read_start = MPI_Wtime();
  Mesh<dim> one_dof_mesh(setup.fluid_mesh_file(), 1, setup);
  print_only_pid<0>(std::cerr)<<"Mesh reading took: "<<std::setprecision(setup.precision()) << MPI_Wtime()-mesh_read_start<<" s\n\n";


  



  //-----------------fluid props------------------------------------------
  fluid_properties fluid_props;

  //-----------------------------------------maps-------------------------------------------
  epetra_maps maps(one_dof_mesh);

  
  //----------------------------------------dofs state-----------------------------------------------------  
  auto p_dof_state = std::make_unique<dof_state>(maps.state_map()->NumMyElements(), 2);
  auto delta_p_dof_state = std::make_unique<dof_state>(maps.state_map()->NumMyElements(), 2);
  auto v1_dof_state = std::make_unique<dof_state>(maps.state_map()->NumMyElements(), 2);
  auto v2_dof_state = std::make_unique<dof_state>(maps.state_map()->NumMyElements(), 2);
  auto U1_dof_state = std::make_unique<dof_state>(maps.state_map()->NumMyElements(), 2);
  auto U2_dof_state = std::make_unique<dof_state>(maps.state_map()->NumMyElements(), 2);
  auto U1_star_dof_state = std::make_unique<dof_state>(maps.state_map()->NumMyElements(), 2);
  auto U2_star_dof_state = std::make_unique<dof_state>(maps.state_map()->NumMyElements(), 2);
  auto U1_star_star_dof_state = std::make_unique<dof_state>(maps.state_map()->NumMyElements(), 2);
  auto U2_star_star_dof_state = std::make_unique<dof_state>(maps.state_map()->NumMyElements(), 2);
  
  
  
  //---------------------------------------model--------------------------------------------
  model<dim> p_model(one_dof_mesh, *p_dof_state, maps, setup);  
  model<dim> delta_p_model(one_dof_mesh, *delta_p_dof_state, maps, setup);  
  model<dim> v1_model(one_dof_mesh, *v1_dof_state, maps, setup);  
  model<dim> v2_model(one_dof_mesh, *v2_dof_state, maps, setup);    
  model<dim> U1_model(one_dof_mesh, *U1_dof_state, maps, setup);  
  model<dim> U2_model(one_dof_mesh, *U2_dof_state, maps, setup);  
  model<dim> U1_star_model(one_dof_mesh, *U1_star_dof_state, maps, setup);  
  model<dim> U2_star_model(one_dof_mesh, *U2_star_dof_state, maps, setup);  
  model<dim> U1_star_star_model(one_dof_mesh, *U1_star_star_dof_state, maps, setup);  
  model<dim> U2_star_star_model(one_dof_mesh, *U2_star_star_dof_state, maps, setup);  


  //----------------------------------------ic bc-------------------------------------------
  using fluid_ic_bc_type = fluid_ic_bc<dim>;
  fluid_ic_bc_type fluid_ic_bc;
 
 
  //----------------------------------------dofs ic bc------------------------------------------
  using fluid_dofs_ic_bc_type = fluid_dofs_ic_bc<fluid_ic_bc_type, dim>;
  fluid_dofs_ic_bc_type fluid_dofs_ic_bc(p_model, T_model, v1_model, v2_model, fluid_ic_bc);
  
  
  
  
  //--------------pressure profile-----------------------------------------------------
  pressure_profile_sc<fluid_ic_bc_type, dim> pressure_profile(fluid_ic_bc, p_model, fluid_props);
  
  if(fluid_ic_bc.custom_pressure_profile())
     fluid_ic_bc.set_custom_pressure_profile(p_model, fluid_props);
    
    
    
  //---------------------updater------------------------------------------------------
  dofs_updater fluid_updater(setup);
      
  //--------------------linear system-------------------------------------------------
  linear_system p_lp(p_model);
  linear_system T_lp(T_model);
  linear_system v_lp(v_model);
  
        
  //--------------------linear solver----------------------------------------------------
  belos_linear_solver ls_solver(setup); 

  //---------------------non linear residual check-------------------------------------------
  residual_check res_check;
  
  //------------------------integral---------------------------------------------------------  
  using fluid_integral_type = fluid<fluid_ic_bc_type, dim>;
  fluid_integral_type fluid_integral(fluid_ic_bc, fluid_props, p_model, T_model, v_model); 

  //-------------------------assembly--------------------------------------------------------
  fluid_assembly<fluid_integral_type, fluid_dofs_ic_bc_type, dim> fluid_assembly;

  // ------------------parallel I/O---------------------------------------------------  
  IO p_io(*p_maps.state_map(), *p_maps.dof_map());
  IO T_io(*T_maps.state_map(), *T_maps.dof_map());
  IO v_io(*v_maps.state_map(), *v_maps.dof_map());


