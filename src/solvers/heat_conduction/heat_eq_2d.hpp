#ifndef HEAT_EQ_2D_HPP
#define HEAT_EQ_2D_HPP




#include "../../fem/fem.hpp"



namespace GALES{
 
   
   
   /*
   
       For heat equation, we use stabilized finite element formulation from
       
       Enriched finite element spaces for transient conduction heat transfer (Hackem E. et al) 
       Applied Mathematics and Computation Volume 217, Issue 8, 15 December 2010, Pages 3929-3943
       (https://doi.org/10.1016/j.amc.2010.09.057)
   
   */
      
   


  template<typename ic_bc_type, int dim> class heat_eq{};


   
  template<typename ic_bc_type>
  class heat_eq<ic_bc_type, 2>
  {
    using element_type = element<2>;
    using vec = boost::numeric::ublas::vector<double>;
    using mat = boost::numeric::ublas::matrix<double>;


   public :  
 
      heat_eq(ic_bc_type& ic_bc, heat_eq_properties& props, model<2>& model)
      :
      ic_bc_(ic_bc),
      props_(props),
      setup_(model.setup()),

      JNxW_(2,0.0),
      JxW_(0.0),
      dt_(0.0), 

      P_(0.0),
      I_(0.0),

      dI_dx_(0.0),
      dI_dy_(0.0),      

      dP_dx_(0.0),
      dP_dy_(0.0)            
      {
        alpha_m_ = 0.5*(3.0-setup_.rho_inf())/(1.0+setup_.rho_inf());
        alpha_f_ = 1.0/(1.0+setup_.rho_inf());
        gamma_ = 0.5 + alpha_m_ - alpha_f_;

        if(props_.get_material_type() == "custom")
        {
          props_.properties();
          rho_ = props_.rho();
          cp_ = props_.cp();
          kappa_ = props_.kappa();
        }
      } 

  
  
  



     // This is for Euler-backward method
    void execute(const element_type& el, const std::vector<vec>& dofs_T, mat& m, vec& r)
    {
      nb_el_nodes_ = el.nb_nodes();      
      dt_= time::get().delta_t();

      vec P_dofs = dofs_T[1];
      vec I_dofs = dofs_T[0];
      
      for(int i=0; i<el.quad_i().size(); i++)
      {
        quad_ptr_ = el.quad_i()[i];
        JxW_ = quad_ptr_->JxW();

        quad_ptr_->interpolate(P_dofs, P_);
        quad_ptr_->interpolate(I_dofs, I_);
        quad_ptr_->x_derivative_interpolate(I_dofs, dI_dx_);
        quad_ptr_->y_derivative_interpolate(I_dofs, dI_dy_);
        quad_ptr_->x_derivative_interpolate(P_dofs, dP_dx_);
        quad_ptr_->y_derivative_interpolate(P_dofs, dP_dy_);

        if(props_.get_material_type() == "magma_krafla")
        {
          double y_coord = el.el_node_vec(i)->get_y(); 
          props_.properties(y_coord, I_);
          rho_ = props_.rho();
          cp_ = props_.cp();
          kappa_ = props_.kappa();
        }
        
        

        double h = el.min_h();
        double xi_k = (rho_*cp_*h*h + 6*kappa_*dt_)/6*kappa_*dt_;
        double Res = rho_*cp_*(I_-P_)/dt_;
        double denom = kappa_*sqrt(dI_dx_*dI_dx_ + dI_dy_*dI_dy_);
        double alpha = h*rho_*cp_*std::abs(Res)/denom;
        if(alpha < 1.0) xi_k = 1.0;

        for (int b=0; b<nb_el_nodes_; b++)
        {
             r[b] += (quad_ptr_->sh(b)*rho_*cp_*(I_-P_)/dt_
                   + quad_ptr_->dsh_dx(b)*kappa_*(xi_k*dI_dx_ + (1.0-xi_k)*dP_dx_)
                   + quad_ptr_->dsh_dy(b)*kappa_*(xi_k*dI_dy_ + (1.0-xi_k)*dP_dy_))*JxW_;
        }


        for(int b=0; b<nb_el_nodes_; b++)
         for(int a=0; a<nb_el_nodes_; a++)
         {
             m(b,a) +=  (quad_ptr_->sh(b)*rho_*cp_*quad_ptr_->sh(a)/dt_
                     + quad_ptr_->dsh_dx(b)*kappa_*xi_k*quad_ptr_->dsh_dx(a)
                     + quad_ptr_->dsh_dy(b)*kappa_*xi_k*quad_ptr_->dsh_dy(a))*JxW_;
         }
      }
      
      if(el.on_boundary())
      {
         for(int i=0; i<el.nb_sides(); i++)
         {
           if(el.is_side_on_boundary(i))
           {
             auto bd_nodes = el.side_nodes(i);
             auto side_flag = el.side_flag(i);
   
             if(side_flag==1)
             {            
               std::vector<std::vector<double>> f_heat_flux;
               ic_bc_.get_fluid_heat_flux(bd_nodes, f_heat_flux);            
               
               for(int j=0; j<el.nb_side_gp(i); j++)
               {
                 quad_ptr_ = el.quad_b(i,j);
                 
                 vec q(2,0.0);
                 q[0] = -f_heat_flux[j][0];
                 q[1] = -f_heat_flux[j][1];
               
                 for (int b=0; b<nb_el_nodes_; b++)
                   r[b] += quad_ptr_->sh(b)*(q[0]+q[1])*quad_ptr_->W_bd();
               }
             }
             else
             {          
               vec q(2, 0.0);
               if(ic_bc_.neumann_q1(bd_nodes, side_flag).first)    
                   q[0] = ic_bc_.neumann_q1(bd_nodes, side_flag).second;
               if(ic_bc_.neumann_q2(bd_nodes, side_flag).first)    
                   q[1] = ic_bc_.neumann_q2(bd_nodes, side_flag).second;
   
               for(const auto& quad_ptr: el.quad_b(i))
               {
                   quad_ptr_ = quad_ptr;               
                   JNxW_ = quad_ptr_->JNxW();
   
                   for (int b=0; b<nb_el_nodes_; b++)
                     r[b] += quad_ptr_->sh(b)*(q[0]*JNxW_[0] + q[1]*JNxW_[1]);
               }
             }                   
           }
         }            
      }

      r *= -1.0;      
    }








     // this is for generalized-alpha method
    void execute(const element_type& el, const std::vector<vec>& dofs_T, mat& m, vec& r, const std::vector<vec>& dofs_T_dot)
    {
      nb_el_nodes_ = el.nb_nodes();      
      dt_= time::get().delta_t();

      vec P_dofs = dofs_T[1];
      vec I_dofs = dofs_T[1] + alpha_f_*(dofs_T[0]-dofs_T[1]);
      vec I_dot_dofs = dofs_T_dot[1] + alpha_m_*(dofs_T_dot[0]-dofs_T_dot[1]);
      
      for(int i=0; i<el.quad_i().size(); i++)
      {
        quad_ptr_ = el.quad_i()[i];
        JxW_ = quad_ptr_->JxW();

        quad_ptr_->interpolate(P_dofs, P_);
        quad_ptr_->interpolate(I_dofs, I_);
        quad_ptr_->interpolate(I_dot_dofs, I_dot_);
        quad_ptr_->x_derivative_interpolate(I_dofs, dI_dx_);
        quad_ptr_->y_derivative_interpolate(I_dofs, dI_dy_);
        quad_ptr_->x_derivative_interpolate(P_dofs, dP_dx_);
        quad_ptr_->y_derivative_interpolate(P_dofs, dP_dy_);

        if(props_.get_material_type() == "magma_krafla")
        {
          double y_coord = el.el_node_vec(i)->get_y(); 
          props_.properties(y_coord, I_);
          rho_ = props_.rho();
          cp_ = props_.cp();
          kappa_ = props_.kappa();
        }
        

        for (int b=0; b<nb_el_nodes_; b++)
        {
             r[b] += (quad_ptr_->sh(b)*rho_*cp_*I_dot_
                   + quad_ptr_->dsh_dx(b)*kappa_*dI_dx_
                   + quad_ptr_->dsh_dy(b)*kappa_*dI_dy_)*JxW_;
        }


        for(int b=0; b<nb_el_nodes_; b++)
         for(int a=0; a<nb_el_nodes_; a++)
         {
             m(b,a) +=  alpha_m_*quad_ptr_->sh(b)*rho_*cp_*quad_ptr_->sh(a)*JxW_;
             m(b,a) +=  alpha_f_* gamma_*dt_*(quad_ptr_->dsh_dx(b)*kappa_*quad_ptr_->dsh_dx(a)
                                            + quad_ptr_->dsh_dy(b)*kappa_*quad_ptr_->dsh_dy(a))*JxW_;
         }
      }
      
      if(el.on_boundary())
      {
         for(int i=0; i<el.nb_sides(); i++)
         {
           if(el.is_side_on_boundary(i))
           {
             auto bd_nodes = el.side_nodes(i);
             auto side_flag = el.side_flag(i);
   
             if(side_flag==1)
             {            
               std::vector<std::vector<double>> f_heat_flux;
               ic_bc_.get_fluid_heat_flux(bd_nodes, f_heat_flux);            
               
               for(int j=0; j<el.nb_side_gp(i); j++)
               {
                 quad_ptr_ = el.quad_b(i,j);
   
                 vec q(2,0.0);
                 q[0] = -f_heat_flux[j][0];
                 q[1] = -f_heat_flux[j][1];
               
                 for (int b=0; b<nb_el_nodes_; b++)
                   r[b] += quad_ptr_->sh(b)*(q[0]+q[1])*quad_ptr_->W_bd();
   
               }
             }
             else
             {          
               vec q(2, 0.0);
               if(ic_bc_.neumann_q1(bd_nodes, side_flag).first)    
                  q[0] = ic_bc_.neumann_q1(bd_nodes, side_flag).second;
               if(ic_bc_.neumann_q2(bd_nodes, side_flag).first)    
                  q[1] = ic_bc_.neumann_q2(bd_nodes, side_flag).second;
   
               for(const auto& quad_ptr: el.quad_b(i))
               {
                   quad_ptr_ = quad_ptr;               
                   JNxW_ = quad_ptr_->JNxW();   
                   for (int b=0; b<nb_el_nodes_; b++)
                     r[b] += quad_ptr_->sh(b)*(q[0]*JNxW_[0] + q[1]*JNxW_[1]);
               }
             }                   
           }
         }               
      }
      
      r *= -1.0;      
    }






   private:

    int nb_el_nodes_;
    
    ic_bc_type& ic_bc_;
    heat_eq_properties& props_;
    read_setup& setup_;

    double JxW_; 
    vec JNxW_;
    double dt_;

    double rho_, cp_, kappa_;

    double P_, I_, I_dot_;
    double dI_dx_, dI_dy_;
    double dP_dx_, dP_dy_;
    
    vec r_;
    mat m_;


    std::shared_ptr<quad> quad_ptr_ = nullptr;
    point<2> dummy_;    
    double alpha_f_, alpha_m_, gamma_;
  }; 






 
} 

#endif

