#include <mpi.h>
#include <iostream>
 

#include "../../fem/fem.hpp"
#include "fluid.hpp"





using namespace GALES;




template<int dim>
void solver(read_setup& setup)
{
  #include "constructors.hpp"

  
  
  

  time::get().t(0.0);
  time::get().delta_t(setup.delta_t());
    
  double restart_or_t_zero = MPI_Wtime();  
  if(!setup.restart())
  {
    //--------- setting up dofs for time = 0 ----------------
    fluid_dofs_ic_bc.dirichlet_bc();
    fluid_io.write("fluid_dofs/", *fluid_dof_state);
    print_only_pid<0>(std::cerr)<<"Dofs writing for time 0 took: "<<std::setprecision(setup.precision()) << MPI_Wtime()-restart_or_t_zero<<" s\n\n";       
  }
  else
  {
    //--------------read restart-----------------------------
    time::get().t(setup.restart_time());
    fluid_io.read("fluid_dofs/", *fluid_dof_state);
    print_only_pid<0>(std::cerr)<<"Dofs reading at restart time " << time::get().t() << " took: "<<std::setprecision(setup.precision()) << MPI_Wtime()-restart_or_t_zero<<" s\n\n";       
  }
  time::get().tick();




  // --------------------------------TIME LOOP-----------------------------------------------------------
  double time_loop_start = MPI_Wtime();
  double non_linear_res(0.0);
  int tot_time_iterations(0);

  for(; time::get().t()<=setup.end_time(); time::get().tick())
  {
    double t_iteration(MPI_Wtime());
    
    print_only_pid<0>(std::cerr)<<"simulation time:  "<<std::setprecision(setup.precision()) << time::get().t()<<" s\n";




      fluid_updater.predictor(*fluid_dof_state);



      fluid_dofs_ic_bc.dirichlet_bc();



      for(int it_count=0; it_count < setup.n_max_it(); it_count++)   //fluid iterations loop
      {
        //------------------------ SOLUTION For fluid ------------------------
        double fill_time = fluid_assembly.execute(fluid_integral, fluid_dofs_ic_bc, fluid_model, fluid_lp);


        bool residual_converged = fluid_res_check.execute(*fluid_lp.rhs(), it_count, non_linear_res);      
        if(residual_converged)
        {
          print_solver_info("FLUID  ", it_count, fill_time, non_linear_res);
          break;
        } 	              
        double solver_time = fluid_ls_solver.execute(fluid_lp);


        fluid_updater.corrector(*fluid_dof_state, fluid_io.state_vec(*fluid_ls_solver.solution()));


        print_solver_info("FLUID  ", it_count, fill_time, non_linear_res, solver_time, fluid_ls_solver);        

        // -----------------------end of fluid solution---------------------------------------------------------
       }
       



    if((tot_time_iterations+1)%(setup.print_freq()) == 0)
    {
       fluid_io.write("fluid_dofs/", *fluid_dof_state);
    }
       


     //-------- adaptive time step-----------------
     if(setup.adaptive_time_step() == true && tot_time_iterations > setup.N())
     {
        adaptive_time_f_sc_mc<dim>(fluid_model, fluid_props, setup);
     }



    print_only_pid<0>(std::cerr)<<"Time step took: "<<MPI_Wtime()-t_iteration<<" s\n\n";
    tot_time_iterations++;    	
  }

  
  print_only_pid<0>(std::cerr)<<"End time reached!!!! "<<"\n\n";
  print_only_pid<0>(std::cerr)<<"Total run time: "<<MPI_Wtime()-time_loop_start<<" s\n\n\n";

}













int main(int argc, char* argv[])
{

  MPI_Init(&argc, &argv);



  //---------------- read setup file -------------------------------
  read_setup setup;



  //---------- creating directories ----------------------------------------------------------------------
  make_dirs("results/fluid_dofs");




 
  if(setup.dim()==2)  solver<2>(setup);
//  else if(setup.dim()==3)  solver<3>(setup);

  MPI_Barrier(MPI_COMM_WORLD);
  MPI_Finalize();
  return 0;
}
