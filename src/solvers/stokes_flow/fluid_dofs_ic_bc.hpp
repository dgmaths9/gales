#ifndef _GALES_FLUID_DOFS_IC_BC_HPP_
#define _GALES_FLUID_DOFS_IC_BC_HPP_


#include"../../fem/fem.hpp"



namespace GALES {



  template<typename ic_bc_type, int dim>
  class fluid_dofs_ic_bc{};






  template<typename ic_bc_type>
  class fluid_dofs_ic_bc <ic_bc_type, 2>
  {
      using model_type = model<2>;
      
      public:
      
      fluid_dofs_ic_bc(model_type& model, ic_bc_type& ic_bc, model_type& dot_model)
      :  
      model_(model), ic_bc_(ic_bc), dot_model_(dot_model)
      {
        ic();
      }


    void ic()
    {
      for(const auto& nd : model_.mesh().nodes())
      {
        model_.state().set_dof(nd->first_dof_lid(), ic_bc_.initial_p(*nd));
        model_.state().set_dof(nd->first_dof_lid()+1, ic_bc_.initial_vx(*nd));
        model_.state().set_dof(nd->first_dof_lid()+2, ic_bc_.initial_vy(*nd));
      }      
    }




     
     void update_models(const node<2>& nd, int dof_indx, double x)
     {
        model_.state().set_dof(nd.first_dof_lid()+dof_indx, x);
     }
     


      void dirichlet_bc()
      {
        std::pair<bool,double> result;
        const double dt(time::get().delta_t());  

        for(const auto& nd : model_.mesh().nodes())
        {
          result = ic_bc_.dirichlet_p(*nd);
          if (result.first) update_models(*nd, 0, result.second);    
           
          result= ic_bc_.dirichlet_vx(*nd);
          if (result.first) update_models(*nd, 1, result.second);          

          result= ic_bc_.dirichlet_vy(*nd);
          if (result.first) update_models(*nd, 2, result.second);           
        }
      }






    auto dofs_constraints(const node<2>& nd)const  
    {
      std::vector<std::pair<bool,double>> nd_dofs_constraints(nd.nb_dofs(), std::make_pair(false, 0.0));              

      if(ic_bc_.dirichlet_p(nd).first) nd_dofs_constraints[0] = std::make_pair(true, 0.0);
      if(ic_bc_.dirichlet_vx(nd).first) nd_dofs_constraints[1] = std::make_pair(true, 0.0);
      if(ic_bc_.dirichlet_vy(nd).first) nd_dofs_constraints[2] = std::make_pair(true, 0.0);
      
      return nd_dofs_constraints;
    }

 
    private:
    model_type& model_;
    ic_bc_type& ic_bc_;
    model_type& dot_model_;
    double gamma_;
  };












  template<typename ic_bc_type>
  class fluid_dofs_ic_bc <ic_bc_type, 3>
  {
      using model_type = model<3>;

      public:
      
      fluid_dofs_ic_bc(model_type& model, ic_bc_type& ic_bc, model_type& dot_model)
      :  
      model_(model), ic_bc_(ic_bc), dot_model_(dot_model)
      {
        ic();
      }


      void ic()
      {
        for(const auto& nd : model_.mesh().nodes())
        {
          model_.state().set_dof(nd->first_dof_lid(), ic_bc_.initial_p(*nd));
          model_.state().set_dof(nd->first_dof_lid()+1, ic_bc_.initial_vx(*nd));
          model_.state().set_dof(nd->first_dof_lid()+2, ic_bc_.initial_vy(*nd));
          model_.state().set_dof(nd->first_dof_lid()+3, ic_bc_.initial_vz(*nd));
        }
      }






     void update_models(const node<3>& nd, int dof_indx, double x)
     {
        model_.state().set_dof(nd.first_dof_lid()+dof_indx, x);
     }




      void dirichlet_bc()
      {
        std::pair<bool,double> result;
        const double dt(time::get().delta_t());  

        for(const auto& nd : model_.mesh().nodes())
        {
          result = ic_bc_.dirichlet_p(*nd);
          if (result.first) update_models(*nd, 0, result.second);          
           
          result= ic_bc_.dirichlet_vx(*nd);
          if (result.first) update_models(*nd, 1, result.second);           

          result= ic_bc_.dirichlet_vy(*nd);
          if (result.first) update_models(*nd, 2, result.second);             
        

          result= ic_bc_.dirichlet_vz(*nd);
          if (result.first) update_models(*nd, 3, result.second);             
        }
      }




    auto dofs_constraints(const node<3>& nd)const  
    {
      std::vector<std::pair<bool,double>> nd_dofs_constraints(nd.nb_dofs(), std::make_pair(false, 0.0));              

      if(ic_bc_.dirichlet_p(nd).first) nd_dofs_constraints[0] = std::make_pair(true, 0.0);
      if(ic_bc_.dirichlet_vx(nd).first) nd_dofs_constraints[1] = std::make_pair(true, 0.0);
      if(ic_bc_.dirichlet_vy(nd).first) nd_dofs_constraints[2] = std::make_pair(true, 0.0);
      if(ic_bc_.dirichlet_vz(nd).first) nd_dofs_constraints[3] = std::make_pair(true, 0.0);

      return nd_dofs_constraints;
    }


    private:
    model_type& model_;
    model_type& dot_model_;
    ic_bc_type& ic_bc_;
    double gamma_;
  };




} /* namespace GALES */

#endif

