#ifndef _FLUID_3D_HPP_
#define _FLUID_3D_HPP_



#include "../../fem/fem.hpp"




namespace GALES
{


    






  template<typename ic_bc_type>
  class fluid<ic_bc_type, 3>
  {
    using element_type = element<3>;
    using vec = boost::numeric::ublas::vector<double>;
    using mat = boost::numeric::ublas::matrix<double>;

    public :

    fluid(ic_bc_type& ic_bc, fluid_properties& props, model<3>& model)
    :
    nb_dof_fluid_(model.mesh().nb_nd_dofs()),
 
    ic_bc_(ic_bc),
    props_(props),
    setup_(model.setup()),

    JNxW_(3,0.0),
    JxW_(0.0),
    dt_(0.0),

    I_dot_(nb_dof_fluid_,0.0),
    P_(nb_dof_fluid_,0.0),
    I_(nb_dof_fluid_,0.0),
    dI_dx_(nb_dof_fluid_,0.0),
    dI_dy_(nb_dof_fluid_,0.0),
    dI_dz_(nb_dof_fluid_,0.0),

    mesh_v_(3,0.0),

    alpha_(0.0),
    beta_(0.0),
    rho_(0.0),
    mu_(0.0),
    cp_(0.0),
    cv_(0.0),
    kappa_(0.0),
    sound_speed_(0.0),
    qL_dp_(0.0), 
    qL_dT_(0.0),

    U_P_(nb_dof_fluid_,0.0),
    U_(nb_dof_fluid_,0.0),
    F1_adv_(nb_dof_fluid_,0.0),
    F2_adv_(nb_dof_fluid_,0.0),
    F3_adv_(nb_dof_fluid_,0.0),
    F1_dif_(nb_dof_fluid_,0.0),
    F2_dif_(nb_dof_fluid_,0.0),
    F3_dif_(nb_dof_fluid_,0.0),
    S_(nb_dof_fluid_,0.0),

    DUDY_(nb_dof_fluid_,nb_dof_fluid_,0.0),
    A1_(nb_dof_fluid_,nb_dof_fluid_,0.0),
    A2_(nb_dof_fluid_,nb_dof_fluid_,0.0),
    A3_(nb_dof_fluid_,nb_dof_fluid_,0.0),
    K11_(nb_dof_fluid_,nb_dof_fluid_,0.0),
    K12_(nb_dof_fluid_,nb_dof_fluid_,0.0),
    K13_(nb_dof_fluid_,nb_dof_fluid_,0.0),
    K21_(nb_dof_fluid_,nb_dof_fluid_,0.0),
    K22_(nb_dof_fluid_,nb_dof_fluid_,0.0),
    K23_(nb_dof_fluid_,nb_dof_fluid_,0.0),
    K31_(nb_dof_fluid_,nb_dof_fluid_,0.0),
    K32_(nb_dof_fluid_,nb_dof_fluid_,0.0),
    K33_(nb_dof_fluid_,nb_dof_fluid_,0.0),
    S0_(nb_dof_fluid_,nb_dof_fluid_,0.0),

    tau_(nb_dof_fluid_,nb_dof_fluid_,0.0),
    shock_matrix_(nb_dof_fluid_,nb_dof_fluid_,0.0),
    u_ref_inv_(nb_dof_fluid_, nb_dof_fluid_, 0.0),

    gravity_(3,0.0),

    tol_(std::numeric_limits<double>::epsilon())
    {
       ic_bc_.body_force(gravity_);
       if(setup_.dc_2006())  u_ref_inv();   

       alpha_m_ = 0.5*(3.0-setup_.rho_inf())/(1.0+setup_.rho_inf());
       alpha_f_ = 1.0/(1.0+setup_.rho_inf());
       gamma_ = 0.5 + alpha_m_ - alpha_f_;
    }





     void assign(const vec &v, double &p, double &vx, double &vy, double &vz, double &T)
     {
        p =  v[0];         vx = v[1];        vy = v[2];        vz = v[3];        T =  v[4];
     }





     void assign(const vec &v, double &p, double &vx, double &vy, double &vz)
     {
        p =  v[0];         vx = v[1];        vy = v[2];        vz = v[3];       
     }




     void u_ref_inv()
     {
         vec u_ref(nb_dof_fluid_,0.0);
         if(setup_.isothermal())
         {
            props_.properties(ic_bc_.p_ref(), 0.0);
            U(ic_bc_.p_ref(), ic_bc_.v1_ref(), ic_bc_.v2_ref(), ic_bc_.v3_ref(), u_ref);
         }
         else
         {
            props_.properties(ic_bc_.p_ref(), ic_bc_.T_ref(), 0.0);
            U(ic_bc_.p_ref(), ic_bc_.v1_ref(), ic_bc_.v2_ref(), ic_bc_.v3_ref(), ic_bc_.T_ref(), u_ref);
         }

         for(int i=0; i<nb_dof_fluid_; i++)
         {
             if(u_ref[i] < tol_)  u_ref_inv_(i,i) = 0.0;
             else u_ref_inv_(i,i) = 1.0/u_ref[i];
         }
     }
 
 
 



 
 
     // This is for Euler-backward method
    void get_dofs(const std::vector<vec>& dofs_fluid)
     {
         dt_= time::get().delta_t();
 
         P_orig_fluid_ = dofs_fluid[1];
         I_orig_fluid_ = dofs_fluid[0]; 
     }
 
 



    // This is for Euler-backward method with moving mesh
     void get_dofs_mm(const std::vector<vec>& dofs_elastostatic, const std::vector<vec>& dofs_fluid)
     {
         get_dofs(dofs_fluid);
         const vec I_orig_elastostatic = dofs_elastostatic[0];
 
         for(int i=0; i<nb_el_nodes_; i++)
         {
             I_ux_e_[i] = I_orig_elastostatic[3*i];
             I_uy_e_[i] = I_orig_elastostatic[3*i+1];
             I_uz_e_[i] = I_orig_elastostatic[3*i+2];
         }
     }




     // this is for generalized-alpha method
     void get_dofs(const std::vector<vec>& dofs_fluid, const std::vector<vec>& dofs_fluid_dot)
     {
         dt_= time::get().delta_t();
 
         P_orig_fluid_ = dofs_fluid[1];
         I_orig_fluid_ = dofs_fluid[1] + alpha_f_*(dofs_fluid[0]-dofs_fluid[1]); 
         I_orig_fluid_dot_ = dofs_fluid_dot[1] + alpha_m_*(dofs_fluid_dot[0]-dofs_fluid_dot[1]); 
     }





    // This is for generalized-alpha method with moving mesh
     void get_dofs_mm(const std::vector<vec>& dofs_elastostatic, const std::vector<vec>& dofs_fluid, const std::vector<vec>& dofs_fluid_dot)
     {
         get_dofs(dofs_fluid, dofs_fluid_dot);
         const vec I_orig_elastostatic = dofs_elastostatic[0];
 
         for(int i=0; i<nb_el_nodes_; i++)
         {
             I_ux_e_[i] = I_orig_elastostatic[3*i];
             I_uy_e_[i] = I_orig_elastostatic[3*i+1];
             I_uz_e_[i] = I_orig_elastostatic[3*i+2];
         }
     }
     
     

 
 
     void get_properties()
     {
         rho_ = props_.rho();
         mu_ = props_.mu();
         kappa_ = props_.kappa();
         sound_speed_ = props_.sound_speed();
         cv_ = props_.cv();
         cp_ = props_.cp();
         alpha_ = props_.alpha();
         beta_ = props_.beta();

         if(setup_.print_props())
         {
            if(setup_.isothermal()) props_.print_props_sc_isothermal();
            else props_.print_props_sc();
         }   
     }
 





     void interpolation()
     {
          quad_ptr_->interpolate(I_orig_fluid_,I_);
          quad_ptr_->x_derivative_interpolate(I_orig_fluid_,dI_dx_);
          quad_ptr_->y_derivative_interpolate(I_orig_fluid_,dI_dy_);     
          quad_ptr_->z_derivative_interpolate(I_orig_fluid_,dI_dz_);     
     }
 
 
 
 
 
    
     void U_P()
     {
              double p,vx,vy,vz,T;
              quad_ptr_->interpolate(P_orig_fluid_,P_);
              assign(P_,p,vx,vy,vz,T);
              props_.properties(p, T, 0.0);           
              U(p,vx,vy,vz,T, U_P_);     
     }
     



     void U_P_isothermal()
     {
              double p,vx,vy,vz;
              quad_ptr_->interpolate(P_orig_fluid_,P_);
              assign(P_,p,vx,vy,vz);
              props_.properties(p, 0.0);           
              U(p,vx,vy,vz, U_P_);     
     }
     


     
     //This is for element interior (isothermal case)
     void flux_matrices_and_vectors_isothermal()
     {
              double p,vx,vy,vz;
              assign(I_,p,vx,vy,vz);
              const double shear_rate = compute_shear_rate(dI_dx_, dI_dy_, dI_dz_);
              props_.properties(p, shear_rate);           
              get_properties();
              flux_matrices_and_vectors(p,vx,vy,vz);
              tau_matrix(vx,vy,vz);     
     }
 



     //This is for element interior
     void flux_matrices_and_vectors()
     {
              double p,vx,vy,vz,T;
              assign(I_,p,vx,vy,vz,T);
              const double shear_rate = compute_shear_rate(dI_dx_, dI_dy_, dI_dz_);
              props_.properties(p,T, shear_rate);           
              get_properties();
              flux_matrices_and_vectors(p,vx,vy,vz,T);
              tau_matrix(vx,vy,vz);     
     }
 




     //This is for boundary elements (isothermal case) 
     void flux_matrices_and_vectors_isothermal(const std::vector<int>& bd_nodes, int side_flag)
     {
              double p,vx,vy,vz;
              assign(I_,p,vx,vy,vz);
              const double shear_rate = compute_shear_rate(dI_dx_, dI_dy_, dI_dz_);
              props_.properties(p, shear_rate);           
              get_properties();
              flux_matrices_and_vectors(p,vx,vy,vz,bd_nodes, side_flag);
     }





     //This is for boundary elements      
     void flux_matrices_and_vectors(const std::vector<int>& bd_nodes, int side_flag)
     {
              double p,vx,vy,vz,T;
              assign(I_,p,vx,vy,vz,T);
              const double shear_rate = compute_shear_rate(dI_dx_, dI_dy_, dI_dz_);
              props_.properties(p,T, shear_rate);           
              get_properties();
              flux_matrices_and_vectors(p,vx,vy,vz,T,bd_nodes, side_flag);
     }
 
 






     void set_el_data_size()
     {
         I_orig_fluid_dot_.resize(nb_dof_fluid_*nb_el_nodes_, false);     
         P_orig_fluid_.resize(nb_dof_fluid_*nb_el_nodes_, false);
         I_orig_fluid_.resize(nb_dof_fluid_*nb_el_nodes_, false);

         I_ux_e_.resize(nb_el_nodes_, false);
         I_uy_e_.resize(nb_el_nodes_, false);
         I_uz_e_.resize(nb_el_nodes_, false);

         r_.resize(nb_dof_fluid_*nb_el_nodes_, false);
         m_.resize(nb_dof_fluid_*nb_el_nodes_, nb_dof_fluid_*nb_el_nodes_, false);
     }
    
    


 
 
 
     // This is for Euler-backward method
     void execute(const element_type& el, const std::vector<vec>& dofs_fluid, mat& m, vec& r)
     {
         nb_el_nodes_ = el.nb_nodes();      
         set_el_data_size();

         get_dofs(dofs_fluid);
         r_.clear();
         m_.clear();

         for(const auto& quad_ptr : el.quad_i())
         {
           quad_ptr_ = quad_ptr;
           JxW_ = quad_ptr_->JxW();   
           interpolation();
           if(setup_.isothermal())
           {
             U_P_isothermal();
             flux_matrices_and_vectors_isothermal();   
           }
           else
           {
             U_P();
             flux_matrices_and_vectors();                            
           }  
           RM_i();
         }

         if(el.on_boundary())
         {
            for(int i=0; i<el.nb_sides(); i++)
            {
              if(el.is_side_on_boundary(i))
              {
                auto bd_nodes = el.side_nodes(i);
                auto side_flag = el.side_flag(i);
                
                for(const auto& quad_ptr: el.quad_b(i))
                {
                  quad_ptr_ = quad_ptr;               
                  JNxW_ = quad_ptr_->JNxW();      
                  interpolation();
                  if(setup_.isothermal()) flux_matrices_and_vectors_isothermal(bd_nodes, side_flag);
                  else flux_matrices_and_vectors(bd_nodes, side_flag);
                  RM_b();
                }
              }
            }            
         }
         m = m_;
         r = -r_;
     }
 
 
 






     // This is for Euler-backward method with moving mesh (ALE)
     void execute(const element_type& el, const std::vector<vec>& dofs_elastostatic, const std::vector<vec>& dofs_fluid, mat& m, vec& r)
     {
         nb_el_nodes_ = el.nb_nodes();      
         set_el_data_size();

         get_dofs_mm(dofs_elastostatic, dofs_fluid);
         r_.clear();
         m_.clear();

         for(int i=0; i<el.nb_gp(); i++)
         {
           quad_ptr_ = std::make_unique<quad>(el, el.gp(i), el.W_in(i));
           JxW_ = quad_ptr_->JxW();
           mesh_v_ = quad_ptr_->mesh_v(I_ux_e_, I_uy_e_, I_uz_e_, dt_);          
           interpolation();
           if(setup_.isothermal())
           {
             U_P_isothermal();
             flux_matrices_and_vectors_isothermal();   
           }
           else
           {
             U_P();
             flux_matrices_and_vectors();                            
           }  
           RM_i();
         }
      
         if(el.on_boundary())
         {
            for(int i=0; i<el.nb_sides(); i++)
            {
              if(el.is_side_on_boundary(i))
              {
                auto bd_nodes = el.side_nodes(i);
                auto side_flag = el.side_flag(i);
                
                for(int j=0; j<el.nb_side_gp(i); j++)
                {
                  quad_ptr_ = std::make_unique<quad>(el, el.side_gp(i,j), dummy_, el.W_bd(i,j));               
                  JNxW_ = quad_ptr_->JNxW();   
                  interpolation();
                  if(setup_.isothermal()) flux_matrices_and_vectors_isothermal(bd_nodes, side_flag);
                  else flux_matrices_and_vectors(bd_nodes, side_flag);
                  RM_b();
                }
              }
            }            
         }         
         m = m_;
         r = -r_;
     }













     // This is for Euler-backward method (ALE); element interior calculation 
     void RM_i()
     {         
         const vec dU_dx = prod(DUDY_, dI_dx_);
         const vec dU_dy = prod(DUDY_, dI_dy_);
         const vec dU_dz = prod(DUDY_, dI_dz_);
         const vec dU_dt = prod(DUDY_, (I_-P_)/dt_);          //(U_ - U_P_)/dt_;
     
         for (int b=0; b<nb_el_nodes_; b++)
         for (int jb=0; jb<nb_dof_fluid_; jb++)
         {
             r_[nb_dof_fluid_*b+jb] += quad_ptr_->sh(b)*dU_dt[jb]*JxW_;
             r_[nb_dof_fluid_*b+jb] += quad_ptr_->dsh_dx(b)*(F1_dif_[jb]-F1_adv_[jb])*JxW_;
             r_[nb_dof_fluid_*b+jb] += quad_ptr_->dsh_dy(b)*(F2_dif_[jb]-F2_adv_[jb])*JxW_;
             r_[nb_dof_fluid_*b+jb] += quad_ptr_->dsh_dz(b)*(F3_dif_[jb]-F3_adv_[jb])*JxW_;
             r_[nb_dof_fluid_*b+jb] -= quad_ptr_->sh(b)*S_[jb]*JxW_;
             r_[nb_dof_fluid_*b+jb] -= quad_ptr_->sh(b)*(mesh_v_[0]*dU_dx[jb] + mesh_v_[1]*dU_dy[jb] + mesh_v_[2]*dU_dz[jb])*JxW_;
         }

         for(int b=0; b<nb_el_nodes_; b++)
         for(int jb=0; jb<nb_dof_fluid_; jb++)
         for(int a=0; a<nb_el_nodes_; a++)
         for(int ja=0; ja<nb_dof_fluid_; ja++)
         {
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += quad_ptr_->sh(b)* DUDY_(jb,ja) * quad_ptr_->sh(a)/dt_*JxW_;
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) -= quad_ptr_->dsh_dx(b)* A1_(jb,ja) * quad_ptr_->sh(a)*JxW_;
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) -= quad_ptr_->dsh_dy(b)* A2_(jb,ja) * quad_ptr_->sh(a)*JxW_;
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) -= quad_ptr_->dsh_dz(b)* A3_(jb,ja) * quad_ptr_->sh(a)*JxW_;
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += quad_ptr_->dsh_dx(b)* K11_(jb,ja) * quad_ptr_->dsh_dx(a)*JxW_;
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += quad_ptr_->dsh_dx(b)* K12_(jb,ja) * quad_ptr_->dsh_dy(a)*JxW_;
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += quad_ptr_->dsh_dx(b)* K13_(jb,ja) * quad_ptr_->dsh_dz(a)*JxW_;
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += quad_ptr_->dsh_dy(b)* K21_(jb,ja) * quad_ptr_->dsh_dx(a)*JxW_;
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += quad_ptr_->dsh_dy(b)* K22_(jb,ja) * quad_ptr_->dsh_dy(a)*JxW_;
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += quad_ptr_->dsh_dy(b)* K23_(jb,ja) * quad_ptr_->dsh_dz(a)*JxW_;
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += quad_ptr_->dsh_dz(b)* K31_(jb,ja) * quad_ptr_->dsh_dx(a)*JxW_;
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += quad_ptr_->dsh_dz(b)* K32_(jb,ja) * quad_ptr_->dsh_dy(a)*JxW_;
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += quad_ptr_->dsh_dz(b)* K33_(jb,ja) * quad_ptr_->dsh_dz(a)*JxW_;
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) -= quad_ptr_->sh(b)* S0_(jb,ja) * quad_ptr_->sh(a)*JxW_;
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) -= quad_ptr_->sh(b)* DUDY_(jb,ja) * (mesh_v_[0]*quad_ptr_->dsh_dx(a) 
                                                                                          + mesh_v_[1]*quad_ptr_->dsh_dy(a)
                                                                                          + mesh_v_[2]*quad_ptr_->dsh_dz(a))*JxW_;
         }



         //--------------------------------------------R_LeastSquares--------------------------------------------------------------------------        
         vec Res_vec = prod(A1_-mesh_v_[0]*DUDY_,dI_dx_) 
                     + prod(A2_-mesh_v_[1]*DUDY_,dI_dy_) 
                     + prod(A3_-mesh_v_[2]*DUDY_,dI_dz_) 
                     - S_;
                     
//         if(!setup_.steady_state()) Res_vec += dU_dt;
                          
         for(int b=0; b<nb_el_nodes_; b++)
         {
             const mat first_part = (A1_-mesh_v_[0]*DUDY_)*quad_ptr_->dsh_dx(b) 
                                  + (A2_-mesh_v_[1]*DUDY_)*quad_ptr_->dsh_dy(b) 
                                  + (A3_-mesh_v_[2]*DUDY_)*quad_ptr_->dsh_dz(b) 
                                  - S0_*quad_ptr_->sh(b);
             const mat first_part_tau = prod(first_part, tau_);
             const vec use = prod(first_part_tau, Res_vec);
             for(int jb=0; jb<nb_dof_fluid_; jb++)
             r_[nb_dof_fluid_*b+jb] += use[jb]*JxW_;
         }

         //--------------------------------------------M_LeastSquares--------------------------------------------------------------------------
         for(int a=0; a<nb_el_nodes_; a++)
         {
              mat Res_mat = (A1_-mesh_v_[0]*DUDY_)*quad_ptr_->dsh_dx(a) 
                          + (A2_-mesh_v_[1]*DUDY_)*quad_ptr_->dsh_dy(a) 
                          + (A3_-mesh_v_[2]*DUDY_)*quad_ptr_->dsh_dz(a) 
                          - S0_*quad_ptr_->sh(a);  
                                      
//             if(!setup_.steady_state())    Res_mat += DUDY_*quad_ptr_->sh(a)/dt_;             
                               
             for(int b=0; b<nb_el_nodes_; b++)
             {
                 const mat first_part = (A1_-mesh_v_[0]*DUDY_)*quad_ptr_->dsh_dx(b) 
                                      + (A2_-mesh_v_[1]*DUDY_)*quad_ptr_->dsh_dy(b) 
                                      + (A3_-mesh_v_[2]*DUDY_)*quad_ptr_->dsh_dz(b) 
                                      - S0_*quad_ptr_->sh(b);
                 const mat first_part_tau = prod(first_part, tau_);
                 const mat use = prod(first_part_tau, Res_mat);
                 for(int jb=0; jb<nb_dof_fluid_; jb++)
                 for(int ja=0; ja<nb_dof_fluid_; ja++)
                 m_(nb_dof_fluid_*b+jb, nb_dof_fluid_*a+ja) += use(jb,ja)*JxW_;
             }
         }




         //-----------------dc------------------------
         dc(dU_dx, dU_dy, dU_dz);

         if(setup_.dc_2006())
         {
             const vec app_1 = prod(shock_matrix_,dI_dx_);
             const vec app_2 = prod(shock_matrix_,dI_dy_);
             const vec app_3 = prod(shock_matrix_,dI_dz_);

             for(int b=0;b<nb_el_nodes_;b++)
             for(int jb=0;jb<nb_dof_fluid_;jb++)
             {
                 r_[nb_dof_fluid_*b+jb] += quad_ptr_->dsh_dx(b)*app_1[jb]*JxW_;
                 r_[nb_dof_fluid_*b+jb] += quad_ptr_->dsh_dy(b)*app_2[jb]*JxW_;
                 r_[nb_dof_fluid_*b+jb] += quad_ptr_->dsh_dz(b)*app_3[jb]*JxW_;
             }

             for(int b=0;b<nb_el_nodes_;b++)
             for(int jb=0;jb<nb_dof_fluid_;jb++)
             for(int a=0;a<nb_el_nodes_;a++)
             for(int ja=0; ja<nb_dof_fluid_;ja++)
             {
                 m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += shock_matrix_(jb,ja)*quad_ptr_->dsh_dx(b)*quad_ptr_->dsh_dx(a)*JxW_;
                 m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += shock_matrix_(jb,ja)*quad_ptr_->dsh_dy(b)*quad_ptr_->dsh_dy(a)*JxW_;
                 m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += shock_matrix_(jb,ja)*quad_ptr_->dsh_dz(b)*quad_ptr_->dsh_dz(a)*JxW_;
             }
         }

     }







     // This is for Euler-backward method (ALE); bd element calculation
     void RM_b()
     {
         for(int b=0;b<nb_el_nodes_;b++)
         for(int j=0;j<nb_dof_fluid_;j++)
         {
             r_[nb_dof_fluid_*b+j] += quad_ptr_->sh(b)*(F1_adv_[j] - F1_dif_[j])*JNxW_[0];
             r_[nb_dof_fluid_*b+j] += quad_ptr_->sh(b)*(F2_adv_[j] - F2_dif_[j])*JNxW_[1];
             r_[nb_dof_fluid_*b+j] += quad_ptr_->sh(b)*(F3_adv_[j] - F3_dif_[j])*JNxW_[2];
         }

         for(int b=0; b<nb_el_nodes_; b++)
         for(int jb=0;jb<nb_dof_fluid_;jb++)
         for(int a=0; a<nb_el_nodes_; a++)
         for(int ja=0;ja<nb_dof_fluid_;ja++)
         {
            m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += quad_ptr_->sh(b)*A1_(jb,ja)*quad_ptr_->sh(a)*JNxW_[0]; 
            m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += quad_ptr_->sh(b)*A2_(jb,ja)*quad_ptr_->sh(a)*JNxW_[1]; 
            m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += quad_ptr_->sh(b)*A3_(jb,ja)*quad_ptr_->sh(a)*JNxW_[2]; 
            m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) -= quad_ptr_->sh(b)*K11_(jb,ja)*quad_ptr_->dsh_dx(a)*JNxW_[0]; 
            m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) -= quad_ptr_->sh(b)*K12_(jb,ja)*quad_ptr_->dsh_dy(a)*JNxW_[0]; 
            m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) -= quad_ptr_->sh(b)*K13_(jb,ja)*quad_ptr_->dsh_dz(a)*JNxW_[0]; 
            m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) -= quad_ptr_->sh(b)*K21_(jb,ja)*quad_ptr_->dsh_dx(a)*JNxW_[1]; 
            m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) -= quad_ptr_->sh(b)*K22_(jb,ja)*quad_ptr_->dsh_dy(a)*JNxW_[1]; 
            m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) -= quad_ptr_->sh(b)*K23_(jb,ja)*quad_ptr_->dsh_dz(a)*JNxW_[1]; 
            m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) -= quad_ptr_->sh(b)*K31_(jb,ja)*quad_ptr_->dsh_dx(a)*JNxW_[2]; 
            m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) -= quad_ptr_->sh(b)*K32_(jb,ja)*quad_ptr_->dsh_dy(a)*JNxW_[2]; 
            m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) -= quad_ptr_->sh(b)*K33_(jb,ja)*quad_ptr_->dsh_dz(a)*JNxW_[2]; 
         }
     }








    // This is for Generalized-alpha method
     void execute(const element_type& el, const std::vector<vec>& dofs_fluid, mat& m, vec& r, const std::vector<vec>& dofs_fluid_dot)
     {
         nb_el_nodes_ = el.nb_nodes();      
         set_el_data_size();

         get_dofs(dofs_fluid, dofs_fluid_dot);
         r_.clear();
         m_.clear();

         for(const auto& quad_ptr : el.quad_i())
         {
           quad_ptr_ = quad_ptr;
           JxW_ = quad_ptr_->JxW();   
           interpolation();
           quad_ptr_->interpolate(I_orig_fluid_dot_,I_dot_);
           if(setup_.isothermal())
           {
             U_P_isothermal();
             flux_matrices_and_vectors_isothermal();   
           }
           else
           {
             U_P();
             flux_matrices_and_vectors();                            
           }  
           RM_i_gen_alpha();
         }

         if(el.on_boundary())
         {
            for(int i=0; i<el.nb_sides(); i++)
            {
              if(el.is_side_on_boundary(i))
              {
                auto bd_nodes = el.side_nodes(i);
                auto side_flag = el.side_flag(i);
                
                for(const auto& quad_ptr: el.quad_b(i))
                {
                  quad_ptr_ = quad_ptr;               
                  JNxW_ = quad_ptr_->JNxW();      
                  interpolation();
                  if(setup_.isothermal()) flux_matrices_and_vectors_isothermal(bd_nodes, side_flag);
                  else flux_matrices_and_vectors(bd_nodes, side_flag);
                  RM_b_gen_alpha();
                }
              }
            }            
         }
         m = m_;
         r = -r_;
     }
 












     // This is for Generalized-alpha method with moving mesh (ALE)
     void execute(const element_type& el, const std::vector<vec>& dofs_elastostatic, const std::vector<vec>& dofs_fluid, mat& m, vec& r, const std::vector<vec>& dofs_fluid_dot)
     {
         nb_el_nodes_ = el.nb_nodes();      
         set_el_data_size();

         get_dofs_mm(dofs_elastostatic, dofs_fluid, dofs_fluid_dot);
         r_.clear();
         m_.clear();

         for(int i=0; i<el.nb_gp(); i++)
         {
           quad_ptr_ = std::make_unique<quad>(el, el.gp(i), alpha_f_, el.W_in(i));
           JxW_ = quad_ptr_->JxW();
           mesh_v_ = quad_ptr_->mesh_v(I_ux_e_, I_uy_e_, I_uz_e_, dt_);          
           interpolation();
           quad_ptr_->interpolate(I_orig_fluid_dot_,I_dot_);
           if(setup_.isothermal())
           {
             U_P_isothermal();
             flux_matrices_and_vectors_isothermal();   
           }
           else
           {
             U_P();
             flux_matrices_and_vectors();                            
           }  
           RM_i_gen_alpha();
         }
      
         if(el.on_boundary())
         {
            for(int i=0; i<el.nb_sides(); i++)
            {
              if(el.is_side_on_boundary(i))
              {
                auto bd_nodes = el.side_nodes(i);
                auto side_flag = el.side_flag(i);
                
                for(int j=0; j<el.nb_side_gp(i); j++)
                {
                  quad_ptr_ = std::make_unique<quad>(el, el.side_gp(i,j), dummy_, alpha_f_, el.W_bd(i,j));               
                  JNxW_ = quad_ptr_->JNxW();   
                  interpolation();
                  if(setup_.isothermal()) flux_matrices_and_vectors_isothermal(bd_nodes, side_flag);
                  else flux_matrices_and_vectors(bd_nodes, side_flag);
                  RM_b_gen_alpha();
                }
              }
            }            
         }         
         m = m_;
         r = -r_;
     }













    // This is for Generalized-alpha method (ALE); element interior calculation 
    void RM_i_gen_alpha()
    {
         const vec dU_dx = prod(DUDY_, dI_dx_);
         const vec dU_dy = prod(DUDY_, dI_dy_);
         const vec dU_dz = prod(DUDY_, dI_dz_);
         const vec dU_dt = prod(DUDY_, I_dot_);
         const double coeff = alpha_f_* gamma_*dt_;

     
         for (int b=0; b<nb_el_nodes_; b++)
         for (int jb=0; jb<nb_dof_fluid_; jb++)
         {
             r_[nb_dof_fluid_*b+jb] += quad_ptr_->sh(b)*dU_dt[jb]*JxW_;
             r_[nb_dof_fluid_*b+jb] += quad_ptr_->dsh_dx(b)*(F1_dif_[jb]-F1_adv_[jb])*JxW_;
             r_[nb_dof_fluid_*b+jb] += quad_ptr_->dsh_dy(b)*(F2_dif_[jb]-F2_adv_[jb])*JxW_;
             r_[nb_dof_fluid_*b+jb] += quad_ptr_->dsh_dz(b)*(F3_dif_[jb]-F3_adv_[jb])*JxW_;
             r_[nb_dof_fluid_*b+jb] -= quad_ptr_->sh(b)*S_[jb]*JxW_;
             r_[nb_dof_fluid_*b+jb] -= quad_ptr_->sh(b)*(mesh_v_[0]*dU_dx[jb] + mesh_v_[1]*dU_dy[jb] + mesh_v_[2]*dU_dz[jb])*JxW_;
         }

         for(int b=0; b<nb_el_nodes_; b++)
         for(int jb=0; jb<nb_dof_fluid_; jb++)
         for(int a=0; a<nb_el_nodes_; a++)
         for(int ja=0; ja<nb_dof_fluid_; ja++)
         {
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += alpha_m_*quad_ptr_->sh(b)* DUDY_(jb,ja) * quad_ptr_->sh(a)*JxW_;
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) -= coeff*quad_ptr_->dsh_dx(b)* A1_(jb,ja) * quad_ptr_->sh(a)*JxW_;
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) -= coeff*quad_ptr_->dsh_dy(b)* A2_(jb,ja) * quad_ptr_->sh(a)*JxW_;
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) -= coeff*quad_ptr_->dsh_dz(b)* A3_(jb,ja) * quad_ptr_->sh(a)*JxW_;
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += coeff*quad_ptr_->dsh_dx(b)* K11_(jb,ja) * quad_ptr_->dsh_dx(a)*JxW_;
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += coeff*quad_ptr_->dsh_dx(b)* K12_(jb,ja) * quad_ptr_->dsh_dy(a)*JxW_;
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += coeff*quad_ptr_->dsh_dx(b)* K13_(jb,ja) * quad_ptr_->dsh_dz(a)*JxW_;
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += coeff*quad_ptr_->dsh_dy(b)* K21_(jb,ja) * quad_ptr_->dsh_dx(a)*JxW_;
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += coeff*quad_ptr_->dsh_dy(b)* K22_(jb,ja) * quad_ptr_->dsh_dy(a)*JxW_;
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += coeff*quad_ptr_->dsh_dy(b)* K23_(jb,ja) * quad_ptr_->dsh_dz(a)*JxW_;
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += coeff*quad_ptr_->dsh_dz(b)* K31_(jb,ja) * quad_ptr_->dsh_dx(a)*JxW_;
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += coeff*quad_ptr_->dsh_dz(b)* K32_(jb,ja) * quad_ptr_->dsh_dy(a)*JxW_;
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += coeff*quad_ptr_->dsh_dz(b)* K33_(jb,ja) * quad_ptr_->dsh_dz(a)*JxW_;
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) -= coeff*quad_ptr_->sh(b)* S0_(jb,ja) * quad_ptr_->sh(a)*JxW_;
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) -= coeff*quad_ptr_->sh(b)* DUDY_(jb,ja) * (mesh_v_[0]*quad_ptr_->dsh_dx(a) 
                                                                                                + mesh_v_[1]*quad_ptr_->dsh_dy(a)
                                                                                                + mesh_v_[2]*quad_ptr_->dsh_dz(a))*JxW_;
         }



         //--------------------------------------------R_LeastSquares--------------------------------------------------------------------------        
         vec Res_vec = prod(A1_-mesh_v_[0]*DUDY_,dI_dx_) 
                     + prod(A2_-mesh_v_[1]*DUDY_,dI_dy_) 
                     + prod(A3_-mesh_v_[2]*DUDY_,dI_dz_) 
                     - S_;
                     
//         if(!setup_.steady_state()) Res_vec += dU_dt;
                          
         for(int b=0; b<nb_el_nodes_; b++)
         {
             const mat first_part = (A1_-mesh_v_[0]*DUDY_)*quad_ptr_->dsh_dx(b) 
                                  + (A2_-mesh_v_[1]*DUDY_)*quad_ptr_->dsh_dy(b) 
                                  + (A3_-mesh_v_[2]*DUDY_)*quad_ptr_->dsh_dz(b) 
                                  - S0_*quad_ptr_->sh(b);
             const mat first_part_tau = prod(first_part, tau_);
             const vec use = prod(first_part_tau, Res_vec);
             for(int jb=0; jb<nb_dof_fluid_; jb++)
             r_[nb_dof_fluid_*b+jb] += use[jb]*JxW_;
         }

         //--------------------------------------------M_LeastSquares--------------------------------------------------------------------------
         for(int a=0; a<nb_el_nodes_; a++)
         {
             mat Res_mat = coeff*(A1_-mesh_v_[0]*DUDY_)*quad_ptr_->dsh_dx(a) 
                         + coeff*(A2_-mesh_v_[1]*DUDY_)*quad_ptr_->dsh_dy(a) 
                         + coeff*(A3_-mesh_v_[2]*DUDY_)*quad_ptr_->dsh_dz(a) 
                         - coeff*S0_*quad_ptr_->sh(a);

//             if(!setup_.steady_state())     Res_mat += alpha_m_*DUDY_*quad_ptr_->sh(a);                                            
                               
             for(int b=0; b<nb_el_nodes_; b++)
             {
                 const mat first_part = (A1_-mesh_v_[0]*DUDY_)*quad_ptr_->dsh_dx(b) 
                                      + (A2_-mesh_v_[1]*DUDY_)*quad_ptr_->dsh_dy(b) 
                                      + (A3_-mesh_v_[2]*DUDY_)*quad_ptr_->dsh_dz(b) 
                                      - S0_*quad_ptr_->sh(b);
                 const mat first_part_tau = prod(first_part, tau_);
                 const mat use = prod(first_part_tau, Res_mat);
                 for(int jb=0; jb<nb_dof_fluid_; jb++)
                 for(int ja=0; ja<nb_dof_fluid_; ja++)
                 m_(nb_dof_fluid_*b+jb, nb_dof_fluid_*a+ja) += use(jb,ja)*JxW_;
             }
         }




         //-----------------dc------------------------
         dc(dU_dx, dU_dy, dU_dz);

         if(setup_.dc_2006())
         {
             const vec app_1 = prod(shock_matrix_,dI_dx_);
             const vec app_2 = prod(shock_matrix_,dI_dy_);
             const vec app_3 = prod(shock_matrix_,dI_dz_);

             for(int b=0;b<nb_el_nodes_;b++)
             for(int jb=0;jb<nb_dof_fluid_;jb++)
             {
                 r_[nb_dof_fluid_*b+jb] += quad_ptr_->dsh_dx(b)*app_1[jb]*JxW_;
                 r_[nb_dof_fluid_*b+jb] += quad_ptr_->dsh_dy(b)*app_2[jb]*JxW_;
                 r_[nb_dof_fluid_*b+jb] += quad_ptr_->dsh_dz(b)*app_3[jb]*JxW_;
             }

             for(int b=0;b<nb_el_nodes_;b++)
             for(int jb=0;jb<nb_dof_fluid_;jb++)
             for(int a=0;a<nb_el_nodes_;a++)
             for(int ja=0; ja<nb_dof_fluid_;ja++)
             {
                 m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += coeff*shock_matrix_(jb,ja)*quad_ptr_->dsh_dx(b)*quad_ptr_->dsh_dx(a)*JxW_;
                 m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += coeff*shock_matrix_(jb,ja)*quad_ptr_->dsh_dy(b)*quad_ptr_->dsh_dy(a)*JxW_;
                 m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += coeff*shock_matrix_(jb,ja)*quad_ptr_->dsh_dz(b)*quad_ptr_->dsh_dz(a)*JxW_;
             }
         }
     }







     // This is for Generalized-alpha method (ALE); bd element calculation
     void RM_b_gen_alpha()
     {
         const double coeff = alpha_f_* gamma_*dt_;

         for(int b=0;b<nb_el_nodes_;b++)
         for(int j=0;j<nb_dof_fluid_;j++)
         {
             r_[nb_dof_fluid_*b+j] += quad_ptr_->sh(b)*(F1_adv_[j] - F1_dif_[j])*JNxW_[0];
             r_[nb_dof_fluid_*b+j] += quad_ptr_->sh(b)*(F2_adv_[j] - F2_dif_[j])*JNxW_[1];
             r_[nb_dof_fluid_*b+j] += quad_ptr_->sh(b)*(F3_adv_[j] - F3_dif_[j])*JNxW_[2];
         }

         for(int b=0; b<nb_el_nodes_; b++)
         for(int jb=0;jb<nb_dof_fluid_;jb++)
         for(int a=0; a<nb_el_nodes_; a++)
         for(int ja=0;ja<nb_dof_fluid_;ja++)
         {
            m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += coeff*quad_ptr_->sh(b)*A1_(jb,ja)*quad_ptr_->sh(a)*JNxW_[0]; 
            m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += coeff*quad_ptr_->sh(b)*A2_(jb,ja)*quad_ptr_->sh(a)*JNxW_[1]; 
            m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += coeff*quad_ptr_->sh(b)*A3_(jb,ja)*quad_ptr_->sh(a)*JNxW_[2]; 
            m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) -= coeff*quad_ptr_->sh(b)*K11_(jb,ja)*quad_ptr_->dsh_dx(a)*JNxW_[0]; 
            m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) -= coeff*quad_ptr_->sh(b)*K12_(jb,ja)*quad_ptr_->dsh_dy(a)*JNxW_[0]; 
            m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) -= coeff*quad_ptr_->sh(b)*K13_(jb,ja)*quad_ptr_->dsh_dz(a)*JNxW_[0]; 
            m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) -= coeff*quad_ptr_->sh(b)*K21_(jb,ja)*quad_ptr_->dsh_dx(a)*JNxW_[1]; 
            m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) -= coeff*quad_ptr_->sh(b)*K22_(jb,ja)*quad_ptr_->dsh_dy(a)*JNxW_[1]; 
            m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) -= coeff*quad_ptr_->sh(b)*K23_(jb,ja)*quad_ptr_->dsh_dz(a)*JNxW_[1]; 
            m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) -= coeff*quad_ptr_->sh(b)*K31_(jb,ja)*quad_ptr_->dsh_dx(a)*JNxW_[2]; 
            m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) -= coeff*quad_ptr_->sh(b)*K32_(jb,ja)*quad_ptr_->dsh_dy(a)*JNxW_[2]; 
            m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) -= coeff*quad_ptr_->sh(b)*K33_(jb,ja)*quad_ptr_->dsh_dz(a)*JNxW_[2]; 
         }
     }







     //this is called to generate U_P_, U_ and U_ref_
     void U(double p, double vx,  double vy, double vz, double T, vec& u)
     {
         double rho =  props_.rho();
         double cv = props_.cv();
         double E = cv*T + 0.5*(vx*vx+vy*vy+vz*vz);
         
         u[0] = rho;
         u[1] = rho*vx;
         u[2] = rho*vy;
         u[3] = rho*vz;
         u[4] = rho*E - qL_dp_*p - qL_dT_*T;
     }






     void flux_matrices_and_vectors(double p, double vx,  double vy, double vz, double T)
     {
         const double E = cv_*T + 0.5*(vx*vx + vy*vy + vz*vz);
         const double drho_dp = rho_*beta_;
         const double drho_dT = -rho_*alpha_;
         const double de_dp =  (beta_*p - alpha_*T)/rho_;
         const double de_dT =  cp_ - p*alpha_/rho_;


         DUDY_.clear();
         DUDY_(0,0) = drho_dp;
         DUDY_(0,4) = drho_dT;

         DUDY_(1,0) = drho_dp*vx;
         DUDY_(1,1) = rho_;
         DUDY_(1,4) = drho_dT*vx;

         DUDY_(2,0) = drho_dp*vy;
         DUDY_(2,2) = rho_;
         DUDY_(2,4) = drho_dT*vy;

         DUDY_(3,0) = drho_dp*vz;
         DUDY_(3,3) = rho_;
         DUDY_(3,4) = drho_dT*vz;

         DUDY_(4,0) = drho_dp*E + rho_*de_dp - qL_dp_;
         DUDY_(4,1) = rho_*vx;
         DUDY_(4,2) = rho_*vy;
         DUDY_(4,3) = rho_*vz;
         DUDY_(4,4) = drho_dT*E + rho_*de_dT - qL_dT_;


         A1_.clear();
         A1_(0,0)  = drho_dp*vx;
         A1_(0,1) = rho_;
         A1_(0,4)  = drho_dT*vx;

         A1_(1,0)  = drho_dp*vx*vx+1.0;
         A1_(1,1) = 2.0*rho_*vx;
         A1_(1,4)  = drho_dT*vx*vx;

         A1_(2,0)  = drho_dp*vy*vx;
         A1_(2,1) = rho_*vy;
         A1_(2,2) = rho_*vx;
         A1_(2,4)  = drho_dT*vy*vx;

         A1_(3,0)  = drho_dp*vz*vx;
         A1_(3,1) = rho_*vz;
         A1_(3,3) = rho_*vx;
         A1_(3,4)  = drho_dT*vz*vx;

         A1_(4,0)  = vx + vx*(drho_dp*E + rho_*de_dp);
         A1_(4,1) = p + rho_*E + rho_*vx*vx;
         A1_(4,2) = rho_*vx*vy;
         A1_(4,3) = rho_*vx*vz;
         A1_(4,4)  = vx*(drho_dT*E + rho_*de_dT);


         A2_.clear();
         A2_(0,0)  = drho_dp*vy;
         A2_(0,2) = rho_;
         A2_(0,4)  = drho_dT*vy;

         A2_(1,0)  = drho_dp*vx*vy;
         A2_(1,1) = rho_*vy;
         A2_(1,2) = rho_*vx;
         A2_(1,4)  = drho_dT*vx*vy;

         A2_(2,0)  = drho_dp*vy*vy+1.0;
         A2_(2,2) = 2.0*rho_*vy;
         A2_(2,4)  = drho_dT*vy*vy;

         A2_(3,0)  = drho_dp*vz*vy;
         A2_(3,2) = rho_*vz;
         A2_(3,3) = rho_*vy;
         A2_(3,4)  = drho_dT*vz*vy;

         A2_(4,0)  = vy + vy*(drho_dp*E + rho_*de_dp);
         A2_(4,1) = rho_*vx*vy;
         A2_(4,2) = p + rho_*E + rho_*vy*vy;
         A2_(4,3) = rho_*vy*vz;
         A2_(4,4)  = vy*(drho_dT*E + rho_*de_dT);


         A3_.clear();
         A3_(0,0)  = drho_dp*vz;
         A3_(0,3) = rho_;
         A3_(0,4)  = drho_dT*vz;

         A3_(1,0)  = drho_dp*vx*vz;
         A3_(1,1) = rho_*vz;
         A3_(1,3) = rho_*vx;
         A3_(1,4)  = drho_dT*vx*vz;

         A3_(2,0)  = drho_dp*vy*vz;
         A3_(2,2) = rho_*vz;
         A3_(2,3) = rho_*vy;
         A3_(2,4)  = drho_dT*vy*vz;

         A3_(3,0)  = drho_dp*vz*vz+1.0;
         A3_(3,3) = 2.0*rho_*vz;
         A3_(3,4)  = drho_dT*vz*vz;

         A3_(4,0)  = vz + vz*(drho_dp*E + rho_*de_dp);
         A3_(4,1) = rho_*vx*vz;
         A3_(4,2) = rho_*vy*vz;
         A3_(4,3) = p + rho_*E + rho_*vz*vz;
         A3_(4,4)  = vz*(drho_dT*E + rho_*de_dT);


         K11_.clear(); K12_.clear(); K13_.clear();
         K21_.clear(); K22_.clear(); K23_.clear();
         K31_.clear(); K32_.clear(); K33_.clear();

         const double lambda = -2.0*mu_/3.0;
         K11_(1,1) = lambda + 2*mu_;
         K11_(2,2) = mu_;
         K11_(3,3) = mu_;
         K11_(4,1) = (lambda + 2*mu_)*vx;
         K11_(4,2) = mu_*vy;
         K11_(4,3) = mu_*vz;
         K11_(4,4) =  kappa_;

         K12_(1,2) = lambda;
         K12_(2,1) =  mu_;
         K12_(4,1) = mu_*vy;
         K12_(4,2) = lambda*vx;

         K13_(1,3) = lambda;
         K13_(3,1) =  mu_;
         K13_(4,1) = mu_*vz;
         K13_(4,3) = lambda*vx;

         K21_(1,2) = mu_;
         K21_(2,1) = lambda;
         K21_(4,1) = lambda*vy;
         K21_(4,2) = mu_*vx;

         K22_(1,1) = mu_;
         K22_(2,2) = lambda + 2*mu_;
         K22_(3,3) = mu_;
         K22_(4,1) = mu_*vx;
         K22_(4,2) = (lambda + 2*mu_)*vy;
         K22_(4,3) = mu_*vz;
         K22_(4,4) =  kappa_;

         K23_(2,3) = lambda;
         K23_(3,2) =  mu_;
         K23_(4,2) = mu_*vz;
         K23_(4,3) = lambda*vy;

         K31_(2,3) = mu_;
         K31_(3,1) = lambda;
         K31_(4,1) = lambda*vz;
         K31_(4,3) = mu_*vx;

         K32_(2,3) =  mu_;
         K32_(3,2) = lambda;
         K32_(4,2) = lambda*vz;
         K32_(4,3) = mu_*vy;

         K33_(1,1) = mu_;
         K33_(2,2) = mu_;
         K33_(3,3) = lambda + 2*mu_;
         K33_(4,1) = mu_*vx;
         K33_(4,2) = mu_*vy;
         K33_(4,3) = (lambda + 2*mu_)*vz;
         K33_(4,4) =  kappa_;


         S0_.clear();
         S0_(4,1) = rho_*gravity_[0];
         S0_(4,2) = rho_*gravity_[1];
         S0_(4,3) = rho_*gravity_[2];


         U(p,vx,vy,vz,T,U_);

         F1_adv_[0] = rho_*vx;
         F1_adv_[1] = rho_*vx*vx + p;
         F1_adv_[2] = rho_*vx*vy;
         F1_adv_[3] = rho_*vx*vz;
         F1_adv_[4] = vx*(rho_*E+p);

         F2_adv_[0] = rho_*vy;
         F2_adv_[1] = rho_*vy*vx;
         F2_adv_[2] = rho_*vy*vy + p;
         F2_adv_[3] = rho_*vy*vz;
         F2_adv_[4] = vy*(rho_*E+p);

         F3_adv_[0] = rho_*vz;
         F3_adv_[1] = rho_*vz*vx;
         F3_adv_[2] = rho_*vz*vy;
         F3_adv_[3] = rho_*vz*vz + p;
         F3_adv_[4] = vz*(rho_*E+p);

         S_[1] = rho_*gravity_[0];
         S_[2] = rho_*gravity_[1];
         S_[3] = rho_*gravity_[2];
         S_[4] = rho_*(gravity_[0]*vx + gravity_[1]*vy + gravity_[2]*vz);

         F1_dif_ =  prod(K11_,dI_dx_) + prod(K12_,dI_dy_) + prod(K13_,dI_dz_);
         F2_dif_ =  prod(K21_,dI_dx_) + prod(K22_,dI_dy_) + prod(K23_,dI_dz_);
         F3_dif_ =  prod(K31_,dI_dx_) + prod(K32_,dI_dy_) + prod(K33_,dI_dz_);
     }





     void flux_matrices_and_vectors(double p, double vx,  double vy, double vz, double T, const std::vector<int>& bd_nodes, int side_flag)
     {
         flux_matrices_and_vectors(p,vx,vy,vz,T);

         const double E = cv_*T + 0.5*(vx*vx + vy*vy + vz*vz);
         const double de_dp =  (beta_*p - alpha_*T)/rho_;
         const double de_dT =  cp_ - p*alpha_/rho_;
         
         if(ic_bc_.mass_flux1(bd_nodes, side_flag).first)
         {
            const double mass_flux1 = ic_bc_.mass_flux1(bd_nodes, side_flag).second;
         
            F1_adv_.clear();
            F1_adv_[0] = mass_flux1;
            F1_adv_[1] = mass_flux1*vx + p;
            F1_adv_[2] = mass_flux1*vy;
            F1_adv_[3] = mass_flux1*vz;
            F1_adv_[4] = mass_flux1*E + p*vx;         

            A1_.clear();
            A1_(1,0)  = 1.0;
            A1_(1,1) = mass_flux1;
            A1_(2,2) = mass_flux1;
            A1_(3,3) = mass_flux1;
            A1_(4,0)  = vx + mass_flux1*de_dp;
            A1_(4,1) = p + mass_flux1*vx;
            A1_(4,2) = mass_flux1*vy;
            A1_(4,3) = mass_flux1*vz;
            A1_(4,4)  = mass_flux1*de_dT;
         }    


         if(ic_bc_.mass_flux2(bd_nodes, side_flag).first)
         {
            const double mass_flux2 = ic_bc_.mass_flux2(bd_nodes, side_flag).second;
         
            F2_adv_.clear();
            F2_adv_[0] = mass_flux2;
            F2_adv_[1] = mass_flux2*vx;
            F2_adv_[2] = mass_flux2*vy + p;
            F2_adv_[3] = mass_flux2*vz;
            F2_adv_[4] = mass_flux2*E + p*vy;         

            A2_.clear();
            A2_(1,1) = mass_flux2;
            A2_(2,0)  = 1.0;
            A2_(2,2) = mass_flux2;
            A2_(3,3) = mass_flux2;
            A2_(4,0)  = vy + mass_flux2*de_dp;
            A2_(4,1) = mass_flux2*vx;
            A2_(4,2) = p + mass_flux2*vy;
            A2_(4,3) = mass_flux2*vz;
            A2_(4,4)  = mass_flux2*de_dT;
         }    
 

         if(ic_bc_.mass_flux3(bd_nodes, side_flag).first)
         {
            const double mass_flux3 = ic_bc_.mass_flux3(bd_nodes, side_flag).second;
         
            F3_adv_.clear();
            F3_adv_[0] = mass_flux3;
            F3_adv_[1] = mass_flux3*vx;
            F3_adv_[2] = mass_flux3*vy;
            F3_adv_[3] = mass_flux3*vz + p;
            F3_adv_[4] = mass_flux3*E + p*vz;         

            A3_.clear();
            A3_(1,1) = mass_flux3;
            A3_(2,2) = mass_flux3;
            A3_(3,0)  = 1.0;
            A3_(3,3) = mass_flux3;
            A3_(4,0)  = vz + mass_flux3*de_dp;
            A3_(4,1) = mass_flux3*vx;
            A3_(4,2) = mass_flux3*vy;
            A3_(4,3) = p + mass_flux3*vz;
            A3_(4,4)  = mass_flux3*de_dT;
         }    


         const double vx_x(dI_dx_[1]), vx_y(dI_dy_[1]), vx_z(dI_dz_[1]);
         const double vy_x(dI_dx_[2]), vy_y(dI_dy_[2]), vy_z(dI_dz_[2]);
         const double vz_x(dI_dx_[3]), vz_y(dI_dy_[3]), vz_z(dI_dz_[3]);
         const double T_x(dI_dx_[4]),  T_y(dI_dy_[4]),  T_z(dI_dz_[4]);

         const double lambda = -2.0*mu_/3.0;
         double tau11 = lambda*(vx_x + vy_y + vz_z) + 2.0*mu_*vx_x;
         double tau22 = lambda*(vx_x + vy_y + vz_z) + 2.0*mu_*vy_y;
         double tau33 = lambda*(vx_x + vy_y + vz_z) + 2.0*mu_*vz_z;
         double tau12 = mu_*(vx_y + vy_x);
         double tau13 = mu_*(vx_z + vz_x);
         double tau23 = mu_*(vy_z + vz_y);
         double q1 = -kappa_*T_x;
         double q2 = -kappa_*T_y;
         double q3 = -kappa_*T_z;

         if(ic_bc_.neumann_tau11(bd_nodes, side_flag).first)   tau11 = ic_bc_.neumann_tau11(bd_nodes, side_flag).second;
         if(ic_bc_.neumann_tau22(bd_nodes, side_flag).first)   tau22 = ic_bc_.neumann_tau22(bd_nodes, side_flag).second;
         if(ic_bc_.neumann_tau33(bd_nodes, side_flag).first)   tau33 = ic_bc_.neumann_tau33(bd_nodes, side_flag).second;
         if(ic_bc_.neumann_tau12(bd_nodes, side_flag).first)   tau12 = ic_bc_.neumann_tau12(bd_nodes, side_flag).second;
         if(ic_bc_.neumann_tau13(bd_nodes, side_flag).first)   tau13 = ic_bc_.neumann_tau13(bd_nodes, side_flag).second;
         if(ic_bc_.neumann_tau23(bd_nodes, side_flag).first)   tau23 = ic_bc_.neumann_tau23(bd_nodes, side_flag).second;
         if(ic_bc_.neumann_q1(bd_nodes, side_flag).first)      q1 = ic_bc_.neumann_q1(bd_nodes, side_flag).second;
         if(ic_bc_.neumann_q2(bd_nodes, side_flag).first)      q2 = ic_bc_.neumann_q2(bd_nodes, side_flag).second;
         if(ic_bc_.neumann_q3(bd_nodes, side_flag).first)      q3 = ic_bc_.neumann_q3(bd_nodes, side_flag).second;

         F1_dif_[1] = tau11;
         F1_dif_[2] = tau12;
         F1_dif_[3] = tau13;
         F1_dif_[4] = tau11*vx + tau12*vy + tau13*vz - q1;

         F2_dif_[1] = tau12;
         F2_dif_[2] = tau22;
         F2_dif_[3] = tau23;
         F2_dif_[4] = tau12*vx + tau22*vy  + tau23*vz - q2;

         F3_dif_[1] = tau13;
         F3_dif_[2] = tau23;
         F3_dif_[3] = tau33;
         F3_dif_[4] = tau13*vx + tau23*vy  + tau33*vz - q3;


         if(ic_bc_.neumann_tau11(bd_nodes, side_flag).first)
         {
           K11_(1, 1) = 0.0;
           K12_(1, 2) = 0.0;
           K13_(1, 3) = 0.0;
           K11_(4, 1) = 0.0;
           K12_(4, 2) = 0.0;
           K13_(4, 3) = 0.0;
         }

         if(ic_bc_.neumann_tau22(bd_nodes, side_flag).first)
         {
           K21_(2, 1) = 0.0;
           K22_(2, 2) = 0.0;
           K23_(2, 3) = 0.0;
           K21_(4, 1) = 0.0;
           K22_(4, 2) = 0.0;
           K23_(4, 3) = 0.0;
         }

         if(ic_bc_.neumann_tau33(bd_nodes, side_flag).first)
         {
           K31_(3, 1) = 0.0;
           K32_(3, 2) = 0.0;
           K33_(3, 3) = 0.0;
           K31_(4, 1) = 0.0;
           K32_(4, 2) = 0.0;
           K31_(4, 3) = 0.0;
         }

         if(ic_bc_.neumann_tau12(bd_nodes, side_flag).first)
         {
           K11_(2, 2) = 0.0;
           K12_(2, 1) = 0.0;
           K21_(1, 2) = 0.0;
           K22_(1, 1) = 0.0;
           K11_(4, 2) = 0.0;
           K12_(4, 1) = 0.0;
           K21_(4, 2) = 0.0;
           K22_(4, 1) = 0.0;
         }

         if(ic_bc_.neumann_tau13(bd_nodes, side_flag).first)
         {
           K11_(3, 3) = 0.0;
           K13_(3, 1) = 0.0;
           K31_(1, 3) = 0.0;
           K33_(1, 1) = 0.0;
           K11_(4, 3) = 0.0;
           K13_(4, 1) = 0.0;
           K31_(4, 3) = 0.0;
           K33_(4, 1) = 0.0;
         }

         if(ic_bc_.neumann_tau23(bd_nodes, side_flag).first)
         {
           K22_(3, 3) = 0.0;
           K23_(3, 2) = 0.0;
           K32_(2, 3) = 0.0;
           K33_(2, 2) = 0.0;
           K22_(4, 3) = 0.0;
           K23_(4, 2) = 0.0;
           K32_(4, 3) = 0.0;
           K33_(4, 2) = 0.0;
         }

         if(ic_bc_.neumann_q1(bd_nodes, side_flag).first)
           K11_(4, 4) =  0.0;

         if(ic_bc_.neumann_q2(bd_nodes, side_flag).first)
           K22_(4, 4) =  0.0;      

         if(ic_bc_.neumann_q3(bd_nodes, side_flag).first)
           K33_(4, 4) =  0.0;      
     }




















     //this is called to generate U_P_, U_ and U_ref_
     void U(double p, double vx,  double vy, double vz, vec& u)
     {
         double rho =  props_.rho();
         
         u[0] = rho;
         u[1] = rho*vx;
         u[2] = rho*vy;
         u[3] = rho*vz;
     }






     void flux_matrices_and_vectors(double p, double vx,  double vy, double vz)
     {
         const double drho_dp = rho_*beta_;

         DUDY_.clear();
         DUDY_(0,0) = drho_dp;

         DUDY_(1,0) = drho_dp*vx;
         DUDY_(1,1) = rho_;

         DUDY_(2,0) = drho_dp*vy;
         DUDY_(2,2) = rho_;

         DUDY_(3,0) = drho_dp*vz;
         DUDY_(3,3) = rho_;

         A1_.clear();
         A1_(0,0)  = drho_dp*vx;
         A1_(0,1) = rho_;

         A1_(1,0)  = drho_dp*vx*vx+1.0;
         A1_(1,1) = 2.0*rho_*vx;

         A1_(2,0)  = drho_dp*vy*vx;
         A1_(2,1) = rho_*vy;
         A1_(2,2) = rho_*vx;

         A1_(3,0)  = drho_dp*vz*vx;
         A1_(3,1) = rho_*vz;
         A1_(3,3) = rho_*vx;


         A2_.clear();
         A2_(0,0)  = drho_dp*vy;
         A2_(0,2) = rho_;

         A2_(1,0)  = drho_dp*vx*vy;
         A2_(1,1) = rho_*vy;
         A2_(1,2) = rho_*vx;

         A2_(2,0)  = drho_dp*vy*vy+1.0;
         A2_(2,2) = 2.0*rho_*vy;

         A2_(3,0)  = drho_dp*vz*vy;
         A2_(3,2) = rho_*vz;
         A2_(3,3) = rho_*vy;


         A3_.clear();
         A3_(0,0)  = drho_dp*vz;
         A3_(0,3) = rho_;

         A3_(1,0)  = drho_dp*vx*vz;
         A3_(1,1) = rho_*vz;
         A3_(1,3) = rho_*vx;

         A3_(2,0)  = drho_dp*vy*vz;
         A3_(2,2) = rho_*vz;
         A3_(2,3) = rho_*vy;

         A3_(3,0)  = drho_dp*vz*vz+1.0;
         A3_(3,3) = 2.0*rho_*vz;


         K11_.clear(); K12_.clear(); K13_.clear();
         K21_.clear(); K22_.clear(); K23_.clear();
         K31_.clear(); K32_.clear(); K33_.clear();

         const double lambda = -2.0*mu_/3.0;
         K11_(1,1) = lambda + 2*mu_;
         K11_(2,2) = mu_;
         K11_(3,3) = mu_;

         K12_(1,2) = lambda;
         K12_(2,1) =  mu_;

         K13_(1,3) = lambda;
         K13_(3,1) =  mu_;

         K21_(1,2) = mu_;
         K21_(2,1) = lambda;

         K22_(1,1) = mu_;
         K22_(2,2) = lambda + 2*mu_;
         K22_(3,3) = mu_;

         K23_(2,3) = lambda;
         K23_(3,2) =  mu_;

         K31_(2,3) = mu_;
         K31_(3,1) = lambda;

         K32_(2,3) =  mu_;
         K32_(3,2) = lambda;

         K33_(1,1) = mu_;
         K33_(2,2) = mu_;
         K33_(3,3) = lambda + 2*mu_;


         S0_.clear();


         U(p,vx,vy,vz,U_);

         F1_adv_[0] = rho_*vx;
         F1_adv_[1] = rho_*vx*vx + p;
         F1_adv_[2] = rho_*vx*vy;
         F1_adv_[3] = rho_*vx*vz;

         F2_adv_[0] = rho_*vy;
         F2_adv_[1] = rho_*vy*vx;
         F2_adv_[2] = rho_*vy*vy + p;
         F2_adv_[3] = rho_*vy*vz;

         F3_adv_[0] = rho_*vz;
         F3_adv_[1] = rho_*vz*vx;
         F3_adv_[2] = rho_*vz*vy;
         F3_adv_[3] = rho_*vz*vz + p;

         S_[1] = rho_*gravity_[0];
         S_[2] = rho_*gravity_[1];
         S_[3] = rho_*gravity_[2];

         F1_dif_ =  prod(K11_,dI_dx_) + prod(K12_,dI_dy_) + prod(K13_,dI_dz_);
         F2_dif_ =  prod(K21_,dI_dx_) + prod(K22_,dI_dy_) + prod(K23_,dI_dz_);
         F3_dif_ =  prod(K31_,dI_dx_) + prod(K32_,dI_dy_) + prod(K33_,dI_dz_);
     }





     void flux_matrices_and_vectors(double p, double vx,  double vy, double vz, const std::vector<int>& bd_nodes, int side_flag)
     {
         flux_matrices_and_vectors(p,vx,vy,vz);

         if(ic_bc_.mass_flux1(bd_nodes, side_flag).first)
         {
            const double mass_flux1 = ic_bc_.mass_flux1(bd_nodes, side_flag).second;
         
            F1_adv_.clear();
            F1_adv_[0] = mass_flux1;
            F1_adv_[1] = mass_flux1*vx + p;
            F1_adv_[2] = mass_flux1*vy;
            F1_adv_[3] = mass_flux1*vz;

            A1_.clear();
            A1_(1,0)  = 1.0;
            A1_(1,1) = mass_flux1;
            A1_(2,2) = mass_flux1;
            A1_(3,3) = mass_flux1;
         }    


         if(ic_bc_.mass_flux2(bd_nodes, side_flag).first)
         {
            const double mass_flux2 = ic_bc_.mass_flux2(bd_nodes, side_flag).second;
         
            F2_adv_.clear();
            F2_adv_[0] = mass_flux2;
            F2_adv_[1] = mass_flux2*vx;
            F2_adv_[2] = mass_flux2*vy + p;
            F2_adv_[3] = mass_flux2*vz;

            A2_.clear();
            A2_(1,1) = mass_flux2;
            A2_(2,0)  = 1.0;
            A2_(2,2) = mass_flux2;
            A2_(3,3) = mass_flux2;
         }    
 

         if(ic_bc_.mass_flux3(bd_nodes, side_flag).first)
         {
            const double mass_flux3 = ic_bc_.mass_flux3(bd_nodes, side_flag).second;
         
            F3_adv_.clear();
            F3_adv_[0] = mass_flux3;
            F3_adv_[1] = mass_flux3*vx;
            F3_adv_[2] = mass_flux3*vy;
            F3_adv_[3] = mass_flux3*vz + p;

            A3_.clear();
            A3_(1,1) = mass_flux3;
            A3_(2,2) = mass_flux3;
            A3_(3,0)  = 1.0;
            A3_(3,3) = mass_flux3;
         }    


         const double vx_x(dI_dx_[1]), vx_y(dI_dy_[1]), vx_z(dI_dz_[1]);
         const double vy_x(dI_dx_[2]), vy_y(dI_dy_[2]), vy_z(dI_dz_[2]);
         const double vz_x(dI_dx_[3]), vz_y(dI_dy_[3]), vz_z(dI_dz_[3]);

         const double lambda = -2.0*mu_/3.0;
         double tau11 = lambda*(vx_x + vy_y + vz_z) + 2.0*mu_*vx_x;
         double tau22 = lambda*(vx_x + vy_y + vz_z) + 2.0*mu_*vy_y;
         double tau33 = lambda*(vx_x + vy_y + vz_z) + 2.0*mu_*vz_z;
         double tau12 = mu_*(vx_y + vy_x);
         double tau13 = mu_*(vx_z + vz_x);
         double tau23 = mu_*(vy_z + vz_y);

         if(ic_bc_.neumann_tau11(bd_nodes, side_flag).first)   tau11 = ic_bc_.neumann_tau11(bd_nodes, side_flag).second;
         if(ic_bc_.neumann_tau22(bd_nodes, side_flag).first)   tau22 = ic_bc_.neumann_tau22(bd_nodes, side_flag).second;
         if(ic_bc_.neumann_tau33(bd_nodes, side_flag).first)   tau33 = ic_bc_.neumann_tau33(bd_nodes, side_flag).second;
         if(ic_bc_.neumann_tau12(bd_nodes, side_flag).first)   tau12 = ic_bc_.neumann_tau12(bd_nodes, side_flag).second;
         if(ic_bc_.neumann_tau13(bd_nodes, side_flag).first)   tau13 = ic_bc_.neumann_tau13(bd_nodes, side_flag).second;
         if(ic_bc_.neumann_tau23(bd_nodes, side_flag).first)   tau23 = ic_bc_.neumann_tau23(bd_nodes, side_flag).second;

         F1_dif_[1] = tau11;
         F1_dif_[2] = tau12;
         F1_dif_[3] = tau13;

         F2_dif_[1] = tau12;
         F2_dif_[2] = tau22;
         F2_dif_[3] = tau23;

         F3_dif_[1] = tau13;
         F3_dif_[2] = tau23;
         F3_dif_[3] = tau33;


         if(ic_bc_.neumann_tau11(bd_nodes, side_flag).first)
         {
           K11_(1, 1) = 0.0;
           K12_(1, 2) = 0.0;
           K13_(1, 3) = 0.0;
         }

         if(ic_bc_.neumann_tau22(bd_nodes, side_flag).first)
         {
           K21_(2, 1) = 0.0;
           K22_(2, 2) = 0.0;
           K23_(2, 3) = 0.0;
         }

         if(ic_bc_.neumann_tau33(bd_nodes, side_flag).first)
         {
           K31_(3, 1) = 0.0;
           K32_(3, 2) = 0.0;
           K33_(3, 3) = 0.0;
         }

         if(ic_bc_.neumann_tau12(bd_nodes, side_flag).first)
         {
           K11_(2, 2) = 0.0;
           K12_(2, 1) = 0.0;
           K21_(1, 2) = 0.0;
           K22_(1, 1) = 0.0;
         }

         if(ic_bc_.neumann_tau13(bd_nodes, side_flag).first)
         {
           K11_(3, 3) = 0.0;
           K13_(3, 1) = 0.0;
           K31_(1, 3) = 0.0;
           K33_(1, 1) = 0.0;
         }

         if(ic_bc_.neumann_tau23(bd_nodes, side_flag).first)
         {
           K22_(3, 3) = 0.0;
           K23_(3, 2) = 0.0;
           K32_(2, 3) = 0.0;
           K33_(2, 2) = 0.0;
         }
     }









     void tau_matrix(double vx,  double vy, double vz)
     {
         tau_.clear();

         const double v_nrm = sqrt(vx*vx + vy*vy + vz*vz);
         if(v_nrm < tol_) return;

         const double c(sound_speed_);
         vec v(3,0.0);
         v[0] = vx;    v[1] = vy;  v[2] = vz;

         mat del_v(3,3,0.0);
         for(int i=0; i<3; i++)
         {
             del_v(i,0) = dI_dx_[1+i];     del_v(i,1) = dI_dy_[1+i];    del_v(i,2) = dI_dz_[1+i];
         }
         auto alpha = quad_ptr_->alpha(v, del_v, dummy_);
         auto h = quad_ptr_->compute_h(alpha, dummy_);

         if(setup_.tau_non_diag_comp_2001())
         {
             double lambda_sqr = alpha[0]*(v_nrm*v_nrm + c*c) 
                               + (alpha[1] + alpha[2]) *0.5*c*c
                               + 0.5*c*sqrt(c*c*(alpha[1] + alpha[2])*(alpha[1] + alpha[2]) 
                                           + 16*alpha[0]*alpha[0]*v_nrm*v_nrm);

             if(!setup_.steady_state()) lambda_sqr += 4/(dt_*dt_);
             const double lambda = 1.0/sqrt(lambda_sqr) + h/(2*v_nrm);

             double use(0.0);
             if(setup_.steady_state())  use = lambda;
             else  use = std::min(dt_*0.5, lambda);

             tau_(0, 0) = use;
             tau_(1, 1) = std::min(use, rho_*h*h/(mu_*12));
             tau_(2, 2) = tau_(1, 1);
             tau_(3, 3) = tau_(1, 1);
             if(!setup_.isothermal()) tau_(4, 4) = std::min(use, rho_*h*h*cv_/(kappa_*12));

             const mat DYDU = inverse(DUDY_);
             tau_= prod(DYDU,tau_);

             // tau correction for weak compressibility (if M < 0.1)
             if(setup_.incompressibility_correction() && v_nrm/c < 0.1)
             {
               const auto G = quad_ptr_->G();
               const auto tr_G = quad_ptr_->trace_G();
               tau_(0, 0) = 1.0/( 1.0/tau_(0,0) + rho_*tau_(1,1)*tr_G ); 
               tau_(0, 1) = 0.0;
               tau_(0, 2) = 0.0;
               tau_(0, 3) = 0.0;
               if(!setup_.isothermal()) tau_(0, 4) = 0.0;
             }
         }


         else if(setup_.tau_non_diag_comp_2019())
         {
             double use(0.0);
             if(setup_.steady_state())  use = h/(2.0*(v_nrm + c)) + h/(2.0*v_nrm);
             else  use = std::min(dt_*0.5, h/(2.0*(v_nrm + c)) + h/(2.0*v_nrm));

             tau_(0, 0) = use;
             tau_(1, 1) = std::min(use, h*h*rho_/(12.0*mu_));
             tau_(2, 2) = tau_(1, 1);
             tau_(3, 3) = tau_(1, 1);
             if(!setup_.isothermal()) tau_(4, 4) = std::min(use, h*h*rho_*cv_/(12.0*kappa_));

             const mat DYDU = inverse(DUDY_);
             tau_= prod(DYDU,tau_);

             // tau correction for weak compressibility (if M < 0.1)
             if(setup_.incompressibility_correction() && v_nrm/c < 0.1)
             {
               const auto G = quad_ptr_->G();
               const auto tr_G = quad_ptr_->trace_G();
               tau_(0, 0) = 1.0/( 1.0/tau_(0,0) + rho_*tau_(1,1)*tr_G ); 
               tau_(0, 1) = 0.0;
               tau_(0, 2) = 0.0;
               tau_(0, 3) = 0.0;
               if(!setup_.isothermal()) tau_(0, 4) = 0.0;
             }
         }

         else if(setup_.tau_diag_incomp_2007())
         {
          const auto G = quad_ptr_->G();
          double use(0.0);
          for(int i=0; i<3; i++)
           for(int j=0; j<3; j++)
            use += G(i,j)*G(i,j);
           
          double tau_m = boost::numeric::ublas::inner_prod(v-mesh_v_, prod(G, v-mesh_v_)) + 9.0*use*mu_*mu_/(rho_*rho_);
          if(!setup_.steady_state()) tau_m += 4.0/(dt_*dt_);
          tau_m = 1.0/sqrt(tau_m); 

          const auto g = quad_ptr_->g();
          const double tau_c = 1.0/(rho_*tau_m*boost::numeric::ublas::inner_prod(g,g));
          
          tau_(0, 0) = tau_c;
          tau_(1, 1) = tau_m/rho_;
          tau_(2, 2) = tau_m/rho_;
          tau_(3, 3) = tau_m/rho_;

          if(!setup_.isothermal())
          { 
            double tau_e = boost::numeric::ublas::inner_prod(v-mesh_v_, prod(G, v-mesh_v_)) + 9.0*use*kappa_*kappa_/(rho_*cv_*rho_*cv_);
            if(!setup_.steady_state()) tau_e += 4.0/(dt_*dt_);
            tau_e = 1.0/sqrt(tau_e); 
            tau_(4, 4) = tau_e/(rho_*cv_);
          }  
         }


         else if(setup_.tau_diag_2014())
         {
            auto A0 = diag_mat_of_mat(DUDY_);
            auto A1 = diag_mat_of_mat(A1_);
            auto A2 = diag_mat_of_mat(A2_);
            auto A3 = diag_mat_of_mat(A3_);
            auto K11 = diag_mat_of_mat(K11_);
            auto K12 = diag_mat_of_mat(K12_);
            auto K13 = diag_mat_of_mat(K13_);
            auto K21 = diag_mat_of_mat(K21_);
            auto K22 = diag_mat_of_mat(K22_);
            auto K23 = diag_mat_of_mat(K23_);
            auto K31 = diag_mat_of_mat(K31_);
            auto K32 = diag_mat_of_mat(K32_);
            auto K33 = diag_mat_of_mat(K33_);
                        
            const auto G = quad_ptr_->G();
            mat tau_t_inv_sqr = 4.0/(dt_*dt_)*prod(A0,A0); 
            mat tau_a_inv_sqr = G(0,0)*prod(A1,A1) + G(0,1)*prod(A1,A2) + G(0,2)*prod(A1,A3)             
                              + G(1,0)*prod(A2,A1) + G(1,1)*prod(A2,A2) + G(1,2)*prod(A2,A3)
                              + G(2,0)*prod(A3,A1) + G(2,1)*prod(A3,A2) + G(2,2)*prod(A3,A3);
            mat tau_d_inv_sqr = G(0,0)*G(0,0)*prod(K11,K11) + G(0,1)*G(0,1)*prod(K12,K12) + G(0,2)*G(0,2)*prod(K13,K13) 
                              + G(1,0)*G(1,0)*prod(K21,K21) + G(1,1)*G(1,1)*prod(K22,K22) + G(1,2)*G(1,2)*prod(K23,K23)
                              + G(2,0)*G(2,0)*prod(K31,K31) + G(2,1)*G(2,1)*prod(K32,K32) + G(2,2)*G(2,2)*prod(K33,K33);            

            vec tau_t_inv(nb_dof_fluid_, 0.0); 
            vec tau_a_inv(nb_dof_fluid_, 0.0); 
            vec tau_d_inv(nb_dof_fluid_, 0.0);
            for(int i=0;i<nb_dof_fluid_;i++)
            {
              tau_t_inv[i] = sqrt(tau_t_inv_sqr(i,i));
              tau_a_inv[i] = sqrt(tau_a_inv_sqr(i,i));
              tau_d_inv[i] = sqrt(tau_d_inv_sqr(i,i));
            }
                              
            for(int i=0;i<nb_dof_fluid_;i++)
              if(tau_t_inv[i] + tau_a_inv[i] + tau_d_inv[i] > tol_) 
                tau_(i,i) = 1.0/(tau_t_inv[i] + tau_a_inv[i] + tau_d_inv[i] + 1.e-8);

            if(setup_.incompressibility_correction() && v_nrm/c < 0.1)
            {
               const auto tr_G = quad_ptr_->trace_G();
               tau_(0, 0) = 1.0/( 1.0/tau_(0,0) + rho_*tau_(1,1)*tr_G ); 
            }  
         }
     }











     void dc(const vec& dU_dx, const vec& dU_dy, const vec& dU_dz)
     {
         if (setup_.dc_2006())
         {
             shock_matrix_.clear();
             const double del_rho_nrm = sqrt(dU_dx[0]*dU_dx[0] + dU_dy[0]*dU_dy[0] + dU_dz[0]*dU_dz[0]);
             if(del_rho_nrm < tol_) return;
             vec J(3,0.0);
             J[0] = dU_dx[0]/del_rho_nrm;
             J[1] = dU_dy[0]/del_rho_nrm;
             J[2] = dU_dz[0]/del_rho_nrm;
             double h(0.0);
             for(int i=0; i<nb_el_nodes_; i++)
             {
                 h += abs(J[0]*quad_ptr_->dsh_dx(i) + J[1]*quad_ptr_->dsh_dy(i) + J[2]*quad_ptr_->dsh_dz(i));
             }
             if(h > tol_) h = 1.0/h;
             else return;

             vec Res = prod(A1_,dI_dx_) + prod(A2_,dI_dy_) + prod(A3_,dI_dz_) - S_;
             const double u_ref_inv_Res_nrm = norm_2(prod(u_ref_inv_,Res));
             const double u_ref_inv_U_nrm = norm_2(prod(u_ref_inv_,U_));
             const double a = norm_2(prod(u_ref_inv_, dU_dx));
             const double b = norm_2(prod(u_ref_inv_, dU_dy));
             const double c = norm_2(prod(u_ref_inv_, dU_dz));

             const double s = setup_.dc_sharp();
             double nu_shoc(0.0);
             if(s == 1.0)
             {
               if(a*a+b*b+c*c < tol_) return;
               nu_shoc = u_ref_inv_Res_nrm*h*1.0/sqrt(a*a+b*b+c*c);
             }  
             else if(s == 2.0) 
             {
               nu_shoc = u_ref_inv_Res_nrm*h*h/u_ref_inv_U_nrm;
             }  
             else if(s == 1.5) 
             {
               if(a*a+b*b+c*c < tol_) nu_shoc = u_ref_inv_Res_nrm*h*h/u_ref_inv_U_nrm;
               else nu_shoc = u_ref_inv_Res_nrm*h*(1.0/sqrt(a*a+b*b+c*c) + h/u_ref_inv_U_nrm)/2.0;
             }

             mat K(nb_dof_fluid_,nb_dof_fluid_,0.0);
             for(int i=0; i<nb_dof_fluid_; i++)
             K(i,i) = nu_shoc;

             shock_matrix_ = setup_.dc_scale_fact()*prod(K, DUDY_);
         }
     }




 private:

     int nb_el_nodes_;
     int nb_dof_fluid_;

     ic_bc_type& ic_bc_;
     fluid_properties& props_;
     read_setup& setup_;

     double JxW_; 
     vec JNxW_;
     double dt_;

     vec I_orig_fluid_dot_;
     vec I_dot_;

     vec P_orig_fluid_;
     vec I_orig_fluid_;
     vec P_;
     vec I_;
     vec dI_dx_;
     vec dI_dy_;
     vec dI_dz_;

     vec I_ux_e_;
     vec I_uy_e_;
     vec I_uz_e_;
     vec mesh_v_;

     double rho_;
     double mu_;
     double cp_, cv_;
     double kappa_;
     double sound_speed_;
     double beta_, alpha_;
     double qL_dp_, qL_dT_;


     vec U_P_;
     vec U_;
     vec F1_adv_;
     vec F2_adv_;
     vec F3_adv_;
     vec F1_dif_;
     vec F2_dif_;
     vec F3_dif_;
     vec S_;

     mat DUDY_;
     mat A1_,A2_,A3_;
     mat K11_,K12_,K13_,K21_,K22_,K23_,K31_,K32_,K33_;
     mat S0_;

     mat tau_;
     mat shock_matrix_;
     mat u_ref_inv_;

     vec gravity_;

     vec  r_;
     mat  m_;

     double tol_;
     std::shared_ptr<quad> quad_ptr_ = nullptr;
     point<3> dummy_;
     double alpha_f_, alpha_m_, gamma_;

  };



}/* namespace GALES */
#endif
