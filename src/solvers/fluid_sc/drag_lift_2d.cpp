#include <mpi.h>
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <iterator>
#include <vector>


#include "../../fem/fem.hpp"
#include "../esmm/esmm.hpp"
#include "fluid.hpp"




using namespace GALES;



int main(int argc, char* argv[])
{


  MPI_Init(&argc, &argv);


  //------------------------ creating directories -----------------------------
  make_dirs("results/coeff");




  const int dim = 2;
  point<dim> dummy;



  //---------------- read setup file -------------------------------
  read_setup setup;  

    

  #include "constructors.hpp"  



  // ----------------------drag lift class object-----------------------------
  ico_f_drag_lift drag_lift;
   
  
  std::vector<double> t_vec, C_d_vec, C_l_vec;

  const double t = setup.pp_start_time();
  while(t < setup.pp_final_time())
  {    
    double t_iteration(MPI_Wtime());    
    print_only_pid<0>(std::cerr)<<"postprocessing at time = "<<t<<" sec.\n";

    time::get().t(t);
    fluid_io.read("fluid_dofs/", *fluid_dof_state);     
  
    double C_d, C_l;
    drag_lift.execute(fluid_model, fluid_props, setup, C_d, C_l);

    t_vec.push_back(t);
    C_d_vec.push_back(C_d);
    C_l_vec.push_back(C_l);
   
    print_only_pid<0>(std::cerr)<<"Time taken for time step = "<<MPI_Wtime()-t_iteration<<"\n"<<"\n";
    t += setup.pp_delta_t();
  }

  std::ofstream ofile;
  ofile.open ("results/coeff/drag_lift.txt");
  for(int i=0; i<t_vec.size(); i++)
    ofile<<t_vec[i]<<"    "<<C_d_vec[i]<<"    "<<C_l_vec[i]<<std::endl;
  ofile.close();  
  
  MPI_Barrier(MPI_COMM_WORLD);
  MPI_Finalize();
  return 0;
}
