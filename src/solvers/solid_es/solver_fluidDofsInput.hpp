#include <mpi.h>
#include <iostream>
 
 
#include "../../fem/fem.hpp"
#include "solid.hpp"





using namespace GALES;




template<int dim>
void solver(read_setup& setup)
{
  #include "constructors.hpp"
  


   
  

  time::get().t(0.0); 
  time::get().delta_t(setup.delta_t());	


  //read fluid dofs at time t = 0
  std::vector<double> f_dofs_0, f_dofs;
  std::string f_file = "input/fluid_dofs/0";
  read_bin(f_file, f_dofs_0);




  if(setup.restart())
  {
     time::get().t(setup.restart_time());
  }
  else
  {
    solid_dofs_ic_bc.dirichlet_bc();
    solid_io.write("solid/u/", *solid_u_dof_state);     
    time::get().tick();
  }






  //---------- starting time of time loop ---------------------
  double time_loop_start = MPI_Wtime();
  int tot_time_iterations(0);


  for(; time::get().t()<=setup.end_time(); time::get().tick())
  {
    double t_iteration(MPI_Wtime());
  
    print_only_pid<0>(std::cerr)<<"simulation time = "<<std::setprecision(setup.precision()) << time::get().t()<<" sec.\n";




       //----------read fluid dofs--------------------------
       std::stringstream ss;
       ss << "input/fluid_dofs/" << time::get().t();
       read_bin(ss.str(), f_dofs);
 
       

       // set fluid dofs in solid_ic_bc       
       solid_ic_bc.set_f_overp(f_dofs_0, f_dofs);
    


        //------------------------ SOLUTION For solid ------------------------
	double fill_time = solid_assembly.execute(solid_integral, solid_dofs_ic_bc, solid_u_model, solid_lp);   
        double solver_time = solid_ls_solver.execute(solid_lp);


     	solid_updater.solution(*solid_u_dof_state, solid_io.state_vec(*solid_ls_solver.solution()));
 

        print_solver_info("SOLID  ", fill_time, solver_time, solid_ls_solver);        
     

        // -----------------------end of solid solution---------------------------------------------------------


    if((tot_time_iterations+1)%(setup.print_freq()) == 0)
    {
      solid_io.write("solid/u/", *solid_u_dof_state);
    }


    print_only_pid<0>(std::cerr)<<"Time taken for time step = "<<MPI_Wtime()-t_iteration<<"\n"<<"\n";
    tot_time_iterations++;    	  
  }

  print_only_pid<0>(std::cerr)<<"End time reached!!!! "<<"\n"<<"\n"<<"\n";
}








int main(int argc, char* argv[])
{

  MPI_Init(&argc, &argv);

   
  //---------- creating directories -----------------------------
  make_dirs("results/solid/u");
  

  //---------------- read setup file -------------------------------
  read_setup setup;  


  if(setup.dim()==2) solver<2>(setup);
  else if(setup.dim()==3) solver<3>(setup);


  MPI_Barrier(MPI_COMM_WORLD);
  MPI_Finalize();
  return 0;
}
