#ifndef SOLID_DOFS_IC_BC_HPP
#define SOLID_DOFS_IC_BC_HPP




#include "../../fem/fem.hpp"



namespace GALES{

 
  template<typename ic_bc_type, int dim>
  class solid_dofs_ic_bc{};
  
  
  
 
  template<typename ic_bc_type>
  class solid_dofs_ic_bc <ic_bc_type, 2>
  {

   public :

   solid_dofs_ic_bc(model<2>& u_model, ic_bc_type& ic_bc, read_setup& setup)
   : 
   u_state_(u_model.state()), mesh_(u_model.mesh()), ic_bc_(ic_bc)
   {
     ic();
   }



    void ic() 
    {
      for(const auto& nd : mesh_.nodes()) 
      {
         const int first_dof_lid (nd->first_dof_lid());
         u_state_.set_dof(first_dof_lid,   ic_bc_.initial_ux(*nd));
         u_state_.set_dof(first_dof_lid+1, ic_bc_.initial_uy(*nd));
      }     
    }


    void dirichlet_bc() 
    {
      std::pair<bool,double> result;

      for(const auto& nd : mesh_.nodes()) 
      {
         const int first_dof_lid (nd->first_dof_lid());

         result = ic_bc_.dirichlet_ux(*nd);
         if (result.first)         u_state_.set_dof(first_dof_lid, result.second);

         result = ic_bc_.dirichlet_uy(*nd);
         if (result.first)         u_state_.set_dof(first_dof_lid+1, result.second);
      }            
    }  



    auto dofs_constraints(const node<2>& nd)const  
    {
      std::vector<std::pair<bool,double>> nd_dofs_constraints(nd.nb_dofs(), std::make_pair(false, 0.0));              
      if(ic_bc_.dirichlet_ux(nd).first) nd_dofs_constraints[0] = std::make_pair(true, ic_bc_.dirichlet_ux(nd).second);
      if(ic_bc_.dirichlet_uy(nd).first) nd_dofs_constraints[1] = std::make_pair(true, ic_bc_.dirichlet_uy(nd).second);      
      return nd_dofs_constraints;
    }


    private:
    dof_state& u_state_;
    Mesh<2>& mesh_;
    ic_bc_type& ic_bc_;    
  };
  








  template<typename ic_bc_type>
  class solid_dofs_ic_bc <ic_bc_type, 3>
  {

   public :

   solid_dofs_ic_bc(model<3>& u_model, ic_bc_type& ic_bc, read_setup& setup)
   : 
   u_state_(u_model.state()), mesh_(u_model.mesh()), ic_bc_(ic_bc)
   {
     ic();
   }


    void ic() 
    {
      for(const auto& nd : mesh_.nodes()) 
      {
         const int first_dof_lid (nd->first_dof_lid());
         u_state_.set_dof(first_dof_lid,   ic_bc_.initial_ux(*nd));
         u_state_.set_dof(first_dof_lid+1, ic_bc_.initial_uy(*nd));
         u_state_.set_dof(first_dof_lid+2, ic_bc_.initial_uz(*nd));
      }     
    }


    void dirichlet_bc() 
    {
      std::pair<bool,double> result;

      for(const auto& nd : mesh_.nodes()) 
      {
         const int first_dof_lid (nd->first_dof_lid());

         result = ic_bc_.dirichlet_ux(*nd);
         if (result.first)         u_state_.set_dof(first_dof_lid, result.second);

         result = ic_bc_.dirichlet_uy(*nd);
         if (result.first)         u_state_.set_dof(first_dof_lid+1, result.second);

         result = ic_bc_.dirichlet_uz(*nd);
         if (result.first)         u_state_.set_dof(first_dof_lid+2, result.second);
      } 
    } 



    auto dofs_constraints(const node<3>& nd)const  
    {
      std::vector<std::pair<bool,double>> nd_dofs_constraints(nd.nb_dofs(), std::make_pair(false, 0.0));              
      if(ic_bc_.dirichlet_ux(nd).first) nd_dofs_constraints[0] = std::make_pair(true, ic_bc_.dirichlet_ux(nd).second);
      if(ic_bc_.dirichlet_uy(nd).first) nd_dofs_constraints[1] = std::make_pair(true, ic_bc_.dirichlet_uy(nd).second);      
      if(ic_bc_.dirichlet_uz(nd).first) nd_dofs_constraints[2] = std::make_pair(true, ic_bc_.dirichlet_uz(nd).second);      
      return nd_dofs_constraints;
    }



    private:
    dof_state& u_state_;
    Mesh<3>& mesh_;
    ic_bc_type& ic_bc_;    
  };



}

#endif



