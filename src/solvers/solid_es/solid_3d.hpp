#ifndef SOLID_3D_HPP
#define SOLID_3D_HPP




#include "../../fem/fem.hpp"



namespace GALES{
 
   
      
   


   
  template<typename ic_bc_type>
  class solid<ic_bc_type, 3>
  {
    using element_type = element<3>;
    using vec = boost::numeric::ublas::vector<double>;
    using mat = boost::numeric::ublas::matrix<double>;


   public :  

    solid
    (
     ic_bc_type& ic_bc,
     solid_properties& props,
     model<3>& model
    ):
            
      ic_bc_(ic_bc),
      props_(props),
      setup_(model.setup()),     

      JNxW_(3,0.0),
      JxW_(0.0),
            
      rho_(props.rho()),
      nu_(props.nu()),
      E_(props.E()),

      D_(6,6,0.0),
      
      gravity_(3,0.0)
      {      
       ic_bc_.body_force(gravity_);         
      }

  
  
  



    void execute(const element_type& el, mat& m, vec& r)
    {
      nb_el_nodes_ = el.nb_nodes();      
      N_.resize(3,3*nb_el_nodes_, false);
      B_.resize(6,nb_el_nodes_*3, false);


      auto nds = el.el_node_vec();

      for(int i=0; i<el.quad_i().size(); i++)
      {
        quad_ptr_ = el.quad_i()[i];
        JxW_ = quad_ptr_->JxW();
        N();
    
        if(props_.heterogeneous())
        {
          props_.properties(nds[i]->get_x(), nds[i]->get_y(), nds[i]->get_z());
          rho_ = props_.rho();
          E_ = props_.E();
          nu_ = props_.nu();
        }


        //------------------------------Mat2---------------------------------------------------------------------------------------------
        B();
        D();
        const mat use1 = prod(D_,B_);
        const mat Mat2 = prod(boost::numeric::ublas::trans(B_),use1)*JxW_;
    
        m += Mat2;
    
    
        //----------------------------vect4------------------------------------------------------------------------------------------------
        const vec Vect4 = prod(boost::numeric::ublas::trans(N_),gravity_)*rho_*JxW_;
         
        r += Vect4;   
      }
      
      

      if(el.on_boundary())
      {
         for(int i=0; i<el.nb_sides(); i++)
         {
           if(el.is_side_on_boundary(i))
           {
              auto bd_nodes = el.side_nodes(i);
              auto side_flag = el.side_flag(i);
    
              if(side_flag==1)
              {            
                 std::vector<std::vector<double>> f_tr;
                 ic_bc_.get_fluid_tr(bd_nodes, f_tr);            
                 
                 for(int j=0; j<el.nb_side_gp(i); j++)
                 {
                   quad_ptr_ = el.quad_b(i,j);
                   
                   N();
                   vec tr(3);
                   for(int i=0; i<3; i++)
                    tr[i] = -f_tr[j][i];
                          
                   r += prod(boost::numeric::ublas::trans(N_), tr)*quad_ptr_->W_bd();
                 }
              }
              else
              {
                 double tau11(0.0), tau22(0.0), tau33(0.0);
                 double tau12(0.0), tau23(0.0), tau13(0.0);
                 double tau21(0.0), tau32(0.0), tau31(0.0);
                 double p(0.0);
             
                 if(ic_bc_.neumann_tau11(bd_nodes,side_flag).first)      tau11 = ic_bc_.neumann_tau11(bd_nodes,side_flag).second;
                 if(ic_bc_.neumann_tau22(bd_nodes,side_flag).first)      tau22 = ic_bc_.neumann_tau22(bd_nodes,side_flag).second;
                 if(ic_bc_.neumann_tau33(bd_nodes,side_flag).first)      tau33 = ic_bc_.neumann_tau33(bd_nodes,side_flag).second; 
                 if(ic_bc_.neumann_tau12(bd_nodes,side_flag).first)      tau12 = ic_bc_.neumann_tau12(bd_nodes,side_flag).second;
                 if(ic_bc_.neumann_tau13(bd_nodes,side_flag).first)      tau13 = ic_bc_.neumann_tau13(bd_nodes,side_flag).second;   
                 if(ic_bc_.neumann_tau23(bd_nodes,side_flag).first)      tau23 = ic_bc_.neumann_tau23(bd_nodes,side_flag).second; 
                 if(ic_bc_.neumann_pressure(bd_nodes,side_flag).first)     p = ic_bc_.neumann_pressure(bd_nodes,side_flag).second; 
                 
                 tau21 = tau12;
                 tau31 = tau13;
                 tau32 = tau23;
    
                 for(const auto& quad_ptr: el.quad_b(i))
                 {
                   quad_ptr_ = quad_ptr;               
                   JNxW_ = quad_ptr_->JNxW();
    
                   vec traction(3,0.0);                       
                   traction[0] = (tau11-p)*JNxW_[0] + tau12*JNxW_[1] + tau13*JNxW_[2];
                   traction[1] = tau21*JNxW_[0] + (tau22-p)*JNxW_[1] + tau23*JNxW_[2];
                   traction[2] = tau31*JNxW_[0] + tau32*JNxW_[1] + (tau33-p)*JNxW_[2];
             
                   N();
                   r += prod(boost::numeric::ublas::trans(N_),traction);
                 }
              }
           }
         }         
      }      
    }










  void D()
  {
    D_.clear();

    const double mu = 0.5*E_/(1.0 + nu_);    
    const double lambda = E_*nu_/((1.0+nu_)*(1.0-2.0*nu_));
    
    D_(0,0) = lambda + 2.0*mu;
    D_(0,1) = lambda;
    D_(0,2) = lambda;
    D_(1,0) = lambda;
    D_(1,1) = lambda + 2.0*mu;
    D_(1,2) = lambda;
    D_(2,0) = lambda;
    D_(2,1) = lambda;
    D_(2,2) = lambda + 2.0*mu;        
    D_(3,3) = mu;
    D_(4,4) = mu;
    D_(5,5) = mu;
  }




   void N()
   {
    N_.clear();
    for(int i=0; i<nb_el_nodes_; i++)
    {
       N_(0,3*i) = quad_ptr_->sh(i);
       N_(1,3*i+1) = quad_ptr_->sh(i);
       N_(2,3*i+2) = quad_ptr_->sh(i);
    }   
   }



   
   void B()
   {
    B_.clear();
    for(int i=0; i<nb_el_nodes_; i++)
    {
       B_(0,3*i) = quad_ptr_->dsh_dx(i);
       B_(1,3*i+1) = quad_ptr_->dsh_dy(i);
       B_(2,3*i+2) = quad_ptr_->dsh_dz(i);          
       B_(3,3*i)   = quad_ptr_->dsh_dy(i);
       B_(3,3*i+1) = quad_ptr_->dsh_dx(i);
       B_(4,3*i+1) = quad_ptr_->dsh_dz(i);
       B_(4,3*i+2) = quad_ptr_->dsh_dy(i);     
       B_(5,3*i)   = quad_ptr_->dsh_dz(i);
       B_(5,3*i+2) = quad_ptr_->dsh_dx(i);
    }   
   }





   private:

    int nb_el_nodes_;
    
    ic_bc_type& ic_bc_;
    solid_properties& props_;
    read_setup& setup_;

    double JxW_; 
    vec JNxW_;

    double rho_, nu_, E_;

    mat N_;
    mat D_;      
    mat B_;

    vec gravity_;   

    std::shared_ptr<quad> quad_ptr_ = nullptr;
    point<3> dummy_;    
  }; 






 
} 

#endif

