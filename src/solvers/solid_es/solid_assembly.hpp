#ifndef SOLID_ASSEMBLY_HPP
#define SOLID_ASSEMBLY_HPP




#include "../../fem/fem.hpp"



namespace GALES{



  template<typename integral_type, typename dofs_ic_bc_type, int dim>
  class solid_assembly
  {
      using model_type = model<dim>;
      using vec = boost::numeric::ublas::vector<double>;
      using mat = boost::numeric::ublas::matrix<double>;
    
    public:

    double execute(integral_type& integral, dofs_ic_bc_type& dofs_ic_bc, model_type& u_model, linear_system& lp)
    {
      double start(MPI_Wtime());      
      
      lp.clear();
      for(const auto& el : u_model.mesh().elements())
      {
        const int nb_dofs = el->nb_dofs();
      
        vec r(nb_dofs, 0.0);
        mat m(nb_dofs, nb_dofs, 0.0);

	integral.execute(*el, m, r);
	lp.assemble(el->dofs_gids(), el->dofs_constraints(dofs_ic_bc), m, r);	
      }
      
      lp.assembled();       
      return MPI_Wtime()-start;                  
    }
  
  };



}

#endif
