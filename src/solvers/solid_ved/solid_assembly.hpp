#ifndef SOLID_ASSEMBLY_HPP
#define SOLID_ASSEMBLY_HPP



#include "../../fem/fem.hpp"



namespace GALES{
	



  template<typename integral_type, typename scatter_type, int dim>
  class solid_assembler_interior
  {
      using model_type = model<dim>;
      using vec = boost::numeric::ublas::vector<double>;
      using mat = boost::numeric::ublas::matrix<double>;
    
    public:
    solid_assembler_interior
    (
     integral_type& integral,
     scatter_type& sct,
     model_type& u_model, 
     model_type& v_model, 
     model_type& a_model, 
     model_type& history_model
    ):
      integral_(integral),
      scatter_(sct),
      u_model_(u_model),
      v_model_(v_model),
      a_model_(a_model),
      history_model_(history_model)
      {}
    

    template<typename T>
    void operator()(T& lp)const
    {      
      const auto& mesh(u_model_.mesh());
      auto& props_(u_model_.props());
      std::vector<vec> dofs_u;
      std::vector<vec> dofs_v;
      std::vector<vec> dofs_a;
      
      
      for(const auto& el : mesh.bd_elements())
      {
	u_model_.extract_element_dofs(*el, dofs_u);
	v_model_.extract_element_dofs(*el, dofs_v);
	a_model_.extract_element_dofs(*el, dofs_a);
	
        vec PP_history;
	history_model_.extract_element_PP_history_dofs(*el, props_.s_nb_Maxwell_el_, PP_history);

	mat m(dofs_u[0].size(), dofs_u[0].size(), 0.0);
	vec r(dofs_u[0].size(), 0.0);

        vec P_history;
        P_history.resize(PP_history.size());

	integral_.execute_i(*el, dofs_u, dofs_v, dofs_a, PP_history, m, r, P_history);

        const int el_gid(el->gid());
        const int k = P_history.size();
        for(int j=el_gid*k, l=0; j<el_gid*k+k; j++) 
        {	
	  history_model_.state().set_dof(j, P_history[l], 1);  
	  l++;
        }   

	scatter_.execute(*el, mesh, lp, m, r);


	if(el->on_boundary())
	{
	  r.clear();
	  m.clear();
 	  integral_.execute_b(*el, dofs_u, r);
	  scatter_.execute(*el, mesh, lp, m, r);	  
	}
      }
    }
  
  private:

    integral_type& integral_;
    scatter_type& scatter_;
    model_type& u_model_;
    model_type& v_model_;
    model_type& a_model_;
    model_type& history_model_;
  };



}

#endif
