#ifndef SOLID_DOFS_IC_HPP
#define SOLID_DOFS_IC_HPP



#include "../../fem/fem.hpp"




namespace GALES{



  template <int dim, typename ic_bc_type>
  class solid_u_dofs_ic
  {
    using model_type = model<dim>;

    public :  

     solid_u_dofs_ic(model_type& s_model, ic_bc_type& ic_bc, const point<dim>& dummy): 
     state_(s_model.state()), mesh_(s_model.mesh()), ic_bc_(ic_bc)
     {
       generate(dummy);
     }



    void generate(const point_2d& dummy) 
    {
      for(const auto& nd : mesh_.nodes()) 
      {
         int first_dof_lid (nd->first_dof_lid());
         state_.set_dof(first_dof_lid,   ic_bc_.initial_ux(nd->coord()));
         state_.set_dof(first_dof_lid+1, ic_bc_.initial_uy(nd->coord()));
      }     
    }
  

    void generate(const point_3d& dummy) 
    {
      for(const auto& nd : mesh_.nodes()) 
      {
         int first_dof_lid (nd->first_dof_lid());
         state_.set_dof(first_dof_lid,   ic_bc_.initial_ux(nd->coord()));
         state_.set_dof(first_dof_lid+1, ic_bc_.initial_uy(nd->coord()));
         state_.set_dof(first_dof_lid+2, ic_bc_.initial_uz(nd->coord()));
      }     
    }


    private:
    dof_state& state_;
    simple_mesh<dim>& mesh_;
    ic_bc_type& ic_bc_;  
  };







  template <int dim, typename ic_bc_type>
  class solid_v_dofs_ic
  {
    using model_type = model<dim>;

    public :  

     solid_v_dofs_ic(model_type& s_model, ic_bc_type& ic_bc, const point<dim>& dummy): 
     state_(s_model.state()), mesh_(s_model.mesh()), ic_bc_(ic_bc)
     {
       generate(dummy);
     }



    void generate(const point_2d& dummy) 
    {
      for(const auto& nd : mesh_.nodes()) 
      {
         const int first_dof_lid (nd->first_dof_lid());
         state_.set_dof(first_dof_lid,   ic_bc_.initial_vx(nd->coord()));
         state_.set_dof(first_dof_lid+1, ic_bc_.initial_vy(nd->coord()));
      }     
      state_.dofs(1) = state_.dofs(0);
    }
  

    void generate(const point_3d& dummy) 
    {
      for(const auto& nd : mesh_.nodes()) 
      {
         const int first_dof_lid (nd->first_dof_lid());
         state_.set_dof(first_dof_lid,   ic_bc_.initial_vx(nd->coord()));
         state_.set_dof(first_dof_lid+1, ic_bc_.initial_vy(nd->coord()));
         state_.set_dof(first_dof_lid+2, ic_bc_.initial_vz(nd->coord()));
      }     
      state_.dofs(1) = state_.dofs(0);
    }

    
    
    private:
    dof_state& state_;
    simple_mesh<dim>& mesh_;
    ic_bc_type& ic_bc_;  
  };



}

#endif


