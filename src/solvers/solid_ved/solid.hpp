#ifndef SOLID_HPP
#define SOLID_HPP



#include "solid_dofs_ic.hpp"
#include "solid_dofs_bc.hpp"
#include "solid_assembly.hpp"
#include "solid_updater.hpp"
#include "history/history.hpp"
#include "solid_3d.hpp"
#include "solid_2d.hpp"


#endif 
