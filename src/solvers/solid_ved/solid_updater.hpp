#ifndef SOLID_UPDATER_HPP
#define SOLID_UPDATER_HPP


#include "../../fem/fem.hpp"




namespace GALES{



  struct solid_updater
  {

    using vec = boost::numeric::ublas::vector<double>;

      solid_updater(dof_state& u_state, dof_state& v_state, dof_state& a_state, read_setup& setup)
      :
      u_state_(u_state), v_state_(v_state), a_state_(a_state), setup_(setup)
      {
        const double alpha_m = 0.5*(3.0-setup.s_rho_inf())/(1.0+setup.s_rho_inf());
        const double alpha_f = 1.0/(1.0+setup.s_rho_inf());
        gamma_ = 0.5 + alpha_m - alpha_f;
        beta_ = 0.25*std::pow(1.0 + alpha_m - alpha_f, 2);                 
      }




    void predictor() 
    {
          const double dt(time::get().delta_t());  

          if(setup_.zero_a())
          {    
                 a_state_.dofs(1) = a_state_.dofs(0); //P  = I, shift in time
                 a_state_.dofs(0).clear();
      
                 v_state_.dofs(1) = v_state_.dofs(0); //P  = I, shift in time
                 v_state_.dofs(0) = v_state_.dofs(1) + (1-gamma_)*dt*a_state_.dofs(1);

                 u_state_.dofs(1) = u_state_.dofs(0); //P  = I, shift in time
                 u_state_.dofs(0) = u_state_.dofs(1) + dt*v_state_.dofs(1) + dt*dt*0.5*(1-2*beta_)*a_state_.dofs(1);
           }
           else if(setup_.constant_v())
           {
                 v_state_.dofs(1) = v_state_.dofs(0); //P  = I, shift in time
                 v_state_.dofs(0) = v_state_.dofs(1);

                 a_state_.dofs(1) = a_state_.dofs(0); //P  = I, shift in time
                 a_state_.dofs(0) = (gamma_-1)*a_state_.dofs(1)/gamma_;
      
                 u_state_.dofs(1) = u_state_.dofs(0); //P  = I, shift in time
                 u_state_.dofs(0) = u_state_.dofs(1) + dt*v_state_.dofs(1) + dt*dt*0.5*((1-2*beta_)*a_state_.dofs(1)+ 2*beta_*a_state_.dofs(0));
           }     
    } 




    void corrector(const vec& correction)
    {
           const double dt(time::get().delta_t());  
      
	   a_state_.dofs(0) = a_state_.dofs(0) + correction;
	   v_state_.dofs(0) = v_state_.dofs(0) + gamma_*dt*correction;
	   u_state_.dofs(0) = u_state_.dofs(0) + beta_*dt*dt*correction;
    }




    private:
    dof_state& u_state_;
    dof_state& v_state_;
    dof_state& a_state_;
    read_setup& setup_;
    double beta_, gamma_;
  }; 



}

#endif


