#ifndef HISTORY_UPDATER_HPP
#define HISTORY_UPDATER_HPP


#include"../../../fem/fem.hpp"


namespace GALES{


  struct history_updater 
  {

    history_updater(dofs_history_state& state): state_(state){}
    

    void initialize() 
    {
	state_.dofs(2) = state_.dofs(0);
	state_.dofs(1) = state_.dofs(0);
    }
 
 
    void predictor() 
    {
	state_.dofs(2) = state_.dofs(1);
    }
    
    
    private:
    dofs_history_state& state_;
    
   };


}

#endif

