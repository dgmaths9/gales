#ifndef SOLID_DOFS_BC_HPP
#define SOLID_DOFS_BC_HPP




#include "../../fem/fem.hpp"



namespace GALES{

 
 
  template<int dim, typename ic_bc_type>
  class solid_dofs_bc
  {

   using model_type = model<dim>;
    
   public :

   solid_dofs_bc
   (
      model_type& u_model, 
      model_type& v_model, 
      model_type& a_model, 
      ic_bc_type& ic_bc, 
      read_setup& setup
   )
   : 
   u_state_(u_model.state()), 
   v_state_(v_model.state()), 
   a_state_(a_model.state()), 
   mesh_(u_model.mesh()), 
   ic_bc_(ic_bc)
   {
        const double alpha_m = 0.5*(3.0-setup.s_rho_inf())/(1.0+setup.s_rho_inf());
        const double alpha_f = 1.0/(1.0+setup.s_rho_inf());
        gamma_ = 0.5 + alpha_m - alpha_f;
        beta_ = 0.25*std::pow(1.0 + alpha_m - alpha_f, 2);                 
   }




    void dirichlet_bc(point_2d& dummy) 
    {
      std::pair<bool,double> result;
      const double dt(time::get().delta_t());  

      for(const auto& nd : mesh_.nodes()) 
      {
         const int first_dof_lid (nd->first_dof_lid());

         result = ic_bc_.dirichlet_ux(*nd);
         if (result.first)         u_state_.set_dof(first_dof_lid, result.second);

         result = ic_bc_.dirichlet_uy(*nd);
         if (result.first)         u_state_.set_dof(first_dof_lid+1, result.second);
      }
            
      a_state_.dofs(0) = (u_state_.dofs(0)-u_state_.dofs(1))/(dt*dt*beta_) - v_state_.dofs(1)/(dt*beta_) - (1-2*beta_)/(2*beta_)*a_state_.dofs(1);
      v_state_.dofs(0) = v_state_.dofs(1) + dt*((1-gamma_)*a_state_.dofs(1) + gamma_*a_state_.dofs(0)); 
    }  



    void dirichlet_bc(point_3d& dummy) 
    {
      std::pair<bool,double> result;
      const double dt(time::get().delta_t());  

      for(const auto& nd : mesh_.nodes()) 
      {
         const int first_dof_gid (nd->first_dof_gid());

         result = ic_bc_.dirichlet_ux(*nd);
         if (result.first)         u_state_.set_dof(first_dof_lid, result.second);

         result = ic_bc_.dirichlet_uy(*nd);
         if (result.first)         u_state_.set_dof(first_dof_lid+1, result.second);

         result = ic_bc_.dirichlet_uz(*nd);
         if (result.first)         u_state_.set_dof(first_dof_lid+2, result.second);
      } 
      
      a_state_.dofs(0) = (u_state_.dofs(0)-u_state_.dofs(1))/(dt*dt*beta_) - v_state_.dofs(1)/(dt*beta_) - (1-2*beta_)/(2*beta_)*a_state_.dofs(1);
      v_state_.dofs(0) = v_state_.dofs(1) + dt*((1-gamma_)*a_state_.dofs(1) + gamma_*a_state_.dofs(0)); 
    } 






   //--------------------------------- These are for fixing dofs in scatter -----------------------------------------
    template<typename nd_type>
    auto dirichlet(int dof, const nd_type &nd, const point_2d& dummy) const
    {
      const int nb_dofs = 2;
      const int n = dof%nb_dofs;
      std::pair<bool,double> result(false, 0.0);
      std::pair<bool,double> value(false, 0.0);

      switch(n)
      {
          case 0:
              result = ic_bc_.dirichlet_ux(nd);
              if(result.first == true) value = std::make_pair(true, 0.0);
              break;
          case 1:
              result = ic_bc_.dirichlet_uy(nd);
              if(result.first == true) value = std::make_pair(true, 0.0);
              break;
      }
      return value;
    }




    template<typename nd_type>
    auto dirichlet(int dof, const nd_type &nd, const point_3d& dummy) const
    {
      const int nb_dofs = 3;
      const int n = dof%nb_dofs;
      std::pair<bool,double> result(false, 0.0);
      std::pair<bool,double> value(false, 0.0);

      switch(n)
      {
          case 0:
              result = ic_bc_.dirichlet_ux(nd);
              if(result.first == true) value = std::make_pair(true, 0.0);
              break;
          case 1:
              result = ic_bc_.dirichlet_uy(nd);
              if(result.first == true) value = std::make_pair(true, 0.0);
              break;
          case 2:
              result = ic_bc_.dirichlet_uz(nd);
              if(result.first == true) value = std::make_pair(true, 0.0);
              break;
      }
      return value;
    }




    private:
    dof_state& u_state_;
    dof_state& v_state_;
    dof_state& a_state_;
    simple_mesh<dim>& mesh_;
    ic_bc_type& ic_bc_;
    double gamma_, beta_;
  };
  


}

#endif



