#include <mpi.h>
#include <iostream>


#include "../../fem/fem.hpp"
#include "solid.hpp"




using namespace GALES;
typedef boost::numeric::ublas::vector<double> v_type;


int main(int argc, char* argv[])
{

  MPI_Init(&argc, &argv);


  //---------- creating directories -----------------------------
  make_dirs("results/solid/u results/solid/v results/solid/a results/solid/history");


  

  //---------------- read setup file -------------------------------
  read_setup setup;


  
  const int dim = 2;
  point<dim> dummy;
  #include "typedefs.hpp"



  const int nb_dofs_s = dim;

  //------------------ space meshes building --------------------------------------------------------------------
  print_only_pid<0>(std::cerr)<< "solid MESH\n";
  double mesh_read_start = MPI_Wtime();
  simple_mesh<dim> solid_mesh(setup.solid_mesh_file(), nb_dofs_s, setup, "s");
  print_only_pid<0>(std::cerr)<<"Mesh reading took: "<<std::setprecision(setup.precision()) << MPI_Wtime()-mesh_read_start<<" s\n\n";




  physical_properties props;


  #include "constructors.hpp"
  
 

  time::get().t(0.0); 
  time::get().delta_t(setup.delta_t());	

  double restart_or_t_zero = MPI_Wtime();  
  if(!setup.restart())
  {
    //--------- setting up dofs for time = 0 ----------------
    solid_dofs_bc.dirichlet_bc(dummy);
    solid_io.write("solid/u/", *solid_u_dof_state);
    solid_io.write("solid/v/", *solid_v_dof_state);
    solid_io.write("solid/a/", *solid_a_dof_state);
    history_io.write("results/solid/history/", *history_dof_state, 1);
    print_only_pid<0>(std::cerr)<<"Dofs writing for time 0 took: "<<std::setprecision(setup.precision()) << MPI_Wtime()-restart_or_t_zero<<" s\n\n";       
  } 
  else
  {
    //--------------read restart-----------------------------    
     time::get().t(setup.restart_time());
     solid_io.read("solid/u/", *solid_u_dof_state);
     solid_io.read("solid/v/", *solid_v_dof_state);
     solid_io.read("solid/a/", *solid_a_dof_state);
     history_io.read("results/solid/history/", *history_dof_state, 2);
    print_only_pid<0>(std::cerr)<<"Dofs reading at restart time took: "<<std::setprecision(setup.precision()) << MPI_Wtime()-restart_or_t_zero<<" s\n\n";       
  }




  //---------- starting time of time loop ---------------------
  double time_loop_start = MPI_Wtime();
  double non_linear_res(0.0);
  int tot_time_iterations(0);

  while(time::get().t() < setup.end_time())
  {
    double t_iteration(MPI_Wtime());

    time::get().tick();    
    print_only_pid<0>(std::cerr)<<"simulation time:  "<<std::setprecision(setup.precision()) << time::get().t()<<" s.\n";


    solid_updater.predictor();
    history_updater.predictor();


    int it_count(0);

    while(it_count < setup.n_max_it())
    {
       //------------------------ SOLUTION For solid ------------------------
        if(setup.t_dependent_dirichlet_bc())
           solid_dofs_bc.dirichlet_bc(dummy);

        linear_system solid_lp;


        double before_fill_time(MPI_Wtime());
        solid_filler.fill(solid_lp, *solid_maps.dof_map(), solid_assembler);
        double fill_time(MPI_Wtime()-before_fill_time);

        
        double before_solver_time(MPI_Wtime());
        bool residual_converged = solid_res_check.execute(*solid_lp.rhs(), it_count, non_linear_res);      
        if(residual_converged)
        {
          print_only_pid<0>(std::cerr) << "SOLID  it: " << parse(it_count,5) << "Assembly: " << parse(fill_time,15) << "NonLinearRes: " << parse(non_linear_res,15) << "\n";        
      	  break;
        } 	      
        solid_ls_solver.execute(*solid_lp.matrix(), *solid_lp.rhs());
        double solver_time(MPI_Wtime()-before_solver_time);


        solid_updater.corrector(solid_io.state_vec(*solid_ls_solver.solution()));


        print_only_pid<0>(std::cerr) 
        << "SOLID  it: " << parse(it_count,5) << "Assembly: " << parse(fill_time,15) << "NonLinearRes: " << parse(non_linear_res,15)<< "Solver: " << parse(solver_time,15)
        << "AbsResErr: " << parse(solid_ls_solver.ARE(),15) << "RelResErr: "<< parse(solid_ls_solver.RRE(),15)
        << "Num_it: " << parse(solid_ls_solver.num_it(),8) << "dt: "<< time::get().delta_t()
        << "\n";

       // -----------------------end of solid solution---------------------------------------------------------

     	it_count++;
     }




    if((tot_time_iterations+1)%(setup.print_freq()) == 0)
    {
       solid_io.write("solid/u/", *solid_u_dof_state);
       solid_io.write("solid/v/", *solid_v_dof_state);
       solid_io.write("solid/a/", *solid_a_dof_state);
       history_io.write("results/solid/history/", *history_dof_state, 1);
    }


    print_only_pid<0>(std::cerr)<<"Time step took: "<<MPI_Wtime()-t_iteration<<" s\n\n";
    tot_time_iterations++;    	  
  }


  print_only_pid<0>(std::cerr)<<"End time reached!!!! "<<"\n\n";
  print_only_pid<0>(std::cerr)<<"Total run time: "<<MPI_Wtime()-time_loop_start<<" s\n\n\n";

  MPI_Barrier(MPI_COMM_WORLD);
  MPI_Finalize();
  return 0;
}
