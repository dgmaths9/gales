
//------------  ic bc type definition -------------------------------
typedef solid_ic_bc<dim> solid_ic_bc_type;


//------------  ic conditions type definition --------------------------
typedef solid_u_dofs_ic<dim, solid_ic_bc_type> solid_u_dofs_ic_type;
typedef solid_v_dofs_ic<dim, solid_ic_bc_type> solid_v_dofs_ic_type;



//------------ bc type definition --------------------------
typedef solid_dofs_bc<dim, solid_ic_bc_type > solid_dofs_bc_type;



//----------------- scatters type definition ------------------------------
typedef scatter<solid_dofs_bc_type> solid_scatter_type;



//---------- integrals type definitions ----------------------
typedef solid<solid_ic_bc_type, dim> solid_integral_type;



//---------- assembler type definitions------------
typedef solid_assembler<solid_integral_type, solid_scatter_type, dim> solid_assembler_type;









epetra_maps solid_maps(solid_mesh);


//----------------------------------- dofs state-----------------------------------------
auto solid_u_dof_state = std::make_unique<dof_state>(*solid_maps.state_map(), 2);
auto solid_v_dof_state = std::make_unique<dof_state>(*solid_maps.state_map(), 2);
auto solid_a_dof_state = std::make_unique<dof_state>(*solid_maps.state_map(), 2);
auto history_dof_state = std::make_unique<dofs_history_state>(solid_mesh, props, 3);




//-------------- models construction --------------------------------------------------------------
model<dim> solid_u_model(solid_mesh, *solid_u_dof_state, props, solid_maps);
model<dim> solid_v_model(solid_mesh, *solid_v_dof_state, props, solid_maps);
model<dim> solid_a_model(solid_mesh, *solid_a_dof_state, props, solid_maps);
model<dim> history_model(solid_mesh, *history_dof_state, props);





//--------- ic bc construction -----------
solid_ic_bc_type solid_ic_bc;



//--------- ic construction-------------------
solid_u_dofs_ic_type solid_u_dofs_ic(solid_u_model, solid_ic_bc, dummy);
solid_v_dofs_ic_type solid_v_dofs_ic(solid_v_model, solid_ic_bc, dummy);


//--------- bc construction -----------
solid_dofs_bc_type solid_dofs_bc(solid_u_model, solid_v_model, solid_a_model, solid_ic_bc, setup);



// ------ construction of linear solver -----------------------------
belos_linear_solver solid_ls_solver(setup);




//------------------------residual check------------------------------
residual_check solid_res_check;



//------ scatters construction --------------------------------
solid_scatter_type solid_scatter(solid_dofs_bc);


//--------------------solid integral construction -----------------
solid_integral_type solid_integral(solid_ic_bc, props, setup);




//---------solid linear system filler construction ---------------
linear_system_filler solid_filler;



//----------state updater construction ---------------------------------
solid_updater solid_updater(*solid_u_dof_state, *solid_v_dof_state, *solid_a_dof_state, setup);
history_updater history_updater(*history_dof_state);



//----------- assembler construction -----------------------
solid_assembler_type solid_assembler
(
  solid_integral,
  solid_scatter,
  solid_u_model,
  solid_v_model,
  solid_a_model,
  history_model
);



// ------------------parallel I/O---------------------------------------------------
IO solid_io(*solid_maps.state_map(), *solid_maps.dof_map());

// ------------------history parallel I/O---------------------------------------------------
IO history_io(history_dof_state->map());


