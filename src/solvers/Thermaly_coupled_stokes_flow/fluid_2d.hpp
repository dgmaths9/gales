#ifndef _FLUID_2D_HPP_
#define _FLUID_2D_HPP_



#include "../../fem/fem.hpp"




namespace GALES
{



  template<typename ic_bc_type, int dim>
  class fluid{};

    



  template<typename ic_bc_type>
  class fluid<ic_bc_type, 2>
  {
    using element_type = element<2>;
    using vec = boost::numeric::ublas::vector<double>;
    using mat = boost::numeric::ublas::matrix<double>;

    public :

    fluid(ic_bc_type& ic_bc, fluid_properties& props, model<2>& model)
    :
    nb_dof_fluid_(model.mesh().nb_nd_dofs()),

    ic_bc_(ic_bc),
    props_(props),
    setup_(model.setup()),

    JNxW_(2,0.0),
    JxW_(0.0),

    I_(nb_dof_fluid_,0.0),
    dI_dx_(nb_dof_fluid_,0.0),
    dI_dy_(nb_dof_fluid_,0.0),

    I_T_(0.0),

    beta_(0.0),
    alpha_(0.0),
    rho_(0.0),
    mu_(0.0),

    F1_adv_(nb_dof_fluid_,0.0),
    F2_adv_(nb_dof_fluid_,0.0),
    F1_dif_(nb_dof_fluid_,0.0),
    F2_dif_(nb_dof_fluid_,0.0),
    S_(nb_dof_fluid_,0.0),

    A1_(nb_dof_fluid_,nb_dof_fluid_,0.0),
    A2_(nb_dof_fluid_,nb_dof_fluid_,0.0),
    K11_(nb_dof_fluid_,nb_dof_fluid_,0.0),
    K12_(nb_dof_fluid_,nb_dof_fluid_,0.0),
    K21_(nb_dof_fluid_,nb_dof_fluid_,0.0),
    K22_(nb_dof_fluid_,nb_dof_fluid_,0.0),

    tau_(nb_dof_fluid_,nb_dof_fluid_,0.0),
    gravity_(2,0.0),

    tol_(std::numeric_limits<double>::epsilon())
    {
       ic_bc_.body_force(gravity_);
    }





     void assign(const vec &v, double &p, double &vx, double &vy)
     {
        p =  v[0];        vx = v[1];        vy = v[2];       
     }





 
     void get_properties()
     {
         rho_ = props_.rho();
         mu_ = props_.mu();
         beta_ = props_.beta();        
         alpha_ = props_.alpha();        
     }
 




     void interpolation()
     {
          quad_ptr_->interpolate(I_orig_fluid_,I_);
          quad_ptr_->x_derivative_interpolate(I_orig_fluid_,dI_dx_);
          quad_ptr_->y_derivative_interpolate(I_orig_fluid_,dI_dy_);     
          quad_ptr_->interpolate(I_orig_T_, I_T_);
     }
 
        


     

     void flux_matrices_and_vectors()
     {
              double p,vx,vy;
              assign(I_,p,vx,vy);
              const double shear_rate = compute_shear_rate(dI_dx_, dI_dy_);
              props_.properties(p, I_T_, shear_rate);           
              get_properties();
              flux_matrices_and_vectors(p,vx,vy);
              tau_matrix(vx,vy);     
     }
 




     void flux_matrices_and_vectors(const std::vector<int>& bd_nodes, int side_flag)
     {
              double p,vx,vy;
              assign(I_,p,vx,vy);
              const double shear_rate = compute_shear_rate(dI_dx_, dI_dy_);
              props_.properties(p, I_T_, shear_rate);           
              get_properties();
              flux_matrices_and_vectors(p,vx,vy,bd_nodes, side_flag);
     }


 



     void set_el_data_size()
     {
         I_orig_fluid_.resize(nb_dof_fluid_*nb_el_nodes_, false);

         r_.resize(nb_dof_fluid_*nb_el_nodes_, false);
         m_.resize(nb_dof_fluid_*nb_el_nodes_, nb_dof_fluid_*nb_el_nodes_, false);
     }
    
 
 


     // This is for Euler-backward method
     void execute(const element_type& el, const std::vector<vec>& dofs_fluid, const vec& dofs_T, mat& m, vec& r)
     {
         nb_el_nodes_ = el.nb_nodes();      
         set_el_data_size();

         I_orig_fluid_ = dofs_fluid[0];
         I_orig_T_ = dofs_T;

         r_.clear();
         m_.clear();

         for(int i=0; i<el.quad_i().size(); i++)
         {
           quad_ptr_ = el.quad_i(i);
           JxW_ = quad_ptr_->JxW();   
           interpolation();
           flux_matrices_and_vectors();   
           RM_i();
         }

         if(el.on_boundary())
         {
            for(int i=0; i<el.nb_sides(); i++)
            {
              if(el.is_side_on_boundary(i))
              {
                auto bd_nodes = el.side_nodes(i);
                auto side_flag = el.side_flag(i);
                
                for(int j=0; j<el.quad_b(i).size(); j++)                
                {
                  quad_ptr_ = el.quad_b(i,j);               
                  JNxW_ = quad_ptr_->JNxW();      
                  interpolation();
                  flux_matrices_and_vectors(bd_nodes, side_flag);
                  RM_b();
                }
              }
            }            
         }
         m = m_;
         r = -r_;
     }
 
 
 





     // This is for Euler-backward method (ALE); element interior calculation 
     void RM_i()
     {     
         for (int b=0; b<nb_el_nodes_; b++)
         for (int jb=0; jb<nb_dof_fluid_; jb++)
         {
             r_[nb_dof_fluid_*b+jb] += quad_ptr_->dsh_dx(b)*(F1_dif_[jb]-F1_adv_[jb])*JxW_;
             r_[nb_dof_fluid_*b+jb] += quad_ptr_->dsh_dy(b)*(F2_dif_[jb]-F2_adv_[jb])*JxW_;
             r_[nb_dof_fluid_*b+jb] -= quad_ptr_->sh(b)*S_[jb]*JxW_;
         }

         for(int b=0; b<nb_el_nodes_; b++)
         for(int jb=0; jb<nb_dof_fluid_; jb++)
         for(int a=0; a<nb_el_nodes_; a++)
         for(int ja=0; ja<nb_dof_fluid_; ja++)
         {
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) -= quad_ptr_->dsh_dx(b)* A1_(jb,ja) * quad_ptr_->sh(a)*JxW_;
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) -= quad_ptr_->dsh_dy(b)* A2_(jb,ja) * quad_ptr_->sh(a)*JxW_;
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += quad_ptr_->dsh_dx(b)* K11_(jb,ja) * quad_ptr_->dsh_dx(a)*JxW_;
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += quad_ptr_->dsh_dx(b)* K12_(jb,ja) * quad_ptr_->dsh_dy(a)*JxW_;
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += quad_ptr_->dsh_dy(b)* K21_(jb,ja) * quad_ptr_->dsh_dx(a)*JxW_;
             m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += quad_ptr_->dsh_dy(b)* K22_(jb,ja) * quad_ptr_->dsh_dy(a)*JxW_;
         }

         //--------------------------------------------R_LeastSquares--------------------------------------------------------------------------        
         vec Res_vec = prod(A1_,dI_dx_) 
                     + prod(A2_,dI_dy_) 
                     - S_;
                           
                           
         for(int b=0; b<nb_el_nodes_; b++)
         {
             const mat first_part = A1_*quad_ptr_->dsh_dx(b) 
                                  + A2_*quad_ptr_->dsh_dy(b);
             const mat first_part_tau = prod(first_part, tau_);
             const vec use = prod(first_part_tau, Res_vec);
             for(int jb=0; jb<nb_dof_fluid_; jb++)
             r_[nb_dof_fluid_*b+jb] += use[jb]*JxW_;
         }

         //--------------------------------------------M_LeastSquares--------------------------------------------------------------------------
         for(int a=0; a<nb_el_nodes_; a++)
         {
             mat Res_mat = A1_*quad_ptr_->dsh_dx(a) 
                         + A2_*quad_ptr_->dsh_dy(a);
                                
             for(int b=0; b<nb_el_nodes_; b++)
             {
                 const mat first_part = A1_*quad_ptr_->dsh_dx(b) 
                                      + A2_*quad_ptr_->dsh_dy(b);
                 const mat first_part_tau = prod(first_part, tau_);
                 const mat use = prod(first_part_tau, Res_mat);
                 for(int jb=0; jb<nb_dof_fluid_; jb++)
                 for(int ja=0; ja<nb_dof_fluid_; ja++)
                 m_(nb_dof_fluid_*b+jb, nb_dof_fluid_*a+ja) += use(jb,ja)*JxW_;
             }
         }

     }







     // This is for Euler-backward method (ALE); bd element calculation
     void RM_b()
     {
         for(int b=0;b<nb_el_nodes_;b++)
         for(int j=0;j<nb_dof_fluid_;j++)
         {
             r_[nb_dof_fluid_*b+j] += quad_ptr_->sh(b)*(F1_adv_[j] - F1_dif_[j])*JNxW_[0];
             r_[nb_dof_fluid_*b+j] += quad_ptr_->sh(b)*(F2_adv_[j] - F2_dif_[j])*JNxW_[1];
         }

         for(int b=0; b<nb_el_nodes_; b++)
         for(int jb=0;jb<nb_dof_fluid_;jb++)
         for(int a=0; a<nb_el_nodes_; a++)
         for(int ja=0;ja<nb_dof_fluid_;ja++)
         {
            m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += quad_ptr_->sh(b)*A1_(jb,ja)*quad_ptr_->sh(a)*JNxW_[0]; 
            m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) += quad_ptr_->sh(b)*A2_(jb,ja)*quad_ptr_->sh(a)*JNxW_[1]; 
            m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) -= quad_ptr_->sh(b)*K11_(jb,ja)*quad_ptr_->dsh_dx(a)*JNxW_[0]; 
            m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) -= quad_ptr_->sh(b)*K12_(jb,ja)*quad_ptr_->dsh_dy(a)*JNxW_[0]; 
            m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) -= quad_ptr_->sh(b)*K21_(jb,ja)*quad_ptr_->dsh_dx(a)*JNxW_[1]; 
            m_(nb_dof_fluid_*b+jb,nb_dof_fluid_*a+ja) -= quad_ptr_->sh(b)*K22_(jb,ja)*quad_ptr_->dsh_dy(a)*JNxW_[1]; 
         }
     }










     void flux_matrices_and_vectors(double p, double vx, double vy)
     {
         const double drho_dp = rho_*beta_;
         

         if(setup_.Boussinesq_approximation())
         {
            F1_adv_[0] = vx;
            F1_adv_[1] = p;         
 
            A1_(0,1) = 1.0;
            A1_(1,0)  = 1.0;
 
   
            F2_adv_[0] = vy;
            F2_adv_[2] = p;
   
            A2_(0,2) = 1.0;
            A2_(2,0)  = 1.0;

//            rho_ = setup_.rho_ref();
//            double x = rho_*(1.0 - alpha_*(I_T_ - setup_.T_ref()) );            
//            S_[1] = gravity_[0]*x;
//            S_[2] = gravity_[1]*x;

            double x = gravity_[1]*(I_T_ - setup_.T_ref());
            S_[2] = x;
         }
         else
         {               
            F1_adv_[0] = rho_*vx;
            F1_adv_[1] = p;
   
            A1_(0,0)  = drho_dp*vx;
            A1_(0,1) = rho_;
            A1_(1,0)  = 1.0;
   
   
            F2_adv_[0] = rho_*vy;
            F2_adv_[2] = p;
   
            A2_(0,0)  = drho_dp*vy;
            A2_(0,2) = rho_;
            A2_(2,0)  = 1.0;
           
          
            S_[1] = rho_*gravity_[0];
            S_[2] = rho_*gravity_[1];
         }
              
      
         K11_.clear(); K12_.clear(); 
         K21_.clear(); K22_.clear(); 

         const double lambda = -2.0*mu_/3.0;

         K11_(1,1) = lambda + 2*mu_;
         K11_(2,2) = mu_;

         K12_(1,2) = lambda;
         K12_(2,1) =  mu_;

         K21_(1,2) = mu_;
         K21_(2,1) = lambda;

         K22_(1,1) = mu_;
         K22_(2,2) = lambda + 2*mu_;         

         F1_dif_ =  prod(K11_,dI_dx_) + prod(K12_,dI_dy_);
         F2_dif_ =  prod(K21_,dI_dx_) + prod(K22_,dI_dy_);
     }






     void flux_matrices_and_vectors(double p, double vx, double vy, const std::vector<int>& bd_nodes, int side_flag)
     {
         flux_matrices_and_vectors(p,vx,vy);

         const double vx_x(dI_dx_[1]), vx_y(dI_dy_[1]);
         const double vy_x(dI_dx_[2]), vy_y(dI_dy_[2]);

         const double lambda = -2.0*mu_/3.0;
         double tau11 = lambda*(vx_x + vy_y) + 2.0*mu_*vx_x;
         double tau22 = lambda*(vx_x + vy_y) + 2.0*mu_*vy_y;
         double tau12 = mu_*(vx_y + vy_x); 

         if(ic_bc_.neumann_tau11(bd_nodes, side_flag).first)  tau11 = ic_bc_.neumann_tau11(bd_nodes, side_flag).second;
         if(ic_bc_.neumann_tau22(bd_nodes, side_flag).first)  tau22 = ic_bc_.neumann_tau22(bd_nodes, side_flag).second;
         if(ic_bc_.neumann_tau12(bd_nodes, side_flag).first)  tau12 = ic_bc_.neumann_tau12(bd_nodes, side_flag).second;
         
         F1_dif_[1] = tau11;
         F1_dif_[2] = tau12;

         F2_dif_[1] = tau12;
         F2_dif_[2] = tau22;

         if(ic_bc_.neumann_tau11(bd_nodes, side_flag).first)
         {
           K11_(1, 1) = 0.0;
           K12_(1, 2) = 0.0;
         }

         if(ic_bc_.neumann_tau22(bd_nodes, side_flag).first)
         {
           K21_(2, 1) = 0.0;
           K22_(2, 2) = 0.0;
         }

         if(ic_bc_.neumann_tau12(bd_nodes, side_flag).first)
         {
           K11_(2, 2) = 0.0;
           K12_(2, 1) = 0.0;
           K21_(1, 2) = 0.0;
           K22_(1, 1) = 0.0;
         }
     }









     void tau_matrix(double vx,  double vy)
     {
         tau_.clear();

         const double v_nrm = sqrt(vx*vx + vy*vy);
         if(v_nrm < tol_) return;

         vec v(2,0.0);
         v[0] = vx;    v[1] = vy;  

         mat del_v(2,2,0.0);
         for(int i=0; i<2; i++)
         {
             del_v(i,0) = dI_dx_[1+i];       del_v(i,1) = dI_dy_[1+i];     
         }
         auto alpha = quad_ptr_->alpha(v, del_v, dummy_);
         auto h = quad_ptr_->compute_h(alpha, dummy_);

         if(setup_.tau_diag_comp_2001())
         {
             double use = h/(2.0*v_nrm);

             tau_(0, 0) = h*v_nrm/2.0;
             tau_(1, 1) = std::min(use/rho_, h*h/(mu_*12));
             tau_(2, 2) = tau_(1, 1);

             const auto G = quad_ptr_->G();
             const auto tr_G = quad_ptr_->trace_G();
             tau_(0, 0) = 1.0/( 1.0/tau_(0,0) + rho_*tau_(1,1)*tr_G ); 
         }

         else if(setup_.tau_diag_incomp_2007())
         {
          const auto G = quad_ptr_->G();
          double use(0.0);
          for(int i=0; i<2; i++)
           for(int j=0; j<2; j++)
            use += G(i,j)*G(i,j);
           
          double tau_m = boost::numeric::ublas::inner_prod(v, prod(G, v)) + 9.0*use*mu_*mu_/(rho_*rho_);
          tau_m = 1.0/sqrt(tau_m); 

          const auto g = quad_ptr_->g();
          const double tau_c = 1.0/(rho_*tau_m*boost::numeric::ublas::inner_prod(g,g));
          
          tau_(0, 0) = tau_c;
          tau_(1, 1) = tau_m/rho_;
          tau_(2, 2) = tau_m/rho_;
         }
     }





 private:

     int nb_el_nodes_;
     int nb_dof_fluid_;

     ic_bc_type& ic_bc_;
     fluid_properties& props_;
     read_setup& setup_;

     double JxW_; 
     vec JNxW_;

     vec I_orig_fluid_;
     vec I_;
     vec dI_dx_;
     vec dI_dy_;

     vec I_orig_T_;
     double I_T_;

     double rho_;
     double mu_;
     double beta_;
     double alpha_;

     vec F1_adv_;
     vec F2_adv_;
     vec F1_dif_;
     vec F2_dif_;
     vec S_;

     mat A1_,A2_;
     mat K11_,K12_,K21_,K22_;

     mat tau_;

     vec gravity_;

     vec  r_;
     mat  m_;

     double tol_;
     std::shared_ptr<quad> quad_ptr_ = nullptr;
     point<2> dummy_;


  };



}/* namespace GALES */
#endif
