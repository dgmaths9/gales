#ifndef ASSEMBLY_HPP_
#define ASSEMBLY_HPP_


#include "../../fem/fem.hpp"



namespace GALES {





  template<typename integral_type, typename dofs_ic_bc_type, int dim>
  class fluid_assembly
  {
      using model_type = model<dim>;
      using vec = boost::numeric::ublas::vector<double>;
      using mat = boost::numeric::ublas::matrix<double>;

    public:
    

     // This is for Euler-backward method
    double execute(integral_type& integral, dofs_ic_bc_type& dofs_ic_bc, model_type& f_model, model_type& T_model, linear_system& lp)
    {
      double start(MPI_Wtime());      
      
      std::vector<vec> dofs_fluid(f_model.state().num_slot());
      vec dofs_T(T_model.state().num_slot());

      lp.clear();
      for(const auto& el: f_model.mesh().elements())
      {
	f_model.extract_element_dofs(el->lid(), dofs_fluid);
	T_model.extract_element_dofs(el->lid(), dofs_T);

        const int nb_dofs = dofs_fluid[0].size();
        vec r(nb_dofs, 0.0);
        mat m(nb_dofs, nb_dofs, 0.0);

	integral.execute(*el, dofs_fluid, dofs_T, m, r);
	lp.assemble(el->dofs_gids(), el->dofs_constraints(dofs_ic_bc), m, r);
      }
      lp.assembled();       
      return MPI_Wtime()-start;                  
    }
      
  };









  template<typename integral_type, typename dofs_ic_bc_type, int dim>
  class T_assembly
  {
      using model_type = model<dim>;
      using vec = boost::numeric::ublas::vector<double>;
      using mat = boost::numeric::ublas::matrix<double>;
    
    public:
    

     // This is for Euler-backward method
    double execute(integral_type& integral, dofs_ic_bc_type& dofs_ic_bc, model_type& T_model, model_type& f_model, linear_system& lp)
    {
      double start(MPI_Wtime());      
      
      std::vector<vec> dofs_T(T_model.state().num_slot());
      vec dofs_fluid;

      lp.clear();
      for(const auto& el: T_model.mesh().elements())
      {
	T_model.extract_element_dofs(el->lid(), dofs_T);
	f_model.extract_element_dofs(el->lid(), dofs_fluid);

        const int nb_dofs = dofs_T[0].size();
        vec r(nb_dofs, 0.0);
        mat m(nb_dofs, nb_dofs, 0.0);

	integral.execute(*el,  dofs_T, dofs_fluid, m, r);
	lp.assemble(el->dofs_gids(), el->dofs_constraints(dofs_ic_bc), m, r);
      }
      lp.assembled();       
      return MPI_Wtime()-start;                  
    }


  };





}//namespace GALES
#endif
