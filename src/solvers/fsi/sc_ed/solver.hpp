#include <mpi.h>
#include <iostream>


#include "../../../fem/fem.hpp"
#include "../../esmm/esmm.hpp"
#include "../../fluid_sc/fluid.hpp"
#include "../../solid_ed/solid.hpp"




using namespace GALES;






template<int dim>
void solver(read_setup& setup)
{
  #include "../../fluid_sc/constructors_mm.hpp"
  #include "../../solid_ed/constructors.hpp"



  //-------------fsi-------------------------------------------------
  s_uv<dim> s_uv;
  f_tr<dim> f_tr;
  fsi_residual_check fsi_res_check;







  time::get().t(0.0);
  time::get().delta_t(setup.delta_t());

  double restart_or_t_zero = MPI_Wtime();  
  if(!setup.restart())
  { 
    //--------- setting up dofs for time = 0 ---------------- 
    fluid_io.write("fluid_dofs/", *fluid_dof_state);
    if(setup.Gen_alpha()) fluid_io.write("fluid_dot_dofs/", *fluid_dot_dof_state);         
    solid_io.write("solid/u/", *solid_u_dof_state);
    solid_io.write("solid/v/", *solid_v_dof_state);
    solid_io.write("solid/a/", *solid_a_dof_state);
    elastostatic_io.write("elastostatic_dofs/", *elastostatic_dof_state);
    mesh_motion.write_mesh(parallel_mesh_writer, fluid_mesh);    
    print_only_pid<0>(std::cerr)<<"Dofs writing for time 0 took: "<<std::setprecision(setup.precision()) << MPI_Wtime()-restart_or_t_zero<<" s\n\n";       
  }
  else
  {
     //--------------read restart-----------------------------
    time::get().t(setup.restart_time());
    fluid_io.read("fluid_dofs/", *fluid_dof_state);
    if(setup.Gen_alpha()) fluid_io.read("fluid_dot_dofs/", *fluid_dot_dof_state);    
    solid_io.read("solid/u/", *solid_u_dof_state);
    solid_io.read("solid/v/", *solid_v_dof_state);
    solid_io.read("solid/a/", *solid_a_dof_state);
    elastostatic_io.read("elastostatic_dofs/", *elastostatic_dof_state);
    print_only_pid<0>(std::cerr)<<"Dofs reading at restart time " << time::get().t() << " took: "<<std::setprecision(setup.precision()) << MPI_Wtime()-restart_or_t_zero<<" s\n\n";       
  }
  time::get().tick();





  // ---------------------TIME LOOP---------------------------
  double time_loop_start = MPI_Wtime();
  double non_linear_res_fluid(0.0), non_linear_res_solid(0.0);
  int tot_time_iterations(0);

  for(; time::get().t()<=setup.end_time(); time::get().tick())
  {

    double t_iteration(MPI_Wtime());

    print_only_pid<0>(std::cerr)<<"simulation time: "<<std::setprecision(setup.precision()) << time::get().t()<<" s\n";





    //----------------predictors--------------------------------------------------------------------------------
    if(setup.Gen_alpha())  fluid_updater.predictor(*fluid_dof_state, *fluid_dot_dof_state);
    else fluid_updater.predictor(*fluid_dof_state);

    solid_updater.predictor(*solid_u_dof_state, *solid_v_dof_state, *solid_a_dof_state);
    elastostatic_updater.predictor(*elastostatic_dof_state);
    
    mesh_motion.set_p_mesh(fluid_mesh);
    mesh_motion.set_p_mesh(elastostatic_mesh);
    //----------------end of predictors--------------------------------------------------------------------------------
    
    
    


    for(int it_count=0; it_count < setup.n_max_it(); it_count++)   //outer iterations loop
    {



      //------------------------ SOLUTION For solid ------------------------------------------------------

      // ------------assigning fluid traction to solid at FSI---------
      double fsi_fluid_tr_start = MPI_Wtime();
      std::map<int, std::vector<double> > f_nd_tr;       
      f_tr.tr(fluid_model, fluid_props, f_nd_tr);
      solid_ic_bc.set_fluid_tr(f_nd_tr);	 
      double fsi_fluid_tr = MPI_Wtime()-fsi_fluid_tr_start;
      //-------------------------------------------------------------

      solid_dofs_ic_bc.dirichlet_bc();

      for(int s_it_count=0; s_it_count < setup.n_max_it(); s_it_count++)   //iterations loop
      {        
	double fill_time = solid_assembly.execute(solid_integral, solid_dofs_ic_bc, solid_u_model, solid_v_model, solid_a_model, solid_lp);   

        
        bool residual_converged = solid_res_check.execute(*solid_lp.rhs(), s_it_count, non_linear_res_solid);      
        if(residual_converged)
        {
          print_solver_info("SOLID  ", s_it_count, fill_time, non_linear_res_solid);        
          break;
        } 	      
        double solver_time = solid_ls_solver.execute(solid_lp);



     	solid_updater.corrector(*solid_u_dof_state, *solid_v_dof_state, *solid_a_dof_state, solid_io.state_vec(*solid_ls_solver.solution()));
     	
        print_solver_info("SOLID  ", s_it_count, fill_time, non_linear_res_solid, solver_time, solid_ls_solver);        

        // -----------------------end of solid solution---------------------------------------------------------
      }






      //------------------------ SOLUTION For elastostatic -----------------------------------------------


      // ------------assigning solid u to elastostatic at FSI-------
      double fsi_solid_u_start = MPI_Wtime();
      std::map<int, std::vector<double> > s_nd_u;       
      s_uv.s_u(solid_u_model, s_nd_u);
      elastostatic_ic_bc.set_solid_u(s_nd_u);
      double fsi_solid_u = MPI_Wtime()-fsi_solid_u_start;
     //------------------------------------------------------------


      double fill_time = elastostatic_assembly.execute(elastostatic_integral, elastostatic_dofs_ic_bc, elastostatic_model, elastostatic_lp);
      double solver_time = elastostatic_ls_solver.execute(elastostatic_lp);


      elastostatic_updater.solution(*elastostatic_dof_state, elastostatic_io.state_vec(*elastostatic_ls_solver.solution()));
      
      print_solver_info(fill_time, solver_time, elastostatic_ls_solver); 
      // -----------------------end of elastostatic solution---------------------------------------------------------




      //----------------mesh update--------------------------------------------------------------------------------
      mesh_motion.update_mesh(elastostatic_model,fluid_mesh);
      mesh_motion.update_mesh(elastostatic_model,elastostatic_mesh);
      //-----------------------------------------------------------------------------------------------------------









      //----------------------------------- SOLUTION For fluid -------------------------------------------

      // ------------assigning solid v to fluid at FSI--------
      double fsi_solid_v_start = MPI_Wtime();
      std::map<int, std::vector<double> > s_nd_v;       
      s_uv.s_v(solid_v_model, s_nd_v);
      fluid_ic_bc.set_solid_v(s_nd_v);
      double fsi_solid_v = MPI_Wtime()-fsi_solid_v_start;
      //------------------------------------------------------

      fluid_dofs_ic_bc.dirichlet_bc();

      for(int f_it_count=0; f_it_count < setup.n_max_it(); f_it_count++)   //fluid iterations loop
      {
        double fill_time(0.0); 
        if(setup.Gen_alpha())  fill_time = fluid_assembly.execute(fluid_integral, fluid_dofs_ic_bc, fluid_model, elastostatic_model, fluid_lp, fluid_dot_model);   
        else fill_time = fluid_assembly.execute(fluid_integral, fluid_dofs_ic_bc, fluid_model, elastostatic_model, fluid_lp);


        bool residual_converged = fluid_res_check.execute(*fluid_lp.rhs(), f_it_count, non_linear_res_fluid);      
        if(residual_converged)
        {
          print_solver_info("FLUID  ", f_it_count, fill_time, non_linear_res_fluid);        
          break;
        } 	      
        double solver_time = fluid_ls_solver.execute(fluid_lp);


        if(setup.Gen_alpha())  fluid_updater.corrector(*fluid_dof_state, *fluid_dot_dof_state, fluid_io.state_vec(*fluid_ls_solver.solution()));
        else fluid_updater.corrector(*fluid_dof_state, fluid_io.state_vec(*fluid_ls_solver.solution()));


        print_solver_info("FLUID  ", f_it_count, fill_time, non_linear_res_fluid, solver_time, fluid_ls_solver);        
      }
      // -----------------------end of fluid solution---------------------------------------------------------



      
      //check fsi residual
      bool fsi_residual_converged = fsi_res_check.execute(solid_u_model);      
      if(fsi_residual_converged)
      {
          break;
      } 	      
      
    }



    if((tot_time_iterations+1)% (setup.print_freq()) == 0)
    {
      fluid_io.write("fluid_dofs/", *fluid_dof_state);
      if(setup.Gen_alpha())  fluid_io.write("fluid_dot_dofs/", *fluid_dot_dof_state);
      solid_io.write("solid/u/", *solid_u_dof_state);
      solid_io.write("solid/v/", *solid_v_dof_state);
      solid_io.write("solid/a/", *solid_a_dof_state);
      elastostatic_io.write("elastostatic_dofs/", *elastostatic_dof_state);
      mesh_motion.write_mesh(parallel_mesh_writer, fluid_mesh);    
    }




    print_only_pid<0>(std::cerr)<<"Time step took: "<<MPI_Wtime()-t_iteration<<" s\n\n";
    tot_time_iterations++;

  }
  

  print_only_pid<0>(std::cerr)<<"End time reached!!!! "<<"\n\n";
  print_only_pid<0>(std::cerr)<<"Total run time: "<<MPI_Wtime()-time_loop_start<<" s\n\n\n";

}









int main(int argc, char* argv[])
{

  MPI_Init(&argc, &argv);


  //---------------- read setup file -------------------------------
  read_setup setup;


  //---------- creating directories -----------------------------
  make_dirs("results/fluid_dofs results/fluid_mesh results/elastostatic_dofs results/solid/u results/solid/v results/solid/a");
  if(setup.Gen_alpha()) make_dirs("results/fluid_dot_dofs");  

   
  if(setup.dim()==2) solver<2>(setup);
  else if(setup.dim()==3) solver<3>(setup);


  MPI_Barrier(MPI_COMM_WORLD);
  MPI_Finalize();
  return 0;
}

