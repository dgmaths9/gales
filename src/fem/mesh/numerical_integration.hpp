#ifndef _GALES_NUMERICAL_INTEGRATION_HPP_
#define _GALES_NUMERICAL_INTEGRATION_HPP_

#include <vector>



namespace GALES {




/**
  This class implements info about numerical integration points on master mesh elements.
  Rule of thumb: for p th order finite element  use p+1 quadrature points in that direction 
*/





  template<int dim>
  class base_numerical_integration
  {
    public:

    //-------------------------------------------------------------------------------------------------------------------------------------        
    /// Deleting the copy and move constructors - no duplication/transfer in anyway
    base_numerical_integration() = default;               //copy constructor
    base_numerical_integration(const base_numerical_integration&) = delete;               //copy constructor
    base_numerical_integration& operator=(const base_numerical_integration&) = delete;    //copy assignment operator
    base_numerical_integration(base_numerical_integration&&) = delete;                    //move constructor  
    base_numerical_integration& operator=(base_numerical_integration&&) = delete;         //move assignment operator 
    //-------------------------------------------------------------------------------------------------------------------------------------


    int nb_gp()const{return gp_.size();}
    auto gp()const {return gp_;}
    auto gp(int i)const {return gp_[i];}    

    auto W_in()const {return W_in_;}
    auto W_in(int i)const {return W_in_[i];}

    
    auto side()const {return side_;}
    auto side(int sd_idx)const {return side_[sd_idx];}
    auto side(int sd_idx, int nd_idx)const {return side_[sd_idx][nd_idx];}    

    int nb_sides()const {return nb_sides_;}
    auto nb_side_nds()const {return nb_side_nds_;}
    int nb_side_nds(int sd_idx)const {return nb_side_nds_[sd_idx];}
    
    int nb_side_gp(int sd_idx) const {return side_gp_[sd_idx].size();}
    auto side_gp()const {return side_gp_;}
    auto side_gp(int sd_idx)const {return side_gp_[sd_idx];}
    auto side_gp(int sd_idx, int gp_idx)const {return side_gp_[sd_idx][gp_idx];}    

    auto W_bd()const {return W_bd_;}
    auto W_bd(int sd_idx)const {return W_bd_[sd_idx];}
    auto W_bd(int sd_idx, int gp_idx)const {return W_bd_[sd_idx][gp_idx];}    
      
    
  
    protected:

    int nb_nodes_;                                         /// number of nodes with which el is composed 
    int nb_gp_;                                            /// number of gauss points for numerical integration 

    std::vector<point<dim>> gp_;                            /// Vector of gauss points for integration of element interior
    std::vector<double> W_in_;                            /// Weights of gauss points for integration of element interior 

    std::vector<std::vector<int>> side_;                  /// Vector of sides of an element
    std::vector<std::vector<point<dim>>> side_gp_;          /// Vector of gauss points for integration of each side
    std::vector<std::vector<double>> W_bd_;               /// Weight of gauss points for integration of element face 

    int nb_sides_;                                        /// Number of sides of an element
    std::vector<int> nb_side_nds_;                        /// Number of nodes of sides of an element
  };







  //-----------------------------------------------------------------------------------------------------------------------   
  // This is generic template for node<dim>
  template<int dim>
  class numerical_integration: public base_numerical_integration<dim>{};
  //-----------------------------------------------------------------------------------------------------------------------





  //-----------------------------------------------------------------------------------------------------------------------
  // This class is tempate specialized with dim = 2 
  template<>
  class numerical_integration<2>: public base_numerical_integration<2>
  {
    public:

    numerical_integration(int nb_nodes, int nb_gp)
    {
       nb_nodes_ = nb_nodes;
       nb_gp_ = nb_gp;
       gp_.resize(nb_gp);
       W_in_.resize(nb_gp);
       gauss_data();
    }
    
    
    //-------------------------------------------------------------------------------------------------------------------------------------        
    /// Deleting the copy and move constructors - no duplication/transfer in anyway
    numerical_integration(const numerical_integration&) = delete;               //copy constructor
    numerical_integration& operator=(const numerical_integration&) = delete;    //copy assignment operator
    numerical_integration(numerical_integration&&) = delete;                    //move constructor  
    numerical_integration& operator=(numerical_integration&&) = delete;         //move assignment operator 
    //-------------------------------------------------------------------------------------------------------------------------------------






  //-----------------------------------------------------------------------------------------------------------------------   
    void gauss_data()
    {

      /**
                          |eta
                          |
                          | 
                          |               
        	    3 ___2|____2
        	     |    |    |
        	     |    |____|____________xi
        	    3|         |1
        	     |_________|
                     0    0    1
      */

      if(nb_nodes_ == 4)
      {
        /// The order of nodes is anticlockwise looking from outside of the cell.
        nb_sides_ = 4;
        
        side_.resize(nb_sides_);
        side_[0] = {0,1};  ///bottom --->  eta = -1
        side_[1] = {1,2};  ///right  --->  xi = 1
        side_[2] = {2,3};  ///top    --->  eta = 1
        side_[3] = {3,0};  ///left   --->  xi = -1

        nb_side_nds_.resize(nb_sides_);
        for(int i=0; i<nb_sides_; i++)
           nb_side_nds_[i] = side_[i].size();


        side_gp_.resize(nb_sides_);
        W_bd_.resize(nb_sides_);
        
        if(nb_gp_ == 4)      // good upto 3rd degree polynomial in \xi and 3rd degree polynomial in \eta
        { 
           const double g_minus = -1.0/sqrt(3.0);      // -0.57735026918962573105886804115
           const double g_plus = 1.0/sqrt(3.0);       //  0.57735026918962573105886804115;
   
           ///------------------------------interior-------------------------------------------
           /// gauss points for element interior         
           gp_[0] = point<2>(g_minus,g_minus);
           gp_[1] = point<2>(g_plus,g_minus);
           gp_[2] = point<2>(g_plus,g_plus);
           gp_[3] = point<2>(g_minus,g_plus);

           W_in_[0] = 1.0;                               
           W_in_[1] = 1.0;                               
           W_in_[2] = 1.0;                               
           W_in_[3] = 1.0;                               
           ///---------------------------------------------------------------------------------
           
   
           ///------------------------------boundary-------------------------------------------        
           /// To facilitate fsi fluid traction computation, gauss points are ordered in accordance to side nodes. 
           side_gp_[0] = {point<2>(g_minus,-1.0), point<2>(g_plus,-1.0)};  ///bottom
           side_gp_[1] = {point<2>(1.0,g_minus),  point<2>(1.0,g_plus)};   ///right
           side_gp_[2] = {point<2>(g_plus,1.0),  point<2>(g_minus,1.0)};   ///top
           side_gp_[3] = {point<2>(-1.0,g_plus), point<2>(-1.0,g_minus)};  ///left

           W_bd_[0] = {1.0, 1.0};                               
           W_bd_[1] = {1.0, 1.0};                               
           W_bd_[2] = {1.0, 1.0};                               
           W_bd_[3] = {1.0, 1.0};                                
           ///---------------------------------------------------------------------------------
        }
        else if(nb_gp_ == 1)  // good upto 1st degree polynomial in \xi and 1st degree polynomial in \eta
        {
           gp_[0] = point<2>(0.0, 0.0);
           W_in_[0] = 4.0;                               
        
           side_gp_[0] = {point<2>(0.0,-1.0)};  ///bottom
           side_gp_[1] = {point<2>(1.0,0.0)};   ///right
           side_gp_[2] = {point<2>(0.0,1.0)};   ///top
           side_gp_[3] = {point<2>(-1.0,0.0)};  ///left

           W_bd_[0] = {2.0};                              
           W_bd_[1] = {2.0};                              
           W_bd_[2] = {2.0};                               
           W_bd_[3] = {2.0};                                
        }          
      }    
                          
      /*
                     |eta
                     | 
                     |
                     |
                    2|
                     |\
                     |  \ 0
                    1|    \
                     |______\_________________
                     0   2    1               xi
      */  



      else if(nb_nodes_ == 3)
      {
        /// the order of nodes is anticlockwise looking from outside of the cell
        nb_sides_ = 3;
        
        side_.resize(nb_sides_);
        side_[0] = {1,2};  /// xi + eta = 1
        side_[1] = {2,0};  /// xi = 0
        side_[2] = {0,1};  /// eta = 0

        nb_side_nds_.resize(nb_sides_);
        for(int i=0; i<nb_sides_; i++)
           nb_side_nds_[i] = side_[i].size();


        side_gp_.resize(nb_sides_);
        W_bd_.resize(nb_sides_);

        if(nb_gp_ == 3)  // good upto 2nd degree polynomial in \xi and 2nd degree polynomial in \eta
        { 
           ///------------------------------interior-------------------------------------------
           gp_[0] = point<2>(1.0/6.0, 1.0/6.0);
           gp_[1] = point<2>(4.0/6.0, 1.0/6.0);
           gp_[2] = point<2>(1.0/6.0, 4.0/6.0);

           W_in_[0] = 1.0/6.0;
           W_in_[1] = 1.0/6.0;
           W_in_[2] = 1.0/6.0;
           ///---------------------------------------------------------------------------------
   
   
   
           ///------------------------------boundary-------------------------------------------      
           /// xi, eta for master triangle edges vary in range [0-1];
           /// To compute gauss quadrature rule on line integral from [-1,1], we define a mapping 
           /// f(t) = (1+t)/2  which maps -1--->0 and 1---->1:   [-1,1]---->[0,1] with determinant of transformation is 1/2
           /// therefore the gauss points are 0.5(1-1/sqrt(3)) and 0.5*(1+1/sqrt(3)) instead of 1/sqrt(3) and -1/sqrt(3)
                   
           const double g_minus = 0.5*(1.0 - 1.0/sqrt(3.0));
           const double g_plus = 0.5*(1.0 + 1.0/sqrt(3.0));         
                  
           /// to facilitate fsi fluid traction computation, gauss points are ordered in accordance to side nodes 
           side_gp_[0] = { point<2>(g_plus,1.0-g_plus), point<2>(g_minus,1.0-g_minus) };  /// x+y=1
           side_gp_[1] = { point<2>(0.0,g_plus), point<2>(0.0,g_minus) };  /// x=0;  
           side_gp_[2] = { point<2>(g_minus,0.0), point<2>(g_plus,0.0) };  /// y=0;

           W_bd_[0] = {0.5, 0.5};    // this accounts w = 1.0 and determinant of transformation = 1/2
           W_bd_[1] = {0.5, 0.5};    // this accounts w = 1.0 and determinant of transformation = 1/2
           W_bd_[2] = {0.5, 0.5};    // this accounts w = 1.0 and determinant of transformation = 1/2
           ///---------------------------------------------------------------------------------
        }      
        else if(nb_gp_ == 1)  // good upto 1st degree polynomial in \xi and 1st degree polynomial in \eta
        {
           gp_[0] = point<2>(1.0/3.0, 1.0/3.0);
           W_in_[0] = 0.5;                               
        
           ///------------------------------boundary-------------------------------------------      
           side_gp_[0] = { point<2>(0.5,0.5)};  /// x+y=1
           side_gp_[1] = { point<2>(0.0,0.5)};  /// x=0;  
           side_gp_[2] = { point<2>(0.5,0.0)};  /// y=0;

           W_bd_[0] = {0.5};  // this accounts w = 1.0 and determinant of transformation = 1/2
           W_bd_[1] = {0.5};  // this accounts w = 1.0 and determinant of transformation = 1/2
           W_bd_[2] = {0.5};  // this accounts w = 1.0 and determinant of transformation = 1/2
        }      
      }
    }    
    //-----------------------------------------------------------------------------------------------------------------------   
    
  };
  //-----------------------------------------------------------------------------------------------------------------------



   












  //-----------------------------------------------------------------------------------------------------------------------
  // This class is tempate specialized with dim = 3 
  template<>
  class numerical_integration<3>: public base_numerical_integration<3>
  {
    public:
                
    numerical_integration(int nb_nodes, int nb_gp)
    {
       nb_nodes_ = nb_nodes;
       nb_gp_ = nb_gp;
       gp_.resize(nb_gp_);
       W_in_.resize(nb_gp_);
       gauss_data();
    }
    
    
    //-------------------------------------------------------------------------------------------------------------------------------------        
    /// Deleting the copy and move constructors - no duplication/transfer in anyway
    numerical_integration(const numerical_integration&) = delete;               //copy constructor
    numerical_integration& operator=(const numerical_integration&) = delete;    //copy assignment operator
    numerical_integration(numerical_integration&&) = delete;                    //move constructor  
    numerical_integration& operator=(numerical_integration&&) = delete;         //move assignment operator 
    //-------------------------------------------------------------------------------------------------------------------------------------







    //-------------------------------------------------------------------------------------------------------------------------------------        
    void gauss_data()
    {

      /*  
                           |eta
                           |     
                           |     
   \                3------|------2                       
    \               |\     |      |\           
     \              | \    |      | \          
      \             |  \   |      |  \         
       \            |   7=============6        
        \           |   || +------|--||--------> xi   
         \          0---||--\-----1  ||        
          \          \  ||   \     \ ||        
           \          \ ||    \     \||        
            \          \||     \     ||        
             \          4=============5        
              \                  \      
               \                  \ zeta 
                \                         
      */


      if(nb_nodes_ == 8)
      {
        nb_sides_ = 6;

        side_.resize(nb_sides_);
        /// the order of nodes is anticlockwise looking from outside of the cell
        side_[0] = {4,5,6,7};  ///front   --->   zeta = 1
        side_[1] = {0,3,2,1};  ///back    --->   zeta = -1
        side_[2] = {0,4,7,3};  ///left    --->   xi = -1
        side_[3] = {1,2,6,5};  ///right   --->   xi = 1
        side_[4] = {2,3,7,6};  ///top     --->   eta = 1 
        side_[5] = {1,5,4,0};  ///bottom  --->   eta = -1


        nb_side_nds_.resize(nb_sides_);
        for(int i=0; i<nb_sides_; i++)
           nb_side_nds_[i] = side_[i].size();

        
        /// to facilitate fsi fluid traction computation, gauss points are ordered in accordance to side nodes 
        side_gp_.resize(nb_sides_);
        W_bd_.resize(nb_sides_);


        if(nb_gp_ == 8)  // good upto 3rd degree polynomial in \xi, 3rd degree polynomial in \eta, 3rd degree polynomial in \zeta
        {         
           const double g_minus = -1.0/sqrt(3.0);
           const double g_plus = 1.0/sqrt(3.0);
   
           gp_[0] = point<3>(g_minus,g_minus,g_minus);        
           gp_[1] = point<3>(g_plus,g_minus,g_minus);
           gp_[2] = point<3>(g_plus,g_plus,g_minus);
           gp_[3] = point<3>(g_minus,g_plus,g_minus);
           gp_[4] = point<3>(g_minus,g_minus,g_plus);
           gp_[5] = point<3>(g_plus,g_minus,g_plus);
           gp_[6] = point<3>(g_plus,g_plus,g_plus);
           gp_[7] = point<3>(g_minus,g_plus,g_plus);

           W_in_[0] = 1.0;                               
           W_in_[1] = 1.0;                               
           W_in_[2] = 1.0;                               
           W_in_[3] = 1.0;                               
           W_in_[4] = 1.0;                               
           W_in_[5] = 1.0;                               
           W_in_[6] = 1.0;                               
           W_in_[7] = 1.0;                               
           
           side_gp_[0] = {point<3>(g_minus,g_minus,1.0),  point<3>(g_plus,g_minus,1.0),  point<3>(g_plus,g_plus,1.0),  point<3>(g_minus,g_plus,1.0)};   ///front                
           side_gp_[1] = {point<3>(g_minus,g_minus,-1.0), point<3>(g_minus,g_plus,-1.0), point<3>(g_plus,g_plus,-1.0), point<3>(g_plus,g_minus,-1.0)};  ///back                
           side_gp_[2] = {point<3>(-1.0,g_minus,g_minus), point<3>(-1.0,g_minus,g_plus), point<3>(-1.0,g_plus,g_plus), point<3>(-1.0,g_plus,g_minus)};  ///left                        
           side_gp_[3] = {point<3>(1.0,g_minus,g_minus),  point<3>(1.0,g_plus,g_minus),  point<3>(1.0,g_plus,g_plus),  point<3>(1.0,g_minus,g_plus)};   ///right                        
           side_gp_[4] = {point<3>(g_plus,1.0,g_minus),   point<3>(g_minus,1.0,g_minus), point<3>(g_minus,1.0,g_plus), point<3>(g_plus,1.0,g_plus)};    ///top                
           side_gp_[5] = {point<3>(g_plus,-1.0,g_minus),  point<3>(g_plus,-1.0,g_plus),  point<3>(g_minus,-1.0,g_plus), point<3>(g_minus,-1.0,g_minus)}; ///bottom

           W_bd_[0] = {1.0, 1.0, 1.0, 1.0};                               
           W_bd_[1] = {1.0, 1.0, 1.0, 1.0};                               
           W_bd_[2] = {1.0, 1.0, 1.0, 1.0};                               
           W_bd_[3] = {1.0, 1.0, 1.0, 1.0};                               
           W_bd_[4] = {1.0, 1.0, 1.0, 1.0};                               
           W_bd_[5] = {1.0, 1.0, 1.0, 1.0};                               
        }   
        else if(nb_gp_ == 1)  // good upto 1st degree polynomial in \xi, 1st degree polynomial in \eta, 1st degree polynomial in \zeta
        {            
           gp_[0] = point<3>(0.0,0.0,0.0);        
           W_in_[0] = 8.0;                               
           
           side_gp_[0] = {point<3>(0.0,0.0,1.0)};   ///front                
           side_gp_[1] = {point<3>(0.0,0.0,-1.0)};  ///back                
           side_gp_[2] = {point<3>(-1.0,0.0,0.0)};  ///left                        
           side_gp_[3] = {point<3>(1.0,0.0,0.0)};   ///right                        
           side_gp_[4] = {point<3>(0.0,1.0,0.0)};    ///top                
           side_gp_[5] = {point<3>(0.0,-1.0,0.0)}; ///bottom

           W_bd_[0] = {4.0};                               
           W_bd_[1] = {4.0};                               
           W_bd_[2] = {4.0};                               
           W_bd_[3] = {4.0};                               
           W_bd_[4] = {4.0};                               
           W_bd_[5] = {4.0};                               
        }           
      }                        


      /*
                           |eta
                           |
                           | 
                           |2
                          /|\,
                         / |  \,
                        /  |    \,
                       /   |      \,
                      /    |        \,
                     /     |__________\______________xi
                    /    ,/0          /1
                   /   ,/         / ^
                  /  ,/       / ^
                 / ,/    / ^
                /,/, / ^
              3/// ^ 
              /     
             /
            /zeta

      */ 

      else if(nb_nodes_ == 4 )
      {
        nb_sides_ = 4;
        
        side_.resize(nb_sides_);
        /// the order of nodes is anticlockwise looking from outside of the cell
        side_[0] = {1,2,3};  /// xi + eta + zeta = 1
        side_[1] = {0,3,2};  /// xi = 0
        side_[2] = {0,1,3};  /// eta = 0
        side_[3] = {0,2,1};  /// zeta = 0


        nb_side_nds_.resize(nb_sides_);
        for(int i=0; i<nb_sides_; i++)
           nb_side_nds_[i] = side_[i].size();


        side_gp_.resize(nb_sides_);
        W_bd_.resize(nb_sides_);


        if(nb_gp_ == 4)  // good upto 2nd degree polynomial in \xi, 2nd degree polynomial in \eta, 2nd degree polynomial in \zeta
        {         
           gp_[0] = point<3>(0.1381966011250105, 0.1381966011250105, 0.1381966011250105);
           gp_[1] = point<3>(0.5854101966249685, 0.1381966011250105, 0.1381966011250105);
           gp_[2] = point<3>(0.1381966011250105, 0.5854101966249685, 0.1381966011250105);
           gp_[3] = point<3>(0.1381966011250105, 0.1381966011250105, 0.5854101966249685);

           W_in_[0] = 1.0/24.0;
           W_in_[1] = 1.0/24.0;
           W_in_[2] = 1.0/24.0;
           W_in_[3] = 1.0/24.0;
   
           side_gp_[0] = {point<3>(4.0/6.0, 1.0/6.0, 1.0/6.0), point<3>(1.0/6.0, 4.0/6.0, 1.0/6.0), point<3>(1.0/6.0, 1.0/6.0, 4.0/6.0)};  /// xi + eta + zeta = 1
           side_gp_[1] = {point<3>(0.0, 1.0/6.0, 1.0/6.0), point<3>(0.0, 1.0/6.0, 4.0/6.0), point<3>(0.0, 4.0/6.0, 1.0/6.0)};  /// xi = 0
           side_gp_[2] = {point<3>(1.0/6.0, 0.0, 1.0/6.0), point<3>(4.0/6.0, 0.0, 1.0/6.0), point<3>(1.0/6.0, 0.0, 4.0/6.0)};  /// eta = 0
           side_gp_[3] = {point<3>(1.0/6.0, 1.0/6.0, 0.0), point<3>(1.0/6.0, 4.0/6.0, 0.0), point_3d(4.0/6.0, 1.0/6.0, 0.0)};  /// zeta = 0

           W_bd_[0] = {1.0/6.0, 1.0/6.0, 1.0/6.0};
           W_bd_[1] = {1.0/6.0, 1.0/6.0, 1.0/6.0};
           W_bd_[2] = {1.0/6.0, 1.0/6.0, 1.0/6.0};
           W_bd_[3] = {1.0/6.0, 1.0/6.0, 1.0/6.0};
        }
        
        else if(nb_gp_ == 1)  // good upto 1st degree polynomial in \xi, 1st degree polynomial in \eta, 1st degree polynomial in \zeta
        {            
           gp_[0] = point<3>(1.0/4.0, 1.0/4.0, 1.0/4.0);
           W_in_[0] = 1.0/6.0;
        
           side_gp_[0] = {point<3>(1.0/3.0, 1.0/3.0, 1.0/3.0)};   /// xi + eta + zeta = 1      
           side_gp_[0] = {point<3>(0.0, 1.0/3.0, 1.0/3.0)};   /// xi = 0      
           side_gp_[0] = {point<3>(1.0/3.0, 0.0, 1.0/3.0)};   /// eta = 0     
           side_gp_[0] = {point<3>(1.0/3.0, 1.0/3.0, 0.0)};   /// zeta = 0 
           
           W_bd_[0] = {1.0/2.0};                            
           W_bd_[1] = {1.0/2.0};                            
           W_bd_[2] = {1.0/2.0};                            
           W_bd_[3] = {1.0/2.0};                            
        }            
      }  
      
      
      /*
                           |eta
                           |
                           | 
                           |2
                          /|\,
                        /  |  \,
                      /    |    \,
                    /      |      \,
                  /        |        \,
                /          |__________\______________xi
              /           /0          /1
            5|\,        /           / 
             |  \,    /           / 
             |    \,/           / 
             |    / \,        / 
             |  /     \,    / 
             |/_________\/ 
            /3           4
          /
        /zeta
     */ 
      
      else if(nb_nodes_ == 6)
      {
        nb_sides_ = 5;
        
        side_.resize(nb_sides_);
        /// the order of nodes is anticlockwise looking from outside of the cell
        side_[0] = {1,3,2};    /// zeta = -1
        side_[1] = {4,5,6};    /// zeta = 1
        side_[2] = {2,3,6,5};  /// xi+eta = 1
        side_[3] = {1,2,5,4};  /// eta = 0
        side_[3] = {1,4,6,3};  /// xi = 0

        nb_side_nds_.resize(nb_sides_);
        for(int i=0; i<nb_sides_; i++)
           nb_side_nds_[i] = side_[i].size();


        side_gp_.resize(nb_sides_);
        W_bd_.resize(nb_sides_);


        if(nb_gp_ == 6)  // good upto 2nd degree polynomial in \xi, 2nd degree polynomial in \eta, 2nd degree polynomial in \zeta
        {         
           gp_[0] = point<3>(1.0/6.0, 1.0/6.0, -1.0/sqrt(3.0));
           gp_[1] = point<3>(4.0/6.0, 1.0/6.0, -1.0/sqrt(3.0));
           gp_[2] = point<3>(1.0/6.0, 4.0/6.0, -1.0/sqrt(3.0));
           gp_[3] = point<3>(1.0/6.0, 1.0/6.0,  1.0/sqrt(3.0));
           gp_[4] = point<3>(4.0/6.0, 1.0/6.0,  1.0/sqrt(3.0));
           gp_[5] = point<3>(1.0/6.0, 4.0/6.0,  1.0/sqrt(3.0));

           W_in_[0] = 1.0/6.0;
           W_in_[1] = 1.0/6.0;
           W_in_[2] = 1.0/6.0;
           W_in_[3] = 1.0/6.0;
           W_in_[4] = 1.0/6.0;
           W_in_[5] = 1.0/6.0;
   
           side_gp_[0] = {point<3>(1.0/6.0, 1.0/6.0, -1.0), point<3>(1.0/6.0, 4.0/6.0, -1.0), point<3>(4.0/6.0, 1.0/6.0, -1.0)};  /// zeta = -1

           side_gp_[1] = {point<3>(1.0/6.0, 1.0/6.0, 1.0), point<3>(4.0/6.0, 1.0/6.0, 1.0), point<3>(1.0/6.0, 4.0/6.0, 1.0)};  /// zeta = 1

           const double g_minus = 0.5*(1.0 - 1.0/sqrt(3.0));
           const double g_plus = 0.5*(1.0 + 1.0/sqrt(3.0));         
                  
           /// to facilitate fsi fluid traction computation, gauss points are ordered in accordance to side nodes 
           side_gp_[2] = {point<3>(g_plus, 1.0-g_plus, -1.0/sqrt(3.0)), point<3>(g_minus,1.0-g_minus,-1.0/sqrt(3.0)),
                          point<3>(g_minus, 1.0-g_minus, 1.0/sqrt(3.0)), point<3>(g_plus, 1.0-g_plus, 1.0/sqrt(3.0))};  /// xi + eta=1

           side_gp_[3] = {point<3>(-1.0/sqrt(3.0), 0.0, -1.0/sqrt(3.0)), point<3>(1.0/sqrt(3.0), 0.0, -1.0/sqrt(3.0)), 
                          point<3>(1.0/sqrt(3.0), 0.0, 1.0/sqrt(3.0)), point<3>(-1.0/sqrt(3.0), 0.0, 1.0/sqrt(3.0))};  /// eta = 0

           side_gp_[4] = {point<3>(0.0, -1.0/sqrt(3.0), -1.0/sqrt(3.0)), point<3>(0.0, -1.0/sqrt(3.0), 1.0/sqrt(3.0)), 
                          point<3>(0.0, 1.0/sqrt(3.0), 1.0/sqrt(3.0)), point<3>(0.0, 1.0/sqrt(3.0), -1.0/sqrt(3.0))};  /// xi = 0                                                    

           W_bd_[0] = {1.0/6.0, 1.0/6.0, 1.0/6.0};
           W_bd_[1] = {1.0/6.0, 1.0/6.0, 1.0/6.0};
           
           W_bd_[2] = {1.0/2.0, 1.0/2.0, 1.0/2.0, 1.0/2.0};   // need to check.

           W_bd_[3] = {1.0/2.0, 1.0/2.0, 1.0/2.0, 1.0/2.0};
           W_bd_[4] = {1.0/2.0, 1.0/2.0, 1.0/2.0, 1.0/2.0};
        }
      }  
        
    }    
    //-------------------------------------------------------------------------------------------------------------------------------------        


  };
  //-----------------------------------------------------------------------------------------------------------------------
  






}//namespace GALES
#endif
