#ifndef _GALES_MESH_HPP_
#define _GALES_MESH_HPP_


#include <vector>
#include "element.hpp"
#include "node.hpp"
#include "quad.hpp"
#include "custom_gales_mesh_reader.hpp"
//#include "gmsh_mesh.hpp"
//#include "gmsh_mesh_partitioned_reader.hpp"



namespace GALES {


  /**
      This class defines the mesh.
  */



  template<int dim>
  class Mesh 
  {
        
    public:
                
        using point_t = point<dim>;
        using node_t = node<dim>;
        using element_t = element<dim>;


                
        Mesh(const std::string& mesh_name, int nb_nd_dofs, const read_setup& setup) 
        { 
           nb_nd_dofs_ = nb_nd_dofs;
           read_mesh(mesh_name, setup);      
           set_nb_nd_dofs();     
        }
                       
        
        
        Mesh(int nb_nd_dofs, const read_setup& setup, int num) 
        { 
           nb_nd_dofs_ = nb_nd_dofs;
//           gmsh_mesh<dim> obj(setup, *this, num);      

//           gmsh_mesh_partitioned_reader<dim> reader(setup, *this, num);
           set_nb_nd_dofs();     
        }
        
               
        
        
        //-------------------------------------------------------------------------------------------------------------------------------------        
        /// Deleting the copy and move constructors - no duplication/transfer in anyway
        Mesh() = default;               //copy constructor
        Mesh(const Mesh&) = delete;               //copy constructor
        Mesh& operator=(const Mesh&) = delete;    //copy assignment operator
        Mesh(Mesh&&) = delete;                    //move constructor  
        Mesh& operator=(Mesh&&) = delete;         //move assignment operator 
        //-------------------------------------------------------------------------------------------------------------------------------------
        
                
     
        
        void delete_mesh()
        {
           elements_.clear();
           bd_elements_.clear();                        

           tot_nodes_ = 0;
           nodes_.clear();           
           bd_nodes_.clear(); 
           host_nodes_.clear();                                                
     
           sides_.clear();
           side_flag_.clear();
        }





        void new_mesh(const std::string& mesh_name, int nd_nb_dofs, const read_setup& setup)
        {
           delete_mesh();
           nb_nd_dofs_ = nb_nd_dofs;
           read_mesh(mesh_name, setup);
           set_nb_nd_dofs();     
        }
                
        
        
        
        
        void read_mesh(const std::string& mesh_name, const read_setup& setup)
        {
           if(!does_file_exist(mesh_name))  Error("The file   '" + mesh_name + "'  to read does not exist!!!!!");
           auto file_extension = extractLastNChars(mesh_name, 3);        
           if(file_extension == "txt")  custom_gales_mesh_reader<dim> reader(mesh_name, setup, *this);
//           else if(file_extension == "msh") gmsh_mesh_partitioned_reader<dim> reader(mesh_name, setup, *this);
           else  Error("Gales can not read this file extension; We support only '.txt' and '.msh' !!!!!");                   
        }
        
        
        
       
       
        void set_nb_nd_dofs()
        {
           // set dofs of nodes for each node of the mesh
           for(auto& nd : nodes())
              nd->nb_dofs(nb_nd_dofs_);
              
           // set dofs of nodes for each node of an element
           for(auto& el : elements())
           {
             for(auto& nd : el->el_node_vec())
                nd->nb_dofs(nb_nd_dofs_);
                
             el->compute_dofs_gids();   
           }                           
        } 
       
       
                
                
                
        int dimension()const{ return dim; }                                 /// it returns the mesh dimension                

        auto& min_x() { return min_x_; }                                    /// mesh bound min_x
        auto& min_y() { return min_y_; }                                    /// mesh bound min_y
        auto& min_z() { return min_z_; }                                    /// mesh bound min_z

        auto& max_x() { return max_x_; }                                    /// mesh bound max_x
        auto& max_y() { return max_y_; }                                    /// mesh bound max_y
        auto& max_z() { return max_z_; }                                    /// mesh bound max_z
                
                              
                                
        auto& elements() { return elements_; }                              /// it returns the vector of shared_ptr to elements on current pid
        const auto& elements()const { return elements_; }                   /// it returns the vector of shared_ptr to elements on current pid (const version)                
        
        auto& bd_elements() { return bd_elements_; }                        /// it returns the vector of shared_ptr to boundary elements on current pid
        const auto& bd_elements()const { return bd_elements_; }             /// it returns the vector of shared_ptr to boundary elements on current pid (const version)        





        void tot_nodes(int n){tot_nodes_=n;}                                /// set total number of mesh nodes
        int tot_nodes()const{return tot_nodes_;}                            /// get total number of mesh nodes

        auto& nodes() { return nodes_; }                                    /// it returns the vector of shared_ptr to nodes on current pid
        const auto& nodes()const { return nodes_; }                         /// it returns the vector of shared_ptr to nodes on current pid (const version) 

        auto& bd_nodes() { return bd_nodes_; }                              /// it returns the vector of shared_ptr to boundary nodes on current pid
        const auto& bd_nodes()const { return bd_nodes_; }                   /// it returns the vector of shared_ptr to boundary nodes on current pid (const version)

        auto& host_nodes() { return host_nodes_; }                          /// it returns the vector of shared_ptr to host nodes on current pid
        const auto& host_nodes()const { return host_nodes_; }               /// it returns the vector of shared_ptr to host nodes on current pid (const version) 

        int nb_nd_dofs()const {return nb_nd_dofs_;}                         /// get number of node dofs (for a single node)





        
        auto& sides() { return sides_; }                                    /// it returns sides_ on current pid
        const auto& sides()const { return sides_; }                         /// it returns sides_ on current pid (const version)
        
        auto& side(int i) { return sides_[i]; }                             /// it returns i th side from sides_ vector
        const auto& side(int i)const { return sides_[i]; }                  /// it returns i th side from sides_ vector(const version)
        
        auto& side_flag() { return side_flag_; }                            /// it returns side_flag_ on current pid
        const auto& side_flag()const { return side_flag_; }                 /// it returns side_flag_ on current pid(const version)

        auto& side_flag(int i) { return side_flag_[i]; }                    /// it returns i th flag from side_flag_ vector
        const auto& side_flag(int i)const { return side_flag_[i]; }         /// it returns i th flag from side_flag_ vector(const version)


        
        
  private:
        
        double min_x_, max_x_, min_y_, max_y_, min_z_, max_z_;              /// mesh bounds        

        std::vector<std::shared_ptr<element_t>> elements_;                  /// vector of shared_ptr to elements on current pid                           
        std::vector<std::shared_ptr<element_t>> bd_elements_;               /// vector of shared_ptr to boundary elements on current pid

        int tot_nodes_;                                                     /// total number of mesh nodes
        std::vector<std::shared_ptr<node_t>> nodes_;                        /// vector of shared_ptr to nodes on current pid  
        std::vector<std::shared_ptr<node_t>> bd_nodes_;                     /// vector of shared_ptr to boundary nodes on current pid  
        std::vector<std::shared_ptr<node_t>> host_nodes_;                   /// vector of shared_ptr to host nodes on current pid  
        int nb_nd_dofs_;                                                    /// number of node dofs 
             
        std::vector<std::vector<int>> sides_;                               /// vector of sides where each side is vector of composing nodes gids
        std::vector<int> side_flag_;                                        /// vector of side flags         
        
  };
    
    



}
//namespace GALES
#endif


