#ifndef _GALES_GMSH_MESH_PARTITIONED_READER_HPP_
#define _GALES_GMSH_MESH_PARTITIONED_READER_HPP_





namespace GALES {


  /**
      This class defines the gmsh partitioned mesh reader.
      Gmsh generated mesh version should be 2.2 or 4.1
      Mesh partitioning is done throuh gmsh by running e.g.   gmsh mesh.geo -<2,3> -part <4,8,..>   
      it is advised to save the partitioned mesh in version 4.1
      All processes first read the whole mesh fle and then according to the partitioning of the mesh each process reads and stores only those elements which are owned by it. 
      We intentionally define all data members local in the constructor instead of making them members of the class.
      The reason behind this is to clean memory once many of them go out of the scope.
   
    el          0    1    2    3                   nd          0    1    2    3    4
    PID         0    0    1    1                   PID         0    0    1    1    1
             o----o---)o(---o----o                            o----o---)o(---o----o
    el GID      0    1    2    3                   nd GID      0    1    2    3    4
    el LID(0)   0    1                             nd LID(0)   0    1    2
    el LID(1)             0    1                   nd LID(1)             0    1    2   
    
    
    An element has a unique GID and a unique PID(disjoint partition). Element LID is defined as the unique local counter number on each process. 
    Therefore two different elements lying on two different processes can have the same LID.
    
    Similarly each process stores and reads the nodes owned by it. 
    A node has a unique GID. some nodes lie on inter process boundaries (e.g. see node 2 above).
    In this case, we define the owner PID of the node based on the largest integer number of the shared PIDs.
    Node LID is defined as the unique local counter number on each process.  Two different nodes lying on two different processes can have the same LID.
    
    Since we do most of the operations through LIDs, each process is responsible for executing the operation on its nodes and elements. 
    This applies even on the shared nodes. For example to do mesh update nodes with GIDs 0,1,2 will be updated by PID 0 and nodes with GIDs 2,3,4 will be
    updated by PID 1. Note that Node 2 is stored as two different objects on respective PIDs:  LID 2 on PID 0 and LID 0 on PID 1.  
    So, it will not be updated twice.
  */



  template<int dim>
  class gmsh_mesh_partitioned_reader 
  {
            
    public:                                
                

        //-------------------------------------------------------------------------------------------------------------------------------------        
        /// Deleting the copy and move constructors - no duplication/transfer in anyway
        gmsh_mesh_partitioned_reader(const gmsh_mesh_partitioned_reader&) = delete;               //copy constructor
        gmsh_mesh_partitioned_reader& operator=(const gmsh_mesh_partitioned_reader&) = delete;    //copy assignment operator
        gmsh_mesh_partitioned_reader(gmsh_mesh_partitioned_reader&&) = delete;                    //move constructor  
        gmsh_mesh_partitioned_reader& operator=(gmsh_mesh_partitioned_reader&&) = delete;         //move assignment operator 
        //-------------------------------------------------------------------------------------------------------------------------------------





        template<typename T>
        gmsh_mesh_partitioned_reader(const read_setup& setup, T& mesh, int num)
        { 
            int rank = get_rank();
            int size = get_size();
            if(rank==0)
            {
                /*---------generate mesh by gmsh API and write in file 'mesh_<num>.msh' ------------*/
                gmsh::initialize();             
                gmsh_mesh_generator mesh_generator(num, size);
                gmsh::finalize();          
            }       

            MPI_Barrier(MPI_COMM_WORLD);
            std::string mesh_name = "mesh/" + std::to_string(num) + ".msh";
            gmsh_mesh_partitioned_reader(mesh_name, setup, mesh);
        } 
         





        template<typename T>
        gmsh_mesh_partitioned_reader(const std::string& mesh_name, const read_setup& setup, T& mesh)
        { 

             std::ifstream infile(mesh_name);                              // opening the mesh file to read
             if(!infile.is_open())
             {
                Error("unable to open and read mesh file");
             }
   
   
             std::vector<std::string> result;
           
        
                                       
    
    
             //-----------------------------------------read mesh version------------------------------------------------------------------------
             skip_one_line(infile);                  //$MeshFormat
             read_one_line(infile, result);          //2.2 0 8    or   4.1 0 8
   
             int mesh_version(0);
             if(result[0] == "2.2")
             {                     
               mesh_version = 2;   
               print_only_pid<0>(std::cerr)<<"\n------------------gmsh partitioned mesh version is 2.2------------------ \n";           
             }
             else if(result[0] == "4.1")
             {
               mesh_version = 4;   
               print_only_pid<0>(std::cerr)<<"\n------------------gmsh partitioned mesh version is 4.1--------------------\n";            
             }
             else
             {
               Error("gmsh mesh version is not 2.2 or 4.1"); 
             }
             skip_one_line(infile);             //$EndMeshFormat
             //-----------------------------------------------------------------------------------------------------------------
        
        
    
    
    
    
    
    
    
    
    
    
             std::map<int,int> pt_map;          // while reading entities this is a map with key: entity tag of point   value: physical tag of point
             std::map<int,int> line_map;        // while reading entities this is a map with key: entity tag of line   value: physical tag of line
             std::map<int,int> surf_map;        // while reading entities this is a map with key: entity tag of surface   value: physical tag of surface
             std::map<int,int> vol_map;         // while reading entities this is a map with key: entity tag of volume   value: physical tag of volume

             std::map<int,int> pt_tag_map;       // while reading partitioned entities this is a map with key: entity tag of point   value: physical tag of point
             std::map<int,int> line_tag_map;     // while reading partitioned entities this is a map with key: entity tag of line   value: physical tag of line
             std::map<int,int> surf_tag_map;     // while reading partitioned entities this is a map with key: entity tag of surface   value: physical tag of surface
             std::map<int,int> vol_tag_map;      // while reading partitioned entities this is a map with key: entity tag of volume   value: physical tag of volume
        
             std::map<int,int> pt_pid_map;       // while reading partitioned entities this is a map with key: entity tag of point   value: pid
             std::map<int,int> line_pid_map;     // while reading partitioned entities this is a map with key: entity tag of line   value: pid
             std::map<int,int> surf_pid_map;     // while reading partitioned entities this is a map with key: entity tag of surface   value: pid
             std::map<int,int> vol_pid_map;      // while reading partitioned entities this is a map with key: entity tag of volume   value: pid

             if(mesh_version == 4)
             {
                      //-----------------------------------read entities------------------------------------------------------------------------------
                      // Note: if partitionedentities are present, then physical tags are defined by the partitionedentities and not by Entities
          
                      skip_one_line(infile);                   //$Entities
                      read_one_line(infile, result);          
                      int numPoints(stoi(result[0])), numCurves(stoi(result[1])), numSurfaces(stoi(result[2])), numVolumes(stoi(result[3])); 
                      for(int i=0; i<numPoints; i++)   
                      {
                         read_one_line(infile, result);
                         int entityTag = stoi(result[0]);
                         int numPhysicalTags = stoi(result[4]);
                         if(numPhysicalTags == 1)
                         {
                            int physicalTag = stoi(result[5]);            
                            if(physicalTag == 0)
                            {
                                std::stringstream ss;
                                ss<<"-----------------Error: gmsh did not write the right physical tag for"<<i<<"th point in "<< mesh_name <<" file ----------\n"
                                  <<"-----------------To do: check and edit manualy for the the right physical tag for the point in the Entities block ------------\n"
                                  <<"-----------------Generate mesh file again ------------------\n\n";               
                                Error(ss.str());
                            }
                            pt_map[entityTag] = physicalTag;               
                         }
                         if(numPhysicalTags > 1)
                         {
                             std::stringstream ss;                  
                             ss<<"-----------------Error: numPhysicalTags for the "<<i<<" th point is greater than 1; it should be 1 ----------"<<std::endl;
                             Error(ss.str());
                         }  
                      }
                      for(int i=0; i<numCurves; i++)   
                      {
                         read_one_line(infile, result);
                         int entityTag = stoi(result[0]);
                         int numPhysicalTags = stoi(result[7]);
                         if(numPhysicalTags == 1)
                         {
                             int physicalTag = stoi(result[8]);            
                             line_map[entityTag] = physicalTag;               
                         }
                         if(numPhysicalTags > 1)
                         {
                             std::stringstream ss;                  
                             ss<<"-----------------Error: numPhysicalTags for the "<<i<<" th point is greater than 1; it should be 1 ----------"<<std::endl;
                             Error(ss.str());
                         }  
                      }   
                      for(int i=0; i<numSurfaces; i++)   
                      {
                         read_one_line(infile, result);
                         int entityTag = stoi(result[0]);
                         int numPhysicalTags = stoi(result[7]);
                         if(numPhysicalTags == 1)
                         {
                             int physicalTag = stoi(result[8]);            
                             surf_map[entityTag] = physicalTag;               
                         }
                             if(numPhysicalTags > 1)
                         {
                             std::stringstream ss;                  
                             ss<<"-----------------Error: numPhysicalTags for the "<<i<<" th point is greater than 1; it should be 1 ----------"<<std::endl;
                             Error(ss.str());
                         }      
                      }   
                      for(int i=0; i<numVolumes; i++)   
                      {
                         read_one_line(infile, result);
                         int entityTag = stoi(result[0]);
                         int numPhysicalTags = stoi(result[7]);
                         if(numPhysicalTags == 1)
                         {
                             int physicalTag = stoi(result[8]);            
                             vol_map[entityTag] = physicalTag;               
                         }
                         if(numPhysicalTags > 1)
                         {
                             std::stringstream ss;                  
                             ss<<"-----------------Error: numPhysicalTags for the "<<i<<" th point is greater than 1; it should be 1 ----------"<<std::endl;
                             Error(ss.str    ());
                         }  
                      }   
                      skip_one_line(infile);                //$EndEntities                    
                      //-----------------------------------------------------------------------------------------------------------------




        
    
    
    
        
    
                      //--------------------------------------read partitioned entities---------------------------------------------------------------------------          
                      skip_one_line(infile);                   //  $PartitionedEntities
                      
                      read_one_line(infile, result);                        
                      int numPartitions = stoi(result[0]);
                      
                      read_one_line(infile, result);                                         
                      int numGhostEntities = stoi(result[0]);
                      
                      if(numGhostEntities > 0)
                      {
                             std::stringstream ss;                  
                             ss<<"-----------Error: numGhostEntities is greater than 0; it should be zero-------------------\n"
                                <<"-----------We do not read ghost entities-------------------"<< std::endl;
                             Error(ss.str());
                      }  
                      read_one_line(infile, result);                                         
                      numPoints = stoi(result[0]); 
                      numCurves = stoi(result[1]); 
                      numSurfaces = stoi(result[2]); 
                      numVolumes = stoi(result[3]);
          
          
                      for(int i=0; i<numPoints; i++)
                      {
                              read_one_line(infile, result);                                         
                              int pointTag(stoi(result[0])), parentDim(stoi(result[1])), parentTag(stoi(result[2])), numPartitions(stoi(result[3]));           
                              
                              if(numPartitions == 1)      
                                  pt_pid_map[pointTag] = stoi(result[4])-1;
                              
                              std::vector<int> partitionTag(numPartitions);
                              for(int j=0; j<numPartitions; j++)
                                  partitionTag[j] = stoi(result[4+j]);
                              
                              int numPhysicalTags = stoi(result[3+numPartitions+4]);
                              
                              if(numPhysicalTags == 1)
                              {
                                 int physicalTag = stoi(result[3+numPartitions+5]);
                                 if(physicalTag == 0)
                                 {
                                     std::stringstream ss;
                                     ss<<"-----------------Error: gmsh did not write the right physical tag for"<<i<<"th point in "<< mesh_name <<" file ----------\n"
                                       <<"-----------------To do: check and edit manualy for the the right physical tag for the point in the Entities block ------------\n"
                                       <<"-----------------Generate mesh file again ------------------\n\n";               
                                     Error(ss.str());
                                 }                       
                                 pt_tag_map[pointTag] = physicalTag;
                              }   
                              else if(numPhysicalTags > 1)
                              {
                                  std::stringstream ss;                  
                                  ss << "-----------------Error: numPhysicalTags for the " << i << "th point is greater than 1 in PartitionedEntities; it should be 1 or 0 ----------"<<std::endl;
                                  Error(ss.str());
                              }    
                      }       
          
                      for(int i=0; i<numCurves; i++)
                      {
                              read_one_line(infile, result);                                         
                              int curveTag(stoi(result[0])), parentDim(stoi(result[1])), parentTag(stoi(result[2])), numPartitions(stoi(result[3]));           
                              
                              if(numPartitions == 1)      
                                  line_pid_map[curveTag] = stoi(result[4])-1;
                              
                              std::vector<int> partitionTag(numPartitions);
                              for(int j=0; j<numPartitions; j++)
                                  partitionTag[j] = stoi(result[4+j]);
                              
                              int numPhysicalTags = stoi(result[3+numPartitions+7]);
                              
                              if(numPhysicalTags == 1)
                              {
                                 int physicalTag = stoi(result[3+numPartitions+8]);
                                 line_tag_map[curveTag] = physicalTag;
                              }   
                              else if(numPhysicalTags > 1)
                              {
                                  std::stringstream ss;                  
                                  ss << "-----------------Error: numPhysicalTags for the " << i << "th curve is greater than 1 in PartitionedEntities; it should be 1 or 0 ----------"<<std::endl;
                                  Error(ss.str());
                              }    
                      }       
                        
                      for(int i=0; i<numSurfaces; i++)
                      {
                              read_one_line(infile, result);                                         
                              int surfaceTag(stoi(result[0])), parentDim(stoi(result[1])), parentTag(stoi(result[2])), numPartitions(stoi(result[3]));           
                              
                              if(numPartitions == 1)      
                                  surf_pid_map[surfaceTag] = stoi(result[4])-1;
                              
                              std::vector<int> partitionTag(numPartitions);
                              for(int j=0; j<numPartitions; j++)
                                  partitionTag[j] = stoi(result[4+j]);
                              
                              int numPhysicalTags = stoi(result[3+numPartitions+7]);
                              
                              if(numPhysicalTags == 1)
                              {
                                 int physicalTag = stoi(result[3+numPartitions+8]);
                                 surf_tag_map[surfaceTag] = physicalTag;
                              }   
                              else if(numPhysicalTags > 1)
                              {
                                  std::stringstream ss;                  
                                  ss << "-----------------Error: numPhysicalTags for the " << i << "th surface is greater than 1 in PartitionedEntities; it should be 1 or 0 ----------"<<std::endl;
                                  Error(ss.str());
                              }    
                      }       
          
                      for(int i=0; i<numVolumes; i++)
                      {
                              read_one_line(infile, result);                                         
                              int volumeTag(stoi(result[0])), parentDim(stoi(result[1])), parentTag(stoi(result[2])), numPartitions(stoi(result[3]));           
                              
                              if(numPartitions == 1)      
                                  vol_pid_map[volumeTag] = stoi(result[4])-1;
                              
                              std::vector<int> partitionTag(numPartitions);
                              for(int j=0; j<numPartitions; j++)
                                  partitionTag[j] = stoi(result[4+j]);
                              
                              int numPhysicalTags = stoi(result[3+numPartitions+7]);
                              
                              if(numPhysicalTags == 1)
                              {
                                 int physicalTag = stoi(result[3+numPartitions+8]);
                                 vol_tag_map[volumeTag] = physicalTag;
                              }   
                              else if(numPhysicalTags > 1)
                              {
                                  std::stringstream ss;                  
                                  ss << "-----------------Error: numPhysicalTags for the " << i << "th volume is greater than 1 in PartitionedEntities; it should be 1 or 0 ----------"<<std::endl;
                                  Error(ss.str());
                              }    
                       }
                       skip_one_line(infile);              // $EndPartitionedEntities                     
                      //-----------------------------------------------------------------------------------------------------------------
 
             } // if(mesh_version == 4)
    
    
    
    
    
    
    




       
             //-----------------------------------------------read nodes------------------------------------------------------------------
             int n_nds(0);                       // number of mesh nodes
             std::vector<double> x, y, z;        // vector of node coordintes
      
             double start = MPI_Wtime();
             skip_one_line(infile);              //$Nodes
             if(mesh_version == 2)
             {
                  read_one_line(infile, result);          
                  n_nds = stoi(result[0]);         
                  x.resize(n_nds);
                  y.resize(n_nds);
                  if(dim == 3)
                     z.resize(n_nds);
                  for(int i=0; i<n_nds; i++)
                  {
                     read_one_line(infile, result);          
                     x[i] = stod(result[1]);
                     y[i] = stod(result[2]);
                     if(dim == 3)
                       z[i] = stod(result[3]);
                  }
             }   
             else if(mesh_version == 4)
             {
                  read_one_line(infile, result);          
                  n_nds = stoi(result[1]);
                  int numEntityBlocks(stoi(result[0])), minNodeTag(stoi(result[2])), maxNodeTag(stoi(result[3]));              
                  x.resize(n_nds);  
                  y.resize(n_nds);  
                  if(dim == 3)
                     z.resize(n_nds);  
                  for(int i=0; i<numEntityBlocks; i++)
                  {
                     read_one_line(infile, result);           
                     int entityDim(stoi(result[0])), entityTag(stoi(result[1])), parametric(stoi(result[2])), numNodesInBlock(stoi(result[3]));        
                     std::vector<int> nd_tag(numNodesInBlock);
                     for(int j=0; j<numNodesInBlock; j++)
                     {
                       read_one_line(infile, result);                             
                       nd_tag[j] = stoi(result[0])-1;
                     }
                     for(int j=0; j<numNodesInBlock; j++)
                     {
                       read_one_line(infile, result);   
                       x[nd_tag[j]] = stod(result[0]);
                       y[nd_tag[j]] = stod(result[1]);
                       if(dim == 3)
                         z[nd_tag[j]] = stod(result[2]);
                     }
                  }
             }
             skip_one_line(infile);              //$EndNodes
             print_only_pid<0>(std::cerr)<<"Nd reading took: "<< MPI_Wtime()-start<<" s\n";           
             //-----------------------------------------------------------------------------------------------------------------
        
             
             double min_x(0.0), max_x(0.0), min_y(0.0), max_y(0.0), min_z(0.0), max_z(0.0);  
             auto minmax_x = std::minmax_element(x.begin(), x.end());
             auto minmax_y = std::minmax_element(y.begin(), y.end());
             
             min_x = *minmax_x.first;       max_x = *minmax_x.second;       
             min_y = *minmax_y.first;       max_x = *minmax_y.second;       

             if(dim==3)
             {
               auto minmax_z = std::minmax_element(z.begin(), z.end());
               min_z = *minmax_z.first;       max_z = *minmax_z.second;                      
             } 
             
             mesh.min_x() = min_x;
             mesh.max_x() = max_x;
             mesh.min_y() = min_y;
             mesh.max_y() = max_y;
             mesh.min_z() = min_z;
             mesh.max_z() = max_z;
      
             
    
    
    
    
    
    
    
    
    
             //------------------------------------------------read_elements-----------------------------------------------------------------
             /// Reads the elements owned by the processor.
             /// Elements are stored with complete id, gid node list and boundary information.
             /// Each element has gids of the connected nodes in the form of "node_gid_vec_".    


             std::vector<int> pt;                  // while reading elements this is vector of points 
             std::vector<int> pt_tag;              // while reading elements this is vector of physical tag of points
             std::vector<int> pt_pid;              // while reading elements this is vector of pid of points
        
             std::vector<std::vector<int>> line;     // while reading elements this is vector of lines 
             std::vector<int> line_tag;              // while reading elements this is vector of physical tag of lines
             std::vector<int> line_pid;              // while reading elements this is vector of pid of lines
             
             std::vector<std::vector<int>> surf;     // while reading elements this is vector of surfaces 
             std::vector<int> surf_tag;              // while reading elements this is vector of physical tag of surfaces
             std::vector<int> surf_pid;              // while reading elements this is vector of pid of surfaces
        
             std::vector<std::vector<int>> vol;     // while reading elements this is vector of volumes 
             std::vector<int> vol_tag;              // while reading elements this is vector of physical tag of volumes
             std::vector<int> vol_pid;              // while reading elements this is vector of pid of volumes
 
 
             start = MPI_Wtime();
             skip_one_line(infile);                //$Elements         
             if(mesh_version == 2)
             {         
                read_one_line(infile, result);          
                int n_els_tot = stoi(result[0]);                  
                for(int i=0; i<n_els_tot; i++)
                {
                   read_one_line(infile, result);
                   int elm_number(stoi(result[0])), elm_type(stoi(result[1])), number_of_tags(stoi(result[2]));
    
                   std::vector<int> tags(number_of_tags);
                   for(int j=0; j<number_of_tags; j++) 
                      tags[j] = stoi(result[3+j]);
                      
                   int physical_tag = tags[0];
                   int geometrical_entity = tags[1];
                   int pid = get_pid(tags);
    
                   switch(elm_type)
                   {
                      case 15:        //point
                      {
                         if(physical_tag==0)
                         {
                            std::stringstream ss;                  
                            ss << "-----------------Error: gmsh did not write the right physical tag for"<<i<<"th element (a point) in "<< mesh_name <<" file ----------\n"
                               <<"-----------------To do: check and edit manualy for the the right physical tag ------------------\n"
                               <<"-----------------Generate mesh file again ------------------\n\n";
                            Error(ss.str());
                         }
                         pt.push_back(stoi(result[2+number_of_tags+1]));
                         pt_tag.push_back(physical_tag);                  
                         if(pid >= 0)  pt_pid.push_back(pid);                     
                         break;
                      }
                      case 1:         //line
                      {
                         std::vector<int> v = {stoi(result[2+number_of_tags+1]), stoi(result[2+number_of_tags+2])}; 
                         line.push_back(v);
                         line_tag.push_back(physical_tag);
                         if(pid >= 0)  line_pid.push_back(pid);                     
                         break;               
                      }
                      case 2:         //tri
                      {
                         std::vector<int> v = {stoi(result[2+number_of_tags+1]), stoi(result[2+number_of_tags+2]), stoi(result[2+number_of_tags+3])}; 
                         surf.push_back(v);               
                         surf_tag.push_back(physical_tag);
                         if(pid >= 0)  surf_pid.push_back(pid);                     
                         break;               
                      }
                      case 3:        //quad
                      {
                         std::vector<int> v = {stoi(result[2+number_of_tags+1]), stoi(result[2+number_of_tags+2]), stoi(result[2+number_of_tags+3]), stoi(result[2+number_of_tags+4])}; 
                         surf.push_back(v);                              
                         surf_tag.push_back(physical_tag);
                         if(pid >= 0)  surf_pid.push_back(pid);                     
                         break;               
                      }
                      case 4:        //tetra
                      {
                         std::vector<int> v = {stoi(result[2+number_of_tags+1]), stoi(result[2+number_of_tags+2]), stoi(result[2+number_of_tags+3]), stoi(result[2+number_of_tags+4])}; 
                         vol.push_back(v);                              
                         vol_tag.push_back(physical_tag);
                         if(pid >= 0)   vol_pid.push_back(pid);                     
                         break;               
                      }
                      case 5:        //hexa
                      {
                         std::vector<int> v = {stoi(result[2+number_of_tags+1]), stoi(result[2+number_of_tags+2]), stoi(result[2+number_of_tags+3]), stoi(result[2+number_of_tags+4]), 
                                          stoi(result[2+number_of_tags+5]), stoi(result[2+number_of_tags+6]), stoi(result[2+number_of_tags+7]), stoi(result[2+number_of_tags+8])}; 
                         vol.push_back(v);                                             
                         vol_tag.push_back(physical_tag);
                         if(pid >= 0)  vol_pid.push_back(pid);                     
                         break;               
                      }
                   }            
                }   
             }
             else if(mesh_version == 4)
             {
                 read_one_line(infile, result);
                 int numEntityBlocks(stoi(result[0])), n_els_tot(stoi(result[1])), minElementTag(stoi(result[2])), maxElementTag(stoi(result[3]));
                 for(int i=0; i<numEntityBlocks; i++)
                 {
                    read_one_line(infile, result);
                    int entityDim(stoi(result[0])), entityTag(stoi(result[1])), elementType(stoi(result[2])), numElementsInBlock(stoi(result[3]));              
                    if(entityDim == 0)                  //point
                    {
                       for(int j=0; j<numElementsInBlock; j++)
                       {
                          read_one_line(infile, result);                         
                          pt.push_back(stoi(result[1]));                      
                          pt_tag.push_back(pt_tag_map.at(entityTag));
                          pt_pid.push_back(pt_pid_map.at(entityTag));
                       }
                    }
                    else if(entityDim == 1)             //line
                    {
                       for(int j=0; j<numElementsInBlock; j++)
                       {
                          read_one_line(infile, result);
                          std::vector<int> v = {stoi(result[1]), stoi(result[2])};
                          line.push_back(v);
                          line_tag.push_back(line_tag_map.at(entityTag));
                          line_pid.push_back(line_pid_map.at(entityTag));
                       }
                    }
                    else if(entityDim == 2 && elementType == 2)      //tri
                    {
                       for(int j=0; j<numElementsInBlock; j++)
                       {
                          read_one_line(infile, result);
                          std::vector<int> v = {stoi(result[1]), stoi(result[2]), stoi(result[3])};
                          surf.push_back(v);
                          surf_tag.push_back(surf_tag_map.at(entityTag));
                          surf_pid.push_back(surf_pid_map.at(entityTag));
                       }
                    }
                    else if(entityDim == 2 && elementType == 3)     //quad
                    {
                       for(int j=0; j<numElementsInBlock; j++)
                       {
                          read_one_line(infile, result);
                          std::vector<int> v = {stoi(result[1]), stoi(result[2]), stoi(result[3]), stoi(result[4])};
                          surf.push_back(v);
                          surf_tag.push_back(surf_tag_map.at(entityTag));
                          surf_pid.push_back(surf_pid_map.at(entityTag));
                       }
                    }
                    else if(entityDim == 3 && elementType == 4)      //tetra
                    {
                       for(int j=0; j<numElementsInBlock; j++)
                       {
                          read_one_line(infile, result);
                          std::vector<int> v = {stoi(result[1]), stoi(result[2]), stoi(result[3]), stoi(result[4])};
                          vol.push_back(v);
                          vol_tag.push_back(vol_tag_map.at(entityTag));
                          vol_pid.push_back(vol_pid_map.at(entityTag));
                       }
                    }
                    else if(entityDim == 3 && elementType == 5)       //hexa
                    {
                       for(int j=0; j<numElementsInBlock; j++)
                       {
                          read_one_line(infile, result);
                          std::vector<int> v = {stoi(result[1]), stoi(result[2]), stoi(result[3]), stoi(result[4]),
                                           stoi(result[5]), stoi(result[6]), stoi(result[7]), stoi(result[8])};
                          vol.push_back(v);
                          vol_tag.push_back(vol_tag_map.at(entityTag));
                          vol_pid.push_back(vol_pid_map.at(entityTag));
                       }
                    }
                 }
             }         
             skip_one_line(infile);             //$EndElements
             
             
             if(pt.size() != pt_tag.size() || pt_tag.size() != pt_pid.size())
                 Error("Either    pt.size() != pt_tag.size()      or     pt_tag.size() != pt_pid.size()");
                 
             if(line.size() != line_tag.size() || line_tag.size() != line_pid.size())
                 Error("Either    line.size() != line_tag.size()      or     line_tag.size() != line_pid.size()");
                 
             if(surf.size() != surf_tag.size() || surf_tag.size() != surf_pid.size())
                 Error("Either    surf.size() != surf_tag.size()      or     surf_tag.size() != surf_pid.size()");
                 
             if(vol.size() != vol_tag.size() || vol_tag.size() != vol_pid.size())
                 Error("Either    vol.size() != vol_tag.size()      or     vol_tag.size() != vol_pid.size()");
                 
             if(vol.size()==0 && dim==3)
                 Error("gmsh mesh is 2D but dim is set equal to 3 in the setup.txt file");
             
             if(vol.size()!=0 && dim==2)
                 Error("gmsh mesh is 3D but dim is set equal to 2 in the setup.txt file");
                 
             
             print_only_pid<0>(std::cerr)<<"El reading took: "<< MPI_Wtime()-start<<" s\n";
             //-----------------------------------------------------------------------------------------------------------------
           
           
           
           
           
           
           
           
             infile.close();






            
            
            
            
            
            
            
            
             //-------------------------------------------- mesh_info ---------------------------------------------------------------------

             bool is_mesh_3d;              // bool to detetct if mesh is 3D or not
             if(vol.size()==0 && dim==2)       is_mesh_3d = false;
             else if(vol.size()!=0 && dim==3)   is_mesh_3d = true;






             //--------------------setting of node flags------------------------------------------------------            
             start = MPI_Wtime();
             std::vector<int> nd_flag(n_nds, 0);          // vector of flag for nodes: for interior nodes flag=0; for boundary nodes flag!=0  
             
             if(is_mesh_3d)
             {
               for(int i=0; i<surf.size(); i++)    // Check if mesh is 3D then loop over all surf elements and overwrite flags of nodes of an element equal to the flag of the element
                 for(auto j : surf[i])
                   nd_flag[j-1] = surf_tag[i];           
             }     
                        
             for(int i=0; i<line.size(); i++)       // Then we loop over all line elements(2D and 3D) and overwrite flags of nodes of an element equal to the flag of the line
               for(auto j : line[i])
                 nd_flag[j-1] = line_tag[i];           
    
             for(int i=0; i<pt.size(); i++)           //Then we loop over all points(2D and 3D) and overwrite flags of nodes equal to the flag of the pt
               nd_flag[pt[i]-1] = pt_tag[i];                        
 
             print_only_pid<0>(std::cerr)<<"setting of node flags took: "<< MPI_Wtime()-start<<" s\n";
             //-------------------------------------------------------------------------------





         

             std::vector<std::vector<int>> els;
             std::vector<int> el_pid;
             std::string el_type;
 
             std::vector<std::vector<int>> sides;
             std::vector<int> sd_tag;
             std::vector<int> sd_pid;
             int nb_gp(0);

             //------------------extracting info from mesh ----------------------------------------         
             if(is_mesh_3d)
             {
                 els = std::move(vol);
                 if(els[0].size()==4)  el_type = "tetra";
                 else if (els[0].size()==8) el_type = "hexa";
                 else Error("In 3D, hexa el must have 8 nodes and tetra el must have 4 nodes");
                 
                 sides = std::move(surf);
                 sd_tag = std::move(surf_tag);
    
                 el_pid = std::move(vol_pid);
                 sd_pid = std::move(surf_pid);
                 
                 if(el_type == "hexa")
                 {
                    if(setup.nb_gauss_integration_points()==-1 || setup.nb_gauss_integration_points()==8)   nb_gp = 8;  // default value
                    else if(setup.nb_gauss_integration_points()==1)  nb_gp = 1;
                    else Error("For 3D linear hexahedron, the number of integration points should be 8(default) or 1");
                 }
                 else if(el_type == "tetra")
                 {
                    if(setup.nb_gauss_integration_points()==-1 || setup.nb_gauss_integration_points()==4)  nb_gp = 4;  // default value
                    else if(setup.nb_gauss_integration_points()==1)  nb_gp = 1;
                    else Error("For 3D linear tetrahedron, the number of integration points should be 4(default) or 1");
                 }
             }
             else
             {                 
                 els = std::move(surf);
                 if(els[0].size()==3)  el_type = "tri";
                 else if (els[0].size()==4) el_type = "quad";
                 else Error("In 2D, quadrangle el must have 4 nodes and triangle el must have 3 nodes");
                 
                 sides = std::move(line);
                 sd_tag = std::move(line_tag);
    
                 el_pid = std::move(surf_pid);
                 sd_pid = std::move(line_pid);             
                
                 if(el_type == "quad")
                 {
                    if(setup.nb_gauss_integration_points()==-1 || setup.nb_gauss_integration_points()==4)   nb_gp = 4;  // default value
                    else if(setup.nb_gauss_integration_points()==1)  nb_gp = 1;
                    else Error("For 2D linear quadrangles, the number of integration points should be 4(default) or 1");
                 }
                 else if(el_type == "tri")
                 {
                    if(setup.nb_gauss_integration_points()==-1 || setup.nb_gauss_integration_points()==3)  nb_gp = 3;  // default value
                    else if(setup.nb_gauss_integration_points()==1)  nb_gp = 1;
                    else Error("For 2D linear triangles, the number of integration points should be 3(default) or 1");
                 }
             }
            //-------------------------------------------------------------------------------






                                                        
            
            
            
            
             //--------------------setting of element flags------------------------------------------------------            
             start = MPI_Wtime();
             std::vector<int> el_flag(els.size(), 0);
             
             
             if(el_type=="hexa")               
             {
               for(int i=0; i<els.size(); i++)
               {
                  auto tot = 0;
                  for(auto j : els[i])
                     if(nd_flag[j-1]>0)  
                         tot += 1;
                  if(tot>=4)                   //for hexa el we have quad as side
                      el_flag[i] = 1;
               }       
             }         
             else if(el_type=="tetra")         
             {
               for(int i=0; i<els.size(); i++)
               {
                  std::vector<int> el_node_flags(els[0].size(), 0);
                                    
                  auto tot = 0;
                  int counter = 0;
                  for(auto j : els[i])
                  {
                     el_node_flags[counter] = nd_flag[j-1];
                     
                     if(nd_flag[j-1]>0)  
                         tot += 1;
                     
                     counter++;    
                  } 
                  
                  auto max = *max_element(el_node_flags.begin(), el_node_flags.end());
                        
                  if(tot>=3)
                  {                 //for tetra el we have tri as side
                      el_flag[i] = 1;  
                      if(max == 4)
                          el_flag[i] = 4;
                  }  
               }       
             }         
             else if(el_type=="quad" || el_type=="tri")  
             {
               for(int i=0; i<els.size(); i++)
               {
                  auto tot = 0;
                  for(auto j : els[i])
                     if(nd_flag[j-1]>0)  
                         tot += 1;
                  if(tot>=2)                //for tri and quad el we have line as side
                      el_flag[i] = 1;
               }       
             }         
 
             print_only_pid<0>(std::cerr)<<"setting of element flags took: "<< MPI_Wtime()-start<<" s\n";
            //-------------------------------------------------------------------------------
            
            
            
            



             mesh.tot_nodes(n_nds);
             print_only_pid<0>(std::cerr)<< "nodes:  "<< mesh.tot_nodes() << "\n" ;
             print_only_pid<0>(std::cerr)<< "elements ("<<el_type<<"):  "<<els.size() << "\n";
             print_only_pid<0>(std::cerr)<< "sides:  "<<sides.size() << "\n";
            
             
             
            
            
             //-----------------------elements on owner pids-------------------------   
             start = MPI_Wtime();
             const int rank = get_rank();
             const int size = get_size();
             const int avg_num_els_per_pid = (int) (els.size()/size);      
             const int avg_num_nds_per_pid = (int) (mesh.tot_nodes()/size);      
             mesh.elements().reserve(avg_num_els_per_pid);            
             std::vector<int> my_nodes;   // my_nodes contains host and ghost nodes
             my_nodes.reserve(avg_num_nds_per_pid);
             const int el_num_nodes = els[0].size();
             
             
             int count(0);    
             for(int i=0; i<els.size(); i++)            
             {
                if(el_pid[i] == rank)
                {
                  auto el =  std::make_shared<element<dim>>(el_num_nodes, nb_gp);
                  el->pid(el_pid[i]);
                  el->gid(i);
                  el->lid(count);
                  auto el_nds = base_1to0(els[i]);
                  el->node_gid_vec(el_nds);
                  my_nodes.insert(std::end(my_nodes), std::begin(el_nds), std::end(el_nds));
                  el->el_flag(el_flag[i]);
                  el->on_boundary(el_flag[i] > 0);
                  mesh.elements().push_back(std::move(el));
                  count++;
                }              
             }
             sort_unique(my_nodes);
             print_only_pid<0>(std::cerr)<<"Setting of gales mesh elements took: "<< MPI_Wtime()-start<<" s\n";
             //-------------------------------------------------------------------------------
 
 
 
 
             ///---------------------- fill bd els container---------------------------------   
             for(const auto& el : mesh.elements())
               if(el->on_boundary())
                  mesh.bd_elements().push_back(el);
             //-------------------------------------------------------------------------------
                 
                 
                                
             std::map<int, int> gid_lid_map;
 
             //----------------------- host and ghost nodes on pids-------------------------   
             start = MPI_Wtime();
             mesh.nodes().resize(my_nodes.size());
             count = 0;            
             for(int i=0; i<my_nodes.size(); i++)
             {
                auto nd = std::make_shared<node<dim>>();
                int nd_num = my_nodes[i];
                nd->gid(nd_num);
                nd->lid(count);
                gid_lid_map[nd_num] = count;
                nd->set_x(x[nd_num]);
                nd->set_y(y[nd_num]);
                if(dim == 3)
                   nd->set_z(z[nd_num]);
                nd->flag(nd_flag[nd_num]);
                nd->on_boundary(nd_flag[nd_num] > 0);                                             
                mesh.nodes()[count] = std::move(nd);  
                count++;            
             }
             print_only_pid<0>(std::cerr)<<"Setting of gales mesh nodes took: "<< MPI_Wtime()-start<<" s\n";
             //-------------------------------------------------------------------------------
 
 
 
 
             ///------------------------------- fill bd nodes container--------------------------   
             for(const auto& nd : mesh.nodes())
               if(nd->on_boundary())
                 mesh.bd_nodes().push_back(nd);         
             //-------------------------------------------------------------------------------
                           
             
             
              
 
 
             //-------------------------------------------------------------------------------                  
             start = MPI_Wtime();
             /// Next we set the ownership of each node i.e. to which process it belongs.
             /// Owner is set according to the highest pid number.
             /// for example if node 11 is shared on pid 3 and 4 then we set owner = 4
             /// We should set it according to the nd pid file generated by Metis
             std::vector<int> nd_gids(mesh.nodes().size());
             count = 0;
             for(const auto& nd : mesh.nodes())
             {
                 nd_gids[count] = nd->gid();
                 count++;
             }
             
             std::vector<int> ownership(n_nds,-1);
             for(auto i : nd_gids)
               ownership[i] = rank;
         
             std::vector<int> ownership_tmp(n_nds);
             MPI_Allreduce(&ownership[0], &ownership_tmp[0], n_nds, MPI_INT, MPI_MAX, MPI_COMM_WORLD);
         
             for(auto& nd : mesh.nodes())
               nd->pid(ownership_tmp[nd->gid()]);    
               
             print_only_pid<0>(std::cerr)<<"Setting of pid for gales mesh nodes took: "<< MPI_Wtime()-start<<" s\n";
             //-------------------------------------------------------------------------------
 
   
   


                                                
             ///------------------- Next we collect the host nodes on each pid-----------------
              for(const auto& nd : mesh.nodes())
                if(nd->pid() == rank)
                  mesh.host_nodes().push_back(nd);         
             //-------------------------------------------------------------------------------
   
                                                   
  
  
 
 
            ///---------------- Define link between each element and its nodes---------------------
            start = MPI_Wtime();
            for(auto& el : mesh.elements())
             {    
               for(int i=0; i<el->nb_nodes(); i++)
               {
                 const int nd_gid = el->node_gid_vec()[i];
                 const int nd_lid = gid_lid_map[nd_gid];
                 el->el_node_vec(mesh.nodes()[nd_lid]);                            /// fill the el_node_vec_ for each element with nodes having complete lid and gid
               }
               //now we know the nodes belonging to the element so we can compute the bound which requires node coordinates info
               el->compute_dofs_gids();                  // here we compute vector of dofs gids of the element
             }  
             print_only_pid<0>(std::cerr)<<"seting Element node links took: "<< MPI_Wtime()-start<<" s\n";
             //-------------------------------------------------------------------------------
  
                 
                 
                 
                     
             //-----------------------reading sides on owner pids-------------------------   
             start = MPI_Wtime();
             for(int i=0; i<sides.size(); i++)            
             {
                if(sd_pid[i] == rank)
                {
                   mesh.sides().push_back(base_1to0(sides[i]));
                   mesh.side_flag().push_back(sd_tag[i]);           
                }            
             } 
             print_only_pid<0>(std::cerr)<<"Setting of gales mesh sides took: "<< MPI_Wtime()-start<<" s\n";
             //-------------------------------------------------------------------------------
            
            


             //---------set el_side_nodes, el_side_flag----------------------------------------
             start = MPI_Wtime();
             for(auto& el : mesh.elements())
             {
               if(el->on_boundary() && el->el_flag()==4)
               {
                 const auto& nds = el->el_node_vec();        
                 for(int i=0; i<el->nb_sides(); i++)
                 {
                    std::vector<int> side_nds;
                    for(int j=0; j<el->nb_side_nds(i); j++)
                    {
                       side_nds.push_back(nds[el->side(i,j)]->gid());
                    }
                    el->set_side_nodes(std::move(side_nds));                                      
       
                    int sd_flag = 0;
                    int k = 0;
                    for(const auto& j : el->side(i))
                    {
                      if(nds[j]->on_boundary())   
                        k++;
                    }  
                    if(k == el->nb_side_nds(i))
                    {
                      sd_flag = 4;             
                    }                                          
                    el->set_side_flag(sd_flag);
       
                 
                    bool side_on_bd = false;
                    if(sd_flag>0) side_on_bd = true;
                    el->set_side_on_boundary(side_on_bd);              
                 }
               }                
             } 
             print_only_pid<0>(std::cerr)<<"El side info setting took: "<< MPI_Wtime()-start<<" s\n";
             //-------------------------------------------------------------------------------
 
 
             
                                  
        
        



//             //--------------------------------------------compute_el_quad_data --------------------------------------------------------------------
//             /// here we compute the data at quadrature points for each element 
//             start = MPI_Wtime();
//             for(auto& el : mesh.elements())
//             {
//               for(int i=0; i<el->nb_gp(); i++)
//               {
//                  auto q = std::make_shared<quad>(*el, el->gp(i));
//                  el->quad_i()[i] = std::move(q);
//               }        
//             }
//       
//             for(auto& el : mesh.bd_elements())
//             {
//                 for(int i=0; i<el->nb_sides(); i++)
//                 {
//                    std::vector<std::shared_ptr<quad>> v(el->nb_side_gp(i), nullptr);
//                    if(el->is_side_on_boundary(i))
//                    {
//                       for(int j=0; j<el->nb_side_gp(i); j++)
//                       {
//                         auto q = std::make_shared<quad>(*el, el->side_gp(i,j), el->side_gp(i,j));
//                         v[j] = std::move(q);
//                       }
//                    }
//                    el->quad_b()[i] = v;
//                 }
//             }
//             print_only_pid<0>(std::cerr)<<"El quad data setting took: "<< MPI_Wtime()-start<<" s\n";            
//             //-----------------------------------------------------------------------------------------------------------------
         
         
         
                         
         
         
        } // end of constructor
         









         auto base_1to0(const std::vector<int>& v)
         {
            std::vector<int> v1(v.size());
            for(int i=0; i<v.size(); i++)
              v1[i] = v[i] - 1;
            return v1;  
         }
         
     
     
     
     
     
         int get_pid(const std::vector<int>& tags)
         {
            int pid = -1;
            if(tags.size() == 4)
            {
               int num_mesh_partitions_of_el = tags[2];
               if(num_mesh_partitions_of_el == 1 && tags[3] >= 0)
                     pid = tags[3]-1;
               else
               {
                 std::stringstream ss;               
                 ss<<"num_mesh_partitions_of_el: "<< num_mesh_partitions_of_el<<"\n"
                   <<"Partition Id: "<< tags[3] << "\n" 
                   <<"-----------------Error----------------------------------------------------------------\n" 
                   <<" 1) Either the number of mesh partitions to which the element belong is more than 1 \n"
                   <<" 2) Or Partition Id is negative means ghost cell "<< std::endl;                                
                 Error(ss.str());
               }      
             }        
             return pid;       
         }

     
  };
    
    



}
//namespace GALES
#endif


