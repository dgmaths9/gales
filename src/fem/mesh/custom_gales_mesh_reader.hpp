#ifndef _GALES_CUSTOM_GALES_MESH_READER_HPP_
#define _GALES_CUSTOM_GALES_MESH_READER_HPP_





namespace GALES {


  /**
      This class defines the custom gales mesh reader.

    The mesh file we read already contains the mesh partition information and nd and el flags.
   
    
   
    el          0    1    2    3                   nd          0    1    2    3    4
    PID         0    0    1    1                   PID         0    0    1    1    1
             o----o---)o(---o----o                            o----o---)o(---o----o
    el GID      0    1    2    3                   nd GID      0    1    2    3    4
    el LID(0)   0    1                             nd LID(0)   0    1    2
    el LID(1)             0    1                   nd LID(1)             0    1    2   
    
    
    Each process reads and stores only those elements which are owned by it.      
    An element has a unique GID and a unique PID(disjoint partition). Element LID is defined as the unique local counter number on each process. 
    Therefore two different elements lying on two different processes can have the same LID.
    
    Similarly each process stores and reads the nodes owned by it. 
    A node has a unique GID. some nodes lie on inter process boundaries (e.g. see node 2 above).
    In this case, we define the owner PID of the node based on the largest integer number of the shared PIDs.
    Node LID is defined as the unique local counter number on each process.  Two different nodes lying on two different processes can have the same LID.
    
    Since we do most of the operations through LIDs, each process is responsible for executing the operation on its nodes and elements. 
    This applies even on the shared nodes. For example to do mesh update nodes with GIDs 0,1,2 will be updated by PID 0 and nodes with GIDs 2,3,4 will be
    updated by PID 1. Note that Node 2 is stored as two different objects on respective PIDs:  LID 2 on PID 0 and LID 0 on PID 1.  
    So, it will not be updated twice.
  */


  template<int dim>
  class custom_gales_mesh_reader 
  {
        
    public:                                
                
        template<typename T>
        custom_gales_mesh_reader(std::string mesh_name, const read_setup& setup, T& mesh) : setup_(setup) 
        { 
          std::ifstream infile(mesh_name);                              // opening the mesh file to read
          if(!infile.is_open())
          {
             Error("unable to open and read mesh file");
          }
      
    
    
          read_header(infile, mesh);
          

    
          double el_read_start = MPI_Wtime();
          std::vector<int> my_nodes;          
          read_elements(infile, my_nodes, mesh);          
          print_only_pid<0>(std::cerr)<<"El reading took: "<< MPI_Wtime()-el_read_start<<" s\n";      
    
    
    
    
          double side_read_start = MPI_Wtime();
          read_sides(infile, mesh);
          print_only_pid<0>(std::cerr)<<"Side reading took: "<< MPI_Wtime()-side_read_start<<" s\n";      
    
    
          
          infile.clear();
          infile.seekg(std::ios_base::beg);           ///rewind the file to read nodes
    
    
    
    
          double nd_read_start = MPI_Wtime();
          std::map<int, int> gid_lid_map;
          read_nodes(infile, my_nodes, mesh, gid_lid_map);
          print_only_pid<0>(std::cerr)<<"Nd reading took: "<< MPI_Wtime()-nd_read_start<<" s\n";
    
    
          infile.close();                                           // closing the mesh file after reading
    
    
          /// With this function each element has el_node_vec_ of its connected nodes; nodes know their lid and gid on respective pid
          double set_element_node_links_start = MPI_Wtime();
          set_element_node_links(my_nodes, mesh, gid_lid_map);
          print_only_pid<0>(std::cerr)<<"seting Element node links took: "<< MPI_Wtime()-set_element_node_links_start<<" s\n";
    
    
          compute_el_min_h(mesh);
          
          /// This functions reads the info about side_nodes, side_flag etc for each element 
          double el_side_info_start = MPI_Wtime();
          el_side_info(mesh);
          print_only_pid<0>(std::cerr)<<"El side info setting took: "<< MPI_Wtime()-el_side_info_start<<" s\n";
            


          /// This functions computes the data at quadrature points for each element 
          double el_quad_data_start = MPI_Wtime();
          compute_el_quad_data(mesh);
          print_only_pid<0>(std::cerr)<<"El quad data setting took: "<< MPI_Wtime()-el_quad_data_start<<" s\n";
        }
        
        






        //-------------------------------------------------------------------------------------------------------------------------------------        
        /// Deleting the copy and move constructors - no duplication/transfer in anyway
        custom_gales_mesh_reader() = default;
        custom_gales_mesh_reader(const custom_gales_mesh_reader&) = delete;               //copy constructor
        custom_gales_mesh_reader& operator=(const custom_gales_mesh_reader&) = delete;    //copy assignment operator
        custom_gales_mesh_reader(custom_gales_mesh_reader&&) = delete;                    //move constructor  
        custom_gales_mesh_reader& operator=(custom_gales_mesh_reader&&) = delete;         //move assignment operator 
        //-------------------------------------------------------------------------------------------------------------------------------------
        
        
        
        
            
            
            
            
    
        //-----------------------------------------------------------------------------------------------------------------
        template<typename T>        
        void read_header(std::ifstream& infile, T& mesh)
        {             
          std::vector<std::string> result;
          
          read_one_line( infile, result);   ///MESH! 2D            or         MESH! 3D
          const int mesh_dim = (int)(result[1][0] - '0');
          if(mesh_dim != dim)
             Error("mesh dimension is not same as the solver;  Either mesh is in 2D and in setup file the dim is set 3D  or  vice versa");
    
          read_one_line( infile, result);   ///nodes  <n>   
          mesh.tot_nodes(std::stoi(result[1]));    /// this sets total number of nodes of mesh
          print_only_pid<0>(std::cerr)<< "nodes:  "<< mesh.tot_nodes() << "\n" ;
    
          read_one_line( infile, result);  ///elements  <n>
          tot_elements_ = std::stoi(result[1]);  /// total number of elements of mesh
          print_only_pid<0>(std::cerr)<< "elements:  "<<tot_elements_ << "\n";
          
          read_one_line( infile, result);   ///sides  <n>
          tot_sides_ = std::stoi(result[1]);  /// this sets total number of bd sides of mesh
          print_only_pid<0>(std::cerr)<< "sides:  "<<tot_sides_ << "\n";
    
          
          double min_x(0.0), max_x(0.0), min_y(0.0), max_y(0.0), min_z(0.0), max_z(0.0);  

          read_one_line( infile, result);   /// This reads  mesh dimension
          min_x = std::stod(result[1]);      max_x = std::stod(result[2]);
          
          read_one_line( infile, result);   /// This reads  mesh dimension
          min_y = std::stod(result[1]);      max_y = std::stod(result[2]);

          if(dim==3)
          {
             read_one_line( infile, result);   /// This reads  mesh dimension
             min_z = std::stod(result[1]);      max_z = std::stod(result[2]);          
          }

          mesh.min_x() = min_x;
          mesh.max_x() = max_x;
          mesh.min_y() = min_y;
          mesh.max_y() = max_y;
          mesh.min_z() = min_z;
          mesh.max_z() = max_z;
        }
        //-----------------------------------------------------------------------------------------------------------------
        
        
    
    
    
    
    
    
    
    
    
        //-----------------------------------------------------------------------------------------------------------------
        /// Reads the elements owned by the processor.
        /// Elements are stored with complete id, gid node list and boundary information.
        /// Each element has gids of the connected nodes in the form of "node_gid_vec_".    
        template<typename T>        
        void read_elements(std::ifstream& infile, std::vector<int>& my_nodes, T& mesh)
        {
          const int rank = get_rank();
          const int size = get_size();
          const int avg_num_els_per_pid = (int) (tot_elements_/size);      
          const int avg_num_nds_per_pid = (int) (mesh.tot_nodes()/size);      
          mesh.elements().reserve(avg_num_els_per_pid);
          my_nodes.reserve(avg_num_nds_per_pid);
          
          int count(0);
          std::vector<std::string> result;
    
          for(int i=0; i<mesh.tot_nodes(); i++)     /// skip all lines starting with Node
             skip_one_line(infile);
          
          for(int i=0; i<tot_elements_; i++)  /// start reading elements
          {
            read_one_line(infile, result);          ///for quad element:     Element 0 0 4 3 13 16 12 1
            const int pid = std::stoi(result[2]);
                      
            ///  Only elements on the current rank are read.
            if(pid == rank)
            {
              const int gid = std::stoi(result[1]); 
              const int nb_nodes = std::stoi(result[3]); 
              const int nb_gp = get_nb_gp(nb_nodes);
      
              auto el =  std::make_shared<element<dim>>(nb_nodes, nb_gp); 
              el->pid(pid);
              el->gid(gid);
              el->lid(count);
                
              std::vector<int> node_gid_vec(nb_nodes);          
              for(int j=0; j<nb_nodes; j++)
              {
                const int nd_gid = std::stoi(result[4+j]);
                node_gid_vec[j] = nd_gid;            
                my_nodes.push_back(nd_gid);
              }
              el->node_gid_vec(std::move(node_gid_vec));            
      
              const int flag = std::stoi(result[4+nb_nodes] );         
              el->on_boundary(flag>0);
              mesh.elements().push_back(std::move(el));
      
              count++;
            }
          }
          
       
          /// Fill "bd_elements_" container  
          for(const auto& el : mesh.elements())
            if(el->on_boundary())
              mesh.bd_elements().push_back(el);    
          
    
          sort_unique(my_nodes);
          /// my_nodes contains a sorted and non repeating list of gids of nodes defined on each pid.
          /// some node gids are shared among multiple pids due to sharing of same node on multiple pids.
        }
        //-----------------------------------------------------------------------------------------------------------------
    
    










        //-----------------------------------------------------------------------------------------------------------------
        /// all processes read all elements.
        /// Elements are stored with complete id, gid node list and boundary information.
        /// Each element has gids of the connected nodes in the form of "node_gid_vec_".   
        /// We need this fuunction in data interpolation class 
        template<typename T>        
        void read_all_elements(std::ifstream& infile, std::vector<int>& my_nodes, T& mesh, int nb_gp)
        {
          mesh.elements().reserve(tot_elements_);
          my_nodes.reserve(mesh.tot_nodes());
          
          int count(0);
          std::vector<std::string> result;
    
          for(int i=0; i<mesh.tot_nodes(); i++)     /// skip all lines starting with Node
             skip_one_line(infile);
          
          for(int i=0; i<tot_elements_; i++)  /// start reading elements
          {
            read_one_line(infile, result);          ///for quad element:     Element 0 0 4 3 13 16 12 1
            const int pid = std::stoi(result[2]);                      
            const int gid = std::stoi(result[1]); 
            const int nb_nodes = std::stoi(result[3]); 
      
            auto el =  std::make_shared<element<dim>>(nb_nodes, nb_gp); 
            el->pid(pid);
            el->gid(gid);
            el->lid(count);
                
            std::vector<int> node_gid_vec(nb_nodes);          
            for(int j=0; j<nb_nodes; j++)
            {
              const int nd_gid = std::stoi(result[4+j]);
              node_gid_vec[j] = nd_gid;            
              my_nodes.push_back(nd_gid);
            }
            el->node_gid_vec(std::move(node_gid_vec));            
    
            const int flag = std::stoi(result[4+nb_nodes] );         
            el->on_boundary(flag>0);
            mesh.elements().push_back(std::move(el));
    
            count++;
          }
          
       
          /// Fill "bd_elements_" container  
          for(const auto& el : mesh.elements())
            if(el->on_boundary())
              mesh.bd_elements().push_back(el);    
          
    
          sort_unique(my_nodes);
          /// my_nodes contains a sorted and non repeating list of gids of nodes defined on each pid.
          /// some node gids are shared among multiple pids due to sharing of same node on multiple pids.
        }
        //-----------------------------------------------------------------------------------------------------------------


    
        
        
        
        
       //-----------------------------------------------------------------------------------------------------------------
       /// Reads the sides owned by the process.
       template<typename T>        
       void read_sides(std::ifstream& infile, T& mesh)
       {
          std::vector<std::string> result;
          const int rank = get_rank();
    
          for(int i=0; i<tot_sides_; i++)        /// start reading sides
          {
            read_one_line(infile, result);         ///for Side of quad and tri:      Side 0 2 2 0 4 5
            const int pid = std::stoi(result[2]);
                      
            ///  Only sides of elements on current rank are read.
            if(pid == rank)
            { 
               const int num_side_nds_ = std::stoi(result[3]);

               std::vector<int> side(num_side_nds_);
               for(int j=0; j<num_side_nds_; j++) 
                 side[j] = std::stoi(result[4+j]);

               mesh.sides().push_back(std::move(side));   
                  
               mesh.side_flag().push_back(std::stoi(result[4+num_side_nds_]));
            }
          }                 
        }    
        //-----------------------------------------------------------------------------------------------------------------
                      




    
        
        
        
        
        
        
    
        //-----------------------------------------------------------------------------------------------------------------
        /// Reads the nodes owned by each processor.
        /// reading nodes: complete id, coordinates;  Each processor reads its own nodes defined in my_nodes
        /// Each node of the mesh on each processor knows its lid and gid
        
        template<typename T>        
        void read_nodes(std::ifstream& infile, const std::vector<int>& my_nodes, T& mesh, std::map<int, int>& gid_lid_map)
        {
          mesh.nodes().resize(my_nodes.size());
          int count(0);
          std::vector<std::string> result;
          auto it(my_nodes.begin());
          
          while(it != my_nodes.end()) 
          {
            read_one_line(infile, result);
            if(result[0]!="Node") continue;
            const int gid(stoi(result[1]));
           
            if(*it==gid)
            {
              auto nd = std::make_shared<node<dim>>();        /// nd_pid is set below
              nd->gid(gid);
              nd->lid(count);                          ///count is node_lid on each pid
              gid_lid_map[gid] = count;
           
              nd->set_x(std::stod(result[2]));
              nd->set_y(std::stod(result[3]));
              if(dim == 3)
                nd->set_z(std::stod(result[4]));

              const int flag(std::stoi(result[dim+2]));
              nd->on_boundary(flag>0);          
              nd->flag(flag);
                                   
              mesh.nodes()[count] = std::move(nd);
    
              it++;
              count++;
            }        
          }
          
    
          /// Fill "bd_nodes_" container  
          /// we should reserve the length of   bd_nodes_  vector by some good estimate!!!!
          for(const auto& nd : mesh.nodes())
            if(nd->on_boundary())
              mesh.bd_nodes().push_back(nd);         
              
          
                           
    
          /// Next we set the ownership of each node i.e. to which process it belongs.
          /// Owner is set according to the highest pid number.
          /// If node 11 is shared on pid 3 and 4 then we set owner = 4
          /// We should set it according to the nd pid file generated by Metis
          std::vector<int> nd_gids(mesh.nodes().size());
          count = 0;
          for(const auto& nd : mesh.nodes())
          {
              nd_gids[count] = nd->gid();
              count++;
          }
          
          const int n_nds = mesh.tot_nodes();
          const int rank = get_rank();
          std::vector<int> ownership(n_nds,-1);
          for(auto i : nd_gids)
            ownership[i] = rank;
      
          std::vector<int> ownership_tmp(n_nds);
          MPI_Allreduce(&ownership[0], &ownership_tmp[0], n_nds, MPI_INT, MPI_MAX, MPI_COMM_WORLD);
      
          for(auto& nd : mesh.nodes())
            nd->pid(ownership_tmp[nd->gid()]);                                        
                    
 
 
 
 
          /// Next we collect the host nodes on each pid
          for(const auto& nd : mesh.nodes())
            if(nd->pid() == rank)
              mesh.host_nodes().push_back(nd);         
       }
        //-----------------------------------------------------------------------------------------------------------------
     

    
       
       
    
    
    
      //-----------------------------------------------------------------------------------------------------------------
      /// Define link between each element and its nodes
      template<typename T>
      void set_element_node_links(const std::vector<int>& my_nodes, T& mesh, std::map<int, int>& gid_lid_map)
      {
        for(auto& el : mesh.elements())
        {    
          for(int i=0; i<el->nb_nodes(); i++)
          {
            const int nd_gid = el->node_gid_vec()[i];
            auto nd_lid = gid_lid_map[nd_gid];            
            el->el_node_vec(mesh.nodes()[nd_lid]);                            /// fill the el_node_vec_ for each element with nodes having complete lid and gid
          }
        }  
      }
        //-----------------------------------------------------------------------------------------------------------------
       
        
        
    
      //-----------------------------------------------------------------------------------------------------------------
      /// compute minimum edge length for each element
      template<typename T>
      void compute_el_min_h(T& mesh)
      {
        for(auto& el : mesh.elements())
        {    
          el->compute_min_h();
        }  
      }
      //-----------------------------------------------------------------------------------------------------------------
    


    
        //-----------------------------------------------------------------------------------------------------------------
        ///  This functions reads the info about side_nodes, side_flag etc for each element
        template<typename T>
        void el_side_info(T& mesh)
        {
          for(auto& el : mesh.elements())
          {
            if(el->on_boundary())
            {
              const auto& nds = el->el_node_vec();        
              for(int i=0; i<el->nb_sides(); i++)
              {
                 std::vector<int> side_nds;
                 for(int j=0; j<el->nb_side_nds(i); j++)
                 {
                    side_nds.push_back(nds[el->side(i,j)]->gid());
                 }
                 el->set_side_nodes(std::move(side_nds));                                      
    
                 int sd_flag = 0;
                 int k = 0;
                 for(const auto& j : el->side(i))
                 {
                   if(nds[j]->on_boundary())   
                     k++;
                 }  
                 if(k == el->nb_side_nds(i))
                 {
                    for(std::size_t j=0; j<mesh.sides().size(); j++) 
                      if(are_equal(side_nds, mesh.side(j)))
                        sd_flag = mesh.side_flag(j);             
                 }                                          
                 el->set_side_flag(sd_flag);
    
              
                 bool side_on_bd = false;
                 if(sd_flag>0) side_on_bd = true;
                 el->set_side_on_boundary(side_on_bd);              
              }
            }                
          }           
        }
        //-----------------------------------------------------------------------------------------------------------------
        
        






    /// This function computes the data at quadrature points for each element 
    //----------------------------------------------------------------------------------------------------------------
    template<typename T>
    void compute_el_quad_data(T& mesh)
    {
      point<dim> dummy; 
      
      for(auto& el : mesh.elements())
      {
        for(int i=0; i<el->nb_gp(); i++)
        {
           auto q = std::make_shared<quad>(*el, el->gp(i), el->W_in(i));
           el->quad_i()[i] = std::move(q);
        }        
      }

      for(auto& el : mesh.bd_elements())
      {
          for(int i=0; i<el->nb_sides(); i++)
          {
             std::vector<std::shared_ptr<quad>> v(el->nb_side_gp(i), nullptr);
             if(el->is_side_on_boundary(i))
             {
                for(int j=0; j<el->nb_side_gp(i); j++)
                {
                  auto q = std::make_shared<quad>(*el, el->side_gp(i,j), dummy, el->W_bd(i,j));
                  v[j] = std::move(q);
                }
             }
             el->quad_b()[i] = v;
          }
      }
    }
    //-----------------------------------------------------------------------------------------------------------------
         







    
    
    
    

    /// This function computes the number of quadrature points
    //----------------------------------------------------------------------------------------------------------------
    int get_nb_gp(int nb_nodes)
    {       
       int nb_gp(1);
       
       if(dim==2)
       {
          if(nb_nodes == 4)
          {
             if(setup_.nb_gauss_integration_points()==-1 || setup_.nb_gauss_integration_points()==4)  nb_gp = 4;  // default value
             else if(setup_.nb_gauss_integration_points()==1)  nb_gp = 1;
             else Error("For 2D linear quadrangles, the number of integration points should be 4(default) or 1");           
          }
          else if(nb_nodes == 3)
          {
             if(setup_.nb_gauss_integration_points()==-1 || setup_.nb_gauss_integration_points()==3)  nb_gp = 3;  // default value
             else if(setup_.nb_gauss_integration_points()==1)  nb_gp = 1;
             else Error("For 2D linear triangles, the number of integration points should be 3(default) or 1");
          }
          else
          {
             Error("In 2D, quadrangle el must have 4 nodes and triangle el must have 3 nodes");
          }                
       }
       else if(dim==3)
       {
          if(nb_nodes == 8)
          {
             if(setup_.nb_gauss_integration_points()==-1 || setup_.nb_gauss_integration_points()==8)  nb_gp = 8;  // default value
             else if(setup_.nb_gauss_integration_points()==1)  nb_gp = 1;
             else Error("For 3D linear hexahedron, the number of integration points should be 8(default) or 1");
          }
          else if(nb_nodes == 4)
          {
             if(setup_.nb_gauss_integration_points()==-1 || setup_.nb_gauss_integration_points()==4)  nb_gp = 4;  // default value
             else if(setup_.nb_gauss_integration_points()==1)  nb_gp = 1;
             else Error("For 3D linear tetrahedron, the number of integration points should be 4(default) or 1");
          }
          else
          {
            Error("In 3D, hexa el must have 8 nodes and tetra el must have 4 nodes");
          }      
       }
       else
       {
             Error("1D element is not implemented");         
       }
       return nb_gp;
    }
    //-----------------------------------------------------------------------------------------------------------------


        
        
        
  private:

        int tot_elements_;                                                  /// total number of mesh elements              
        int tot_sides_;                                                     /// total number of boundary sides
        const read_setup& setup_;
  };
    
    



}
//namespace GALES
#endif


