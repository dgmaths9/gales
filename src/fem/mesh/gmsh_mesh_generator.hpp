#ifndef _GALES_GMSH_MESH_GENERATOR_HPP_
#define _GALES_GMSH_MESH_GENERATOR_HPP_



#include <set>
#include <cmath>
#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <chrono>
#include "gmsh.h"
#include <math.h>




namespace GALES {



    

/*
  -----------topography data bounds ------------------- 
               476,000 <= x <= 524,000
   4,152,191.488873656 <= y <= 4,200,191.488873656
   -2259.23 <= z <= 3300.74


  -----------tomography data bounds ------------------- 
               476,000 <= x <= 524,000
   4,152,191.488873656 <= y <= 4,200,191.488873656
                -25000 <= z <= 2000
                
   axis 500144, 4177750                 





  cx and cy are coordinates in UTM system and in km ---> m
  cz is in meters
  
  dx and dz are also in km --> m  
  
  DEM data is w.r.t. sea level
*/  










class gmsh_mesh_generator
{
     using vecpair = std::vector<std::pair<int, int>>;
     using vec_i = std::vector<int>;
     using vec_d = std::vector<double>;


  public:


     /*
         modified input_data.txt contains following in order
          cx cy cz dx dz ty tz 
          
         cx cy dx dz are in m 
         cz is in m
     */
     
               
     
     
     
     void params(int num)
     {
         std::ifstream infile("input/dike_parameters.txt");
         if(infile.fail()) 
           Error("input/dike_parameters.txt is not opened"); 
     
         //jump to the beginning of relavent line n the file    
         infile.seekg(126*num);
     
     
         std::string line;    
         std::getline(infile, line);        
         if(line.size()==0)
                Error("line is empty or line is not present in the file"); 

         std::stringstream ss(line);
         ss >> cx_ >> cy_ >> cz_ >> dx_ >> dz_ >> ty_ >> tz_ >> depth_from_surface_;    
     }
     





     gmsh_mesh_generator(int num, int size)
     {
         params(num);    // read dike parameters
                 


         //------------------------outer box---------------------------------------
         try 
         {
           gmsh::merge("input/etna_surface.stl");
         } 
         catch(...) 
         {
           gmsh::logger::write("Could not load STL mesh: bye!");
           return;
         }
         
         //--------------------outer box -----------------------------------
         gmsh::model::mesh::classifySurfaces(M_PI, true, false, M_PI/3);
             
         vecpair s;
         gmsh::model::getEntities(s, 2); //    print(s) = {(2, 2)}      (dim, surface tag)    
         
         vecpair c;
         gmsh::model::getBoundary(s, c);  //    print(c) = {(1, 3), (1, 4), (1, 5), (1, 6)}   (dim, boundary curve tag) 
         
         if (c.size() != 4)
         {
             gmsh::logger::write("Should have 4 boundary curves!");        
             
             for(auto a :  s)
               std::cout<<"s : "<< a.first<<" "<<a.second<<std::endl<<std::endl;
            
             for(auto a :  c)
               std::cout<<"c : "<< a.first<<" "<<a.second<<std::endl<<std::endl;     
         }
         
         vec_i p;
         vec_d xyz;        
         for(const auto& e : c)
         {
             vecpair pt;
             
             vecpair x = {e};        
             gmsh::model::getBoundary(x, pt, false);                 
             p.push_back(pt[0].second);
             
             vec_d parametricCoord;
             vec_d coord;
             gmsh::model::getValue(0, pt[0].second, parametricCoord, coord);                
             
             for(auto a : coord) 
               xyz.push_back(a);    
         }                
             
        /*
            for example:
            
            e : (1, 3)
            pt : [(0, 1), (0, 2)]
            p : [1]
            xyz : [524000.0, 4152000.0, -1585.619995117188]
            
            e : (1, 4)
            pt : [(0, 2), (0, 3)]
            p : [1, 2]
            xyz : [524000.0, 4152000.0, -1585.619995117188, 474000.0, 4152000.0, 232.7689971923828]
            
            e : (1, 5)
            pt : [(0, 3), (0, 4)]
            p : [1, 2, 3]
            xyz : [524000.0, 4152000.0, -1585.619995117188, 474000.0, 4152000.0, 232.7689971923828, 474000.0, 4200000.0, 1448.826049804688]
            
            e : (1, 6)
            pt : [(0, 4), (0, 1)]
            p : [1, 2, 3, 4]
            xyz : [524000.0, 4152000.0, -1585.619995117188, 474000.0, 4152000.0, 232.7689971923828, 474000.0, 4200000.0, 1448.826049804688, 524000.0, 4200000.0, 464.4190063476562]
            
        */
             
         const double depth = -50000.0; 
         int p1 = gmsh::model::geo::addPoint(xyz[0], xyz[1], depth);   
         int p2 = gmsh::model::geo::addPoint(xyz[3], xyz[4], depth);
         int p3 = gmsh::model::geo::addPoint(xyz[6], xyz[7], depth);
         int p4 = gmsh::model::geo::addPoint(xyz[9], xyz[10], depth);      
         
         int c1 = gmsh::model::geo::addLine(p1, p2);
         int c2 = gmsh::model::geo::addLine(p2, p3);
         int c3 = gmsh::model::geo::addLine(p3, p4);
         int c4 = gmsh::model::geo::addLine(p4, p1);
                      
         int c5 = gmsh::model::geo::addLine(p1, p[0]);
         int c6 = gmsh::model::geo::addLine(p2, p[1]);
         int c7 = gmsh::model::geo::addLine(p3, p[2]);
         int c8 = gmsh::model::geo::addLine(p4, p[3]);
         
         int c_01 = c[0].second;
         int c_02 = c[1].second;
         int c_03 = c[2].second;
         int c_04 = c[3].second;
             
         auto ll1 = gmsh::model::geo::addCurveLoop(vec_i({c1, c2, c3, c4}));      
         int s1 = gmsh::model::geo::addPlaneSurface(vec_i({ll1}));      // bottom surface
         
         auto ll2 = gmsh::model::geo::addCurveLoop(vec_i({c1, c6, -c_01, -c5}));
         int s2 = gmsh::model::geo::addPlaneSurface(vec_i({ll2}));              // front surface
     
         auto ll3 = gmsh::model::geo::addCurveLoop(vec_i({c2, c7, -c_02, -c6}));
         int s3 = gmsh::model::geo::addPlaneSurface(vec_i({ll3}));             // right surface 
     
         auto ll4 = gmsh::model::geo::addCurveLoop(vec_i({c3, c8, -c_03, -c7}));
         int s4 = gmsh::model::geo::addPlaneSurface(vec_i({ll4}));              // back surface
     
         auto ll5 = gmsh::model::geo::addCurveLoop(vec_i({c4, c5, -c_04, -c8}));
         int s5 = gmsh::model::geo::addPlaneSurface(vec_i({ll5}));              // left surface 
     
         int ob_top = s[0].second;     // top surface with topography
         
         int sl1 = gmsh::model::geo::addSurfaceLoop(vec_i({s1, s2, s3, s4, s5, ob_top}));








         //------------------------dike---------------------------------------
         const double dy = 2.0;
     
         auto p11 = gmsh::model::geo::addPoint(cx_, cy_, cz_);
         auto p12 = gmsh::model::geo::addPoint(cx_+dx_, cy_, cz_);
         auto p13 = gmsh::model::geo::addPoint(cx_+dx_, cy_+dy, cz_);
         auto p14 = gmsh::model::geo::addPoint(cx_, cy_+dy, cz_);
         auto p15 = gmsh::model::geo::addPoint(cx_, cy_, cz_-dz_);
         auto p16 = gmsh::model::geo::addPoint(cx_+dx_, cy_, cz_-dz_);
         auto p17 = gmsh::model::geo::addPoint(cx_+dx_, cy_+dy, cz_-dz_);
         auto p18 = gmsh::model::geo::addPoint(cx_, cy_+dy, cz_-dz_);
     
         int c11 = gmsh::model::geo::addLine(p11, p12);
         int c12 = gmsh::model::geo::addLine(p12, p13);
         int c13 = gmsh::model::geo::addLine(p13, p14);
         int c14 = gmsh::model::geo::addLine(p14, p11);
         int c15 = gmsh::model::geo::addLine(p11, p15);        
         int c16 = gmsh::model::geo::addLine(p15, p18);
         int c17 = gmsh::model::geo::addLine(p18, p14);
         int c18 = gmsh::model::geo::addLine(p12, p16);
         int c19 = gmsh::model::geo::addLine(p16, p17);
         int c20 = gmsh::model::geo::addLine(p17, p13);
         int c21 = gmsh::model::geo::addLine(p17, p18);
         int c22 = gmsh::model::geo::addLine(p15, p16);
         
         auto ll11 = gmsh::model::geo::addCurveLoop(vec_i({c13, c14, c11, c12}));      
         int s11 = gmsh::model::geo::addPlaneSurface(vec_i({ll11}));               
             
         auto ll12 = gmsh::model::geo::addCurveLoop(vec_i({c17, c14, c15, c16}));          
         int s12 = gmsh::model::geo::addPlaneSurface(vec_i({ll12})); 
     
         auto ll13 = gmsh::model::geo::addCurveLoop(vec_i({c22, c19, c21, -c16}));      
         int s13 = gmsh::model::geo::addPlaneSurface(vec_i({ll13})); 
     
         auto ll14 = gmsh::model::geo::addCurveLoop(vec_i({c19, c20, -c12, c18}));      
         int s14 = gmsh::model::geo::addPlaneSurface(vec_i({ll14})); 
     
         auto ll15 = gmsh::model::geo::addCurveLoop(vec_i({c20, c13, -c17, -c21}));      
         int s15 = gmsh::model::geo::addPlaneSurface(vec_i({ll15})); 
     
         auto ll16 = gmsh::model::geo::addCurveLoop(vec_i({c22, -c18, -c11, c15}));      
         int s16 = gmsh::model::geo::addPlaneSurface(vec_i({ll16}));
                  
         vecpair vec{std::make_pair(2,s11), std::make_pair(2,s12), 
                     std::make_pair(2,s13), std::make_pair(2,s14), 
                     std::make_pair(2,s15), std::make_pair(2,s16)}; 
                                                                                                                         
         /*-------azimuth--------------------------*/
         if(ty_ != 0.0)
         {
            if(ty_ >= 180)
            {                                           
                gmsh::model::geo::rotate(vec,  cx_,cy_,cz_,  0,0,1, 180); // ty rotation in xy plane   from y+ ---x+ (clockwise N-->E)  
                gmsh::model::geo::rotate(vec,  cx_,cy_,cz_,  0,0,1, 360-ty_); // ty rotation in xy plane  from y+ ---x+ (clockwise N-->E) 
            } 
            else
            {
                gmsh::model::geo::rotate(vec,  cx_,cy_,cz_,  0,0,1, ty_); // ty rotation in xy plane from y+ ---x+ (clockwise N-->E)     
            }         
         }
         /*-----------dip--------------------*/    
         if(tz_ != 0.0)
            gmsh::model::geo::rotate(vec,  cx_,cy_,cz_,  1,0,0, -tz_); // tz rotation in yz plane 
         
         
         int sl2 = gmsh::model::geo::addSurfaceLoop(vec_i({s12, s13, s11, s16, s14, s15}));






         gmsh::option::setNumber("Mesh.Algorithm3D", 10);    
         gmsh::option::setNumber("Mesh.MshFileVersion", 4.1);
         
         int v = gmsh::model::geo::addVolume(vec_i({sl1, sl2}));   
         
         int nx, ny{4}, nz;
         
         nx = int(dx_/15)+1;   
         nz = int(dz_/15)+1;  
         
         if(dx_ > 1000.0) nx = 71;
         if(dz_ > 1000.0) nz = 71;
         
         if(dx_ < 100.0) nx = 21;
         if(dz_ < 100.0) nz = 21;


         gmsh::model::geo::mesh::setTransfiniteCurve(c22, nx);
         gmsh::model::geo::mesh::setTransfiniteCurve(c11, nx);
         gmsh::model::geo::mesh::setTransfiniteCurve(c21, nx);
         gmsh::model::geo::mesh::setTransfiniteCurve(c13, nx);
         
         gmsh::model::geo::mesh::setTransfiniteCurve(c12, ny);
         gmsh::model::geo::mesh::setTransfiniteCurve(c14, ny);
         gmsh::model::geo::mesh::setTransfiniteCurve(c16, ny);
         gmsh::model::geo::mesh::setTransfiniteCurve(c19, ny);
         
         gmsh::model::geo::mesh::setTransfiniteCurve(c15, nz);
         gmsh::model::geo::mesh::setTransfiniteCurve(c18, nz);
         gmsh::model::geo::mesh::setTransfiniteCurve(c17, nz);
         gmsh::model::geo::mesh::setTransfiniteCurve(c20, nz);

         gmsh::model::geo::mesh::setTransfiniteSurface(s11);
         gmsh::model::geo::mesh::setTransfiniteSurface(s12);
         gmsh::model::geo::mesh::setTransfiniteSurface(s13);
         gmsh::model::geo::mesh::setTransfiniteSurface(s14);
         gmsh::model::geo::mesh::setTransfiniteSurface(s15);
         gmsh::model::geo::mesh::setTransfiniteSurface(s16);
          
         gmsh::model::addPhysicalGroup(3, vec_i({v}), 0);  // volume tag = 0
         
         gmsh::model::addPhysicalGroup(2, vec_i({s1, s2, s3, s4, s5}), 5);   // lateral boundaries tag = 5
         gmsh::model::addPhysicalGroup(2, vec_i({s11, s12, s13, s14, s15, s16}), 4);   // dike tag = 4    
         gmsh::model::addPhysicalGroup(2, vec_i({ob_top}), 2);   // top surface boundaries tag = 2
         
         gmsh::model::addPhysicalGroup(1, vec_i({c5, c6, c7, c8, c_01, c_02, c_03, c_04}), 5);   // lateral boundaries tag = 5
 
         gmsh::model::addPhysicalGroup(0, vec_i({p[0], p[1], p[2], p[3]}), 5);   // lateral boundaries tag = 5
         
     
         gmsh::model::geo::synchronize();
         gmsh::model::mesh::generate(3);         
         gmsh::model::mesh::partition(size);
         
//         nodes();
//         elements();

         std::string mesh_name = "mesh/" + std::to_string(num) + ".msh";
         gmsh::write(mesh_name);  
//         gmsh::fltk::run();                
     }








     
     
     void nodes()
     {
        std::vector<double> parametricCoord;
        gmsh::model::mesh::getNodes(nd_Tags_, nd_coords_, parametricCoord, -1, -1, false, false);        
     }
     
     
     auto get_nd_Tags() const {return nd_Tags_;}
     auto get_nd_coords() const {return nd_coords_;}
     





     void elements_for_physicalTags(const std::vector<int>& physicalTags, int dim, int el_type, std::vector<int>& num_els, std::vector<std::vector<int>>& nds)
     {
        num_els.resize(physicalTags.size());
        nds.resize(physicalTags.size());
        for(int i=0; i<physicalTags.size(); i++)
        {            
            std::vector<int> entityTags;
            gmsh::model::getEntitiesForPhysicalGroup(dim, physicalTags[i], entityTags);
            
            int n = 0;            
            std::vector<int> nds_tags;      
            for(auto b : entityTags)
            {
              std::vector<std::size_t> elementTags;
              std::vector<std::size_t> nodeTags;
              gmsh::model::mesh::getElementsByType(el_type, elementTags, nodeTags, b);
              n += elementTags.size();
              nds_tags.insert(std::end(nds_tags), std::begin(nodeTags), std::end(nodeTags));
            }
            num_els[i] = n;
            nds[i] = nds_tags;
        }            
     }


     
     void elements()
     {
            elements_for_physicalTags(physicalTags_tet_, 3, 4,  num_tet_, tet_nds_);
            elements_for_physicalTags(physicalTags_tri_, 2, 2,  num_tri_, tri_nds_);
            elements_for_physicalTags(physicalTags_line_, 1, 1,  num_line_, line_nds_);
            elements_for_physicalTags(physicalTags_pt_, 0, 15,  num_pt_, pt_nds_);
     }


     
     auto get_physicalTags_tet() const {return physicalTags_tet_;}
     auto get_num_tet() const {return num_tet_;}
     auto get_tet_nds() const {return tet_nds_;}
     
     auto get_physicalTags_tri() const {return physicalTags_tri_;}
     auto get_num_tri() const {return num_tri_;}
     auto get_tri_nds() const {return tri_nds_;}

     auto get_physicalTags_line() const {return physicalTags_line_;}
     auto get_num_line() const {return num_line_;}
     auto get_line_nds() const {return line_nds_;}

     auto get_physicalTags_pt() const {return physicalTags_pt_;}
     auto get_num_pt() const {return num_pt_;}
     auto get_pt_nds() const {return pt_nds_;}
     


     
     
     private:
     
     /*---------dike input parameters----------*/
     double cx_, cy_, cz_, dx_, dz_, ty_, tz_, depth_from_surface_;

               
     
     /*---------nodes--------------*/
     std::vector<std::size_t> nd_Tags_;
     std::vector<double> nd_coords_;  // [n1x, n1y, n1z, n2x, ...]
     
     /*---------elements--------------*/     
     std::vector<int> physicalTags_tet_{0};
     std::vector<int> num_tet_; 
     std::vector<std::vector<int>> tet_nds_;      
     
     std::vector<int> physicalTags_tri_{2,4,5};
     std::vector<int> num_tri_; 
     std::vector<std::vector<int>> tri_nds_;      

     std::vector<int> physicalTags_line_{5};
     std::vector<int> num_line_; 
     std::vector<std::vector<int>> line_nds_;      

     std::vector<int> physicalTags_pt_{5};
     std::vector<int> num_pt_; 
     std::vector<std::vector<int>> pt_nds_;      
};


}//namespace GALES
#endif
