#ifndef _GALES_ELEMENT_HPP_
#define _GALES_ELEMENT_HPP_

#include <vector>
#include "node.hpp"
#include "numerical_integration.hpp"



namespace GALES {


/**
  A generic mesh element.
  This class implements a generic mesh elements: it can be composed by any number of nodes; also, it can have any number of adjacent elements.
*/


  //-----------------------------------------------------------------------------------------------------------------------   
  //  this is forward decleration of element class
  class quad;    
  //-----------------------------------------------------------------------------------------------------------------------







  //-----------------------------------------------------------------------------------------------------------------------   
  // This is generic template for node<dim>
  template<int dim>
  class element: public numerical_integration<dim>{};
  //-----------------------------------------------------------------------------------------------------------------------





  //-----------------------------------------------------------------------------------------------------------------------
  // This class is tempate specialized of node<dim> with dim = 2 
  template<>
  class element<2>: public numerical_integration<2>
  {
    public:

    element(int nb_nodes, int nb_gp): nb_nodes_(nb_nodes), numerical_integration<2>(nb_nodes, nb_gp)
    {
      for(int i=0; i<nb_gp; i++)
        quad_i_.push_back(nullptr);

      for(int i=0; i<nb_sides(); i++)
      {
        std::vector<std::shared_ptr<quad>> v(nb_side_gp(i), nullptr);
        quad_b_.push_back(v);
      }         
    }
    
    
    //-------------------------------------------------------------------------------------------------------------------------------------        
    /// Deleting the copy and move constructors - no duplication/transfer in anyway
    element(const element&) = delete;               //copy constructor
    element& operator=(const element&) = delete;    //copy assignment operator
    element(element&&) = delete;                    //move constructor  
    element& operator=(element&&) = delete;         //move assignment operator 
    //-------------------------------------------------------------------------------------------------------------------------------------







  //-----------------------------------------------------------------------------------------------------------------------   
    void compute_bounds()  // this function computes the element bounds [min_x, max_x, min_y, max_y]
    {
       std::vector<double> x, y;
       x.reserve(nb_nodes_);
       y.reserve(nb_nodes_);
       for(const auto& nd : el_node_vec_)
       {
         x.push_back(nd->get_x());
         y.push_back(nd->get_y());
       }  
       auto minmax_x = std::minmax_element(x.begin(), x.end());
       auto minmax_y = std::minmax_element(y.begin(), y.end());
       
       min_x_ = *minmax_x.first;       max_x_ = *minmax_x.second;       
       min_y_ = *minmax_y.first;       max_y_ = *minmax_y.second;

       cx_ = 0.0;
       cy_ = 0.0;
       for(int i=0; i<nb_nodes_; i++)
       {
          cx_ += x[i];
          cy_ += y[i];
       } 
       cx_ /= nb_nodes_;
       cy_ /= nb_nodes_;
    }
  //-----------------------------------------------------------------------------------------------------------------------   




    double min_x()const {return min_x_;}
    double max_x()const {return max_x_;}
    double min_y()const {return min_y_;}
    double max_y()const {return max_y_;}    
    auto bounds()const{ return std::vector<double>{min_x_,max_x_,min_y_,max_y_};}

    double cx()const {return cx_;}
    double cy()const {return cy_;}
    auto centre()const{ return std::vector<double>{cx_,cy_};}
       
                
    void pid(int i){pid_=i;}                                                                  /// set pid of the el
    int pid()const{return pid_;}                                                              /// get pid of the el  
    
    void gid(int i){gid_=i;}                                                                  /// set gid of the el
    int gid()const{return gid_;}                                                              /// get gid of the el

    void lid(int i){lid_=i;}                                                                  /// set lid of the el
    int lid()const{return lid_;}                                                              /// get lid of the el
  
    void node_gid_vec(const std::vector<int>& node_gid_vec) { node_gid_vec_ = std::move(node_gid_vec);}                                /// set vector of node gids 
    auto node_gid_vec()const { return node_gid_vec_; }                                                                                 /// get vector of node gids

    int nb_nodes()const { return nb_nodes_; }                                                 /// get the number of nodes of the element
    int nb_nd_dofs()const { return el_node_vec_[0]->nb_dofs(); }                              /// get number of node dofs (for a single node)
    int nb_dofs()const { return nb_nodes_* (el_node_vec_[0]->nb_dofs()); }                    /// get number of node dofs (for a single node)
      
    void on_boundary(bool a) { on_boundary_ = a; }                                            /// set the flag if el is on boundary or not
    bool on_boundary()const { return on_boundary_; }                                          /// get the flag if el is on boundary or not                       

    void el_flag(int flag) { el_flag_ = flag; }                                            /// set the flag number of el 
    int el_flag()const { return el_flag_; }                                                /// get the flag number of el                        



    void set_side_nodes(const std::vector<int>& v) {side_nodes_.push_back(v);}                /// set nodes for a side of the element
    auto side_nodes(int sd_idx)const {return side_nodes_[sd_idx];}                            /// get nodes for a(sd_idx) side of the element
    auto side_nodes(int sd_idx, int nd_idx)const {return side_nodes_[sd_idx][nd_idx];}        /// get node for a(sd_idx and nd_indx) side of the element

    void set_side_flag(int side_flag) {side_flag_.push_back(side_flag);}                      /// set side flag for a side of the element
    int side_flag(int sd_idx)const {return side_flag_[sd_idx];}                               /// get side flag for a(sd_idx) side of the element
    
    void set_side_on_boundary(bool flag) {is_side_on_boundary_.push_back(flag);}              /// set vector of side flags that tells which sides are on boundary
    bool is_side_on_boundary(int sd_idx)const {return is_side_on_boundary_[sd_idx];}          /// get a flag that tells if a(sd_idx) side is on boundary or not
             



    void el_node_vec(const std::shared_ptr<node<2>>& nd) { el_node_vec_.push_back(nd);}       /// set vector of nodes
    auto el_node_vec() const {return el_node_vec_;}                                           /// get vector of nodes 
    auto el_node_vec(int i) const {return el_node_vec_[i];}                                   /// get ith node 

    int nd_gid(int nd_idx) const{return el_node_vec_[nd_idx]->gid();}                                   /// get gid of ith node 
    int nd_lid(int nd_idx) const{return el_node_vec_[nd_idx]->lid();}                                   /// get lid of ith node                

    int nd_gid(int sd_idx, int nd_idx) const{return el_node_vec_[side_[sd_idx][nd_idx]]->gid();}                  /// get gid of jth node of ith side
    int nd_lid(int sd_idx, int nd_idx) const{return el_node_vec_[side_[sd_idx][nd_idx]]->lid();}                  /// get lid of jth node of ith side

    void set_x(int nd_idx, double x) {el_node_vec_[nd_idx]->set_x(x);}                    /// set x-coord of the node
    void set_y(int nd_idx, double y) {el_node_vec_[nd_idx]->set_y(y);}                    /// set y-coord of the node

    double get_x(int nd_idx) const {return el_node_vec_[nd_idx]->get_x();}                /// get x-coord of the node
    double get_y(int nd_idx) const {return el_node_vec_[nd_idx]->get_y();}                /// get y-coord of the node
    
    
    
    
    void set_x_p(int nd_idx, double x) {el_node_vec_[nd_idx]->set_x_p(x);}                    /// set x-coord of the node before mesh updation
    void set_y_p(int nd_idx, double y) {el_node_vec_[nd_idx]->set_y_p(y);}                    /// set y-coord of the node before mesh updation

    double get_x_p(int nd_idx) const {return el_node_vec_[nd_idx]->get_x_p();}                /// get x-coord of the node before mesh updation
    double get_y_p(int nd_idx) const {return el_node_vec_[nd_idx]->get_y_p();}                /// get y-coord of the node before mesh updation
             
    
    
    double get_side_x(int sd_idx, int nd_indx)const{return el_node_vec_[side_[sd_idx][nd_indx]]->get_x();}
    double get_side_y(int sd_idx, int nd_indx)const{return el_node_vec_[side_[sd_idx][nd_indx]]->get_y();}
    
    
    
    double get_side_mid_x(int sd_idx)const 
    { 
       double x = 0.0;
       for(int i=0; i<nb_side_nds(sd_idx); i++)
         x += el_node_vec_[side_[sd_idx][i]]->get_x();
       return x/nb_side_nds(sd_idx);
    }
    
    double get_side_mid_y(int sd_idx)const 
    { 
       double y = 0.0;
       for(int i=0; i<nb_side_nds(sd_idx); i++)
         y += el_node_vec_[side_[sd_idx][i]]->get_y();
       return y/nb_side_nds(sd_idx);
    }
    
    
    
    
    bool is_inside(const point<2>& pt)
    {
       point<2> local_pt;
       return is_inside(pt, local_pt);
    }
    
    
    
    // check if point pt is inside this element
    bool is_inside(const point<2>& pt, point<2>& local_pt)
    {       
       // we first use element mapping to find the coordinates of the point in parametric domain
       // and then test if it belongs to the parametric element

       bool pt_is_inside = false;       
       double tol(std::numeric_limits<double>::epsilon());
       if(nb_nodes_== 3)
       {
         double x0 = get_x(0),  x1 = get_x(1),  x2 = get_x(2); 
         double y0 = get_y(0),  y1 = get_y(1),  y2 = get_y(2); 

         double a1 = x1-x0,   b1 = x2-x0,   d1 = pt.get_x()-x0;
         double a2 = y1-y0,   b2 = y2-y0,   d2 = pt.get_y()-y0;
         
         double D = a1*b2 + b1*a2;
         double D1 = d1*b2 - b1*d2;
         double D2 = a1*d2 - d1*a2;
         
         double xi = D1/D;  
         double eta = D2/D;  
                                
         local_pt.set_x(xi);
         local_pt.set_y(eta);
                                
         if(xi >= 0.0-tol && xi <= 1.0+tol)
           if(eta >= 0.0-tol && eta <= 1.0+tol)
             if(xi+eta <= 1.0+tol)
               pt_is_inside = true;         
       }

       else if(nb_nodes_== 4)
       {
         double x0 = get_x(0),  x1 = get_x(1),  x2 = get_x(2),  x3 = get_x(3); 
         double y0 = get_y(0),  y1 = get_y(1),  y2 = get_y(2),  y3 = get_y(3); 

         double a1 = -x0 + x1 + x2 - x3;
         double b1 = -x0 - x1 + x2 + x3;
         double c1 = x0 - x1 + x2 - x3;
         double d1 = 4*pt.get_x() - (x0 + x1 + x2 + x3);

         double a2 = -y0 + y1 + y2 - y3;
         double b2 = -y0 - y1 + y2 + y3;
         double c2 = y0 - y1 + y2 - y3;
         double d2 = 4*pt.get_y() - (y0 + y1 + y2 + y3);

         double a = -b2*c1 - c2*b1;
         double b = -b1*a2 + a1*b2 + c2*d1 + c1*d2;
         double c = a2*d1 - d2*a1;
         
         double eta = (-b + sqrt(b*b - 4*a*c))/(2*a);
         double xi = (d1 - b1*eta)/(a1-c1*eta);

         local_pt.set_x(xi);
         local_pt.set_y(eta);

         if(xi >= -1.0-tol && xi <= 1.0+tol)
           if(eta >= -1.0-tol && eta <= 1.0+tol)
             pt_is_inside = true;         


//          std::vector<point<2>> pt_vec(4);
//          for(int i=0: i<4; i++)
//            pt_vec[i] = el_node_vec_[i]->get_pt();
//          pt_is_inside = point_in_polygon(pt, pt_vec);
       }
        
       return pt_is_inside;  
    }
    
    
    
    
    auto& quad_i() { return quad_i_; }                        
    const auto& quad_i()const { return quad_i_; }                    

    auto& quad_i(int gp_idx) { return quad_i_[gp_idx]; }                        
    const auto& quad_i(int gp_idx)const { return quad_i_[gp_idx]; }                    

    auto& quad_b(){return quad_b_;}
    const auto& quad_b()const {return quad_b_;} 
           
    auto& quad_b(int side_idx){return quad_b_[side_idx];}    
    const auto& quad_b(int side_idx)const{return quad_b_[side_idx];} 
       
    auto& quad_b(int side_idx, int gp_idx){return quad_b_[side_idx][gp_idx];}    
    const auto& quad_b(int side_idx, int gp_idx)const{return quad_b_[side_idx][gp_idx];}    
    
    
    
    

    void compute_dofs_gids()
    {
      int nb_nd_dofs = el_node_vec_[0]->nb_dofs();
      dofs_gids_.reserve(nb_nodes_ * nb_nd_dofs);
      clear(dofs_gids_);
      
      for (int i=0; i<nb_nodes_; i++) 
	for(int j=0; j<nb_nd_dofs; j++)	
	  dofs_gids_.push_back(el_node_vec_[i]->first_dof_gid() + j);
    }

    auto& dofs_gids()const {return dofs_gids_;}






    template<typename dofs_ic_bc_type>
    auto dofs_constraints(dofs_ic_bc_type& dofs_ic_bc)
    {
      int nb_nd_dofs = el_node_vec_[0]->nb_dofs();
      std::vector<std::pair<bool,double>> el_dofs_constraints;   /// vector of dofs constraints with values of the element
      el_dofs_constraints.reserve(nb_nodes_ * nb_nd_dofs);            

      for (int i=0; i<nb_nodes_; i++) 
      {
        auto nd_dofs_constraints = dofs_ic_bc.dofs_constraints(*(el_node_vec_[i]));
        el_dofs_constraints.insert(std::end(el_dofs_constraints), std::begin(nd_dofs_constraints), std::end(nd_dofs_constraints));
      }
      
      return el_dofs_constraints;
    }




    void compute_min_h()
    {
       if(nb_nodes_== 3)
       {
         double x0 = get_x(0),  x1 = get_x(1),  x2 = get_x(2); 
         double y0 = get_y(0),  y1 = get_y(1),  y2 = get_y(2); 
         double l1 = std::sqrt((x1 - x0)*(x1 - x0) + (y1 - y0)*(y1 - y0));  
         double l2 = std::sqrt((x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1));  
         double l3 = std::sqrt((x0 - x2)*(x0 - x2) + (y0 - y2)*(y0 - y2));
         min_h_ = std::min({l1, l2, l3});  
       }   

       else if(nb_nodes_== 4)
       {
         double x0 = get_x(0),  x1 = get_x(1),  x2 = get_x(2),  x3 = get_x(3); 
         double y0 = get_y(0),  y1 = get_y(1),  y2 = get_y(2),  y3 = get_y(3); 
         double l1 = std::sqrt((x1 - x0)*(x1 - x0) + (y1 - y0)*(y1 - y0));  
         double l2 = std::sqrt((x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1));  
         double l3 = std::sqrt((x3 - x2)*(x3 - x2) + (y3 - y2)*(y3 - y2));
         double l4 = std::sqrt((x0 - x3)*(x0 - x3) + (y0 - y3)*(y0 - y3));
         min_h_ = std::min({l1, l2, l3, l4});  
       }       
    }    

    
    double min_h() const {return min_h_;}    
   
   
   

  private:

    int pid_;                                              /// process id of the item
    int gid_;                                              /// global id of the item among all processes
    int lid_;                                              /// local id of the item on the owner process
    bool on_boundary_;                                     /// flag: if el is on bundary
    int el_flag_;                                          /// flag number of the element
    int nb_nodes_;                                         /// number of nodes with which el is composed 
    std::vector<int> node_gid_vec_;                        /// vector of node gids belong to the element
    std::vector<std::vector<int>> side_nodes_;             /// vector of node_gids for each side of the el
    std::vector<int> side_flag_;                           /// vector of side_flag for each side of the el
    std::vector<bool> is_side_on_boundary_;                /// vector of flag(if side is on bd) for each side of the el
    std::vector<std::shared_ptr<node<2>>> el_node_vec_;    /// vector of nodes belong to the element
    std::vector<int> dofs_gids_;                           /// vector of dofs gids of the element     
    double min_x_, max_x_, min_y_, max_y_;
    double cx_, cy_;                                       /// centroid of element
    double min_h_;                                         /// minimum edge length of the element

    std::vector<std::shared_ptr<quad>> quad_i_;                            ///quad data computed at each gp for interior integration  
    std::vector<std::vector<std::shared_ptr<quad>>> quad_b_;               ///quad data computed at each gp for boundary integration      
  };
  //-----------------------------------------------------------------------------------------------------------------------



   












  //-----------------------------------------------------------------------------------------------------------------------
  // This class is tempate specialized of node<dim> with dim = 3 
  template<>
  class element<3>: public numerical_integration<3>
  {
    public:
                
    element(int nb_nodes, int nb_gp): nb_nodes_(nb_nodes), numerical_integration<3>(nb_nodes, nb_gp)
    {
      for(int i=0; i<nb_gp; i++)
        quad_i_.push_back(nullptr);

      for(int i=0; i<nb_sides(); i++)
      {
        std::vector<std::shared_ptr<quad>> v(nb_side_gp(i), nullptr);
        quad_b_.push_back(v);
      }  
    }
    
    
    //-------------------------------------------------------------------------------------------------------------------------------------        
    /// Deleting the copy and move constructors - no duplication/transfer in anyway
    element(const element&) = delete;               //copy constructor
    element& operator=(const element&) = delete;    //copy assignment operator
    element(element&&) = delete;                    //move constructor  
    element& operator=(element&&) = delete;         //move assignment operator 
    //-------------------------------------------------------------------------------------------------------------------------------------









    //-------------------------------------------------------------------------------------------------------------------------------------        
    void compute_bounds()  // this function computes the element bounds [min_x, max_x, min_y, max_y, min_z, max_z]
    {
       std::vector<double> x, y, z;
       x.reserve(nb_nodes_);
       y.reserve(nb_nodes_);
       z.reserve(nb_nodes_);
       for(const auto& nd : el_node_vec_)
       {
         x.push_back(nd->get_x());
         y.push_back(nd->get_y());
         z.push_back(nd->get_z());
       }  
       auto minmax_x = std::minmax_element(x.begin(), x.end());
       auto minmax_y = std::minmax_element(y.begin(), y.end());
       auto minmax_z = std::minmax_element(z.begin(), z.end());
       
       min_x_ = *minmax_x.first;       max_x_ = *minmax_x.second;       
       min_y_ = *minmax_y.first;       max_y_ = *minmax_y.second;
       min_z_ = *minmax_z.first;       max_z_ = *minmax_z.second;  
       
       cx_ = 0.0;
       cy_ = 0.0;
       cz_ = 0.0;
       for(int i=0; i<nb_nodes_; i++)
       {
          cx_ += x[i];
          cy_ += y[i];
          cz_ += z[i];
       } 
       cx_ /= nb_nodes_;
       cy_ /= nb_nodes_;
       cz_ /= nb_nodes_;
    }
    //-------------------------------------------------------------------------------------------------------------------------------------        




    double min_x()const {return min_x_;}
    double max_x()const {return max_x_;}
    double min_y()const {return min_y_;}
    double max_y()const {return max_y_;}
    double min_z()const {return min_z_;}
    double max_z()const {return max_z_;}
    auto bounds()const{ return std::vector<double>{min_x_,max_x_,min_y_,max_y_,min_z_,max_z_};}

    double cx()const {return cx_;}
    double cy()const {return cy_;}
    double cz()const {return cz_;}
    auto centre()const{ return std::vector<double>{cx_,cy_,cz_};}

    
    void pid(int i){pid_=i;}                                                                  /// set pid of the el
    int pid()const{return pid_;}                                                              /// get pid of the el  
    
    void gid(int i){gid_=i;}                                                                  /// set gid of the el
    int gid()const{return gid_;}                                                              /// get gid of the el

    void lid(int i){lid_=i;}                                                                  /// set lid of the el
    int lid()const{return lid_;}                                                              /// get lid of the el
  
    void node_gid_vec(const std::vector<int>& node_gid_vec) { node_gid_vec_ = std::move(node_gid_vec);}                                /// set vector of node gids 
    auto node_gid_vec()const { return node_gid_vec_; }                                                                                 /// get vector of node gids

    int nb_nodes()const { return nb_nodes_; }                                                 /// get the number of nodes of the element
    int nb_nd_dofs()const { return el_node_vec_[0]->nb_dofs(); }                              /// get number of node dofs (for a single node)
    int nb_dofs()const { return nb_nodes_* (el_node_vec_[0]->nb_dofs()); }                              /// get number of node dofs (for a single node)
    
    void on_boundary(bool a) { on_boundary_ = a; }                                            /// set the flag if el is on boundary or not
    bool on_boundary()const { return on_boundary_; }                                          /// get the flag if el is on boundary or not                       

    void el_flag(int flag) { el_flag_ = flag; }                                            /// set the flag number of el 
    int el_flag()const { return el_flag_; }                                                /// get the flag number of el                        
    


    void set_side_nodes(const std::vector<int>& v) {side_nodes_.push_back(v);}                /// set nodes for a side of the element
    auto side_nodes(int sd_idx)const {return side_nodes_[sd_idx];}                            /// get nodes for a(sd_idx) side of the element
    auto side_nodes(int sd_idx, int nd_idx)const {return side_nodes_[sd_idx][nd_idx];}        /// get node for a(sd_idx and nd_indx) side of the element

    void set_side_flag(int side_flag) {side_flag_.push_back(side_flag);}                      /// set side flag for a side of the element
    int side_flag(int sd_idx)const {return side_flag_[sd_idx];}                               /// get side flag for a(sd_idx) side of the element
    
    void set_side_on_boundary(bool flag) {is_side_on_boundary_.push_back(flag);}              /// set vector of side flags that tells which sides are on boundary
    bool is_side_on_boundary(int sd_idx)const {return is_side_on_boundary_[sd_idx];}          /// get a flag that tells if a(sd_idx) side is on boundary or not
             


    void el_node_vec(const std::shared_ptr<node<3>>& nd) { el_node_vec_.push_back(nd);}       /// set vector of nodes
    auto el_node_vec() const {return el_node_vec_;}                                           /// get vector of nodes 
    auto el_node_vec(int nd_idx) const {return el_node_vec_[nd_idx];}                                   /// get ith node 

    int nd_gid(int nd_idx) const{return el_node_vec_[nd_idx]->gid();}                                   /// get gid of ith node 
    int nd_lid(int nd_idx) const{return el_node_vec_[nd_idx]->lid();}                                   /// get lid of ith node                
    
    int nd_gid(int sd_idx, int nd_idx) const{return el_node_vec_[side_[sd_idx][nd_idx]]->gid();}                  /// get gid of jth node of ith side
    int nd_lid(int sd_idx, int nd_idx) const{return el_node_vec_[side_[sd_idx][nd_idx]]->lid();}                  /// get lid of jth node of ith side

    void set_x(int nd_idx, double x) {el_node_vec_[nd_idx]->set_x(x);}                    /// set x-coord of the node
    void set_y(int nd_idx, double y) {el_node_vec_[nd_idx]->set_y(y);}                    /// set y-coord of the node
    void set_z(int nd_idx, double z) {el_node_vec_[nd_idx]->set_z(z);}                    /// set z-coord of the node

    double get_x(int nd_idx) const {return el_node_vec_[nd_idx]->get_x();}                /// get x-coord of the node
    double get_y(int nd_idx) const {return el_node_vec_[nd_idx]->get_y();}                /// get y-coord of the node
    double get_z(int nd_idx) const {return el_node_vec_[nd_idx]->get_z();}                /// get z-coord of the node
        
        
        
    
    void set_x_p(int nd_idx, double x) {el_node_vec_[nd_idx]->set_x_p(x);}                    /// set x-coord of the node before mesh updation
    void set_y_p(int nd_idx, double y) {el_node_vec_[nd_idx]->set_y_p(y);}                    /// set y-coord of the node before mesh updation
    void set_z_p(int nd_idx, double z) {el_node_vec_[nd_idx]->set_z_p(z);}                    /// set z-coord of the node before mesh updation

    double get_x_p(int nd_idx) const {return el_node_vec_[nd_idx]->get_x_p();}                /// get x-coord of the node before mesh updation
    double get_y_p(int nd_idx) const {return el_node_vec_[nd_idx]->get_y_p();}                /// get y-coord of the node before mesh updation
    double get_z_p(int nd_idx) const {return el_node_vec_[nd_idx]->get_z_p();}                /// get z-coord of the node before mesh updation

    

    double get_side_x(int sd_idx, int nd_indx)const{return el_node_vec_[side_[sd_idx][nd_indx]]->get_x();}
    double get_side_y(int sd_idx, int nd_indx)const{return el_node_vec_[side_[sd_idx][nd_indx]]->get_y();}
    double get_side_z(int sd_idx, int nd_indx)const{return el_node_vec_[side_[sd_idx][nd_indx]]->get_z();}
 
    double get_side_mid_x(int sd_idx)const 
    { 
       double x = 0.0;
       for(int i=0; i<nb_side_nds(sd_idx); i++)
         x += el_node_vec_[side_[sd_idx][i]]->get_x();
       return x/nb_side_nds(sd_idx);
    }
    
    double get_side_mid_y(int sd_idx)const 
    { 
       double y = 0.0;
       for(int i=0; i<nb_side_nds(sd_idx); i++)
         y += el_node_vec_[side_[sd_idx][i]]->get_y();
       return y/nb_side_nds(sd_idx);
    }
       
    double get_side_mid_z(int sd_idx)const 
    { 
       double z = 0.0;
       for(int i=0; i<nb_side_nds(sd_idx); i++)
         z += el_node_vec_[side_[sd_idx][i]]->get_z();
       return z/nb_side_nds(sd_idx);
    }
    



    // check if point pt is inside this element
    bool is_inside(const point<3>& pt)
    {           
       // we first use element mapping to find the coordinates of the point in parametric domain
       // and then test if it belongs to the parametric element

       bool pt_is_inside = false;       
       double tol(std::numeric_limits<double>::epsilon());
       if(nb_nodes_== 4)
       {
         double x0 = get_x(0),  x1 = get_x(1),  x2 = get_x(2),  x3 = get_x(3); 
         double y0 = get_y(0),  y1 = get_y(1),  y2 = get_y(2),  y3 = get_y(3); 
         double z0 = get_z(0),  z1 = get_z(1),  z2 = get_z(2),  z3 = get_z(3); 

         double a1 = x1-x0,   b1 = x2-x0,   c1 = x3-x0,   d1 = pt.get_x()-x0;
         double a2 = y1-y0,   b2 = y2-y0,   c2 = y3-y0,   d2 = pt.get_y()-y0;
         double a3 = z1-z0,   b3 = z2-z0,   c3 = z3-z0,   d3 = pt.get_z()-z0;
         
         double D = (c1*a2*b3 + a1*b2*c3 + b1*c2*a3) - (b1*a2*c3 + c1*b2*a3 + a1*c2*b3);
         double D1 = (c1*d2*b3 + d1*b2*c3 + b1*c2*d3) - (b1*d2*c3 + c1*b2*d3 + d1*c2*b3);
         double D2 = (c1*a2*d3 + a1*d2*c3 + d1*c2*a3) - (d1*a2*c3 + c1*d2*a3 + a1*c2*d3);
         double D3 = (d1*a2*b3 + a1*b2*d3 + b1*d2*a3) - (b1*a2*d3 + d1*b2*a3 + a1*d2*b3);
         
         double xi = D1/D;  
         double eta = D2/D;  
         double zeta = D3/D;  
                            
         if(xi >= 0.0-tol && xi <= 1.0+tol)
           if(eta >= 0.0-tol && eta <= 1.0+tol)
             if(zeta >= 0.0-tol && zeta <= 1.0+tol)
               if(xi+eta+zeta <= 1.0+tol)
                  pt_is_inside = true;         
          
       }

       else if(nb_nodes_== 8)
       {
          // to be implemented
       }
        
       return pt_is_inside;  
    }
    
    
    auto& quad_i() { return quad_i_; }                        
    const auto& quad_i()const { return quad_i_; }                     

    auto& quad_i(int gp_idx) { return quad_i_[gp_idx]; }                        
    const auto& quad_i(int gp_idx)const { return quad_i_[gp_idx]; }                    

    auto& quad_b(){return quad_b_;}
    const auto& quad_b()const {return quad_b_;}        
    
    auto& quad_b(int side_idx){return quad_b_[side_idx];}   
    const auto& quad_b(int side_idx)const{return quad_b_[side_idx];}    
    
    auto& quad_b(int side_idx, int gp_idx){return quad_b_[side_idx][gp_idx];}    
    const auto& quad_b(int side_idx, int gp_idx)const{return quad_b_[side_idx][gp_idx];}    
    
    
    

    void compute_dofs_gids()
    {
      int nb_nd_dofs = el_node_vec_[0]->nb_dofs();
      dofs_gids_.reserve(nb_nodes_ * nb_nd_dofs);
      clear(dofs_gids_);
      
      for (int i=0; i<nb_nodes_; i++) 
	for(int j=0; j<nb_nd_dofs; j++)	
	  dofs_gids_.push_back(el_node_vec_[i]->first_dof_gid() + j);
    }

    auto dofs_gids()const {return dofs_gids_;}
    



    template<typename dofs_ic_bc_type>
    auto dofs_constraints(dofs_ic_bc_type& dofs_ic_bc)
    {
      int nb_nd_dofs = el_node_vec_[0]->nb_dofs();
      std::vector<std::pair<bool,double>> el_dofs_constraints;   /// vector of dofs constraints with values of the element
      el_dofs_constraints.reserve(nb_nodes_ * nb_nd_dofs);            

      for (int i=0; i<nb_nodes_; i++) 
      {
        auto nd_dofs_constraints = dofs_ic_bc.dofs_constraints(*(el_node_vec_[i]));
        el_dofs_constraints.insert(std::end(el_dofs_constraints), std::begin(nd_dofs_constraints), std::end(nd_dofs_constraints));
      }
      
      return el_dofs_constraints;
    }




    void compute_min_h()
    {
       if(nb_nodes_== 3)
       {
         double x0 = get_x(0),  x1 = get_x(1),  x2 = get_x(2); 
         double y0 = get_y(0),  y1 = get_y(1),  y2 = get_y(2); 
         double l1 = std::sqrt((x1 - x0)*(x1 - x0) + (y1 - y0)*(y1 - y0));  
         double l2 = std::sqrt((x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1));  

         double l3 = std::sqrt((x0 - x2)*(x0 - x2) + (y0 - y2)*(y0 - y2));
         min_h_ = std::min({l1, l2, l3});  
       } 
       
       if(nb_nodes_== 4)
       {
         double x0 = get_x(0),  x1 = get_x(1),  x2 = get_x(2),  x3 = get_x(3); 
         double y0 = get_y(0),  y1 = get_y(1),  y2 = get_y(2),  y3 = get_y(3); 
         double l1 = std::sqrt((x1 - x0)*(x1 - x0) + (y1 - y0)*(y1 - y0));  
         double l2 = std::sqrt((x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1));  
         double l3 = std::sqrt((x2 - x0)*(x2 - x0) + (y2 - y0)*(y2 - y0));
         double l4 = std::sqrt((x0 - x3)*(x0 - x3) + (y0 - y3)*(y0 - y3));
         double l5 = std::sqrt((x2 - x3)*(x2 - x3) + (y2 - y3)*(y2 - y3));
         double l6 = std::sqrt((x1 - x3)*(x1 - x3) + (y1 - y3)*(y1 - y3));
         min_h_ = std::min({l1, l2, l3, l4, l5, l6});  
       }

       else if(nb_nodes_== 8)
       {
         double x0 = get_x(0),  x1 = get_x(1),  x2 = get_x(2),  x3 = get_x(3); 
         double x4 = get_x(4),  x5 = get_x(5),  x6 = get_x(6),  x7 = get_x(7);          
         double y0 = get_y(0),  y1 = get_y(1),  y2 = get_y(2),  y3 = get_y(3); 
         double y4 = get_y(4),  y5 = get_y(5),  y6 = get_y(6),  y7 = get_y(7); 

         double l1 = std::sqrt((x1 - x0)*(x1 - x0) + (y1 - y0)*(y1 - y0));  
         double l2 = std::sqrt((x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1));  
         double l3 = std::sqrt((x3 - x2)*(x3 - x2) + (y3 - y2)*(y3 - y2));
         double l4 = std::sqrt((x0 - x3)*(x0 - x3) + (y0 - y3)*(y0 - y3));

         double l5 = std::sqrt((x5 - x4)*(x5 - x4) + (y5 - y4)*(y5 - y4));  
         double l6 = std::sqrt((x6 - x5)*(x6 - x5) + (y6 - y5)*(y6 - y5));  
         double l7 = std::sqrt((x7 - x6)*(x7 - x6) + (y7 - y6)*(y7 - y6));
         double l8 = std::sqrt((x4 - x7)*(x4 - x7) + (y4 - y7)*(y4 - y7));
         
         double l9 = std::sqrt((x4 - x0)*(x4 - x0) + (y4 - y0)*(y4 - y0));
         double l10 = std::sqrt((x7 - x3)*(x7 - x3) + (y7 - y3)*(y7 - y3));
         double l11 = std::sqrt((x2 - x6)*(x2 - x6) + (y2 - y6)*(y2 - y6));
         double l12 = std::sqrt((x1 - x5)*(x1 - x5) + (y1 - y5)*(y1 - y5));
         
         min_h_ = std::min({l1, l2, l3, l4, l5, l6, l7, l8, l9, l10, l11, l12});  
       }                         
    }    


    double min_h() const {return min_h_;}    


  private:
  
    int pid_;                                              /// process id of the item
    int gid_;                                              /// global id of the item among all processes
    int lid_;                                              /// local id of the item on the owner process
    bool on_boundary_;                                     /// flag: if el is on bundary
    int el_flag_;                                          /// flag number of the element
    int nb_nodes_;                                         /// number of nodes with which el is composed 
    std::vector<int> node_gid_vec_;                        /// vector of node gids belong to the element
    std::vector<std::vector<int>> side_nodes_;             /// vector of node_gids for each side of the el
    std::vector<int> side_flag_;                           /// vector of side_flag for each side of the el
    std::vector<bool> is_side_on_boundary_;                /// vector of flag(if side is on bd) for each side of the el
    std::vector<std::shared_ptr<node<3>>> el_node_vec_;    /// vector of nodes belong to the element
    std::vector<int> dofs_gids_;                           /// vector of dofs gids of the element 
    double min_x_, max_x_, min_y_, max_y_, min_z_, max_z_;
    double cx_, cy_, cz_;                                  /// centroid of element
    double min_h_;                                         /// minimum edge length of the element

    std::vector<std::shared_ptr<quad>> quad_i_;                            ///quad data computed at each gp for interior integration  
    std::vector<std::vector<std::shared_ptr<quad>>> quad_b_;               ///quad data computed at each gp for boundary integration  
    
  };
  //-----------------------------------------------------------------------------------------------------------------------
  






}//namespace GALES
#endif
