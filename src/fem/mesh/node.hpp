#ifndef _GALES_NODE_HPP_
#define _GALES_NODE_HPP_

#include <vector>
#include <tuple>
#include "point.hpp"



namespace GALES {



   /**
       This class contains all info for a node such as coordinates, boundary_flag, number of dofs it carries, etc.   
   */
      
   
   
   //-----------------------------------------------------------------------------------------------------------------------   
   // This is generic template for node<dim>
   template<int dim>
   class node{};
   //-----------------------------------------------------------------------------------------------------------------------
   



   

   //-----------------------------------------------------------------------------------------------------------------------
   // This class is tempate specialized of node<dim> with dim = 2 
   template<>
   class node<2>
   {
     public:

         //-------------------------------------------------------------------------------------------------------------------------------------        
         node() = default;   //default constructor         
         node(const point<2>& pt) : point_(pt){} // custom constructor (this is used in pressure profiles)        
         /// Deleting the copy and move constructors - no duplication/transfer in anyway
         node(const node&) = delete;               //copy constructor
         node& operator=(const node&) = delete;    //copy assignment operator
         node(node&&) = delete;                    //move constructor  
         node& operator=(node&&) = delete;         //move assignment operator 
         //-------------------------------------------------------------------------------------------------------------------------------------
     
         void pid(int i){pid_=i;}                                                                /// set pid of the node
         int pid()const{return pid_;}                                                            /// get pid of the node  
       
         void gid(int i){gid_=i;}                                                                /// set gid of the node
         int gid()const{return gid_;}                                                            /// get gid of the node
   
         void lid(int i){lid_=i;}                                                                /// set lid of the node
         int lid()const{return lid_;}                                                            /// get lid of the node
   
         void on_boundary(bool a) { on_boundary_=a;}                                             /// set whether the node is on boundary
         bool on_boundary() const { return on_boundary_;}                                        /// returns whether the node is on boundary
   
         void flag(int flag) {flag_ = flag;}                                                     /// set node flag for location;   interior: 0;    boundary: non zero;   fluid-solid interface: 1
         int flag() const {return flag_;}                                                        /// returns the flag
         
         void nb_dofs(int nb_dofs) {nb_dofs_ = nb_dofs;}                                         /// set the number of dofs for node            
         int nb_dofs()const { return nb_dofs_; }                                                 /// returns the number of dofs for node
   
         int first_dof_gid()const {return gid_*nb_dofs_;}                                        /// This returns first_dof_gid of node

         void first_dof_lid(int lid) {first_dof_lid_ = lid;}                                     /// This is called from maps.hpp to set the first_dof_lid
         int first_dof_lid()const {return first_dof_lid_;}                                       /// This is called from dof_extractor

         void set_x(double x) { point_.set_x(x);}                                  /// set x-coord of the node at the current time (t_{n+1}) 
         void set_y(double y) { point_.set_y(y);}                                  /// set y-coord of the node at the current time (t_{n+1}) 
         void set_z(double z) { /* do nothing */; }                                                  
   
         double get_x() const {return point_.get_x(); }                            /// get x-coord of the node at the current time (t_{n+1}) 
         double get_y() const {return point_.get_y(); }                            /// get y-coord of the node at the current time (t_{n+1}) 
         double get_z() const {return 1.e20; }                                 

         
         point<2> get_pt() const {return point_;}
         

         void set_x_p(double x) { point_p_.set_x(x);}                                  /// set x-coord of the node before mesh updation  
         void set_y_p(double y) { point_p_.set_y(y);}                                  /// set y-coord of the node before mesh updation 
         void set_z_p(double z) { /* do nothing */; }                                                  
   
         double get_x_p() const {return point_p_.get_x(); }                            /// get x-coord of the node before mesh updation  
         double get_y_p() const {return point_p_.get_y(); }                            /// get y-coord of the node before mesh updation  
         double get_z_p() const {return 1.e20; }                                 


         void coord(const point<2>& pt) {point_ = pt;}                                /// set nd coordinates in form of point
         const point<2>& coord()const { return point_;}                               /// get nd coordinates in form of point
        
         static int dimension() { return 2; }                                      /// returns the node dimension
   

         ///---------------- These are for mesh motion -------------------------------------------------------------------------
         void set_p_coord() 
         {           
           set_x_p(get_x());          // x ----> x_p
           set_y_p(get_y());          // y ----> x_p     
         }

         void update_coord(const std::vector<double>& u) 
         {
           set_x(get_x_p() + u[0]);           // update the x-coord  x ---> x_p + ux
           set_y(get_y_p() + u[1]);           // update the y-coord  y ---> y_p + uy           
         }
         ///---------------- ---------------------------------------------------------------------------------------------------
  
     
     private:
         int pid_;                                              /// process id of the item
         int gid_;                                              /// global id of the item among all processes
         int lid_;                                              /// local id of the item on the owner process
         bool on_boundary_;                                     /// flag to check if node is on boundary or not  
         int flag_;                                             /// flag for location: interior(0) and boundary(non zero)
         int nb_dofs_;                                          /// number of dofs carried by the node
         int first_dof_lid_;                                    /// the lid of the first dof of the node
         point<2> point_;                                       /// coordinates of the node at the current time (t_{n+1}) 
         point<2> point_p_;                                     /// coordinates of the node before mesh updation          
   };
   //-----------------------------------------------------------------------------------------------------------------------



   




   //-----------------------------------------------------------------------------------------------------------------------
   // This class is tempate specialized of node<dim> with dim = 3 
   template<>
   class node<3>
   {
     public:
     
         //-------------------------------------------------------------------------------------------------------------------------------------        
         node() = default;   //default constructor
         node(const point<3>& pt) : point_(pt){} // custom constructor  (this is used in pressure profiles)          
         /// Deleting the copy and move constructors - no duplication/transfer in anyway
         node(const node&) = delete;               //copy constructor
         node& operator=(const node&) = delete;    //copy assignment operator
         node(node&&) = delete;                    //move constructor  
         node& operator=(node&&) = delete;         //move assignment operator 
         //-------------------------------------------------------------------------------------------------------------------------------------

         void pid(int i){pid_=i;}                                                                /// set pid of the node
         int pid()const{return pid_;}                                                            /// get pid of the node  
       
         void gid(int i){gid_=i;}                                                                /// set gid of the node
         int gid()const{return gid_;}                                                            /// get gid of the node
   
         void lid(int i){lid_=i;}                                                                /// set lid of the node
         int lid()const{return lid_;}                                                            /// get lid of the node
   
         void on_boundary(bool a) { on_boundary_=a;}                                             /// set whether the node is on boundary
         bool on_boundary() const { return on_boundary_;}                                        /// returns whether the node is on boundary
   
         void flag(int flag) {flag_ = flag;}                                                     /// set node flag for location;   interior: 0;    boundary: non zero;   fluid-solid interface: 1
         int flag() const {return flag_;}                                                        /// returns the flag
         
         void nb_dofs(int nb_dofs) {nb_dofs_ = nb_dofs;}                                         /// set the number of dofs for node            
         int nb_dofs()const { return nb_dofs_; }                                                 /// returns the number of dofs for node
   
         int first_dof_gid()const {return gid()*nb_dofs_;}                                       /// This returns first_dof_gid of node
         void first_dof_lid(int lid) {first_dof_lid_ = lid;}                                     /// This is called from maps.hpp to set the first_dof_lid
         int first_dof_lid()const {return first_dof_lid_;}                                       /// This is called from dof_extractor
   
         void set_x(double x) { point_.set_x(x);}                                  /// set x-coord of the node at the current time (t_{n+1}) 
         void set_y(double y) { point_.set_y(y);}                                  /// set y-coord of the node at the current time (t_{n+1}) 
         void set_z(double z) { point_.set_z(z);}                                  /// set z-coord of the node at the current time (t_{n+1}) 
   
         double get_x() const {return point_.get_x(); }                                              /// get x-coord of the node at the current time (t_{n+1}) 
         double get_y() const {return point_.get_y(); }                                              /// get y-coord of the node at the current time (t_{n+1}) 
         double get_z() const {return point_.get_z(); }                                              /// get z-coord of the node at the current time (t_{n+1}) 


         point<3> get_pt() const {return point_;}

        
         void set_x_p(double x) { point_p_.set_x(x);}                                  /// set x-coord of the node before mesh updation  
         void set_y_p(double y) { point_p_.set_y(y);}                                  /// set y-coord of the node before mesh updation 
         void set_z_p(double z) { point_p_.set_z(z); }                                 /// set z-coord of the node before mesh updation         
   
         double get_x_p() const {return point_p_.get_x(); }                            /// get x-coord of the node before mesh updation  
         double get_y_p() const {return point_p_.get_y(); }                            /// get y-coord of the node before mesh updation  
         double get_z_p() const {return point_p_.get_z(); }                            /// get z-coord of the node before mesh updation     

         void coord(const point<3>& pt) {point_ = pt;}                                /// set nd coordinates in form of point
         const point<3>& coord()const { return point_;}                               /// get nd coordinates in form of point

         static int dimension() { return 3; }                                                    /// returns the node dimension
   
         ///---------------- These are for mesh motion -------------------------------------------------------------------------
         void set_p_coord() 
         {           
           set_x_p(get_x());           // x ----> x_p
           set_y_p(get_y());           // y ----> x_p     
           set_z_p(get_z());           // z ----> z_p           
         }

         void update_coord(const std::vector<double>& u) 
         {
           set_x(get_x_p() + u[0]);           // update the x-coord  x ---> x_p + ux
           set_y(get_y_p() + u[1]);           // update the y-coord  y ---> y_p + uy           
           set_z(get_z_p() + u[2]);           // update the z-coord  z ---> z_p + uz            
         }
         ///---------------- ---------------------------------------------------------------------------------------------------


     
     private:
         int pid_;                                              /// process id of the item
         int gid_;                                              /// global id of the item among all processes
         int lid_;                                              /// local id of the item on the owner process
         bool on_boundary_;                                     /// flag to check if node is on boundary or not  
         int flag_;                                             /// flag for location: interior(0) and boundary(non zero)
         int nb_dofs_;                                          /// number of dofs carried by the node
         int first_dof_lid_;                                    /// the lid of the first dof of the node
         point<3> point_;                                       /// coordinates of the node at the current time (t_{n+1}) 
         point<3> point_p_;                                     /// coordinates of the node before mesh updation          
   };
   //-----------------------------------------------------------------------------------------------------------------------
   
   
   


} //namespace GALES
#endif

