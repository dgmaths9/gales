#ifndef DOFS_UPDATER_HPP
#define DOFS_UPDATER_HPP





namespace GALES{



  class dofs_updater
  {

     using vec = boost::numeric::ublas::vector<double>;


     public:
      
      dofs_updater(read_setup& setup)
      :
      setup_(setup)
      {
        alpha_m_ = 0.5*(3.0-setup.rho_inf())/(1.0+setup.rho_inf());
        alpha_f_ = 1.0/(1.0+setup.rho_inf());
        gamma_ = 0.5 + alpha_m_ - alpha_f_;
        beta_ = 0.25*std::pow(1.0 + alpha_m_ - alpha_f_, 2);                       
      }




    //-------------------------------------------------------------------------------------------------------------------------------------        
    /// Deleting the copy and move constructors - no duplication/transfer in anyway
    dofs_updater(const dofs_updater&) = delete;               //copy constructor
    dofs_updater& operator=(const dofs_updater&) = delete;    //copy assignment operator
    dofs_updater(dofs_updater&&) = delete;                    //move constructor  
    dofs_updater& operator=(dofs_updater&&) = delete;         //move assignment operator 
    //-------------------------------------------------------------------------------------------------------------------------------------




    //-------------------------------- This is for elastostatics mesh motion and fluid 1st order-------------------------------------------------
    void predictor(dof_state& state) 
    {
       state.dofs(1) = state.dofs(0); //P = last corrected/computed      
    }

    //------------------------------------------------------------------------------------------------------------------





    //-------------------------------- This is for fluid 2nd order -------------------------------------------------
    void predictor(dof_state& v_state, dof_state& a_state) 
    {
          const double dt(time::get().delta_t());  
          if(setup_.zero_a())
          {    
                 a_state.dofs(1) = a_state.dofs(0); //P  = I, shift in time
                 a_state.dofs(0).clear();
      
                 v_state.dofs(1) = v_state.dofs(0); //P  = I, shift in time
                 v_state.dofs(0) = v_state.dofs(1) + (1-gamma_)*dt*a_state.dofs(1);
           }
           else if(setup_.constant_v())
           {
                 v_state.dofs(1) = v_state.dofs(0); //P  = I, shift in time

                 a_state.dofs(1) = a_state.dofs(0); //P  = I, shift in time
                 a_state.dofs(0) = (gamma_-1)*a_state.dofs(1)/gamma_;      
           }     
    }

    //------------------------------------------------------------------------------------------------------------------





    //-------------------------------- This is for elastodynamics 2nd order -------------------------------------------------
    void predictor(dof_state& u_state, dof_state& v_state, dof_state& a_state) 
    {
          const double dt(time::get().delta_t());  
          if(setup_.zero_a())
          {    
                 a_state.dofs(1) = a_state.dofs(0); //P  = I, shift in time
                 a_state.dofs(0).clear();
      
                 v_state.dofs(1) = v_state.dofs(0); //P  = I, shift in time
                 v_state.dofs(0) = v_state.dofs(1) + (1-gamma_)*dt*a_state.dofs(1);

                 u_state.dofs(1) = u_state.dofs(0); //P  = I, shift in time
                 u_state.dofs(0) = u_state.dofs(1) + dt*v_state.dofs(1) + dt*dt*0.5*(1-2*beta_)*a_state.dofs(1);
           }
           else if(setup_.constant_v())
           {
                 v_state.dofs(1) = v_state.dofs(0); //P  = I, shift in time

                 a_state.dofs(1) = a_state.dofs(0); //P  = I, shift in time
                 a_state.dofs(0) = (gamma_-1)*a_state.dofs(1)/gamma_;
      
                 u_state.dofs(1) = u_state.dofs(0); //P  = I, shift in time
                 u_state.dofs(0) = u_state.dofs(1) + dt*v_state.dofs(1) + dt*dt*0.5*((1-2*beta_)*a_state.dofs(1)+ 2*beta_*a_state.dofs(0));
           }     
    } 
    //------------------------------------------------------------------------------------------------------------------













    //-------------------------------- This is for elastostatics and elastostatics mesh motion -------------------------------------------------

    void solution(dof_state& state, const vec& correction)
    {
        state.dofs(0) = correction;     //I  = solution
    }
    //------------------------------------------------------------------------------------------------------------------



    //--------------------------------------This is for fluid 1st order-----------------------------------------------
    void corrector(dof_state& state, const vec& correction)
    {
         state.dofs(0) += correction;             //I   = I + correction
    }
    //------------------------------------------------------------------------------------------------------------------
    



    //-------------------------------- This is for fluid 2nd order -------------------------------------------------
    void corrector(dof_state& v_state, dof_state& a_state, const vec& correction)
    {
           const double dt(time::get().delta_t());        
	   a_state.dofs(0) = a_state.dofs(0) + correction;
	   v_state.dofs(0) = v_state.dofs(0) + gamma_*dt*correction;
    }
    //------------------------------------------------------------------------------------------------------------------





    //-------------------------------- This is for elastodynamics 2nd order -------------------------------------------------
    void corrector(dof_state& u_state, dof_state& v_state, dof_state& a_state, const vec& correction)
    {
           const double dt(time::get().delta_t());        
	   a_state.dofs(0) = a_state.dofs(0) + correction;
	   v_state.dofs(0) = v_state.dofs(0) + gamma_*dt*correction;
	   u_state.dofs(0) = u_state.dofs(0) + beta_*dt*dt*correction;
    }
    //------------------------------------------------------------------------------------------------------------------




    private:
    read_setup& setup_;
    double beta_, gamma_, alpha_f_, alpha_m_;
  }; 



}

#endif


