#ifndef GALES_DOF_TRIMMER_HPP
#define GALES_DOF_TRIMMER_HPP




namespace GALES 
{

  template<int dim>
  class dof_trimmer
  {
     
     using model_type = model<dim>;
     
     public:
     
         
     dof_trimmer(model_type& model)
     :
     model_(&model)
     {}


     
     dof_trimmer(model_type& model, model_type& dot_model)
     :
     model_(&model), dot_model_(&dot_model)
     {
       const auto alpha_m = 0.5*(3.0-model.setup().rho_inf())/(1.0+model.setup().rho_inf());
       const auto alpha_f = 1.0/(1.0+model.setup().rho_inf());
       gamma_ = 0.5 + alpha_m - alpha_f;     
     }


     
    ///---------------------------------------------------------------------------------------------
    /// This function trims the dofs between lower and upper bounds according to the dof index. 
    /// for example to trim Y_dofs(p, vx, vy, vz, T, Y) between 0 and 1 we call 
    ///   dof_treamer(5, 0.0, 1.0)
    void trim(int dof_index, double l_bound, double u_bound)
    {      
      for(const auto& nd : model_->mesh().nodes())
      {
        const auto dof = model_->state().get_dof(nd->first_dof_lid() + dof_index);
        const auto new_value = std::max(l_bound, std::min(dof, u_bound) );
        model_->state().set_dof(nd->first_dof_lid() + dof_index, new_value);                
      }  
    }
    ///---------------------------------------------------------------------------------------------






    ///---------------------------------------------------------------------------------------------
    /// This function is for generalized-alpha method; in case we trim the fluid dofs
    void trim(int dof_index)
    {      
      
      for(const auto& nd : model_->mesh().nodes())
      {
        auto n_1 = model_->extract_node_dof(nd->lid(), dof_index);
        auto n = model_->extract_node_dof(nd->lid(), dof_index, 1);
        auto dot_n = dot_model_->extract_node_dof(nd->lid(), dof_index, 1);
        const auto dot_n_1 = (n_1 - n)/(gamma_*time::get().delta_t()) - (1.0-gamma_)/gamma_*dot_n;
        dot_model_->state().set_dof(nd->first_dof_lid()+dof_index, dot_n_1);        
      }  
    }
    ///---------------------------------------------------------------------------------------------



   private:
    model_type* dot_model_ = nullptr;
    model_type* model_ = nullptr;
    double gamma_;

  };

}//namespace GALES

#endif
