#ifndef GALES_IC_BC_READER_HPP
#define GALES_IC_BC_READER_HPP







namespace GALES {







template<int dim>
class ic_bc_reader
{

  using vec = boost::numeric::ublas::vector<double>;
  
  public:

  template<typename T>
  ic_bc_reader(T& ic_bc)
  {  
      std::vector<std::string> s;
      std::ifstream file("ic_bc.txt");
      if(!file.is_open())
      {
         Error("unable to open and read  'ic_bc.txt' "); 
      }

      while(file)
      {
         read_one_line(file, s);         
         if(s[0] == "solid")
         {
            read_s_ic_bc(file, s, ic_bc);
         }
      }
      file.close();                  
  }





  //-------------------------------------------------------------------------------------------------------------------------------------        
  /// Deleting the copy and move constructors - no duplication/transfer in anyway
  ic_bc_reader(const ic_bc_reader&) = delete;               //copy constructor
  ic_bc_reader& operator=(const ic_bc_reader&) = delete;    //copy assignment operator
  ic_bc_reader(ic_bc_reader&&) = delete;                    //move constructor  
  ic_bc_reader& operator=(ic_bc_reader&&) = delete;         //move assignment operator 
  //-------------------------------------------------------------------------------------------------------------------------------------






  template<typename T>
  void read_s_ic_bc(std::ifstream& file, std::vector<std::string>& s, T& ic_bc)
  {
     while(file)
     {
         read_one_line(file, s); 

         if(s[0] == "{")
         {
              print_only_pid<0>(std::cerr)<<"\n\n\n"<<"---------------------------------- solid ic-bc ---------------------------------------"<<"\n";                      
         } 

         else if(s[0] == "dirichlet_u")
         {
            read_dirichlet_u(file, s, ic_bc);            
         }

         else if(s[0] == "neumann_pressure")
         {
            read_neumann_pressure(file, s, ic_bc);            
         }

         else if(s[0] == "}")
         {
            print_only_pid<0>(std::cerr)<<"----------------------------------------------------------------------------------------"<<"\n\n\n";             
            return;
         }
     }    
  }











  template<typename T>
  void read_dirichlet_u(std::ifstream& file, std::vector<std::string>& s, T& ic_bc)
  {           
     std::map<int, std::vector<std::pair<bool,double>>> dirichlet_u;

     while(file)
     {
         read_one_line(file, s);      
                           
         if(s[0] == "{")
         {
            // do nothing                      
         }
         else if(s[0] == "nd_flag")
         {
            const int nd_flag = stoi(s[1]);

            std::vector<std::pair<bool,double>> u(dim);
            for(int i=0; i<dim; i++)
            {
              if(s[2+i] == "unfixed")   u[i] = std::make_pair(false, 0.0);
              else u[i] = std::make_pair(true, stod(s[2+i]));
            }  
            
            dirichlet_u[nd_flag] = u;               
         }
         else if(s[0] == "}")
         {
            ic_bc.set_dirichlet_u(dirichlet_u);                        
            return;
         }    
     }            
  }







  template<typename T>
  void read_neumann_pressure(std::ifstream& file, std::vector<std::string>& s, T& ic_bc)
  {           
     std::map<int, double> neumann_pressure;

     while(file)
     {
         read_one_line(file, s);      
                           
         if(s[0] == "{")
         {
            // do nothing                      
         }
         else if(s[0] == "side_flag")
         {
            const int side_flag = stoi(s[1]);
            const double p = stod(s[2]); 
            neumann_pressure[side_flag] = p;          
         }
         else if(s[0] == "}")
         {
            ic_bc.set_neumann_pressure(neumann_pressure);
            return;
         }    
     }            
  }






     
};


}

#endif
