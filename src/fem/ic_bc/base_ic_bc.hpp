#ifndef GALES_BASE_IC_BC_HPP
#define GALES_BASE_IC_BC_HPP


#include <vector>
#include <map>
#include "boost/numeric/ublas/vector.hpp"

#include"../mesh/node.hpp"

namespace GALES{




  template<int dim>
  class base_ic_bc 
  {
    using nd_type = node<dim>;
    using vec = boost::numeric::ublas::vector<double>;

    public : 
    
    void body_force(vec& gravity) {}

    double initial_p(const nd_type &nd)const { return 0.0; }
    double initial_vx(const nd_type &nd)const { return 0.0; }
    double initial_vy(const nd_type &nd)const { return 0.0; }
    double initial_vz(const nd_type &nd)const { return 0.0; }
    double initial_T(const nd_type &nd)const { return 0.0; }    
    void initial_Y(const nd_type &nd, vec &Y)const {}   
    

    double initial_ux(const nd_type &nd)const { return 0.0; }
    double initial_uy(const nd_type &nd)const { return 0.0; }
    double initial_uz(const nd_type &nd)const { return 0.0; }



    std::pair<bool, double> dirichlet_p(const nd_type &nd)const { return std::make_pair(false,0.0); }
    std::pair<bool, double> dirichlet_vx(const nd_type &nd)const { return std::make_pair(false,0.0); }
    std::pair<bool, double> dirichlet_vy(const nd_type &nd)const { return std::make_pair(false,0.0); }
    std::pair<bool, double> dirichlet_vz(const nd_type &nd)const { return std::make_pair(false,0.0); }
    std::pair<bool, double> dirichlet_T(const nd_type &nd)const { return std::make_pair(false,0.0); }
    std::pair<bool, double> dirichlet_Y(const nd_type &nd, int comp_index)const { return std::make_pair(false,0.0); }
    
    std::pair<bool, double> dirichlet_ux(const nd_type &nd)const { return std::make_pair(false,0.0); }
    std::pair<bool, double> dirichlet_uy(const nd_type &nd)const { return std::make_pair(false,0.0); }
    std::pair<bool, double> dirichlet_uz(const nd_type &nd)const { return std::make_pair(false,0.0); }


    std::pair<bool, double> neumann_tau11(const std::vector<int>& bd_nodes, int side_flag)const { return std::make_pair(false,0.0); }
    std::pair<bool, double> neumann_tau22(const std::vector<int>& bd_nodes, int side_flag)const { return std::make_pair(false,0.0); }
    std::pair<bool, double> neumann_tau33(const std::vector<int>& bd_nodes, int side_flag)const { return std::make_pair(false,0.0); }
    std::pair<bool, double> neumann_tau12(const std::vector<int>& bd_nodes, int side_flag)const { return std::make_pair(false,0.0); }
    std::pair<bool, double> neumann_tau13(const std::vector<int>& bd_nodes, int side_flag)const { return std::make_pair(false,0.0); }
    std::pair<bool, double> neumann_tau23(const std::vector<int>& bd_nodes, int side_flag)const { return std::make_pair(false,0.0); }

    std::pair<bool, double> neumann_q1(const std::vector<int>& bd_nodes, int side_flag)const { return std::make_pair(false,0.0); }
    std::pair<bool, double> neumann_q2(const std::vector<int>& bd_nodes, int side_flag)const { return std::make_pair(false,0.0); }
    std::pair<bool, double> neumann_q3(const std::vector<int>& bd_nodes, int side_flag)const { return std::make_pair(false,0.0); }
    
    std::pair<bool, double> neumann_q1(const std::vector<std::shared_ptr<nd_type>>& bd_nodes, int side_flag)const { return std::make_pair(false,0.0); }
    std::pair<bool, double> neumann_q2(const std::vector<std::shared_ptr<nd_type>>& bd_nodes, int side_flag)const { return std::make_pair(false,0.0); }
    std::pair<bool, double> neumann_q3(const std::vector<std::shared_ptr<nd_type>>& bd_nodes, int side_flag)const { return std::make_pair(false,0.0); } 
    
    std::pair<bool, double> neumann_J1(int comp_index, const std::vector<int>& bd_nodes, int side_flag)const { return std::make_pair(false,0.0); }
    std::pair<bool, double> neumann_J2(int comp_index, const std::vector<int>& bd_nodes, int side_flag)const { return std::make_pair(false,0.0); }
    std::pair<bool, double> neumann_J3(int comp_index, const std::vector<int>& bd_nodes, int side_flag)const { return std::make_pair(false,0.0); }

    std::pair<bool, double> mass_flux1(const std::vector<int>& bd_nodes, int side_flag)const { return std::make_pair(false,0.0); } // rho*v1 towards outwards normal 
    std::pair<bool, double> mass_flux2(const std::vector<int>& bd_nodes, int side_flag)const { return std::make_pair(false,0.0); } // rho*v2 towards outwards normal 
    std::pair<bool, double> mass_flux3(const std::vector<int>& bd_nodes, int side_flag)const { return std::make_pair(false,0.0); } // rho*v3 towards outwards normal 




    void get_fluid_heat_flux(const std::vector<int>& bd_nodes, std::vector<std::vector<double>>& f_heat_flux)const {}

  
    void set_solid_v(const std::map<int, std::vector<double> >& s_nd_v){}
    void get_sv(int f_nd_gid)const {}

    void set_solid_u(const std::map<int, std::vector<double> >& s_nd_u) {}
    void get_su(int f_nd_gid)const {}

    void set_fluid_tr(const std::map<int, std::vector<double> >& f_nd_tr) {}
    void get_fluid_tr(const std::vector<int>& s_bd_nodes, std::vector<std::vector<double>>& f_tr)const {}

    void set_heat_eq_T(const std::map<int, double>& heat_eq_nd_T) {}
    void get_heat_eq_T(int f_nd_gid) const {}
    
    
    template<typename T1, typename T2>
    void set_custom_pressure_profile(T1& model, T2& props) {}
    
    
    
    
    bool custom_pressure_profile()const {return custom_pressure_profile_;}
    bool central_pressure_profile()const {return central_pressure_profile_;}
    bool x_dependent_pressure_profile()const {return x_dependent_pressure_profile_;}
    double p0()const {return p0_;}
    double Y_min()const {return Y_min_;}
    double Y_max()const {return Y_max_;}
    double p_ref()const {return p_ref_;}
    double v1_ref()const {return v1_ref_;}
    double v2_ref()const {return v2_ref_;}
    double v3_ref()const {return v3_ref_;}
    double T_ref()const {return T_ref_;}
    double max()const {return max_;}
    double min()const {return min_;}
    int total_steps()const {return total_steps_;}
    double h()const {return h_;}
    

    protected:
    bool custom_pressure_profile_ = false;
    bool central_pressure_profile_ = false;
    bool x_dependent_pressure_profile_ = false;
    double p0_ = 0.0;
    double Y_min_ = 0.0; 
    double Y_max_ = 0.0;        
    double p_ref_ = 0.0; 
    double v1_ref_ = 0.0; 
    double v2_ref_ = 0.0; 
    double v3_ref_ = 0.0; 
    double T_ref_ = 0.0;
    double max_ = 0.0; 
    double min_ = 0.0;
    int total_steps_ = 0;
    double h_ = 0.0;
           
  };


}


#endif
