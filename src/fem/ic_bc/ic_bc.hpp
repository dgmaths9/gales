#ifndef IC_BC_HPP
#define IC_BC_HPP



#include "../mesh/node.hpp"
#include "../mesh/point.hpp"
#include "ic_bc_reader.hpp"





namespace GALES{




  template<int dim>
  class ic_bc 
  {
    using nd_type = node<dim>;
    using point_type = point<dim>;
    using vec = boost::numeric::ublas::vector<double>;

    public : 
    

    ic_bc() 
    {
//      ic_bc_reader<dim> reader(*this);
    }



 
   //------------set dirichlet bc------------------------------------
    void set_dirichlet_u(const std::map<int, std::vector<std::pair<bool,double>>>& dirichlet_u) {dirichlet_u_ = dirichlet_u;}             
    auto dirichlet_u() const {return dirichlet_u_;}
           
           


    //--------------set Neummann pressure-------------------------------------
    void set_neumann_pressure(const std::map<int, double>& neumann_p) {neumann_p_ = neumann_p;}             
    auto neumann_pressure() const {return neumann_p_;}

  
  
  
  private:
  std::map<int, std::vector<std::pair<bool,double>>> dirichlet_u_;
  std::map<int, double> neumann_p_;

  };


}


#endif
