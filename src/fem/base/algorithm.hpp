#ifndef GALES_ALGORITHM_HPP
#define GALES_ALGORITHM_HPP


#include <vector>
#include <algorithm>
#include <iterator>
#include <dirent.h>
#include <fstream>
#include <string>
#include <map>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/vector_proxy.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/matrix_proxy.hpp>
#include <boost/numeric/ublas/triangular.hpp>
#include <boost/numeric/ublas/lu.hpp>
#include <boost/numeric/ublas/io.hpp>
#include <boost/algorithm/string.hpp>
#include <type_traits>
#include <typeinfo>
#include <mpi.h>
#include "../mesh/point.hpp"



namespace GALES{





  ///----------------------------------------------------------------------------------------- 
  // Error(" err_msg") can now be used to produce a error message that includes the file and line in which the error occurrred
  void error(const char* file, int line, const std::string& message)
  {
    std::cerr<<"\n\n\n\n\n";    
    std::cerr<<"===========================================  ERROR  ==============================================="<<"\n";    
    std::cerr<<"==================================================================================================="<<"\n";
    std::cerr << file << ":" << line << ": error: "  << message <<"\n";
    std::cerr<<"===================================================================================================";    
    std::cerr<<"\n\n\n\n\n";    
    MPI_Abort(MPI_COMM_WORLD, 1);
  }
  #define Error( message ) error( __FILE__, __LINE__, message );
  ///----------------------------------------------------------------------------------------- 




   
   
  ///----------------------------------------------------------------------------------------- 
  // This function returns a boolean to check if a file exists in a given path 
  bool does_file_exist(const std::string& file_path)
  {
      std::ifstream ifile(file_path);    	
      return (bool)ifile;
  } 
  ///----------------------------------------------------------------------------------------- 
   




  ///----------------------------------------------------------------------------------------- 
  // This function returns a boolean to check if a value is in vector 
  template<typename T>
  bool is_in_vector(const std::vector<T>& v, T d)
  {
     int cnt = count(v.begin(), v.end(), d); 
     if(cnt > 0) return true;
     else return false;
  }
  ///----------------------------------------------------------------------------------------- 




  ///----------------------------------------------------------------------------------------- 
  /// Sort the vector. 
  template<typename T>
  void sort(std::vector<T>& v)
  {
      sort(v.begin(), v.end());
  }
  ///----------------------------------------------------------------------------------------- 
  







  ///----------------------------------------------------------------------------------------- 
  /// Sort the vector. 
  template<typename T>
  auto sort_vector(const std::vector<T>& v)
  {
      auto v1 = v;
      sort(v1.begin(), v1.end());
      return v1;
  }
  ///----------------------------------------------------------------------------------------- 







  ///----------------------------------------------------------------------------------------- 
  /// unique the vector. 
  template<typename T>
  void unique(std::vector<T>& v)
  {
      auto last = std::unique(v.begin(), v.end()) ;
      v.erase(last, v.end());             
  }
  ///----------------------------------------------------------------------------------------- 








  ///----------------------------------------------------------------------------------------- 
  /// unique the vector. 
  template<typename T>
  auto unique_vector(const std::vector<T>& v)
  {
      auto v1 = v;
      auto last = std::unique(v1.begin(), v1.end()) ;
      v1.erase(last, v1.end());             
      return v1;
  }
  ///----------------------------------------------------------------------------------------- 






  ///----------------------------------------------------------------------------------------- 
  /// Sort and unique the vector. 
  template<typename T>
  void sort_unique(std::vector<T>& v)
  {
      sort(v);
      unique(v);            
  }
  ///----------------------------------------------------------------------------------------- 








  ///----------------------------------------------------------------------------------------- 
  /// Sort and unique the vector. 
  template<typename T>
  auto sort_unique_vector(const std::vector<T>& v)
  {
      auto v1 = v;
      sort(v1);
      unique(v1);            
      return v1;
  }
  ///----------------------------------------------------------------------------------------- 











  ///----------------------------------------------------------------------------------------- 
   /// This is to format data; it returns a string with data alligned according to the input width.
   template<typename data_type>
   auto parse(const data_type& d, int width)
   {
     std::stringstream ss;
     ss<< std::left;
     ss.fill(' ');      
     ss.width(width); 
     ss << d;    
     return ss.str();
   }
  ///----------------------------------------------------------------------------------------- 








  ///----------------------------------------------------------------------------------------- 
  /// check if vecs: v1 and v2 are equal irrespective of the order of the numbers
  template<typename T>
  bool are_equal(std::vector<T> v1, std::vector<T> v2)  //we don't want to modify the original, so we do copy
  {
      sort(v1.begin(), v1.end());
      sort(v2.begin(), v2.end());
      if(v1==v2) return true;
      else return false;
  }
  ///----------------------------------------------------------------------------------------- 







   ///----------------------------------------------------------------------------------------- 
   /// This is to print std vector with std::cout or std::cerr. 
    template<typename E, typename T, typename data_type>
    std::basic_ostream<E, T> &operator << (std::basic_ostream<E,T>& os, const std::vector<data_type>& v) 
    {
        std::size_t size = v.size();        
        std::basic_ostringstream<E, T, std::allocator<E>> s;        
        s.flags(os.flags());
        s.imbue(os.getloc());
        s.precision(os.precision());
        s << "[" << size << "](";
        if(size > 0) s << v[0];
        for(std::size_t i=1; i<size; i++)  s << ", " << v[i];
        s << ")";
        return os << s.str().c_str();
    }
   ///----------------------------------------------------------------------------------------- 







   ///----------------------------------------------------------------------------------------- 
   /// This is to fill std::vector with zeros. 
   template<typename T>
   void clear(std::vector<T>& v)
   {
     std::fill(v.begin(), v.end(), T(0.0));
   }
   ///----------------------------------------------------------------------------------------- 






  ///----------------------------------------------------------------------------------------- 
  /// return diagonal matrix of a matrix
  template<typename mat>
  mat diag_mat_of_mat(const mat& m)
  {
     mat m1(m.size1(),m.size1(),0.0);
     for(int i=0; i<m.size1(); i++) 
       m1(i,i) = m(i,i);
     return m1;  
  }
  ///----------------------------------------------------------------------------------------- 







  ///----------------------------------------------------------------------------------------- 
  /// return average value of the diagonal of a matrix
  template<typename mat>
  double avg_of_diag(const mat& m)
  {
     const int n_item(m.size1());
     double avg_val(1.0);
     for(int i=0; i<n_item; ++i)
       avg_val += m(i,i);
     avg_val /= n_item;
     return avg_val;  
  }
  ///----------------------------------------------------------------------------------------- 








  ///----------------------------------------------------------------------------------------- 
  /// return absolute average value of the diagonal of a matrix
  template<typename mat>
  double absolute_avg_of_diag(const mat& m)
  {
     const int n_item(m.size1());
     double avg_val(1.0);
     for(int i=0; i<n_item; ++i)
       avg_val += std::abs(m(i,i));
     avg_val /= n_item;
     return avg_val;  
  }
  ///----------------------------------------------------------------------------------------- 





  ///----------------------------------------------------------------------------------------- 
  /// This function reads only one line from the file (input) and return a vector of string as output
  void read_one_line(std::ifstream& file, std::vector<std::string>& split_result)
  {
      std::string s;
      getline(file,s);
      boost::trim(s);
      boost::split(split_result, s, boost::is_any_of(", "), boost::token_compress_on);
  }
  ///----------------------------------------------------------------------------------------- 




   auto split_line(const std::string & line)
   {
      using namespace std;
      vector<string> split_s;
      stringstream ss(line);            
      string s;      
      while(ss >> s) 
        split_s.push_back(s);

      return split_s;         
   }



  ///----------------------------------------------------------------------------------------- 
  /// This function reads only one line from the file (input) and return a vector of string as output
  auto read_one_line(std::ifstream& f)
  {
      using namespace std;
      string line;
      getline(f, line);
      return split_line(line);
  }
  ///----------------------------------------------------------------------------------------- 






  ///----------------------------------------------------------------------------------------- 
  /// This function reads only one line from the file (input) to skip
  void skip_one_line(std::ifstream& file)
  {
      std::string s;
      getline(file,s);
  }
  ///----------------------------------------------------------------------------------------- 







  ///----------------------------------------------------------------------------------------- 
  /// This function extracts the last n characters of a string and returns a substring
  std::string extractLastNChars(const std::string& str, int n)
  {
    if (str.size() < n)  return str;
    return str.substr(str.size() - n);
  }
  ///----------------------------------------------------------------------------------------- 






  ///----------------------------------------------------------------------------------------- 
  /// This function returns a vector upto num-1;   e.g.  range(3) = {0,1,2} like python style
  auto range(int num)
  {
    std::vector<int> v(num);
    for(int i=0; i<num; i++)
       v[i] = i;
    return v;
  }
  ///----------------------------------------------------------------------------------------- 








  ///----------------------------------------------------------------------------------------- 
  /// This function generate directories
  void make_dirs(const std::string& s)
  {
    std::string str = "mkdir -p " + s;
    int p = system(str.c_str());
  }  
  ///----------------------------------------------------------------------------------------- 






  ///----------------------------------------------------------------------------------------- 
  /// this function converts data layout from boost matrix(2d structure) to std::vector(1d structure)
  template<typename m_type>
  auto mat_to_vec(const m_type& m)
  { 
     const int n_item(m.size1());                       
     std::vector<double> m_vec(n_item*n_item);

     for(int i=0; i<n_item; ++i)  
      for(int j=0; j<n_item; ++j)  
        m_vec[i*n_item + j] = m(i,j);

     return m_vec;
  }   
  ///----------------------------------------------------------------------------------------- 
  






  ///----------------------------------------------------------------------------------------- 
  /// This function returns a vector of all files contained in a directory
  std::vector<std::string> dir_files(const char *path) 
  {  
    std::vector<std::string> files; 

    DIR *dir = opendir(path);   
    if (dir == NULL) 
    {
       Error("Directory is empty");
    }
    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL) 
    {
        files.push_back(entry->d_name);
    }
    closedir(dir);


    std::vector<std::string> Files; 
    for(auto s: files)
    {
      if(s == "." || s == ".."){}
      else Files.push_back(s);
    }
    
    return Files;        
  }
  ///----------------------------------------------------------------------------------------- 










  ///----------------------------------------------------------------------------------------- 
  /// This function checks if a given point is inside a 2d polygon
  bool point_in_polygon(const point<2>& pt, std::vector<point<2>> pt_vec)
  {
    int num_vertices = pt_vec.size();
    double x = pt.get_x(), y = pt.get_y();
    bool inside = false;
     
    point<2> p1 = pt_vec[0], p2;    // Store the first point in the polygon and initialize the second point
     
    for(int i = 1; i <= num_vertices; i++)   // Loop through each edge in the polygon
    {
       p2 = pt_vec[i % num_vertices];         // Get the next point in the polygon       
       if(y > std::min(p1.get_y(), p2.get_y()))   // Check if the point is above the minimum y coordinate of the edge
       {            
          if(y <= std::max(p1.get_y(), p2.get_y()))   // Check if the point is below the maximum y coordinate of the edge
          {                
             if (x <= std::max(p1.get_x(), p2.get_x()))  // Check if the point is to the left of the maximum x coordinate of the edge
             {
                // Calculate the x-intersection of the line connecting the point to the edge
                double x_intersection = (y - p1.get_y()) * (p2.get_x() - p1.get_x())/ (p2.get_y() - p1.get_y()) + p1.get_x();

                // Check if the point is on the same line as the edge or to the left of the x-intersection
                if (p1.get_x() == p2.get_x() || x <= x_intersection) 
                {
                    inside = !inside;
                }
             }
          }
       }
        p1 = p2;   // Store the current point as the first point for the next iteration
    }
 
    // Return the value of the inside flag
    return inside;
  }
  ///----------------------------------------------------------------------------------------- 







  ///----------------------------------------------------------------------------------------- 
  /// This function writes vector "v" in a file in binary format
  template<typename T>
  void write_bin(const std::string& file, const std::vector<T>& v)
  {
    std::ofstream f(file, std::ios::out | std::ios::binary);
    if(!f.is_open())
    {
         Error("file " +  file + " is not opened");
    }       
    f.write(reinterpret_cast<const char*>(&v[0]), v.size()*sizeof(T));
    f.close();
  }
  ///----------------------------------------------------------------------------------------- 







  ///----------------------------------------------------------------------------------------- 
  /// This function reads vector "v" from a binary file
  template<typename T>
  void read_bin(const std::string& file, std::vector<T>& v)
  {
    std::ifstream f(file, std::ios::in | std::ios::binary);
    if(!f.is_open())
    {
         Error("file " +  file + " is not opened");
    }        
    
    f.seekg(0, std::ios::end);      // go to end of file
    const int length = f.tellg();    // get length in bytes of file
    f.seekg (0, std::ios::beg);     // go to beginning of file
    
    const int size_of_v = length/sizeof(T);        
    v.clear();
    v.resize(size_of_v);
       
    f.read(reinterpret_cast<char*>(&v[0]), v.size()*sizeof(T));
    f.close();
  }
  ///----------------------------------------------------------------------------------------- 








  ///----------------------------------------------------------------------------------------- 
  /// This function (type bool) computes inverse of a square matrix using LU-decomposition with backsubstitution of unit vectors
  template<typename mat>
  bool inverse(const mat& from, mat& to)
  {
    mat A(from);                          // create a working copy of the input
    boost::numeric::ublas::permutation_matrix<std::size_t> pm(A.size1());                // create a permutation matrix for the LU-factorization
    const int res = lu_factorize(A,pm);   // perform LU-factorization
    if(res != 0) 
    {
       Error("LU factorization fails in inverse function");   
    }

    to.assign(boost::numeric::ublas::identity_matrix<double>(A.size1()));        // create identity matrix of "inverse"
    lu_substitute(A, pm, to);                             // backsubstitute to get the inverse
    return true;
  }
  ///----------------------------------------------------------------------------------------- 






  ///----------------------------------------------------------------------------------------- 
  /// This function (type mat) computes inverse of a square matrix using LU-decomposition with backsubstitution of unit vectors
  template<typename mat>
  mat inverse(const mat& from)
  {
    mat A(from);                           // create a working copy of the input
    boost::numeric::ublas::permutation_matrix<std::size_t> pm(A.size1());                // create a permutation matrix for the LU-factorization
    const int res = lu_factorize(A, pm);   // perform LU-factorization
    if(res != 0)
    {
       Error("LU factorization fails in inverse function");   
    }        
    mat to(boost::numeric::ublas::identity_matrix<double>(A.size1()));    // create identity matrix of "inverse"
    lu_substitute(A, pm, to);                      // backsubstitute to get the inverse
    return to;
  }
  ///----------------------------------------------------------------------------------------- 







  ///----------------------------------------------------------------------------------------- 
  /// Denman_Beavers_method to compute sqrt inverse of a matrix
  template<typename mat, typename vec>
  mat mat_sqrt_inv(const mat& A)
  {
    boost::numeric::ublas::identity_matrix<double> I(A.size1()); 
    mat P = A;
    mat Q = I;
   
    double err(0.0);
    const double tol(1.e-2);
   
    mat Q_inv(A.size1(), A.size1(), 0.0);
    mat P_old_inv(A.size1(), A.size1(),0.0);
      
    int it=1;
    while(err > tol)
    {
        mat P_old = P;
        inverse(Q, Q_inv);
        inverse(P_old, P_old_inv);            
        
        P = 0.5*(P_old + Q_inv);        
        Q = 0.5*(Q + P_old_inv);
        
        mat err_mat = P - P_old;                        
        
        for(std::size_t i=0; i<A.size1(); i++)
        {
           vec r = row(err_mat, i);
           double temp_err = norm_inf(r);
           if(temp_err > err)
              err = temp_err;         
        }
        it++;
    }   
   return Q;    //Q = sqrt_inv(A);    P = sqrt(A);
  }  
  ///----------------------------------------------------------------------------------------- 








  ///----------------------------------------------------------------------------------------- 
  // this function reads the size of the vec which is written in the first line of the file
  int read_vec_size(const std::string& s)
  {
      std::ifstream f(s);                
      if(!f.is_open())  Error("unable to open and read file " + s);     
      int size;
      f>>size;
      f.close();  
      return size;
  }
  ///----------------------------------------------------------------------------------------- 




  ///----------------------------------------------------------------------------------------- 
  // this function reads the vec from the file for a given size, passing size as an argument
  template<typename vec>
  void read_vec(const std::string& s, std::unique_ptr<vec>& v, int size)
  {
      std::ifstream f(s);                
      if(!f.is_open())  Error("unable to open and read file " + s);     
      v = std::make_unique<vec>(size);
      for (int i=0; i<size; i++)
        f >> (*v)[i];
      f.close();  
  }
  ///----------------------------------------------------------------------------------------- 





  ///----------------------------------------------------------------------------------------- 
  // this function reads the vec and its size from the file,  size is written in the first line of the file
  template<typename vec>
  void read_size_and_vec(const std::string& s, std::unique_ptr<vec>& v, int& size)
  {
      std::ifstream f(s);                
      if(!f.is_open())  Error("unable to open and read file " + s);     
      f>>size;
      v = std::make_unique<vec>(size);
      for (int i=0; i<size; i++)
        f >> (*v)[i];
      f.close();  
  }
  ///----------------------------------------------------------------------------------------- 











  /// ---------------------------------------------------------------------------------------------------------
  //              
  //                           --------->
  //                     _____p1__________p2____
  //                  |  |
  //                  |  T1    .           .
  //                T |  |
  //                  |  |        .(T,p)
  //                  v  |
  //                    T2    .           .
  //                     |
  //                     |
          
  template<typename mat>
  void read_mat(const std::string& s, std::unique_ptr<mat>& m, int rows, int cols)
  {
      std::ifstream f(s);
      if(!f.is_open())  Error("unable to open and read file " + s);     
      m = std::make_unique<mat>(rows, cols);
      for (int i=0; i<rows; i++)
       for (int j=0; j<cols; j++)
         f >> (*m)(i,j);
      f.close();            
  }
  /// ---------------------------------------------------------------------------------------------------------








  /// ---------------------------------------------------------------------------------------------------------
  /// This function interpolates the value of pressure from p_vec depending on y position in y_vec
  template<typename vec>       
  double p_interpolation(const vec& y_vec, const vec& p_vec, double y)
  {   
    std::size_t i;
    if( *(y_vec.begin()) < *(y_vec.end()-1))   i = std::lower_bound(y_vec.begin(), y_vec.end(), y) - y_vec.begin();
    else  i = std::lower_bound(y_vec.begin(), y_vec.end(), y, std::greater<double>()) - y_vec.begin();
         
    if(i>0)  i--;
    if(i >= y_vec.size()-1)
    {
         Error("index i goes out of the vector scope");
    }
    
    const double f = (y-y_vec[i])/(y_vec[i+1]-y_vec[i]);
    const double p = f*p_vec[i+1]+(1.0-f)*p_vec[i];
    return p;
  }
  /// ---------------------------------------------------------------------------------------------------------






  /// ---------------------------------------------------------------------------------------------------------
  /// This function interpolate var(p) on equispaced vectors (y and var_table)
  /// y = wf_l_h2o_table   or  y = wf_l_co2_table
  /// consider var(p) is along x-axis and y along y-axis
  template<typename vec>       
  double linear_interpolation(double var, const vec& var_table, const vec& y, const std::string& var_name, const std::string& data_var)
  {
      const double delta(var_table[1]-var_table[0]);             // delta_p
      const auto indx = (int)floor((var-var_table[0])/delta);    // p_indx
      
      if(indx >= var_table.size() || indx < 0)
      {
          std::stringstream ss;
          ss << data_var << ": In linear_interpolation function "<<var_name <<"_index goes out of the vector scope "<<"\n"
             <<"     "<< var_name <<" = "<<var<<std::endl;                    
          Error(ss.str());                                        
      }
      
      const double slope = (y[indx+1]-y[indx])/delta;
      return y[indx] + slope*(var-var_table[indx]);
  }
  /// ---------------------------------------------------------------------------------------------------------














  /// ---------------------------------------------------------------------------------------------------------
  /**
     This is bilinear interpolation of (T,p) on equispaced vectors(row_var_table(temperature_table), col_var_table(pressure_table)) and matrix m
     m = wf_l_h2o_table   or m = wf_l_co2_table
     Format of m as well as bilinear interpolation is as follows:
                    p
                 --------->
           _____p1__________p2____
        |  |
        |  T1    .           .
      T |  |
        |  |        .(T,p)
        v  |
          T2    .           .
           |
           |
           
      mind the order (T is row-wise and the other variable is column-wise)
  **/   
  template<typename vec>
  void bilinear_interpolation_coefficients(double row_var, double col_var, const vec& row_var_table, const vec& col_var_table,
                                           const std::string& row_var_name, const std::string& row_err_msg,
                                           const std::string& col_var_name, const std::string& col_err_msg,
                                           int& r_indx, int& c_indx, double& a1, double& a2, double& a3, double& a4)            
  {
      const double delta_r(std::abs(row_var_table[1] - row_var_table[0]));  //delta_T
      r_indx = (int)floor(std::abs(row_var - row_var_table[0])/delta_r);  //T_indx  

      const double delta_c(std::abs(col_var_table[1]-col_var_table[0]));   //delta_p
      c_indx = (int)floor(std::abs(col_var-col_var_table[0])/delta_c);    //p_indx      
        
      if(c_indx >= col_var_table.size() || c_indx<0)
      {
          std::stringstream ss;
          ss << "in bilinear_interpolation function "<<col_var_name <<"_index goes out of the vector scope "<<"\n"
             <<"     "<< col_var_name <<" = "<<col_var<<std::endl;                    
          Error(ss.str());                                        
      }
      
      if(r_indx >= row_var_table.size() || r_indx<0)
      {
          std::stringstream ss;
          ss << "in bilinear_interpolation function "<<row_var_name <<"_index goes out of the vector scope "<<"\n"
             <<"     "<< row_var_name <<" = "<<row_var<<std::endl;                    
          Error(ss.str());                                        
      }
      
      const double c1 = col_var_table[c_indx];         
      const double c2 = col_var_table[c_indx+1];       
      const double r1 = row_var_table[r_indx];      
      const double r2 = row_var_table[r_indx+1];   

      a1 = (r2-row_var)*(c2-col_var)/(delta_c*delta_r);
      a2 = (r2-row_var)*(col_var-c1)/(delta_c*delta_r);
      a3 = (row_var-r1)*(c2-col_var)/(delta_c*delta_r);
      a4 = (row_var-r1)*(col_var-c1)/(delta_c*delta_r);           
  }
  /// ---------------------------------------------------------------------------------------------------------








  /// ---------------------------------------------------------------------------------------------------------
  template<typename mat>
  double bilinear_interpolated_value(int r_indx, int c_indx, double a1, double a2, double a3, double a4, const mat& m)
  {       
    return a1*m(r_indx, c_indx) + a2*m(r_indx, c_indx+1) + a3*m(r_indx+1, c_indx) + a4*m(r_indx+1, c_indx+1);       
  }
  /// ---------------------------------------------------------------------------------------------------------










  /// ---------------------------------------------------------------------------------------------------------  
  /**
     This is bilinear interpolation of (T,p) on equispaced vectors(row_var_table(temperature_table), col_var_table(pressure_table)) and matrix m
     m = wf_l_h2o_table   or m = wf_l_co2_table
     Format of m as well as bilinear interpolation is as follows:
                    p
                 --------->
           _____p1__________p2____
        |  |
        |  T1    .           .
      T |  |
        |  |        .(T,p)
        v  |
          T2    .           .
           |
           |
           
      mind the order (T is row-wise and the other variable is column-wise)
  **/   
  template<typename vec, typename mat>
  double bilinear_interpolation(double row_var, double col_var, const vec& row_var_table, const vec& col_var_table, const mat& m,
                                const std::string& row_var_name, const std::string& col_var_name, const std::string& data_var)            
  {
      const double delta_c(std::abs(col_var_table[1]-col_var_table[0]));   //delta_p
      const int c_indx = (int)floor(std::abs(col_var-col_var_table[0])/delta_c);    //p_indx
      
      const double delta_r(std::abs(row_var_table[1] - row_var_table[0]));  //delta_T
      const int r_indx = (int)floor(std::abs(row_var - row_var_table[0])/delta_r);  //T_indx  
        
      if(c_indx >= col_var_table.size() || c_indx<0)
      {
          std::stringstream ss;
          ss << data_var <<": In bilinear_interpolation function "<<col_var_name <<"_index goes out of the vector scope "<<"\n"
             <<"     "<< col_var_name <<" = "<<col_var<<std::endl;                    
          Error(ss.str());                                        
      }
      
      if(r_indx >= row_var_table.size() || r_indx<0)
      {
          std::stringstream ss;
          ss << data_var << ": In bilinear_interpolation function "<<row_var_name <<"_index goes out of the vector scope "<<"\n"
             <<"     "<< row_var_name <<" = "<<row_var<<std::endl;                    
          Error(ss.str());                                        
      }
      
      const double c1 = col_var_table[c_indx];         
      const double c2 = col_var_table[c_indx+1];       
      const double r1 = row_var_table[r_indx];      
      const double r2 = row_var_table[r_indx+1];    
      
      double mean = (r2-row_var)*(c2-col_var)*m(r_indx, c_indx) + (r2-row_var)*(col_var-c1)*m(r_indx, c_indx+1)
      + (row_var-r1)*(c2-col_var)*m(r_indx+1, c_indx) + (row_var-r1)*(col_var-c1)*m(r_indx+1, c_indx+1);
      
      mean /= delta_r*delta_c;        
      return mean;
  }
  /// ---------------------------------------------------------------------------------------------------------










  ///----------------------------------------------------------------------------------------- -------------------
  /// This function prints information of mesh motion solver 
  template<typename ls_solver_type>
  void print_solver_info(double fill_time, double solver_time, const ls_solver_type& ls_solver)
  {
      print_only_pid<0>(std::cerr) << "MESH            " << "Assembly: " << parse(fill_time,44) << "Solver: " << parse(solver_time,15)
      << "AbsResErr: " << parse(ls_solver.ARE(),15) << "RelResErr: " << parse(ls_solver.RRE(),15)
      << "Num_it: " << parse(ls_solver.num_it(),8) << "dt: " << time::get().delta_t()
      << "\n";        
  }
  ///----------------------------------------------------------------------------------------- 




  ///----------------------------------------------------------------------------------------- -------------------
  /// This function prints solver information after solving the linear system for static case
  template<typename solver_name_type, typename ls_solver_type>
  void print_solver_info(const solver_name_type& solver_type, double fill_time, double solver_time, const ls_solver_type& ls_solver)
  {
        print_only_pid<0>(std::cerr) 
        << solver_type << "Assembly: " << parse(fill_time,15) << "Solver: " << parse(solver_time,15)
        << "AbsResErr: " << parse(ls_solver.ARE(),15) << "RelResErr: "<< parse(ls_solver.RRE(),15)
        << "Num_it: " << parse(ls_solver.num_it(),8) << "dt: "<< time::get().delta_t()
        << "\n";
  }
  ///----------------------------------------------------------------------------------------- 




  ///----------------------------------------------------------------------------------------- -------------------
  /// This function prints solver information before solving the linear system 
  template<typename solver_name_type>
  void print_solver_info(const solver_name_type& solver_type, int it_count, double fill_time, double non_linear_res)
  {
     print_only_pid<0>(std::cerr) 
     << solver_type << "it: " << parse(it_count,5) << "Assembly: " << parse(fill_time,15) << "NonLinearRelRes: " << parse(non_linear_res,15) << "\n";        
  }
  ///----------------------------------------------------------------------------------------- 





  ///----------------------------------------------------------------------------------------- -------------------
  /// This function prints solver information at the end of each iteration, after solving the linear system and solution correction 
  template<typename solver_name_type, typename ls_solver_type>
  void print_solver_info(const solver_name_type& solver_type, int it_count, double fill_time, double non_linear_res, double solver_time, const ls_solver_type& ls_solver)
  {
        std::string s;     
        if(it_count == 0) s = "NonLinearAbsRes: ";
        else s = "NonLinearRelRes: ";
     
        print_only_pid<0>(std::cerr) 
        << solver_type << "it: " << parse(it_count,5) << "Assembly: " << parse(fill_time,15) << s << parse(non_linear_res,15)<< "Solver: " << parse(solver_time,15)
        << "AbsResErr: " << parse(ls_solver.ARE(),15) << "RelResErr: "<< parse(ls_solver.RRE(),15)
        << "Num_it: " << parse(ls_solver.num_it(),8) << "dt: "<< time::get().delta_t()
        << "\n";    
  }
  ///----------------------------------------------------------------------------------------- 











   ///----------------------------------------------------------------------------------------- 
   /// This function reads the matching gids of the nodes at fluid-solid interface from input/fsi_nd.txt and returns the corresponding gids in form of a map 
   auto read_matching_nd_gid(const std::string& file)
   {
     std::ifstream if_stream(file);
     if(!if_stream.is_open())
     {
       Error("unable to open and read '" + file + " '");    
     }     

     int rows;
     if_stream >> rows;
    
     int a,b;
     std::map<int,int> matching_nd_gid;    
     for(int i=0; i<rows; i++)
     {
       if_stream >> a >> b;
       matching_nd_gid[a] = b;
     }       
     if_stream.close();       
     return matching_nd_gid;
   }
  ///----------------------------------------------------------------------------------------- 
  









  ///----------------------------------------------------------------------------------------- 
  /// Here we average the v1 and v2 over the repeated nodes (2D version)
   void avg_over_repeated_nodes(
     const std::vector<int>& repeated_nds, const std::vector<double>& v1, const std::vector<double>& v2,
     std::vector<int>& unique_nds, std::vector<double>& avg_v1, std::vector<double>& avg_v2
     )
   { 
        avg_v1.resize(unique_nds.size());
        avg_v2.resize(unique_nds.size());
   
        for(int i=0; i<unique_nds.size(); i++)
        {
          double d1(0.0), d2(0.0), k(0.0);          
          for(int j=0; j<repeated_nds.size(); j++)
            if(unique_nds[i] == repeated_nds[j])
            {
                d1 += v1[j];
                d2 += v2[j];
                k++;
            }
          
          avg_v1[i] = d1/k; 
          avg_v2[i] = d2/k; 
       }   
  }
  ///----------------------------------------------------------------------------------------- 
  








  ///----------------------------------------------------------------------------------------- 
  /// Here we average v1, v2 and v3 over the repeated nodes (3D version)
   void avg_over_repeated_nodes(
     const std::vector<int>& repeated_nds, const std::vector<double>& v1, const std::vector<double>& v2, const std::vector<double>& v3,
     std::vector<int>& unique_nds, std::vector<double>& avg_v1, std::vector<double>& avg_v2, std::vector<double>& avg_v3
     )
   { 
        avg_v1.resize(unique_nds.size());
        avg_v2.resize(unique_nds.size());
        avg_v3.resize(unique_nds.size());
   
        for(int i=0; i<unique_nds.size(); i++)
        {
          double d1(0.0), d2(0.0), d3(0.0), k(0.0);          
          for(int j=0; j<repeated_nds.size(); j++)
            if(unique_nds[i] == repeated_nds[j])
            {
                d1 += v1[j];
                d2 += v2[j];
                d3 += v3[j];
                k++;
            }
          
          avg_v1[i] = d1/k; 
          avg_v2[i] = d2/k; 
          avg_v3[i] = d3/k; 
       }   
  }
  ///----------------------------------------------------------------------------------------- 



  ///----------------------------------------------------------------------------------------- 
  /// Here we average v1, v2, v3, v4, v5 and v6 over the repeated nodes (3D version)
   void avg_over_repeated_nodes(
     const std::vector<int>& repeated_nds, const std::vector<double>& v1, const std::vector<double>& v2, const std::vector<double>& v3, 
     const std::vector<double>& v4, const std::vector<double>& v5, const std::vector<double>& v6,
     std::vector<int>& unique_nds, std::vector<double>& avg_v1, std::vector<double>& avg_v2, std::vector<double>& avg_v3,
     std::vector<double>& avg_v4, std::vector<double>& avg_v5, std::vector<double>& avg_v6
     )
   { 
        avg_v1.resize(unique_nds.size());
        avg_v2.resize(unique_nds.size());
        avg_v3.resize(unique_nds.size());
        avg_v4.resize(unique_nds.size());
        avg_v5.resize(unique_nds.size());
        avg_v6.resize(unique_nds.size());
   
        for(int i=0; i<unique_nds.size(); i++)
        {
          double d1(0.0), d2(0.0), d3(0.0), d4(0.0), d5(0.0), d6(0.0);
          int k(0);          
          for(int j=0; j<repeated_nds.size(); j++)
            if(unique_nds[i] == repeated_nds[j])
            {
                d1 += v1[j];
                d2 += v2[j];
                d3 += v3[j];
                d4 += v4[j];
                d5 += v5[j];
                d6 += v6[j];
                k++;
            }
          
          avg_v1[i] = d1/k; 
          avg_v2[i] = d2/k; 
          avg_v3[i] = d3/k; 
          avg_v4[i] = d4/k; 
          avg_v5[i] = d5/k; 
          avg_v6[i] = d6/k;
       }   
  }
  ///-----------------------------------------------------------------------------------------








  ///----------------------------------------------------------------------------------------- 
  /// This function computes a map (nd_var_map) from my_nd, my_v1 and my_v2 (2D version)
  /// this is called from   sc_f_tr<2>,   sc_isothermal_f_tr<2>, mc_f_tr<2>,  mc_isothermal_f_tr<2> and mc_f_heat_flux<2>
   void all_pid_avg_over_repeated_nodes(const std::vector<int>& my_nd, const std::vector<double>& my_v1, const std::vector<double>& my_v2, std::map<int, std::vector<double>>& nd_var_map)
   {   
         /**  example demonstration for tractions on two processes
         
                            P0                                                   P1
               4       5  5       6   6       7                    7       8   8       9  
               o-------o  o-------o   o-------o                    o-------o   o-------o   
              nd1    nd2  nd1    nd2  nd1    nd2                   nd1    nd2  nd1    nd2 
        
              my_nd = [                                         my_nd = [
                         4,5,                                             7,8, 
                         5,6,                                             8,9 
                         6,7                                            ]
                      ]                          

              my_v1 = [                                         my_v1 = [
                     gn1_tx_4, gn2_tx_5,                                 gn1_tx_7, gn2_tx_8,
                     gn1_tx_5, gn2_tx_6,                                 gn1_tx_8, gn2_tx_9,
                     gn1_tx_6, gn2_tx_7,                                ]
                    ]
              
              my_v2 = [                                         my_v2 = [
                     gn1_ty_4, gn2_ty_5,                                 gn1_ty_7, gn2_ty_8,
                     gn1_ty_5, gn2_ty_6,                                 gn1_ty_8, gn2_ty_9,
                     gn1_ty_6, gn2_ty_7,                                ]
                    ]
         */


        
        auto my_unique_nd = unique_vector(my_nd);           /// my_nd has repeated enteries; we make it unique
        std::vector<double> my_avg_v1, my_avg_v2;        

        /// Here we average my_v1 and my_v2 over the repeated nodes
        avg_over_repeated_nodes(my_nd, my_v1, my_v2, my_unique_nd, my_avg_v1, my_avg_v2);
        
        /*
                    P0                                                P1
            my_unique_nd = [4,5,6,7]                             my_unique_nd = [7,8,9]
            
            my_avg_v1 = [                                           my_avg_v1 = [
                            gn1_tx_4                                              gn1_tx_7
                           (gn2_tx_5 + gn1_tx_5)/2                               (gn2_tx_8 + gn1_tx_8)/2
                           (gn2_tx_6 + gn1_tx_6)/2                                gn2_tx_9 
                            gn2_tx_7                                             ]
                         ]

            //similarly my_avg_v2

            At this point each process has its nodes and the traction forces computed at nodes.
            Now we need to combine this for all processes
        */




        MPI_Barrier(MPI_COMM_WORLD);
        int rank = get_rank();
        
        const int send_buffer_size = my_unique_nd.size();      
        int sum;
        std::vector<int> num_el_buffer, u;   
        MPI_NumElbuffer_U_Sum(send_buffer_size, num_el_buffer, u, sum);   

        /**            
                        pid =  0   1   
                       data = ----|---
              num_el_buffer =  4   3    
                 position u = 0---4   
                        sum = 7    
        */               
        

        std::vector<int> all_pid_nd(sum);
        MPI_Allgatherv(&my_unique_nd[0], num_el_buffer[rank], MPI_INT, &all_pid_nd[0], &num_el_buffer[0], &u[0], MPI_INT, MPI_COMM_WORLD);

        std::vector<double> all_pid_v1(sum);
        MPI_Allgatherv(&my_avg_v1[0], num_el_buffer[rank], MPI_DOUBLE, &all_pid_v1[0], &num_el_buffer[0], &u[0], MPI_DOUBLE, MPI_COMM_WORLD);

        std::vector<double> all_pid_v2(sum);
        MPI_Allgatherv(&my_avg_v2[0], num_el_buffer[rank], MPI_DOUBLE, &all_pid_v2[0], &num_el_buffer[0], &u[0], MPI_DOUBLE, MPI_COMM_WORLD);


        /** 
                    P0                                                                    P1
            all_pid_nd = [4, 5, 6, 7, 7, 8, 9]                                        same as P0
            all_pid_v1 = [tx_4, tx_5, tx_6, tx_7, tx_7, tx_8, tx_9]
            all_pid_v2 = [ty_4, ty_5, ty_6, ty_7, ty_7, ty_8, ty_9]                                        
        */



        auto all_pid_unique_nds = unique_vector(all_pid_nd);     /// all_pid_nd has repeated enteries; we make it unique
        std::vector<double> all_pid_avg_v1, all_pid_avg_v2;

        /// Here we average the tractions over the repeated nodes
        avg_over_repeated_nodes(all_pid_nd, all_pid_v1, all_pid_v2, all_pid_unique_nds, all_pid_avg_v1, all_pid_avg_v2);
        
       
        for(int i=0; i<all_pid_unique_nds.size(); i++)
          nd_var_map[all_pid_unique_nds[i]] = std::vector<double>{all_pid_avg_v1[i], all_pid_avg_v2[i]};
   }
  ///----------------------------------------------------------------------------------------- 









  ///----------------------------------------------------------------------------------------- 
  /// This function computes a map (nd_var_map) from my_nd, my_v1, my_v2 and my_v3 (3D version)
  /// this is called from   sc_f_tr<3>,   sc_isothermal_f_tr<3>, mc_f_tr<3>,  mc_isothermal_f_tr<3> and mc_f_heat_flux<3>
  /// this also called from sigma<2>
   void all_pid_avg_over_repeated_nodes(const std::vector<int>& my_nd, const std::vector<double>& my_v1, const std::vector<double>& my_v2, 
                        const std::vector<double>& my_v3, std::map<int, std::vector<double>>& nd_var_map)
   {   
        /**  example demonstration for traction on two processes with quadrangle sides
        
                            P0                                                        P1
            12(n4)  11(n3) 11(n4) 10(n3)  10(n4)   9(n3)                  9(n4)   8(n3)  8(n4)   7(n3) 
               o-------o    o-------o     o-------o                          o-------o    o-------o    
               |       |    |       |     |       |                          |       |    |       |    
               |       |    |       |     |       |                          |       |    |       |    
               o-------o    o-------o     o-------o                          o-------o    o-------o    
            1(n1)   2(n2)  2(n1) 3(n2)   3(n1)    4(n2)                  4(n1)    5(n2)   5(n1)    6(n2) 
        
              my_nd = [                                                my_nd = [
                         1,2,11,12                                               4,5,8,9 
                         2,3,10,11                                               5,6,7,8 
                         3,4,9,10                                              ]
                      ]                          

              my_v1 = [                                                my_v1 = [
                     gn1_tx_1, gn2_tx_2, gn3_tx_11, gn4_tx_12                   gn1_tx_4, gn2_tx_5, gn3_tx_8, gn4_tx_9
                     gn1_tx_2, gn2_tx_3, gn3_tx_10, gn4_tx_11                   gn1_tx_5, gn2_tx_6, gn3_tx_7, gn4_tx_8
                     gn1_tx_3, gn2_tx_4, gn3_tx_9, gn4_tx_10                   ]
                    ]
              
              my_v2 = [                                                my_v2 = [
                     gn1_ty_1, gn2_ty_2, gn3_ty_11, gn4_ty_12                   gn1_ty_4, gn2_ty_5, gn3_ty_8, gn4_ty_9
                     gn1_ty_2, gn2_ty_3, gn3_ty_10, gn4_ty_11                   gn1_ty_5, gn2_ty_6, gn3_ty_7, gn4_ty_8
                     gn1_ty_3, gn2_ty_4, gn3_ty_9, gn4_ty_10                   ]
                    ]
              
              my_v3 = [                                                my_v3 = [
                     gn1_tz_1, gn2_tz_2, gn3_tz_11, gn4_tz_12                   gn1_tz_4, gn2_tz_5, gn3_tz_8, gn4_tz_9
                     gn1_tz_2, gn2_tz_3, gn3_tz_10, gn4_tz_11                   gn1_tz_5, gn2_tz_6, gn3_tz_7, gn4_tz_8
                     gn1_tz_3, gn2_tz_4, gn3_tz_9, gn4_tz_10                   ]
                    ]
         */        


        auto my_unique_nd = unique_vector(my_nd);           /// my_nd has repeated enteries; we make it unique
        std::vector<double> my_avg_v1, my_avg_v2, my_avg_v3;        

        /// Here we average my_v1, my_v2 and my_v3 over the repeated nodes
        avg_over_repeated_nodes(my_nd, my_v1, my_v2, my_v3, my_unique_nd, my_avg_v1, my_avg_v2, my_avg_v3);


        /*
                    P0                                                P1
            my_unique_nd = [1,2,11,12,3,10,4,9]                     my_unique_nd = [4,5,8,9,6,7]
            
            my_avg_v1 = [                                           my_avg_v1 = [
                            gn1_tx_1                                              gn1_tx_4
                           (gn2_tx_2 + gn1_tx_2)/2                               (gn2_tx_5 + gn1_tx_5)/2
                           (gn3_tx_11 + gn4_tx_11)/2                             (gn3_tx_8 + gn4_tx_8)/2
                            gn4_tx_12                                             gn4_tx_9
                           (gn2_tx_3 + gn1_tx_3)/2                                gn2_tx_6
                           (gn3_tx_10 + gn4_tx_10)/2                              gn3_tx_7
                            gn2_tx_4                                            ]
                            gn3_tx_9                             
                       ]

            //similarly for my_avg_v2 and my_avg_v3

            At this point each process has its nodes and the traction forces computed at nodes.
            Now we need to combine this for all processes
        */




        MPI_Barrier(MPI_COMM_WORLD);
        int rank = get_rank();
        
        const int send_buffer_size = my_unique_nd.size();      
        int sum;
        std::vector<int> num_el_buffer, u;   
        MPI_NumElbuffer_U_Sum(send_buffer_size, num_el_buffer, u, sum);   

        /**            
                        pid =  0   1   
                       data = ----|---
              num_el_buffer =  4   3    
                 position u = 0---4   
                        sum = 7    
        */               


        std::vector<int> all_pid_nd(sum);
        MPI_Allgatherv(&my_unique_nd[0], num_el_buffer[rank], MPI_INT, &all_pid_nd[0], &num_el_buffer[0], &u[0], MPI_INT, MPI_COMM_WORLD);

        std::vector<double> all_pid_v1(sum);
        MPI_Allgatherv(&my_avg_v1[0], num_el_buffer[rank], MPI_DOUBLE, &all_pid_v1[0], &num_el_buffer[0], &u[0], MPI_DOUBLE, MPI_COMM_WORLD);

        std::vector<double> all_pid_v2(sum);
        MPI_Allgatherv(&my_avg_v2[0], num_el_buffer[rank], MPI_DOUBLE, &all_pid_v2[0], &num_el_buffer[0], &u[0], MPI_DOUBLE, MPI_COMM_WORLD);

        std::vector<double> all_pid_v3(sum);
        MPI_Allgatherv(&my_avg_v3[0], num_el_buffer[rank], MPI_DOUBLE, &all_pid_v3[0], &num_el_buffer[0], &u[0], MPI_DOUBLE, MPI_COMM_WORLD);

        /** 
                    P0                                                                    P1
            all_pid_nd = [4, 5, 6, 7, 7, 8, 9]                                        same as P0
            all_pid_tx = [tx_4, tx_5, tx_6, tx_7, tx_7, tx_8, tx_9]
            all_pid_ty = [ty_4, ty_5, ty_6, ty_7, ty_7, ty_8, ty_9]                                        
        */

        auto all_pid_unique_nds = unique_vector(all_pid_nd);     /// all_pid_nd has repeated enteries; we make it unique
        std::vector<double> all_pid_avg_v1, all_pid_avg_v2, all_pid_avg_v3;

        /// Here we average the tractions over the repeated nodes
        avg_over_repeated_nodes(all_pid_nd, all_pid_v1, all_pid_v2, all_pid_v3, all_pid_unique_nds, all_pid_avg_v1, all_pid_avg_v2, all_pid_avg_v3);
        
       
        for(int i=0; i<all_pid_unique_nds.size(); i++)
          nd_var_map[all_pid_unique_nds[i]] = std::vector<double>{all_pid_avg_v1[i], all_pid_avg_v2[i], all_pid_avg_v3[i]};
   }   
  ///----------------------------------------------------------------------------------------- 



  ///----------------------------------------------------------------------------------------- 
  /// This function computes a map (nd_var_map) from my_nd, my_v1, my_v2, my_v3, my_v4, my_v5 and my_v6 (3D version)
  /// this is called from sigma<3>
   void all_pid_avg_over_repeated_nodes(const std::vector<int>& my_nd, const std::vector<double>& my_v1, const std::vector<double>& my_v2, 
                        const std::vector<double>& my_v3, const std::vector<double>& my_v4, const std::vector<double>& my_v5, 
                        const std::vector<double>& my_v6, std::map<int, std::vector<double>>& nd_var_map)
   {   
        auto my_unique_nd = unique_vector(my_nd);           /// my_nd has repeated enteries; we make it unique
        
        std::vector<double> my_avg_v1, my_avg_v2, my_avg_v3, my_avg_v4, my_avg_v5, my_avg_v6;        

        /// Here we average my_v1, my_v2, my_v3, my_v4, my_v5 and my_v6 over the repeated nodes
        avg_over_repeated_nodes(my_nd, my_v1, my_v2, my_v3, my_v4, my_v5, my_v6, my_unique_nd, my_avg_v1, my_avg_v2, my_avg_v3, my_avg_v4, my_avg_v5, my_avg_v6);
      
        /*
            At this point each process has its nodes and the traction forces computed at nodes.
            Now we need to combine this for all processes
        */

        MPI_Barrier(MPI_COMM_WORLD);
        int rank = get_rank();
        
        const int send_buffer_size = my_unique_nd.size();      
        int sum;
        std::vector<int> num_el_buffer, u;   
        MPI_NumElbuffer_U_Sum(send_buffer_size, num_el_buffer, u, sum);   

        std::vector<int> all_pid_nd(sum);
        MPI_Allgatherv(&my_unique_nd[0], num_el_buffer[rank], MPI_INT, &all_pid_nd[0], &num_el_buffer[0], &u[0], MPI_INT, MPI_COMM_WORLD);

        std::vector<double> all_pid_v1(sum);
        MPI_Allgatherv(&my_avg_v1[0], num_el_buffer[rank], MPI_DOUBLE, &all_pid_v1[0], &num_el_buffer[0], &u[0], MPI_DOUBLE, MPI_COMM_WORLD);

        std::vector<double> all_pid_v2(sum);
        MPI_Allgatherv(&my_avg_v2[0], num_el_buffer[rank], MPI_DOUBLE, &all_pid_v2[0], &num_el_buffer[0], &u[0], MPI_DOUBLE, MPI_COMM_WORLD);

        std::vector<double> all_pid_v3(sum);
        MPI_Allgatherv(&my_avg_v3[0], num_el_buffer[rank], MPI_DOUBLE, &all_pid_v3[0], &num_el_buffer[0], &u[0], MPI_DOUBLE, MPI_COMM_WORLD);

        std::vector<double> all_pid_v4(sum);
        MPI_Allgatherv(&my_avg_v4[0], num_el_buffer[rank], MPI_DOUBLE, &all_pid_v4[0], &num_el_buffer[0], &u[0], MPI_DOUBLE, MPI_COMM_WORLD);
        
        std::vector<double> all_pid_v5(sum);
        MPI_Allgatherv(&my_avg_v5[0], num_el_buffer[rank], MPI_DOUBLE, &all_pid_v5[0], &num_el_buffer[0], &u[0], MPI_DOUBLE, MPI_COMM_WORLD); 
        
        std::vector<double> all_pid_v6(sum);
        MPI_Allgatherv(&my_avg_v6[0], num_el_buffer[rank], MPI_DOUBLE, &all_pid_v6[0], &num_el_buffer[0], &u[0], MPI_DOUBLE, MPI_COMM_WORLD);     
        

        auto all_pid_unique_nds = unique_vector(all_pid_nd);     /// all_pid_nd has repeated enteries; we make it unique
        
        std::vector<double> all_pid_avg_v1, all_pid_avg_v2, all_pid_avg_v3, all_pid_avg_v4, all_pid_avg_v5, all_pid_avg_v6;

        /// Here we average the tractions over the repeated nodes
        avg_over_repeated_nodes(all_pid_nd, all_pid_v1, all_pid_v2, all_pid_v3, all_pid_v4, all_pid_v5, all_pid_v6, all_pid_unique_nds, all_pid_avg_v1, all_pid_avg_v2, all_pid_avg_v3, all_pid_avg_v4, all_pid_avg_v5, all_pid_avg_v6);
        

        for(int i=0; i<all_pid_unique_nds.size(); i++)
          nd_var_map[all_pid_unique_nds[i]] = std::vector<double>{all_pid_avg_v1[i], all_pid_avg_v2[i], all_pid_avg_v3[i], all_pid_avg_v4[i], all_pid_avg_v5[i], all_pid_avg_v6[i]};
   
  } 
  ///----------------------------------------------------------------------------------------- 



} //namespace GALES

#endif
