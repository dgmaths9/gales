#ifndef _GALES_RHO_CP_KAPPA_HPP_
#define _GALES_RHO_CP_KAPPA_HPP_






namespace GALES {




  template<int dim>
  class Rho_cp_kappa
  {    
    public:
           
    
    explicit Rho_cp_kappa(Epetra_Map& map, read_setup& setup) : sec_io_(map) 
    {
       if(setup.print_rho_cp_kappa())
       {
           make_dirs("results/sec_dofs/rho");   
           make_dirs("results/sec_dofs/cp");   
           make_dirs("results/sec_dofs/kappa");   
       }           
    }                       
            
            
            
    template<typename T1, typename T2>        
    void execute(model<dim>& model, T1& props, T2& ic_bc) 
    {
       std::vector<double> rho, cp, kappa;
       
       for(const auto& nd : model.mesh().nodes())
       {
          rho.push_back(props.rho(nd->get_y()));
          cp.push_back(props.cp(nd->get_y()));
          kappa.push_back(props.kappa(nd->get_y()));
       }
              
        print_var("rho", rho);              
        print_var("cp", cp);              
        print_var("kappa", kappa);              
    }
    


     /// This function writes the vector "v" in parallel in directory "results/solid/{s}_{time}"
     void print_var(const std::string& s, const std::vector<double>& v)
     {
       std::stringstream ss;
       ss<<"results/sec_dofs/"<<s<<"/"<<time::get().t();
       sec_io_.write2(ss.str(), v);  	
     }


     private:     
     IO sec_io_;      
  }; 







}//namespace GALES
#endif
