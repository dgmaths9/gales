#ifndef _GALES_RHO_E_NU_HPP_
#define _GALES_RHO_E_NU_HPP_






namespace GALES {




  template<int dim>
  class Rho_E_Nu
  {    
    public:
           
           
    explicit Rho_E_Nu(Epetra_Map& map, read_setup& setup) : sec_io_(map) 
    {
       if(setup.print_rho_E_nu())
       {
           make_dirs("results/solid/rho");   
           make_dirs("results/solid/E");   
           make_dirs("results/solid/nu");   
       }    
    }                       
            
            
    void execute(model<dim>& model, solid_properties& props) 
    {
       std::vector<double> rho, E, nu;
       
       for(const auto& nd : model.mesh().nodes())
       {
         if(dim == 2) props.properties(nd->get_x(), nd->get_y());
         else if(dim == 3) props.properties(nd->get_x(), nd->get_y(), nd->get_z());

          rho.push_back(props.rho());
          E.push_back(props.E());
          nu.push_back(props.nu());
       }
              
        print_var("rho", rho);              
        print_var("E", E);              
        print_var("nu", nu);              
    }
    


     /// This function writes the vector "v" in parallel in directory "results/solid/{s}_{time}"
     void print_var(const std::string& s, const std::vector<double>& v)
     {
       std::stringstream ss;
       ss<<"results/solid/"<<s<<"/"<<time::get().t();
       sec_io_.write2(ss.str(), v);  	
     }


     private:     
     IO sec_io_;      
  }; 







}//namespace GALES
#endif
