#ifndef PP_HPP
#define PP_HPP



#include <vector>
#include "boost/numeric/ublas/vector.hpp"



namespace GALES{


  /**
       This file computes hydrostatic pressure profile   
  */ 


  typedef boost::numeric::ublas::vector<double> v_type;







  /// This class computes hydrostatic pressure profile for monocoponent fluid
  template<typename ic_bc_type, int dim>
  class pressure_profile_sc
  {
      using model_type = model<dim>;
      using nd_type = node<dim>;

      public:
   
      pressure_profile_sc(ic_bc_type& ic_bc, model_type& f_model, fluid_properties& props): 
      ic_bc_(ic_bc),
      state_(f_model.state()),  
      props_(props),
      mesh_(f_model.mesh()),
      total_steps_(ic_bc.total_steps()),
      h_(ic_bc.h()),
      max_(ic_bc.max()),
      isothermal_(f_model.setup().isothermal())
      {  
         if(!ic_bc_.central_pressure_profile() && !ic_bc_.x_dependent_pressure_profile())  return;   

         v_type y_vec, p_vec;         
         if(ic_bc_.central_pressure_profile()) correct(0.0, y_vec, p_vec);                ///This is when pressure is same at a horizontal level but very only vertically
         
                  
         for(const auto& nd : mesh_.nodes())
         {
             if(ic_bc_.x_dependent_pressure_profile())                                      ///This is when pressure varies at a horizontal and vertical levels
             {
                 const double x(nd->get_x());
                 correct(x, y_vec, p_vec);
             }

            const double y(nd->get_y());
            const double p_interpolated = p_interpolation(y_vec, p_vec, y);            
            state_.set_dof(nd->first_dof_lid(), p_interpolated);
         }         
      }

      
      private:
  

      void correct(double x, v_type& y_vec, v_type& p_vec)
      {
        p_vec.resize(total_steps_+1), y_vec.resize(total_steps_+1);
        v_type T(total_steps_+1,0.0), T1(total_steps_+1,0.0);
  
        for(int i=0; i<=total_steps_; i++)
        {
          y_vec[i]= max_ - h_*i;

          pt_.set_x(x);          
          pt_.set_y(y_vec[i]);          
          nd_type nd1(pt_);
          T[i] = ic_bc_.initial_T(nd1);

          pt_.set_y(y_vec[i]- h_*0.5);          
          nd_type nd2(pt_);
          T1[i] = ic_bc_.initial_T(nd2);
        }


        // Runge kutta
        double m1,m2,m3,m4;
        p_vec[0] = ic_bc_.p0();
 
        if(isothermal_)
        {
           for(int i=0; i<total_steps_; i++)
           {
             props_.properties(p_vec[i],0.0);
             m1 = 9.81*props_.rho();
             props_.properties(p_vec[i]+0.5*h_*m1,0.0);
             m2 = 9.81*props_.rho();
             props_.properties(p_vec[i]+0.5*h_*m2,0.0);
             m3 = 9.81*props_.rho();          
             props_.properties(p_vec[i]+h_*m3,0.0);
             m4 = 9.81*props_.rho();          
             p_vec[i+1] = p_vec[i]+h_/6.*(m1+2.0*(m2+m3)+m4);
           }                
        }
        else
        {
           for(int i=0; i<total_steps_; i++)
           {
             props_.properties(p_vec[i],T[i],0.0);
             m1 = 9.81*props_.rho();
             props_.properties(p_vec[i]+0.5*h_*m1,T1[i],0.0);
             m2 = 9.81*props_.rho();
             props_.properties(p_vec[i]+0.5*h_*m2,T1[i],0.0);
             m3 = 9.81*props_.rho();          
             props_.properties(p_vec[i]+h_*m3,T[i+1],0.0);
             m4 = 9.81*props_.rho();          
             p_vec[i+1] = p_vec[i]+h_/6.*(m1+2.0*(m2+m3)+m4);
           }        
        }        
      }
  
              
      ic_bc_type& ic_bc_;
      dof_state& state_;
      fluid_properties& props_;
      Mesh<dim>& mesh_;
      int total_steps_;
      double h_;
      double max_;
      point<dim> pt_;
      bool isothermal_;               
   };















  /// This class computes hydrostatic pressure profile for multi fluid
  template<typename ic_bc_type, int dim>
  class pressure_profile_mc
  {
      using model_type = model<dim>;
      using nd_type = node<dim>;

      public:
   
      pressure_profile_mc(ic_bc_type& ic_bc, model_type& f_model, fluid_properties& props): 
      ic_bc_(ic_bc),
      state_(f_model.state()),  
      props_(props),
      mesh_(f_model.mesh()),
      total_steps_(ic_bc.total_steps()),
      h_(ic_bc.h()),
      max_(ic_bc.max()),
      isothermal_(f_model.setup().isothermal())
      {              
         if(!ic_bc_.central_pressure_profile() && !ic_bc_.x_dependent_pressure_profile())  return;   

         v_type y_vec, p_vec;         
         if(ic_bc_.central_pressure_profile())   correct(0.0, y_vec, p_vec);    ///This is when pressure is same at a horizontal level but very only vertically along x=0.0
                  
         for(const auto& nd : mesh_.nodes())
         {
             if(ic_bc_.x_dependent_pressure_profile())                    ///This is when pressure varies at a horizontal and vertical levels
             {
                 const double x(nd->get_x());
                 correct(x, y_vec, p_vec);
             }

            const double y(nd->get_y());
            const double p_interpolated = p_interpolation(y_vec, p_vec, y);
            state_.set_dof(nd->first_dof_lid(), p_interpolated);
         }
      }

      
      private:
    
  
      void correct(double x, v_type& y_vec, v_type& p_vec)
      {
        p_vec.resize(total_steps_+1), y_vec.resize(total_steps_+1);
        v_type T(total_steps_+1,0.0), T1(total_steps_+1,0.0);
        std::vector<v_type> A(total_steps_+1), A1(total_steps_+1);
  
        for(int i=0; i<=total_steps_; i++)
        {
          y_vec[i]= max_ - h_*i;

          pt_.set_x(x);          
          pt_.set_y(y_vec[i]);          
          nd_type nd1(pt_);
          T[i] = ic_bc_.initial_T(nd1);
          A[i].resize(props_.nb_comp());
          ic_bc_.initial_Y(nd1,A[i]);
          A[i][props_.nb_comp()-1] = 1.0- std::accumulate(&A[i][0],&A[i][props_.nb_comp()-1],0.0);

          pt_.set_y(y_vec[i]- h_*0.5);          
          nd_type nd2(pt_);
          T1[i] = ic_bc_.initial_T(nd2);
          A1[i].resize(props_.nb_comp());
          ic_bc_.initial_Y(nd2,A1[i]);
          A1[i][props_.nb_comp()-1] = 1.0- std::accumulate(&A1[i][0],&A1[i][props_.nb_comp()-1],0.0);
        }

        // Runge kutta
        double m1,m2,m3,m4;
        p_vec[0] = ic_bc_.p0();
 
        if(isothermal_)
        {
           for(int i=0; i<total_steps_; i++)
           {
             props_.properties(p_vec[i],A[i],0.0);
             m1 = 9.81*props_.rho();
             props_.properties(p_vec[i]+0.5*h_*m1,A1[i],0.0);
             m2 = 9.81*props_.rho();
             props_.properties(p_vec[i]+0.5*h_*m2,A1[i],0.0);
             m3 = 9.81*props_.rho();          
             props_.properties(p_vec[i]+h_*m3,A[i+1],0.0);
             m4 = 9.81*props_.rho();          
             p_vec[i+1] = p_vec[i]+h_/6.*(m1+2.0*(m2+m3)+m4);
           }                
        }
        else
        {
           for(int i=0; i<total_steps_; i++)
           {
             props_.properties(p_vec[i],T[i],A[i],0.0);
             m1 = 9.81*props_.rho();
             props_.properties(p_vec[i]+0.5*h_*m1,T1[i],A1[i],0.0);
             m2 = 9.81*props_.rho();
             props_.properties(p_vec[i]+0.5*h_*m2,T1[i],A1[i],0.0);
             m3 = 9.81*props_.rho();          
             props_.properties(p_vec[i]+h_*m3,T[i+1],A[i+1],0.0);
             m4 = 9.81*props_.rho();          
             p_vec[i+1] = p_vec[i]+h_/6.*(m1+2.0*(m2+m3)+m4);
           }        
        }        
      }
    
      ic_bc_type& ic_bc_;
      dof_state& state_;
      fluid_properties& props_;
      Mesh<dim>& mesh_;
      int total_steps_;
      double h_;
      double max_;
      point<dim> pt_;
      bool isothermal_;               
              
   };





    
}    
    
#endif    

