#ifndef ADAPTIVE_TIME_HPP
#define ADAPTIVE_TIME_HPP



#include <vector>
#include "boost/numeric/ublas/vector.hpp"


namespace GALES{


        
    /**
        This file defines the functions for adaptive time step 
    */
    
    
      void set_dt(read_setup& setup, const std::vector<double>& dt_vec)
      {
         auto it = min_element(std::begin(dt_vec), std::end(dt_vec));  
         double dt_pid = *it;                  
         double dt_min(0.0); 
         MPI_Allreduce(&dt_pid, &dt_min, 1, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);

         auto dt = time::get().delta_t();         
         if(dt_min >  setup.increment_rate()*dt)  dt = setup.increment_rate()*dt;            //Bound maximum increase in dt by increment rate
         else dt = dt_min;
                                  
         dt = std::max(std::min(dt, setup.dt_max()), setup.dt_min());     
         time::get().delta_t(dt);      
      }






    
        

     //------------------------------- criterion for sc and mc fluid ----------------------------------------------------------------            
      template<int dim>
      void adaptive_time_f_sc_mc(model<dim>& f_model, fluid_properties& props, read_setup& setup)
      {
         using vec = boost::numeric::ublas::vector<double>;
         const double CFL = setup.CFL();
         const int nb_comp = props.nb_comp();
         
         const auto& mesh(f_model.mesh());
         vec dofs_fluid;
         point<dim> pt;
         std::vector<double> dt_vec;
         const double tol = std::numeric_limits<double>::epsilon();
         double shear_rate(0.0);           
         
         for(const auto& el : mesh.elements())         
         {
           const int nb_dofs = el->nb_dofs();
           vec I(nb_dofs, 0.0), dI_dx(nb_dofs, 0.0), dI_dy(nb_dofs, 0.0), dI_dz(nb_dofs, 0.0);
           
           f_model.extract_element_dofs(*el, dofs_fluid);

           std::vector<double> dt_gp_vec;
           dt_gp_vec.reserve(el->nb_gp());
           for(auto& quad_ptr : el->quad_i())       // here we loop over each gauss point of the element and compute dt        
           {             
             quad_ptr->interpolate(dofs_fluid, I);
             quad_ptr->x_derivative_interpolate(dofs_fluid, dI_dx);
             quad_ptr->y_derivative_interpolate(dofs_fluid, dI_dy);
             if(dim==3) quad_ptr->z_derivative_interpolate(dofs_fluid, dI_dz);
             
             boost::numeric::ublas::matrix<double> del_v(dim,dim,0.0);             
             if(dim==2)
             {
               for(int i=0; i<2; i++)
               {
                  del_v(i,0) = dI_dx[1+i];       del_v(i,1) = dI_dy[1+i];     
               }
             }  
             else if (dim==3)
             {
               for(int i=0; i<3; i++)
               {
                  del_v(i,0) = dI_dx[1+i];       del_v(i,1) = dI_dy[1+i];     del_v(i,2) = dI_dz[1+i];
               }
             }
             

             const double p = I[0];           
             vec v(dim);
             for(int i=0; i<dim; i++)
              v[i] = I[1+i];

             const double v_nrm = boost::numeric::ublas::norm_2(v);
             if(v_nrm < tol)
             {
               dt_gp_vec.push_back(2.0*time::get().delta_t());	
             }
             else
             {
               auto alpha = quad_ptr->alpha(v, del_v, pt);
               auto h = quad_ptr->compute_h(alpha, pt);
               
//               double h = quad_ptr->compute_h(pt);
               
               double v_tau = v_nrm;

               if(nb_comp==1)
               {
                   if(I.size()==3)   props.properties(p, shear_rate);               // fluid_sc  isothermal                      
                   else if (I.size()==4)   props.properties(p, I[I.size()-1], shear_rate);   // fluid_sc
               }
               else if(nb_comp>1)
               {
                   boost::numeric::ublas::vector<double> Y(nb_comp, 0.0);
                   for(int i=0; i<nb_comp-1; i++)
                     Y[i] = I[I.size()-(nb_comp-1) +i];
                   Y[nb_comp-1] = 1.0 - std::accumulate(&Y[0], &Y[nb_comp-1], 0.0);
                   
                   if(I.size()-(nb_comp-1)==3) props.properties(p, Y, shear_rate);           //fluid_mc isothermal
                   else if (I.size()-(nb_comp-1)==4) props.properties(p, I[I.size()-(nb_comp-1)-1], Y, shear_rate);  // fluid_mc
               }                   
               const double c = props.sound_speed();
               const double Mach = v_nrm/c;
               if(Mach > 0.3)
               {
                 if(dim==2) v_tau = sqrt(v_nrm*v_nrm + 1.5*c*c + c*sqrt(16.0*v_nrm*v_nrm + c*c));
                 else v_tau = sqrt(v_nrm*v_nrm + 2.0*c*c + c*sqrt(4.0*v_nrm*v_nrm + c*c));
               }

               if(setup.neglect_diffusion())
               {
                  dt_gp_vec.push_back(CFL*h/v_tau);
               }
               else
               {
                  const double rho = props.rho();
                  const double mu = props.mu();
                  const double kappa = props.kappa();
                  const double cv = props.cv();
                        
                  double use(0.0);    
                  if(f_model.setup().isothermal())  use = 2.0*mu/(rho*h*h) + v_tau/h; 
                  else   use = 2.0*std::max(mu/(rho*h*h), kappa/(rho*cv*h*h)) + v_tau/h;     
                                                                
                  dt_gp_vec.push_back(CFL/use);
               }   
             }
           }
           auto it = min_element(std::begin(dt_gp_vec), std::end(dt_gp_vec));  
           dt_vec.push_back(*it);                  // here we compute the minimum dt on all gauss points of the element and put that in  dt_vec
         }                 
         set_dt(setup, dt_vec); 
      }
      
      //-----------------------------------------------------------------------------------------------      





      











     //------------------------------- criterion for heat equation ----------------------------------------------------------------            
      template<int dim>
      void adaptive_time_heat_eq(model<dim>& h_model, heat_eq_properties& props, read_setup& setup)
      {
         const double CFL = setup.CFL();         
         point<dim> pt;
         std::vector<double> dt_vec;
                  
         for(const auto& el : h_model.mesh().elements())         
         {
           std::vector<double> dt_gp_vec;
           dt_gp_vec.reserve(el->nb_gp());

           const double h = el->min_h();
           for(auto& quad_ptr : el->quad_i())       // here we loop over each gauss point of the element and compute dt        
           {             
               const double rho = props.rho();
               const double cp = props.cp();
               const double kappa = props.kappa();               
               const double use = 2.0*kappa/(rho*cp*h*h);                                                                 
               dt_gp_vec.push_back(CFL/use);                          
           }
           auto it = min_element(std::begin(dt_gp_vec), std::end(dt_gp_vec));  
           dt_vec.push_back(*it);                  // here we compute the minimum dt on all gauss points of the element and put that in  dt_vec
         }                 

         set_dt(setup, dt_vec); 
      }
      
      //-----------------------------------------------------------------------------------------------      











     //------------------------------- criterion for solid_ed ----------------------------------------------------------------            
      template<int dim>
      void adaptive_time_solid_ed(model<dim>& s_model, solid_properties& props, read_setup& setup)
      {
         const double CFL = setup.CFL();         
         point<dim> pt;
         std::vector<double> dt_vec;
                  
         for(const auto& el : s_model.mesh().elements())         
         {
           std::vector<double> dt_gp_vec;
           dt_gp_vec.reserve(el->nb_gp());

           const double h = el->min_h();
           for(auto& quad_ptr : el->quad_i())       // here we loop over each gauss point of the element and compute dt        
           {             
               const double rho = props.rho();
               const double E = props.E();
               
               const double use = 2.0*sqrt(E/rho)/h;                                                                 
               dt_gp_vec.push_back(CFL/use);                          
           }
           auto it = min_element(std::begin(dt_gp_vec), std::end(dt_gp_vec));  
           dt_vec.push_back(*it);                  // here we compute the minimum dt on all gauss points of the element and put that in  dt_vec
         }                 

         set_dt(setup, dt_vec); 
      }
      
      //-----------------------------------------------------------------------------------------------      






         
    
    

}    
    

#endif


