#ifndef SIGMA_HPP
#define SIGMA_HPP




#include "boost/numeric/ublas/vector.hpp"
#include <numeric>
#include <algorithm>
#include <vector>
#include <map>



namespace GALES{

  template<int dim> class sigma{};
  
  
  template<>
  class sigma<2>
  {
    using vec = boost::numeric::ublas::vector<double>;
    using mat = boost::numeric::ublas::matrix<double>;

    public: 
    
    void sgm
    (
      model<2>& u_model,
      model<2>& T_model,
      solid_properties& props, 
      std::map<int, std::vector<double> >& s_nd_sgm
    )
    {
        std::vector<int> my_nd;
        std::vector<double> my_sgm11, my_sgm22, my_sgm12;

        vec dofs_solid, dofs_T;        
        vec thermal_strain(3,0.0);
        double I_T(0.0);

        point<2> dummy;
        
        nu_ = props.nu();
        E_ = props.E();
        alpha_ = props.alpha();
        T_ref_ = props.T_ref();
        condition_type_ = props.condition_type();

              
        /// We first collect the nd gids and the sigma vectors at gauss nodes of each element at each process
        for(auto& el : u_model.mesh().elements())
        {
          u_model.extract_element_dofs(el->lid(), dofs_solid);
          T_model.extract_element_dofs(el->lid(), dofs_T);
          nb_el_nodes_ = el->nb_nodes();            
          B_.resize(3,nb_el_nodes_*2,0.0);
          D_.resize(3,3, 0.0);

          for(int i=0; i<el->quad_i().size(); i++)
          {
             quad_ptr_ = el->quad_i()[i];
             B();
             D();
             vec mechanical_strains = prod(B_,dofs_solid);
             vec mechanical_stresses = prod(D_,mechanical_strains);
             quad_ptr_->interpolate(dofs_T,I_T);
             thermal_strain[0] = alpha_*(I_T-T_ref_);
             thermal_strain[1] = alpha_*(I_T-T_ref_);
             vec thermal_stresses = prod(D_,thermal_strain);
             vec stresses = mechanical_stresses - thermal_stresses;
             my_nd.push_back(el->el_node_vec(i)->gid());
             my_sgm11.push_back(stresses[0]);
             my_sgm22.push_back(stresses[1]);
             my_sgm12.push_back(stresses[2]);
           }
         }
           
        ///This function collects the nds and sigmas from each pid and average over the repeated nodes, and fill s_nd_sgm
        all_pid_avg_over_repeated_nodes(my_nd, my_sgm11, my_sgm22, my_sgm12, s_nd_sgm);
      }


  void D()
  {
    D_.clear();
    const double mu = 0.5*E_/(1.0 + nu_);    
    double lambda(0.0);
    if(condition_type_ == "Plain_Strain" || condition_type_ == "Axisymmetric")  lambda = E_*nu_/((1.0+nu_)*(1.0-2.0*nu_));
    else if(condition_type_ == "Plain_Stress")  lambda = E_*nu_/(1.0-nu_*nu_);
    
    D_(0,0) = lambda + 2.0*mu;
    D_(0,1) = lambda;
    D_(1,0) = lambda;
    D_(1,1) = lambda + 2.0*mu;
    D_(2,2) = mu;        

    if(condition_type_ == "Axisymmetric")
    {         
      D_(3,0) = lambda;
      D_(0,3) = lambda;
      D_(3,1) = lambda;
      D_(1,3) = lambda;
      D_(3,3) = lambda + 2.0*mu;
    } 
  }

   
   void B()
   {
    B_.clear();        
    for(int i=0; i<nb_el_nodes_; i++)
    {
       B_(0,2*i) = quad_ptr_->dsh_dx(i);
       B_(1,2*i+1) = quad_ptr_->dsh_dy(i);
       B_(2,2*i)   = quad_ptr_->dsh_dy(i);
       B_(2,2*i+1) = quad_ptr_->dsh_dx(i);
    }   
    if(condition_type_ == "Axisymmetric")
    {         
      for(int i=0; i<nb_el_nodes_; i++)
         B_(3,2*i) = quad_ptr_->sh(i)/quad_ptr_->x();
    }
   }





   private:
   
   int nb_el_nodes_;
   std::shared_ptr<quad> quad_ptr_ = nullptr;
   double nu_, E_, alpha_, T_ref_;
   std::string condition_type_;
   mat D_, B_;

  }; 


  
  template<>
  class sigma<3>
  {
    using vec = boost::numeric::ublas::vector<double>;
    using mat = boost::numeric::ublas::matrix<double>;

    public: 
    
    void sgm
    (
      model<3>& u_model,
      model<3>& T_model,
      solid_properties& props, 
      std::map<int, std::vector<double> >& s_nd_sgm
    )
    {
        std::vector<int> my_nd;
        std::vector<double> my_sgm11, my_sgm22, my_sgm33, my_sgm12, my_sgm23, my_sgm13;

        vec dofs_solid, dofs_T;
        vec thermal_strain(6,0.0);
        double I_T(0.0);
        
        point<3> dummy;
        
        nu_ = props.nu();
        E_ = props.E();
        alpha_ = props.alpha();
        T_ref_ = props.T_ref();
        
        
        /// We first collect the nd gids and the sigma vectors at gauss nodes of each element at each process
        for(auto& el : u_model.mesh().elements())
        {
          u_model.extract_element_dofs(el->lid(), dofs_solid);
          T_model.extract_element_dofs(el->lid(), dofs_T);
          nb_el_nodes_ = el->nb_nodes();            
          B_.resize(6,nb_el_nodes_*3, false);
          D_.resize(6,6, false);

          
          for(int i=0; i<el->quad_i().size(); i++)
          {
             quad_ptr_ = el->quad_i()[i];
             B();
             D();
             vec mechanical_strains = prod(B_,dofs_solid);
             vec mechanical_stresses = prod(D_,mechanical_strains);
             quad_ptr_->interpolate(dofs_T,I_T);
             thermal_strain[0] = alpha_*(I_T-T_ref_);
             thermal_strain[1] = alpha_*(I_T-T_ref_);
             thermal_strain[2] = alpha_*(I_T-T_ref_);
             vec thermal_stresses = prod(D_,thermal_strain);
             vec stresses = mechanical_stresses - thermal_stresses;
             my_nd.push_back(el->el_node_vec(i)->gid());
             my_sgm11.push_back(stresses[0]);
             my_sgm22.push_back(stresses[1]);
             my_sgm33.push_back(stresses[2]);
             my_sgm12.push_back(stresses[3]);
             my_sgm23.push_back(stresses[4]);
             my_sgm13.push_back(stresses[5]);
           }
         }
           
        ///This function collects the nds and sigmas from each pid and average over the repeated nodes, and fill s_nd_sgm
        all_pid_avg_over_repeated_nodes(my_nd, my_sgm11, my_sgm22, my_sgm33, my_sgm12, my_sgm23, my_sgm23, s_nd_sgm);
      }



  void D()
  {
    D_.clear();

    const double mu = 0.5*E_/(1.0 + nu_);    
    const double lambda = E_*nu_/((1.0+nu_)*(1.0-2.0*nu_));
    
    D_(0,0) = lambda + 2.0*mu;
    D_(0,1) = lambda;
    D_(0,2) = lambda;
    D_(1,0) = lambda;
    D_(1,1) = lambda + 2.0*mu;
    D_(1,2) = lambda;
    D_(2,0) = lambda;
    D_(2,1) = lambda;
    D_(2,2) = lambda + 2.0*mu;        
    D_(3,3) = mu;
    D_(4,4) = mu;
    D_(5,5) = mu;
  }

   void B()
   {
    B_.clear();
    for(int i=0; i<nb_el_nodes_; i++)
    {
       B_(0,3*i) = quad_ptr_->dsh_dx(i);
       B_(1,3*i+1) = quad_ptr_->dsh_dy(i);
       B_(2,3*i+2) = quad_ptr_->dsh_dz(i);          
       B_(3,3*i)   = quad_ptr_->dsh_dy(i);
       B_(3,3*i+1) = quad_ptr_->dsh_dx(i);
       B_(4,3*i+1) = quad_ptr_->dsh_dz(i);
       B_(4,3*i+2) = quad_ptr_->dsh_dy(i);     
       B_(5,3*i)   = quad_ptr_->dsh_dz(i);
       B_(5,3*i+2) = quad_ptr_->dsh_dx(i);
    }   
   }

   private:
   
   int nb_el_nodes_;
   std::shared_ptr<quad> quad_ptr_ = nullptr;
   double nu_, E_, alpha_, T_ref_;
   mat D_, B_;

  }; 



}
#endif
