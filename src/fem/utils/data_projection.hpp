#ifndef _GALES_DATA_PROJECTION_HPP_
#define _GALES_DATA_PROJECTION_HPP_






namespace GALES {



  /*
       This class implements simple data projection from old mesh to new mesh;
       new_mesh must be a subset or equal to the old mesh;
       new_mesh may be serial or distributed by gales_mesh_reader;
       in the latter case, new_mesh is already partitioned; Each process stores its owned part of the mesh elements and mesh nodes
       
       First of all we have to find for each new_mesh node its location (cell) in the old_mesh. For this we read the entire old_mesh on each process.
       To ease the search procedure we divide all old mesh domain into 4 blocks; Each block contains 100 bins
       Each element must correspond to a unique bin. A bin is composed on multiple elemnts.
       An element side <= bin_length 
  */




  template<int dim>
  class data_projection
  {    
    public:
    
    data_projection(const std::string& old_mesh_name, const std::string& old_sol_f_name, const Mesh<dim>& new_mesh, std::vector<double>& new_sol)
    {
          //read old solution data
          read_old_mesh_data(old_sol_f_name);


          // we first create a mesh object and read the old mesh with all processes have info about all elements and all nodes                        
          read_mesh(old_mesh_name, old_mesh_); 
          

          // to do a very first indication about the location of a point we make bin blocks
          make_bin_blocks();


          // here we split the old mesh into different bins so that each element belongs to a unique bin 
          // we divide the domain into 10x10 = 100 uniform bins    
          make_bins();
          
          new_sol.resize(new_mesh.nodes());          
          for(int i=0; i<new_mesh.nodes().size(); i++)
          {                      
             auto nd = new_mesh.nodes()[i];
             nd_x_ = nd->get_x();   
             nd_y_ = nd->get_y();   
             auto value = project();
             new_sol[i] = value;             
          }
    }       
    
    
    
    
    
    
    void read_old_mesh_data(const std::string& old_sol_f_name)
    {
       read_bin(old_sol_f_name, old_sol_);
    }



    
    void read_mesh(const std::string& mesh_name, Mesh<dim>& mesh)
    {
          std::ifstream infile(mesh_name);                              
          if(!infile.is_open())
          {
             Error("unable to open and read mesh file");
          }
          

          custom_gales_mesh_reader<dim> reader;


          print_only_pid<0>(std::cerr)<<"--------------------Reading old mesh--------------------------- "<<" s\n";                        
          reader.read_header(infile, mesh);
              
          double el_read_start = MPI_Wtime();
          std::vector<int> my_nodes;          
          reader.read_all_elements(infile, my_nodes, mesh, 1);
          print_only_pid<0>(std::cerr)<<"El reading took: "<< MPI_Wtime()-el_read_start<<" s\n";              
    
          
          infile.clear();
          infile.seekg(std::ios_base::beg);           ///rewind the file to read nodes
    
    
        
          double nd_read_start = MPI_Wtime();
          std::map<int, int> gid_lid_map;
          read_nodes(infile, my_nodes, mesh, gid_lid_map);
          print_only_pid<0>(std::cerr)<<"Nd reading took: "<< MPI_Wtime()-nd_read_start<<" s\n";
    
    
          infile.close();                                           // closing the mesh file after reading
    
    
          /// With this function each element has el_node_vec_ of its connected nodes; nodes know their lid and gid on respective pid
          double set_element_node_links_start = MPI_Wtime();
          set_element_node_links(my_nodes, mesh, gid_lid_map);
          print_only_pid<0>(std::cerr)<<"seting Element node links took: "<< MPI_Wtime()-set_element_node_links_start<<" s\n";
          print_only_pid<0>(std::cerr)<<"---------------------------------------------------------------- "<<" s\n";                        
    
        
          for(auto& el : mesh.elements())
             el->compute_bounds();    
    }
    
    
    
    
    
    




    void make_bin_blocks()
    {
          /* 
               B1       B3
                 (cx,cy)
               B0       B2  
          
          */
          
          bin_blocks_.resize(4);
          std::vector<int> B0 = {0,1,2,3,4,
                10,11,12,13,14,
                20,21,22,23,24,
                30,31,32,33,34,
                40,41,42,43,44};
          
          std::vector<int> B1 = {50,51,52,53,54,
                60,61,62,63,64,
                70,71,72,73,74,
                80,81,82,83,84,
                90,91,92,93,94};          

          std::vector<int> B2 = {5,6,7,8,9,
                15,16,17,18,19,
                25,26,27,28,29,
                35,36,37,38,39,
                45,46,47,48,49};

          std::vector<int> B3 = {55,56,57,58,59,
                65,66,67,68,69,
                75,76,77,78,79,
                85,86,87,88,89,
                95,96,97,98,99};

          bin_blocks_[0] = B0;
          bin_blocks_[1] = B1;
          bin_blocks_[2] = B2;
          bin_blocks_[3] = B3;
          
          double bin_blocks_cx_ = 0.5*(old_mesh_->min_x() + old_mesh_->max_x());
          double bin_blocks_cy_ = 0.5*(old_mesh_->min_y() + old_mesh_->max_y());    
    }






    
    
    
    
    void make_bins()
    {
        auto min_x = old_mesh_->min_x();  
        auto min_y = old_mesh_->min_y();  
        auto max_x = old_mesh_->max_x();  
        auto max_y = old_mesh_->max_y();  

        auto lx = (max_x - min_x)/num_bins_;
        auto ly = (max_y - min_y)/num_bins_;
        
        // first set bounds for each bin
        bin_min_x_.resize(num_bins_*num_bins_), bin_min_y_.resize(num_bins_*num_bins_), bin_max_x_.resize(num_bins_*num_bins_), bin_max_y_.resize(num_bins_*num_bins_);

        for(int i=0; i<num_bins_; i++)
         for(int j=0; j<num_bins_; j++)
         {
           bin_min_x_[num_bins_*i + j] = min_x + j*lx;
           bin_max_x_[num_bins_*i + j] = min_x + (j+1)*lx;
      
           bin_min_y_[num_bins_*i + j] = min_y + j*ly;
           bin_max_y_[num_bins_*i + j] = min_y + (j+1)*ly;
         }
                        
        bin_els_gid_.resize(num_bins_*num_bins_);   //vector of el_gids belongs to each bin                        
        for(auto& el : old_mesh_.elements())
        {
          std::vector<int> nd_bin(el->nb_nodes());   // bin number for each node of the element

          for(const auto& nd : el->el_node_vec())
            for(int i=0; i<num_bins_*num_bins_; i++)             
               if(nd->get_x() >= bin_min_x_[i] && nd->get_x() <= bin_max_x_[i])
                 if(nd->get_y() >= bin_min_y_[i] && nd->get_y() <= bin_max_y_[i])              
                   nd_bin.push_back(i);
          
          //if all nodes fall in the same bin then push el_gid to this bin
          if(std::adjacent_find(nd_bin.begin(), nd_bin.end(), std::not_equal_to<>() ) == nd_bin.end() )  //all enteries of nd_bin are same
          {
             bin_els_gid_[nd_bin[0]].push_back(el->gid());   
          }
          else //if nodes fall in different bins then we chose the maximum number of the bin
          {
             auto minmax = std::minmax_element(nd_bin.begin(), nd_bin.end());
             auto max = *minmax.second;
             bin_els_gid_[max].push_back(el->gid());             
          }                                         
        }        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    int find_bin_block_num()
    {        
        if(nd_x_ <= bin_block_cx_ && nd_y_ <= bin_block_cy_)   return 0;
        else if(nd_x_ <= bin_block_cx_ && nd_y_ >= bin_block_cy_)   return 1;
        else if(nd_x_ >= bin_block_cx_ && nd_y_ <= bin_block_cy_)   return 2;
        else if(nd_x_ >= bin_block_cx_ && nd_y_ >= bin_block_cy_)   return 3;   
        else
        {
           std::stringstream ss;
           ss<<"new mesh nd_coord  = ("<<nd_x_<<", "<<nd_y_<<")"<"\n"; 
           ss<<"The new nd is not in the range of any bin block"<<"\n"; 
           ss<<"It means the new nd is not in the bounding box of the old mesh"<<"\n"; 
           Error(ss.str());       
        }         
    }




    bool nd_in_bin(int bin_num)
    {
       bool is_nd_in_bin = false;
       if(bin_min_x_[bin_num] <= nd_x_ && nd_x_ <= bin_max_x_[bin_num])
        if(bin_min_y_[bin_num] <= nd_y_ && nd_y_ <= bin_max_y_[bin_num])
          is_nd_in_bin = true;
             
       return is_nd_in_bin;              
    }



    
    bool nd_in_el_bounds(int el_gid)
    {
       bool is_nd_in_el_bounds = false;       
       if(old_mesh_.elements()[el_gid]->min_x() <= nd_x_ && nd_x_ <= old_mesh_.elements()[el_gid]->max_x())
         if(old_mesh_.elements()[el_gid]->min_y() <= nd_y_ && nd_y_ <= old_mesh_.elements()[el_gid]->max_y())
            is_nd_in_el_bounds = true;
       return is_nd_in_el_bounds;
    }





    bool nd_in_el_and_el_in_bin(int bin_num, int& elem_gid)
    {
      // if nd is in the element, there are 3 possibilities:
      // 1. nd can be one of the vertex of the element 
      // 2. nd can lie of one of the side of the element
      // 3. nd can be inside the element
    
       bool is_el_in_bin = false;
       for(int el_gid :  bin_els_gid_[bin_num]) // loop over all elements in the bin 
         if(nd_in_el_bounds(el_gid))
           if(old_mesh_.elements()[el_gid]->is_inside(point<2>(nd_x_, nd_y_),  local_pt_))  // if nd belongs to el and they are in the same bin;  we have read all elements on all pids so lids = gids
           {
             is_el_in_bin = true;
             elem_gid = el_gid;
           }  
             
       return is_el_in_bin;              
    }







    auto neighbouring_bins(int i)
    {
      //  i+9   i+10  i+11
      //  i-1,   i,   i+1
      //  i-11  i-10  i-9

       std::vector<int> bd_l = {10,20,30,40,50,60,70,80};
       std::vector<int> bd_r = {19,29,39,49,59,69,79,89};
       std::vector<int> bd_b = {1,2,3,4,5,6,7,8};
       std::vector<int> bd_u = {91,92,93,94,95,96,97,98};
              
       std::vector<int> n_bins;
       if(i==0) n_bins = {1,10,11};  
       else if(i==9) n_bins = {8,19,18};  
       else if(i==90) n_bins = {80,91,81};  
       else if(i==99) n_bins = {98,89,88};  
       else if(is_in_vector(bd_l, i)) n_bins = {i-10, i+10, i+1, i-9, i+11};
       else if(is_in_vector(bd_r, i)) n_bins = {i-10, i+10, i-1, i-11, i+9};
       else if(is_in_vector(bd_b, i)) n_bins = {i-1, i+1, i+10, i+9, i+11};
       else if(is_in_vector(bd_u, i)) n_bins = {i-1, i+1, i-10, i-11, i-9};       
       else n_bins = {i-1, i+1, i-10, i+10, i-11, i-9, i+9, i+11};
       return n_bins;               
    }
    
    


    
    
    bool nd_in_el_and_el_in_neighbouring_bin(int bin_num, int& el_gid)
    {
       auto neighbour_bins = neighbouring_bins(bin_num);
       for(int bin : neighbour_bins)
         return nd_in_el_and_el_in_bin(bin, el_gid); 
    }
    
 


    
    auto el_dofs(int el_gid)
    {
      auto nd_gids = old_mesh_.elements()[el_gid]->node_gid_vec();
      std::vector<double> dofs(nd_gids.size());
      for(int i=0; i<nd_gids.size(); i++)
         dofs[i] = old_sol_[nd_gids[i]];
      return dofs;   
    }
    
    
    






    double interpolation(int el_gid)
    {
      auto dofs = el_dofs(el_gid); 
      
      auto xi = local_pt_.get_x();
      auto eta = local_pt_.get_y();
             
      if(old_mesh_.nb_el_nodes() == 4)
      {
        return 0.25*((1.0-xi)*(1.0-eta)*dofs[0] + (1.0+xi)*(1.0-eta)*dofs[1] + (1.0+xi)*(1.0+eta)*dofs[2] + (1.0-xi)*(1.0+eta)*dofs[3]);
      }        
      else if(old_mesh_.nb_el_nodes() == 3)
      {
        return (1.0-xi-eta)*dofs[0] + xi*dofs[1] + eta*dofs[2];
      }        
    }


    
    


   
    
    


    double project()
    {
        bool is_new_node_in_old_mesh_domain = false; 
        
        int bin_block_num = find_bin_block_num();  // find block number of node        

        for(int i=0; i<bin_blocks_[bin_block_num].size(); i++)  //loop over all bins in the block
        {
           int bin_num = bin_blocks_[bin_block_num][i]; 
           int el_gid(-1);

           if(nd_in_bin(bin_num))     
           {
              // if nd is in the bin, there are 3 possibilities:
              // 1. nd belongs to one of the elements of the current bin 
              // 2. nd belongs to one of the elements of the neighbouring bins 
              // 3. nd neither belongs to the elements of the current bin nor neighbouring bins (in this case new_node is outside the domain of the old mesh) 
              
              if(nd_in_el_and_el_in_bin(bin_num, el_gid))  // nd and el are in same bin in the same block
              {
                return interpolation(el_gid);  //interpolate data from el to node           
              }            
              else if(nd_in_el_and_el_in_neighbouring_bin(bin_num, el_gid))  // nd and el are in different bins; search el in neighbouring bins
              {
                return interpolation(el_gid);   //interpolate data from el to node
              }
              else
              {
                std::stringstream ss;
                ss<<"new_nd is not inside any old mesh element"<<"\n"; 
                ss<<"new_node is outside the domain of the old mesh" 
                Error(ss.str());              
              }              
           }
        }
    }
    
    
    
    
    
    
    
    
    
    
  



    
    
    private:

    double nd_x_, nd_y_;
    
    Mesh<2> old_mesh_;    
    std::vector<double> old_sol_; 
    int num_bins_ = 10;   // # of bins in each direction
    std::vector<double> bin_min_x_, bin_min_y_, bin_max_x_, bin_max_y_;      //bounds for each bin
    std::vector<std::vector<int>> bin_els_gid_;   //vector of el_gids belongs to each bin        
    
    double bin_blocks_cx_, bin_blocks_cy_;  // point around which blocks divison is done
    std::vector<std::vector<int>> bin_blocks_;  // bin blocks    
    
    point<2> local_pt_;
  }; 







}//namespace GALES
#endif
