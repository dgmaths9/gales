#ifndef GALES_FLUID_PROPERTIES_HPP
#define GALES_FLUID_PROPERTIES_HPP



#include "fluid_properties_reader.hpp"



namespace GALES {


 /**
      This class defined the physical properties of fluid.
      Propeties along with their configuration such as "mix_of_mix" etc. are read from an input "props.txt" by fluid_properties_reader class       
 */



class fluid_properties : public base_component
{

  using vec = boost::numeric::ublas::vector<double>;
  
  public:

  fluid_properties()
  {  
      fluid_properties_reader reader(*this);
  }



  //-------------------------------------------------------------------------------------------------------------------------------------        
  /// Deleting the copy and move constructors - no duplication/transfer in anyway
  fluid_properties(const fluid_properties&) = delete;               //copy constructor
  fluid_properties& operator=(const fluid_properties&) = delete;    //copy assignment operator
  fluid_properties(fluid_properties&&) = delete;                    //move constructor  
  fluid_properties& operator=(fluid_properties&&) = delete;         //move assignment operator 
  //-------------------------------------------------------------------------------------------------------------------------------------
  



  /// This is called from fluid_sc_isothermal
  void properties(double p, double strain_rate)final
  {
       comp_ptrs(0)->properties(p, isothermal_T_, strain_rate);
       rho_ = comp_ptrs(0)->rho();
       mu_ = comp_ptrs(0)->mu();    
       sound_speed_ = comp_ptrs(0)->sound_speed();
       beta_ = comp_ptrs(0)->beta();     
  }






  /// This is called from fluid_sc
  void properties(double p, double T, double strain_rate)final
  {
       comp_ptrs(0)->properties(p,T,strain_rate);
       rho_ = comp_ptrs(0)->rho();
       mu_ = comp_ptrs(0)->mu();    
       kappa_ = comp_ptrs(0)->kappa();     
       sound_speed_ = comp_ptrs(0)->sound_speed();
       cv_ = comp_ptrs(0)->cv();
       cp_ = comp_ptrs(0)->cp();
       alpha_ = comp_ptrs(0)->alpha();	 
       beta_ = comp_ptrs(0)->beta();     
       qL_dp_ = comp_ptrs(0)->qL_dp();	 
       qL_dT_ = comp_ptrs(0)->qL_dT();              
    
     if(Sutherland_law_)
     {
         mu_ = Sutherland_law_a_*sqrt(T*T*T)/(T+Sutherland_law_b_);
         kappa_ = mu_*cp_/0.72;   //Pr = 0.72           
     }
  }
  


   

  /// This is called from fluid_mc_isothermal
  void properties(double p, const vec &Y, double strain_rate)final
  {
       clear();
       for(int c=0; c<nb_comp_; c++)
       {
         comp_ptrs(c)->properties(p, isothermal_T_, strain_rate);
         rho_ += Y[c]/comp_ptrs(c)->rho();
         mu_ += Y[c]*comp_ptrs(c)->mu();
         chemical_diffusivity_ += Y[c]*comp_ptrs(c)->chemical_diffusivity();
         sound_speed_ += Y[c]/(pow(comp_ptrs(c)->rho(),2) * pow(comp_ptrs(c)->sound_speed(),2));

         rho_comp_[c] = comp_ptrs(c)->rho();
         chemical_diffusivity_comp_[c] = comp_ptrs(c)->chemical_diffusivity();
       }
       rho_ = 1./rho_;
       sound_speed_ = sqrt(1.0/sound_speed_);
       sound_speed_ /= rho_;     

       for (int c=0; c<nb_comp_; c++)
       {
         beta_ += Y[c]*comp_ptrs(c)->beta()*rho_/comp_ptrs(c)->rho();
       }
  }







  /// This is called from fluid_mc
  void properties(double p, double T, const vec &Y, double strain_rate)final
  {
       clear();
       for(int c=0; c<nb_comp_; c++)
       {
         comp_ptrs(c)->properties(p,T,strain_rate);
         rho_ += Y[c]/comp_ptrs(c)->rho();
         mu_ += Y[c]*comp_ptrs(c)->mu();
         cp_ += Y[c]*comp_ptrs(c)->cp();
         cv_ += Y[c]*comp_ptrs(c)->cv();
         kappa_ += Y[c]*comp_ptrs(c)->kappa();
         chemical_diffusivity_ += Y[c]*comp_ptrs(c)->chemical_diffusivity();
         qL_dp_ += Y[c]*comp_ptrs(c)->qL_dp();
         qL_dT_ += Y[c]*comp_ptrs(c)->qL_dT();
         sound_speed_ += Y[c]/(pow(comp_ptrs(c)->rho(),2) * pow(comp_ptrs(c)->sound_speed(),2));
         
         rho_comp_[c] = comp_ptrs(c)->rho();
         internal_energy_comp_[c] = comp_ptrs(c)->cv()*T;
         chemical_diffusivity_comp_[c] = comp_ptrs(c)->chemical_diffusivity();
       }
       rho_ = 1./rho_;
       sound_speed_ = sqrt(1.0/sound_speed_);
       sound_speed_ /= rho_;     

       for (int c=0; c<nb_comp_; c++)
       {
         alpha_ += Y[c]*comp_ptrs(c)->alpha()*rho_/comp_ptrs(c)->rho();
         beta_ += Y[c]*comp_ptrs(c)->beta()*rho_/comp_ptrs(c)->rho();
       }
  }









   void Sutherland_law(bool f) {Sutherland_law_ = f;}
   void Sutherland_law_a(double a) {Sutherland_law_a_ = a;}
   void Sutherland_law_b(double b) {Sutherland_law_b_ = b;}

   void isothermal_T(double b) 
   {
     isothermal_T_ = b;
     if(isothermal_T_ == 0.0)
     {
       Error("isothermal_T is set to 0.0;  it should not be zero");
     }
   }
      


   private:

   bool Sutherland_law_ = false;
   double Sutherland_law_a_ = 0.0;
   double Sutherland_law_b_ = 0.0;
    
   double isothermal_T_ = 273.0;   
   
};


}

#endif
