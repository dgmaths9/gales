#ifndef GALES_NON_NEWTONIAN_HPP
#define GALES_NON_NEWTONIAN_HPP


#include <numeric>
#include <algorithm>
#include <math.h>



namespace GALES {


/**
   This file defines calculation of shear rate and some general laws for non newtonian flows such as power law and Herschel_Bulkley model
*/



    double compute_shear_rate(const boost::numeric::ublas::vector<double>& dI_dx, const boost::numeric::ublas::vector<double>& dI_dy)
    {
       const double dv1_1(dI_dx[1]), dv2_1(dI_dx[2]);
       const double dv1_2(dI_dy[1]), dv2_2(dI_dy[2]);
       const double shear_rate = sqrt(std::pow(dv1_1,2) + std::pow(dv2_2,2) + 2*std::pow(dv1_2 + dv2_1,2) );
       return shear_rate;
    }
    
    
    double compute_shear_rate(const boost::numeric::ublas::vector<double>& dI_dx, const boost::numeric::ublas::vector<double>& dI_dy, const boost::numeric::ublas::vector<double>& dI_dz)
    {
       const double dv1_1(dI_dx[1]), dv2_1(dI_dx[2]), dv3_1(dI_dx[3]);
       const double dv1_2(dI_dy[1]), dv2_2(dI_dy[2]), dv3_2(dI_dy[3]);
       const double dv1_3(dI_dz[1]), dv2_3(dI_dz[2]), dv3_3(dI_dz[3]);
       const double shear_rate = sqrt(std::pow(dv1_1,2) + std::pow(dv2_2,2) + std::pow(dv3_3,2) + 2*std::pow(dv1_2 + dv2_1,2) + 2*std::pow(dv1_3 + dv3_1,2) + 2*std::pow(dv2_3 + dv3_2,2));
       return shear_rate;
    }
    
    
    double Power_Law(double consistency_index, double power_law_index, double shear_rate)
    {
      if(shear_rate < std::numeric_limits<double>::epsilon())  return 0.0;
      else  return consistency_index*pow(shear_rate, power_law_index-1);
    }
    
    
    
    double Herschel_Bulkley(double consistency_index, double power_law_index, double yield_stress, double critical_shear_rate, double shear_rate)
    {
      if(shear_rate < std::numeric_limits<double>::epsilon())  return 0.0;
      else if(shear_rate < critical_shear_rate)  return consistency_index*pow(critical_shear_rate, power_law_index-1) + yield_stress/critical_shear_rate;
      else  return consistency_index*pow(shear_rate, power_law_index-1) + yield_stress/shear_rate;
    }
    
    



    ///---------Einstein-Roscoe (1952) suspensions of rigid spheres objects in magma -----------------------------------
    double Einstein_Roscoe(double b, double c_vf, double mu)
    {
        return mu*pow(1.0-b*c_vf, -2.5);
    }
    
    
    
    
    ///---------Caricchi (2007) eq(3) crystal-bearing magma---------------------------------------------------------
    double Caricchi(double c_vf, double mu, double Strain_Rate)
    {
        double strain_rate = Strain_Rate;
        if(strain_rate < std::numeric_limits<double>::epsilon())  
           strain_rate = 1.e-6;
        const double phi_max = 0.066499*tanh(0.913424*log10(strain_rate) + 3.850623) + 0.591806;
        const double delta = -6.301095*tanh(0.818496*log10(strain_rate) + 2.86) + 7.462405;
        const double alpha = -0.000378*tanh(1.148101*log10(strain_rate) + 3.92) + 0.999572;
        const double gamma = 3.987815*tanh(0.8908*log10(strain_rate) + 3.24) + 5.099645;                    
        const double num = 1.0 + pow(c_vf/phi_max, delta);                    
        const double x = sqrt(3.142)*c_vf/(2*alpha*phi_max)*(1.0 + pow(c_vf/phi_max, gamma));
        const double den = pow(1.0-alpha*erf(x), 2.5*phi_max);
        double mu_r = num/den;
        return mu*mu_r;
    }
    
    
    
    
    ///--------- Ishii_Zuber (1979) non deformable, spherical bubbles bearing magma (Ca<<1) ---------------------------
    double Ishii_Zuber(double g_vf, double mu)
    {
        return mu/(1.0-g_vf);
    }
    
    
    
    
    ///---------pal 2003 4th model (eq 21) for bubble bearing magma (broad Ca range with deformable bubbles)---------------------------
    double Pal_2003(double g_vf, double mu, double shear_rate)
    {
        if(shear_rate < std::numeric_limits<double>::epsilon())  return mu;
        else
        {
            const double surface_tension = 0.36;
            const double bubble_number_density = 1.e11;
            const double max_volume_packing = 0.9999;
            const double bubble_radius = cbrt(3.0/(4*3.142*bubble_number_density));
            const double capillary_number = bubble_radius*mu*shear_rate/surface_tension;
            const double a = 2.4*pow(capillary_number,2);
            const double T = pow(1.0-a, -0.8) * pow(1.0-g_vf/max_volume_packing, -max_volume_packing);
            double x = 1.e-5;
            double F, F_prime;
            do
            {
                F = pow(x,5)/pow(1.0-a*x*x,4) - pow(T,5);
                F_prime = 5*pow(x,4)/pow(1.0-a*x*x,4) + 8*a*pow(x,6)/pow(1.0-a*x*x,5);
                x -= F/F_prime;
            }
            while(F < 1.e-9);
            return mu*x;
        }
    }






     ///---------pal 2003 4th model (eq 21) for bubble bearing magma (broad Ca range with deformable bubbles)---------------------------
     double Pal_2003_simplified(double g_vf, double mu, double shear_rate)
     {
         if(shear_rate < std::numeric_limits<double>::epsilon())  return mu;
         else
         {
             const double surface_tension = 0.36;
             const double bubble_number_density = 1.e11;
             const double max_volume_packing = 0.9999;
             const double bubble_radius = cbrt(3.0/(4*3.142*bubble_number_density));
             const double capillary_number = bubble_radius*mu*shear_rate/surface_tension;
             double mu_r(0.0);
             if(capillary_number < 1.0) { mu_r = pow(1.0-g_vf, -1.0); }     
             else  { mu_r = pow(1.0-g_vf, 5.0/3.0); }             
             return mu*mu_r;            
         }
     }


}
#endif
