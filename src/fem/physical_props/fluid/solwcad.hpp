#ifndef __SOLWCAD_HPP
#define __SOLWCAD_HPP


#include <vector>
#include <cmath>
#include <functional>
#include <sstream>




namespace GALES {



 


class solwcad
{

   using vec = std::vector<double>;


  
  public:



    solwcad(double p, double T): p_(p), T_(T)
    {
         folmax_[0] = fol_[0] + covfol_[0];
         folmin_[0] = fol_[0] - covfol_[0];
         folmax_[1] = fol_[1] + covfol_[1];
         folmin_[1] = fol_[1] - covfol_[1];    

         for(int i=0; i<10; i++)
         {
           parmax_[i] = par_[i] + covpar_[i];
           parmin_[i] = par_[i] - covpar_[i];
         }

         for(int i=0; i<20; i++)
         {
            wijmax_[i][0] = wij_[i][0] + covp_[i][0];
            wijmin_[i][0] = wij_[i][0] - covp_[i][0];
            wijmax_[i][1] = wij_[i][1] + covp_[i][1];
            wijmin_[i][1] = wij_[i][1] - covp_[i][1];
         }
    }





   static double solwcad_function1(double& x, double& a, double& b, double& c, double p)
   {
      if(x <= 1e-15) x = 1e-6; 
      double sol = log(a*p/x) - pow(1.0-x,2)*b - c;
      return sol;
   }
   
   
   
   
   static double solwcad_function2(double& x, double& a, double& b, double& c, double& d, double& e, double p, double T, double R)
   {
      double m = b + c/x + d/(x*x);
      e = 0.25*a/x;
      double sol = p - R*T*(1. + e + pow(e,2) - pow(e,3))/(x*pow(1.-e,3)) + m/(sqrt(T)*x*(x+a));
      return sol;
   }





    //igas = 2 as it computes for h20 and co2  ||  m must be equal to 0  ||  xy is output vector: (0)wt H2O dissolved, (1)wt CO2 dissolved, (2)wt H2O exsolved, (3)wt CO2 exsolved

    void execute(vec& ox, vec& xy, int m, int igas)                                               
    {
        vec ox2(nox_), x(nox_), x2(nox_), h2oco2(2), fref(2), xgg(2), xgg1(2), fvec(2), oxnew(nox_), xnew(nox_);        
                                                      
         pc_=p_;

        
         // max/min of slope (= of CO2 in gas phase)
         if(mslope_ != 0)
         {
           if(kslope_ <= 1)     fol_[0] = folmin_[0];
           else    fol_[0] = folmax_[0];
           
           if(kslope_ == 0 || kslope_==2)
           {
             fol_[1] = folmin_[1];
             for(int i=0; i<10; i++) par_[i] = parmin_[i];
           }
           else
           {
             fol_[1] = folmax_[1];
             for(int i=0; i<10; i++) par_[i] = parmax_[i];
           }
           ffref1_ = fol_[0];
           ffref2_ = fol_[1];
         }
        
        
         if(ighior_==1) 
           for(int i=0; i<nox_; i++) 
              f_[i] = f1_[i];
         else 
           for(int i=0; i<nox_; i++) 
              f_[i] = f2_[i];
        
     
         // select Ghiorso et al. 1983 or Ghiorso and Sack 1995
         for(int i=2; i<12; i++)
           for(int j=i+1; j<12; j++)
           { 
             if(ighior_==1)  wo_[j][i] = wo83_[j][i];
             else   wo_[j][i] = wo95_[j][i];
             wo_[i][j] = wo_[j][i];
           }



         if(igas==0 || p_>=1.e0 && p_<=1.e12)
         {
            igs_=igas;
            imode_=2;
            iscale_=0;
            int ligas=0, ligs=0;
            double redfac=0.8;
            tc_=T_;
                        
            clear(h2oco2);
            
            if(igas != 2)
            {
               clear(xy);
               clear(vref_);
               clear(vexc_);
               clear(gam_);
               clear(act_);
               clear(phig_);
            }
            
            
            
            for(int i=2; i<nox_; i++)
              for(int j=i+1; j<nox_; j++)
                wo_[i][j] = wo_[j][i];
    

            // re-express composition -------------------------
            int kspur=0;
            double oxtott = 0.0;
            double pln = log(p_/poco2_);
            
            comp_and_initial_guess(igas, ligas, kspur, m, ligs, oxtott, ox, ox2, x, x2, 
                                   oxnew, xnew, xy, xgg, xgg1, fvec, fref, h2oco2);
            
            pc_=p_;
            tc_=T_;
            
            scaling_unknowns(igas, ligas, kspur, m, ligs, oxtott, ox, ox2, x, 
                             x2, oxnew, xnew, xy, xgg, xgg1, fvec, fref, h2oco2);

            if(igas!=2) return;
            
            numerical_procedure(igas, ligas, kspur, m, ligs, oxtott, ox, ox2, x, 
                                x2, oxnew, xnew, xy, xgg, xgg1, fvec, fref, h2oco2);//81 continue

            set_properties(igas, x, pln, h2oco2, xy, oxtott, ligas); //70 continue 

            // normalize volatile-free composition
            
            double oxsn=0.0;
            for(int i=2; i<12; i++) oxsn += ox[i];
            for(int i=2; i<12; i++) ox[i] /= oxsn;            
         }// pressure check    if(igas==0 || (p_>=1.e0 && p_<=1.e12))
         else
         {
            std::stringstream ss;            
            ss<<"**error in calling solwcad"<<std::endl;
            ss<<"p_ = "<<p_<<" out of range"<<std::endl;
            Error(ss.str());
         }
    } 
   
   
      
   
   
   
   
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
//------------------------------End of main solwcad subroutine--------------------------------------------------------------------------



  //------------------------------Beginning of solwcad functions and sub-functions--------------------------------------------------------   
   
   
    void max_min_slope_first(int& kgam)
    {
       if(mslope_!=0)
       {
         kgam+=1;
         if(kgam==1)
         { 
           for(int i=0; i<20; i++)
             for(int j=0; j<2; j++)
               wij_[i][j] = wijmin_[i][j];    
         }
         else if(kgam==2) 
         {
           for (int i=0; i<20; i++)
           {
             wij_[i][0] = wijmin_[i][0];
             wij_[i][1] = wijmax_[i][1];
           }
         }
         else if(kgam==3)
         {
           for(int i=0; i<20; i++)
           {
             wij_[i][0] = wijmax_[i][0];
             wij_[i][1] = wijmin_[i][1];
           }
         }
         else if(kgam==4)
         {
           for(int i=0; i<20; i++)
             for(int j=0; j<2; j++)
               wij_[i][j] = wijmax_[i][j];  
         }
       }
    }






    void max_min_slope_second(vec& x, double pln, double igas)
    {    
      xwo_=0.0;
      clear(ctrm_);
      clear(ctrm2_);
      
      for(int i=2;i<nox_;i++)
      {
        ctrm_[0] = ctrm_[0] + x[i]*wij_[i-2][0];
        ctrm_[1] = ctrm_[1] + x[i]*(wij_[i-2][1] + pln*wij_[i+8][1]*ttrm_);
        ctrm2_[1] = ctrm2_[1] + x[i]*wij_[i+8][1];
        
        if(igas==0) ctrm2_[0] = ctrm2_[0] + x[i]*wij_[i-2][1];
        
        if(i<nox_-1)
          for(int j=i+1; j<nox_; j++)   
            xwo_ += x[i]*x[j]*wo_[i][j];      
      }      
    }
    
    
    
    
    
    
    void max_min_slope_third(int& kgam, int& kgamh, int& kgamc, double& gammnh, double& gammxc, double& gammxh, double& gammnc, vec& x)
    {
      double gamh2o = (1.-x[0])*(1.-x[0]-x[1])*ctrm_[0] - x[1]*(1.-x[0]-x[1])*ctrm_[1];
      double gamco2 = -x[0]*(1.-x[0]-x[1])*ctrm_[0] + (1.-x[1])*(1.-x[0]-x[1])*ctrm_[1];
      
      
      if(kslope_==0)
      {  
        if(gamh2o <= gammnh)
        {
          gammnh = gamh2o;
          kgamh = kgam;
        }
        if(gamco2 <= gammnc)
        {
          gammnc = gamco2;
          kgamc = kgam;
        }
      }
      else if(kslope_==1)
      {
        if(gamh2o <= gammnh)
        {
          gammnh = gamh2o;
          kgamh = kgam;
        }
        if(gamco2 >= gammxc)
        {
          gammxc = gamco2;
          kgamc = kgam;
        }
      }
      else if(kslope_ == 2)
      {
        if(gamh2o >= gammxh)
        {
          gammxh = gamh2o;
          kgamh = kgam;
        }
        if(gamco2 <= gammnc)
        {
          gammnc = gamco2;
          kgamc = kgam;
        }
      }
      else if(kslope_==3)
      {     
        if(gamh2o>=gammxh)
        {
          gammxh = gamh2o;
          kgamh = kgam;
        }
        if(gamco2 >= gammxc)
        {
          gammxc = gamco2;
          kgamc = kgam;
        }
      }
    }
    
    
    
    
    
    
    
    void max_min_slope_forth(int& kgam1, int& kgamh, int& kgamc)
    {
      kgam1=1;

      if(kgamh<=2)
        for(int i=0; i<20; i++)
          wij_[i][0] = wijmin_[i][0];
      else
        for(int i=0; i<20; )
          wij_[i][0] = wijmax_[i][0];
      
      if(kgamc==1 && kgamc==3) 
        for(int i=0; i<20; i++)
          wij_[i][1] = wijmin_[i][1];
      else
        for(int i=0; i<20; i++)
          wij_[i][1] = wijmax_[i][1];
    }
    
    
    
    
    
    
    
    
    void comp_and_initial_guess(int& igas, int& ligas, int& kspur, int m, int& ligs,
                                double& oxtott, vec& ox, vec& ox2, 
                                vec& x, vec& x2, vec& oxnew, vec& xnew, vec& xy, vec& xgg, vec& xgg1, vec& fvec, vec& fref, 
                                vec& h2oco2)
    {
       comp(ox,x);
       
       for(int i=0; i<12; i++) oxnew[i]=ox[i];

       double xdissw = ox[0];
       double xdissc = ox[1];
       oxnew[0] = xdissw_;
       oxnew[1] = xdissc_;
       
       for(int i=0; i<2; i++) if(oxnew[i]<1e-18) oxnew[i]=1e-18;

       comp(oxnew, xnew);
       
       for(int i=0; i<2; i++)
       {
         xtot_[i] = x[i];
         oxtot_[i] = ox[i];
         xt_[i] = xtot_[i];
       }
  
       if(igas==4) //This is igas == 0 in Paolo's code. Cannot be 0 for me since 0 in c++ is 1 in fortran indexation'
       {
         xy[0] = ox[0];
         xy[1] = ox[1];
         xym_[0] = x[0];
         xym_[1] = x[1];
       }
       oxtott = ox[0] + ox[1];
  
       if(igas != 2) oxtott = ox[igas];
       
  
       //  compositional terms -------------------------              
       double pln=log(p_/poco2_), gammxh=-1.e6, gammxc=-1.e6, gammnh=1.e6, gammnc=1.e6;
       int kgam=0, kgam1=0, kgamh=0, kgamc=0;
       
       
       max_min_slope_first(kgam);
       max_min_slope_second(x, pln, igas);
       
       while(mslope_!=0 && kgam<=4)
       {
         max_min_slope_third(kgam, kgamh, kgamc, gammxh, gammxc, gammnh, gammnc, x);
         max_min_slope_first(kgam);
         max_min_slope_second(x, pln, igas);
       }
       
       while(mslope_!=0 && kgam1==0)
       {
         max_min_slope_forth(kgam1, kgamh, kgamc);
         max_min_slope_second(x, pln, igas);
         max_min_slope_third(kgam, kgamh, kgamc, gammxh, gammxc, gammnh, gammnc, x);
       }
       
       
       //------------ reference terms -------------------------
       
       const double sg=4.1e-6, expg=0.5;
       
       for(int i=0; i<2; i++)    xtrm_[i] = (ctrm_[i]-xwo_)/(rgas_*T_);
       
       if(igas==4)
       {
          xtrm_[0] = (1.0-x[1])*ctrm_[0] - x[1]*ctrm2_[0] - (1.0-x[0]-x[1])*xwo_;
          xtrm_[0] = xtrm_[0]*(1.0-x[0]-x[1])/(rgas_*T_);
          xtrm_[1] = -x[0]*ctrm_[0] + (1.0-x[1])*ctrm2_[1] - (1.0-x[0]-x[1])*xwo_;
          xtrm_[1] = xtrm_[1]*(1.0-x[0]-x[1])/(rgas_*T_);
          
          xgg[0] = 0.5;
          pref_ = pow((ox[0]/sg), (1.0/expg));
          
          if(ox[0]<0.01 && ox[1]/ox[0]>=0.01)   pref_ *= 10.0;

          xgg[1] = 1.0;
          
          numerical_procedure(igas, ligas, kspur, m, ligs, oxtott, 
                              ox, ox2, x, x2, oxnew, xnew, xy, xgg, xgg1, fvec, fref, 
                              h2oco2);   //goto 81 substituted for if/else up to start numerical procedure
          return;
       }
       
       if(igas==2) 
       {       
         for(int ig=0; ig<2; ig++)
         {           
           fpt(ycd_, ig, fref);           
           rtrm_[ig] = fol_[ig] + fref[ig]/(rgas_*T_);
           vref_[ig] = vr_;
         }  
       }
       
       else 
       {
          ycd_ = 0.0;        
          fpt(ycd_, igas, fref);
       
          rtrm_[igas] = fol_[igas] + fref[igas]/(rgas_*T_);
          vref_[igas] = vr_;
       }
       
       
       //    decide if one or two gases -----------------------       
       
       for (m=0; m<2 ;m++)
       {
          int kigas = 0;
          if(igas==2)
          {
            if(m==0)
            {
              igas = 0;
              kigas = 1;
            }  
            else return;
          }
          //-------- one gas (or two gases and setting initial guess) --------
          
          //check---------------------------
          if(igas!=2 && kigas!=1)
          {
            double bterm = rgas_*T_*log(phi_[igas]*p_/xnew[igas]);
            bterm += pow((1.0-xnew[igas]), 2.0)*xwo_;
            double aterm = pow(1.0-xnew[igas], 2.0)*ctrm_[igas] + rgas_*T_*fol_[igas];
            aterm += fref[igas];
            resid2_ = bterm - aterm;
            xgxg_ = xnew[igas];
            phic_ = phi_[igas];
            frefc_ = fref[igas];
            xwoc_ = xwo_;
            ctrmc_ = ctrm_[igas];
          }
          //check---------------------------
          
          //set initial guess for one gas
          if(m!=1)
          { 
             ox2[0] = sg*pow(p_,expg);
             ox2[1] = 0.0;

             for(int i=2; i<nox_; i++)   ox2[i] = ox[i];

             comp(ox2, x2);
             
             estxy_ = {x2[0], x2[0]*0.1};
             
             
             //start iteration -----------------------------------
             
             double eps = 1.e-12;
             double xg = 0.0;
             double ah = 0.8*estxy_[igas];
             double bh = 1.2*estxy_[igas];
                                  
             int k = 0;
             while(k!=2)
             {   
                secantmethod(k, ah, bh, eps, xg, solwcad_function1, phi_[igas], xtrm_[igas], rtrm_[igas], p_);
                
                if (k==3) //----change boundaries-----------
                {
                   ah = 0.5*ah;
                   bh = 1.5*bh;
                   if(ah<1e-15 && bh>1.0)
                   {
                     std::stringstream ss;            
                     ss<<"*** error from solwcad2"<<std::endl;
                     ss<<"at P = "<<p_<<"  and T_ = "<<T_<<std::endl;
                     ss<<"the solution cannot be bracketed"<<std::endl;
                     Error(ss.str());
                   }
                   
                   if(bh>=1.0) bh=1.0;
                }
             }
              
              
             //xg = x[igas] has been found ------------------
             if(k==2)
             {
                clear(xy);
                clear(xym_);
                
                for(int i=0; i<2; i++)
                  x[i] = 0.0;     
                
                if(kigas==0) 
                {
                  if(xg >= xtot_[igas] && itotal_==1)
                  {
                    ligs = 1;
                    x[igas] = xtot_[igas];
                    xym_[igas] = xtot_[igas];
                  }
                  else
                  {
                    x[igas] = xg;
                    xym_[igas] = xg;
                    xym_[igas+2] = 1.0;
                  }
                  set_properties(igas, x, pln, h2oco2, xy, oxtott, ligas); 
                  return;
                }
             }
             
             //set initial estimate for two gases ----------------
             estxy_[0] = xg;
             estxy_[1] = xg*0.1;
          }
          else
          {
             estxy_[0] = xtot_[0];
             estxy_[1] = xtot_[1];
          }
          
          for(int i=0; i<2; i++)
            if(estxy_[i] >= xtot_[i])    
              estxy_[i] = xtot_[i]-1.e-6;
            
          igas = 2;
       }  
    
    } 








    void scaling_unknowns(int& igas, int& ligas, int& kspur, int m, int& ligs, double& oxtott, vec& ox, vec& ox2, 
                          vec& x, vec& x2, vec& oxnew, vec& xnew, vec& xy, vec& xgg, vec& xgg1, vec& fvec, vec& fref, 
                          vec& h2oco2)  //this is tag 91
    {
       int inot = 1;

       for(int i=0; i<2; i++)
       {          
          if(i == inot)  inot=0;
          
          if(xtot_[i] < tresho_)
          {
            igas = inot;
            ox[i] = 0.0;
            oxtott = ox[igas];
            m = 0;
            ligas = 1;
            comp_and_initial_guess(igas, ligas, kspur, m, ligs, oxtott, ox, ox2, 
                                   x, x2, oxnew, xnew, xy, xgg, xgg1, fvec, fref, h2oco2);
            return;
          }
          
          if(estxy_[i]/estxy_[inot] < tolscl_)
          {
            iscale_ = 1;
            isc_ = i;
            xscale_ = estxy_[i]/estxy_[inot];
            xt_[i] /= xscale_;
            estxy_[i] = estxy_[inot];
          }
       }
              
       if(kspur!=0)
       { 
          numerical_procedure(igas, ligas, kspur, m, ligs, oxtott, ox, ox2, x, x2, 
                              oxnew, xnew, xy, xgg, xgg1, fvec, fref, h2oco2);
          return;
       }
       
       for(int i=0; i<2; i++)
       {
         xgg[i] = estxy_[i];
         xgg1[i] = xgg[i];
       }    
    } 
    
    
    
    
    
    
    
    
    
    
    void numerical_procedure(int& igas, int& ligas, int& kspur, int m, int& ligs, double& oxtott, vec& ox, vec& ox2, 
                             vec& x, vec& x2, vec& oxnew, vec& xnew, vec& xy, vec& xgg, vec& xgg1, vec& fvec, vec& fref, 
                             vec& h2oco2)
    {
       bool err;
       newt(xgg, err, fvec);  // extended newton-raphson method for two equations
       
       if(igas != 4) 
       {        
          //xgg[0] = 0.20779959738311998;
          //xgg[1] = 5.2807856903919126e-3;
          
          funcv(xgg, fvec);
          
          if(err)   Error("convergency problems in newt");
          
          for(int i=0; i<2; i++)
          {
             if(fabs(fvec[i])>tol_)
             {
                kspur += 1;
                m = 0;
                
                //undersaturated conditions ------------
                
                for(int j=0; j<2; j++)    xgg[j] = xgg1[j]*0.8/kspur;
                
                if(kspur >= 3)
                { 
                   for (int ispur=0; ispur<2; ispur++)    xgg[ispur] = xtot_[ispur];
                   
                   if(iscale_==1 && i==isc_)    xgg[i] *= xscale_;
                   
                   x[i] = xgg[i];
                   xym_[i] = xgg[i];
                   break;
                }
                scaling_unknowns(igas, ligas, kspur, m, ligs, oxtott, ox, ox2, x, x2, 
                                 oxnew, xnew, xy, xgg, xgg1, fvec, fref, h2oco2); //goto 91
                return;
             }
             if(iscale_==1 && i==isc_) xgg[i]*= xscale_;
             x[i] = xgg[i];
             xym_[i] = xgg[i];
          }
           
          xym_[2] = y_[0];
          xym_[3] = 1.0-y_[0];
          
          return; 
       }
       else 
       {
         if(klorig_ == -1)
         {
           xym_[2] = xgg[0];
           xym_[3] = 1.0 - xgg[0];
           p_ = xgg[1]*pref_;
         }
         else
         {
           xym_[0] = xgg[0];
           xym_[1] = xgg[1];
           xym_[2] = 1.0 - ycd2_;
           xym_[3] = ycd2_;
           p_ = pc_;   
         }
       }
    } 










    void set_properties(int& igas, vec& x, double pln, vec& h2oco2, vec& xy, double oxtott, int& ligas) //this function is label 70
    {
       // set properties and xy(1 -> 4) ----------------------------------------
       double xvolo = 1.0 - x[0] - x[1];
       
       //c H2O
       if(igas!=1)
       {
         vexc_[0] = (-x[1]*xvolo*ctrm2_[1] + (1.0-x[0])*x[1]*h2oco2[1])/p_;
         gam_[0] = (1.0-x[0])*(xvolo*ctrm_[0] + x[1]*(h2oco2[0]+pln*h2oco2[1])) - x[1]*xvolo*ctrm_[1] - pow(xvolo,2.0) *xwo_;
         gam_[0] = exp(gam_[0]/(rgas_*T_));
         act_[0] = gam_[0]*x[0];
         phig_[0] = phi_[0];
       }
       
       //c CO2
       if(igas!=0)
       {
         vexc_[1] = (1.0-x[1])*(xvolo*ctrm2_[1] + x[0]*h2oco2[1])/p_;
         gam_[1] = (1.0-x[1])*(xvolo*ctrm_[1] + x[0]*(h2oco2[0]+pln*h2oco2[1])) - x[0]*xvolo*ctrm_[0] - pow(xvolo,2.0) *xwo_;
         gam_[1] = exp(gam_[1]/(rgas_*T_));
         act_[1] = gam_[1]*x[1];
         phig_[1] = phi_[1];
       }
       
       for(int i=0; i<2; i++)    estxy_[i] = x[i];
       
       double xyws = xym_[2]*pm_[0] + xym_[3]*pm_[1];
       for(int i=0; i<nox_; i++)
       {
         if(i>=2) x[i] *= xvolo;
         fmol_[i] = x[i]/f_[i];         
       }
       
       //***************************************************** Ghiorso and Sack 1995
       if(ighior_==1)   fmol_[2] += 0.5*(fmol_[6]+fmol_[7]+fmol_[8]+fmol_[9]) + fmol_[10] + fmol_[11];
       else
       {
         fmol_[2] = fmol_[2] + 0.5*fmol_[6] + 0.5*fmol_[8] + fmol_[9] + fmol_[10] + 2.0*fmol_[11];
         fmol_[4] = fmol_[4] + fmol_[11];
       }
       //***************************************************** Ghiorso and Sack 1995
       
       double oxn = 0.0;
       double oxs = 0.0;
       for(int i=0; i<nox_; i++)
       {
         oxn = fmol_[i]*pm_[i];
         oxs += oxn;
         if(i<=1)    xy[i] = oxn;
       }
       
       xy[0] /= oxs;
       xy[1] /= oxs;
       
       xy[2] = xym_[2]*pm_[0]/xyws;
       xy[3] = 1.0 - xy[2];
       
       if(igas!=0)
       {           
          //c check mass conservation --------------          
          double wtfrex_ = (oxtott-xy[0]-xy[1])/(1.0-xy[0]-xy[1]);
          
          //c undersaturated conditions ------------------------
          if(wtfrex_<1.0e-8)
          { 
            wtfrex_ = 0.0;
            if(igas==2)
            {
              for(int i=0; i<4; i++)
              {
                 if(i<=1)   xy[i] = oxtot_[i];
                 else
                 {
                   xy[i] = 0.0;
                   xym_[i] = 0.0;
                 }
              } //for
            }
          }
       }       
       //   if(ligas==1) igas=2;
    }












    double sub_vm(double T, int igas)
    {    
       double ahfac=0.1;
       double fac=0.2;
       double eps=1.e-16;
       int kerr=0;
       int kerr2=0;
       int kerr3=0;
       int klim=20;
       double ah = 1.0, bh = 1.0;
       double ahst = 1.0;
       
       //c set boundaries ----------------------------------
       
       if(igas==1) ah=2.e-5;
       else
       {
         ah=1.9901e-5 - 4.6125e-15*p_ + 5.5523e-25*p_*p_;
         if(igas!=0)
         {
           ah=ah+1.3539e-5*ycd_;
           if(T<=1300.0) ah=ah*0.8;
         }
         ahst=ah;
       }
       
       bh=ah*(1.0+fac);
       
       //c start iterating ----------------------------------
       
       double a = 0.0;
       double v = 0.0;
       int k=0;
       
       while (k!=2)
       {
         //a = cm_ + dm_/v + em_/v*v;
         //ym_ = 0.25*bm_/v;      
         //yh_ = p_ - rgas_*T*(1.0+ym_ + ym_*ym_ - ym_*ym_*ym_)/(v*pow((1.0-ym_),3)) + a/(sqrt(T)*v*(v+bm_));
         secantmethod(k, ah, bh, eps, v, solwcad_function2, bm_, cm_, dm_, em_, ym_, p_, T_, rgas_);
         
         // change boundaries ----------------------------------
         
         if (k==3)
         {
           kerr+=1;
           if(kerr==klim)
           {
             kerr=0;
             kerr2+=1;
             if(kerr2==100)
             {
               kerr=0;
               kerr2=0;
               kerr3+=1;
               ah=ahst;
               ahfac=-ahfac;
               if(kerr3==2)
               {
                 int ivm=1;
                 return v;
               }
             }
             fac=0.1;
             ah=ah*(1.0+ahfac);
           }
           fac=2.0*fac;
           bh=ah*(1.0+fac); //goto 5
         }
       }//while
       
       if (k==2)
       {
         a = cm_ + dm_/v + em_/v*v;
         ym_ = 0.25*bm_/v;      
         yh_=p_-rgas_*T*(1.0+ym_+ym_*ym_-ym_*ym_*ym_)/(v*pow((1.0-ym_),3)) + a/(sqrt(T)*v*(v+bm_));
       }
       return v;
    }








    void funcv_first(vec& f, vec& fref, vec& xtrmx, vec& x) //continue 8878 to continue 7788
    {
    
       // ---- igas = 0 ----start
       // DETERMINE P SATURATION AND GAS COMPOSITION FROM DISSOLVED H2O-CO2 PAIR
       // or
       // DETERMINE DISSOLVED H2O and CO2 FOR GIVEN P AND GAS PHASE COMPOSITION
       
       double xh = 0.0;
       double xc = 0.0;
       
       T_=tc_;
       
       if(klorig_==-1)
       {
         ycd_=1.0-x[0];
         p_=x[1]*pref_;
         xh=xym_[0];
         xc=xym_[1];
       }
       else
       {
         ycd_=ycd2_;
         p_=pc_;
         xh=x[0];
         xc=x[1];
       }
       
       for(int ig=0; ig<2; ig++)
       {
         fpt(ycd_,ig,fref);
         rtrm_[ig] = fol_[ig]+fref[ig]/(rgas_*T_);
       }
       
       int ig=3;
       fpt(ycd_,ig,fref);
       
       double trm1=(1.0-xh-xc)*ttrm_*log(p_/poco2_)*ctrm2_[1]/(rgas_*T_);
       xtrmx[0]=xtrm_[0]-xc*trm1;
       xtrmx[1]=xtrm_[1]+(1.0-xc)*trm1;
       
       f[0]=log(phi_[0]*p_*(1.0-ycd_)/xh)-xtrmx[0]-rtrm_[0]+(1.0-xh)*xc*whc_;
       f[1]=log(phi_[1]*p_*ycd_/xc)-xtrmx[1]-rtrm_[1]+(1.0-xc)*xh*whc_;
    }
    
    
    
    
    
    
    
    
          
       
       
    
    void fpt(double& ycd_, int igas, vec& fref)
    {
    
       // returns:
       //          fref = liquid components reference terms
       //          phi = gas components fugacities
       
       
       vec x(2);
       vec xsfl{vec(13)}, fgsfl{vec(13)}, actsfl{vec(13)};
       
       const vec bd = {9.144e-6,3.685e-9,1.168e-11,-1.99e-15,1.22e-16,-1.945e-17,-1.58e-21,4.68e-24,1.144e-26,-3.96e-33};
       
       // calculates phi[i], i = 1 -> 2 (two-component gas phase) ------------
       
       double tp=pow(T_,1.5);
       
       double v=vm(T_,igas);
       
       vfld_=v;
       vr_=0.0;
       
       if(igas==0)
       {
          vr_=bd[0]+bd[1]*T_+bd[2]*T_*T_+bd[3]*T_*T_*T_+p_*(bd[4]+bd[5]*T_+bd[6]*T_*T_)+p_*p_*(bd[7]+bd[8]*T_)+p_*p_*p_*bd[8];
          vmolh_=vr_;
       }
       else if(igas==1) vr_=par_[0]+par_[1]*T_+par_[2]*T_*T_+par_[3]*T_*T_*T_+p_*(par_[4]+par_[5]*T_+par_[6]*T_*T_)+p_*p_*(par_[7]+par_[8]*T_)+p_*p_*p_*par_[8];
       
       double vln=log((v+bm_)/v);
       double phex = 0.0;
       if(igas==2)
       {
          x[0]=1.0-ycd_;
          x[1]=ycd_;
       
          for(int i=0;i<2;i++)
          {
             phex = (4.0*ym_-3.0*ym_*ym_)/(pow((1.0-ym_),2)) 
                  + b_[i]/bm_*(4.0*ym_-2.0*ym_*ym_)/pow((1.0-ym_),3.) 
                  - (2.0*c_[i][i]*x[i]+2.0*(1.0-x[i])*c_[0][1])*vln/(rgas_*tp*bm_) 
                  - cm_*b_[i]/(rgas_*tp*bm_*(v+bm_)) 
                  + cm_*b_[i]*vln/(rgas_*tp*bm_*bm_) 
                  - (2.0*d_[i][i]*x[i]+2.0*(1.0-x[i])*d_[0][1]+dm_)/(rgas_*tp*bm_*v) 
                  + (2.0*d_[i][i]*x[i]+2.0*(1.0-x[i])*d_[0][1]+dm_)*vln/(rgas_*tp*bm_*bm_) 
                  + b_[i]*dm_/(rgas_*tp*v*bm_*(v+bm_))
                  + 2.0*b_[i]*dm_/(rgas_*tp*bm_*bm_*(v+bm_)) 
                  - 2.0*b_[i]*dm_*vln/(rgas_*tp*bm_*bm_*bm_)
                  - 2.0*(e_[i][i]*x[i]+(1.0-x[i])*e_[0][1]+em_)/(rgas_*tp*2.0*bm_*v*v) 
                  + 2.0*(e_[i][i]*x[i]+e_[0][1]*(1.0-x[i])+em_)/(rgas_*tp*bm_*bm_*v)
                  - 2.0*(e_[i][i]*x[i] + e_[0][1]*(1.0-x[i])+em_)*vln/(rgas_*tp*bm_*bm_*bm_) 
                  + em_*b_[i]/(rgas_*tp*2.0*bm_*v*v*(v+bm_))
                  - 3.0*em_*b_[i]/(rgas_*tp*2.0*bm_*bm_*v*(v+bm_)) 
                  + 3.0*em_*b_[i]*vln/(rgas_*tp*bm_*bm_*bm_*bm_) 
                  - 3.0*em_*b_[i]/(rgas_*tp*bm_*bm_*bm_*(v+bm_))
                  - log(p_*v/(rgas_*T_));
                     
              phi_[i]=exp(phex);
          
          }
          return;
       }




       // calculates phi_[igas] (one-component gas phase) --------------
       
       double phexp = (8.0*ym_-9.0*ym_*ym_+3.0*ym_*ym_*ym_)/pow((1.0-ym_),3.0) - log(p_*v/(rgas_*T_)) - cm_/(rgas_*tp*(v+bm_))
                    - dm_/(rgas_*tp*v*(v+bm_)) - em_/(rgas_*tp*v*v*(v+bm_)) - cm_/(rgas_*tp*bm_)*vln 
                    - dm_/(rgas_*tp*bm_*v) + dm_/(rgas_*tp*bm_*bm_)*vln - em_/(rgas_*tp*2.0*bm_*v*v) + em_/(rgas_*tp*bm_*bm_*v) - em_/(rgas_*tp*bm_*bm_*bm_)*vln;
   
       phi_[igas] = exp(phexp);
       
       //c calculates fref[igas] ---------------------------------
       
       double t1=0.0;
       double t2=0.0;
       double t3=0.0;
       
       if(igas!=1) 
       {
         //c H2O
         t1=2.0-T_/to_[0];
         t2=2.0*T_-to_[0];
         t3=2.0*T_*T_-to_[0]*to_[0];
         fref[0]=p_*(bd[0]*t1+bd[1]*T_+bd[2]*T_*t2+bd[3]*T_*t3) + p_*p_*(bd[4]*t1+bd[5]*T_+bd[6]*T_*t2)*0.5 + p_*p_*p_*(bd[7]*t1+bd[8]*T_)/3.0 + p_*p_*p_*p_*bd[9]*t1*0.25;
         return;
       }
       //c CO2
       else 
       {
         if(imode_==2)
         { 
           t1=2.0-T_/to_[1];
           t2=2.0*T_-to_[1];
           t3=2.0*T_*T_-to_[1]*to_[1];
           fref[1]=p_*(par_[0]*t1 + par_[1]*T_ + par_[2]*T_*t2+par_[3]*T_*t3) + p_*p_*(par_[4]*t1+par_[5]*T_+par_[6]*T_*t2)*0.5 + p_*p_*p_*(par_[7]*t1+par_[8]*T_)/3.0 + p_*p_*p_*p_*par_[9]*t1*0.25;
           //frefcm=fref[1];
           
           return;
         }
       }
    }









    void comp(vec& ox, vec& x)
    {
       // returns the mole fraction composition from the oxides wt distribution              
       double xs = 0.0;
       for (int i=2; i<12; i++) xs += ox[i]; 
       
       for(int i=0;i<12;i++)
       {
         if(i>=2) ox[i]=ox[i]*(1.0-ox[0]-ox[1])/xs;
         fmol_[i]=ox[i]/pm_[i];  
       }
       
       double xsum = 0.0;
       for(int i=0;i<12;i++)
       {
          // ***************************************************** Ghiorso and Sack 1995
          if(ighior_==1) {if(i==2) fmol_[i] = fmol_[i]-0.5*(fmol_[6]+fmol_[7]+fmol_[8]+fmol_[9])-fmol_[10]-fmol_[11];}
          else
          {
             if(i==2)  fmol_[i]=fmol_[i]-0.5*fmol_[6]-0.5*fmol_[8]-fmol_[9]-fmol_[10]-2.0*fmol_[11];
             if(i==4)  fmol_[i]=fmol_[i]-fmol_[11];
          }
          // ***************************************************** Ghiorso and Sack 1995
         
          x[i]=fmol_[i]*f_[i];
          xsum=xsum+x[i];
       }
       
       for(int i=0;i<12;i++)
       {
         x[i]=x[i]/xsum;
         if(i>=2) x[i]=x[i]/(1.0-x[0]-x[1]);
       }
    }






      double vm(double T, int igas)
      {
         int ivm;
         vec x(2);
         
         if(igas!=1)
         {
            b_[0]=2.9e-5;
            c_[0][0]=(290.78 - 0.30276*T + 1.4774e-4*T*T)*1.e-1;
            d_[0][0]=(-8374.0 + 19.437*T - 8.148e-3*T*T)*1.e-7;
            e_[0][0]=(76600.0 - 133.9*T + 0.1071*T*T)*1.e-13;
            bm_=b_[0];
            cm_=c_[0][0];
            dm_=d_[0][0];
            em_=e_[0][0];
            if(igas!=2) return sub_vm(T,igas);         
         }
         
         b_[1]=5.8e-5;
         c_[1][1]=(28.31 + 0.10721*T - 8.81e-6*T*T)*1.e-1;
         d_[1][1]=(9380.0 - 8.53*T + 1.189e-3*T*T)*1.e-7;
         e_[1][1]=(-368654.0 + 715.9*T + 0.1534*T*T)*1.e-13;
         bm_=b_[1];
         cm_=c_[1][1];
         dm_=d_[1][1];
         em_=e_[1][1];
         
         if(igas!=2) return sub_vm(T,igas);
      
         x[0]=1.0-ycd_;
         x[1]=ycd_;
         
         bm_=x[0]*b_[0]+x[1]*b_[1];
         
         if(c_[0][0]*c_[1][1]<=0.0)   c_[0][1]=0.0;
         else c_[0][1] = sqrt(c_[0][0]*c_[1][1]);
         
         if(d_[0][0]*d_[1][1]<=0.0)   d_[0][1]=0.0;
         else d_[0][1] = sqrt(d_[0][0]*d_[1][1]);
         
         if(e_[0][0]*e_[1][1]<=0.0)   e_[0][1]=0.0;
         else e_[0][1] = sqrt(e_[0][0]*e_[1][1]);
         
         
         c_[1][0]=c_[0][1];
         d_[1][0]=d_[0][1];
         e_[1][0]=e_[0][1];
         cm_=0.0;
         dm_=0.0;
         em_=0.0;
         for(int i=0;i<2;i++)
          for(int j=0;j<2;j++)
          {
            cm_=cm_+x[i]*x[j]*c_[i][j];
            dm_=dm_+x[i]*x[j]*d_[i][j];
            em_=em_+x[i]*x[j]*e_[i][j];
          }
         
         return sub_vm(T,igas);
      }






    void funcv(vec& x, vec& f)
    {    
       // called from newt
       // restitutes the functions f(x) for any given value of x
    
       const double tin=1.0e-16;
       vec fref(2), gtrm(2), xtrmx(2), x2(2); //x(n),f_(n)
    
       int igas=igs_;
       if(igas==4) // THIS WOULD BE TO USE AN ALREADY DEFINED GAS COMPOSITION
       {
         funcv_first(f, fref, xtrmx, x);
         return;      
       }
    
       for(int i=0; i<2; i++)
         if(x[i]<=0.0) 
           x[i]=tin;
         
       x2[0]=x[0];
       x2[1]=x[1];
       
       for(int i=0; i<2; i++)
         if(iscale_==1 && i==isc_) 
           x2[i]=x[i]*xscale_;
       
       y_[0]=(xtot_[0]*(1.0-x2[1])-x2[0]*(1.0-xtot_[1]))/(xtot_[1]-x2[1]+xtot_[0]-x2[0]);
       
       if(y_[0]<=tin) y_[0]=tin;
       if(y_[0]>=1.0-tin) y_[0]=1.0-tin;
       
       y_[1]=1.0-y_[0];
       for(int i=0; i<2; i++)
       {
         int inot=1;
         if(i==inot) inot=0;
           if(y_[i]<=0.0)
           {
              y_[i]=tin;
              y_[inot]=1.0-y_[i];
           }
           else if(y_[i]>=1.0)
           {
              y_[i]=1.0-tin;
              y_[inot]=1.0-y_[i];
           }
       }
       ycd_=y_[1];
    
       p_=pc_;
       T_=tc_;
       int ig=2;
    
       fpt(ycd_,ig,fref);
    
       for(int i=0;i<2;i++)
       {
         int inot=1;
         if(i==inot) inot=0;
         gtrm[i]=log(phi_[i]*y_[i]*p_/x2[i]);
         xtrm_[i]=(1.0-x2[0]-x2[1])*((1.0-x2[i])*ctrm_[i] - x2[inot]*ctrm_[inot]) - pow((1.0-x2[0]-x2[1]),2.0)*xwo_;
         xtrm_[i]/=(rgas_*T_);           
         f[i]=gtrm[i]-xtrm_[i]-rtrm_[i];    
       }
    } 








//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
//---------End of solwcad functions and start of Numerical Recipes functions------------------------------------------------------------




    //! Returns f=1/2(F*F) at point x (from Numerical Recipes, modified).
    double fmin(vec& x, vec& f)
    {  
      funcv(x, f);
      auto sum = 0.0;
      for(int i=0; i<2; i++)    sum += (f[i]*f[i]);  
      return 0.5*sum;
    }
    
    
    
    
    
    void fdjac(vec& x, vec& fvec, double (&df)[2][2])
    {
      const double EPS = 1.0e-8; //approximate square root of the machine precision
      int i, j;
      double h, temp;
      vec f(2, 0.0);
      
      for (j=0; j<2; j++)
      {
        temp = x[j];
        h = EPS*fabs(temp);
        if(h == 0.0)    h = EPS;
        x[j] = temp + h; //trick to reduce finite precision error
        h = x[j] - temp;
        funcv(x, f);
        x[j] = temp;
   
        for (i=0; i<2; i++)   
          df[i][j] = (f[i] - fvec[i])/h; //forward difference formula
      }
    }
    
    
    
    
    
      //! Given an n-dimensional point (xold), the value of the function (fold) and
      //! gradient there (g), and a direction (p), finds a new point (x) along the
      //! direction p from xold where the function func has decreased "sufficiently"
      //! (from Numerical Recipes).
    void lnsrch(vec& xold, const double fold, vec& g, vec& p, vec& x, double &f, const double stpmax, bool &err, vec& fvec)
    {
      const double ALF=1.0e-6, TOLX = 1.0e-12;
      double a, alam, alam2=0.0, alamin, b, disc, f2=0.0;
      double fold2 = 0;
      double rhs1, rhs2, slope, sum, temp, test, tmplam;
      err = false;
      
      sum = 0.0;
      for(int i=0; i<2; i++)  sum += p[i]*p[i];
      sum = sqrt(sum);
      
      if(sum > stpmax)
       for (int i=0; i<2; i++)  
         p[i] *= stpmax/sum; //scale if attempted step is too big
      
      slope = 0.0;
      for(int i=0; i<2; i++)   slope += g[i]*p[i];
      
      if(slope >= 0.0)   Error("Roundoff problem in lnsrch.");
      
      test=0.0; // for compute lambda_min(alamin)
      
      for(int i=0; i<2;i++)
      {
        temp = fabs(p[i])/std::max(fabs(xold[i]), 1.0);
        if (temp > test) test = temp;
      }
      
      alamin = TOLX/test;//minimum step lenght
      alam = 1.0; //always try full Newton step first
      
      
      for (;;)
      {
        for(int i=0; i<2; i++)    x[i] = xold[i] + alam*p[i];
        
        double f = fmin(x, fvec); //f_min
       
        if (alam < alamin) //convergence on Delta_x
        {
          for(int i=0; i<2; i++)   x[i] = xold[i];
          err = true;
          return;
        }

        else if (f <= fold + ALF*alam*slope)      return;       //sufficient function decrease

        else //backtrack
        {
          if (alam == 1.0) //first time
            tmplam = -slope/(2.0*(f-fold-slope));

          else //subsequent backtracks
          {
            rhs1 = f - fold - alam*slope;
            rhs2 = f2 - fold - alam2*slope;
          
            a = (rhs1/(alam*alam) - rhs2/(alam2*alam2))/(alam-alam2);
            b = (-alam2*rhs1/(alam*alam) + alam*rhs2/(alam2*alam2))/(alam-alam2);

            if(a == 0.0)
            {
              tmplam = -slope/(2.0*b);
            }
            else
            {
              disc = b*b - 3.0*a*slope;
              if(disc < 0.0)
              {
                tmplam = 0.5*alam;
              }
              else if(b <= 0.0)
              {
                tmplam = (-b + sqrt(disc))/(3.0*a);
              }
              else
              {
                tmplam = -slope/(b+sqrt(disc));
              }
            }
            
            if (tmplam > 0.5*alam)  tmplam = 0.5*alam; //lambda <= 0.5*lambda1 (alam)
          }
        }
        alam2 = alam;
        f2 = f;
        alam = std::max(tmplam, 0.1*alam); //lambda >= 0.1*lambda1
      }
    }









    //! Given an initial guess (x) for a root in 2 dimensions, find the root by a
    //! globally convergent Newton's method.
    void newt(vec& x, bool &err, vec& fvec)
    {
       const int MAXITS=1000;
       const double TOLF=1.0e-12, TOLMIN=1.0e-12, STPMX=100.0;
       const double TOLX=1.0e-13, tolstp=1.e-15;
       //   TOLF= set the convergence criterion on function values                //
       //   TOLMIN=sets the criterion for deciding whether spurious convergence   //
       //       to a minimum of fmin has occurred                                 //
       //   TOLX=convergence criterion on delta_x                                 //
     
       int i, j, its;
       double d, den, f, fold, stpmax, sum, temp, test;
       
       std::vector<int> indx(2,0);
       vec g(2,0.0), p(2,0.0), xold(2,0.0);
       double fjac[2][2];
       
       f = fmin(x, fvec);
     
       test = 0.0; //test for initial guess being a root. use more stringent test than simply TOLF
       for (i=0; i<2; i++)
         if (fabs(fvec[i]) > test)   
           test = fabs(fvec[i]);
      
       if (test < 0.01*TOLF)
       {
         err = false;
         return;
       }

       sum = 0.0;

       for (i=0; i<2; i++)  sum += (x[i]*x[i]);

       stpmax = STPMX*std::max(sqrt(sum), 2.0); //calculate stpmax for line searches
       stpmax = x[0]*(1.0 - tolstp);      //stpmax definition
       double xless = x[0];
       
       for(int istp=1; istp<2; istp++)
       {
         if(x[istp] < xless)
         {
           xless = x[istp];
           stpmax = x[istp]*(1.0 - tolstp);
         }
       }
       
       for (its=0; its<MAXITS; its++) //start of iteration loop
       {
          fdjac(x, fvec, fjac);
          for (i=0; i<2; i++) //compute Nabla_f for the line search
          {
            sum = 0.0;
            for (j=0; j<2; j++)    sum += fjac[j][i]*fvec[j];
            g[i] = sum;
          }
          
          for (i=0; i<2; i++)     xold[i] = x[i]; //store x

          fold = f; //store f

          for (i=0; i<2; i++)   p[i] = -fvec[i]; //right-hand side for linear equations
            
            
          auto det =  fjac[0][0]*fjac[1][1] - fjac[0][1]*fjac[1][0];
          auto x1 = (fjac[1][1]*p[0] - fjac[0][1]*p[1])/det;
          auto x2 = (fjac[0][0]*p[1] - fjac[1][0]*p[0])/det;
          p[0] = x1;
          p[1] = x2;

          lnsrch(xold, fold, g, p, x, f, stpmax, err, fvec);
          
          //returns new x and f.it also calculates fvec at the new x when it calls fmin
          test=0.0;
          
          for (i=0; i<2; i++)
            if (fabs(fvec[i]) > test)      
               test = fabs(fvec[i]);
          
          if (test < TOLF)
          {
            err = false;
            return;
          }
   
          if (err)
          {
             test = 0.0;
             den = std::max(f,0.5*2);
             for(i=0; i<2; i++)
             {
               temp = fabs(g[i])*std::max(fabs(x[i]), 1.0)/den;
               if (temp > test) test=temp;
             }
             err = (test < TOLMIN);
             return;
          }
          
          test = 0.0; //test for convergence on delta_x
          
          for(i=0; i<2; i++)
          {
            temp = (fabs(x[i] - xold[i]))/std::max(fabs(x[i]),1.0);
            if (temp > test) test=temp;
          }
          
          if (test < TOLX)   return;
       }
       Error("MAXITS exceeded in newt");
    }








    template<typename T, typename ... Args>
    void secantmethod(int& k, double& x1, double& x2, const double& precision, double& root, T&& func, Args&&...args) 
    {
       const int max_iter = 1000; 
       double yl, yr, dx, swap, xl, rts;
     
       yl = func(x1, args...);
       yr = func(x2, args...);
       
       if(x1==x2 || yl==yr)
       {
          k = 3;
          Error("error in secantMethod call; x1==x2 or f(x1)==f(x2)");
          return;
       }
     
       if(fabs(yl) < fabs(yr))
       {
          rts = x1;
          xl = x2;
          swap = yl;
          yl = yr;
          yr = swap;
       }
       else
       {
          xl = x1;
          rts = x2;
       }
       
       for(int j = 1; j <= max_iter; j++)
       {
          dx = (xl-rts)*yr/(yr-yl);
          xl = rts;
          yl = yr;
          rts += dx;
          yr = std::forward<T>(func)(rts, std::forward<Args>(args)...);
                   
          if(fabs(dx) < precision || yr == 0.0)
          {
            k = 2;
            root = rts;
            return;
          }
       }
    }





   private:     //delcaring here the common variables from fortran
  
     constexpr static int nox_ = 12;
     const double rgas_ = 8.31451; double vr_ = 0.0; int imode_ = 2; //xgas/
     vec par_ = {0.2418946564E-05, 0.1344868775E-07, 0., 0., 0.2106552365E-14, 0., 0., 0., 0., 0.};
     const vec to_ = {1000.0, 1400.0};
          
     vec ctrm_{vec(2)}, ctrm2_{vec(2)}, xtot_{vec(2)}, oxtot_{vec(2)}, rtrm_{vec(2)}, y_{vec(2)}, xtrm_{vec(2)};//funcvc/ 
     double pc_, tc_, whc_, xwo_, poco2_=0.1013250000E+06; //funcvc
     int igs_, kint_; //funcvc/
     
     
     vec f_{vec(nox_)}, fmol_{vec(nox_)}, pm_ = {18.02,44.01,60.085,79.899,101.96,159.69,71.846,70.937,40.304,56.079,61.979,94.195}; //compc/

     vec xym_{vec(4)}, vref_{vec(2)}, vexc_{vec(2)}, gam_{vec(2)}, act_{vec(2)}, phig_{vec(2)}, phi_{vec(2)}; //solwcadc/
     int iopen_, itotal_ = 0; //solwcadc/

     vec estxy_{vec(2)}; //solwcadd/
     
     double pref_, xscale_; //scale/
     int iscale_ = 0, isc_ = 0; //scale/
     
     vec xt_{vec(2)}; //xtt/
     
     double wo_[nox_][nox_]; //modpar/
     vec fol_ = {0.1979347855E+02,0.2348447513E+02}; //modpar/
     
     double wij_[20][2]={{-0.3409307328E+05,-0.5996216205E+05},
                         {0.0000000000E+00 ,0.0000000000E+00 },
                         {-0.1891165069E+06,-0.5909571939E+06},
                         {0.1359353090E+06 ,0.4469622832E+07 },
                         {-0.1957509655E+06,0.2166575511E+05 },
                         {0.0000000000E+00 ,0.0000000000E+00 },
                         {-0.8641805096E+05,0.5286613028E+05 },
                         {-0.2099966494E+06,-0.3287921030E+06},
                         {-0.3222531515E+06,0.1400344548E+06 },
                         {-0.3497975003E+06,0.3090699699E+06 },
                         {0.               ,0.6049208301E+04 },
                         {0.               , 0.0000000000E+00},
                         {0.               ,0.4139537240E+05 },
                         {0.               ,-0.5293012049E+06},
                         {0.               ,0.1213717202E+04 },
                         {0.               ,0.0000000000E+00 },
                         {0.               ,-0.1344620202E+05},
                         {0.               ,0.1278883196E+05 },
                         {0.               ,-0.3521319385E+05},
                         {0.               ,-0.5800953852E+05}};

     // Model parameters ---------------------------------------------------------------------------------
     double wo83_[12][12] ={{0.,0.,0.          ,0.          ,0.          ,0.          ,0.          ,0.          ,0.          ,0.          ,0.         ,0.},
                                 {0.,0.,0.          ,0.          ,0.          ,0.          ,0.          ,0.          ,0.          ,0.          ,0.         ,0.},
                                 {0.,0.,0.          ,0.          ,0.          ,0.          ,0.          ,0.          ,0.          ,0.          ,0.         ,0.},
                                 {0.,0.,-122861.0680,0.          ,0.          ,0.          ,0.          ,0.          ,0.          ,0.          ,0.         ,0.},
                                 {0.,0.,-328708.4288,-281791.1448,0.          ,0.          ,0.          ,0.          ,0.          ,0.          ,0.         ,0.},
                                 {0.,0.,11037.09912 ,-28542.49488,5189.49888  ,0.          ,0.          ,0.          ,0.          ,0.          ,0.         ,0.},
                                 {0.,0.,-40292.50576,-19223.76456,-249067.6624,18930.34064 ,0.          ,0.          ,0.          ,0.          ,0.         ,0.},
                                 {0.,0., 23118.10624,-8548.7488  ,-8023.866   ,887.828064  ,-2942.77456 ,0.          ,0.          ,0.          ,0.         ,0.},
                                 {0.,0.,-126999.4624,53026.3424  ,-203655.3632,-5343.09352 ,-242361.5472,-11757.4584 ,0.          ,0.          ,0.         ,0.},
                                 {0.,0.,-268060.9304,-428617.328 ,-411824.0072,6358.88504  ,-248343.412 ,2925.130632 ,-330220.108 ,0.          ,0.         ,0.},
                                 {0.,0.,-308604.7272,-422893.616 ,-567413.16  ,-15553.51792,-154666.5808,3264.1476   ,-387486.0976,-262671.1016,0.         ,0.},
                                 {0.,0.,-366503.3376,-170291.7288,-733563.984 ,1187.109584 ,-353880.628 ,-254.0696344,-188961.5736,-116767.072 ,-75854.6648,0.}};
      double wo95_[12][12] ={{0.,0.,0.     ,0.     ,0.      ,0.      ,0.     ,0.,0.     ,0.     ,0.   ,0.},
                                  {0.,0.,0.     ,0.     ,0.      ,0.      ,0.     ,0.,0.     ,0.     ,0.   ,0.},
                                  {0.,0.,0.     ,0.     ,0.      ,0.      ,0.     ,0.,0.     ,0.     ,0.   ,0.},
                                  {0.,0.,26267. ,0.     ,0.      ,0.      ,0.     ,0.,0.     ,0.     ,0.   ,0.},
                                  {0.,0.,-39120.,-29450.,0.      ,0.      ,0.     ,0.,0.     ,0.     ,0.   ,0.},
                                  {0.,0.,8110.  ,-84757.,-17089. ,0.      ,0.     ,0.,0.     ,0.     ,0.   ,0.},
                                  {0.,0.,23661. ,5209.  ,-30509. ,-179065.,0.     ,0.,0.     ,0.     ,0.   ,0.},
                                  {0.,0.,0.     ,0.     ,0.      ,0.      ,0.     ,0.,0.     ,0.     ,0.   ,0.},
                                  {0.,0.,3421.  ,-4178. ,-32880. ,-71519. ,-37257.,0.,0.     ,0.     ,0.   ,0.},
                                  {0.,0.,-864.  ,-35373.,-57918. ,12077.  ,-12971.,0.,-31732.,0.     ,0.   ,0.},
                                  {0.,0.,-99039.,-15416.,-130785.,-149662.,-90534.,0.,-41877.,-13247.,0.   ,0.},
                                  {0.,0.,-33922.,-48095.,-25859. ,57556.  ,23649. ,0.,22323. ,17111. ,6523.,0.}};


     //************************** Ghiorso and Sack 1995
     vec f1_ = {1.0,1.0,0.25,0.25,0.375,0.375,0.25,0.25,0.25,0.25,0.375,0.375};
     vec f2_ = {1.0,1.0,1.0,1.0,1.0,1.0,0.5,1.0,0.5,1.0,1.0,2.0};
     //************************** Ghiorso and Sack 1995 


     double covp_[20][2]={{0.6316242175E+03,0.1065551645E+05},
                          {0.0000000000E+00,0.0000000000E+00},
                          {0.4801528874E+04,0.1887635505E+06},
                          {0.1267547652E+05,0.3898097404E+06},
                          {0.6130377278E+04,0.1746504717E+06},
                          {0.0000000000E+00,0.0000000000E+00},
                          {0.6097020738E+04,0.1001263771E+06},
                          {0.3500308057E+04,0.7567161446E+05},
                          {0.4581663847E+04,0.2041019346E+06},
                          {0.6257676676E+04,0.1495171552E+06},
                          {0.              ,0.1162983976E+04},
                          {0.              ,0.0000000000E+00},
                          {0.              ,0.1936693988E+05},
                          {0.              ,0.4662886265E+05},
                          {0.              ,0.1820384707E+05},
                          {0.              ,0.0000000000E+00},
                          {0.              ,0.1051294566E+05},
                          {0.              ,0.8091964307E+04},
                          {0.              ,0.2114478691E+05},
                          {0.              ,0.1594015370E+05}};
     
     double wijmax_[20][2], wijmin_[20][2]; 

     vec covfol_ = {0.1001737547E-01,0.1095849897E+00};
     vec covpar_ = {0.1570083361E-05,0.6432481672E-09,0.,0.,0.1399168896E-14,0.,0.,0.,0.,0.};

     vec folmax_{vec(2)}, folmin_{vec(2)};
     vec parmax_{vec(10)}, parmin_{vec(10)};
                         
     int icount_ = 0; //count/
     int ighior_ = 1; //ghior/
     double xdissw_, xdissc_, resid2_, xgxg_, phic_, frefc_, xwoc_, ctrmc_; //check/
     double ttrm_ = 1.0; //twij/
     int mslope_ = 0, kslope_ = 0; //gco2mm/
     double ffref1_, ffref2_; //fff/
     
     double whvc_, wcvc_, wh2o_, wco2_, wsio2_, ycd_, ycd2_; //vcalc/
     const int ivcalc_ = 0; int klorig_ = 0; //vcalc/   ivcalc should be 0 always
     
     int ipr_; //qa05dd/
     double vfluid_; //fluid2/
     double vfld_, vmolh_; //gasvol/
     double c_[2][2], d_[2][2], e_[2][2]; 
     vec b_{vec(2)}; 
     double bm_, cm_, dm_, em_, ym_;
     double yh_;
     double p_ = 0.0, T_ = 0.0;
     double tol_ = 1.0e-1, tolscl_ = 0.9, tresho_ = 5.0e-5;




}; //class




}  //namespace

#endif








//    //! Solve the set of n linear equations A*X = B (from Numerical Recipes).
//    void lubksb(double (&a)[2][2], std::vector<int>& indx, vec& b)
//    {
//      int i,ii=0,ip,j;
//      double sum;
//      
//      for(i=0; i<2; i++)
//      {
//         ip = indx[i];
//         sum = b[ip];
//         b[ip] = b[i];

//         if(ii != 0)
//           for(j=ii-1; j<i; j++)
//             sum -= a[i][j]*b[j];

//         else if(sum != 0.0)
//            ii = i+1;

//         b[i] = sum;
//      }
//      
//      for(i=1; i>=0; i--)
//      {
//        sum = b[i];
//   
//        for(j=i+1; j<2; j++)
//          sum -= a[i][j]*b[j];
//   
//        b[i] = sum/a[i][i];
//      }
//    }





//    //! Given a matrix, this routine replaces it by the LU decomposition of a rowwise
//    //! permutation of itself (from Numerical Recipes).

//    void ludcmp(double (&a)[2][2], std::vector<int>& indx, double &d)
//    {
//      const double TINY=1.0e-20;
//      int i,imax,j,k;
//      double big,dum,sum,temp;
//      vec vv(2,0.0); //vv stores the implicit scaling of each row
//      d=1.0;
//   
//      for (i=0;i<2;i++)  //loop over rows to get the implicit scaling information
//      {
//        big=0.0;

//        for (j=0;j<2;j++)
//          if ( (temp=fabs(a[i][j]) ) > big)      big=temp;

//        if (big == 0.0)     Error("Singular matrix in routine ludcmp");

//        vv[i]=1.0/big;
//      }
//      
//      for (j=0;j<2;j++) //loop over columns of crout's method
//      {
//        for (i=0;i<j;i++)
//        {
//          sum=a[i][j];
//          for (k=0;k<i;k++) sum -= a[i][k]*a[k][j];
//          a[i][j]=sum;
//        }
//        
//        big=0.0;
//        
//        for (i=j;i<2;i++)
//        {
//          sum=a[i][j];
//          for (k=0;k<j;k++) sum -= a[i][k]*a[k][j];
//          a[i][j]=sum;
//          
//          if ((dum=vv[i]*fabs(sum)) >= big)
//          {
//            big=dum;
//            imax=i;
//          }
//        }
//        
//        if (j != imax)
//        {
//          for (k=0;k<2;k++)
//          {
//            dum=a[imax][k];
//            a[imax][k]=a[j][k];
//            a[j][k]=dum;
//          }
//          d = -d;
//          vv[imax]=vv[j];
//        }
//   
//        indx[j]=imax;
//        
//        if (a[j][j] == 0.0) a[j][j]=TINY;
//        
//        if (j != 1)
//        {
//          dum=1.0/a[j][j];
//          for (i=j+1;i<2;i++) a[i][j] *= dum;
//        }
//      }
//    }








