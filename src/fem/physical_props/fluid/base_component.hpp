#ifndef GALES_BASE_COMPONENT_HPP
#define GALES_BASE_COMPONENT_HPP



#include<vector>
#include<memory>


namespace GALES {


  /**
       base chemical class
       it is supposed to be used as base class when we define other chemicals
  */



class base_component
{

 public:


  //-------------------------------------------------------------------------------------------------------------------------------------        
  base_component() = default;               //constructor
  /// Deleting the copy and move constructors - no duplication/transfer in anyway
  base_component(const base_component&) = delete;               //copy constructor
  base_component& operator=(const base_component&) = delete;    //copy assignment operator
  base_component(base_component&&) = delete;                    //move constructor  
  base_component& operator=(base_component&&) = delete;         //move assignment operator 
  //-------------------------------------------------------------------------------------------------------------------------------------


  virtual void properties(double p, double T, double strain_rate) {}
  virtual void properties(double p, double strain_rate) {}
  virtual void properties(double p, double T, const boost::numeric::ublas::vector<double> &Y, double strain_rate){}
  virtual void properties(double p, const boost::numeric::ublas::vector<double> &Y, double strain_rate){}

  virtual void properties(double p, double T, const boost::numeric::ublas::vector<double> &Y, double strain_rate, const std::string& dummy){}
  virtual void properties(double p, const boost::numeric::ublas::vector<double> &Y, double strain_rate, const std::string& dummy){}


  /// this is used in fluid_sc_isothermal to print properties for debugging purpose
  virtual void print_props_sc_isothermal()
  {
       print_in_file("properties", "rho", rho_);
       print_in_file("properties", "mu", mu_);
       print_in_file("properties", "sound_speed", sound_speed_);
       print_in_file("properties", "beta", beta_);
       print_in_file("properties", " ");
  }



  /// this is used in fluid_sc to print properties for debugging purpose
  virtual void print_props_sc()
  {
       print_in_file("properties", "rho", rho_);
       print_in_file("properties", "mu", mu_);
       print_in_file("properties", "kappa", kappa_);
       print_in_file("properties", "sound_speed", sound_speed_);
       print_in_file("properties", "alpha", alpha_);
       print_in_file("properties", "beta", beta_);
       print_in_file("properties", "cv", cv_);
       print_in_file("properties", "cp", cp_);
       print_in_file("properties", " ");
  }



  ///this prints properties in fluid_mc_isothermal for debugging purpose
  virtual void print_props_mc_isothermal()
  {
       print_in_file("properties", "rho", rho_);
       print_in_file("properties", "mu", mu_);
       print_in_file("properties", "sound_speed", sound_speed_);
       print_in_file("properties", "beta", beta_);
       print_in_file("properties", "rho_comp", rho_comp_);
       print_in_file("properties", "chemical_diffusivity_comp", chemical_diffusivity_comp_);
       print_in_file("properties", " ");
  }


  ///this prints properties in fluid_mc for debugging purpose
  virtual void print_props_mc()
  {
       print_in_file("properties", "rho", rho_);
       print_in_file("properties", "mu", mu_);
       print_in_file("properties", "kappa", kappa_);
       print_in_file("properties", "sound_speed", sound_speed_);
       print_in_file("properties", "alpha", alpha_);
       print_in_file("properties", "beta", beta_);
       print_in_file("properties", "cv", cv_);
       print_in_file("properties", "cp", cp_);
       print_in_file("properties", "rho_comp", rho_comp_);
       print_in_file("properties", "internal_energy_comp", internal_energy_comp_);
       print_in_file("properties", "chemical_diffusivity_comp", chemical_diffusivity_comp_);
       print_in_file("properties", " ");
  }



  virtual void clear()
  {
    rho_ = 0.0;
    cv_ = 0.0;
    cp_ = 0.0;
    kappa_ = 0.0;
    chemical_diffusivity_ = 0.0;
    mu_ = 0.0;
    sound_speed_ = 0.0;
    alpha_ = 0.0;
    beta_ = 0.0;
    
    std::fill(rho_comp_.begin(), rho_comp_.end(), 0.0);
    std::fill(chemical_diffusivity_comp_.begin(), chemical_diffusivity_comp_.end(), 0.0);
    std::fill(internal_energy_comp_.begin(), internal_energy_comp_.end(), 0.0);
  }







  virtual double rho()const{return rho_;}
  virtual double mu()const{return mu_;}
  virtual double cv()const{return cv_;}
  virtual double cp()const{return cp_;}
  virtual double kappa()const{return kappa_;}
  virtual double chemical_diffusivity()const{return chemical_diffusivity_;}
  virtual double sound_speed()const{return sound_speed_;}
  virtual double alpha()const{return alpha_;}
  virtual double beta()const{return beta_;}
  virtual double qL_dp()const{return qL_dp_;}
  virtual double qL_dT()const{return qL_dT_;}
  virtual double R()const{return R_;}
  virtual double gamma()const{return gamma_;}
  virtual double molar_mass()const{return molar_mass_;}
  virtual double molar_volume()const{return molar_volume_;}
  virtual double dV_dT()const{return dV_dT_;}
  virtual double dV_dp()const{return dV_dp_;}

  virtual std::vector<double> oxides_wf(){return oxides_wf_;}
  virtual std::vector<double> oxides_molar_mass(){return oxides_molar_mass_;}
  virtual std::vector<double> oxides_molar_fraction(){return oxides_molar_fraction_;}

  virtual void nb_comp(int i)
  {
     nb_comp_ = i;
     wf_.resize(i);
     vf_.resize(i);
     rho_comp_.resize(i);
     internal_energy_comp_.resize(i);
     chemical_diffusivity_comp_.resize(i);
  }
  
  virtual int nb_comp()const{return nb_comp_;}
  
  virtual boost::numeric::ublas::vector<double> wf()const{return wf_;}
  virtual boost::numeric::ublas::vector<double> vf()const{return vf_;}
  virtual boost::numeric::ublas::vector<double> rho_comp()const{return rho_comp_;}
  virtual boost::numeric::ublas::vector<double> internal_energy_comp()const{return internal_energy_comp_;}
  virtual boost::numeric::ublas::vector<double> chemical_diffusivity_comp()const{return chemical_diffusivity_comp_;}
  
  virtual double wf(int i)const{return wf_[i];}
  virtual double vf(int i)const{return vf_[i];}
  virtual double rho_comp(int i)const{return rho_comp_[i];}
  virtual double internal_energy_comp(int i)const{return internal_energy_comp_[i];}
  virtual double chemical_diffusivity_comp(int i)const{return chemical_diffusivity_comp_[i];}
  
  virtual boost::numeric::ublas::vector<double> wf_ph()const{return wf_ph_;}
  virtual boost::numeric::ublas::vector<double> vf_ph()const{return vf_ph_;}

  virtual double wf_ph(int i)const{return wf_ph_[i];}
  virtual double vf_ph(int i)const{return vf_ph_[i];}
  
//  virtual void comp_ptr(const std::shared_ptr<base_component>& ptr){comp_ptrs_.push_back(ptr);} 
  
  virtual std::vector<std::shared_ptr<base_component>>& comp_ptrs(){return comp_ptrs_;}
  virtual const std::vector<std::shared_ptr<base_component>>& comp_ptrs()const {return comp_ptrs_;}

  virtual std::shared_ptr<base_component>& comp_ptrs(int i){return comp_ptrs_[i];}
  virtual const std::shared_ptr<base_component>& comp_ptrs(int i)const {return comp_ptrs_[i];}
  
  




  
  int l_ = 0;
  int g_ = 1;
  int s_= 2;
  
        
  
  protected:

  double rho_ = 0.0;
  double cv_ = 0.0;
  double cp_ = 0.0;
  double kappa_ = 0.0;
  double chemical_diffusivity_ = 1.e-10;
  double mu_ = 0.0;
  double sound_speed_ = 0.0;
  double alpha_ = 0.0;
  double beta_ = 0.0;
  double qL_dp_ = 0.0;  // latent heat contribution w.r.t Pressure
  double qL_dT_ = 0.0;  // latent heat contribution w.r.t Temperature
  double R_ = 0.0;
  double gamma_ = 0.0;
  double molar_mass_ = 0.0;
  double molar_volume_ = 0.0;  
  double dV_dT_ = 0.0;
  double dV_dp_ = 0.0;

  std::vector<double> oxides_wf_ = std::vector<double>(10, 0.0);
  std::vector<double> oxides_molar_mass_ = std::vector<double>(10, 0.0); 
  std::vector<double> oxides_molar_fraction_ = std::vector<double>(10, 0.0);

  int nb_comp_ = 1;

  std::vector<std::shared_ptr<base_component>> comp_ptrs_;
  
  boost::numeric::ublas::vector<double> rho_comp_;
  boost::numeric::ublas::vector<double> internal_energy_comp_;
  boost::numeric::ublas::vector<double> chemical_diffusivity_comp_;

  boost::numeric::ublas::vector<double> wf_;     
  boost::numeric::ublas::vector<double> vf_;

  boost::numeric::ublas::vector<double> wf_ph_ = boost::numeric::ublas::vector<double>(3, 0.0);                       // wf_ph_[l_=0],  wf_ph_[g_=1],  wf_ph_[s_=2]
  boost::numeric::ublas::vector<double> vf_ph_ = boost::numeric::ublas::vector<double>(3, 0.0);                       // vf_ph_[l_=0],  vf_ph_[g_=1],  wf_ph_[s_=2]
  
  
};



}


#endif
