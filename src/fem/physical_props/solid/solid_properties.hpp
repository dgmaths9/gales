#ifndef GALES_SOLID_PROPERTIES_HPP
#define GALES_SOLID_PROPERTIES_HPP



#include "solid_properties_reader.hpp"



namespace GALES {


 /**
      This class defined the physical properties of material.
      Propeties are read from "props.txt" by solid_properties_reader class       
 */



class solid_properties
{
  
  public:

  //--------------------------------constructor------------------------------------
  solid_properties()
  {  
      solid_properties_reader reader(*this);
  }
  //-------------------------------------------------------------------------------



  //-------------------------------------------------------------------------------------------------------------------------------------        
  /// Deleting the copy and move constructors - no duplication/transfer in anyway
  solid_properties(const solid_properties&) = delete;               //copy constructor
  solid_properties& operator=(const solid_properties&) = delete;    //copy assignment operator
  solid_properties(solid_properties&&) = delete;                    //move constructor  
  solid_properties& operator=(solid_properties&&) = delete;         //move assignment operator 
  //-------------------------------------------------------------------------------------------------------------------------------------



  

  //-------------------------------------------------------------------------------
  void properties(double x, double y)
  {
    if(heterogeneous_layers())
    {
      layer_properties(x,y);
    }
    else if(heterogeneous_pointwise())
    {
      pointwise_properties(x,y);
    }
  }
  //-------------------------------------------------------------------------------





  //-------------------------------------------------------------------------------
  void properties(double x, double y, double z)
  {
    if(heterogeneous_layers())
    {
      layer_properties(x,y,z);
    }
    else if(heterogeneous_pointwise())
    {
      pointwise_properties(x,y,z);    
    }
  }
  //-------------------------------------------------------------------------------





  //-------------------------------------------------------------------------------
  void get_layer_props(int i)
  {
            rho_ = layers_rho_[i];
            E_ = layers_E_[i];
            nu_ = layers_nu_[i];  
  }
  //-------------------------------------------------------------------------------






  //-------------------------------------------------------------------------------
  void layer_properties(double x, double y)
  {
     if(layering_style_ == "x-wise")
     {
       for(int i=0; i<num_layers_; i++)
        if(layers_bounds_[i].first < x && x <= layers_bounds_[i].second)          // ( x ] 
          get_layer_props(i);
      }  

     else if(layering_style_ == "y-wise")
     {
       for(int i=0; i<num_layers_; i++)
        if(layers_bounds_[i].first < y && y <= layers_bounds_[i].second)      // ( y ] 
          get_layer_props(i);
      }  
  } 
  //-------------------------------------------------------------------------------
   




  //-------------------------------------------------------------------------------
  void layer_properties(double x, double y, double z)
  {
     if(layering_style_ == "x-wise")
     {
       for(int i=0; i<num_layers_; i++)
        if(layers_bounds_[i].first <= x && x <= layers_bounds_[i].second)
          get_layer_props(i);
      }  

     else if(layering_style_ == "y-wise")
     {
       for(int i=0; i<num_layers_; i++)
        if(layers_bounds_[i].first <= y && y <= layers_bounds_[i].second)
          get_layer_props(i);
      }  
     else if(layering_style_ == "z-wise")
     {
       for(int i=0; i<num_layers_; i++)
        if(layers_bounds_[i].first <= z && z <= layers_bounds_[i].second)
          get_layer_props(i);
      }  
  } 
  //-------------------------------------------------------------------------------
   
  
  



  //-------------------------------------------------------------------------------
  int layer_indx(const std::vector<double> v, double c)
  {    
    int last = v.size()-1;
    int index;
    if((c < v[0]) || (v[0]<=c && c<v[1]) ) index = 0;   // if x(mesh) < min_x (tomography)  then we set index = 0    
    else if((c > v[last]) || (v[last-1]<=c && c<=v[last]))    index = last-1;  // if x(mesh) > max_x (tomography)  then we set index = last-1  
    else
    {   
      for(int i=1; i<last-1; i++)
        if(v[i] <= c && c < v[i+1])
           index = i; 
    }   
        
    
    return index;
  }
  //-------------------------------------------------------------------------------



   


  //-------------------------------------------------------------------------------  
  void pointwise_properties(double x, double y)
  {  
    /* 
        for Owen's simulation: 
        Instead of using the numbers given by topography and tomography data in UTM coordinate system
        without any reason, he translated all the coordinates to include (0,0)
        He could just simply use the original coordinates. 
        Now all the implementation as well as the comparison has to account this traslation.
               
        -----------tomography data bounds ------------------- 
                  800 <= x <= 100800
                  -28350 <= y <= -1350

        -----------mesh bounds ------------------- 
                  815 <= x <= 100711
               -50000 <= y <= -81
    */
    
    if(y<= -25000)
    {
       rho_ = 3300.0;
       E_ = 125510000000.0;
       nu_ = 0.25;
    }
    else
    {
        int x0 = layer_indx(x_, x),   x1 = x0+1;      
        int y0 = layer_indx(y_, y),   y1 = y0+1;
      
        double xd = (x - x_[x0])/(x_[x1] - x_[x0]);
        double yd = (y - y_[y0])/(y_[y1] - y_[y0]);
                     
        rho_ = ((rho_vec_[x_.size()*y0 + x0]*(1 - xd) + rho_vec_[x_.size()*y0 + x1]*xd)*(1-yd) + (rho_vec_[x_.size()*y1 + x0]*(1 - xd) + rho_vec_[x_.size()*y1 + x1]*xd)*yd);   
               
        E_ = ((E_vec_[x_.size()*y0 + x0]*(1 - xd) + E_vec_[x_.size()*y0 + x1]*xd)*(1-yd) + (E_vec_[x_.size()*y1 + x0]*(1 - xd) + E_vec_[x_.size()*y1 + x1]*xd)*yd);
               
        nu_ = ((nu_vec_[x_.size()*y0 + x0]*(1 - xd) + nu_vec_[x_.size()*y0 + x1]*xd)*(1-yd) + (nu_vec_[x_.size()*y1 + x0]*(1 - xd) + nu_vec_[x_.size()*y1 + x1]*xd)*yd);   
    }
  }
  //-------------------------------------------------------------------------------
  




   

  //-------------------------------------------------------------------------------  
  void pointwise_properties(double x, double y, double z)
  {  
     
    /* 
        -----------tomography data bounds ------------------- 
                  476,000 <= x <= 524,000
      4,152,191.488873656 <= y <= 4,200,191.488873656
                   -25000 <= z <= 2000

        -----------mesh bounds ------------------- 
                  474,000 <= x <= 524,000
                 4,151000 <= y <= 4200200
                   -25000 <= z <= 2000
    */

    if(z<= -25000)
    {
       rho_ = 3300.0;
       E_ = 125510000000.0;
       nu_ = 0.25;
    }
    else
    {
        int x0 = layer_indx(x_, x),   x1 = x0+1;      
        int y0 = layer_indx(y_, y),   y1 = y0+1;
        
        int z0;
        
        if(z<=2000.0) z0 = layer_indx(z_, z);
        else  z0 = z_.size()-2;
        
        int z1 = z0 + 1;
      
        double xd = (x - x_[x0])/(x_[x1] - x_[x0]);
        double yd = (y - y_[y0])/(y_[y1] - y_[y0]);
        double zd = (z - z_[z0])/(z_[z1] - z_[z0]);                    
            
        rho_ = ((rho_vec_[x_.size()*y_.size()*z0 + x_.size()*y0 + x0]*(1 - xd) + rho_vec_[x_.size()*y_.size()*z0 + x_.size()*y0 + x1]*xd)*(1-yd) + 
                (rho_vec_[x_.size()*y_.size()*z0 + x_.size()*y1 + x0]*(1 - xd) + rho_vec_[x_.size()*y_.size()*z0 + x_.size()*y1 + x1]*xd)*yd)*(1 - zd) +   
               ((rho_vec_[x_.size()*y_.size()*z1 + x_.size()*y0 + x0]*(1 - xd) + rho_vec_[x_.size()*y_.size()*z1 + x_.size()*y0 + x1]*xd)*(1-yd) + 
                (rho_vec_[x_.size()*y_.size()*z1 + x_.size()*y1 + x0]*(1 - xd) + rho_vec_[x_.size()*y_.size()*z1 + x_.size()*y1 + x1]*xd)*yd)*zd;
               
        E_ = ((E_vec_[x_.size()*y_.size()*z0 + x_.size()*y0 + x0]*(1 - xd) + E_vec_[x_.size()*y_.size()*z0 + x_.size()*y0 + x1]*xd)*(1-yd) + 
              (E_vec_[x_.size()*y_.size()*z0 + x_.size()*y1 + x0]*(1 - xd) + E_vec_[x_.size()*y_.size()*z0 + x_.size()*y1 + x1]*xd)*yd)*(1 - zd) +   
             ((E_vec_[x_.size()*y_.size()*z1 + x_.size()*y0 + x0]*(1 - xd) + E_vec_[x_.size()*y_.size()*z1 + x_.size()*y0 + x1]*xd)*(1-yd) + 
              (E_vec_[x_.size()*y_.size()*z1 + x_.size()*y1 + x0]*(1 - xd) + E_vec_[x_.size()*y_.size()*z1 + x_.size()*y1 + x1]*xd)*yd)*zd;
               
        nu_ = ((nu_vec_[x_.size()*y_.size()*z0 + x_.size()*y0 + x0]*(1 - xd) + nu_vec_[x_.size()*y_.size()*z0 + x_.size()*y0 + x1]*xd)*(1-yd) + 
               (nu_vec_[x_.size()*y_.size()*z0 + x_.size()*y1 + x0]*(1 - xd) + nu_vec_[x_.size()*y_.size()*z0 + x_.size()*y1 + x1]*xd)*yd)*(1 - zd) +   
              ((nu_vec_[x_.size()*y_.size()*z1 + x_.size()*y0 + x0]*(1 - xd) + nu_vec_[x_.size()*y_.size()*z1 + x_.size()*y0 + x1]*xd)*(1-yd) + 
               (nu_vec_[x_.size()*y_.size()*z1 + x_.size()*y1 + x0]*(1 - xd) + nu_vec_[x_.size()*y_.size()*z1 + x_.size()*y1 + x1]*xd)*yd)*zd;
    }        
  }
  //-------------------------------------------------------------------------------
  




  
  
  
   
  //-------------------------------------------------------------------------------  
  void material_type(const std::string& s){material_type_ = s;}   
  auto material_type()const {return material_type_;}   
  
  void condition_type(const std::string& s){condition_type_ = s;}   
  auto condition_type()const {return condition_type_;}   
  
  void rho(double x){rho_ = x;}
  auto rho() const {return rho_;}
    
  void E(double x){E_ = x;}
  auto E() const {return E_;}  

  void nu(double x){nu_ = x;}
  auto nu() const {return nu_;}  
  
  void alpha(double x){alpha_ = x;}
  auto alpha() const {return alpha_;}    

  void T_ref(double x){T_ref_ = x;}
  auto T_ref() const {return T_ref_;}  

  void a_damping(double x){a_damping_ = x;}
  auto a_damping() const {return a_damping_;}  

  void b_damping(double x){b_damping_ = x;}
  auto b_damping() const {return b_damping_;}  
  
  void nb_Maxwell_el(int x){nb_Maxwell_el_ = x;}
  auto nb_Maxwell_el() const {return nb_Maxwell_el_;}
    
  void Max_el_E(const std::vector<double>& v){Max_el_E_ = v;}
  auto Max_el_E() const {return Max_el_E_;}  
  
  void Max_el_eta(const std::vector<double>& v){Max_el_eta_ = v;}
  auto Max_el_eta() const {return Max_el_eta_;}  

  void heterogeneous(bool f) {heterogeneous_ = f;}  
  auto heterogeneous() const {return heterogeneous_;}  

  void heterogeneous_layers(bool f) {heterogeneous_layers_ = f;}  
  bool heterogeneous_layers() const {return heterogeneous_layers_;}  
  
  void num_layers(int i){num_layers_ = i;}
  auto num_layers() const {return num_layers_;}  
  
  void layering_style(const std::string& s){layering_style_ = s;}  
  void layers_bounds(const std::pair<double, double>& layer_bounds) {layers_bounds_.push_back(layer_bounds);}
  
  void layers_rho(double d){layers_rho_.push_back(d);}
  auto layers_rho() const {return layers_rho_;}  

  void layers_E(double d){layers_E_.push_back(d);}
  auto layers_E() const {return layers_E_;}  

  void layers_nu(double d){layers_nu_.push_back(d);}
  auto layers_nu() const {return layers_nu_;}  
  
  
  void heterogeneous_pointwise(bool f) {heterogeneous_pointwise_ = f;}  
  bool heterogeneous_pointwise() const {return heterogeneous_pointwise_;}  



  void heterogeneous_pointwise_data_3d(const std::string& input_file_name)
  {
      print_only_pid<0>(std::cerr)<<"--------heterogeneous_pointwise_data_3d-------"<<"\n";             

      std::string s = "input/"+input_file_name;
      std::ifstream file(s);
      if(!file.is_open())  Error("unable to open and read "+input_file_name);            
     
      int size;
      file>>size;
      
      std::vector<double> x(size), y(size), z(size);
      rho_vec_.resize(size), E_vec_.resize(size), nu_vec_.resize(size);
      for(int i=0; i<size; i++)
      {
        file>>x[i];   file>>y[i];   file>>z[i];  file>>rho_vec_[i];   file>>E_vec_[i];   file>>nu_vec_[i];
      }
      file.close();     
      
      x_ = sort_unique_vector(x); 
      y_ = sort_unique_vector(y); 
      z_ = sort_unique_vector(z); 
            
      print_only_pid<0>(std::cerr)<<"--------heterogeneous_pointwise_data_3d read-------"<<"\n";             
  }
  


  void heterogeneous_pointwise_data_2d(const std::string& input_file_name)
  {
      print_only_pid<0>(std::cerr)<<"--------heterogeneous_pointwise_data_2d-------"<<"\n";      
             
      std::string s = "input/"+input_file_name;
      std::ifstream file(s);
      if(!file.is_open())  Error("unable to open and read "+input_file_name);            
     
      int size;
      file>>size;
      
      std::vector<double> x(size), y(size);
      rho_vec_.resize(size), E_vec_.resize(size), nu_vec_.resize(size);
      for(int i=0; i<size; i++)
      {
        file>>x[i];   file>>y[i];   file>>rho_vec_[i];   file>>E_vec_[i];   file>>nu_vec_[i];
      }
      file.close();      

      x_ = sort_unique_vector(x); 
      y_ = sort_unique_vector(y); 
    
      print_only_pid<0>(std::cerr)<<"--------heterogeneous_pointwise_data_2d read-------"<<"\n";             

  }
  //-------------------------------------------------------------------------------
    
   
   

  private:
   
  std::string material_type_ = "Hookes";

  // Plain_Strain: one dimension very large in comparison to other two; e.g. beam  
  // Plain_Stress: one dimesion is very small in comparison to other two; e.g. thin plate
  std::string condition_type_ = "";  
      
  double rho_ = 0.0;
  double E_ = 0.0;
  double nu_ = 0.0;   
  double alpha_ = 0.0;
  double T_ref_ = 0.0; 
  
  double a_damping_ = 0.0;
  double b_damping_ = 0.0;

  int nb_Maxwell_el_ = 1;
  std::vector<double> Max_el_E_;
  std::vector<double> Max_el_eta_;

  bool heterogeneous_ = false;

  bool heterogeneous_layers_ = false;
  std::string layering_style_;
  int num_layers_ = 0;
  std::vector<std::pair<double, double>> layers_bounds_;   // This is a closed interval such as [0, -1000]  [-1000.1, -2000] ..
  std::vector<double> layers_rho_;
  std::vector<double> layers_E_;
  std::vector<double> layers_nu_;
    
  bool heterogeneous_pointwise_ = false;
  std::vector<double> rho_vec_;
  std::vector<double> E_vec_;
  std::vector<double> nu_vec_;
    
  
  std::vector<double> x_, y_, z_;  

  
      
   
};


}

#endif
