#ifndef GALES_HEAT_EQ_PROPERTIES_READER_HPP
#define GALES_HEAT_EQ_PROPERTIES_READER_HPP



#include "materials_heat_eq.hpp"



namespace GALES {



   /**
        This class reads heat eqn properties.
   */






class heat_eq_properties_reader
{

  using vec = boost::numeric::ublas::vector<double>;
  
  public:

  template<typename T>
  heat_eq_properties_reader(T& props)
  {  
      std::vector<std::string> s;
      std::ifstream file("props.txt");
      if(!file.is_open())
      {
         Error("unable to open and read  'props.txt' "); 
      }

      while(file)
      {
         read_one_line(file, s);         
         if(s[0] == "heat_equation")
         {
            read_heat_eq_props(file, s, props);
         }
      }
      file.close();                  
  }





  //-------------------------------------------------------------------------------------------------------------------------------------        
  /// Deleting the copy and move constructors - no duplication/transfer in anyway
  heat_eq_properties_reader(const heat_eq_properties_reader&) = delete;               //copy constructor
  heat_eq_properties_reader& operator=(const heat_eq_properties_reader&) = delete;    //copy assignment operator
  heat_eq_properties_reader(heat_eq_properties_reader&&) = delete;                    //move constructor  
  heat_eq_properties_reader& operator=(heat_eq_properties_reader&&) = delete;         //move assignment operator 
  //-------------------------------------------------------------------------------------------------------------------------------------







  template<typename T>
  void read_heat_eq_props(std::ifstream& file, std::vector<std::string>& s, T& props)
  {
     while(file)
     {
         read_one_line(file, s); 

         if(s[0] == "{")
         {
              print_only_pid<0>(std::cerr)<<"\n"<<"---------------------------------- heat eq material ---------------------------------------"<<"\n";                      
         } 
         else if(s[0] == "custom")
         {
            read_custom_material(file, s, props); 
            props.set_material_type("custom");        
         }
         else if(s[0] == "y-dependent-class")
         {
            read_y_dependent_class(file, s, props);
            props.set_material_type("y-dependent-class");        
         }
         
         else if(s[0] == "}")
         {
            print_only_pid<0>(std::cerr)<<"----------------------------------------------------------------------------------------"<<"\n";             
            return;
         }
     }    
  }




   






  template<typename T>
  void read_custom_material(std::ifstream& file, std::vector<std::string>& s, T& props)
  {       
     double rho(0.0), cp(0.0), cv(0.0), kappa(0.0), alpha(0.0), mu(0.0), heat_source(0.0);
     while(file)
     {
         read_one_line(file, s);          

         if(s[0] == "{")
         {
           print_only_pid<0>(std::cerr)<<"    custom material:  "<<"\n\n";         
         }
         else if(s[0] == "rho")
         {
           rho = stod(s[1]);
           print_only_pid<0>(std::cerr)<<"            rho: "<<rho<<"\n";
         }               
         else if(s[0] == "cp")
         {
           cp = stod(s[1]);             
           print_only_pid<0>(std::cerr)<<"            cp: "<<cp<<"\n";
         }  
         else if(s[0] == "cv")
         {
           cv = stod(s[1]);             
           print_only_pid<0>(std::cerr)<<"            cv: "<<cv<<"\n";
         }  
         else if(s[0] == "mu")
         {
           mu = stod(s[1]);             
           print_only_pid<0>(std::cerr)<<"            mu: "<<mu<<"\n";
         }  
         else if(s[0] == "kappa")
         {
           kappa = stod(s[1]);   
           print_only_pid<0>(std::cerr)<<"            kappa: "<<kappa<<"\n";
         }  
         else if(s[0] == "alpha")
         {
           alpha = stod(s[1]);   
           print_only_pid<0>(std::cerr)<<"            alpha: "<<alpha<<"\n";
         }  
         else if(s[0] == "heat_source")
         {
           heat_source = stod(s[1]);   
           print_only_pid<0>(std::cerr)<<"            heat_source: "<<heat_source<<"\n";
         } 
         else if(s[0] == "}")
         {    
            std::shared_ptr<base_heat_props> ptr = std::make_shared<custom_material_heat_eq>(rho, cp, cv, kappa, alpha, mu, heat_source);
            props.material_ptr() = ptr;
            return;
         }   
     }               
  }












  /**
       This function reads a class for a given class_name; 
       This is used when nothing is read as input and everything is defined in the class       
  */
  template<typename T>
  void read_y_dependent_class(std::ifstream& file, std::vector<std::string>& s, T& props)
  {
     std::string name;
     while(file)
     {
       read_one_line(file, s);     

       if(s[0] == "{")
       {
          /* do nothing */
       }
       else if(s[0] == "steam_water")
       {
          print_only_pid<0>(std::cerr)<<"    steam_water class:  \n";   
          auto y_min =  stod(s[1]);               
          auto y_max =  stod(s[2]);               
          if(y_max <= y_min) 
          {
             std::stringstream ss;
             ss<<"The y_max is defined before y_min;   y_min should come before y_max"<<std::endl;
             ss<<"for example    steam_water   <y_min>   <y_max>"<<std::endl;
             Error(ss.str()); 
          }   
          std::shared_ptr<base_heat_props> ptr = std::make_shared<steam_water>();
          props.material_ptrs().push_back(ptr);          
          props.class_y().push_back(std::tuple<double, double>(y_min, y_max));
       }       
       else if(s[0] == "magma_krafla")
       {
          print_only_pid<0>(std::cerr)<<"    magma_krafla class:  \n";   
          auto y_min =  stod(s[1]);               
          auto y_max =  stod(s[2]);               
          if(y_max <= y_min) 
          {
             std::stringstream ss;
             ss<<"The y_max is defined before y_min;   y_min should come before y_max"<<std::endl;
             ss<<"for example    magma_krafla   <y_min>   <y_max>"<<std::endl;
             Error(ss.str()); 
          }   
          std::shared_ptr<base_heat_props> ptr = std::make_shared<magma_krafla>();
          props.material_ptrs().push_back(ptr);          
          props.class_y().push_back(std::tuple<double, double>(y_min, y_max));          
       }       
       else if(s[0] == "}")
       {
          return;
       }              
     }  
  } 



     
};


}

#endif
