#ifndef GALES_MATERIALS_HEAT_EQ_HPP
#define GALES_MATERIALS_HEAT_EQ_HPP



#include <string>
#include <vector>
#include "base_heat_props.hpp"



namespace GALES {





    /// this is for custom material
    class custom_material_heat_eq : public base_heat_props
    {
       public:
       
       custom_material_heat_eq(double rho, double cp, double cv, double kappa, double alpha, double mu, double heat_source)
       {
          rho_ = rho;
          cp_ = cp;
          cv_ = cv;
          mu_ = mu;
          kappa_ = kappa;      
          alpha_ = alpha;  
          heat_source_ = heat_source_;    
       } 
    
      //-------------------------------------------------------------------------------------------------------------------------------------        
      /// Deleting the copy and move constructors - no duplication/transfer in anyway
      custom_material_heat_eq(const custom_material_heat_eq&) = delete;               //copy constructor
      custom_material_heat_eq& operator=(const custom_material_heat_eq&) = delete;    //copy assignment operator
      custom_material_heat_eq(custom_material_heat_eq&&) = delete;                    //move constructor  
      custom_material_heat_eq& operator=(custom_material_heat_eq&&) = delete;         //move assignment operator 
      //-------------------------------------------------------------------------------------------------------------------------------------
    
    };
    
    






    class steam_water: public base_heat_props
    {
       using vec = boost::numeric::ublas::vector<double>;
       using mat = boost::numeric::ublas::matrix<double>;
       
       public:

       steam_water()
       {
          read_size_and_vec("input/lookuptable/T.txt", T_vec_, size_T_);
          read_size_and_vec("input/lookuptable/p.txt", p_vec_, size_p_);
          
          read_mat("input/lookuptable/steam/rho.txt", steam_rho_, size_T_, size_p_);
          read_mat("input/lookuptable/steam/cp.txt", steam_cp_, size_T_, size_p_);
          read_mat("input/lookuptable/steam/cv.txt", steam_cv_, size_T_, size_p_);
          read_mat("input/lookuptable/steam/mu.txt", steam_mu_, size_T_, size_p_);
          read_mat("input/lookuptable/steam/kappa.txt", steam_kappa_, size_T_, size_p_);           
          read_mat("input/lookuptable/steam/alpha.txt", steam_alpha_, size_T_, size_p_);           
       }
                     

       
       void properties(double x, double y, double p, double T) final
       {
          rho_ = bilinear_interpolation(T, p, *T_vec_, *p_vec_, *steam_rho_, "T", "p", "steam_water::rho");
          cp_ = bilinear_interpolation(T, p, *T_vec_, *p_vec_, *steam_cp_, "T", "p", "steam_water::cp");
          cv_ = bilinear_interpolation(T, p, *T_vec_, *p_vec_, *steam_cv_, "T", "p", "steam_water::cv");
          mu_ = bilinear_interpolation(T, p, *T_vec_, *p_vec_, *steam_mu_, "T", "p", "steam_water::mu");
          kappa_ = bilinear_interpolation(T, p, *T_vec_, *p_vec_, *steam_kappa_, "T", "p", "steam_water::kappa");         
          alpha_ = bilinear_interpolation(T, p, *T_vec_, *p_vec_, *steam_alpha_, "T", "p", "steam_water::alpha");         
       } 


     private:
       
     int size_p_;
     int size_T_;
      
     std::unique_ptr<vec> p_vec_ = nullptr; 
     std::unique_ptr<vec> T_vec_ = nullptr;
     
     std::unique_ptr<mat> steam_rho_ = nullptr;    // function of T, p 
     std::unique_ptr<mat> steam_cp_ = nullptr;     // function of T, p
     std::unique_ptr<mat> steam_cv_ = nullptr;  // function of T, p    
     std::unique_ptr<mat> steam_mu_ = nullptr;  // function of T, p    
     std::unique_ptr<mat> steam_kappa_ = nullptr;  // function of T, p    
     std::unique_ptr<mat> steam_alpha_ = nullptr;     // function of T, p
    };









    class magma_krafla: public base_heat_props
    {
       using vec = boost::numeric::ublas::vector<double>;
       using mat = boost::numeric::ublas::matrix<double>;
       
       public:

       magma_krafla()
       {
          read_size_and_vec("input/lookuptable/T.txt", T_vec_, size_T_);
          read_size_and_vec("input/lookuptable/p.txt", p_vec_, size_p_);
          read_size_and_vec("input/lookuptable/TG.txt", Tg_vec_, size_Tg_);
          
          read_mat("input/lookuptable/magma/w_h2o.txt", w_h2o_mat_, size_T_, size_p_);
          
          read_size_and_vec("input/lookuptable/w_h2o_vec.txt", w_h2o_vec_, size_Tg_);   //  size of w_h2o_vec_ != size of Tg_vec_ 
          
          read_mat("input/lookuptable/magma/gas_co2.txt", gas_co2_, size_T_, size_Tg_);        
          read_mat("input/lookuptable/magma/eps_h2o.txt", eps_h2o_, size_T_, size_Tg_);
          read_mat("input/lookuptable/magma/rhog_h2o.txt", rhog_h2o_, size_T_, size_Tg_);
          read_mat("input/lookuptable/magma/rhom_h2o.txt", rhom_h2o_, size_T_, size_Tg_);
          read_mat("input/lookuptable/magma/alpha_h2o.txt", alpha_h2o_, size_T_, size_Tg_);
          read_mat("input/lookuptable/magma/mu.txt", magma_mu_, size_T_, size_Tg_);
          
           
          std::vector<double> oxides_molar_mass = {60.084, 79.876, 101.956, 159.692, 71.846, 70.937, 40.304, 56.078, 61.979, 94.195};   // g/mol  
          std::vector<double> oxides_wf = {0.7653, 0.0032, 0.1201, 0.01365, 0.01365, 0.0006, 0.0018, 0.0132, 0.0378, 0.0306};
          std::vector<double> cp_L_bar = {1331., 1392., 1545., 1434., 1100., 0., 2424., 1781., 1651., 1030.};                            

          for(int i=0; i<10; i++)  cp_L_ += oxides_wf[i]*cp_L_bar[i];          // Stebbins et al., 1984;   Lesher and Spera, 2015;    Bouhifd et al., 2006
            
          for(auto& a: oxides_molar_mass) a *= 1.e-3;        // in kg
            
          auto molar_mass = 0.0;
          for (std::size_t i=0; i<10; i++)   molar_mass += oxides_wf[i]/oxides_molar_mass[i];//sommatoria
          molar_mass = 1./molar_mass;      // in kg
          
          oxides_molar_fraction_.resize(10);     // se conti gli ossidi sono 10 
          for(std::size_t i=0; i<10; i++)   oxides_molar_fraction_[i] =  oxides_wf[i]*molar_mass/oxides_molar_mass[i];
          
          for(int i=0; i<10; i++) pmav_ += oxides_molar_mass[i]*oxides_molar_fraction_[i]; //questo è per grammi
          
          
          set_pressure_profile_magma();
       }
       
       
       
       
       void set_pressure_profile_magma()
       {
          double h = 0.01;
          double max = -2104.0;
          double min = -2129.0;
          int steps = (int)((max-min)/h);  
  
          p_vec_magma_static_.resize(steps+1); 
          y_vec_.resize(steps+1);
    
          for(int i=0; i<=steps; i++)
            y_vec_[i] = max - h*i;
  
          
          // Runge kutta
          double m1,m2,m3,m4;
          p_vec_magma_static_[0] = 45.4e6;
  
          for(int i=0; i<steps; i++)
          {
            compute_props(p_vec_magma_static_[i], 1173.15);
            m1 = 9.81*rho();
            compute_props(p_vec_magma_static_[i]+0.5*h*m1, 1173.15);
            m2 = 9.81*rho();
            compute_props(p_vec_magma_static_[i]+0.5*h*m2, 1173.15);
            m3 = 9.81*rho();          
            compute_props(p_vec_magma_static_[i]+h*m3, 1173.15);
            m4 = 9.81*rho();          
            p_vec_magma_static_[i+1] = p_vec_magma_static_[i]+h/6.*(m1+2.0*(m2+m3)+m4);
          }               
       }
       
       

      


       void properties(double x, double y, double p, double T) final
       {        
          p = p_interpolation(y_vec_, p_vec_magma_static_, y);
          compute_props(p, T);
       } 





     void compute_props(double p, double T)
     {
          auto w_h2o = bilinear_interpolation(T, p, *T_vec_, *p_vec_, *w_h2o_mat_, "T", "p", "magma_krafla::w_h2o");
          
          rho_ = bilinear_interpolation(T, w_h2o, *T_vec_, *w_h2o_vec_, *rhom_h2o_, "T", "w_h2o", "magma_krafla::rhom_h2o");
          alpha_ = bilinear_interpolation(T, w_h2o, *T_vec_, *w_h2o_vec_, *alpha_h2o_, "T", "w_h2o", "magma_krafla::alpha_h2o");

          mu_ =  bilinear_interpolation(T, w_h2o, *T_vec_, *w_h2o_vec_, *magma_mu_, "T", "w_h2o", "magma_krafla::mu");
          //if(mu_>=10.0)  mu_ = 10.0;
          mu_ = std::pow(10.0, mu_);
      
          //-----------------------------interpolation ------------------------------
          auto cp_L = (1.0 - w_h2o)*cp_L_ + w_h2o*4717.0; //Bouhifd et al 2006 eq. 4; 4717 = (~85(mol)/18(massa mol acqua) * 1e3(kg))  kg
          auto cp_D = cp_L; 
           
          auto alpha_D_L = (-0.06 + 4.57e-4*T)*1.e-6; //Moitra et al., 2018  1e-6 è per passare da mm2 a m2  
          auto alpha_D = alpha_D_L;
          
          auto delta_w_h2o = (*w_h2o_vec_)[1]-(*w_h2o_vec_)[0];          
          auto w_h2o_ind_l = (int)floor((w_h2o-(*w_h2o_vec_)[0])/delta_w_h2o);
          auto w_h2o_ind_r = w_h2o_ind_l + 1;
            
          auto T_G = ((*Tg_vec_)[w_h2o_ind_r] - (*Tg_vec_)[w_h2o_ind_l]) * (w_h2o - (*w_h2o_vec_)[w_h2o_ind_l])/delta_w_h2o + (*Tg_vec_)[w_h2o_ind_l];

          if (T < T_G)    // glass transition occurs
          {
             auto delta_tg = 100.0;     
             auto T_G_right = T_G;
             auto T_G_left = T_G - delta_tg;
                 
             // specific heat for water-free glass(Richet, 1987)
             auto cp_GL = 0.0;
             for(int i=0; i<a_.size(); i++)
                cp_GL += oxides_molar_fraction_[i]*(a_[i] + b_[i]*T*1.e-3 + c_[i]*1.e5/(T*T) + d_[i]/sqrt(T));  //eq. 3 J/ mol K
             cp_GL = cp_GL/pmav_;   // this is to convert from cp/mol to cp/kg
                
             //add dissolved water (Bouhifd et al. 2006)
             auto cpwgl = -122.319 + (341.631*1.e-3*T) + (63.4426*1.e5/(T*T));   // J/ mol K eq. 3 
             cpwgl *= 1.e3/18.01528; //J/ kg K
             //auto x_h2o_l = 1.0/(1.0+((1.0-w_h2o)*18.01528)/(w_h2o*pmav_)); 
             //cpwgl = cpwgl*1.e-3/x_h2o_l;
             cp_GL = w_h2o*cpwgl + (1.0-w_h2o)*cp_GL; //eq. 4
              
      
             auto alpha_D_GL = (5.98*std::pow(T,-0.41) + 9.23e-5*T)*1.e-6;            //Moitra et al., 2018 1e-6 per passare da mm2 a m2
             cp_D = cp_GL;
             alpha_D =  alpha_D_GL;
             
               
             if (T_G_left <= T) 
             {
     
                 cp_D = (cp_L*(T-T_G_left) + cp_GL*(T_G_right-T))/delta_tg;  
                 alpha_D = (alpha_D_L*(T-T_G_left) + alpha_D_GL*(T_G_right- T))/delta_tg;   
             }
          }    
               

          //------------------------------------------Correct cp for magma porosity (h2o and co2)------------------------------------
          // Regression of data from classical engineering handbooks
          // cp: J/Kg K     T: K       Range: 175 < T(K) < 1500
          auto cp_G_h2o = 1840.4 - 0.14972*T + 9.145e-4*T*T - 3.1759e-7*T*T*T; 
          auto cp_G_co2 = 472.27 + 1.5228*T - 1.014e-3*T*T + 2.5368e-7*T*T*T;  
          auto gas_co2 = bilinear_interpolation(T, w_h2o, *T_vec_, *w_h2o_vec_, *gas_co2_, "T", "w_h2o", "magma_krafla::gas_co2"); 
          auto cp_G = (1.0-gas_co2)*cp_G_h2o + gas_co2*cp_G_co2;
         
          auto eps = bilinear_interpolation(T, w_h2o, *T_vec_, *w_h2o_vec_, *eps_h2o_, "T", "w_h2o", "magma_krafla::eps_h2o");
          auto rho_G = bilinear_interpolation(T, w_h2o, *T_vec_, *w_h2o_vec_, *rhog_h2o_, "T", "w_h2o", "magma_krafla::rhog_h2o");     
          auto rho_D = (rho_ - eps*rho_G)/(1.0-eps);
          cp_ = (rho_D*cp_D*(1.0-eps) + rho_G*cp_G*eps)/(rho_D*(1.0-eps) + rho_G*eps);    // Heap et al., 2020 eq. 3
          //----------------------------------------------------------------------------------------------------------------------------
                    

          //--------------------------------------------compute cv-----------------------------(slide 17)--------------------------------------
          auto cv_D = cp_D;          
          auto R_G = 8.3144598*1.e3/((1.0-gas_co2)*18.01528 + gas_co2*44.01);      
          auto cv_G = cp_G - R_G;
          cv_ = (rho_D*cv_D*(1.0-eps) + rho_G*cv_G*eps)/(rho_D*(1.0-eps) + rho_G*eps);    // Heap et al., 2020       
          //----------------------------------------------------------------------------------------------------------------------------

         
          //--------------------------------------------kappa for dense phase + porosity---------(slide 18)-------------------------------     
          auto k_D = rho_D*cp_D*alpha_D; // Moitra et al., 2018 eq. 2          
          auto k_h2o = 4.1771e-3 + 2.2849e-5*T + 9.4663e-8*T*T - 2.5778e-11*T*T*T;
          auto k_co2 = -1.0081e-2 + 9.1422e-5*T - 6.4318e-9*T*T - 4.31e-12*T*T*T;
    
          auto x_h2o = 1.0/((1.0+ gas_co2*18.01528)/((1.0-gas_co2)*44.01));      
          auto x_co2 = 1.0 - x_h2o;         
              
          auto k_G = 0.5*((k_h2o*k_co2/(x_h2o*k_co2 + x_co2*k_h2o)) + x_h2o*k_h2o + x_co2*k_co2);      //Udoetok 2013 eq.16
          auto m = k_G/k_D;                    
          auto beta = 3.0*(1-m)/(2+m);                // for spherical pores (Zimmermann 1989)
          kappa_ = (k_D*(1-eps)*(1-m) + m*beta*eps)/((1-eps)*(1-m) + beta*eps);    // Maxwell equation  Heap et al., 2020  
          //----------------------------------------------------------------------------------------------------------------------------
     } 




     private:
 
     boost::numeric::ublas::vector<double> y_vec_, p_vec_magma_static_;
      
     int size_p_;
     int size_T_;
     int size_Tg_;
      

     // ----------these are for m_cp ---------------                    
     double cp_L_ = 0.0;  
     std::vector<double> oxides_molar_fraction_; // computed in the constructor            
     double pmav_ = 0.0;    //average molecular weight of the volatile-free liquid
     std::vector<double> a_ = {127.2, 64.111, 175.491, 135.25, 31.77, 0.0, 46.704, 39.159, 70.884, 84.323};          //table 1 Richet 1987
     std::vector<double> b_ = {-10.777, 22.59, -5.839, 12.311, 38.515, 0.0, 11.22, 18.65, 26.11, 0.731};          
     std::vector<double> c_ = {4.3127, -23.02, -13.47, -39.098, -0.012, 0.0, -13.28, -1.523, -3.582, -8.298};           
     std::vector<double> d_ = {-1463.9, 0.0, -1370.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};             
            
     
        
     std::unique_ptr<vec> p_vec_ = nullptr; 
     std::unique_ptr<vec> T_vec_ = nullptr;
     std::unique_ptr<vec> w_h2o_vec_ = nullptr;
     std::unique_ptr<vec> Tg_vec_ = nullptr;
          
     std::unique_ptr<mat> w_h2o_mat_ = nullptr;   // function of T, p
     
     std::unique_ptr<mat> gas_co2_ = nullptr;     // function of T, w_h2o_
     std::unique_ptr<mat> magma_mu_ = nullptr;    // function of T, w_h2o_   
     std::unique_ptr<mat> eps_h2o_ = nullptr;     // function of T, w_h2o_
     std::unique_ptr<mat> rhog_h2o_ = nullptr;    // function of T, w_h2o_
     std::unique_ptr<mat> rhom_h2o_ = nullptr;    // function of T, w_h2o_
     std::unique_ptr<mat> alpha_h2o_ = nullptr;   // function of T, w_h2o_
    };



}


#endif
 
