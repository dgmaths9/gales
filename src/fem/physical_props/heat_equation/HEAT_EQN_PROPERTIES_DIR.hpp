#ifndef GALES_HEAT_EQN_PROPERTIES_DIR_HPP
#define GALES_HEAT_EQN_PROPERTIES_DIR_HPP

#include "base_heat_props.hpp"
#include "materials_heat_eq.hpp"
#include "heat_eq_properties_reader.hpp"
#include "heat_eq_properties.hpp"

#endif
