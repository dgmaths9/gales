#ifndef GALES_BASE_HEAT_PROPS_HPP
#define GALES_BASE_HEAT_PROPS_HPP



#include<vector>
#include<memory>


namespace GALES {


  /**
       base heat props class
  */



class base_heat_props
{

 public:


  //-------------------------------------------------------------------------------------------------------------------------------------        
  base_heat_props() = default;               //constructor
  /// Deleting the copy and move constructors - no duplication/transfer in anyway
  base_heat_props(const base_heat_props&) = delete;               //copy constructor
  base_heat_props& operator=(const base_heat_props&) = delete;    //copy assignment operator
  base_heat_props(base_heat_props&&) = delete;                    //move constructor  
  base_heat_props& operator=(base_heat_props&&) = delete;         //move assignment operator 
  //-------------------------------------------------------------------------------------------------------------------------------------


  virtual void properties(){}
  virtual void properties(double p, double T){}
  virtual void properties(double x, double y, double p, double T){}
  virtual void properties(const std::string& class_name, double x, double y, double p, double T){}



  /// this is used in heat eqn to print properties for debugging purpose
  virtual void print_props()
  {
       print_in_file("properties", "rho", rho_);
       print_in_file("properties", "cp", cp_);
       print_in_file("properties", "cv", cv_);
       print_in_file("properties", "mu", mu_);
       print_in_file("properties", "kappa", kappa_);
       print_in_file("properties", "alpha", alpha_);
       print_in_file("properties", "heat_source", heat_source_);
  }




  virtual double rho()const{return rho_;}
  virtual double cp()const{return cp_;}
  virtual double cv()const{return cv_;}
  virtual double mu()const{return mu_;}
  virtual double kappa()const{return kappa_;}
  virtual double alpha()const{return alpha_;}
  virtual double heat_source()const{return heat_source_;}
    
    
  virtual std::shared_ptr<base_heat_props>& material_ptr(){return material_ptr_;}
  virtual const std::shared_ptr<base_heat_props>& material_ptr()const {return material_ptr_;}
  
  
  virtual std::vector<std::shared_ptr<base_heat_props>>& material_ptrs(){return material_ptrs_;}
  virtual const std::vector<std::shared_ptr<base_heat_props>>& material_ptrs()const {return material_ptrs_;}
 
  virtual std::vector<std::tuple<double, double>>& class_y(){return class_y_;}
  virtual const std::vector<std::tuple<double, double>>& class_y()const {return class_y_;}
             
  
  protected:

  double rho_ = 0.0;
  double cp_ = 0.0;
  double cv_ = 0.0;
  double mu_ = 0.0;
  double kappa_ = 0.0;
  double alpha_ = 0.0;
  double heat_source_ = 0.0;
  bool heterogeneous_ = false;
  
  std::shared_ptr<base_heat_props> material_ptr_ = nullptr;   
  std::vector<std::shared_ptr<base_heat_props>> material_ptrs_;
  std::vector<std::tuple<double, double>> class_y_;
  
};



}


#endif
