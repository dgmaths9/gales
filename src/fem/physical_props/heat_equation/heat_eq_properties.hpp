#ifndef GALES_HEAT_EQ_PROPERTIES_HPP
#define GALES_HEAT_EQ_PROPERTIES_HPP



#include "heat_eq_properties_reader.hpp"



namespace GALES {


 /**
      This class defined the thermal properties for heat_eqn.
 */



class heat_eq_properties : public base_heat_props
{

  using vec = boost::numeric::ublas::vector<double>;
  
  public:

  heat_eq_properties()
  {  
      heat_eq_properties_reader reader(*this);
  }



  //-------------------------------------------------------------------------------------------------------------------------------------        
  /// Deleting the copy and move constructors - no duplication/transfer in anyway
  heat_eq_properties(const heat_eq_properties&) = delete;               //copy constructor
  heat_eq_properties& operator=(const heat_eq_properties&) = delete;    //copy assignment operator
  heat_eq_properties(heat_eq_properties&&) = delete;                    //move constructor  
  heat_eq_properties& operator=(heat_eq_properties&&) = delete;         //move assignment operator 
  //-------------------------------------------------------------------------------------------------------------------------------------
  




  //----------------this is for custom properties------------------
  void properties() final
  {
       rho_ = material_ptr()->rho();
       cp_ = material_ptr()->cp();    
       cv_ = material_ptr()->cv();    
       mu_ = material_ptr()->mu();    
       kappa_ = material_ptr()->kappa();
       alpha_ = material_ptr()->alpha();
       heat_source_ = material_ptr()->heat_source();
  }

    
    
  void properties(double x, double y, double p,  double T) final
  {
    for(int i=0; i<class_y().size(); i++)
     if(y >= std::get<0>(class_y()[i]) &&  y <= std::get<1>(class_y()[i]))
     {
        material_ptrs()[i]->properties(x, y, p, T); 
        rho_ = material_ptrs()[i]->rho();
        cp_ = material_ptrs()[i]->cp();    
        cv_ = material_ptrs()[i]->cv();    
        mu_ = material_ptrs()[i]->mu();    
        kappa_ = material_ptrs()[i]->kappa();
        alpha_ = material_ptrs()[i]->alpha();
        heat_source_ = material_ptrs()[i]->heat_source();
     }   
  }

   
  void set_material_type(const std::string& s){material_type_ = s;}
  auto get_material_type()const{return material_type_;}
  
  private:
  std::string material_type_;



   
};


}

#endif
