![](cover.png)

## Description
GALES is a general purpose 2D/3D linear finite element method (FEM) based multi-physics numerical code for the numerical solution of partial differential equations. The code was primarily developed to study problems related to volcano physics by running numerical simulations. The software is written in modern C++ and is parallelized using openMPI. The code is written as a collection of header files only and use template specialization to differentiate between 2D and 3D implementations. For parallel programming, GALES heavily depends on the trilinos software (https://trilinos.github.io/). The source code contains a tpl directory where all dependencies are bundeled in tar files. We also provide a set of scripts in tpl directory for postprocessing by paraview.


The software contains the following solvers:

1. fluid_mc            (for transient/steady multi-fluid flows on fixed/moving meshes by ALE approach)  
2. fluid_sc            (for transient/steady single-fluid flows on fixed/moving meshes by ALE approach)   

3. solid_es            (for linear-static deformation of an elastic material) 
4. solid_ed            (for linear/non-linear dynamic deformation of an elastic/hyperelastic material) 

5. fsi                 (for fluid-solid/structure interaction)
 
6. adv_diff            (for transient/steady advection diffuson equation) 

7. heat_equation       (for transient/steady heat conduction)

... more to come



## Downloading 
To download GALES type one of the following in your shell:

```bash
git clone git@gitlab.com:dgmaths9/gales.git
```

or

```bash
git clone https://gitlab.com/dgmaths9/gales.git
```

or download the code directly as a tar/zip file.




## Installation
If you've downloaded the code from a tar/zip file, then unpack the code into a desired directory say `/home/user/project/gales`. GALES is just a collection of header files so after downloading the code all you need is to install only the third party dependencies. To do that go into the tpl folder of the code and follow the instructions given in `README.md`. 



## Testing
After installing the dependencies and sourcing the environment file generated inside the `tpl` directory, you can test the installation by running the following command in your shell from `/home/user/project/gales`:

```bash    
./test_gales_installation.sh	
```

The script performs the two tests: 

1) it builds the mesh preprocessing utils for GALES. In addition it also builds the solwcad software. 
2) for a sim, it generates mesh, build the executable and run it in parallel on 2 cores using mpi.   
    
If everything is fine, you will see at the end 


```bash    
-------- Gales is installed!!!  --------------"    
```





## Getting started:
coming soon



## Support
For any questions do not hesitate to create an issue on the project page.




## Contributing
We invite and welcome volunteers to step in and contribute in optimizing/developing new solvers in the code. 



## License
GALES is open source software distributed under the MIT license.

