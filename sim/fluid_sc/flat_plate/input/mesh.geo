lc=0.02;
lc1=0.01;

Point(1) = {-0.2,0.0,0,lc1};
Point(2) = { 1.2,0.0,0,lc1};
Point(3) = { 1.2,0.8,0,lc};
Point(4) = {-0.2,0.8,0,lc};
Point(5) = {0.0,0.0,0,lc};


Line(1) = {4, 1};
Line(2) = {1, 5};
Line(3) = {5, 2};
Line(4) = {2, 3};
Line(5) = {3, 4};


Transfinite Line {1} = 71 Using Progression 0.98;
Transfinite Line {4} = 71 Using Progression 1.02;
Transfinite Line {5} = 101 Using Progression 1;
Transfinite Line {3} = 86 Using Progression 1;
Transfinite Line {2} = 16 Using Progression 1;

Line Loop(6) = {5, 1, 2, 3, 4};
Plane Surface(7) = {6};
Transfinite Surface {7}={1,2,3,4};
Recombine Surface {7};

Physical Line(4) = {2};
Physical Line(5) = {3};
Physical Line(7) = {4};
Physical Line(6) = {1, 5};
Physical Point(6) = {1, 4, 3};
Physical Point(5) = {5,2};
Physical Surface(0) = {7};

Mesh.MshFileVersion = 2;
