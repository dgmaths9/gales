//Mesh.MshFileVersion = 2;

/*
lc=0.05;
Point(1) = {0,0,0,lc};
Point(2) = {0,1,0,lc};
Point(3) = {0,0,1,lc};
Point(4) = {0,-1,0,lc};
Point(5) = {0,0,-1,lc};

//+
Circle(1) = {4, 1, 5};
//+
Circle(2) = {5, 1, 2};
//+
Circle(3) = {2, 1, 3};
//+
Circle(4) = {3, 1, 4};
//+
Line Loop(1) = {4, 1, 2, 3};
//+
Plane Surface(1) = {1};
//+
Recombine Surface {1};
//+
Extrude {10, 0, 0} {
  Surface{1}; Layers{100}; Recombine;
}
//+
Physical Volume(1) = {1};
//+
Physical Surface(6) = {1};
//+
Physical Surface(7) = {26};
//+
Physical Surface(5) = {21, 25, 17, 13};
//+
Physical Line(2) = {2, 3, 4, 1};
//+
Physical Line(3) = {9, 8, 7, 6};
//+
Physical Point(2) = {5, 2, 3, 4};
//+
Physical Point(3) = {18, 13, 8, 6};
*/








//+
SetFactory("OpenCASCADE");
//+
Cylinder(1) = {0, 0, 0, 10, 0, 0, 0.5, 2*Pi};
//+
Physical Volume(1) = {1};
//+
Physical Surface(5) = {1};
//+
Physical Surface(6) = {3};
//+
Physical Surface(7) = {2};
//+
Physical Line(2) = {3};
//+
Physical Line(3) = {1};
//+
Physical Point(2) = {2};
//+
Physical Point(3) = {1};
//+
Characteristic Length {2, 1} = 0.05;

