Mesh.MshFileVersion = 2;


//+
SetFactory("OpenCASCADE");
//+
Box(1) = {0, 0, 0, 1.2, 0.8, 0.3};
//+
Box(2) = {-0.2, 0, 0, 0.2, 0.8, 0.3};
//+
Coherence;
//+
Physical Volume(1) = {2, 1};
//+
Physical Surface(6) = {7, 10, 5};
//+
Physical Surface(5) = {2};
//+
Physical Surface(7) = {6};
//+
Physical Surface(4) = {8};
//+
Physical Surface(3) = {9, 11, 3, 4};
//+
Physical Line(6) = {14, 16, 13, 15, 4, 12, 19, 20, 8, 10};
//+
Physical Line(5) = {5, 1, 7, 6};
//+
Physical Line(2) = {18, 17};
//+
Physical Line(3) = {3, 2};
//+
Physical Point(5) = {1, 2, 6, 5};
//+
Physical Point(6) = {9, 10, 12, 11, 4, 3, 7, 8};
//+
Characteristic Length {12, 11, 3, 4, 7, 8, 5, 6, 2, 1, 9, 10} = 0.015;
