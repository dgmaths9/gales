Mesh.MshFileVersion = 2;

/* 
   This is a mesh for a unit square. 
   'N' is the number of points in x and y direction 
*/

N = 512;   



h = 1;
Point(1) = {0, 0, 0};
Point(2) = {h, 0, 0};
Point(3) = {h, h, 0};
Point(4) = {0, h, 0};

//+
Line(1) = {1, 2};
//+
Line(2) = {2, 3};
//+
Line(3) = {3, 4};
//+
Line(4) = {4, 1};

//+
Curve Loop(1) = {4, 1, 2, 3};
//+
Plane Surface(1) = {1};
//+
Transfinite Curve {4, 2, 1, 3} = N+1 Using Progression 1;
//+
Transfinite Surface {1};
//+
Recombine Surface {1};
//+
Physical Surface(5) = {1};
//+
Physical Curve(5) = {4, 1, 2};
//+
Physical Curve(4) = {3};
//+
Physical Point(5) = {4, 3};
