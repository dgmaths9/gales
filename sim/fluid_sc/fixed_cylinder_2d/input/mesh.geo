Mesh.MshFileVersion = 2;

lc=0.05;
lc1= 0.2;



Point(1) = {0, 0, 0, lc};
Point(2) = {-0.4, 0.3, 0, lc};
Point(3) = {-0.4, -0.3, 0, lc};
Point(4) = {0.4, 0.3, 0, lc};
Point(5) = {0.4, -0.3, 0, lc};

Circle(6) = {4, 1, 5};
Circle(7) = {5, 1, 3};
Circle(8) = {3, 1, 2};
Circle(9) = {2, 1, 4};

Point(11) = {-4.5,-4.5, 0, lc1};
Point(12) = {-4.5, 4.5, 0, lc1};

Point(13) = {15.5,-4.5, 0, lc1};
Point(14) = {15.5, 4.5, 0, lc1};


//+
Line(10) = {12, 11};
//+
Line(11) = {11, 13};
//+
Line(12) = {13, 14};
//+
Line(13) = {14, 12};
//+
Curve Loop(1) = {13, 10, 11, 12};
//+
Curve Loop(2) = {9, 6, 7, 8};
//+
Plane Surface(1) = {1, 2};
//+
Physical Surface(14) = {1};
//+
Physical Curve(4) = {11, 13};
//+
Physical Curve(6) = {10};
//+
Physical Curve(7) = {12};
//+
Physical Curve(5) = {9, 6, 7, 8};
//+
Physical Point(4) = {11, 12, 14, 13};
