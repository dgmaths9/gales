lc=2;

Point(0) = {0,-4100,0,lc};

Point(1) = {0,-4000,0,lc};
Point(2) = {400,-4100,0,lc};
Point(3) = {0,-4200,0,lc};
Point(4) = {-400,-4100,0,lc};

Ellipse(1) = {1, 0, 3, 4};
Ellipse(2) = {4, 0, 2, 3};
Ellipse(3) = {3, 0, 1, 2};
Ellipse(4) = {2, 0, 4, 1};


Line Loop(5) = {1, 2, 3, 4};
Plane Surface(6) = {5};
Physical Line(5) = {1, 4, 3, 2};
Physical Surface(0) = {6};
