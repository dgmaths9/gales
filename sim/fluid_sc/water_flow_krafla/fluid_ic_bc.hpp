#ifndef __FLUID_IC_BC_HPP
#define __FLUID_IC_BC_HPP



#include "../../../src/fem/fem.hpp"



namespace GALES {




  template<int dim>
  class fluid_ic_bc : public base_ic_bc<dim> 
  {

    using nd_type = node<dim>;
    using vec = boost::numeric::ublas::vector<double>;


  public :
 
    fluid_ic_bc()
    {
        this->central_pressure_profile_ = true;
        this->p0_ = 20.e6;
        this->max_ = -2079.0;
        this->min_ = -2104.0;
        this->h_ = 0.01;
        this->total_steps_ = (int)((this->max_-this->min_)/this->h_);     

        this->p_ref_ = 20.e6;
        this->v1_ref_ = 0.0;
        this->v2_ref_ = -0.53;
        this->T_ref_ = 313.15;
    }




    void body_force(vec& gravity) const
    {
      gravity[1] = -9.81;
    }






   
    //-------------------- IC ----------------------------------------

    double initial_p(const nd_type &nd) const { return 0.0; }
    double initial_vx(const nd_type &nd) const { return 0.0; }
    double initial_vy(const nd_type &nd) const { return 0.0; }
    double initial_T(const nd_type &nd) const { return 313.15; } 
//    { 
//       double T_max = 1173.15;
//       if(nd.get_y()>=-2103.95) return 313.15;
//       else return T_max + (nd.get_y()+2104.0)*(313.15-T_max)/(-2103.95+2104.0);
//    }
 	





  // --------------------Dirichlet BC------------------------------------

    auto dirichlet_p(const nd_type &nd) const 
    {
       if(nd.flag() ==  7)     return std::make_pair(true, 20.e6); 
	
      return std::make_pair(false,0.0);
    }


    auto dirichlet_vx(const nd_type &nd) const 
    { 
	if( nd.flag() ==  5)     return std::make_pair(true,0.0); 
	if( nd.flag() ==  1)     return std::make_pair(true,0.0); 
	if( nd.flag() ==  6)     return std::make_pair(true,0.0); 
	
      return std::make_pair(false,0.0);
    }


    auto dirichlet_vy(const nd_type &nd) const  
    {
	if( nd.flag() ==  5)     return std::make_pair(true,0.0); 
	if( nd.flag() ==  1)     return std::make_pair(true,0.0); 
	if( nd.flag() ==  6)     return std::make_pair(true,-0.53); 
	  
      return std::make_pair(false,0.0);
    }

 

    auto dirichlet_T(const nd_type &nd) const
    {
      if(nd.flag() ==  6)     return std::make_pair(true, 313.15);
      if(nd.flag() ==  1)     return std::make_pair(true, 1173.15);
    
      return std::make_pair(false,0.0);
    }




  //-------------------------Neumann BC--------------------------------------------------

    auto neumann_tau11(const std::vector<int>& bd_nodes, int side_flag) const  
    { 
      return std::make_pair(false,0.0);  
    }

    auto neumann_tau12(const std::vector<int>& bd_nodes, int side_flag) const  
    { 
      if(side_flag == 7)     return std::make_pair(true,0.0);

      return std::make_pair(false,0.0);
    }
  
    auto neumann_tau22(const std::vector<int>& bd_nodes, int side_flag) const  
    { 
      if(side_flag == 7)     return std::make_pair(true,0.0);

      return std::make_pair(false,0.0); 
    }


    auto neumann_q1(const std::vector<int>& bd_nodes, int side_flag)  const
    {
      if(side_flag == 5)      return std::make_pair(true, 0.0); 
      
      return std::make_pair(false,0.0);
    }


    auto neumann_q2(const std::vector<int>& bd_nodes, int side_flag)  const
    {
      if(side_flag == 7)      return std::make_pair(true, 0.0); 
  
      return std::make_pair(false,0.0);
    }
    
  };
  
} //namespace
#endif



