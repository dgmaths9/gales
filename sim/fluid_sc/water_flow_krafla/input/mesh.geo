Mesh.MshFileVersion=2;


h1 = 0.01;
h2 = 0.01;
h3 = 0.01;


inlet_interface_d = 0.2;



Point(1) = {0,-2104,0, h2};


Point(5) = {0.205,-2104,0,h2};
Point(6) = {-0.205,-2104,0,h2};

Point(7) = {-0.205,-2079,0, h3};
Point(8) = {0.205,-2079,0, h3};

Point(9) = {-0.06,-2104+inlet_interface_d,0,h2};
Point(10) = {0.06,-2104+inlet_interface_d,0,h2};

Point(11) = {-0.06,-2079,0, h3};
Point(12) = {0.06,-2079,0, h3};





//magma chamber
Line(12) = {6, 1};
Line(13) = {1, 5};


//well
Line(4) = {6, 7};
Line(5) = {7, 11};
Line(6) = {11, 9};
Line(7) = {9, 10};
Line(8) = {10, 12};
Line(9) = {12, 8};
Line(10) = {8, 5};
Curve Loop(1) = {8, 9, 10, -13, -12, 4, 5, 6, 7};
Plane Surface(1) = {1};


// water-steam
Physical Surface(14) = {1};
Physical Curve(5) = {10, 8, 6, 4};
Physical Curve(6) = {7};
Physical Curve(7) = {5, 9};
Physical Curve(1) = {12, 13};
Physical Point(5) = {6, 5, 10, 9, 8, 12, 11, 7};




