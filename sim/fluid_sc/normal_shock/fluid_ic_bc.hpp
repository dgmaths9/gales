#ifndef __FLUID_IC_BC_HPP
#define __FLUID_IC_BC_HPP


#include "../../../src/fem/fem.hpp"



namespace GALES {




  template<int dim>
  class fluid_ic_bc : public base_ic_bc<dim>  
  {
    using nd_type = node<dim>;
    using vec = boost::numeric::ublas::vector<double>;

  public :
 

    fluid_ic_bc()
    {
      this->p_ref_ = 0.17857;
      this->v1_ref_ = 0.0;
      this->v2_ref_ = 0.0;
      this->T_ref_ = 0.000622195122;
    }










    //-------------------- IC ----------------------------------------
    double initial_p (const nd_type &nd) const
    { 
       if(nd.get_x()< 0.0)   return 0.17857;  
       else return 0.80357;
    }

    double initial_vx(const nd_type &nd) const 
    { 
       if(nd.get_x()< 0.0)   return 1.0;  
       else return 0.375;
    }
    double initial_vy(const nd_type &nd) const { return 0.0;  }

    double initial_T(const nd_type &nd) const 
    { 
       if(nd.get_x()< 0.0)   return 0.000622195122;  
       else return 0.00104995949;
    }






    // -------------------------  Dirichlet BC ------------------------

    auto dirichlet_p(const nd_type &nd) const
    {
 	if( nd.flag() == 6)       return std::make_pair(true, 0.17857); 

      return std::make_pair(false, 0.0);
    }


    auto dirichlet_vx(const nd_type &nd) const
    {
 	if( nd.flag() == 6)       return std::make_pair(true, 1.0); 

      return std::make_pair(false,0.0);
    }


    auto dirichlet_vy(const nd_type &nd) const
    {
      return std::make_pair(true,0.0);
    }


    auto dirichlet_T(const nd_type &nd) const
    {
 	if( nd.flag() == 6)       return std::make_pair(true, 0.000622195122); 
 	if( nd.flag() == 7)       return std::make_pair(true, 0.00104995949); 

      return std::make_pair(false,0.0);
    }






  //-------------------------Neumann BC--------------------------------------------------

    auto neumann_tau11(const std::vector<int>& bd_nodes, int side_flag) const  
    { 
      return std::make_pair(false,0.0);  
    }

    auto neumann_tau12(const std::vector<int>& bd_nodes, int side_flag) const  
    { 
      return std::make_pair(false,0.0);
    }

    auto neumann_tau22(const std::vector<int>& bd_nodes, int side_flag) const  
    { 
      return std::make_pair(false,0.0); 
    }
                
    auto neumann_q1(const std::vector<int>& bd_nodes, int side_flag) const  
    {   
      return std::make_pair(false,0.0); 
    }

    auto neumann_q2(const std::vector<int>& bd_nodes, int side_flag) const  
    { 
      return std::make_pair(false,0.0); 
    }


  };

} //namespace
#endif
