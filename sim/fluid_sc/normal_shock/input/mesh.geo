Mesh.MshFileVersion = 2;
lc=0.020;
lc1=0.01;

Point(1) = {-19.5,-0.5,0,lc};
Point(2) = {19.5,-0.5,0,lc};
Point(3) = {19.5,0.5,0,lc};
Point(4) = {-19.5,0.5,0,lc};


Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};
Line Loop(5) = {1, 2, 3, 4};
Plane Surface(6) = {5};



//+
Transfinite Curve {3, 1} = 40 Using Progression 1;
//+
Transfinite Curve {4, 2} = 2 Using Progression 1;
//+
Transfinite Surface {6};
//+
Recombine Surface {6};
//+
Physical Surface(7) = {6};
//+
Physical Curve(6) = {4};
//+
Physical Curve(7) = {2};
//+
Physical Curve(2) = {1,3};
