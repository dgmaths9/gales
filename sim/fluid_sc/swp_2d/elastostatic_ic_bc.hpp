#ifndef ELASTOSTATIC_IC_BC_HPP
#define ELASTOSTATIC_IC_BC_HPP



#include "../../../src/fem/fem.hpp"



namespace GALES{



  template<int dim>
  class elastostatic_ic_bc : public base_ic_bc<dim>
  {
    using nd_type = node<dim>;
    using model_type = model<dim>; 



    public :

    elastostatic_ic_bc(model_type& f_model)
    : 
    f_model_(f_model)
    {}




    //----------------------Dirichlet  BC------------------------------------------------------------------------------
    auto dirichlet_ux(const nd_type &nd) const 
    {
      if( nd.flag() == 2)
      {
        double v = f_model_.extract_node_dof(nd.lid(), 1, 0);
        double dt = time::get().delta_t();
        double ux = dt*v;
        return std::make_pair(true,ux);
      }

      if( nd.flag() == 4)      return std::make_pair(true,0.0);
      if( nd.flag() == 5)      return std::make_pair(true,0.0);
      if( nd.flag() == 6)      return std::make_pair(true,0.0);
      if( nd.flag() == 7)      return std::make_pair(true,0.0);

      return std::make_pair(false,0.0);
    }



    auto dirichlet_uy(const nd_type &nd) const 
    {
      if(nd.flag() == 2 || nd.flag() == 6 || nd.flag() == 4)
      {
        double v = f_model_.extract_node_dof(nd.lid(), 2, 0);
        double dt = time::get().delta_t();
        double uy = dt*v;
        return std::make_pair(true,uy);
      }

      if( nd.flag() == 5)      return std::make_pair(true,0.0);
      if( nd.flag() == 7)      return std::make_pair(true,0.0);

      return std::make_pair(false,0.0);
    }




    //-------------Sets initial conditions -------------------------
    double initial_ux(const nd_type &nd)const  {return 0.0;}
    double initial_uy(const nd_type &nd)const  {return 0.0;}
 


  private:
    model_type& f_model_;


  };


}

#endif
