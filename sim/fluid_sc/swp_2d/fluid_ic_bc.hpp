#ifndef __FLUID_IC_BC_HPP
#define __FLUID_IC_BC_HPP


#include "../../../src/fem/fem.hpp"



namespace GALES {




  template<int dim>
  class fluid_ic_bc : public base_ic_bc<dim>
  {

    using nd_type = node<dim>;
    using vec = boost::numeric::ublas::vector<double>;


  public :
 


    void body_force(vec& gravity) const 
    {
      gravity[1] = -9.81;
    }





   
    //-------------------- IC ----------------------------------------

    double initial_p(const nd_type &nd) const  
    {
      double x = nd.get_x();
      double y = nd.get_y();
      double H(2.0), d(10.0);
      double use1 = 1.0/cosh(sqrt(3*H/(4*d*d*d))*x);
      double yp = d + H*use1*use1;       //free surface
      return this->p0() + 1.0*9.81*(yp-y);
    }

    double initial_vx(const nd_type &nd)const  
    {
      double x = nd.get_x();
      double H(2.0), d(10.0);
      double use1(1.0/cosh(sqrt(3*H/(4*d*d*d))*x));
      return sqrt(9.81*d)*(H/d)*use1*use1;
    }

    double initial_vy (const nd_type &nd)const  
    {
      double x = nd.get_x();
      double y = nd.get_y();
      double H(2.0), d(10.0);
      double use1(1.0/cosh(sqrt(3*H/(4*d*d*d))*x));
      double use2(tanh(sqrt(3*H/(4*d*d*d))*x));
      return sqrt(3*9.81/d)*(H/d)*sqrt(H/d)*y*use1*use1*use2;
    }
 	    




  // --------------------Dirichlet BC------------------------------------

    auto dirichlet_p(const nd_type &nd) const   
    {
      if( nd.flag() ==  6)	  return std::make_pair(true,0.0); 
      if( nd.flag() ==  2)	  return std::make_pair(true,0.0);   
	
      return std::make_pair(false,0.0);
    }


    auto dirichlet_vx(const nd_type &nd) const   
    { 
      if( nd.flag() ==  4)     return std::make_pair(true,0.0); 
      if( nd.flag() ==  6)     return std::make_pair(true,0.0); 
      if( nd.flag() ==  7)     return std::make_pair(true,0.0);
	
      return std::make_pair(false,0.0);
    }


    auto dirichlet_vy(const nd_type &nd) const   
    {
      if( nd.flag() ==  5)     return std::make_pair(true,0.0); 
      if( nd.flag() ==  7)     return std::make_pair(true,0.0); 
	  
      return std::make_pair(false,0.0);
    }


 




  //-------------------------Neumann BC--------------------------------------------------

    auto neumann_tau11(const std::vector<int>& bd_nodes, int side_flag) const     
    { 
      if(side_flag == 2)   return std::make_pair(true,0.0);

      return std::make_pair(false,0.0);  
    }

    auto neumann_tau12(const std::vector<int>& bd_nodes, int side_flag) const     
    { 
      if(side_flag == 2)      return std::make_pair(true,0.0);
      if(side_flag == 4)      return std::make_pair(true,0.0);
      if(side_flag == 5)      return std::make_pair(true,0.0);

      return std::make_pair(false,0.0);
    }

    auto neumann_tau22(const std::vector<int>& bd_nodes, int side_flag) const     
    { 
      if(side_flag == 2)      return std::make_pair(true,0.0);

      return std::make_pair(false,0.0); 
    }
                


  };
  
} //namespace
#endif



