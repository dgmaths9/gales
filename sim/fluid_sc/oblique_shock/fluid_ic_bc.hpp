#ifndef __FLUID_IC_BC_HPP
#define __FLUID_IC_BC_HPP


#include "../../../src/fem/fem.hpp"



namespace GALES {




  template<int dim>
  class fluid_ic_bc : public base_ic_bc<dim>  
  {
    using nd_type = node<dim>;
    using vec = boost::numeric::ublas::vector<double>;

  public :
 

    fluid_ic_bc()
    {    
      this->p_ref_ = 0.17857;
      this->v1_ref_ = 0.0;
      this->v2_ref_ = 0.0;
      this->T_ref_ = 0.000621731;
    }






    //-------------------- IC ----------------------------------------
    double initial_p (const nd_type &nd)const { return 0.17857;  }
    double initial_vx(const nd_type &nd)const { return cos(3.1428*10/180);  }
    double initial_vy(const nd_type &nd)const { return -sin(3.1428*10/180);  }
    double initial_T(const nd_type &nd)const { return 0.000621731; }






    // -------------------------  Dirichlet BC ------------------------

    auto dirichlet_p(const nd_type &nd) const
    {
 	if( nd.flag() == 2)       return std::make_pair(true,0.17857); 

      return std::make_pair(false,0.0);
    }


    auto dirichlet_vx(const nd_type &nd) const
    {
        if( nd.flag() == 2)       return std::make_pair(true,cos(3.1428*10/180));

      return std::make_pair(false,0.0);
    }


    auto dirichlet_vy(const nd_type &nd) const
    {
        if( nd.flag() == 2)          return std::make_pair(true, -sin(3.1428*10/180));
        if( nd.flag() == 3)          return std::make_pair(true, 0.0);

      return std::make_pair(false,0.0);
    }


    auto dirichlet_T(const nd_type &nd) const
    {
        if( nd.flag() == 2)          return std::make_pair(true, 0.000621731);

      return std::make_pair(false,0.0);
    }






  //-------------------------Neumann BC--------------------------------------------------

    auto neumann_tau11(const std::vector<int>& bd_nodes, int side_flag)const  
    { 
      return std::make_pair(false,0.0);  
    }

    auto neumann_tau12(const std::vector<int>& bd_nodes, int side_flag)const  
    { 
      return std::make_pair(false,0.0);
    }

    auto neumann_tau22(const std::vector<int>& bd_nodes, int side_flag)const  
    { 
      return std::make_pair(false,0.0); 
    }                                


  };

} //namespace
#endif
