Mesh.MshFileVersion = 2;
lc=0.020;
lc1=0.01;

Point(1) = {0,0.025,0,lc1};
Point(2) = {1,0,0,lc1};
Point(3) = {1,1,0,lc};
Point(4) = {0,1,0,lc};


Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};
Line Loop(5) = {1, 2, 3, 4};
Plane Surface(6) = {5};


Transfinite Line {3, 1, 4, 2} = 51 Using Progression 1;
Transfinite Surface {6};
Recombine Surface {6};
Physical Surface(0) = {6};
Physical Line(2) = {4,3};
Physical Line(4) = {2};
Physical Line(3) = {1};

