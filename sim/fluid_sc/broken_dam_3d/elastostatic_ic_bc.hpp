#ifndef ELASTOSTATIC_IC_BC_HPP
#define ELASTOSTATIC_IC_BC_HPP




#include "../../../src/fem/fem.hpp"



namespace GALES{



  template<int dim>
  class elastostatic_ic_bc : public base_ic_bc<dim>
  {
    using nd_type = node<dim>;
    using model_type = model<dim>; 



    public :

    elastostatic_ic_bc(model_type& f_model)
    : 
    f_model_(f_model) 
    {}




    //----------------------Dirichlet  BC------------------------------------------------------------------------------
    auto dirichlet_ux(const nd_type &nd) const
    {
      if( nd.flag() == 3)     return std::make_pair(true,0.0); 
      if( nd.flag() == 6)     return std::make_pair(true,0.0); 
      if( nd.flag() == 7)     return std::make_pair(true,0.0); 
      if( nd.flag() == 9)     return std::make_pair(true,0.0); 

      if( nd.flag() == 5)
      {
        std::vector<double> dofs_f;
        f_model_.extract_node_dofs(*(f_model_.mesh().nodes()[nd.lid()]), dofs_f);        
        //The above is true only because fluid and elastostatic meshes are same.
        //Therefore, lid of the nd is same for fluid and elastostatic.        
        double ux(dofs_f[1]*time::get().delta_t());
        return std::make_pair(true,ux);
      }

      return std::make_pair(false,0.0);
    }



    auto dirichlet_uy(const nd_type &nd) const
    {
      if( nd.flag() == 4)     return std::make_pair(true,0.0); 
      if( nd.flag() == 6)     return std::make_pair(true,0.0); 
      if( nd.flag() == 8)     return std::make_pair(true,0.0); 
      if( nd.flag() == 9)     return std::make_pair(true,0.0); 

      if( nd.flag() == 5)
      {
        std::vector<double> dofs_f;
        f_model_.extract_node_dofs(*(f_model_.mesh().nodes()[nd.lid()]), dofs_f);        
        double uy(dofs_f[2]*time::get().delta_t());
        return std::make_pair(true,uy);
      }

      return std::make_pair(false,0.0);
    }


    auto dirichlet_uz(const nd_type &nd) const
    {
      if( nd.flag() == 2)     return std::make_pair(true,0.0); 
      if( nd.flag() == 7)     return std::make_pair(true,0.0); 
      if( nd.flag() == 8)     return std::make_pair(true,0.0); 
      if( nd.flag() == 9)     return std::make_pair(true,0.0); 

      if( nd.flag() == 5)
      {
        std::vector<double> dofs_f;
        f_model_.extract_node_dofs(*(f_model_.mesh().nodes()[nd.lid()]), dofs_f);        
        double uz(dofs_f[3]*time::get().delta_t());
        return std::make_pair(true,uz);
      }

      return std::make_pair(false,0.0);
    }



    //-------------Sets initial conditions -------------------------
    double initial_ux(const nd_type &nd)const {return 0.0;}
    double initial_uy(const nd_type &nd)const {return 0.0;}
    double initial_uz(const nd_type &nd)const {return 0.0;}




  private:
    model_type& f_model_;



  };


}

#endif
