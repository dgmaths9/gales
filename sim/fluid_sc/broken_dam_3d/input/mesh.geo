
//+
SetFactory("OpenCASCADE");




//+
Box(1) = {0, 0, 0, 3.5, 7, 3.5};

Physical Volume(14) = {1};
//+
Characteristic Length {3, 7, 8, 4, 2, 1, 5, 6} = 0.2;


//+
Physical Surface(2) = {5};
//+
Physical Surface(3) = {1};
//+
Physical Surface(4) = {3};
//+
Physical Surface(5) = {6, 4, 2};


//+
Physical Line(6) = {1};
//+
Physical Line(3) = {2, 3};
//+
Physical Line(7) = {4};
//+
Physical Line(4) = {5, 10};
//+
Physical Line(2) = {8, 11};
//+
Physical Line(8) = {9};
//+
Physical Point(6) = {1};
//+
Physical Point(9) = {2};
//+
Physical Point(3) = {3};
//+
Physical Point(7) = {4};
//+
Physical Point(4) = {5};
//+
Physical Point(8) = {6};
//+
Physical Point(2) = {8};

Mesh.MshFileVersion = 2;
