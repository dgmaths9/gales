Mesh.MshFileVersion=2;

h1 = 0.01;
h2 = 0.2;


inlet_interface_d = 0.2;



Point(1) = {0,-2104,0, h1};

Point(5) = {0.205,-2104,0,h1};
Point(6) = {-0.205,-2104,0,h1};

Point(7) = {-0.205,-2079,0, h1};
Point(8) = {0.205,-2079,0, h1};

Point(9) = {-0.06,-2104+inlet_interface_d,0,h1};
Point(10) = {0.06,-2104+inlet_interface_d,0,h1};

Point(11) = {-0.06,-2079,0, h1};
Point(12) = {0.06,-2079,0, h1};

Point(13) = {-0.06,-2104,0, h1};
Point(14) = {0.06,-2104,0, h1};

Point(15) = {-0.205,-2104+inlet_interface_d,0, h1};
Point(16) = {0.205,-2104+inlet_interface_d,0, h1};




Line(5) = {7, 11};
Line(6) = {11, 9};
Line(7) = {9, 10};
Line(8) = {10, 12};
Line(9) = {12, 8};
Line(11) = {9, 15};
Line(12) = {10, 16};
Line(13) = {14, 10};
Line(14) = {9, 13};
Line(15) = {13, 6};
Line(16) = {13, 1};
Line(17) = {1, 14};
Line(18) = {14, 5};
Line(23) = {6, 15};
Line(24) = {5, 16};
Line(25) = {16, 8};
Line(26) = {7, 15};


//well
Curve Loop(1) = {14, 15, 23, -11};
Plane Surface(1) = {1};
Curve Loop(2) = {7, -13, -17, -16, -14};
Plane Surface(2) = {2};
Curve Loop(3) = {13, 12, -24, -18};
Plane Surface(3) = {3};
Curve Loop(4) = {25, -9, -8, 12};
Plane Surface(4) = {4};
Curve Loop(5) = {6, 11, -26, 5};
Plane Surface(5) = {5};
Transfinite Curve {7} = 15 Using Progression 1;
Transfinite Curve {11, 15, 18, 12, 9, 5} = 16 Using Progression 1;
Transfinite Curve {16, 17} = 8 Using Progression 1;
Transfinite Curve {23, 14, 13, 24} = 21 Using Progression 1;
Transfinite Curve {26, 6, 8, 25} = 2501 Using Progression 1;
Transfinite Surface {5};
Transfinite Surface {4};
Transfinite Surface {3};
Transfinite Surface {1};
Transfinite Surface(2) = {9,10,14,13};
Recombine Surface {5};
Recombine Surface {1};
Recombine Surface {2};
Recombine Surface {3};
Recombine Surface {4};
Physical Surface(27) = {5, 4, 3, 2, 1};
Physical Curve(6) = {7};
Physical Curve(7) = {9, 5};
Physical Curve(5) = {26, 23, 25, 24, 8, 6};
Physical Curve(1) = {15, 16, 17, 18};
Physical Point(5) = {6, 5, 9, 10, 8, 12, 11, 7};



// magma
Point(17) = {-25,-2104,0, h2};
Point(18) = {25,-2104,0, h2};
Point(19) = {0,-2129,0, h2};
Line(19) = {17, 6};
Line(20) = {5, 18};
Circle(21) = {17, 1, 19};
Circle(22) = {19, 1, 18};
Curve Loop(6) = {19, -15, 16, 17, 18, 20, -22, -21};
Plane Surface(6) = {6};
Recombine Surface {6};
Physical Surface(27) += {6};
Physical Curve(1) -= {15, 16, 17, 18};
Physical Curve(4) = {19, 20};
Physical Curve(3) = {21, 22};
Physical Point(3) = {17, 18};

