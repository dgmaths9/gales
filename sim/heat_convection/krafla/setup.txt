heat_eq_mesh_file     mesh_144core.txt
dim                   2
delta_t               0.01
final_time            100000	
restart               T
restart_time          6792.0
n_max_it              3
print_freq            100


ls_solver           Flexible GMRES
ls_precond          ILU
ls_rel_res_tol      1.e-9
ls_maxsubspace      200
ls_maxrestarts      5
ls_maxiters        -1
ls_fill             1




steady_state                F
tau_non_diag_comp_2019      T
tau_diag_incomp_2007        F
dc_2006                     T
dc_sharp                    1.0 
dc_scale_fact               1.5            

Generalized_alpha_method      F
rho_inf                       0.5         
constant_v                    T
zero_a                        F


