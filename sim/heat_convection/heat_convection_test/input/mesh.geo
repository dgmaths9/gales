Mesh.MshFileVersion=2;

lc=1.0;


Point(1) = {0,0,0,lc};
Point(2) = {1,0,0,lc};
Point(3) = {1,0.5,0,lc};
Point(4) = {0,0.5,0,lc};


//+
Line(1) = {1, 2};
//+
Line(2) = {2, 3};
//+
Line(3) = {3, 4};
//+
Line(4) = {4, 1};
//+
Curve Loop(1) = {4, 1, 2, 3};
//+
Plane Surface(1) = {1};
//+
Transfinite Curve {1, 3} = 51 Using Progression 1;
//+
Transfinite Curve {4, 2} = 26 Using Progression 1;
//+
Transfinite Surface {1};


//+
Recombine Surface {1};
//+
Physical Surface(5) = {1};
//+
Physical Curve(6) = {4};
//+
Physical Curve(7) = {2};
//+
Physical Curve(4) = {1, 3};
Physical Point(4) = {1,2,3,4};
