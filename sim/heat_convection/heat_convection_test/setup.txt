heat_eq_mesh_file     mesh_16core.txt
dim                   2
delta_t               0.000001
final_time            0.00051
restart               F
restart_time          0.0
n_max_it              3
print_freq           10


ls_solver           Flexible GMRES
ls_precond          ILU
ls_rel_res_tol      1.e-9
ls_maxsubspace      200
ls_maxrestarts      5
ls_maxiters        -1
ls_fill             0


steady_state                F
tau_non_diag_comp_2019      F
tau_diag_incomp_2007        T
dc_2006                     T
dc_sharp                    1
dc_scale_fact               1.0

Generalized_alpha_method      F
rho_inf                       0.1
constant_v                    T
zero_a                        F



