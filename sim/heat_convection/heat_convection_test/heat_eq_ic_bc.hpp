#ifndef HEAT_EQ_IC_BC_HPP
#define HEAT_EQ_IC_BC_HPP



#include "../../../src/fem/fem.hpp"



namespace GALES{




  template<int dim>
  class heat_eq_ic_bc : public base_ic_bc<dim>  
  {
    using nd_type = node<dim>;
    using vec = boost::numeric::ublas::vector<double>;

    public : 
    



    heat_eq_ic_bc()
    {    
      this->T_ref_ = 1.0;    
    }




    void body_force(vec& gravity) const
    {
      gravity[1] = 0.0;  
    }



    void get_p(int nd_gid, double& p_nd)
    {
       p_nd = 0.0;
    }


    void get_v(int nd_gid, vec& v_nd)
    {
       v_nd[0] = 2000.0;
       v_nd[1] = 0.0;
    }
 
 
 
 
    //---------------------   IC  ------------------------------------------
    double initial_T(const nd_type &nd)const {return 0.0;}


 
     //------------set dirichlet bc------------------------------------      
    auto dirichlet_T(const nd_type &nd) const 
    {
      if(nd.flag() == 6)      return std::make_pair(true, 1.0);

      return std::make_pair(false,0.0);
    }


    //--------------set Neummann bc-------------------------------------
    auto neumann_q1(const std::vector<int>& bd_nodes, int side_flag) const 
    { 
      if(side_flag == 7)  std::make_pair(true,0.0);
      return std::make_pair(false,0.0);
    }

    auto neumann_q2(const std::vector<int>& bd_nodes, int side_flag) const 
    {
      if(side_flag == 4)  std::make_pair(true,0.0);
      return std::make_pair(false,0.0); 
    }


  };


}


#endif
