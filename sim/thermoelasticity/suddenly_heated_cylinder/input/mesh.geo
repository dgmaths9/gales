Mesh.MshFileVersion = 2;

lc=0.01;
a = 1;
b = 2;

Point(0) = {0,0,0,lc};

Point(1) = {0.0,a,0,lc};
Point(2) = {0.0,-a,0,lc};
Point(3) = {-a,0.0,0,lc};
Point(4) = {a,0.0,0,lc};

Point(11) = {0.0,b,0,lc};
Point(12) = {0.0,-b,0,lc};
Point(13) = {-b,0.0,0,lc};
Point(14) = {b,0.0,0,lc};


//+
Circle(1) = {11, 0, 14};
//+
Circle(2) = {14, 0, 12};
//+
Circle(3) = {12, 0, 13};
//+
Circle(4) = {13, 0, 11};
//+
Circle(5) = {1, 0, 4};
//+
Circle(6) = {4, 0, 2};
//+
Circle(7) = {2, 0, 3};
//+
Circle(8) = {3, 0, 1};
//+
Curve Loop(1) = {4, 1, 2, 3};
//+
Curve Loop(2) = {8, 5, 6, 7};
//+
Surface(1) = {1, 2};
//+
Physical Surface(11) = {1};
//+
Physical Curve(2) = {8, 5, 6, 7};  // internal radius
//+
Physical Curve(3) = {4, 1, 2, 3}; // external radius
