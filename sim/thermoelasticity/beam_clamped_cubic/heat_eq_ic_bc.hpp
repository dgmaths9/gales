#ifndef HEAT_EQ_IC_BC_HPP
#define HEAT_EQ_IC_BC_HPP



#include "../../../src/fem/fem.hpp"
#include <cmath>



namespace GALES{




  template<int dim>
  class heat_eq_ic_bc : public base_ic_bc<dim>  
  {
    using nd_type = node<dim>;
    using vec = boost::numeric::ublas::vector<double>;

    public : 
    


 
 
    //---------------------   IC  ------------------------------------------
    double initial_T(const nd_type &nd)const {
      return 0.0;
    }


 
     //------------set dirichlet bc------------------------------------      
    auto dirichlet_T(const nd_type &nd) const 
    {
      const double y(nd.get_y());
      const double T = 50*(y*y*y+y*y+y+1);
      return std::make_pair(true,T);
    }


    //--------------set Neummann bc-------------------------------------
    auto neumann_q1(const std::vector<std::shared_ptr<GALES::node<dim> > >& bd_nodes, int side_flag) const 
    {   
      return std::make_pair(false,0.0);
    }

    auto neumann_q2(const std::vector<std::shared_ptr<GALES::node<dim> > >& bd_nodes, int side_flag) const 
    {
      return std::make_pair(false,0.0);
    }
    
    auto neumann_q1(const std::vector<int>& bd_nodes, int side_flag) const 
    {   
      return std::make_pair(false,0.0);
    }

    auto neumann_q2(const std::vector<int>& bd_nodes, int side_flag) const 
    {
      return std::make_pair(false,0.0);
    }



  };


}


#endif
