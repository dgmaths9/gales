Mesh.MshFileVersion = 2;

l = 0.5;
h = 1;
lc = 0.01;

Point(1) = {-h,-l,0,lc};
Point(2) = { h,-l,0,lc};
Point(3) = { h, l,0,lc};
Point(4) = {-h, l,0,lc};

Point(5) = {-h, 0,0,lc};
Point(6) = { h, 0,0,lc};

//+
Line(1) = {4, 5};
//+
Line(2) = {5, 1};
//+
Line(3) = {1, 2};
//+
Line(4) = {2, 6};
//+
Line(5) = {6, 3};
//+
Line(6) = {3, 4};
//+
Curve Loop(1) = {1, 2, 3, 4, 5, 6};
//+
Plane Surface(1) = {1};
//+
Physical Surface(11) = {1};
//+
Physical Curve(2) = {1, 2, 4, 5};  // left/right
//+
Physical Curve(3) = {3};  // bottom
//+
Physical Curve(4) = {6}; // top
//+
Physical Point(5) = {5, 6}; // pinned center of sides
