Mesh.MshFileVersion = 2;

a = 0.5;
b = 0.05;
lc = 0.001;

Point(1) = { -a, -b,0,lc};
Point(2) = {  a, -b,0,lc};
Point(3) = {  a,  b,0,lc};
Point(4) = { -a,  b,0,lc};


//+
Line(1) = {4, 1};
//+
Line(2) = {1, 2};
//+
Line(3) = {2, 3};
//+
Line(4) = {3, 4};
//+
Curve Loop(1) = {1, 2, 3, 4};
//+
Plane Surface(1) = {1};
//+
Physical Surface(11) = {1};
//+
Physical Curve(2) = {1}; // left
//+
Physical Curve(5) = {3}; // right
//+
Physical Curve(3) = {2}; // bottom
//+
Physical Curve(4) = {4};  // top
