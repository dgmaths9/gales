solid
{
   material        Hookes
   
   plane_strain    T
   plane_stress    F
   axisymmetric    F

   rho             1
   E               2e11
   nu              0.3
   alpha           1.3e-5
   T_ref           0
}




heat_equation
{
   custom
   {
     rho       1
     cp        1       
     kappa     60
     mu        1
     heat_source 0.0
   }
}
