Mesh.MshFileVersion = 2;
lc = 0.1;

Point(1) = {-2, 0,0,lc};
Point(2) = { 2, 0,0,lc};
Point(3) = {-2,10,0,lc};
Point(4) = { 2,10,0,lc};
Point(5) = {-6,10,0,lc};
Point(6) = { 6,10,0,lc};
Point(7) = {-6,14,0,lc};
Point(8) = { 6,14,0,lc};

//+
Line(1) = {7, 5};
//+
Line(2) = {5, 3};
//+
Line(3) = {3, 1};
//+
Line(4) = {1, 2};
//+
Line(5) = {2, 4};
//+
Line(6) = {4, 6};
//+
Line(7) = {6, 8};
//+
Line(8) = {8, 7};
//+
Curve Loop(1) = {2, 3, 4, 5, 6, 7, 8, 1};
//+
Plane Surface(1) = {1};
//+
Physical Surface(11) = {1};
//+
Physical Curve(2) = {4};
//+
Physical Curve(3) = {3, 2, 1, 8, 7, 6, 5};
