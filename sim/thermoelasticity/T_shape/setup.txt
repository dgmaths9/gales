solid_mesh_file   mesh_8core.txt
heat_eq_mesh_file mesh_8core.txt
dim               2 
delta_t           10
final_time        20.1
restart           F
restart_time      10.0
n_max_it          3
print_freq        1

ls_solver         Flexible GMRES
ls_precond        ILU
ls_rel_res_tol    1.e-6
ls_maxsubspace    200
ls_maxrestarts    10
ls_fill           1

adaptive_time_step    F
dt_min                0.01
dt_max                0.1
N                     1
CFL                   0.8

steady_state                F
tau_diag_comp_2001          T
tau_diag_incomp_2007        F
dc_2006                     F
dc_sharp                    1.0 
dc_scale_fact               1.0            


Boussinesq_approximation    F
rho_ref                     1
T_ref                       0.0



