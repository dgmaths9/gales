Mesh.MshFileVersion = 2;

a = 1;
lc = 0.05;

Point(1) = {0,0,0,lc};
Point(2) = {-a,0,0,lc};
Point(3) = {a,0,0,lc};
Point(4) = {0,a,0,lc};
Point(5) = {0,-a,0,lc};
Point(6) = {0,0,a,lc};
Point(7) = {0,0,-a,lc};

//+
Circle(1) = {4, 1, 7};
//+
Circle(2) = {7, 1, 5};
//+
Circle(3) = {5, 1, 6};
//+
Circle(4) = {6, 1, 4};
//+
Circle(5) = {4, 1, 3};
//+
Circle(6) = {3, 1, 5};
//+
Circle(7) = {5, 1, 2};
//+
Circle(8) = {2, 1, 4};
//+
Circle(9) = {7, 1, 3};
//+
Circle(10) = {3, 1, 6};
//+
Circle(11) = {6, 1, 2};
//+
Circle(12) = {2, 1, 7};
//+
Curve Loop(1) = {5, -9, -1};
//+
Surface(1) = {1};
//+
Curve Loop(2) = {8, 1, -12};
//+
Surface(2) = {2};
//+
Curve Loop(3) = {4, -8, -11};
//+
Surface(3) = {3};
//+
Curve Loop(4) = {4, 5, 10};
//+
Surface(4) = {4};
//+
Curve Loop(5) = {9, 6, -2};
//+
Surface(5) = {5};
//+
Curve Loop(6) = {2, 7, 12};
//+
Surface(6) = {6};
//+
Curve Loop(7) = {11, -7, 3};
//+
Surface(7) = {7};
//+
Curve Loop(8) = {10, -3, -6};
//+
Surface(8) = {8};
//+
Surface Loop(1) = {6, 5, 1, 4, 3, 2, 7, 8};
//+
Volume(1) = {1};
//+
Physical Volume(11) = {1};
//+
Physical Surface(2) = {2, 3, 4, 1, 8, 5, 6, 7};
