solid_mesh_file   mesh_8core.txt
heat_eq_mesh_file mesh_8core.txt
dim               3 
delta_t           0.01
final_time        0.26
restart           F
restart_time      10.0
n_max_it          3
print_freq        1

ls_solver           CG
ls_precond          ILU
ls_overlaplevel     1
ls_fill             1
ls_left_precond     T
ls_rel_res_tol      1.e-9
ls_maxsubspace      200
ls_maxrestarts      5
ls_maxiters        -1

adaptive_time_step    F
dt_min                0.01
dt_max                0.1
N                     1
CFL                   0.8

steady_state                F
tau_diag_comp_2001          T
tau_diag_incomp_2007        F
dc_2006                     F
dc_sharp                    1.0 
dc_scale_fact               1.0            


Boussinesq_approximation    F
rho_ref                     1
T_ref                       0.0


pp_start_time  0
pp_final_time  0.26
pp_delta_t     0.01
