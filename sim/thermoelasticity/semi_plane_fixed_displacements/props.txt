solid
{
   material        Hookes
   
   plane_strain    T
   plane_stress    F
   axisymmetric    F

   E               1.0e10
   nu              0.25
   alpha           0.01
   T_ref           0
}

heat_equation
{
   custom
   {
     rho       1
     cp        1       
     kappa     1
     heat_source 0.0
   }
}
