Mesh.MshFileVersion = 2;

lc=5;
lc1=0.01;
d = 100;
h = 10;


Point(11) = {-d,-d,0,lc};
Point(12) = {d,-d,0,lc};
Point(13) = {-d,0,0,lc};
Point(14) = {d,0,0,lc};

Point(1) = {-h,0,0,lc1};
Point(2) = {h,0,0,lc1};



//+
Line(1) = {13, 1};
//+
Line(2) = {1, 2};
//+
Line(3) = {2, 14};
//+
Line(4) = {14, 12};
//+
Line(5) = {12, 11};
//+
Line(6) = {11, 13};
//+
Curve Loop(1) = {6, 1, 2, 3, 4, 5};
//+
Plane Surface(1) = {1};
//+
Physical Surface(11) = {1};
//+
Physical Curve(2) = {1, 2, 3}; // surface
//+
Physical Curve(3) = {6, 5, 4}; // lateral and bottom boundaries
