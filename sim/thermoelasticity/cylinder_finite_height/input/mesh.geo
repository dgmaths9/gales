Mesh.MshFileVersion = 2;

r = 50;
h = 60;
lc = 1;

Point(1) = {0,0,0,lc};

Point(2) = {r,0,0,lc};
Point(3) = {-r,0,0,lc};
Point(4) = {0,0,r,lc};
Point(5) = {0,0,-r,lc};

Point(11) = {0,h,0,lc};

Point(12) = {r,h,0,lc};
Point(13) = {-r,h,0,lc};
Point(14) = {0,h,r,lc};
Point(15) = {0,h,-r,lc};


//+
Circle(1) = {15, 11, 12};
//+
Circle(2) = {12, 11, 14};
//+
Circle(3) = {14, 11, 13};
//+
Circle(4) = {13, 11, 15};
//+
Circle(5) = {5, 1, 2};
//+
Circle(6) = {2, 1, 4};
//+
Circle(7) = {4, 1, 3};
//+
Circle(8) = {3, 1, 5};
//+
Line(9) = {13, 3};
//+
Line(10) = {14, 4};
//+
Line(11) = {15, 5};
//+
Line(12) = {12, 2};
//+
Curve Loop(1) = {9, -7, -10, 3};
//+
Surface(1) = {1};
//+
Curve Loop(2) = {4, 11, -8, -9};
//+
Surface(2) = {2};
//+
Curve Loop(3) = {1, 12, -5, -11};
//+
Surface(3) = {3};
//+
Curve Loop(4) = {2, 10, -6, -12};
//+
Surface(4) = {4};
//+
Curve Loop(5) = {3, 4, 1, 2};
//+
Plane Surface(5) = {5};
//+
Curve Loop(6) = {8, 5, 6, 7};
//+
Plane Surface(6) = {6};
//+
Surface Loop(1) = {1, 2, 5, 3, 4, 6};
//+
Volume(1) = {1};
//+
Physical Volume(11) = {1};
//+
Physical Surface(2) = {1, 2, 4, 3};  // fixed side
//+
Physical Surface(3) = {5, 6}; // free top/bottom
//+
Physical Curve(4) = {3, 2, 1, 4, 8, 7, 6, 5};  // circle at top and bottom fixed or free?
