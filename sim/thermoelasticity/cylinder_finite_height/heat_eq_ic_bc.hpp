#ifndef HEAT_EQ_IC_BC_HPP
#define HEAT_EQ_IC_BC_HPP



#include "../../../src/fem/fem.hpp"
#include <cmath>



namespace GALES{




  template<int dim>
  class heat_eq_ic_bc : public base_ic_bc<dim>  
  {
    using nd_type = node<dim>;
    using vec = boost::numeric::ublas::vector<double>;

    public : 
    


 
 
    //---------------------   IC  ------------------------------------------
    double initial_T(const nd_type &nd)const {
      return 0.0;
    }


 
     //------------set dirichlet bc------------------------------------      
    auto dirichlet_T(const nd_type &nd) const 
    {
      const double x(nd.get_x());
      const double z(nd.get_z());
      const double R2(pow(x/50,2)+pow(z/50,2));
      const double T = 100/(exp(R2)+pow(1+R2,7));
      return std::make_pair(true,T);
    }


    //--------------set Neummann bc-------------------------------------
    auto neumann_q1(const std::vector<int>& bd_nodes, int side_flag) const 
    {   
      return std::make_pair(false,0.0);
    }

    auto neumann_q2(const std::vector<int>& bd_nodes, int side_flag) const 
    {
      return std::make_pair(false,0.0);
    }
    
    auto neumann_q1(const std::vector<std::shared_ptr<nd_type>>& bd_nodes, int side_flag) const 
    {   
      return std::make_pair(false,0.0);
    }

    auto neumann_q2(const std::vector<std::shared_ptr<nd_type>>& bd_nodes, int side_flag) const 
    {   
      return std::make_pair(false,0.0);
    }

  };


}


#endif
