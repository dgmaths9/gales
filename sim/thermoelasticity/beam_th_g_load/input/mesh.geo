Mesh.MshFileVersion = 2;

lc=0.05;

H = 0.3;
L = 5;

Point(1) = {0,0,0,lc};
Point(2) = {L,0,0,lc};
Point(3) = {L,H,0,lc};
Point(4) = {0,H,0,lc};


//+
Line(1) = {4, 1};
//+
Line(2) = {1, 2};
//+
Line(3) = {2, 3};
//+
Line(4) = {3, 4};
//+
Curve Loop(1) = {4, 1, 2, 3};
//+
Plane Surface(1) = {1};
//+
Physical Surface(11) = {1};
//+
Physical Curve(2) = {1, 3}; // left/right ends
//+
Physical Curve(3) = {2}; // bottom
//+
Physical Curve(4) = {4}; // top
