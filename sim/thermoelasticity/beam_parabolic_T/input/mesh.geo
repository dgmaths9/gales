Mesh.MshFileVersion = 2;

lc=0.02;
d = 1;
h = 2;


Point(11) = {-h/2,-d/2,0,lc};
Point(12) = {h/2,-d/2,0,lc};
Point(13) = {h/2,d/2,0,lc};
Point(14) = {-h/2,d/2,0,lc};


//+
Line(1) = {14, 13};
//+
Line(2) = {13, 12};
//+
Line(3) = {12, 11};
//+
Line(4) = {11, 14};
//+
Curve Loop(1) = {4, 1, 2, 3};
//+
Plane Surface(1) = {1};
//+
Physical Surface(11) = {1};
//+
Physical Curve(2) = {4, 2}; // left right
//+
Physical Curve(3) = {3, 1}; // top bottom
