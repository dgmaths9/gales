
fluid
{
    Isothermal_T    273.0  

    ch  
    {
       custom         
       rho            100.0
       mu             1.0
       beta           0.00000000001
    }
}
