Mesh.MshFileVersion=2;

lc=0.2;

Point(1) = {0.0,0.0,0,lc};
Point(2) = {60.0,0.0,0,lc};
Point(3) = {60.0,6.0,0,lc};
Point(4) = {0.0,6.0,0,lc};

//+
Line(1) = {1, 2};
//+
Line(2) = {2, 3};
//+
Line(3) = {3, 4};
//+
Line(4) = {4, 1};
//+
Point(5) = {7.0,2.5,0,lc};
Point(6) = {7.0,1.0,0,lc};
Point(7) = {8.0,2.5,0,lc};
Point(8) = {7.0,4.0,0,lc};
Point(9) = {6.0,2.5,0,lc};

//+
Ellipse(5) = {8, 5, 9, 9};
//+
Ellipse(6) = {9, 5, 6, 6};
//+
Ellipse(7) = {6, 5, 7, 7};
//+
Ellipse(8) = {7, 5, 8, 8};
//+
Curve Loop(1) = {4, 1, 2, 3};
//+
Curve Loop(2) = {5, 6, 7, 8};
//+
Plane Surface(1) = {1, 2};

//+
Physical Surface(9) = {1};
//+
Physical Curve(4) = {4}; //inlet v
//+
Physical Curve(3) = {2}; //outlet fixed p
//+
Physical Curve(2) = {1, 3, 5, 8, 7, 6}; //no slip
//+
Physical Point(2) = {4, 1, 2, 3}; //no slip
