#ifndef __FLUID_IC_BC_HPP
#define __FLUID_IC_BC_HPP



#include "../../../src/fem/fem.hpp"



namespace GALES {




  template<int dim>
  class fluid_ic_bc : public base_ic_bc<dim> 
  {

    using nd_type = node<dim>;
    using el_type = element<dim>;
    using vec = boost::numeric::ublas::vector<double>;

  public :



    void body_force(vec& gravity) const 
    {
      gravity[1] = -0.001;
    }



   
    //-------------------- IC ----------------------------------------

    double initial_p(const nd_type &nd) const { return 0.0; }

    double initial_vx(const nd_type &nd) const 
    { 
    	  double y = nd.get_y();
	  double vx = -0.001*(y-3.0)*(y-3.0)+0.009;    
          return vx; 
    }
    
    double initial_vy(const nd_type &nd) const { return 0.0; }
 	





  // --------------------Dirichlet BC------------------------------------

    auto dirichlet_p(const nd_type &nd) const 
    {
	if( nd.flag() ==  3)	  return std::make_pair(true,0.0);   
	
      return std::make_pair(false,0.0);
    }


    auto dirichlet_vx(const nd_type &nd) const 
    { 

	if( nd.flag() ==  2)
	{
	     return std::make_pair(true,0.0); 
	}     
	if( nd.flag() ==  4)     
	{
	  double y = nd.get_y();
	  double vx = -0.001*(y-3.0)*(y-3.0)+0.009;
	  return std::make_pair(true,vx); 
	}
	
      return std::make_pair(false,0.0);
    }


    auto dirichlet_vy(const nd_type &nd) const  
    {
	if( nd.flag() ==  2)     return std::make_pair(true,0.0); 
	if( nd.flag() ==  4)     return std::make_pair(true,0.0); 
	  
      return std::make_pair(false,0.0);
    }




  //-------------------------Neumann BC--------------------------------------------------

    auto neumann_tau11(const std::vector<int>& bd_nodes, int side_flag) const   
    { 
      return std::make_pair(false,0.0);  
    }

    auto neumann_tau12(const std::vector<int>& bd_nodes, int side_flag) const   
    { 
    return std::make_pair(false,0.0);
    }
  
    auto neumann_tau22(const std::vector<int>& bd_nodes, int side_flag) const   
    { 
      return std::make_pair(false,0.0); 
    }


  };

  
} //namespace
#endif



