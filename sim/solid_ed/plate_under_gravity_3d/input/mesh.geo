//+
SetFactory("OpenCASCADE");





//+
Box(1) = {0, 0, 0, 2, 0.05, 1};
//+
Physical Volume(1) = {1};
//+
Physical Surface(5) = {1};
//+
Physical Surface(6) = {2};
//+
Characteristic Length {2, 4, 3, 1, 6, 8, 7, 5} = 0.05;
