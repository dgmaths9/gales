#ifndef SOLID_IC_BC_HPP
#define SOLID_IC_BC_HPP



#include "../../../src/fem/fem.hpp"


namespace GALES{



  template<int dim>
  class solid_ic_bc : public base_ic_bc<dim> 
  {
    using nd_type = node<dim>;
    using vec = boost::numeric::ublas::vector<double>;


  public : 
  






    //------------- IC ------------------------------------------
    double initial_ux (const nd_type &nd)const {return 0.0;}
    double initial_uy (const nd_type &nd)const {return 0.0;}

    double initial_vx (const nd_type &nd)const {return 0.0;}
    double initial_vy (const nd_type &nd)const {return 0.0;}



 
     //------------set dirichlet bc------------------------------------      
    auto dirichlet_ux(const nd_type &nd) const 
    {
      if( nd.flag() == 6)	return std::make_pair(true,0.0);  
       
      return std::make_pair(false,0.0);
    }

    auto dirichlet_uy(const nd_type &nd) const 
    {
	return std::make_pair(true,0.0);
    }
 


    //--------------set Neummann bc-------------------------------------
    auto neumann_tau11(const std::vector<int>& bd_nodes, int side_flag) const 
    { 
	double t = time::get().t();
	
	if(side_flag == 7) 
	{
	  double tmp1(0.0), tmp2(0.0), tmp3(0.0);
	  if(t<0.875) 
	  {
	   tmp1 = t/0.875;
	   return std::make_pair(true,tmp1);
	  }

	  if(0.875<=t&& t<=1.75) 
	  {
	   tmp2 = 2.0 - t/0.875; 
	   return std::make_pair(true,tmp2);
	  }

	  if(t>1.75) 
	  {
	   tmp3= 0.0;     
           return std::make_pair(true,tmp3);
	  }
	}
      return std::make_pair(false,0.0);
    }
    
    

    auto neumann_tau22(const std::vector<int>& bd_nodes, int side_flag) const 
    {
      return std::make_pair(false,0.0); 
    }

    auto neumann_tau12(const std::vector<int>& bd_nodes, int side_flag) const 
    {    
     return std::make_pair(false,0.0);
    }
  


  };


}

#endif
