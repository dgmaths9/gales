Mesh.MshFileVersion = 2;

lc = 0.1;
Point(1) = {0,0,0,lc};
Point(2) = {1,0,0,lc};
Point(3) = {1,0.01,0,lc};
Point(4) = {0,0.01,0,lc};

Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,1};


Line Loop(5) = {2, 3, 4, 1};
Plane Surface(6) = {5};
Recombine Surface{6};

Transfinite Line{1,3} = 101;
Transfinite Line{2,4} = 3;
Transfinite Surface{6}; 


Physical Line(6) = {4};
Physical Line(7) = {2};
Physical Line(3) = {1, 3};
Physical Point(7) = {2, 3};
Physical Point(6) = {1, 4};

Physical Surface(0) = {6};

