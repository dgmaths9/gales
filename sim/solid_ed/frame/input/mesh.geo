Mesh.MshFileVersion = 2;
lc = 0.2;

Point(1) = {0,0,0,lc};
Point(2) = {2,0,0,lc};
Point(3) = {2,2,0,lc};
Point(4) = {4,2,0,lc};
Point(5) = {4,0,0,lc};
Point(6) = {6,0,0,lc};
Point(7) = {6,4,0,lc};
Point(8) = {0,4,0,lc};


Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,5};
Line(5) = {5,6};
Line(6) = {6,7};
Line(7) = {7,8};
Line(8) = {8,1};




Line Loop(1) = {8, 1, 2, 3, 4, 5, 6, 7};
Plane Surface(1) = {1};
Physical Surface(0) = {1};
Physical Line(5) = {1, 5};
Physical Line(8) = {8};
Physical Point(5) = {1};
