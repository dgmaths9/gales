Mesh.MshFileVersion=2;

lc=50;
lc1=10;
lc2=1000;





Point(1) = {-20000, 0,0,lc};
Point(2) = {20000, 0,0,lc};
Point(3) = {-20000, -20000,0,lc2};
Point(4) = {20000, -20000,0,lc2};


//+
Line(1) = {1, 3};
//+
Line(2) = {3, 4};
//+
Line(3) = {4, 2};
//+
Line(4) = {2, 1};





Point(5) = {-50, -3000,0,lc1};
Point(6) = {50, -3000,0,lc1};
Point(7) = {-50, -5000,0,lc1};
Point(8) = {50, -5000,0,lc1};

//+
Line(5) = {5, 7};
//+
Line(6) = {7, 8};
//+
Line(7) = {8, 6};
//+
Line(8) = {6, 5};
//+
Curve Loop(1) = {4, 1, 2, 3};
//+
Curve Loop(2) = {7, 8, 5, 6};
//+
Plane Surface(1) = {1, 2};
//+
Physical Surface(9) = {1};
//+
Physical Curve(2) = {7, 8, 5, 6};
//+
Physical Curve(5) = {1, 2, 3};
//+
Physical Curve(3) = {4};
//+
Physical Point(5) = {1, 3, 4, 2};
