Mesh.MshFileVersion=2;

lc=1000;



//+
SetFactory("OpenCASCADE");
//+
Ellipse(1) = {0, -8000, 0, 2500, 500, 0, 2*Pi};
//+
Ellipse(2) = {-1800, -2500, 0, 500, 100, 0, 2*Pi};
//+
Rotate {{0, 0, 1}, {-1800, -2500, 0}, Pi/2+Pi/9} {
  Curve{2}; 
}


Point(100) = {0, 1500,0,lc};
Point(101) = {-20000, 0,0,lc};
Point(102) = {-5000, 0,0,lc};
Point(103) = {-2000, 300,0,lc};
Point(104) = {-1000, 1000,0,lc};
Point(105) = {-500, 1400,0,lc};
Point(106) = {500, 1400,0,lc};
Point(107) = {1000, 1000,0,lc};
Point(108) = {2000, 300,0,lc};
Point(109) = {5000, 0,0,lc};
Point(110) = {20000, 0,0,lc};



//+
BSpline(3) = {101, 102, 103, 104, 105, 100, 106, 107, 108, 109, 110};

Point(111) = {-20000, -20000,0,lc};
Point(112) = {20000, -20000,0,lc};

//+
Line(4) = {101, 111};
//+
Line(5) = {111, 112};
//+
Line(6) = {112, 110};
//+
Characteristic Length {1, 2, 102, 103, 104, 105, 100, 106, 107, 108, 109} = 10;
//+
Characteristic Length {101, 110} = 100;
//+
Characteristic Length {111, 112} = 1000;
//+
Curve Loop(1) = {3, -6, -5, -4};
//+
Curve Loop(2) = {1};
//+
Curve Loop(3) = {2};
//+
Plane Surface(1) = {1, 2, 3};
//+
Physical Surface(0) = {1};
//+
Physical Curve(2) = {3};
//+
Physical Curve(5) = {4, 5, 6};
//+
Physical Curve(3) = {2};
//+
Physical Curve(4) = {1};
