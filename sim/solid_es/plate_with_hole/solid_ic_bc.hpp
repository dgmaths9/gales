#ifndef SOLID_IC_BC_HPP
#define SOLID_IC_BC_HPP



#include "../../../src/fem/fem.hpp"



namespace GALES{




  template<int dim>
  class solid_ic_bc : public base_ic_bc<dim>  
  {
    using nd_type = node<dim>;
    using vec = boost::numeric::ublas::vector<double>;

    public : 
     
 
 
    //---------------------   IC  ------------------------------------------
    double initial_ux(const nd_type &nd)const {return 0.0;}
    double initial_uy(const nd_type &nd)const {return 0.0;}




 
     //------------set dirichlet bc------------------------------------      
    auto dirichlet_ux(const nd_type &nd) const 
    {
      if(nd.flag() == 2)       return std::make_pair(true,0.0);
      
      return std::make_pair(false,0.0);
    }

    auto dirichlet_uy(const nd_type &nd)const 
    {
      if(nd.flag() == 3)       return std::make_pair(true,0.0);

	return std::make_pair(false,0.0);
    }
 



    //--------------set Neummann bc-------------------------------------
    auto neumann_tau11(const std::vector<int>& bd_nodes, int side_flag) const 
    { 
      if(side_flag == 4)       return std::make_pair(true,1.e4);

      return std::make_pair(false,0.0);
    }

    auto neumann_tau22(const std::vector<int>& bd_nodes, int side_flag) const 
    {
      return std::make_pair(false,0.0); 
    }

    auto neumann_tau12(const std::vector<int>& bd_nodes, int side_flag) const 
    {    
     return std::make_pair(false,0.0);
    }

    auto neumann_pressure(const std::vector<int>& bd_nodes, int side_flag) const 
    { 
      return std::make_pair(false,0.0);
    }
  

  };


}


#endif
