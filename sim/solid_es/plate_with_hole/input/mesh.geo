Mesh.MshFileVersion = 2;
lc = 0.005;
Point(1) = {0,0,0,lc};
Point(2) = {0.11,0,0,lc};
Point(3) = {0,0.11,0,lc};
Point(4) = {0.55,0,0,lc};
Point(5) = {0.55,0.55,0,lc};
Point(6) = {0,0.55,0,lc};

Circle(1) = {2, 1, 3};
Line(2) = {2, 4};
Line(3) = {4, 5};
Line(4) = {5, 6};
Line(5) = {6, 3};
Line Loop(6) = {4, 5, -1, 2, 3};

Plane Surface(7) = {6};
Recombine Surface {7};


//+
Physical Line(2) = {5};
//+
Physical Line(3) = {2};
//+
Physical Line(4) = {3};
//+
Physical Surface(0) = {7};
