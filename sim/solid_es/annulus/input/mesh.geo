Mesh.MshFileVersion = 2;
lc=0.05;


Point(1) = {0,0,0,lc};

Point(2) = {1,0,0,lc};
Point(3) = {0,1,0,lc};
Point(4) = {-1,0,0,lc};
Point(5) = {0,-1,0,lc};

Point(6) = {2,0,0,lc};
Point(7) = {0,2,0,lc};
Point(8) = {-2,0,0,lc};
Point(9) = {0,-2,0,lc};

//+
Circle(1) = {2, 1, 3};
//+
Circle(2) = {3, 1, 4};
//+
Circle(3) = {4, 1, 5};
//+
Circle(4) = {5, 1, 2};
//+
Circle(5) = {6, 1, 7};
//+
Circle(6) = {7, 1, 8};
//+
Circle(7) = {8, 1, 9};
//+
Circle(8) = {9, 1, 6};


//+
Line(9) = {8, 4};
//+
Line(10) = {3, 7};
//+
Line(11) = {2, 6};
//+
Line(12) = {5, 9};

//+
Curve Loop(1) = {6, 9, -2, 10};
//+
Surface(1) = {1};
//+
Curve Loop(2) = {10, -5, -11, 1};
//+
Surface(2) = {2};
//+
Curve Loop(3) = {11, -8, -12, 4};
//+
Surface(3) = {3};
//+
Curve Loop(4) = {12, -7, 9, 3};
//+
Surface(4) = {4};
//+
Recombine Surface {4};
//+
Recombine Surface {3};
//+
Recombine Surface {2};
//+
Recombine Surface {1};
//+
Transfinite Curve {9, 12, 11, 10} = 21 Using Progression 1;
//+
Transfinite Curve {6, 2, 1, 5, 4, 8, 3, 7} = 51 Using Progression 1;
//+
Transfinite Surface {1};
//+
Transfinite Surface {4};
//+
Transfinite Surface {3};
Transfinite Surface {2};
//+
Physical Curve(2) = {2, 3, 4, 1};
//+
Physical Surface(13) = {1, 4, 3, 2};
