Mesh.MshFileVersion=2;


lc = 50;
lc1 = 10;
lc2 = 100;

Point(1) = {0,0,0,lc1};           //top left
Point(2) = {20000,0,0,lc};             //top right
Point(3) = {20000,-20000,0,lc2};       //bottom right
Point(4) = {0,-20000,0,lc2};      //bottom left


// chamber
d = 2000;
r = 1000;
Point(5) = {0,-d,0,lc1};
Point(6) = {0,-d+r,0,lc1};
Point(7) = {0,-d-r,0,lc1};
Point(8) = {r,-d,0,lc1};





//+
Line(1) = {4, 3};
//+
Line(2) = {3, 2};
//+
Line(3) = {2, 1};
//+
Line(4) = {1, 6};
//+
Line(5) = {7, 4};
//+
Circle(6) = {6, 5, 8};
//+
Circle(7) = {8, 5, 7};
//+
Line Loop(1) = {5, 1, 2, 3, 4, 6, 7};
//+
Plane Surface(1) = {1};
//+
Physical Surface(1) = {1};
//+
Physical Line(2) = {3};
//+
Physical Line(5) = {1, 2};
//+
Physical Line(6) = {7, 6};
//+
Physical Line(3) = {5, 4};

//+
Physical Point(5) = {3, 2};
//+
Physical Point(3) = {1, 6, 7, 4};
