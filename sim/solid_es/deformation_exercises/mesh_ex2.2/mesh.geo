Mesh.MshFileVersion=2;


lc = 20;
lc1 = 5;
lc2 = 100;

Point(1) = {0,0,0,lc1};           //top left
Point(2) = {10000,0,0,lc};             //top right
Point(3) = {10000,-10000,0,lc2};       //bottom right
Point(4) = {0,-10000,0,lc2};      //bottom left


// chamber
d = 1100;
rx = 1000;
ry = 100;

Point(5) = {0,-d,0,lc1};
Point(6) = {0,-d+ry,0,lc1};
Point(7) = {0,-d-ry,0,lc1};
Point(8) = {rx,-d,0,lc1};


//+
Line(1) = {4, 3};
//+
Line(2) = {3, 2};
//+
Line(3) = {2, 1};
//+
Line(4) = {1, 6};
//+
Line(5) = {7, 4};
//+
Ellipse(6) = {7, 5, 6, 8};
//+
Ellipse(7) = {6, 5, 7, 8};
//+
Line Loop(1) = {5, 1, 2, 3, 4, 7, -6};
//+
Plane Surface(1) = {1};
//+
Physical Surface(1) = {1};
//+
Physical Line(5) = {1, 2};
//+
Physical Line(6) = {6, 7};
//+
Physical Line(3) = {5, 4};
//+
Physical Line(2) = {3};
//+
Physical Point(5) = {4, 3, 2};
//+
Physical Point(3) = {1, 6, 7};
