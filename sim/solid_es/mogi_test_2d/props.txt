
solid
{
   material        Hookes
   
   plane_strain    F
   plane_stress    F
   axisymmetric    T

   rho             3000.0
   E               25.e9
   nu              0.25
}
