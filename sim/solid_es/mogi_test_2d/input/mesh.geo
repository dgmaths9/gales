Mesh.MshFileVersion=2;




r = 1000;    //radius = 1km
d = 4000;    //depth = 4km




h1 = 5;   // mesh element size around and above chamber
h2 = 20;   // mesh element size at surface far boundary
h3 = 500;  // mesh element size at far boundaries


Point(1) = {0,0,0,h1};           //top left
Point(2) = {20000,0,0,h2};             //top right
Point(3) = {20000,-20000,0,h3};       //bottom right
Point(4) = {0,-20000,0,h3};      //bottom left



// chamber
Point(5) = {0,-d,0,h1};
Point(6) = {0,-d+r,0,h1};
Point(7) = {0,-d-r,0,h1};
Point(8) = {r,-d,0,h1};





//+
Line(1) = {4, 3};
//+
Line(2) = {3, 2};
//+
Line(3) = {2, 1};
//+
Line(4) = {1, 6};
//+
Line(5) = {7, 4};
//+
Circle(6) = {6, 5, 8};
//+
Circle(7) = {8, 5, 7};
//+
Line Loop(1) = {5, 1, 2, 3, 4, 6, 7};
//+
Plane Surface(1) = {1};
//+
Physical Surface(1) = {1};
//+
Physical Line(2) = {3};
//+
Physical Line(5) = {1, 2};
//+
Physical Line(6) = {7, 6};
//+
Physical Line(3) = {5, 4};

//+
Physical Point(5) = {3, 2};
//+
Physical Point(3) = {1, 6, 7, 4};
