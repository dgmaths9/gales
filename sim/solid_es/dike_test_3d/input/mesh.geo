SetFactory("OpenCASCADE");

Box(1) = {-500, -7000, -500, 100, 100, 1000};
Box(2) = {-10000, 0, -10000, 20000, -30000, 20000};

BooleanDifference{ Volume{2}; Delete; }{ Volume{1}; Delete; }
//+
Physical Volume(25) = {2};
//+
Physical Surface(5) = {3, 1, 2, 6, 5};
//+
Physical Surface(2) = {4};
//+
Physical Surface(4) = {9, 12, 11, 10, 7, 8};
//+
Physical Curve(5) = {8, 3, 10, 11};
//+
Physical Point(5) = {3, 4, 8, 7};


//+
MeshSize {1, 2, 5, 6} = 1000;
//+
MeshSize {3, 7, 8, 4} = 100;
//+
MeshSize {9, 10, 14, 13, 11, 12, 15, 16} = 20;
