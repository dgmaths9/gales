#ifndef SOLID_IC_BC_HPP
#define SOLID_IC_BC_HPP



#include "../../../src/fem/fem.hpp"



namespace GALES{




  template<int dim>
  class solid_ic_bc : public base_ic_bc<dim>  
  {
    using nd_type = node<dim>;
    using vec = boost::numeric::ublas::vector<double>;

    public : 
     
     
    solid_ic_bc()
    {
      matching_nd_gid_ = read_matching_nd_gid("input/fsi_nd.txt");    
    } 
 
 
    //---------------------   IC  ------------------------------------------
    double initial_ux(const nd_type &nd)const {return 0.0;}
    double initial_uy(const nd_type &nd)const {return 0.0;}




 
     //------------set dirichlet bc------------------------------------      
    auto dirichlet_ux(const nd_type &nd) const 
    {
      if(nd.flag() == 3)       return std::make_pair(true,0.0);
      
      return std::make_pair(false,0.0);
    }

    auto dirichlet_uy(const nd_type &nd)const 
    {
      if(nd.flag() == 3)       return std::make_pair(true,0.0);

	return std::make_pair(false,0.0);
    }
 



    //--------------set Neummann bc-------------------------------------
    auto neumann_tau11(const std::vector<int>& bd_nodes, int side_flag) const 
    { 
      return std::make_pair(false,0.0);
    }

    auto neumann_tau22(const std::vector<int>& bd_nodes, int side_flag) const 
    {
      return std::make_pair(false,0.0); 
    }

    auto neumann_tau12(const std::vector<int>& bd_nodes, int side_flag) const 
    {    
     return std::make_pair(false,0.0);
    }

    auto neumann_pressure(const std::vector<int>& bd_nodes, int side_flag) const 
    { 
      if(side_flag == 5)
      {      
         std::vector<int> f_nds;
         for(auto i : bd_nodes)
           for(auto a : matching_nd_gid_)
             if(a.second == i)
                f_nds.push_back(a.first);

         assert(f_nds.size() == 2);
         return std::make_pair(true, 0.5*(overp_[f_nds[0]] + overp_[f_nds[1]]) );      
      }

      return std::make_pair(false,0.0);
    }
  

    void set_f_overp(const std::vector<double>& f_dofs_0, const std::vector<double>& f_dofs)
    {
       const int overp_size = 12510;   // 12510 nodes on magma rock interface
       const int nb_dofs = 5;      
       overp_.resize(overp_size);
       for(int i=0; i<overp_size; i++)
          overp_[i] = f_dofs[i*nb_dofs]-f_dofs_0[i*nb_dofs];
                 
    }


    private:
      std::map<int,int> matching_nd_gid_;
      std::vector<double> overp_;
      
  };


}


#endif
