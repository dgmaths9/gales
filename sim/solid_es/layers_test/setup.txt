solid_mesh_file       mesh_1core.txt
dim                   2 
delta_t               1
final_time            1
restart               F
restart_time          0.0
n_max_it              1
print_freq            1

ls_solver           CG
ls_precond          IC
ls_overlaplevel     0
ls_fill             0
ls_left_precond     T
ls_rel_res_tol      1.e-6
ls_maxsubspace      200
ls_maxrestarts      5
ls_maxiters        -1


