#ifndef SOLID_IC_BC_HPP
#define SOLID_IC_BC_HPP



#include "../../../src/fem/fem.hpp"



namespace GALES{




  template<int dim>
  class solid_ic_bc : public base_ic_bc<dim> 
  {
    using nd_type = node<dim>;
    using vec = boost::numeric::ublas::vector<double>;

    public : 
    


 
 
 
 
    //---------------------   IC  ------------------------------------------
    double initial_ux(const nd_type &nd)const {return 0.0;}
    double initial_uy(const nd_type &nd)const {return 0.0;}




 
     //------------set dirichlet bc------------------------------------      
    auto dirichlet_ux(const nd_type &nd) const 
    {
      if(nd.flag() == 5)      return std::make_pair(true,0.0);

      return std::make_pair(false,0.0);
    }

    auto dirichlet_uy(const nd_type &nd)const 
    {
      if(nd.flag() == 5)      return std::make_pair(true,0.0);

	return std::make_pair(false,0.0);
    }
 



    //--------------set Neummann bc-------------------------------------
    auto neumann_tau11(const std::vector<int>& bd_nodes, int side_flag) const 
    { 
      return std::make_pair(false,0.0);
    }

    auto neumann_tau22(const std::vector<int>& bd_nodes, int side_flag) const 
    {
      return std::make_pair(false,0.0); 
    }

    auto neumann_tau12(const std::vector<int>& bd_nodes, int side_flag) const 
    {          
      return std::make_pair(false,0.0);
    }
  

    auto neumann_pressure(const std::vector<int>& bd_nodes, int side_flag) const 
    { 
      if(side_flag == 3)      return std::make_pair(true, 0.5*(overp_[bd_nodes[0]] + overp_[bd_nodes[1]]) );      

      return std::make_pair(false, 0.0);
    }

  
        
    void set_f_overp(const std::vector<double>& f_dofs_0, const std::vector<double>& f_dofs)
    {
       const int overp_size = 5640;   // we have [0:5631] nodes on magma domain  
       const int nb_dofs = 5;      
       overp_.resize(overp_size);
       for(std::size_t i=0; i<overp_size; i++)
          overp_[i] = f_dofs[i*nb_dofs]-f_dofs_0[i*nb_dofs];
                 
    }


    private:
      std::vector<double> overp_;
      

  };


}


#endif
