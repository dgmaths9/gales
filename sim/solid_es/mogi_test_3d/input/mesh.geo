SetFactory("OpenCASCADE");
Sphere(1) = {0, -4000, 0, 1000, -Pi/2, Pi/2, 2*Pi};
Box(2) = {-50000, 0, -50000, 100000, -100000, 100000};
BooleanDifference{ Volume{2}; Delete; }{ Volume{1}; Delete; }



//+
Physical Volume(16) = {2};
//+
Physical Surface(5) = {5, 1, 3, 2, 6};
//+
Physical Surface(4) = {7};
//+
Physical Curve(5) = {3, 8, 11, 10};
//+
Physical Point(5) = {4, 3, 7, 8};
//+
MeshSize {4, 3, 7, 8} = 1000;
//+
MeshSize {9, 10} = 100;
//+
MeshSize {2, 1, 6, 5} = 10000;
