#!/usr/bin/python3

import gmsh
import math
import os
import sys






# this should be binary stl data file path
input_file = 'etna_surface.stl'


z = -50000.0
mesh_min_size = 50.0
mesh_max_size = 1000.0


N=1
#N = os.cpu_count()
print('cpu_count: ', N)




#parameters 
cx, cy, cz =  50000, 50000, -5000
dx, dy, dz = 3000, 3000, 3000
theta_x, theta_y, theta_z = 30, 40, 60    







def outer_box(h):
    # classify the surface mesh according to given angle, and create discrete model
    # entities (surfaces, curves and points) accordingly; curveAngle forces bounding
    # curves to be split on sharp corners
    gmsh.model.mesh.classifySurfaces(math.pi, curveAngle=math.pi / 3)


    
    # create a geometry for the discrete curves and surfaces
#    gmsh.model.mesh.createGeometry()
    
    # retrieve the surface, its boundary curves and corner points
    s = gmsh.model.getEntities(2)     # print(s) = [(2, 2)]      (dim, surface tag)
    c = gmsh.model.getBoundary(s)     # print(c) = [(1, 3), (1, 4), (1, 5), (1, 6)]   (dim, boundary curve tag) 
    


    if (len(c) != 4):
        print('s: ', s)
        print('c: ', c)
        gmsh.logger.write('Should have 4 boundary curves!', level='error')
    
    
    
    p = []
    xyz = []
    for e in c:
        pt = gmsh.model.getBoundary([e], combined=False) 
        # print (pt) = [(0, 1), (0, 2)],   [(0, 2), (0, 3)],    [(0, 3), (0, 4)],    [(0, 4), (0, 1)]         (dim, point tag)
        
        p.extend([pt[0][1]])        
        xyz.extend(gmsh.model.getValue(0, pt[0][1], []))
    
    # print(p)  = [1, 2, 3, 4]
    # print(xyz) = [p1x, p1y, p1z,  p2x, p2y, p2z,  p3x, p3y, p3z,  p4x, p4y, p4z]
    
    
    
    # create other CAD entities to form one volume below the terrain surface; beware
    # that only built-in CAD entities can be hybrid, i.e. have discrete entities on
    # their boundary: OpenCASCADE does not support this feature
    p1 = gmsh.model.geo.addPoint(xyz[0], xyz[1], z, h)    # bottom surface points
    p2 = gmsh.model.geo.addPoint(xyz[3], xyz[4], z, h)
    p3 = gmsh.model.geo.addPoint(xyz[6], xyz[7], z, h)
    p4 = gmsh.model.geo.addPoint(xyz[9], xyz[10], z, h)      
    
    c1 = gmsh.model.geo.addLine(p1, p2)
    c2 = gmsh.model.geo.addLine(p2, p3)
    c3 = gmsh.model.geo.addLine(p3, p4)
    c4 = gmsh.model.geo.addLine(p4, p1)
    
    c5 = gmsh.model.geo.addLine(p1, p[0])
    c6 = gmsh.model.geo.addLine(p2, p[1])
    c7 = gmsh.model.geo.addLine(p3, p[2])
    c8 = gmsh.model.geo.addLine(p4, p[3])
    
    ll1 = gmsh.model.geo.addCurveLoop([c1, c2, c3, c4])      
    s1 = gmsh.model.geo.addPlaneSurface([ll1])              # bottom surface
    
    ll2 = gmsh.model.geo.addCurveLoop([c1, c6, -c[0][1], -c5])
    s2 = gmsh.model.geo.addPlaneSurface([ll2])              #front surface

    ll3 = gmsh.model.geo.addCurveLoop([c2, c7, -c[1][1], -c6])
    s3 = gmsh.model.geo.addPlaneSurface([ll3])              # right surface 

    ll4 = gmsh.model.geo.addCurveLoop([c3, c8, -c[2][1], -c7])
    s4 = gmsh.model.geo.addPlaneSurface([ll4])              # back surface

    ll5 = gmsh.model.geo.addCurveLoop([c4, c5, -c[3][1], -c8])
    s5 = gmsh.model.geo.addPlaneSurface([ll5])              # left surface 

    #s[0][1]     top surface with topography

    sl1 = gmsh.model.geo.addSurfaceLoop([s1, s2, s3, s4, s5, s[0][1]])

    return sl1, s1, s2, s3, s4, s5, s[0][1], c[0][1], c[1][1], c[2][1], c[3][1], p[0], p[1], p[2], p[3]








def box_hole(cx, cy, cz, x_l, y_l, z_l, h):
    p1 = gmsh.model.geo.addPoint(cx-x_l, cy-y_l, cz-z_l, h)
    p2 = gmsh.model.geo.addPoint(cx+x_l, cy-y_l, cz-z_l, h)
    p3 = gmsh.model.geo.addPoint(cx+x_l, cy+y_l, cz-z_l, h)
    p4 = gmsh.model.geo.addPoint(cx-x_l, cy+y_l, cz-z_l, h)

    p5 = gmsh.model.geo.addPoint(cx-x_l, cy-y_l, cz+z_l, h)
    p6 = gmsh.model.geo.addPoint(cx+x_l, cy-y_l, cz+z_l, h)
    p7 = gmsh.model.geo.addPoint(cx+x_l, cy+y_l, cz+z_l, h)
    p8 = gmsh.model.geo.addPoint(cx-x_l, cy+y_l, cz+z_l, h)

    c1 = gmsh.model.geo.addLine(p1, p2)
    c2 = gmsh.model.geo.addLine(p2, p3)
    c3 = gmsh.model.geo.addLine(p3, p4)
    c4 = gmsh.model.geo.addLine(p4, p1)
    
    c5 = gmsh.model.geo.addLine(p5, p6)
    c6 = gmsh.model.geo.addLine(p6, p7)
    c7 = gmsh.model.geo.addLine(p7, p8)
    c8 = gmsh.model.geo.addLine(p8, p5)

    c9 = gmsh.model.geo.addLine(p1, p5)
    c10 = gmsh.model.geo.addLine(p2, p6)
    c11 = gmsh.model.geo.addLine(p3, p7)
    c12 = gmsh.model.geo.addLine(p4, p8)
    
    ll1 = gmsh.model.geo.addCurveLoop([c1, c2, c3, c4])      
    s1 = gmsh.model.geo.addPlaneSurface([ll1])              #bottom -ve z
        
    ll2 = gmsh.model.geo.addCurveLoop([c5, c6, c7, c8])      
    s2 = gmsh.model.geo.addPlaneSurface([ll2])              #top +ve z 

    ll3 = gmsh.model.geo.addCurveLoop([c1, c10, -c5, -c9])      
    s3 = gmsh.model.geo.addPlaneSurface([ll3])              #front -ve y

    ll4 = gmsh.model.geo.addCurveLoop([c7, -c12, -c3, c11])      
    s4 = gmsh.model.geo.addPlaneSurface([ll4])              #back +ve y

    ll5 = gmsh.model.geo.addCurveLoop([c8, -c9, -c4, c12])      
    s5 = gmsh.model.geo.addPlaneSurface([ll5])              #left -ve x

    ll6 = gmsh.model.geo.addCurveLoop([c6, -c11, -c2, c10])      
    s6 = gmsh.model.geo.addPlaneSurface([ll6])              #right +ve x
    
    sl1 = gmsh.model.geo.addSurfaceLoop([s2, s3, s1, s6, s4, s5])

    return sl1, s1, s2, s3, s4, s5, s6








# theta_x: angle of anticlockwise rotation in xy plane perpendicular to z-axis    < 180
# theta_y: angle of anticlockwise rotation in yz plane perpendicular to x-axis    < 180
# theta_z: angle of anticlockwise rotation in zx plane perpendicular to y-axis    < 180
def transformation(cx,cy,cz, B_bottom, B_top, B_front, B_back, B_left, B_right, theta_x, theta_y, theta_z):
    gmsh.model.geo.rotate([(2,B_bottom), (2,B_top), (2,B_front), (2,B_back), (2,B_left), (2,B_right)],  cx,cy,cz,  0,0,1, theta_x)
    gmsh.model.geo.rotate([(2,B_bottom), (2,B_top), (2,B_front), (2,B_back), (2,B_left), (2,B_right)],  cx,cy,cz,  1,0,0, theta_y)
    gmsh.model.geo.rotate([(2,B_bottom), (2,B_top), (2,B_front), (2,B_back), (2,B_left), (2,B_right)],  cx,cy,cz,  0,1,0, theta_z)








def execute(input_file, z, mesh_min_size, mesh_max_size):

    gmsh.initialize(sys.argv)
    
    path = os.path.dirname(os.path.abspath(__file__))
    
    # load an STL surface
    gmsh.merge(os.path.join(path, input_file))
    
    
    ob_sl, ob_bottom, ob_front, ob_right, ob_back, ob_left, ob_top, c1, c2, c3, c4, p1, p2, p3, p4 = outer_box(h=1000)


    B_sl, B_bottom, B_top, B_front, B_back, B_left, B_right = box_hole(cx,cy,cz, dx,dy,dz, 100)    

    transformation(cx,cy,cz, B_bottom, B_top, B_front, B_back, B_left, B_right, theta_x, theta_y, theta_z)

        
    
    v = gmsh.model.geo.addVolume([ob_sl, B_sl])

    
      
    gmsh.option.setNumber('Mesh.MeshSizeMin', mesh_min_size)
    gmsh.option.setNumber('Mesh.MeshSizeMax', mesh_max_size)
    gmsh.option.setNumber("Mesh.MeshSizeFromCurvature", 20)    


    gmsh.model.geo.synchronize()
    
    
    
    gmsh.model.addPhysicalGroup(3, [v], 10)  # volume tag = 10
    
    gmsh.model.addPhysicalGroup(2, [ob_bottom, ob_front, ob_right, ob_back, ob_left], 5)   # lateral boundaries tag = 5
    gmsh.model.addPhysicalGroup(1, [c1, c2, c3, c4], 5)   # lateral boundaries tag = 5
    gmsh.model.addPhysicalGroup(0, [p1, p2, p3, p4], 5)   # lateral boundaries tag = 5
    
    gmsh.model.addPhysicalGroup(2, [B_sl, B_bottom, B_top, B_front, B_back, B_left, B_right], 4)   # hole boundaries tag = 4
    
    

    
    gmsh.option.setNumber("Mesh.Algorithm3D", 10)    
    gmsh.option.setNumber("General.NumThreads", N)   # for parallel 3D meshing
    gmsh.model.mesh.generate(3)
    gmsh.option.setNumber("Mesh.MshFileVersion", 2)
    gmsh.write("mesh.msh")
        
    gmsh.finalize()






execute(input_file, z, mesh_min_size, mesh_max_size)    

