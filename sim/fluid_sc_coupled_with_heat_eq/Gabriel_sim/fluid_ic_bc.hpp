#ifndef FLUID_IC_BC_HPP
#define FLUID_IC_BC_HPP



#include "../../../../src/fem/fem.hpp"





namespace GALES{



  template<int dim>
  class fluid_ic_bc : public base_ic_bc<dim>   
  {
    using nd_type = node<dim>;
    using vec = boost::numeric::ublas::vector<double>;


    public :

    fluid_ic_bc()
    {
        this->central_pressure_profile_ = true;
        this->p0_ = 45.e6;
        this->max_ = -2100.0;
        this->min_ = -2360.0;
        this->h_ = 1.0;
        this->total_steps_ = (int)((this->max_-this->min_)/this->h_);     

        this->p_ref_ = this->p0_;
        this->v1_ref_ = 0.0;
        this->v2_ref_ = 0.0;
        this->T_ref_ = 1173.15;

        matching_nd_gid_ = read_matching_nd_gid("input/fhi_nd.txt");  
    }







    void body_force(vec& gravity) const
    {
      gravity[1] = -9.812;
    }







    //-------------------- IC ----------------------------------------
    double initial_p(const nd_type &nd) const { return 0.0; }
    double initial_vx(const nd_type &nd) const { return 0.0; }
    double initial_vy(const nd_type &nd) const { return 0.0; }

    double initial_T(const nd_type &nd) const  
    {  
      auto x = nd.get_x();
      auto y = nd.get_y();
      double l = 10.0;
      
      if(x*x/((600-l)*(600-l)) + (y+2230)*(y+2230)/((130-l)*(130-l)) <= 1.0) return 1173.15;
      else if(nd.flag()==1) return 898.0;
      else
      {
         double d = get_distance(600-l, 130-l, x, y, 0.0, -2230.0);         
         return 1173.15 - 275.15*d/l;
      }
    }





    double initialAngle(double a, double b, double x, double y) const  
    {
       auto abs_x = fabs(x);
       auto abs_y = fabs(y);
       bool isOutside = false;
       if (abs_x > a || abs_y > b) isOutside = true;
   
       double xd, yd;
       if (!isOutside) 
       {
           xd = sqrt((1.0 - y * y / (b * b)) * (a * a));
           if (abs_x > xd) isOutside = true;
           else 
           {
               yd = sqrt((1.0 - x * x / (a * a)) * (b * b));
               if (abs_y > yd) isOutside = true;
           }
       }
       double t;
       if (isOutside) t = atan2(a*y, b*x); //The point is outside of ellipse
       else 
       {
           //The point is inside
           if (xd < yd) 
           {
               if (x < 0) xd = -xd;
               t = atan2(y, xd);
           }
           else 
           {
               if (y < 0) yd = -yd;
               t = atan2(yd, x);
           }
       }
       return t;
    }


    
    double get_distance(double a, double b, double x, double y, double x0, double y0, double maxError = 1.e-5) const  
    {
       x -= x0;
       y -= y0;
       
       double t = initialAngle(a, b, x, y);   
       double err;
       for (int i = 0; i < 10; i++) 
       {
           auto t2 = t - ((a*a - b*b)*cos(t)*sin(t) - x*a*sin(t) + y*b*cos(t))/((a*a - b*b)*(cos(t)*cos(t) - sin(t)*sin(t)) - x*a*cos(t) - y*b*sin(t));
           err = fabs(t2 - t);
           t = t2;
           if (err < maxError) break;
       }
       auto dx = a*cos(t) - x;
       auto dy = b*sin(t) - y;
       return sqrt(dx * dx + dy * dy);
    }    
    
    
     

     
     
     



    // -------------------------  Dirichlet BC ------------------------
    auto dirichlet_p(const nd_type &nd) const
    {
      return std::make_pair(false, 0.0);
    }


    auto dirichlet_vx(const nd_type &nd) const
    {
	if( nd.flag() ==  1)	  return std::make_pair(true,0.0); 

      return std::make_pair(false,0.0); 
    }


    auto dirichlet_vy(const nd_type &nd) const
    {
	if( nd.flag() == 1)	  return std::make_pair(true,0.0); 

      return std::make_pair(false,0.0);  
    }



    auto dirichlet_T(const nd_type &nd) const
    {
      if(nd.flag()==1)
      {
          get_heat_eq_T(nd.gid());
          return std::make_pair(true, T_);
      }
    
      return std::make_pair(false,0.0);
    }





    //--------------- neumann --------------------------------
    auto neumann_tau11(const std::vector<int>& bd_nodes, int side_flag)  const
    {
//      if(side_flag == 7)      return std::make_pair(true,0.0); 

      return std::make_pair(false,0.0);
    }


    auto neumann_tau12(const std::vector<int>& bd_nodes, int side_flag)  const
    {
      return std::make_pair(false,0.0);
    }


    auto neumann_tau22(const std::vector<int>& bd_nodes, int side_flag)  const
    {
      return std::make_pair(false,0.0);
    }


    auto neumann_q1(const std::vector<int>& bd_nodes, int side_flag)  const
    {
//      if(side_flag == 1)
//      {
//         get_h_q(bd_nodes);
//         return std::make_pair(true,h_q1_);
//      }      
      
      return std::make_pair(false,0.0);
    }


    auto neumann_q2(const std::vector<int>& bd_nodes, int side_flag)  const
    {
//      if(side_flag == 1)      return std::make_pair(true, h_q2_); 
  
      return std::make_pair(false,0.0);
    }



    void set_heat_eq_T(const std::map<int, double>& heat_eq_nd_T)
    {
      heat_eq_nd_T_ = std::move(heat_eq_nd_T);
    }



    void get_heat_eq_T(int f_nd_gid) const
    {
      int heat_eq_nd_gid = matching_nd_gid_.find(f_nd_gid)->second;
      T_ = heat_eq_nd_T_.find(heat_eq_nd_gid)->second;
    } 




  private:  
    std::map<int,int> matching_nd_gid_;
    std::map<int, double> heat_eq_nd_T_;
    mutable double T_;
    

  };


}

#endif
