#ifndef HEAT_EQ_IC_BC_HPP
#define HEAT_EQ_IC_BC_HPP



#include "../../../src/fem/fem.hpp"



namespace GALES{




  template<int dim>
  class heat_eq_ic_bc : public base_ic_bc<dim>  
  {
    using nd_type = node<dim>;
    using vec = boost::numeric::ublas::vector<double>;

    public : 
    


    heat_eq_ic_bc()
    {
      matching_nd_gid_ = read_matching_nd_gid("input/fhi_nd.txt");  
    }



 
 
 
    //---------------------   IC  ------------------------------------------
    double initial_T(const nd_type &nd)const 
    {
      auto x = nd.get_x();
      auto y = nd.get_y();
      double l = 10.0;
      
      if(x*x/((600+l)*(600+l)) + (y+2230)*(y+2230)/((130+l)*(130+l)) >= 1.0) return 623.15;
      else if(nd.flag()==1) return 898.0;
      else
      {
         double d = get_distance(600, 130, x, y, 0.0, -2230.0);
         return 898.0 - 275.0*d/l;
      }
    }






    double initialAngle(double a, double b, double x, double y) const  
    {
       auto abs_x = fabs(x);
       auto abs_y = fabs(y);
       bool isOutside = false;
       if (abs_x > a || abs_y > b) isOutside = true;
   
       double xd, yd;
       if (!isOutside) 
       {
           xd = sqrt((1.0 - y * y / (b * b)) * (a * a));
           if (abs_x > xd) isOutside = true;
           else 
           {
               yd = sqrt((1.0 - x * x / (a * a)) * (b * b));
               if (abs_y > yd) isOutside = true;
           }
       }
       double t;
       if (isOutside) t = atan2(a*y, b*x); //The point is outside of ellipse
       else 
       {
           //The point is inside
           if (xd < yd) 
           {
               if (x < 0) xd = -xd;
               t = atan2(y, xd);
           }
           else 
           {
               if (y < 0) yd = -yd;
               t = atan2(yd, x);
           }
       }
       return t;
    }


    
    double get_distance(double a, double b, double x, double y, double x0, double y0, double maxError = 1.e-5) const  
    {
       x -= x0;
       y -= y0;
       
       double t = initialAngle(a, b, x, y);   
       double err;
       for (int i = 0; i < 10; i++) 
       {
           auto t2 = t - ((a*a - b*b)*cos(t)*sin(t) - x*a*sin(t) + y*b*cos(t))/((a*a - b*b)*(cos(t)*cos(t) - sin(t)*sin(t)) - x*a*cos(t) - y*b*sin(t));
           err = fabs(t2 - t);
           t = t2;
           if (err < maxError) break;
       }
       auto dx = a*cos(t) - x;
       auto dy = b*sin(t) - y;
       return sqrt(dx * dx + dy * dy);
    }    
    



 
     //------------set dirichlet bc------------------------------------      
    auto dirichlet_T(const nd_type &nd) const 
    {
      if(nd.flag() == 2)      return std::make_pair(true, 623.15);


      return std::make_pair(false,0.0);
    }




    //--------------set Neummann bc-------------------------------------
    auto neumann_q1(const std::vector<int>& bd_nodes, int side_flag) const 
    { 
      return std::make_pair(false,0.0);
    }

    auto neumann_q2(const std::vector<int>& bd_nodes, int side_flag) const 
    {
      return std::make_pair(false,0.0); 
    }





    void set_fluid_heat_flux(const std::map<int, std::vector<double> >& f_nd_q)
    {
      f_nd_q_ = std::move(f_nd_q);                  
    }



    void get_fluid_heat_flux(const std::vector<int>& h_bd_nodes, std::vector<std::vector<double>>& f_heat_flux)const
    {
      for(auto i : h_bd_nodes)
        for(auto a : matching_nd_gid_)
      	  if(a.second == i)
             f_heat_flux.push_back(f_nd_q_.find(a.first)->second);
    }
  


    private:
    std::map<int, int> matching_nd_gid_;    
    std::map<int, std::vector<double> > f_nd_q_;


  };


}


#endif
