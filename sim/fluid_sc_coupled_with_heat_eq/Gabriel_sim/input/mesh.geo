//Preliminar simulation domain (1200x260 m at 2100 m depth for the body top)


Mesh.MshFileVersion = 2;

lc=2;


Point(0) = {0,-2230,0,lc};

Point(1) = {0,-2100,0,lc};
Point(2) = {600,-2230,0,lc};
Point(3) = {0,-2360,0,lc};
Point(4) = {-600,-2230,0,lc};


Ellipse(1) = {1, 0, 3, 4};
Ellipse(2) = {4, 0, 2, 3};
Ellipse(3) = {3, 0, 1, 2};
Ellipse(4) = {2, 0, 4, 1};






//+ magma
Line Loop(1) = {1, 2, 3, 4};
Plane Surface(1) = {1};
Physical Line(1) = {1, 4, 3, 2};
Physical Surface(0) = {1};






/*
//+ heat eq
Point(5) = {0,-2000,0,lc};
Point(6) = {700,-2230,0,lc};
Point(7) = {0,-2460,0,lc};
Point(8) = {-700,-2230,0,lc};


Ellipse(5) = {5, 0, 7, 8};
Ellipse(6) = {8, 0, 6, 7};
Ellipse(7) = {7, 0, 5, 6};
Ellipse(8) = {6, 0, 8, 5};


Line Loop(1) = {5, 6, 7, 8};
Line Loop(2) = {1, 2, 3, 4};
Plane Surface(1) = {1, 2};
Physical Line(1) = {1, 4, 3, 2};
Physical Line(2) = {5, 8, 7, 6};
Physical Surface(0) = {1};
*/


Field[1] = Distance;
Field[1].CurvesList = {1, 2, 3, 4};
Field[1].Sampling = 100;



Field[2] = Threshold;
Field[2].InField = 1;
Field[2].SizeMin = lc/5;
Field[2].SizeMax = lc;
Field[2].DistMin = 10;
Field[2].DistMax = 20;
Background Field = 2;



Mesh.MeshSizeExtendFromBoundary = 0;
Mesh.MeshSizeFromPoints = 0;
Mesh.MeshSizeFromCurvature = 0;

