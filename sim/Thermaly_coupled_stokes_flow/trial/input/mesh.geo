lc=0.02;
lc1=0.01;

Point(1) = {0,0.0,0,lc1};
Point(2) = { 1,0.0,0,lc1};
Point(3) = { 1,1,0,lc};
Point(4) = {0,1,0,lc};

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};


Transfinite Line {1,3} = 51 Using Progression 1;
Transfinite Line {2,4} = 51 Using Progression 1;

Line Loop(6) = {1, 2, 3, 4};
Plane Surface(7) = {6};
Transfinite Surface {7};
Recombine Surface {7};

Physical Line(2) = {3};
Physical Line(3) = {2,4};
Physical Line(4) = {1};
Physical Surface(0) = {7};


