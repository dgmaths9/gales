
l = 1;
h = 1;

Point(1) = {0,0,0};
Point(2) = {l,0,0};
Point(3) = {l,h,0};
Point(4) = {0,h,0};

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};


Transfinite Line {1,3} = 51 Using Progression 1;
Transfinite Line {2,4} = 51 Using Progression 1;

Line Loop(6) = {1, 2, 3, 4};
Plane Surface(7) = {6};
Transfinite Surface {7};
Recombine Surface {7};

Physical Line(3) = {1};
Physical Line(2) = {2,4};
Physical Line(4) = {3};
Physical Point(5) = {1,2};
Physical Point(6) = {3,4};
Physical Surface(5) = {7};


