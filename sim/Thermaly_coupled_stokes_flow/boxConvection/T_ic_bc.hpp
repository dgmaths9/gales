#ifndef T_IC_BC_HPP
#define T_IC_BC_HPP



#include "../../../src/fem/fem.hpp"



namespace GALES{




  template<int dim>
  class T_ic_bc : public base_ic_bc<dim>  
  {
    using nd_type = node<dim>;
    using vec = boost::numeric::ublas::vector<double>;

    public : 
    



    T_ic_bc()
    {    
      this->T_ref_ = 1.0;    
    }

 
 
 
 
    //---------------------   IC  ------------------------------------------
    double initial_T(const nd_type &nd)const 
    {
          return 0.0;
//        auto x = nd.get_x();
//        auto y = nd.get_y();
//        return (1.0-y) - 0.01*cos(3.14159*x)*sin(3.14159*y);
    }


 
     //------------set dirichlet bc------------------------------------      
    auto dirichlet_T(const nd_type &nd) const 
    {
      if(nd.flag() == 3)      return std::make_pair(true, 1.0);
      if(nd.flag() == 5)      return std::make_pair(true, 1.0);
      if(nd.flag() == 4)      return std::make_pair(true, 0.0);
      if(nd.flag() == 6)      return std::make_pair(true, 0.0);

      return std::make_pair(false,0.0);
    }


    //--------------set Neummann bc-------------------------------------
    auto neumann_q1(const std::vector<int>& bd_nodes, int side_flag) const 
    { 
      if(side_flag == 2)  std::make_pair(true,0.0);
      
      return std::make_pair(false,0.0);
    }

    auto neumann_q2(const std::vector<int>& bd_nodes, int side_flag) const 
    {       
      return std::make_pair(false,0.0); 
    }

  };


}


#endif
