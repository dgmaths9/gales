#ifndef ELASTOSTATIC_IC_BC_HPP
#define ELASTOSTATIC_IC_BC_HPP



#include "../../../../src/fem/fem.hpp"



namespace GALES{


  template<int dim>
  class elastostatic_ic_bc : public base_ic_bc<dim>
  {
    using nd_type = node<dim>;
    using vec = boost::numeric::ublas::vector<double>;
    using model_type = model<dim>;


    public :

    elastostatic_ic_bc(model_type& f_model)
    :  
    f_model_(f_model)
    {
      matching_nd_gid_ = read_matching_nd_gid("input/fsi_nd.txt");  
    }






    //------------- IC -------------------------
    double initial_ux(const nd_type &nd)const {return 0.0;}
    double initial_uy(const nd_type &nd)const {return 0.0;}




    //----------------------Dirichlet  BC------------------------------------------------------------------------------
    auto dirichlet_ux(const nd_type &nd)const
    {
	if( nd.flag() == 5)	  return std::make_pair(true,0.0); 
 	if( nd.flag() == 6)	  return std::make_pair(true,0.0); 
        if(nd.flag() == 1) 
        {
          int e_nd_gid(nd.gid());
          get_su(e_nd_gid);          
          return std::make_pair(true, s_u_[0]);
        } 

      return std::make_pair(false,0.0);
    }



    auto dirichlet_uy(const nd_type &nd)const
    {
	if( nd.flag() == 5)	  return std::make_pair(true,0.0); 
 	if( nd.flag() == 6)	  return std::make_pair(true,0.0); 
        if( nd.flag() == 1)       return std::make_pair(true, s_u_[1]);

      return std::make_pair(false,0.0); 
    }





    void set_solid_u(const std::map<int, std::vector<double> >& s_gid_u)
    {
      s_gid_u_ = std::move(s_gid_u);
    }



    void get_su(int e_nd_gid)const
    {
      int s_nd_gid = matching_nd_gid_.find(e_nd_gid)->second;
      s_u_ = s_gid_u_.find(s_nd_gid)->second;
    } 



  private: 
  model_type& f_model_;
  std::map<int,int> matching_nd_gid_;
  std::map<int, std::vector<double> > s_gid_u_;
  mutable std::vector<double> s_u_;

  };


}

#endif

