Mesh.MshFileVersion=2;

lc1 = 0.03;
lc2 = 0.15;

Point(0) = {0,0,0, lc2};
Point(1) = {10,0,0, lc2};
Point(2) = {10,10,0, lc2};
Point(3) = {0,10,0, lc2};
Point(4) = {4.9,0,0, lc1};
Point(5) = {5.1,0,0, lc1};


Point(6) = {5,5,0, lc1};
Point(7) = {5,6,0, lc1};
Point(8) = {4,5,0, lc1};
Point(9) = {6,5,0, lc1};
Point(10) = {5,4,0, lc1};

Point(11) = {4.9,4,0, lc1};
Point(12) = {5.1,4,0, lc1};
//+
Line(1) = {3, 0};
//+
Line(2) = {0, 4};
//+
Line(4) = {5, 1};
//+
Line(5) = {1, 2};
//+
Line(6) = {2, 3};
//+
Ellipse(7) = {7, 6, 10, 8};
//+
Ellipse(8) = {8, 6, 9, 11};
//+
Ellipse(9) = {7, 6, 10, 9};
//+
Ellipse(10) = {9, 6, 8, 12};
//+
Line(11) = {11, 4};
//+
Line(12) = {5, 12};



/*
// fluid
Line(3) = {4, 5};
Curve Loop(1) = {12, -10, -9, 7, 8, 11, 3};
Plane Surface(1) = {1};
Physical Surface(0) = {1};
Physical Curve(6) = {3};
Physical Curve(1) = {11, 8, 7, 9, 10, 12};
Physical Point(6) = {4, 5};
*/



//+ solid
Curve Loop(2) = {2, -11, -8, -7, 9, 10, -12, 4, 5, 6, 1};
Plane Surface(2) = {2};
Physical Surface(0) = {2};
Physical Curve(1) = {11, 8, 7, 9, 10, 12};
Physical Curve(5) = {2, 4, 1, 5};
Physical Curve(2) = {6};
Physical Point(5) = {3, 0, 1, 2, 4, 5};



