#ifndef FLUID_IC_BC_HPP
#define FLUID_IC_BC_HPP



#include "../../../../src/fem/fem.hpp"





namespace GALES{



  template<int dim>
  class fluid_ic_bc : public base_ic_bc<dim> 
  {
    using nd_type = node<dim>;
    using vec = boost::numeric::ublas::vector<double>;

    public :

    fluid_ic_bc()
    {

      matching_nd_gid_ = read_matching_nd_gid("input/fsi_nd.txt");  
    }




    //-------------------- IC ----------------------------------------
    double initial_p(const nd_type &nd) const { return 1.e5; }
    double initial_vx(const nd_type &nd) const { return 0.0; }
    double initial_vy(const nd_type &nd) const { return 0.0; }
   





    // -------------------------  Dirichlet BC ------------------------
    auto dirichlet_p(const nd_type &nd) const
    {
      return std::make_pair(false, 0.0);
    }


    auto dirichlet_vx(const nd_type &nd) const
    {
 	if( nd.flag() == 5)      return std::make_pair(true,0.0);
 	if( nd.flag() == 6)      return std::make_pair(true,0.0);
        if( nd.flag() == 1) 
        {
	    int f_nd_gid(nd.gid());
	    get_sv(f_nd_gid);	    	    
	    return std::make_pair(true, s_v_[0]);
        } 

      return std::make_pair(false,0.0); 
    }


    auto dirichlet_vy(const nd_type &nd) const
    {
 	if( nd.flag() == 5)       return std::make_pair(true,0.0);
 	if( nd.flag() == 6)	   return std::make_pair(true, sin(5*time::get().t()) ); 
        if(nd.flag() == 1)        return std::make_pair(true, s_v_[1]);
        
      return std::make_pair(false,0.0);  
    }









    //--------------- neumann --------------------------------
    auto neumann_tau11(const std::vector<int>& bd_nodes, int side_flag)  const
    {
      return std::make_pair(false,0.0);
    }


    auto neumann_tau12(const std::vector<int>& bd_nodes, int side_flag)  const
    {
      return std::make_pair(false,0.0);
    }


    auto neumann_tau22(const std::vector<int>& bd_nodes, int side_flag)  const
    {
      return std::make_pair(false,0.0);
    }



    void set_solid_v(const std::map<int, std::vector<double> >& s_gid_v)
    {
      s_gid_v_ = std::move(s_gid_v);
    }



    void get_sv(int f_nd_gid) const
    {
      int s_nd_gid = matching_nd_gid_.find(f_nd_gid)->second;
      s_v_ = s_gid_v_.find(s_nd_gid)->second;
    } 





  public:
    model<dim>& model_;
    bool central_pressure_profile_;
    bool automatic_correction_;
    bool x_dependent_pressure_profile_;
    double max_, min_;
    double p0_;
    double p_ref_, v1_ref_, v2_ref_, v3_ref_, T_ref_;    
    int total_steps_;
    double h_;

    
    private:
    std::map<int,int> matching_nd_gid_;
    std::map<int, std::vector<double> > s_gid_v_;
    mutable std::vector<double> s_v_;


  };


}

#endif
