#ifndef FLUID_IC_BC_HPP
#define FLUID_IC_BC_HPP



#include "../../../../src/fem/fem.hpp"





namespace GALES{



  template<int dim>
  class fluid_ic_bc : public base_ic_bc<dim>  
  {
    using nd_type = node<dim>;
    using point_type = point<dim>;
    using vec = boost::numeric::ublas::vector<double>;

    public :

    fluid_ic_bc()
    {
      pl_ = 156.18e3; 
      Tl_ = 328.85;  
      vl_ = 112.61;
      
      pr_ =100.e3;   
      Tr_ = 290.36;  
      vr_ = 0.0;
     
      p_ref_ = pl_;
      v1_ref_ = vl_;
      T_ref_ = Tl_;   
      
      matching_nd_gid_ = read_matching_nd_gid("input/fsi_nd.txt");  
    }






    //-------------------- IC ----------------------------------------
    double initial_p(const nd_type &nd) const 
    { 
      double x = nd.get_x();
      if(x<=0.130) return pl_;     
      else return pr_; 
    }

    double initial_vx(const nd_type &nd) const
    { 
      double x = nd.get_x();
      if(x<=0.130) return vl_;     
      else return vr_; 
    }

    double initial_vy(const nd_type &nd) const { return 0.0; }

    double initial_T(const nd_type &nd) const
    { 
      double x = nd.get_x();
      if(x<=0.130) return Tl_;     
      else return Tr_; 
    }
    





    // -------------------------  Dirichlet BC ------------------------
    auto dirichlet_p(const nd_type &nd) const
    {
      if(nd.flag() == 6)        return std::make_pair(true, pl_);

      return std::make_pair(false, 0.0);
    }


    auto dirichlet_vx(const nd_type &nd) const
    {
        if(nd.flag() == 6)          return std::make_pair(true, vl_);
	if(nd.flag() == 4)	    return std::make_pair(true,0.0); 
	if(nd.flag() == 5)     	    return std::make_pair(true,0.0); 
        if( nd.flag() == 1) 
        {
	    int f_nd_gid(nd.gid());
	    get_sv(f_nd_gid);	    	    
	    return std::make_pair(true, s_v_[0]);
        } 

      return std::make_pair(false,0.0); 
    }


    auto dirichlet_vy(const nd_type &nd) const
    {
        if(nd.flag() == 6)          return std::make_pair(true, 0.0);
 	if(nd.flag() == 3)  	    return std::make_pair(true,0.0); 
	if(nd.flag() == 5) 	    return std::make_pair(true,0.0); 
        if(nd.flag() == 1)          return std::make_pair(true, s_v_[1]);
        
      return std::make_pair(false,0.0);  
    }


    auto dirichlet_T(const nd_type &nd) const
    {
        if(nd.flag() == 6)          return std::make_pair(true, Tl_);

      return std::make_pair(false,0.0);
    }






    //--------------- neumann --------------------------------
    auto neumann_tau11(const std::vector<int>& bd_nodes, int side_flag)  const
    {
      return std::make_pair(false,0.0);
    }


    auto neumann_tau12(const std::vector<int>& bd_nodes, int side_flag)  const
    {
      if(side_flag == 3)      return std::make_pair(true,0.0); 
      if(side_flag == 4)      return std::make_pair(true,0.0); 

      return std::make_pair(false,0.0);
    }



    auto neumann_tau22(const std::vector<int>& bd_nodes, int side_flag)  const
    {
      return std::make_pair(false,0.0);
    }



    auto neumann_q1(const std::vector<int>& bd_nodes, int side_flag)  const
    {
      if(side_flag == 3)      return std::make_pair(true,0.0); 

      return std::make_pair(false,0.0);
    }

    auto neumann_q2(const std::vector<int>& bd_nodes, int side_flag)  const
    {
      if(side_flag == 4)      return std::make_pair(true,0.0); 

      return std::make_pair(false,0.0);
    }



    void set_solid_v(const std::map<int, std::vector<double> >& s_gid_v)
    {
      s_gid_v_ = std::move(s_gid_v);
    }



    void get_sv(int f_nd_gid) const
    {
      int s_nd_gid = matching_nd_gid_.find(f_nd_gid)->second;
      s_v_ = s_gid_v_.find(s_nd_gid)->second;
    } 



    
    private:
    double pl_, vl_, Tl_;
    double pr_, vr_, Tr_;
    std::map<int,int> matching_nd_gid_;
    std::map<int, std::vector<double> > s_gid_v_;
    mutable std::vector<double> s_v_;


  };


}

#endif
