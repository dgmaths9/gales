#ifndef SOLID_IC_BC_HPP
#define SOLID_IC_BC_HPP



#include "../../../../src/fem/fem.hpp"



namespace GALES{



  template<int dim>
  class solid_ic_bc : public base_ic_bc<dim>  
  {
    using nd_type = node<dim>;
    using vec = boost::numeric::ublas::vector<double>;

  public : 
  
    solid_ic_bc()
    {
      matching_nd_gid_ = read_matching_nd_gid("input/fsi_nd.txt");  
    }


 
 
 
    //---------------------- Set IC------------------------------------------
    double initial_ux (const nd_type &nd)const {return 0.0;}
    double initial_uy (const nd_type &nd)const {return 0.0;}

    double initial_vx (const nd_type &nd)const {return 0.0;}
    double initial_vy (const nd_type &nd)const {return 0.0;}



 
     //------------set dirichlet bc------------------------------------      
    auto dirichlet_ux(const nd_type &nd) const 
    {
      if(nd.flag() == 5)      return std::make_pair(true,0.0);
       
      return std::make_pair(false,0.0);
    }

    auto dirichlet_uy(const nd_type &nd)const 
    {
      if(nd.flag() == 5)      return std::make_pair(true,0.0);

	return std::make_pair(false,0.0);
    }
 




    //--------------set Neummann bc-------------------------------------
    auto neumann_sigma11( const std::vector<int>& bd_nodes, int side_flag)  const 
    { 
      return std::make_pair(false,0.0);
    }

    auto neumann_sigma22( const std::vector<int>& bd_nodes, int side_flag)  const 
    {
      return std::make_pair(false,0.0); 
    }

    auto neumann_sigma12( const std::vector<int>& bd_nodes, int side_flag)  const 
    {    
     return std::make_pair(false,0.0);
    }

  
  


  

    void set_fluid_tr(const std::map<int, std::vector<double> >& f_nd_tr)
    {
      f_nd_tr_ = std::move(f_nd_tr);                  
    }



    void get_fluid_tr(const std::vector<int>& s_bd_nodes, std::vector<std::vector<double>>& f_tr) const
    {
      for(auto i : s_bd_nodes)
        for(auto a : matching_nd_gid_)
      	  if(a.second == i)
             f_tr.push_back(f_nd_tr_.find(a.first)->second);
    }
  


    private:
    std::map<int, int> matching_nd_gid_;    
    std::map<int, std::vector<double> > f_nd_tr_;
  };



}

#endif
