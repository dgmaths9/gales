Mesh.MshFileVersion=2;

lc1 = 0.003;
lc2 = 0.0009;
lc4 = 0.0006;


s = 0.01;

Point(1) = {-4.5*s,-4.5*s, 0, lc1};
Point(2) = {-4.5*s, 4.5*s, 0, lc1};
Point(3) = {15.5*s,-4.5*s, 0, lc1};
Point(4) = {15.5*s, 4.5*s, 0, lc1};


Point(5) = {0,-0.5*s,0,lc2};
Point(6) = {1*s,-0.5*s,0,lc2};
Point(7) = {1*s,0.5*s,0,lc2};
Point(8) = {0,0.5*s,0,lc2};

Point(9) = {1*s,-0.03*s,0,lc4};
Point(10) = {1*s,0.03*s,0,lc4};
Point(11) = {5*s,-0.03*s,0,lc4};
Point(12) = {5*s,0.03*s,0,lc4};


Line(1) = {2, 1};
Line(2) = {1, 3};
Line(3) = {3, 4};
Line(4) = {4, 2};
Line(5) = {8, 5};
Line(6) = {5, 6};
Line(7) = {6, 9};
Line(8) = {9, 11};
Line(9) = {11, 12};
Line(10) = {12, 10};
Line(11) = {10, 7};
Line(12) = {7, 8};
Line(13) = {10, 9};

Line Loop(14) = {4, 1, 2, 3};
Line Loop(15) = {12, 5, 6, 7, 8, 9, 10, 11};
Plane Surface(16) = {14, 15};
Line Loop(17) = {10, 13, 8, 9};
Plane Surface(18) = {17};



//structure
Physical Surface(0) = {18};
Physical Line(1) = {10, 8, 9};
Physical Line(5) = {13};
Physical Point(5) = {10, 9};
Mesh 2;
Save "s.msh";


Delete Physicals;


//fluid
Physical Surface(0) = {16};
Physical Line(6) = {1};
Physical Line(7) = {3};
Physical Line(4) = {4, 2};
Physical Line(5) = {12, 11, 7, 6, 5};
Physical Line(1) = {8, 9, 10};
Physical Point(2) = {2, 1};
Physical Point(3) = {3, 4};
Physical Point(5) = {8, 7, 10, 9, 6, 5};
Physical Point(1) = {11, 12};
Mesh 2;
Save "f.msh";






