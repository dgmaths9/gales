Mesh.MshFileVersion = 2;
SetFactory("OpenCASCADE");
Box(1) = {2, 0, 1, 0.05, 1, 1}; // solid
Box(2) = {0, 0, 0, 10, 2, 3};   // fluid
Coherence;                      // fragment everything to make the CAD conformal

Characteristic Length {3, 7, 8, 4, 2, 6, 1, 5} = 0.08;   //fsi (solid)
Characteristic Length {12, 10, 9, 11} = 0.1;             //inlet
Characteristic Length {16, 13, 14, 15} = 0.2;            //outlet



Mesh 3;
Physical Volume(0) = {1};
Physical Surface(1) = {1, 2, 6, 4, 5};
Physical Surface(5) = {3};
Physical Curve(5) = {9, 1, 5, 10};
Physical Point(5) = {2, 6, 1, 5};
Save "s.msh";



Delete Physicals;



BooleanDifference{ Volume{2}; Delete; }{ Volume{1}; Delete; }
Physical Volume(0) = {2};
Physical Surface(1) = {5, 1, 2, 4, 6};
Physical Curve(5) = {1, 10, 5, 9};
Physical Point(5) = {2, 6, 1, 5};
Physical Surface(6) = {7};
Physical Surface(7) = {12};
Physical Surface(2) = {8, 10};
Physical Surface(3) = {11, 9};
Physical Curve(6) = {16, 13, 14, 15};
Physical Curve(8) = {20, 22, 17, 19};
Physical Curve(9) = {21, 24};
Physical Curve(10) = {23, 18};
Physical Point(6) = {10, 9, 11, 12};
Physical Point(11) = {16, 15, 14, 13};
Save "f.msh";









