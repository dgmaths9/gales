Mesh.MshFileVersion=2;


h1 = 0.5;
h2 = 0.01;



inlet_interface_d = 0.2;



Point(1) = {0,-2104,0, h2};

Point(2) = {25,-2104,0, h1};
Point(3) = {-25,-2104,0, h1};
Point(4) = {0,-2129,0, h1};

Point(5) = {0.205,-2104,0,h2};
Point(6) = {-0.205,-2104,0,h2};

Point(7) = {-0.205,-2079,0, h2};
Point(8) = {0.205,-2079,0, h2};

Point(9) = {-0.06,-2104+inlet_interface_d,0,h2};
Point(10) = {0.06,-2104+inlet_interface_d,0,h2};

Point(11) = {-0.06,-2079,0, h2};
Point(12) = {0.06,-2079,0, h2};





//magma chamber
Circle(1) = {3, 1, 4};
Circle(2) = {2, 1, 4};
Line(3) = {3, 6};
Line(11) = {5, 2};
Line(12) = {6, 1};
Line(13) = {1, 5};


//well
Line(4) = {6, 7};
Line(5) = {7, 11};
Line(6) = {11, 9};
Line(7) = {9, 10};
Line(8) = {10, 12};
Line(9) = {12, 8};
Line(10) = {8, 5};







//+
Curve Loop(1) = {3, 12, 13, 11, 2, -1};
//+
Plane Surface(1) = {1};
//+
Curve Loop(2) = {10, -13, -12, 4, 5, 6, 7, 8, 9};
//+
Plane Surface(2) = {2};



//+
Physical Surface(14) = {1, 2};
//+
Physical Curve(6) = {7};
//+
Physical Curve(7) = {5, 9};
//+
Physical Curve(5) = {4, 6, 8, 10, 1, 2, 3, 11};

//+
Physical Point(5) = {9, 10, 11, 12, 8, 7, 6, 5, 3, 4, 2};

