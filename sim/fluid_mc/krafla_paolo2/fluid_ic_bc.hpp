#ifndef __FLUID_IC_BC_HPP
#define __FLUID_IC_BC_HPP



#include "../../../src/fem/fem.hpp"



namespace GALES {




  template<int dim>
  class fluid_ic_bc : public base_ic_bc<dim> 
  {

    using nd_type = node<dim>;
    using vec = boost::numeric::ublas::vector<double>;


  public :

    fluid_ic_bc()
    {
        this->custom_pressure_profile_ = true;
        this->p0_ = 20.e6;
        
        this->p_ref_ = 20.e6;
        this->v1_ref_ = 0.0;
        this->v2_ref_ = 0.0;
        this->T_ref_ = 800.0;

        this->Y_min_ = 0.0;
        this->Y_max_ = 1.0 - this->Y_min_;        
    }
            
    


    void body_force(vec& gravity) const
    {
      gravity[1] = -9.81;
    }


 



   
    //-------------------- IC ----------------------------------------

    double initial_p(const nd_type &nd) const { return 0.0; }
    double initial_vx(const nd_type &nd) const { return 0.0; }
    double initial_vy(const nd_type &nd) const { return 0.0; }
    double initial_T(const nd_type &nd)const  
    { 
       double y = nd.get_y();
       if (y <= -2104.0+1.e-6)  return 1173.15; 
       else return 313.15;        
    }

    void initial_Y(const nd_type &nd, vec        &Y)const 
    {
      double y = nd.get_y();
      if (y <= -2104.0+1.e-6)  Y[0] = this->Y_max_; 
      else   Y[0] = this->Y_min_;
    }







  // --------------------Dirichlet BC------------------------------------

    auto dirichlet_p(const nd_type &nd) const 
    {
       if(nd.flag() ==  7)     return std::make_pair(true, 19842285.8); 

      return std::make_pair(false,0.0);
    }


    auto dirichlet_vx(const nd_type &nd) const 
    { 
       if(nd.flag() ==  5)     return std::make_pair(true,0.0);
       if(nd.flag() ==  6)     return std::make_pair(true,0.0);
	
      return std::make_pair(false,0.0);
    }


    auto dirichlet_vy(const nd_type &nd) const 
    {
       if(nd.flag() ==  5)     return std::make_pair(true,0.0); 
       if(nd.flag() ==  6)     return std::make_pair(true, -0.53);  //inlet trivella    //-0.53
	  
      return std::make_pair(false,0.0);
    }


    auto dirichlet_T(const nd_type &nd) const 
    {
      if(nd.flag() ==  6)     return std::make_pair(true, 313.15);

      return std::make_pair(false,0.0);
    }
 

    // comp_index is the index of component. e.g. for 3 component mixture we have first 2 components as dofs; Y[0] and Y[1]
    // 0---first component;   1----second component
    auto dirichlet_Y(const nd_type &nd, int comp_index) const
    {
       if( nd.flag() == 6 && comp_index == 0 )          return std::make_pair(true, this->Y_min_);

      return std::make_pair(false,0.0);
    }





  //-------------------------Neumann BC--------------------------------------------------

    auto neumann_tau11(const std::vector<int>& bd_nodes, int side_flag) const 
    { 
      return std::make_pair(false,0.0);  
    }

    auto neumann_tau12(const std::vector<int>& bd_nodes, int side_flag) const 
    { 
      if(side_flag == 7)     return std::make_pair(true,0.0);

      return std::make_pair(false,0.0);
    }

    auto neumann_tau22(const std::vector<int>& bd_nodes, int side_flag) const 
    { 
      if(side_flag == 7)     return std::make_pair(true,0.0);

      return std::make_pair(false,0.0); 
    }
                
    auto neumann_q1(const std::vector<int>& bd_nodes, int side_flag) const  
    {   
      if(side_flag == 5)     return std::make_pair(true,0.0);

      return std::make_pair(false,0.0); 
    }

    auto neumann_q2(const std::vector<int>& bd_nodes, int side_flag) const 
    { 
      if(side_flag == 5)     return std::make_pair(true,0.0);
      if(side_flag == 7)     return std::make_pair(true,0.0);

      return std::make_pair(false,0.0); 
    }

    auto neumann_J1(int comp_index, const std::vector<int>& bd_nodes, int side_flag) const 
    { 
      if(side_flag == 5 && comp_index==0)     return std::make_pair(true,0.0);

      return std::make_pair(false,0.0); 
    }

    auto neumann_J2(int comp_index, const std::vector<int>& bd_nodes, int side_flag) const 
    { 
      if(side_flag == 5 && comp_index==0)     return std::make_pair(true,0.0);
      if(side_flag == 7 && comp_index==0)     return std::make_pair(true,0.0);

      return std::make_pair(false,0.0); 
    }



    
    template<typename X1, typename X2>
    void set_custom_pressure_profile(X1& model, X2& props)
    {
       double max(-2079.0), min(-2129.0);
       auto h = 0.01;        
       int steps = (int)((max-min)/h);
       vec p(steps+1), y(steps+1), T(steps+1), T1(steps+1);
       std::vector<vec> A(steps+1), A1(steps+1);
       point<dim> pt;
       for(int i=0; i<=steps; i++)
       {
         y[i]= max - h*i;       //wall and string surface=-2079  h=0.005   //magma surface=-2104
         pt.set_x(0.0);          
         pt.set_y(y[i]);          
         node<dim> nd1(pt);
         T[i] = initial_T(nd1);
         A[i].resize(props.nb_comp());
         initial_Y(nd1,A[i]);
         A[i][props.nb_comp()-1] = 1.0- std::accumulate(&A[i][0], &A[i][props.nb_comp()-1],0.0);
         
         pt.set_y(y[i]- h*0.5);          
         node<dim> nd2(pt);
         T1[i] = initial_T(nd2);
         A1[i].resize(props.nb_comp());
         initial_Y(nd2,A1[i]);
         A1[i][props.nb_comp()-1] = 1.0- std::accumulate(&A1[i][0],&A1[i][props.nb_comp()-1],0.0);
       }
    
       //---------well water----------------------------------------------
       p[0] = 19842285.8;         // p = rho(972.9) * g(9.81) * z(2079)
       double m1,m2,m3,m4; 
              
       for(int i=0; i<=(steps/2); i++)
       {
         props.properties(p[i], T[i], A[i], 0.0);
         m1 = 9.81*props.rho();     
         props.properties(p[i]+0.5*h*m1, T1[i], A1[i], 0.0);
         m2 = 9.81*props.rho();     
         props.properties(p[i]+0.5*h*m2, T1[i], A1[i], 0.0);
         m3 = 9.81*props.rho();               
         props.properties(p[i]+h*m3, T[i+1], A[i+1], 0.0);
         m4 = 9.81*props.rho();               
         p[i+1] = p[i]+h/6.0*(m1+2.0*(m2+m3)+m4);     
       }
       
       //------ magma----------------------------------------------------
       p[steps/2] = 45.4e6;             // from Paolo's presentation;   at magma surface at y=-2104   
       for(int i=steps/2; i<steps; i++)
       {
         props.properties(p[i], T[i], A[i], 0.0);
         m1 = 9.81*props.rho();
         props.properties(p[i]+0.5*h*m1, T1[i], A1[i], 0.0);
         m2 = 9.81*props.rho();
         props.properties(p[i]+0.5*h*m2, T1[i], A1[i], 0.0);
         m3 = 9.81*props.rho();          
         props.properties(p[i]+h*m3, T[i+1], A[i+1], 0.0);
         m4 = 9.81*props.rho();          
         p[i+1] = p[i]+h/6.0*(m1+2.0*(m2+m3)+m4);     
       }
         
       //------ setting pressure at the nodes--------------------------
       for(const auto& nd : model.mesh().nodes())
       {
          const double y_coord(nd->get_y());
          const double p_interpolated = p_interpolation(y, p, y_coord);
          model.state().set_dof(nd->first_dof_lid(), p_interpolated);
       }
    
    
       //------Smoothing at the interface--------------------------------------------------------------------------           
       const double y_min(-2104.0), y_max(-2103.97);
       const double p_at_y_min = p_interpolation(y, p, y_min);
       const double p_at_y_max = p_interpolation(y, p, y_max);
       const double T_at_y_min = 1173.15;
       const double T_at_y_max = 313.15;
       const double Y_at_y_min = 1.0;
       const double Y_at_y_max = 0.0;
       
       
       for(const auto& nd : model.mesh().nodes())
       {
          const double y(nd->get_y());
          if(y < y_max && y >= y_min) 
          {
            auto p = p_at_y_min + (y-y_min)*(p_at_y_max-p_at_y_min)/(y_max-y_min);        
            model.state().set_dof(nd->first_dof_lid(), p);
            
            auto T = T_at_y_min + (y-y_min)*(T_at_y_max-T_at_y_min)/(y_max-y_min);
            model.state().set_dof(nd->first_dof_lid()+3, T);
    
            auto Y = Y_at_y_min + (y-y_min)*(Y_at_y_max-Y_at_y_min)/(y_max-y_min);
            model.state().set_dof(nd->first_dof_lid()+4, Y);
          }       
       }
           
    }    
    
    
    



  };
  
} //namespace
#endif



