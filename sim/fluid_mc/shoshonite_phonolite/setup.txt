fluid_mesh_file   mesh_96core.txt
dim               2
delta_t           0.01
final_time        0.05
restart           F
restart_time      0.0
n_max_it          3
print_freq        1

ls_solver         Flexible GMRES
ls_precond        ILU
ls_rel_res_tol    1.e-9
ls_maxsubspace    300
ls_maxrestarts    5
ls_maxiters      -1
ls_fill           1

adaptive_time_step    F
dt_min                0.01
dt_max                0.5
N                     10
CFL                   1

steady_state                F
tau_non_diag_comp_2001      F
tau_non_diag_comp_2019      T
tau_diag_incomp_2007        F
tau_diag_2014               F
dc_2006                     T
dc_sharp                    1.0 
dc_scale_fact               1.5

isothermal                     T    
incompressibility_correction   T
moving_mesh                    F        


