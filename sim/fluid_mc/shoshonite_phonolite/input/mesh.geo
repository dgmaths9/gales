Mesh.MshFileVersion = 2;


lc1 = 10;
lc2 = 5;
lc3 = 1;
lc4 = 1;

//upper chamber
Point(0) = {0,-3000,0,lc2};
Point(1) = {-400,-3200,0,lc2};
Point(2) = {400,-3200,0,lc2};
Point(3) = {0,-3200,0,lc3};
Point(5) = {10,-3400,0,lc4};
Point(6) = {-10,-3400,0,lc4};


//bottom chamber
Point(7) = {-10,-8000,0,lc3};
Point(8) = {10,-8000,0,lc3};
Point(10) = {4000,-8500,0,lc1};
Point(11) = {-4000,-8500,0,lc1};
Point(12) = {0,-9000,0,lc1};
Point(13) = {0,-8500,0,lc1};


Ellipse(1) = {1, 3, 2, 6};
Ellipse(2) = {1, 3, 2, 0};
Ellipse(3) = {2, 3, 1, 0};
Ellipse(4) = {2, 3, 1, 5};
Line(5) = {5, 8};
Line(6) = {7, 6};
Ellipse(7) = {11, 13, 10, 12};
Ellipse(8) = {10, 13, 11, 12};
Ellipse(9) = {10, 13, 11, 8};
Ellipse(10) = {11, 13, 10, 7};
Curve Loop(1) = -{5, -9, 8, -7, 10, 6, -1, 2, -3, 4};
Plane Surface(1) = {1};
Physical Surface(0) = {1};
Physical Curve(5) = {7, 8, 9, 10, 5, 6, 4, 3, 2, 1};
