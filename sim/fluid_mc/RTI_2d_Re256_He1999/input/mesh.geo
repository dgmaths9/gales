Mesh.MshFileVersion = 2;
lc=0.05;


Point(1) = {0,0,0,lc};
Point(2) = {1,0,0,lc};
Point(3) = {1,4,0,lc};
Point(4) = {0,4,0,lc};

//+
Line(1) = {1, 2};
//+
Line(2) = {2, 3};
//+
Line(3) = {3, 4};
//+
Line(4) = {4, 1};
//+
Line Loop(1) = {4, 1, 2, 3};
//+
Plane Surface(1) = {1};
//+
Physical Surface(0) = {1};
//+
Physical Line(5) = {1, 3};
//+
Physical Line(4) = {4, 2};
//+
Transfinite Line {4, 2} = 1025 Using Progression 1;
//+
Transfinite Line {1, 3} = 257 Using Progression 1;
//+
Transfinite Surface {1};
//+
Recombine Surface {1};
