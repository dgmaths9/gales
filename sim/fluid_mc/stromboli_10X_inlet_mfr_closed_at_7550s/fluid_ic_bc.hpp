#ifndef FLUID_IC_BC_HPP
#define FLUID_IC_BC_HPP


#include "../../../src/fem/fem.hpp"


namespace GALES{



  template<int dim>
  class fluid_ic_bc : public base_ic_bc<dim>  
  {
    using nd_type = node<dim>;
    using vec = boost::numeric::ublas::vector<double>;

    public :


    fluid_ic_bc()
    {
        this->central_pressure_profile_ = true;
        this->p0_ = 1.e5;
        this->max_ = 761.0;
        this->min_ = -3375.0;
        this->h_ = 1.0;
        this->total_steps_ = (int)((this->max_-this->min_)/this->h_);     

        this->p_ref_ = this->p0_;
        this->v1_ref_ = 0.0;
        this->v2_ref_ = 0.0;
        this->T_ref_ = 0.0;

        this->Y_min_ = 0.0;
        this->Y_max_ = 1.0 - this->Y_min_;        
    }
            
    


    void body_force(v_type& gravity) const
    {
      gravity[1] = -9.81;
    }

    
    

    //-------------------- IC ----------------------------------------
    double initial_p(const nd_type &nd) const { return 0.0; }
    double initial_vx(const nd_type &nd) const { return 0.0; }
    double initial_vy(const nd_type &nd) const { return 0.0; }
    void initial_Y (const nd_type &nd, vec &Y)const 
    {
       double x = nd.get_x();
       double y = nd.get_y();
       
       if((y<=-2938.0 && y>=-3238.0) && (x<=3.55 && x>=-3.55)) //LP
       	 Y[0] = this->Y_min_;
       else //HP
       	 Y[0] = this->Y_max_;
    }

 	




    // -------------------------  Dirichlet BC ------------------------
    auto dirichlet_p(const nd_type &nd) const
    {
      if(nd.flag()==7)       return std::make_pair(true,1.e5);

      return std::make_pair(false, 0.0);
    }


    auto dirichlet_vx(const nd_type &nd) const
    {
      if(nd.flag()==5)       return std::make_pair(true,0.0);
      if(nd.flag()==6)       return std::make_pair(true,0.0);

      return std::make_pair(false,0.0);
    }


    auto dirichlet_vy(const nd_type &nd) const
    {
      if(nd.flag()==5)       return std::make_pair(true,0.0);
      if(nd.flag()==6)       return std::make_pair(true,0.0);

      return std::make_pair(false,0.0);
    }



     // comp_index is the index of component. e.g. for 3 component mixture we have first 2 components as dofs; Y[0] and Y[1]
    // 0---first component;   1----second component
    auto dirichlet_Y(const nd_type &nd, int comp_index) const
    {      
//      if(nd.flag()==6 && comp_index==0)       return std::make_pair(true, this->Y_min_);
      //if(nd.flag()==7 && comp_index==0)       return std::make_pair(true, this->Y_max_);
       
      return std::make_pair(false,0.0);
    }






  //-------------------------Neumann BC--------------------------------------------------

    auto neumann_tau11(const std::vector<int>& bd_nodes, int side_flag) const 
    { 
      return std::make_pair(false,0.0);  
    }

    auto neumann_tau12(const std::vector<int>& bd_nodes, int side_flag) const 
    { 
      if(side_flag == 7)       return std::make_pair(true,0.0);  

      return std::make_pair(false,0.0);
    }

    auto neumann_tau22(const std::vector<int>& bd_nodes, int side_flag) const 
    { 
      if(side_flag == 7)       return std::make_pair(true,0.0);  

      return std::make_pair(false,0.0); 
    }
                
  };


}


#endif
