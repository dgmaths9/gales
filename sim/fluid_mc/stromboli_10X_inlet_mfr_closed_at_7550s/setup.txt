fluid_mesh_file   mesh_192core.txt
dim               2

delta_t           0.01
final_time        100000
restart           T
restart_time      8459.0
n_max_it          2
print_freq        100
precision         8

ls_solver         Flexible GMRES
ls_precond        ILU
ls_rel_res_tol    1.e-9
ls_maxsubspace    300
ls_maxrestarts    5
ls_fill           1

adaptive_time_step    F
dt_min                0.0001
dt_max                0.1
N                     10
CFL                   0.5

steady_state                F 
tau_non_diag_comp_2001      F
tau_non_diag_comp_2019      T
tau_diag_incomp_2007        F
tau_diag_2014               F
dc_2006                     T
dc_sharp                    2.0 
dc_scale_fact               1.0

incompressibility_correction   T
isothermal                     T
moving_mesh                    F            

pp_start_time        7500
pp_final_time        10000
pp_delta_t           50

pp_rho               T
pp_mu                T
pp_cp                T
pp_cv                T
pp_alpha             T
pp_beta              T
pp_sound_speed       T
pp_kappa             T
pp_molar_mass        T
pp_vf_g              T
