lc=0.05;


a=0.146;
Point(1) = {0,0,0,lc};
Point(2) = {4*a,0,0,lc};
Point(3) = {4*a,4*a,0,lc};
Point(4) = {0,4*a,0,lc};

//+
Line(1) = {1, 2};
//+
Line(2) = {2, 3};
//+
Line(3) = {3, 4};
//+
Line(4) = {4, 1};
//+
Line Loop(1) = {4, 1, 2, 3};
//+
Plane Surface(1) = {1};
//+
Physical Surface(0) = {1};

//+ bootom
Physical Line(2) = {1};

//+ left and right
Physical Line(3) = {4, 2};

//+ top 
Physical Line(7) = {3};

//+
Physical Point(5) = {1,2};

//+
Physical Point(7) = {3,4};



Transfinite Line {4, 2} = 241 Using Progression 1;
//+
Transfinite Line {1, 3} = 241 Using Progression 1;
//+
Transfinite Surface {1};
//+
Recombine Surface {1};
