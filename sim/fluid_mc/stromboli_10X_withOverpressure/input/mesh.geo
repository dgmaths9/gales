lc=1;
lc1=2;
lc4=20;



// crater top
Point(2) = {-5,761,0,lc};      
Point(3) = {5,761,0,lc};       


// chamber
Point(6) = {-5,-2250,0,lc};
Point(7) = {5,-2250,0,lc};
Point(8) = {-500,-2350,0,lc4};
Point(16) = {0,-2350,lc1};
Point(9) = {500,-2350,0,lc4};
Point(10) = {-5,-2450,0,lc};
Point(11) = {5,-2450,0,lc};


// bottom of dike
Point(14) = {-5,-3375,0,lc};
Point(15) = {5,-3375,0,lc};






//+
Ellipse(8) = {8, 16, 9, 6};
//+
Ellipse(9) = {8, 16, 9, 10};
//+
Ellipse(10) = {9, 16, 8, 7};
//+
Ellipse(11) = {9, 16, 8, 11};





//+
Line(12) = {6, 2};
//+
Line(13) = {2, 3};
//+
Line(14) = {3, 7};
//+
Line(15) = {10, 14};
//+
Line(16) = {11, 15};
//+
Line(17) = {15, 14};
//+
Line Loop(1) = {12, 13, 14, -10, 11, 16, 17, -15, -9, 8};
//+
Plane Surface(1) = {1};
Recombine Surface{1};
//+
Physical Surface(0) = {1};
//+
Physical Line(7) = {13};
Mesh.Algorithm = 8;

