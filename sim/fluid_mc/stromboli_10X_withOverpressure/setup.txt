fluid_mesh_file   mesh_192core.txt
dim               2
delta_t           0.01
final_time        1000000.0
restart           T
restart_time      13000.0
n_max_it          3
print_freq        100
precision         8

ls_solver         Flexible GMRES
ls_precond        ILU
ls_rel_res_tol    1.e-9
ls_maxsubspace    200
ls_maxrestarts    5
ls_fill           1

adaptive_time_step    F
dt_min                0.0001
dt_max                0.1
N                     10
CFL                   0.5

steady_state                F 
tau_non_diag_comp_2001      F
tau_non_diag_comp_2019      T
tau_diag_incomp_2007        F
tau_diag_2014               F
dc_2006                     T
dc_sharp                    1.0 
dc_scale_fact               0.1

isothermal                     T    
incompressibility_correction   T
moving_mesh                    F        

incompressibility_correction   T
isothermal                     T
moving_mesh                    F            

pp_start_time        0
pp_final_time        10000
pp_delta_t           500

pp_rho               T
pp_mu                T
pp_cp                T
pp_cv                T
pp_alpha             T
pp_beta              T
pp_sound_speed       T
pp_kappa             T
pp_molar_mass        T
pp_vf_g              T
