fluid
{
     Isothermal_T    1400.0  

     mix     
     {
        magma_mix      
        {
            name                  HP_stromboli
            oxide_wf              0.5273, 0.016, 0.156, 0.02, 0.0865, 0.002, 0.0376, 0.0768, 0.0355, 0.0422                       
            
            h2o_wf                0.00001
            co2_wf                0.000001
       
            crystal_fraction      0.5
            crystal_rho           3000.0
            gas_on_mu_model       Ishii_Zuber
            crystal_on_mu_model   Caricchi
        }
        magma_mix 
        {
            name                  LP_stromboli
            oxide_wf              0.5030, 0.0095, 0.1831, 0.0279, 0.0560, 0.0016, 0.0517, 0.1269, 0.0242, 0.0171                      

            h2o_wf                0.035
            co2_wf                0.035

            crystal_fraction      0.0
            crystal_rho           0.0
            gas_on_mu_model       Ishii_Zuber
            crystal_on_mu_model   none
        }
     }            
}



