lc=0.4;
lc1=1;
lc4=20;



// crater top
Point(2) = {-5,761,0,lc};      
Point(3) = {5,761,0,lc};       

Point(4) = {-5,650,0,lc};      
Point(5) = {5,650,0,lc};       

// chamber
Point(6) = {-5,-2250,0,lc1};
Point(7) = {5,-2250,0,lc1};
Point(8) = {-500,-2350,0,lc4};
Point(16) = {0,-2350,lc1};
Point(9) = {500,-2350,0,lc4};
Point(10) = {-5,-2450,0,lc1};
Point(11) = {5,-2450,0,lc1};


// bottom of dike
Point(14) = {-5,-3375,0,lc};
Point(15) = {5,-3375,0,lc};


//Mesh.Algorithm = 8;


//+
Line(1) = {14, 10};
//+
Ellipse(2) = {8, 16, 9, 10};
//+
Ellipse(3) = {8, 16, 9, 6};
//+
Line(4) = {6, 4};
//+
Line(5) = {4, 2};
//+
Line(6) = {3, 5};
//+
Line(7) = {5, 7};
//+
Ellipse(8) = {9, 16, 8, 7};
//+
Ellipse(9) = {9, 16, 8, 11};
//+
Line(10) = {11, 15};
//+
Line(11) = {15, 14};
//+
Line(12) = {2, 3};
//+
Line Loop(1) = {5, 12, 6, 7, -8, 9, 10, 11, 1, -2, 3, 4};
//+
Plane Surface(1) = {1};


Physical Surface(0) = {1};


Physical Line(6) = {11};
Physical Line(7) = {12};
Physical Line(5) = {5, 6, 7, 4, 8, 9, 2, 3, 10, 1};

