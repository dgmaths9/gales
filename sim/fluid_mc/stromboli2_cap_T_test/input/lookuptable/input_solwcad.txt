magma_1         HP_stromboli
oxides_wf_1     0.5273, 0.016, 0.156, 0.02, 0.0865, 0.002, 0.0376, 0.0768, 0.0355, 0.0422
h2o_1           0.001
co2_1           0.001

magma_2         LP_stromboli
oxides_wf_2     0.5030, 0.0095, 0.1831, 0.0279, 0.0560, 0.0016, 0.0517, 0.1269, 0.0242, 0.0171
h2o_2           0.035
co2_2           0.035

initial_T     850
final_T       2000 
delta_T       50 

fixed_T       1400

initial_p     50000
final_p       300.e6
delta_p       5.e5

