Mesh.MshFileVersion = 2;



lc=0.1;
Point(1) = {0,0,0,lc};
Point(2) = {1,0,0,lc};
Point(3) = {1,4,0,lc};
Point(4) = {0,4,0,lc};

//+
Line(1) = {1, 2};
//+
Line(2) = {2, 3};
//+
Line(3) = {3, 4};
//+
Line(4) = {4, 1};
//+
Line Loop(1) = {4, 1, 2, 3};
//+
Plane Surface(1) = {1};
//+
Transfinite Line {4, 2} = 257 Using Progression 1;
//+
Transfinite Line {1, 3} = 65 Using Progression 1;
//+
Transfinite Surface {1};
//+
Recombine Surface {1};
//+
Extrude {0, 0, 1} {
  Surface{1}; Layers{64}; Recombine;
}
//+
Physical Volume(1) = {1};
//+
Physical Surface(2) = {13, 21};
//+
Physical Surface(3) = {26, 1};
//+
Physical Surface(5) = {17, 25};
//+
Physical Line(4) = {4, 6, 2, 8};
//+
Physical Line(5) = {11, 3, 20, 9, 1, 16, 7, 12};
//+
Physical Point(5) = {4, 5, 14, 3, 2, 10, 6, 1};












/*
//+
SetFactory("OpenCASCADE");
Box(1) = {0, 0, 0, 1, 4, 1};
//+
Characteristic Length {4, 3, 7, 8, 2, 1, 5, 6} = 0.02;

//+
Physical Volume(1) = {1};
//+
Physical Surface(2) = {1, 2};
//+
Physical Surface(3) = {6, 5};
//+
Physical Surface(5) = {4, 3};
//+
Physical Line(4) = {4, 2, 6, 8};
//+
Physical Line(5) = {12, 3, 11, 7, 9, 5, 10, 1};
//+
Physical Point(5) = {4, 3, 8, 7, 2, 1, 5, 6};
*/

