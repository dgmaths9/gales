#ifndef __FLUID_IC_BC_HPP
#define __FLUID_IC_BC_HPP



#include "../../../src/fem/fem.hpp"



namespace GALES {




  template<int dim>
  class fluid_ic_bc 
  {

    using nd_type = node<dim>;
    using point_type = point<dim>;
    using vec = boost::numeric::ublas::vector<double>;


  public :
 
    fluid_ic_bc():
      Y_min_(0.0), 
      Y_max_(1.0-Y_min_),      
      p_ref_(1.0), v1_ref_(1.0), v2_ref_(1.0), v3_ref_(1.0), T_ref_(1.0)   // these are for dc_2006      
      {}




    void body_force(vec& gravity, const point<2>& position)
    {
      gravity[0] = 0.0;
      gravity[1] = -9.81;
    }


    void body_force(vec& gravity,  const point<3>& position)
    {
      gravity[0] = 0.0;
      gravity[1] = -9.81;
      gravity[2] = 0.0;
    }




   
    //-------------------- IC ----------------------------------------

    double initial_p(const nd_type &nd) const { return 0.0; }
    double initial_vx(const nd_type &nd) const { return 1.e-8; }
    double initial_vy(const nd_type &nd) const { return 1.e-8; }
    double initial_vz(const nd_type &nd) const { return 0.0; }
    double initial_T(cconst nd_type &nd)const  
    { 
       double y = nd.get_y();
       if (y <= -2104.0+1.e-6)  return 1173.15; 
       else return 313.15;        
    }

    void initial_Y (const nd_type &nd, vec &Y)const 
    {
       double y = nd.get_y();
       if (y <= -2104.0+1.e-6)  Y[0] = Y_max_;
       else   Y[0] = Y_min_;
    }
 	





  // --------------------Dirichlet BC------------------------------------

    auto dirichlet_p(const nd_type &nd) const 
    {
       if(nd.flag() ==  7)     return std::make_pair(true, 19842285.8); 
      return std::make_pair(false,0.0);
    }


    auto dirichlet_vx(const nd_type &nd) const 
    { 
       if(nd.flag() ==  5)     return std::make_pair(true,0.0); 
       if(nd.flag() ==  4)     return std::make_pair(true,0.0);
       if(nd.flag() ==  6)     return std::make_pair(true,0.0);
       if(nd.get_y() <= -2104.0)  return std::make_pair(true,0.0);
	
      return std::make_pair(false,0.0);
    }


    auto dirichlet_vy(const nd_type &nd) 
    {
       if(nd.flag() ==  5)     return std::make_pair(true,0.0); 
       if(nd.flag() ==  4)     return std::make_pair(true,0.0);
       if(nd.flag() ==  6)     return std::make_pair(true,-0.314);                 //(true,-0.314);   //mass flux = 333.33 = rho*v;  rho=1060.0
//       if(nd.flag() ==  6)     
//       {
//           auto t = time::get().t();
//           if(t<1.0) return std::make_pair(true,-0.314*time::get().t());
//           else return std::make_pair(true,-0.314);
//       }
       
       if(nd.get_y() <= -2104.0)  return std::make_pair(true,0.0);
	  
      return std::make_pair(false,0.0);
    }


    auto dirichlet_vz(const nd_type &nd) 
    {
      return std::make_pair(false,0.0);
    }


    auto dirichlet_T(const nd_type &nd) const 
    {
       if(nd.flag() ==  6)     return std::make_pair(true, 313.15);

      return std::make_pair(false,0.0);
    }
 

    // comp_index is the index of component. e.g. for 3 component mixture we have first 2 components as dofs; Y[0] and Y[1]
    // 0---first component;   1----second component
    auto dirichlet_Y(const nd_type &nd, int comp_index)const
     {
       if( nd.flag() == 6 && comp_index == 0 )          return std::make_pair(true, Y_min_);
       if( nd.flag() == 7 && comp_index == 0 )          return std::make_pair(true, Y_min_);
       if(nd.get_y() <= -2104.0)  return std::make_pair(true, Y_max_);
       if(nd.get_y() > -2104.0)  return std::make_pair(true, Y_min_);


      return std::make_pair(false,0.0);
    }





  //-------------------------Neumann BC--------------------------------------------------

    auto neumann_tau11(const std::vector<int>& bd_nodes, int side_flag) 
    { 
      return std::make_pair(false,0.0);  
    }

    auto neumann_tau12(const std::vector<int>& bd_nodes, int side_flag) 
    { 
      if(side_flag == 7)     return std::make_pair(true,0.0);
      return std::make_pair(false,0.0);
    }

    auto neumann_tau13(const std::vector<int>& bd_nodes, int side_flag) 
    {  
      return std::make_pair(false,0.0);
    }
  
    auto neumann_tau22(const std::vector<int>& bd_nodes, int side_flag) 
    { 
      if(side_flag == 7)     return std::make_pair(true,0.0);
      return std::make_pair(false,0.0); 
    }
                
    auto neumann_tau23(const std::vector<int>& bd_nodes, int side_flag) 
    { 
      return std::make_pair(false,0.0);
    }

    auto neumann_tau33(const std::vector<int>& bd_nodes, int side_flag) 
    {  
      return std::make_pair(false,0.0); 
    }
                
    auto neumann_q1(const std::vector<int>& bd_nodes, int side_flag)  
    {   
      if(side_flag == 4)     return std::make_pair(true,0.0);
      if(side_flag == 5)     return std::make_pair(true,0.0);

      return std::make_pair(false,0.0); 
    }

    auto neumann_q2(const std::vector<int>& bd_nodes, int side_flag) 
    { 
      if(side_flag == 4)     return std::make_pair(true,0.0);
      if(side_flag == 5)     return std::make_pair(true,0.0);
      if(side_flag == 7)     return std::make_pair(true,0.0);

      return std::make_pair(false,0.0); 
    }

    auto neumann_q3(const std::vector<int>& bd_nodes, int side_flag) 
    { 
      return std::make_pair(false,0.0); 
    }

    auto neumann_J1(int comp_index, const std::vector<int>& bd_nodes, int side_flag) 
    { 
      if(side_flag == 4 && comp_index==0 )     return std::make_pair(true,0.0);
      if(side_flag == 5 && comp_index==0 )     return std::make_pair(true,0.0);
      return std::make_pair(false,0.0); 
    }

    auto neumann_J2(int comp_index, const std::vector<int>& bd_nodes, int side_flag) 
    { 
      if(side_flag == 4 && comp_index==0 )     return std::make_pair(true,0.0);
      if(side_flag == 5 && comp_index==0 )     return std::make_pair(true,0.0);
      return std::make_pair(false,0.0); 
    }

    auto neumann_J3(int comp_index, const std::vector<int>& bd_nodes, int side_flag) 
    { 
      return std::make_pair(false,0.0); 
    }



    // rho*v1 towards outwards normal 
    auto mass_flux1(const std::vector<int>& bd_nodes, int side_flag)
    {
//      if(side_flag == 6)     return std::make_pair(true,0.0);
      return std::make_pair(false,0.0); 
    }

    // rho*v2 towards outwards normal
    auto mass_flux2(const std::vector<int>& bd_nodes, int side_flag)
    {
//      if(side_flag == 6)     return std::make_pair(true, -333.33);   //mass flow rate = 40 L/s,  length of inlet = 0.12,  mass flux = mass flow rate/length of inlet
      return std::make_pair(false,0.0); 
    }

    // rho*v3 towards outwards normal
    auto mass_flux3(const std::vector<int>& bd_nodes, int side_flag)
    {
      return std::make_pair(false,0.0); 
    }


  public:
    double Y_min_, Y_max_;    
    double p_ref_, v1_ref_, v2_ref_, v3_ref_, T_ref_;


  };
  
} //namespace
#endif



