#ifndef _GALES_FLUID_DOFS_IC_HPP_
#define _GALES_FLUID_DOFS_IC_HPP_






namespace GALES {



  template <int dim, typename ic_bc_type>
  class fluid_dofs_ic
  {
      using model_type = model<dim>;
      using vec = boost::numeric::ublas::vector<double>;

      public:
      
      fluid_dofs_ic(model_type& f_model, ic_bc_type& ic_bc, int nb_comp, const point<dim>& dummy)
      :  
      state_(f_model.state()), mesh_(f_model.mesh()), ic_bc_(ic_bc)
      {
        generate(dummy, nb_comp);     
      }   
  

    void generate(const point_2d& dummy, int nb_comp)
    {
      vec Y(nb_comp-1,0.0);
      for(const auto& nd : mesh_.nodes())
      {
        const int first_dof_lid (nd->first_dof_lid());
        state_.set_dof(first_dof_lid, ic_bc_.initial_p(nd->coord()));
        state_.set_dof(first_dof_lid+1, ic_bc_.initial_vx(nd->coord()));
        state_.set_dof(first_dof_lid+2, ic_bc_.initial_vy(nd->coord()));
	state_.set_dof(first_dof_lid+3, ic_bc_.initial_T(nd->coord()));

        ic_bc_.initial_Y(nd->coord(),Y);
        for(int i=0; i<nb_comp-1; i++)
          state_.set_dof(first_dof_lid+4+i, Y[i]);
      }
    }


    void generate(const point_3d& dummy, int nb_comp)
    {
      vec Y(nb_comp-1,0.0);
      for(const auto& nd : mesh_.nodes())
      {
        const int first_dof_lid (nd->first_dof_lid());
        state_.set_dof(first_dof_lid, ic_bc_.initial_p(nd->coord()));
        state_.set_dof(first_dof_lid+1, ic_bc_.initial_vx(nd->coord()));
        state_.set_dof(first_dof_lid+2, ic_bc_.initial_vy(nd->coord()));
        state_.set_dof(first_dof_lid+3, ic_bc_.initial_vz(nd->coord()));
	state_.set_dof(first_dof_lid+4, ic_bc_.initial_T(nd->coord()));

        ic_bc_.initial_Y(nd->coord(),Y);
        for(int i=0; i<nb_comp-1; i++)
          state_.set_dof(first_dof_lid+5+i, Y[i]);
      }
    }

  
    private:
    dof_state& state_;
    mesh<dim>& mesh_;
    ic_bc_type& ic_bc_;  
  };
  


}//namespace GALES
#endif


