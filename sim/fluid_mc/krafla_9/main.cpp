#include <mpi.h>
#include <iostream>

#include "../../../src/fem/fem.hpp"


#include "fluid_props.hpp" 
#include "fluid_ic_bc.hpp" 
#include "fluid_assembly.hpp"
#include "fluid_updater.hpp"
#include "fluid_dofs_ic.hpp"
#include "fluid_dofs_bc.hpp"
#include "fluid_2d.hpp"
#include "sec_dofs.hpp"





using namespace GALES;


int main(int argc, char* argv[])
{

  MPI_Init(&argc, &argv);


  //---------------- read setup file -------------------------------
  read_setup setup;


  //---------- creating directories ----------------------------------------------------------------------
  make_dirs("results/fluid_dofs");







  const int dim = 2;
  point<dim> dummy;
  const int nb_dofs_f = 2 + dim + setup.nb_comp() - 1;  










  //---------------------- ic bc -------------------------------------------------------------------------------------------------------
  typedef fluid_ic_bc<dim> fluid_ic_bc_type;
  
  //----------------------- ic ---------------------------------------------------------------------------------------------------------
  typedef fluid_dofs_ic<dim, fluid_ic_bc_type> fluid_dofs_ic_type;

  //----------------------  bc  -------------------------------------------------------------------------------------------------------
  typedef fluid_dofs_bc<dim, fluid_ic_bc_type> fluid_dofs_bc_type;

  //--------------------- scatters --------------------------------------------------------------------------------------------------------
  typedef scatter<fluid_dofs_bc_type>  fluid_scatter_type; 

  //-------------------- integrals -------------------------------------------------------------------------------------------------------
  typedef fluid<fluid_ic_bc_type, dim> fluid_integral_type;

  //-------------------- assembly ------------------------------------------------------------------------------------------------------
  typedef fluid_assembly<fluid_integral_type, fluid_scatter_type, dim> fluid_assembly_type;












  

  //------------------ mesh building --------------------------------------------------------------------
  print_only_pid<0>(std::cerr)<< "FLUID MESH\n";
  double mesh_read_start = MPI_Wtime();
  mesh<dim> fluid_mesh(setup.fluid_mesh_file(), nb_dofs_f, setup);
  print_only_pid<0>(std::cerr)<<"Mesh reading took: "<<std::setprecision(setup.precision()) << MPI_Wtime()-mesh_read_start<<" s\n\n";



  
  
  
  //-----------------fluid props------------------------------------------
  Fluid_Props fluid_props;

  //-----------------------------------------maps------------------------------------------------------------
  epetra_maps fluid_maps(fluid_mesh);
  
  //----------------------------------------dofs state-----------------------------------------------------  
  auto fluid_dof_state = std::make_unique<dof_state>(*fluid_maps.state_map(), 2);
  
  //---------------------------------------model---------------------------------------------------
  model<dim> fluid_model(fluid_mesh, *fluid_dof_state, fluid_maps);  

  //----------------------------------------ic bc---------------------------------------------------------
  fluid_ic_bc_type fluid_ic_bc;
  
  //----------------------------------------ic---------------------------------------------------------
  fluid_dofs_ic_type fluid_dofs_ic(fluid_model, fluid_ic_bc, setup.nb_comp(), dummy);
  
  //----------------------------------------bc---------------------------------------------------------
  fluid_dofs_bc_type fluid_dofs_bc(fluid_model, fluid_ic_bc, setup.nb_comp());

        
    
    
    
    //--------------pressure profile-----------------------------------------------------
   typedef boost::numeric::ublas::vector<double> v_type;

   int steps = 10001;
   auto h = 0.005;      // in mesh we have el_length = 0.01 m
   v_type p(steps+1), y(steps+1), T(steps+1), T1(steps+1);
   std::vector<v_type> A(steps+1), A1(steps+1);
   for(int i=0; i<=steps; i++)
   {
     y[i]= -2079.0 - h*i;       //wall and string surface=-2079  h=0.005   //magma surface=-2104
     point_2d pt(0.0, y[i]);
     T[i] = fluid_ic_bc.initial_T(pt);
     A[i].resize(2,0.0);
     fluid_ic_bc.initial_Y(pt, A[i]);          
     A[i][1] = 1.0 - A[i][0];

     auto y1 = y[i] - h/2.0;
     point_2d c(0.0, y1);
     T1[i] = fluid_ic_bc.initial_T(c);
     A1[i].resize(2,0.0);
     fluid_ic_bc.initial_Y(c, A1[i]);
     A1[i][1] = 1.0 - A1[i][0];
   }         

   //---------well----------------------------------------------
   p[0] = 19842285.8;         // p = rho(972.9) * g(9.81) * z(2079)
   double m1,m2,m3,m4; 
   for(int i=0; i<(steps-1)/2; i++)
   {
     fluid_props.properties(p[i], T[i], A[i], p[i], T[i]);
     m1 = 9.81*fluid_props.rho_;     
     fluid_props.properties(p[i]+0.5*h*m1, T1[i], A1[i], p[i]+0.5*h*m1, T1[i]);
     m2 = 9.81*fluid_props.rho_;     
     fluid_props.properties(p[i]+0.5*h*m2, T1[i], A1[i], p[i]+0.5*h*m2, T1[i]);
     m3 = 9.81*fluid_props.rho_;               
     fluid_props.properties(p[i]+h*m3, T[i+1], A[i+1], p[i]+h*m3, T[i+1]);
     m4 = 9.81*fluid_props.rho_;               
     p[i+1] = p[i]+h/6.0*(m1+2.0*(m2+m3)+m4);     
   }

   //------ magma----------------------------------------------------
   p[(steps-1)/2] = 45.4e6;             // from Paolo's presentation;   at magma surface at y=-2104   
   for(int i=(steps-1)/2; i<steps; i++)
   {
     fluid_props.properties(p[i], T[i], A[i], p[i], T[i]);
     m1 = 9.81*fluid_props.rho_;
     fluid_props.properties(p[i]+0.5*h*m1, T1[i], A1[i], p[i]+0.5*h*m1, T1[i]);
     m2 = 9.81*fluid_props.rho_;
     fluid_props.properties(p[i]+0.5*h*m2, T1[i], A1[i], p[i]+0.5*h*m2, T1[i]);
     m3 = 9.81*fluid_props.rho_;          
     fluid_props.properties(p[i]+h*m3, T[i+1], A[i+1], p[i]+h*m3, T[i+1]);
     m4 = 9.81*fluid_props.rho_;          
     p[i+1] = p[i]+h/6.0*(m1+2.0*(m2+m3)+m4);
   }
         
   for(const auto& nd : fluid_mesh.nodes())
   {
      const double y_coord(nd->get_y());
      const double x_coord(nd->get_x());
      const double p_interpolated = p_interpolation(y, p, y_coord);
      fluid_dof_state->set_dof(nd->first_dof_lid(), p_interpolated);
      
//      //----------------------this is to include effect of hook load on water just below string---------------
//      if(y_coord<=-2103.5 && y_coord>-2104)
//        if(x_coord >= -0.2413 && x_coord <= 0.2413)
//           fluid_dof_state->set_dof(nd->first_dof_lid(), p_interpolated+162845.9);     // hook load = 86.63 tonne = 78589.414 kg     L = 0.4826   p= hookload/L = 162845.9   
   }
   //--------------------------------------------------------------------------------    
   
    
    
    

  //----------------------scatter------------------------------------------------------
  fluid_scatter_type fluid_scatter(fluid_dofs_bc);   

  
  //---------------------updater------------------------------------------------------
  fluid_updater fluid_updater(*fluid_dof_state);

      
  //--------------------linear system-------------------------------------------------
  linear_system fluid_lp(fluid_model);
  
        
  //--------------------linear solver----------------------------------------------------
  belos_linear_solver fluid_ls_solver(setup, fluid_lp); 


  //---------------------non linear residual check-------------------------------------------
  residual_check fluid_res_check;
  
  //------------------------integral---------------------------------------------------------  
  fluid_integral_type fluid_integral(fluid_ic_bc, fluid_props, setup); 

  //-------------------------assembly--------------------------------------------------------
  fluid_assembly_type fluid_assembly(fluid_integral, fluid_scatter, fluid_model, fluid_lp);


  // ------------------parallel I/O---------------------------------------------------  
  IO fluid_io(*fluid_maps.state_map(), *fluid_maps.dof_map());


  //----------------------sec_dofs-----------------------------------------------
  Sec_Dofs<dim> sec_dofs(*fluid_maps.shared_node_map());















  
  time::get().t(0.0);
  time::get().delta_t(setup.delta_t());
    
  double restart_or_t_zero = MPI_Wtime();  
  if(!setup.restart())
  {
    //--------- setting up dofs for time = 0 ----------------
    fluid_dofs_bc.dirichlet_bc(dummy);
    fluid_io.write("fluid_dofs/", *fluid_dof_state);
    fluid_dof_state->dofs(1) = fluid_dof_state->dofs(0);
    sec_dofs.execute(fluid_model, fluid_props);    
    print_only_pid<0>(std::cerr)<<"Dofs writing for time 0 took: "<<std::setprecision(setup.precision()) << MPI_Wtime()-restart_or_t_zero<<" s\n\n";       
  }
  else
  {
    //--------------read restart-----------------------------
    time::get().t(setup.restart_time());
    fluid_io.read("fluid_dofs/", *fluid_dof_state);
    print_only_pid<0>(std::cerr)<<"Dofs reading at restart time took: "<<std::setprecision(setup.precision()) << MPI_Wtime()-restart_or_t_zero<<" s\n\n";       
  }



  // --------------------------------TIME LOOP-----------------------------------------------------------
  double time_loop_start = MPI_Wtime();
  double non_linear_res(0.0);
  int tot_time_iterations(0);

  while(time::get().t() < setup.end_time())
  {
    double t_iteration(MPI_Wtime());

    time::get().tick();
    print_only_pid<0>(std::cerr)<<"simulation time:  "<<std::setprecision(setup.precision()) << time::get().t()<<" s\n";

    fluid_updater.predictor();


      int it_count(0);
      while(it_count < setup.n_max_it())
      {
       //------------------------ SOLUTION For fluid ------------------------
        if(setup.t_dependent_dirichlet_bc())
           fluid_dofs_bc.dirichlet_bc(dummy);
	
	double fill_time = fluid_assembly.execute(it_count);   


        bool residual_converged = fluid_res_check.execute(*fluid_lp.rhs(), it_count, non_linear_res);      
        if(residual_converged)
        {
          print_only_pid<0>(std::cerr) << "FLUID  it: " << parse(it_count,5) << "Assembly: " << parse(fill_time,15) << "NonLinearRes: " << parse(non_linear_res,15) << "\n";        
          break;
        } 	              
        double solver_time = fluid_ls_solver.execute();


	fluid_updater.corrector(fluid_io.state_vec(*fluid_ls_solver.solution()));
//	fluid_model.trim_dofs_by_dof_index(0, 1.e5, 80.e6);
//	fluid_model.trim_dofs_by_dof_index(3, 303.15, 1273.15);
	fluid_model.trim_dofs_by_dof_index(4, 0.0, 1.0);
	

        print_only_pid<0>(std::cerr) 
        << "FLUID  it: " << parse(it_count,5) << "Assembly: " << parse(fill_time,15) << "NonLinearRes: " << parse(non_linear_res,15)<< "Solver: " << parse(solver_time,15)
        << "AbsResErr: " << parse(fluid_ls_solver.ARE(),15) << "RelResErr: "<< parse(fluid_ls_solver.RRE(),15)
        << "Num_it: " << parse(fluid_ls_solver.num_it(),8) << "dt: "<< time::get().delta_t()
        << "\n";

        // -----------------------end of fluid solution---------------------------------------------------------


    	it_count++;	
       }
       



    if((tot_time_iterations+1)%(setup.print_freq()) == 0)
    {
       fluid_io.write("fluid_dofs/", *fluid_dof_state);
//       sec_dofs.execute(fluid_model, fluid_props);    
    }
       



    print_only_pid<0>(std::cerr)<<"Time step took: "<<MPI_Wtime()-t_iteration<<" s\n\n";
    tot_time_iterations++;    	
  }

  print_only_pid<0>(std::cerr)<<"End time reached!!!! "<<"\n\n";
  print_only_pid<0>(std::cerr)<<"Total run time: "<<MPI_Wtime()-time_loop_start<<" s\n\n\n";

  MPI_Barrier(MPI_COMM_WORLD);
  MPI_Finalize();
  return 0;
}
