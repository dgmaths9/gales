#ifndef GALES_FLUID_PROPS_HPP
#define GALES_FLUID_PROPS_HPP





namespace GALES {



using namespace std;


class Fluid_Props : public mixture
{

  using vec = boost::numeric::ublas::vector<double>;
  using mat = boost::numeric::ublas::matrix<double>;
  
  public:

  Fluid_Props()
  {  
          read_lookup_table();
          nb_comp_ = 2;
          rho_comp_.resize(2);
          internal_energy_comp_.resize(2);
          chemical_diffusivity_comp_.resize(2);                  
          
          std::vector<double> oxides_molar_mass = {60.084, 79.876, 101.956, 159.692, 71.846, 70.937, 40.304, 56.078, 61.979, 94.195};   // g/mol  
          std::vector<double> oxides_wf = {0.7653, 0.0032, 0.1201, 0.01365, 0.01365, 0.0006, 0.0018, 0.0132, 0.0378, 0.0306};
          std::vector<double> cp_L_bar = {1331., 1392., 1545., 1434., 1100., 0., 2424., 1781., 1651., 1030.};           
          for(int i=0; i<10; i++) 
            cp_L_ += oxides_wf[i]*cp_L_bar[i];          // Stebbins et al., 1984;   Lesher and Spera, 2015;    Bouhifd et al., 2006
            

          for(auto& a: oxides_molar_mass) a *= 1.e-3;        // in kg
            
          auto molar_mass = 0.0;
          for (std::size_t i=0; i<10; i++)   molar_mass += oxides_wf[i]/oxides_molar_mass[i];
          molar_mass = 1./molar_mass;      // in kg
          
          oxides_molar_fraction_.resize(10);      
          for(std::size_t i=0; i<10; i++)   oxides_molar_fraction_[i] =  oxides_wf[i]*molar_mass/oxides_molar_mass[i];
          
          for(int i=0; i<10; i++) 
             pmav_ += oxides_molar_mass[i]*oxides_molar_fraction_[i];
  }








    ///magma is considered as first component and steam/water second     
   /// have to introduce iteration because we want to use  w_h2o_ from previous time step at first iteration to compute m_mu and then check the condition 
   /// and for subsequent iterations
   /// 
  /// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  void properties(double p, double T, const vec &Y, double p_P, double T_P, int itr = 0)
  {
     //---------------------------------------------------------------------------------------------------------------------------
     interpolation_range_T_p(T, p);
     auto s_rho = get_value(*steam_rho_);
     auto s_mu = get_value(*steam_mu_);
     auto s_alpha = get_value(*steam_alpha_);
     auto s_beta = get_value(*steam_beta_);
     auto s_c = get_value(*steam_c_);
     auto s_cp = get_value(*steam_cp_);
     auto s_cv = get_value(*steam_cv_);
     auto s_kappa = get_value(*steam_kappa_);          
     //---------------------------------------------------------------------------------------------------------------------------         



     interpolation_range_T_p(T_P, p_P);         
     w_h2o_P_ = get_value(*w_h2o_mat_);
     if(itr==0)
     {
        w_h2o_ = w_h2o_P_;         //at iteration 0 we use the previous values of p and T to compute w_h2o_
     }  
     else 
     {
        interpolation_range_T_p(T, p);  
        w_h2o_ = get_value(*w_h2o_mat_);                  
     }        
            
     
     interpolation_range_T_h2o(T, w_h2o_);
     
//     auto m_mu = get_value(*magma_mu_);
     auto m_mu =  6.0;
     
     auto m_rho = get_value(*rhom_h2o_);
     auto m_alpha = get_value(*alpha_h2o_);
     auto m_beta = get_value(*beta_h2o_);
     auto m_c = 1.0/sqrt(m_rho*m_beta);


     double cp_D(0.0), alpha_D(0.0);           
 
     //-----------------------------compute cp and thermal diffusivity(alpha) for dense phase ------------------------------------
     if(itr > 0  && log(m_mu) + log((T-T_P)/time::get().delta_t()) > 11.5)    //at itr=0 T=T_P
     {             
         // glass transition occurs         
         auto T_G_right = T;
         auto T_G_left = T_G_right - 100.0;
                  
                  
         // Stebbins et al., 1984; Lesher and Spera, 2015; Bouhifd et al., 2006
         auto cp_L = cp_L_ + w_h2o_*4717.0;
         auto alpha_D_L = (-0.06 + 4.57e-5*T)*1.e-6;
         
         // specific heat for water-free glass(Richet, 1987)
         auto cp_GL = 0.0;
         for(int i=0; i<a_.size(); i++) 
            cp_GL += oxides_molar_fraction_[i]*(a_[i] + b_[i]*T*1.e-3 + c_[i]*1.e-5/(T*T) + d_[i]/sqrt(T));                        
         cp_GL = cp_GL/pmav_;   // this is to convert from cp/mol to cp/kg
         
         //add dissolved water (Bouhifd et al. 2006)
         auto cpwgl = -122.319 + 341.631*1.e-3*T + 63.4426*1.e5/(T*T);     
         auto x_h2o_l = 1.0/(1.0+((1.0-w_h2o_P_)*18.01528)/(w_h2o_P_*pmav_));   
         cpwgl = cpwgl*1.e-3/x_h2o_l;
         cp_GL = w_h2o_P_*cpwgl + (1.0-w_h2o_P_)*cp_GL; 
          
         auto alpha_D_GL = (5.98*std::pow(T,-0.41) + 9.23e-5*T)*1.e-6;            //Moitra et al., 2018        
                           
                           
         if(T >= T_G_right)  
         { 
             cp_D = cp_L;  
             alpha_D = alpha_D_L;
         }
         else if(T <= T_G_left) 
         {
             cp_D = cp_GL;
             alpha_D = alpha_D_GL;                   //Moitra et al., 2018
         }    
         else   //in between
         { 
            cp_D = (cp_L*(T-T_G_left) + cp_GL*(T_G_right- T))/100.0;                  
            alpha_D = (alpha_D_L*(T-T_G_left) + alpha_D_GL*(T_G_right- T))/100.0;
         }   
     }
     else
     {
        cp_D = cp_L_ + w_h2o_*4717.0;
        alpha_D = (-0.06 + 4.57e-5*T)*1.e-6;
     }
     //---------------------------------------------------------------------------------- ----------------------------------------
          
          
          
          
     //------------------------------------------Correct cp for magma porosity (h2o and co2) -------------------------------------
     // Regression of data from classical engineering handbooks
     // cp: J/Kg K     T: K       Range: 175 < T(K) < 1500
     auto cp_G_h2o = 1840.4 - 0.14972*T + 9.145e-4*T*T - 3.1759e-7*T*T*T; 
     auto cp_G_co2 = 472.27 + 1.5228*T - 1.014e-3*T*T + 2.5368e-7*T*T*T;
     auto w_G_co2_h2o = get_value(*w_g_co2_h2o_); 
     auto cp_G = (1.0-w_G_co2_h2o)*cp_G_h2o + w_G_co2_h2o*cp_G_co2;
     
     auto eps = get_value(*eps_h2o_);
     auto rho_G = get_value(*rhog_h2o_);     
     auto rho_D = (m_rho - eps*rho_G)/(1.0-eps);
     auto m_cp = (rho_D*cp_D*(1.0-eps) + rho_G*cp_G*eps)/(rho_D*(1.0-eps) + rho_G*eps);    // Heap et al., 2020
     //----------------------------------------------------------------------------------------------------------------------------
          


     //--------------------------------------------compute cv----------------------------------------------------------------------
     auto cv_D = cp_D;          
     auto R_G = 8.3144598*1.e3/((1.0-w_G_co2_h2o)*18.01528 + w_G_co2_h2o*44.01);      
     auto cv_G = cp_G - R_G;
     auto m_cv = (rho_D*cv_D*(1.0-eps) + rho_G*cv_G*eps)/(rho_D*(1.0-eps) + rho_G*eps);    // Heap et al., 2020       
     //----------------------------------------------------------------------------------------------------------------------------
     
     
     
     //--------------------------------------------kappa for dense phase + porosity -----------------------------------------------     
     auto k_D = rho_D*cp_D*alpha_D;                  
     auto k_h2o = 4.1771e-3 + 2.2849e-5*T + 9.4663e-8*T*T - 2.5778e-11*T*T*T;
     auto k_co2 = -1.0081e-2 + 9.1422e-5*T - 6.4318e-9*T*T - 4.31e-12*T*T*T;

     double x_h2o = 1.0/(1.0+((1.0-w_G_co2_h2o)*44.01)/(w_G_co2_h2o*18.01528));      
     double x_co2 = 1.0 - x_h2o;         
     
     
     auto k_G = 0.5*(k_h2o*k_co2/(x_h2o*k_h2o + x_co2*k_co2) + x_h2o*k_h2o + x_co2*k_co2);      //Udoetok 2013
     auto m = k_G/k_D;                    
     auto beta = 3.0*(1-m)/(2+m);                // for spherical pores (Zimmermann 1989)
     auto m_kappa = (k_D*(1-eps)*(1-m) + m*beta*eps)/((1-eps)*(1-m) + beta*eps);    // Maxwell equation  Heap et al., 2020  
     //----------------------------------------------------------------------------------------------------------------------------


         
////     print_in_file("err", "s_rho", s_rho);
////     print_in_file("err", "s_mu", s_mu);
////     print_in_file("err", "s_alpha", s_alpha);
////     print_in_file("err", "s_beta", s_beta);
////     print_in_file("err", "s_c", s_c);
////     print_in_file("err", "s_cp", s_cp);
////     print_in_file("err", "s_cv", s_cv);
////     print_in_file("err", "s_kappa", s_kappa);
//         
////     print_in_file("err", "m_rho", m_rho);
////     print_in_file("err", "m_mu", m_mu);
////     print_in_file("err", "m_alpha", m_alpha);
////     print_in_file("err", "m_beta", m_beta);
////     print_in_file("err", "m_c", m_c);
////     print_in_file("err", "m_cp", m_cp);
////     print_in_file("err", "m_cv", m_cv);
////     print_in_file("err", "m_kappa", m_kappa);



//     auto s_rho = 1.e3;
//     auto s_mu = 1.e-3;
//     auto s_alpha = 3.9e-4;
//     auto s_beta = 4.2e-10;
//     auto s_c = 1560.0;
//     auto s_cp = 4132.0;
//     auto s_cv = 4017.0;
//     auto s_kappa = 0.6;          
     

//     auto m_rho = 2060.0;
//     auto m_mu =  6.0;
//     auto m_alpha = 1.6e-4;
//     auto m_beta = 2.e-9;
//     auto m_c = 500.0;
//     auto m_cp = 1454.0;
//     auto m_cv = 1451.0;
//     auto m_kappa = 0.58;          
     
     
         
     clear();
     rho_ = std::pow(Y[0]/m_rho + Y[1]/s_rho,-1); 
     mu_  = Y[0]*m_mu + Y[1]*s_mu;
     cp_  = Y[0]*m_cp + Y[1]*s_cp;
     cv_  = Y[0]*m_cv + Y[1]*s_cv;     
     kappa_  = Y[0]*m_kappa + Y[1]*s_kappa;     
     alpha_  = rho_*(Y[0]*m_alpha/m_rho + Y[1]*s_alpha/s_rho);
     beta_  = rho_*(Y[0]*m_beta/m_rho + Y[1]*s_beta/s_rho);
     sound_speed_  =  m_rho*s_rho*m_c*s_c/(rho_*sqrt(Y[0]*s_rho*s_rho*s_c*s_c + Y[1]*m_rho*m_rho*m_c*m_c));

     rho_comp_[0] = m_rho;     rho_comp_[1] = s_rho;
     internal_energy_comp_[0] = m_cv*T;     internal_energy_comp_[1] = s_cv*T;
     chemical_diffusivity_comp_[0] = 1.e-10;     chemical_diffusivity_comp_[1] = 1.e-10;


//     print_in_file("err", "rho", rho_);
//     print_in_file("err", "mu", mu_);
//     print_in_file("err", "alpha", alpha_);
//     print_in_file("err", "beta", beta_);
//     print_in_file("err", "c", sound_speed_);
//     print_in_file("err", "cp", cp_);
//     print_in_file("err", "cv", cv_);
//     print_in_file("err", "kappa", kappa_);
//     print_in_file("err", "");

  }
  /// ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   





         


   
      
      
  /// ---------------------------------------------------------------------------------------------------------
  void read_lookup_table()
  {
      read_vec("input/lookuptable/T.txt", T_vec_, 98);
      read_vec("input/lookuptable/p.txt", p_vec_, 800);      
                  
      read_mat("input/lookuptable/steam/rho.txt", steam_rho_, 98, 800);
      read_mat("input/lookuptable/steam/mu.txt", steam_mu_, 98, 800);
      read_mat("input/lookuptable/steam/alpha.txt", steam_alpha_, 98, 800);
      read_mat("input/lookuptable/steam/beta.txt", steam_beta_, 98, 800);
      read_mat("input/lookuptable/steam/sound_speed.txt", steam_c_, 98, 800);
      read_mat("input/lookuptable/steam/cp.txt", steam_cp_, 98, 800);
      read_mat("input/lookuptable/steam/cv.txt", steam_cv_, 98, 800);
      read_mat("input/lookuptable/steam/kappa.txt", steam_kappa_, 98, 800);            


      read_mat("input/lookuptable/magma/w_h2o.txt", w_h2o_mat_, 98, 800);


      read_vec("input/lookuptable/w_h2o_vec.txt", w_h2o_vec_, 386);            
      read_mat("input/lookuptable/magma/mu.txt", magma_mu_, 98, 386);
      read_mat("input/lookuptable/magma/w_g_co2_h2o.txt", w_g_co2_h2o_, 98, 386);
      read_mat("input/lookuptable/magma/eps_h2o.txt", eps_h2o_, 98, 386);
      read_mat("input/lookuptable/magma/rhog_h2o.txt", rhog_h2o_, 98, 386);
      read_mat("input/lookuptable/magma/rhom_h2o.txt", rhom_h2o_, 98, 386);
      read_mat("input/lookuptable/magma/alpha_h2o.txt", alpha_h2o_, 98, 386);
      read_mat("input/lookuptable/magma/beta_h2o.txt", beta_h2o_, 98, 386);


  }
  /// ---------------------------------------------------------------------------------------------------------







  /// ---------------------------------------------------------------------------------------------------------
  void read_vec(const std::string& s, std::unique_ptr<vec>& v, int size)
  {
      ifstream f(s);                
      if(!f.is_open())  Error("unable to open and read file " + s);     
      f>>size;
      v = make_unique<vec>(size);
      for (int i=0; i<size; i++)
        f >> (*v)[i];
      f.close();  
  }
  /// ---------------------------------------------------------------------------------------------------------
  
   
   
//              
//                           --------->
//                     _____p1__________p2____
//                  |  |
//                  |  T1    .           .
//                T |  |
//                  |  |        .(T,p)
//                  v  |
//                    T2    .           .
//                     |
//                     |
         
  /// ---------------------------------------------------------------------------------------------------------
  void read_mat(const std::string& s, std::unique_ptr<mat>& m, int rows, int cols)
  {
      ifstream f(s);
      if(!f.is_open())  Error("unable to open and read file " + s);     
      m = make_unique<mat>(rows, cols);
      for (int i=0; i<rows; i++)
       for (int j=0; j<cols; j++)
         f >> (*m)(i,j);
      f.close();            
  }
  /// ---------------------------------------------------------------------------------------------------------






  /// ---------------------------------------------------------------------------------------------------------
  void interpolation_range_T_p(double T, double p)
  {                
      auto delta_T = std::abs((*T_vec_)[1] - (*T_vec_)[0]);
      r_indx_ = (int)floor(std::abs(T - (*T_vec_)[0])/delta_T);      
      if(r_indx_ >= 98)
      {
          std::cerr<<"\n T = "<<T<<"\n\n";
          Error("r_indx_ goes out of the vector scope");
      }
      const double T1 = (*T_vec_)[r_indx_];      
      const double T2 = (*T_vec_)[r_indx_+1];    

      auto delta_p = std::abs((*p_vec_)[1]-(*p_vec_)[0]);
      c_indx_ = (int)floor(std::abs(p - (*p_vec_)[0])/delta_p);     
      if(c_indx_ >= 800)
      {
          std::cerr<<"\n p = "<<p<<"\n\n";
          Error("c_index goes out of the vector scope");
      }
      const double p1 = (*p_vec_)[c_indx_];         
      const double p2 = (*p_vec_)[c_indx_+1];       
      
      a1_ = (T2-T)*(p2-p)/(delta_T*delta_p);
      a2_ = (T2-T)*(p-p1)/(delta_T*delta_p);
      a3_ = (T-T1)*(p2-p)/(delta_T*delta_p);
      a4_ = (T-T1)*(p-p1)/(delta_T*delta_p);
  }
  /// ---------------------------------------------------------------------------------------------------------
  







  /// ---------------------------------------------------------------------------------------------------------
  void interpolation_range_T_h2o(double T, double w_h2o)
  {                
      auto delta_T = std::abs((*T_vec_)[1] - (*T_vec_)[0]);
      r_indx_ = (int)floor(std::abs(T - (*T_vec_)[0])/delta_T);      
      if(r_indx_ >= 98)
      {
          std::cerr<<"\n T = "<<T<<"\n\n";
          Error("r_indx_ goes out of the vector scope");
      }
      const double T1 = (*T_vec_)[r_indx_];      
      const double T2 = (*T_vec_)[r_indx_+1];    

      auto delta_w_h2o = std::abs((*w_h2o_vec_)[1]-(*w_h2o_vec_)[0]);
      c_indx_ = (int)floor(std::abs(w_h2o - (*w_h2o_vec_)[0])/delta_w_h2o);     
      if(c_indx_ >= 386)
      {
          std::cerr<<"\n w_h2o = "<<w_h2o<<"\n\n";
          Error("c_index goes out of the vector scope");
      }
      const double w_h2o_1 = (*w_h2o_vec_)[c_indx_];         
      const double w_h2o_2 = (*w_h2o_vec_)[c_indx_+1];       
      
      a1_ = (T2-T)*(w_h2o_2-w_h2o)/(delta_T*delta_w_h2o);
      a2_ = (T2-T)*(w_h2o-w_h2o_1)/(delta_T*delta_w_h2o);
      a3_ = (T-T1)*(w_h2o_2-w_h2o)/(delta_T*delta_w_h2o);
      a4_ = (T-T1)*(w_h2o-w_h2o_1)/(delta_T*delta_w_h2o);
  }
  /// ---------------------------------------------------------------------------------------------------------
  
  
  
  
  
  
  
  
  /// ---------------------------------------------------------------------------------------------------------
  double get_value(const mat& m)
  {
     return a1_*m(r_indx_, c_indx_) + a2_*m(r_indx_, c_indx_+1) + a3_*m(r_indx_+1, c_indx_) + a4_*m(r_indx_+1, c_indx_+1);
  }
  /// ---------------------------------------------------------------------------------------------------------
  
     
     
     
     
     
 private:
 
     // ----------these are for interpolation---------------       
     int c_indx_ = 0;
     int r_indx_ = 0;
     double a1_ = 0.0;    
     double a2_ = 0.0;    
     double a3_ = 0.0;    
     double a4_ = 0.0;

     // ----------these are for m_cp --------------- 
     double w_h2o_ = 0.0;               
     double w_h2o_P_ = 0.0;    //  w_h2o_ of previous time step             
     double cp_L_ = 0.0;  
     std::vector<double> oxides_molar_fraction_; // computed in the constructor            
     double pmav_ = 0.0;    //average molecular weight of the volatile-free liquid
     std::vector<double> a_ = {127.2, 64.111, 175.491, 135.25, 31.77, 0.0, 46.704, 39.159, 70.884, 84.323};          
     std::vector<double> b_ = {-10.777, 22.59, -5.839, 12.311, 38.515, 0.0, 11.22, 18.65, 26.11, 0.731};          
     std::vector<double> c_ = {4.3127, -23.02, -13.47, -39.098, -0.012, 0.0, -13.28, -1.523, -3.582, -8.298};           
     std::vector<double> d_ = {-1463.9, 0.0, -1370.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};             
            
     
        
     unique_ptr<vec> p_vec_ = nullptr; 
     unique_ptr<vec> T_vec_ = nullptr;
     unique_ptr<vec> w_h2o_vec_ = nullptr;
     
     unique_ptr<mat> steam_rho_ = nullptr; 
     unique_ptr<mat> steam_mu_ = nullptr;
     unique_ptr<mat> steam_alpha_ = nullptr; 
     unique_ptr<mat> steam_beta_ = nullptr;
     unique_ptr<mat> steam_c_ = nullptr; 
     unique_ptr<mat> steam_cp_ = nullptr;
     unique_ptr<mat> steam_cv_ = nullptr;
     unique_ptr<mat> steam_kappa_ = nullptr;     
     
     unique_ptr<mat> w_h2o_mat_ = nullptr;   // function of T, p
     unique_ptr<mat> magma_mu_ = nullptr;    // function of T, w_h2o_
     unique_ptr<mat> w_g_co2_h2o_ = nullptr;    // function of T, w_h2o_
     unique_ptr<mat> eps_h2o_ = nullptr;    // function of T, w_h2o_
     unique_ptr<mat> rhog_h2o_ = nullptr;    // function of T, w_h2o_
     unique_ptr<mat> rhom_h2o_ = nullptr;    // function of T, w_h2o_
     unique_ptr<mat> alpha_h2o_ = nullptr;    // function of T, w_h2o_
     unique_ptr<mat> beta_h2o_ = nullptr;    // function of T, w_h2o_
     
     
};


} //namespace
#endif

