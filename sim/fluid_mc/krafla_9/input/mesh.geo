Mesh.MshFileVersion=2;


chamber_outer = 0.5;
annulus_top = 0.008;
well_bottom = 0.008;
string_bottom = 0.008;


Point(0) = {0,-2104,0, well_bottom};

Point(1) = {25,-2104,0, chamber_outer};
Point(2) = {-25,-2104,0, chamber_outer};
Point(3) = {0,-2129,0, chamber_outer};

Point(6) = {-0.205,-2079,0, annulus_top};
Point(7) = {0.205,-2079,0, annulus_top};
Point(10) = {-0.06,-2079,0, annulus_top};
Point(11) = {0.06,-2079,0, annulus_top};

Point(4) = {0.205,-2104,0,well_bottom};
Point(5) = {-0.205,-2104,0,well_bottom};

Point(8) = {-0.06,-2103.80,0,string_bottom};
Point(9) = {0.06,-2103.80,0,string_bottom};




//+
Circle(1) = {2, 0, 3};
//+
Circle(2) = {1, 0, 3};
//+
Line(3) = {2, 5};
//+
Line(4) = {5, 6};
//+
Line(5) = {6, 10};
//+
Line(6) = {10, 8};
//+
Line(7) = {8, 9};
//+
Line(8) = {9, 11};
//+
Line(9) = {11, 7};
//+
Line(10) = {7, 4};
//+
Line(11) = {4, 1};
//+
Line(12) = {5, 0};
//+
Line(13) = {0, 4};
//+
Line Loop(1) = {10, -13, -12, 4, 5, 6, 7, 8, 9};
//+
//+
Plane Surface(1) = {1};
//+
Line Loop(2) = {3, 12, 13, 11, 2, -1};
//+
Plane Surface(2) = {2};

//Recombine Surface{1,2};

Physical Surface(1) = {1,2};
//+
Physical Line(6) = {7};
//+
Physical Line(7) = {5, 9};
//+
Physical Line(5) = {6, 8, 10, 4, 3, 11};
//+
Physical Line(4) = {1, 2};
//+
Physical Point(4) = {2, 1, 3};
//+
Physical Point(5) = {10, 6, 11, 7, 5, 8, 9, 4};
