#ifndef __SEC_DOFS_HPP
#define __SEC_DOFS_HPP



namespace GALES {



  template<int dim>
  class Sec_Dofs 
  {

    private:     
     IO sec_io_;  


    public:

     explicit Sec_Dofs(Epetra_Map& map) : sec_io_(map)
     {
       std::vector<std::string> v = {"rho", "mu", "cp", "cv", "alpha", "beta", "sound_speed", "kappa"};
       for(auto a : v)  make_dirs("results/sec_dofs/" + a);
     }
      


     void print_sec_dofs(std::string s, const std::vector<double>& v)
     {
       std::stringstream ss;
       ss<<"results/sec_dofs/"<<s<<"/"<<time::get().t();
       sec_io_.write2(ss.str(), v);  	
     }
     
                   
     
     void execute(const model<dim>& fluid_model, Fluid_Props& fluid_props)
     {
        std::vector<double> rho, mu, cp, cv, alpha, beta, kappa, sound_speed;
        for(const auto& nd : fluid_model.mesh().nodes()) 
        {
           const int first_dof_lid (nd->first_dof_lid());      
           const double p(fluid_model.state().get_dof(first_dof_lid));           
           const double T(fluid_model.state().get_dof(first_dof_lid+dim+1));          
           const double p_P(fluid_model.state().get_dof(first_dof_lid, 1));           
           const double T_P(fluid_model.state().get_dof(first_dof_lid+dim+1, 1));          
        
           boost::numeric::ublas::vector<double> Y(2, 0.0);	
           Y[0] = fluid_model.state().get_dof(first_dof_lid+dim+2);
           Y[1] = 1.0 - Y[0];
           
           fluid_props.properties(p, T, Y, p_P, T_P);
        
           rho.push_back(fluid_props.rho_);
           mu.push_back(fluid_props.mu_);
           cp.push_back(fluid_props.cp_);
           cv.push_back(fluid_props.cv_);
           alpha.push_back(fluid_props.alpha_);
           beta.push_back(fluid_props.beta_);
           kappa.push_back(fluid_props.kappa_);
           sound_speed.push_back(fluid_props.sound_speed_);
        }   
        
        print_sec_dofs("rho", rho);        
        print_sec_dofs("mu", mu);        
        print_sec_dofs("cp", cp);        
        print_sec_dofs("cv", cv);        
        print_sec_dofs("alpha", alpha);
        print_sec_dofs("beta", beta);        
        print_sec_dofs("kappa", kappa);
        print_sec_dofs("sound_speed", sound_speed);
     }

  };   
}   
#endif
