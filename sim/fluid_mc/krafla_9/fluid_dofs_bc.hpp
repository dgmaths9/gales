#ifndef _GALES_FLUID_DOFS_BC_HPP_
#define _GALES_FLUID_DOFS_BC_HPP_






namespace GALES {



  template <int dim, typename ic_bc_type>
  class fluid_dofs_bc
  {
      using model_type = model<dim>;
  
      public:
      
      fluid_dofs_bc(model_type& f_model, ic_bc_type& ic_bc, int nb_comp)
      :  
      state_(f_model.state()), mesh_(f_model.mesh()), ic_bc_(ic_bc), nb_comp_(nb_comp)
      {}



      void dirichlet_bc(const point_2d& dummy)
      {
        std::pair<bool,double> result;

        for(const auto& nd : mesh_.nodes())
        {
          const int first_dof_lid (nd->first_dof_lid());

          result= ic_bc_.dirichlet_p(*nd);
          if (result.first)          state_.set_dof(first_dof_lid, result.second);

          result= ic_bc_.dirichlet_vx(*nd);
          if (result.first)          state_.set_dof(first_dof_lid+1, result.second);

          result= ic_bc_.dirichlet_vy(*nd);
          if (result.first)          state_.set_dof(first_dof_lid+2, result.second);

          result= ic_bc_.dirichlet_T(*nd);
          if (result.first)          state_.set_dof(first_dof_lid+3, result.second);

          for(int i=0; i<nb_comp_-1; i++)
          {
            result = ic_bc_.dirichlet_Y(*nd, i);
            if(result.first)         state_.set_dof(first_dof_lid+4+i, result.second);
          }    
        }
      }


   
      void dirichlet_bc(const point_3d& dummy)
      {
        std::pair<bool,double> result;

        for(const auto& nd : mesh_.nodes())
        {
          const int first_dof_lid (nd->first_dof_lid());

          result= ic_bc_.dirichlet_p(*nd);
          if (result.first)          state_.set_dof(first_dof_lid, result.second);

          result= ic_bc_.dirichlet_vx(*nd);
          if (result.first)          state_.set_dof(first_dof_lid+1, result.second);

          result= ic_bc_.dirichlet_vy(*nd);
          if (result.first)          state_.set_dof(first_dof_lid+2, result.second);

          result= ic_bc_.dirichlet_vz(*nd);
          if (result.first)          state_.set_dof(first_dof_lid+3, result.second);

          result= ic_bc_.dirichlet_T(*nd);
          if (result.first)          state_.set_dof(first_dof_lid+4, result.second);

          for(int i=0; i<nb_comp_-1; i++)
          {
            result = ic_bc_.dirichlet_Y(*nd, i);
            if(result.first)         state_.set_dof(first_dof_lid+5+i, result.second);
          }    
        }
      }







    template<typename nd_type>
    auto dirichlet(int dof, const nd_type &nd, const point_2d& dummy)const  
    {
      const int nb_dofs = 4 + nb_comp_-1;
      const int n = dof%nb_dofs;
      std::pair<bool,double> result(false, 0.0);
      std::pair<bool,double> value(false, 0.0);

      switch(n)
      {
          case 0:
              result = ic_bc_.dirichlet_p(nd);
              if(result.first == true) value = std::make_pair(true, 0.0);
              break;
          case 1:
              result = ic_bc_.dirichlet_vx(nd);
              if(result.first == true) value = std::make_pair(true, 0.0);
              break;
          case 2:
              result = ic_bc_.dirichlet_vy(nd);
              if(result.first == true) value = std::make_pair(true, 0.0);
              break;
          case 3:
              result = ic_bc_.dirichlet_T(nd);
              if(result.first == true) value = std::make_pair(true, 0.0);
              break;
          default:
              result = ic_bc_.dirichlet_Y(nd,n%4);
              if(result.first == true) value = std::make_pair(true, 0.0);
              break;
      }
      return value;
    }
 


    template<typename nd_type>
    auto dirichlet(int dof, const nd_type &nd, const point_3d& dummy)const  
    {
      const int nb_dofs = 5 + nb_comp_-1;
      const int n = dof%nb_dofs;
      std::pair<bool,double> result(false, 0.0);
      std::pair<bool,double> value(false, 0.0);

      switch(n)
      {
          case 0:
              result = ic_bc_.dirichlet_p(nd);
              if(result.first == true) value = std::make_pair(true, 0.0);
              break;
          case 1:
              result = ic_bc_.dirichlet_vx(nd);
              if(result.first == true) value = std::make_pair(true, 0.0);
              break;
          case 2:
              result = ic_bc_.dirichlet_vy(nd);
              if(result.first == true) value = std::make_pair(true, 0.0);
              break;
          case 3:
              result = ic_bc_.dirichlet_vz(nd);
              if(result.first == true) value = std::make_pair(true, 0.0);
              break;
          case 4:
              result = ic_bc_.dirichlet_T(nd);
              if(result.first == true) value = std::make_pair(true, 0.0);
              break;
          default:
              result = ic_bc_.dirichlet_Y(nd,n%5);
              if(result.first == true) value = std::make_pair(true, 0.0);
      }
      return value;
    }



    private:
    dof_state& state_;
    mesh<dim>& mesh_;
    ic_bc_type& ic_bc_;
    int nb_comp_;

  };


} /* namespace GALES */

#endif

