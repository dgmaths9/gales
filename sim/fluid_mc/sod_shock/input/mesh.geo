Mesh.MshFileVersion = 2;

lc=0.020;

Point(1) = {0,0,0,lc};
Point(2) = {1,0,0,lc};
Point(3) = {1,0.001,0,lc};
Point(4) = {0,0.001,0,lc};


Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};

Line Loop(6) = {1, 2, 3, 4};
Plane Surface(7) = {6};

Transfinite Line {1,3} = 201;
Transfinite Line {2,4} = 1;

Transfinite Surface {7}={1,2,3,4};
Recombine Surface {7};

Physical Surface(0) = {7};
Physical Line(2) = {4};
Physical Line(3) = {2};
Physical Line(4) = {1,3};
Physical Point(2) = {1,4};
Physical Point(3) = {2,3};

