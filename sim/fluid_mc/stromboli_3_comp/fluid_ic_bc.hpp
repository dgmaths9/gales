#ifndef FLUID_IC_BC_HPP
#define FLUID_IC_BC_HPP


#include "../../../src/fem/fem.hpp"


namespace GALES{



  template<int dim>
  class fluid_ic_bc 
  {
    using nd_type = node<dim>;
    using vec = boost::numeric::ublas::vector<double>;

    public :



    fluid_ic_bc()
    {
        this->p0_ = 1.e5;
        this->max_ = 761.0;
        this->min_ = -3375.0;

        this->p_ref_ = this->p0_;
        this->v1_ref_ = 0.0;
        this->v2_ref_ = 0.0;
        this->T_ref_ = 0.0;

        this->Y_min_ = 0.0;
        this->Y_max_ = 1.0 - this->Y_min_;        
    }

    


    void body_force(v_type& gravity) const
    {
      gravity[1] = -9.81;
    }




    //-------------------- IC ----------------------------------------
    double initial_p(const nd_type &nd) const { return this->p0_; }
    double initial_vx(const nd_type &nd) const { return 0.0; }
    double initial_vy(const nd_type &nd) const { return 0.0; }
    double initial_vz(const nd_type &nd) const { return 0.0; }
    double initial_T(const nd_type &nd) const { return 0.0; }
        
    void initial_Y (const nd_type &nd, vec &Y)const 
    {
       double x = nd.get_x();
       double y = nd.get_y();
       
       if(y<-10) {Y[0]=0.0; Y[1]=1.0;}
       else {Y[0]=1.0; Y[1]=0.0;}
    }

 	




    // -------------------------  Dirichlet BC ------------------------
    auto dirichlet_p(const nd_type &nd) const
    {
//      if(nd.flag()==7)       return std::make_pair(false,1.e5);

      return std::make_pair(false, 0.0);
    }


    auto dirichlet_vx(const nd_type &nd) const
    {
      if(nd.flag()==4)       return std::make_pair(true,0.0);
      if(nd.flag()==5)       return std::make_pair(true,0.0);
      if(nd.flag()==6)       return std::make_pair(true,0.0);
      if(nd.flag()==7)       return std::make_pair(true,0.0);

      return std::make_pair(false,0.0);
    }


    auto dirichlet_vy(const nd_type &nd) const
    {
      if(nd.flag()==4)       return std::make_pair(true,0.0);
      if(nd.flag()==5)       return std::make_pair(true,0.0);
      if(nd.flag()==6)       return std::make_pair(true,0.0);
      if(nd.flag()==7)       return std::make_pair(true,0.0);

      return std::make_pair(false,0.0);
    }


    // comp_index is the index of component. e.g. for 3 component mixture we have first 2 components as dofs; Y[0] and Y[1]
    // 0---first component;   1----second component
    auto dirichlet_Y(const nd_type &nd, int comp_index) const
    {      
//      if(nd.flag()==6 && comp_index==0)       return std::make_pair(true, this->Y_min_);
      //if(nd.flag()==7 && comp_index==0)       return std::make_pair(true, this->Y_max_);
       
      return std::make_pair(false,0.0);
    }






  //-------------------------Neumann BC--------------------------------------------------

    auto neumann_tau11(const std::vector<int>& bd_nodes, int side_flag) const 
    { 
      if(side_flag == 3)       return std::make_pair(true,0.0);  
      return std::make_pair(false,0.0);  
    }

    auto neumann_tau12(const std::vector<int>& bd_nodes, int side_flag) const 
    { 
      if(side_flag == 3)       return std::make_pair(true,0.0);  
      if(side_flag == 2)       return std::make_pair(true,0.0);  

      return std::make_pair(false,0.0);
    }
  
    auto neumann_tau22(const std::vector<int>& bd_nodes, int side_flag) const 
    { 
      if(side_flag == 2)       return std::make_pair(true,0.0);  

      return std::make_pair(false,0.0); 
    }
                












     template<typename T1, typename T2>
//   void set_custom_pressure_profile(T1& model, T2& props)
//   {
//        dof_state& state(model.state()); 
//        simple_mesh<2>& mesh(model.mesh());
//           
//        v_type p_vec(total_steps_+1), y_vec(total_steps_+1);        
//        v_type T(total_steps_+1,0.0), T1(total_steps_+1,0.0);
//        std::vector<v_type> A(total_steps_+1), A1(total_steps_+1);
//        
//        print_only_pid<0>(std::cerr) << "inside set pp" <<"\n";
//  
//        for(int i=0; i<=total_steps_; i++)
//        {
//          y_vec[i]= max_ - h_*i;
//          point_2d b(0.0, y_vec[i]);
//          T[i] = initial_T(b);
//          A[i].resize(props.nb_comp_);
//          initial_Y(b, A[i]);
//          A[i][props.nb_comp_-1] = 1.0- std::accumulate(&A[i][0], &A[i][props.nb_comp_-1], 0.0);

//          auto y1 = y_vec[i] - h_*0.5;
//          point_2d c(0.0, y1);
//          T1[i] = initial_T(c);
//          A1[i].resize(props.nb_comp_);
//          initial_Y(c, A1[i]);
//          A1[i][props.nb_comp_-1] = 1.0- std::accumulate(&A1[i][0], &A1[i][props.nb_comp_-1], 0.0);
//        }        
//        print_only_pid<0>(std::cerr) << "first loop" <<"\n";

//   
//        double m1,m2,m3,m4;
//        p_vec[0] = p0_;
// 
//        for(int i=0; i<total_steps_; i++)
//        {
//          props.properties(p_vec[i],T[i],A[i],0.0);
//          m1 = 9.81*props.rho_;
//          props.properties(p_vec[i]+0.5*h_*m1,T1[i],A1[i],0.0);
//          m2 = 9.81*props.rho_;
//          props.properties(p_vec[i]+0.5*h_*m2,T1[i],A1[i],0.0);
//          m3 = 9.81*props.rho_;          
//          props.properties(p_vec[i]+h_*m3,T[i+1],A[i+1],0.0);
//          m4 = 9.81*props.rho_;          
//          p_vec[i+1] = p_vec[i]+h_/6.*(m1+2.0*(m2+m3)+m4);
//        }
//        
//        print_only_pid<0>(std::cerr) << "second loop" <<"\n";

//         for(const auto& nd : mesh.nodes())
//         {
//            const int first_dof_gid = nd->first_dof_gid();
//            const double y(nd->get_y());
//            const double x(nd->get_x());
//            double p(0.0);
//            if((y<-1.0) || ((y>=-1.0 && y<=761.0) && (x>=-5.0 &&  x<=5.0)))
//            {
//               p = p_interpolation(y_vec, p_vec, y);
//            }
//            else
//            {
//               p = 1.e5 + (9.81*y);
//            }                        
//            state.global_dof(0, first_dof_gid, p);
//         }
//        print_only_pid<0>(std::cerr) << "third loop" <<"\n";

//   }

  };


}


#endif
