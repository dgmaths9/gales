Mesh.MshFileVersion=2;

lc=1.0;
lc2=10;
lc3=100;
lc4=10;



// crater top
Point(2) = {-5,761,0,lc};      
Point(3) = {5,761,0,lc};       





// chamber
Point(6) = {-5,-2250,0,lc};
Point(7) = {5,-2250,0,lc};
Point(8) = {-500,-2350,0,5};
Point(16) = {0,-2350,0,lc};
Point(9) = {500,-2350,0,5};
Point(10) = {-5,-2450,0,lc};
Point(11) = {5,-2450,0,lc};



// bottom of dike
Point(14) = {-5,-3375,0,lc};
Point(15) = {5,-3375,0,lc};



Ellipse(8) = {8, 16, 9, 6};
Ellipse(9) = {8, 16, 9, 10};
Ellipse(10) = {9, 16, 8, 7};
Ellipse(11) = {9, 16, 8, 11};

Line(12) = {7, 3};
Line(13) = {2, 6};
Line(14) = {10, 14};
Line(15) = {14, 15};
Line(16) = {15, 11};










// right side of crater
Point(21) = {200,800,0, lc};
Point(22) = {307,916,0, lc2};

//sea level left
Point(17) = {-1040,0,0,lc2};
Point(18) = {-3000,0,0,lc3};

// sea level right
Point(19) = {1520,0,0,lc2};
Point(20) = {3000,0,0,lc3};

// top of atmospheric box
Point(23) = {-3000,5000,0,lc3};
Point(24) = {3000,5000,0,lc3};



Line(17) = {2, 17};
Line(18) = {17, 18};
Line(19) = {18, 23};
Line(20) = {23, 24};
Line(21) = {24, 20};
Line(22) = {20, 19};
Line(23) = {19, 22};

BSpline(24) = {3, 21, 22};
Curve Loop(1) = {19, 20, 21, 22, 23, -24, -12, -10, 11, -16, -15, -14, -9, 8, -13, 17, 18};
Plane Surface(1) = {1};
Physical Surface(1) = {1};
Physical Curve(5) = {14, 16, 11, 10, 12, 13, 8, 9, 17, 24, 23};
Physical Curve(3) = {19, 21};
Physical Curve(2) = {18, 20, 22};
Physical Point(4) = {14, 15};
Physical Point(5) = {10, 11, 7, 6, 8, 9, 2, 3, 22};
Physical Point(3) = {23, 24, 20, 18};
Physical Point(7) = {17, 19};
