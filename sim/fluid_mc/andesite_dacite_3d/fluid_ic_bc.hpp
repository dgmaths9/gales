#ifndef __FLUID_IC_BC_HPP
#define __FLUID_IC_BC_HPP



#include "../../../src/fem/fem.hpp"



namespace GALES {




  template<int dim>
  class fluid_ic_bc : public base_ic_bc<dim>  
  {

    using nd_type = node<dim>;
    using vec = boost::numeric::ublas::vector<double>;


  public :
 
    fluid_ic_bc():
    {
        this->central_pressure_profile_ = true;
        this->p0_ = 1.e8;
        this->max_ = -4000.0;
        this->min_ = -4200.0;
        this->h_ = 1.0;
        this->total_steps_ = (int)((this->max_-this->min_)/this->h_);     

        this->p_ref_ = this->p0_;
        this->v1_ref_ = 0.0;
        this->v2_ref_ = 0.0;
        this->v3_ref_ = 0.0;
        this->T_ref_ = 1200.0;

        this->Y_min_ = 0.0;
        this->Y_max_ = 1.0 - this->Y_min_;        
    }




    void body_force(vec& gravity) const
    {
      gravity[1] = -9.81;
    }





   
    //-------------------- IC ----------------------------------------

    double initial_p(const nd_type &nd) const { return 0.0; }
    double initial_vx(const nd_type &nd) const { return 0.0; }
    double initial_vy(const nd_type &nd) const { return 0.0; }
    double initial_vz(const nd_type &nd) const { return 0.0; }
    double initial_T(const nd_type &nd) const  
    { 
       double y = nd.get_y();
       if (y <= -4150.0)  return 1200.0; 
       else return 1150.0;        
    }

    void initial_Y (const nd_type &nd, vec &Y) const 
    {
       double y = nd.get_y();
       if (y <= -4150.0)  Y[0] = this->Y_max_;
       else   Y[0] = this->Y_min_;
    }
 	





  // --------------------Dirichlet BC------------------------------------

    auto dirichlet_p(const nd_type &nd) const 
    {
      return std::make_pair(false,0.0);
    }


    auto dirichlet_vx(const nd_type &nd) const 
    { 
	if( nd.flag() ==  5)     return std::make_pair(true,0.0); 
	
      return std::make_pair(false,0.0);
    }


    auto dirichlet_vy(const nd_type &nd) const 
    {
	if( nd.flag() ==  5)     return std::make_pair(true,0.0); 
	  
      return std::make_pair(false,0.0);
    }


    auto dirichlet_vz(const nd_type &nd) const 
    {
	if( nd.flag() ==  5)     return std::make_pair(true,0.0); 

      return std::make_pair(false,0.0);
    }


    auto dirichlet_T(const nd_type &nd) const 
    {
      return std::make_pair(false,0.0);
    }
 

    // comp_index is the index of component. e.g. for 3 component mixture we have first 2 components as dofs; Y[0] and Y[1]
    // 0---first component;   1----second component
    auto dirichlet_Y(const nd_type &nd, int comp_index) const
    {
//       if( nd.flag() == 2 && comp_index == 0 )          return std::make_pair(true,0.01);

      return std::make_pair(false,0.0);
    }






  //-------------------------Neumann BC--------------------------------------------------

    auto neumann_tau11(const std::vector<int>& bd_nodes, int side_flag) const 
    { 
      return std::make_pair(false,0.0);  
    }

    auto neumann_tau12(const std::vector<int>& bd_nodes, int side_flag) const 
    { 
      return std::make_pair(false,0.0);
    }

    auto neumann_tau13(const std::vector<int>& bd_nodes, int side_flag) const 
    {  
      return std::make_pair(false,0.0);
    }
  
    auto neumann_tau22(const std::vector<int>& bd_nodes, int side_flag) const 
    { 
      return std::make_pair(false,0.0); 
    }
                
    auto neumann_tau23(const std::vector<int>& bd_nodes, int side_flag) const 
    { 
      return std::make_pair(false,0.0);
    }

    auto neumann_tau33(const std::vector<int>& bd_nodes, int side_flag) const 
    {  
      return std::make_pair(false,0.0); 
    }
                
    auto neumann_q1(const std::vector<int>& bd_nodes, int side_flag) const  
    {   
      if(side_flag == 5)     return std::make_pair(true,0.0);

      return std::make_pair(false,0.0); 
    }

    auto neumann_q2(const std::vector<int>& bd_nodes, int side_flag) const 
    { 
      if(side_flag == 5)     return std::make_pair(true,0.0);

      return std::make_pair(false,0.0); 
    }

    auto neumann_q3(const std::vector<int>& bd_nodes, int side_flag) const 
    { 
      if(side_flag == 5)     return std::make_pair(true,0.0);

      return std::make_pair(false,0.0); 
    }

    auto neumann_J1(int comp_index, const std::vector<int>& bd_nodes, int side_flag) const 
    { 
      return std::make_pair(false,0.0); 
    }

    auto neumann_J2(int comp_index, const std::vector<int>& bd_nodes, int side_flag) const 
    { 
//      if(side_flag == 5 && comp_index==0)     return std::make_pair(true,0.0);

      return std::make_pair(false,0.0); 
    }

    auto neumann_J3(int comp_index, const std::vector<int>& bd_nodes, int side_flag) const 
    { 
      return std::make_pair(false,0.0); 
    }



  };
  
} //namespace
#endif



