// domain is an ellipsoid 800x200*200 (x*y*z) m^3 at 4km depth

lc=3.0;

Point(0) = {0,-4100,0,lc};

Point(1) = {0,-4000,0,lc};
Point(2) = {400,-4100,0,lc};
Point(3) = {0,-4200,0,lc};
Point(4) = {-400,-4100,0,lc};
Point(5) = {0,-4100,-100,lc};
Point(6) = {0,-4100,100,lc};


//+
Ellipse(1) = {4, 0, 2, 3};
//+
Ellipse(2) = {3, 0, 1, 2};
//+
Ellipse(3) = {2, 0, 4, 1};
//+
Ellipse(4) = {4, 0, 2, 1};
//+
Ellipse(5) = {1, 0, 3, 6};
//+
Ellipse(6) = {6, 0, 5, 3};
//+
Ellipse(7) = {3, 0, 1, 5};
//+
Ellipse(8) = {5, 0, 6, 1};
//+
Ellipse(9) = {4, 0, 2, 6};
//+
Ellipse(10) = {6, 0, 5, 2};
//+
Ellipse(11) = {4, 0, 2, 5};
//+
Ellipse(12) = {5, 0, 6, 2};
//+
Line Loop(1) = {3, -8, 12};
//+
Surface(1) = {1};
//+
Line Loop(2) = {8, -4, 11};
//+
Surface(2) = {2};
//+
Line Loop(3) = {9, 6, -1};
//+
Surface(3) = {3};
//+
Line Loop(4) = {6, 2, -10};
//+
Surface(4) = {4};
//+
Line Loop(5) = {10, 3, 5};
//+
Surface(5) = {5};
//+
Line Loop(6) = {5, -9, 4};
//+
Surface(6) = {6};
//+
Line Loop(7) = {11, -7, -1};
//+
Surface(7) = {7};
//+
Line Loop(8) = {7, 12, -2};
//+
Surface(8) = {8};
//+
Surface Loop(1) = {7, 2, 1, 5, 4, 3, 6, 8};
//+
Volume(1) = {1};
//+
Physical Surface(5) = {7, 6, 3, 4, 8, 5, 1, 2};
//+
Physical Volume(0) = {1};

Mesh.MshFileVersion = 2;
