#ifndef __FLUID_IC_BC_HPP
#define __FLUID_IC_BC_HPP


#include "../../../src/fem/fem.hpp"



namespace GALES {




  template<int dim>
  class fluid_ic_bc : public base_ic_bc<dim> 
  {
    using nd_type = node<dim>;
    using vec = boost::numeric::ublas::vector<double>;

  public :
 
    fluid_ic_bc()
    {
        this->p_ref_ = 97.e5;
        this->v1_ref_ = 0.0;
        this->v2_ref_ = 0.0;
        this->T_ref_ = 550.0;
        
        this->Y_min_ = 0.0;
        this->Y_max_ = 1.0 - this->Y_min_;        
    }






    //----------------------------initial conditions----------------------------------------   
    double initial_p (const nd_type &nd) const
    {
      double x = nd.get_x();
      if(x <= 0.501)  return 194.3e5;
      else  return 1.e5;
    }

    double initial_vx(const nd_type &nd) const { return 0.0; }
    double initial_vy(const nd_type &nd) const { return 0.0; }

    double initial_T(const nd_type &nd) const
    {
      double x = nd.get_x();
      if(x <= 0.501)  return 823.66;
      else  return 293.52;
    }

    void initial_Y (const nd_type &nd, vec &Y) const
    {
      double x = nd.get_x();
      if(x <= 0.501) Y[0] = this->Y_max_;
      else Y[0] = this->Y_min_;
    }





    // -------------------------  Dirichlet BC ------------------------
    auto dirichlet_p(const nd_type &nd) const 
    {
 	if( nd.flag() == 2)	  return std::make_pair(true,194.3e5); 
	if( nd.flag() == 3)	  return std::make_pair(true,1.e5); 

      return std::make_pair(false,0.0); 
    }


    auto dirichlet_vx(const nd_type &nd) const 
    { 
 	if( nd.flag() == 2)	  return std::make_pair(true,0.0); 
	if( nd.flag() == 3)	  return std::make_pair(true,0.0); 

      return std::make_pair(false,0.0); 
    }


    auto dirichlet_vy(const nd_type &nd) const 
    {
      return std::make_pair(true,0.0); 
    }
      
        
    auto dirichlet_T(const nd_type &nd) const 
    {
      if( nd.flag() == 2)       return std::make_pair(true, 823.66);
      if( nd.flag() == 3)       return std::make_pair(true, 293.52);

      return std::make_pair(false, 0.0);
    }


    // comp_index is the index of component. e.g. for 3 component mixture we have first 2 components as dofs; Y[0] and Y[1]
    // 0---first component;   1----second component
    auto dirichlet_Y(const nd_type &nd, int comp_index) const 
    { 
       if( (nd.flag() == 2) && (comp_index == 0))          return std::make_pair(true, this->Y_max_);
                 
       return std::make_pair(false,0.0); 
    }






  //-------------------------Neumann BC--------------------------------------------------

    auto neumann_tau11(const std::vector<int>& bd_nodes, int side_flag) const 
    { 
      return std::make_pair(false,0.0);  
    }

    auto neumann_tau12(const std::vector<int>& bd_nodes, int side_flag) const 
    { 
      return std::make_pair(false,0.0);
    }
  
    auto neumann_tau22(const std::vector<int>& bd_nodes, int side_flag) const 
    { 
      return std::make_pair(false,0.0); 
    }
                
    auto neumann_q1(const std::vector<int>& bd_nodes, int side_flag) const  
    {   
      return std::make_pair(false,0.0); 
    }

    auto neumann_q2(const std::vector<int>& bd_nodes, int side_flag) const 
    { 
      return std::make_pair(false,0.0); 
    }



  };

} //namespace
#endif
