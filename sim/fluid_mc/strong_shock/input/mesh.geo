Mesh.MshFileVersion = 2;
lc = 0.002;
lc2 = 0.004;


Point(1) = {0,0,0,lc};
Point(2) = {1,0,0,lc2};
Point(3) = {1,0.001,0,lc2};
Point(4) = {0,0.001,0,lc2};



Line(1) = {4, 1};
Line(2) = {1, 2};
Line(3) = {2, 3};
Line(4) = {3, 4};

Line Loop(5) = {4, 1, 2, 3};
Plane Surface(6) = {5};
Physical Surface(0) = {6};
Recombine Surface{6};

Transfinite Line {4, 2} = 401 Using Progression 1;
Transfinite Line {1, 3} = 2 Using Progression 1;
Transfinite Surface {6};


//+
Physical Line(3) = {3};
//+
Physical Line(2) = {1};
//+
Physical Line(4) = {2, 4};
//+
Physical Point(2) = {1, 4};
//+
Physical Point(3) = {2, 3};
