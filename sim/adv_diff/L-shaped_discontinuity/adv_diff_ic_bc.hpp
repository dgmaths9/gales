#ifndef ADV_DIFF_IC_BC_HPP
#define ADV_DIFF_IC_BC_HPP



#include "../../../src/fem/fem.hpp"



namespace GALES{




  template<int dim>
  class adv_diff_ic_bc : public base_ic_bc<dim>
  {
    using nd_type = node<dim>;
    using vec = boost::numeric::ublas::vector<double>;

    public : 
    

 
 
 
    void get_v(int nd_gid, vec& v)
    {
        v[0] = 1.0/sqrt(2.0);
        v[1] = 1.0/sqrt(2.0);
    }
 
 
 
    
 
 
 
    //---------------------   IC  ------------------------------------------
    double initial_Y(const nd_type &nd)const 
    {
       if(nd.get_y()<=0.5+1.e-3 && nd.get_x()<=0.25+1.e-3)  return 1.0;
       else if(nd.get_y()<=0.25+1.e-3 && nd.get_x()<=0.5+1.e-3)  return 1.0;
       else return 0.0;
    }


 
     //------------set dirichlet bc------------------------------------      
    auto dirichlet_Y(const nd_type &nd) const 
    {
      if(nd.flag()==5)
      {
        if(nd.get_y() == 0.0 && nd.get_x()<= 0.5+1.e-3) return std::make_pair(true, 1.0);
        else if(nd.get_x() == 0.0 && nd.get_y()<= 0.5+1.e-3) return std::make_pair(true, 1.0);
        else return std::make_pair(true, 0.0);
      }  
       
      return std::make_pair(false,0.0);
    }


    //--------------set Neummann bc-------------------------------------
    auto neumann_J1(const std::vector<int>& bd_nodes, int side_flag) const 
    { 
      return std::make_pair(false,0.0);
    }

    auto neumann_J2(const std::vector<int>& bd_nodes, int side_flag) const 
    {
      return std::make_pair(false,0.0); 
    }

    auto neumann_J3(const std::vector<int>& bd_nodes, int side_flag) const 
    {
      return std::make_pair(false,0.0); 
    }
    

  };


}


#endif
