#ifndef ADV_DIFF_IC_BC_HPP
#define ADV_DIFF_IC_BC_HPP



#include "../../../src/fem/fem.hpp"



namespace GALES{




  template<int dim>
  class adv_diff_ic_bc 
  {
    using nd_type = node<dim>;
    using point_type = point<dim>;
    using vec = boost::numeric::ublas::vector<double>;

    public : 
    

    adv_diff_ic_bc()
    {
        read_bin("input/200", fluid_dofs_);
    }
    


    void body_force(vec& gravity) 
    {
        gravity[1] = -9.81;  
    }

 
 
 
    void get_v(int nd_gid, vec& v)
    {
       v[0] = fluid_dofs_[nd_gid*4 + 1];
       v[1] = fluid_dofs_[nd_gid*4 + 2];
    }
 
 
 
    
    void pressure_profile_magma()
    {
        magma_krafla obj;
        int steps = 2500;
        double h = 0.01;
        double max = -2104.0;

        p_vec_.resize(steps+1); 
        y_vec_.resize(steps+1);
  
        for(int i=0; i<=steps; i++)
          y_vec_[i] = max - h*i;

        
        // Runge kutta
        double m1,m2,m3,m4;
        p_vec_[0] = 45.4e6;

        for(int i=0; i<steps; i++)
        {
          obj.properties(p_vec_[i], 1173.15, 0.0);
          m1 = 9.81*obj.rho();
          obj.properties(p_vec_[i]+0.5*h*m1, 1173.15, 0.0);
          m2 = 9.81*obj.rho();
          obj.properties(p_vec_[i]+0.5*h*m2, 1173.15, 0.0);
          m3 = 9.81*obj.rho();          
          obj.properties(p_vec_[i]+h*m3, 1173.15, 0.0);
          m4 = 9.81*obj.rho();          
          p_vec_[i+1] = p_vec_[i]+h/6.*(m1+2.0*(m2+m3)+m4);
        }        
    
    }
    
    
    
    
    double p_interpolated(double y_coord)
    {
       return p_interpolation(y_vec_, p_vec_, y_coord);
    }
    
 
 
 
    //---------------------   IC  ------------------------------------------
    double initial_Y(const nd_type &nd)const 
    {
       double y = nd.get_y();
       if(y <= -2104.0+1.e-6)  return 1173.15;
//       else if(y>-2104.0+1.e-6 && y <= -2103.95) return 313.15 - 860*(y+2103.95)/0.05;
       else return 313.15;
    }


 
     //------------set dirichlet bc------------------------------------      
    auto dirichlet_Y(const nd_type &nd) const 
    {
      if(nd.flag()==6)  return std::make_pair(true, 313.15);
       
      return std::make_pair(false,0.0);
    }


    //--------------set Neummann bc-------------------------------------
    auto neumann_J1(const std::vector<int>& bd_nodes, int side_flag) const 
    { 
      return std::make_pair(false,0.0);
    }

    auto neumann_J2(const std::vector<int>& bd_nodes, int side_flag) const 
    {
      return std::make_pair(false,0.0); 
    }

    auto neumann_J3(const std::vector<int>& bd_nodes, int side_flag) const 
    {
      return std::make_pair(false,0.0); 
    }


    private:
    
     std::vector<double> fluid_dofs_; 
     boost::numeric::ublas::vector<double> y_vec_, p_vec_;

  };


}


#endif
