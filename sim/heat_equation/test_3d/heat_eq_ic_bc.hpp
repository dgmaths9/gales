#ifndef HEAT_EQ_IC_BC_HPP
#define HEAT_EQ_IC_BC_HPP



#include "../../../src/fem/fem.hpp"



namespace GALES{




  template<int dim>
  class heat_eq_ic_bc : public base_ic_bc<dim>  
  {
    using nd_type = node<dim>;
    using vec = boost::numeric::ublas::vector<double>;

    public : 
    


 
 
    //---------------------   IC  ------------------------------------------
    double initial_T(const nd_type &nd)const {return 0.0;}


 
     //------------set dirichlet bc------------------------------------      
    auto dirichlet_T(const nd_type &nd) const 
    {
      if(nd.flag() == 2)      return std::make_pair(true, 20.0);
      if(nd.flag() == 3)      return std::make_pair(true, -20.0);      
      if(nd.flag() == 4)      return std::make_pair(true, 10.0);
      if(nd.flag() == 5)      return std::make_pair(true, -10.0);      
      if(nd.flag() == 6)      return std::make_pair(true, 30.0);
      if(nd.flag() == 7)      return std::make_pair(true, -30.0);      


      return std::make_pair(false,0.0);
    }


    //--------------set Neummann bc-------------------------------------
    auto neumann_q1(const std::vector<int>& bd_nodes, int side_flag) const 
    { 
      return std::make_pair(false,0.0);
    }

    auto neumann_q2(const std::vector<int>& bd_nodes, int side_flag) const 
    {
      return std::make_pair(false,0.0); 
    }

    auto neumann_q3(const std::vector<int>& bd_nodes, int side_flag) const 
    {
      return std::make_pair(false,0.0); 
    }


  };


}


#endif
