Mesh.MshFileVersion=2;

lc=1.0;



//+
SetFactory("OpenCASCADE");
//+
Box(1) = {0, 0, 0, 2, 1, 1};
//+
Physical Volume(13) = {1};
//+
Physical Surface(2) = {1};
//+
Physical Surface(3) = {2};
//+
Physical Surface(4) = {3};
//+
Physical Surface(5) = {4};
//+
Physical Surface(6) = {5};
//+
Physical Surface(7) = {6};
//+
Physical Curve(2) = {4, 3, 2, 1};
//+
Physical Curve(3) = {8, 7, 6, 5};
//+
Physical Curve(4) = {9, 10};
//+
Physical Curve(5) = {11, 12};
//+
Physical Point(2) = {4, 3, 2, 1};
//+
Physical Point(3) = {8, 7, 6, 5};
//+
MeshSize {2, 1, 3, 4, 8, 7, 6, 5} = 0.1;


