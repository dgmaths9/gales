Mesh.MshFileVersion=2;

lc=1.0;


Point(1) = {0,0,0,lc};
Point(2) = {1,0,0,lc};
Point(3) = {1,1,0,lc};
Point(4) = {0,1,0,lc};


//+
Line(1) = {1, 2};
//+
Line(2) = {2, 3};
//+
Line(3) = {3, 4};
//+
Line(4) = {4, 1};
//+
Curve Loop(1) = {4, 1, 2, 3};
//+
Plane Surface(1) = {1};
//+
Transfinite Curve {1, 3} = 11 Using Progression 1;
//+
Transfinite Curve {4, 2} = 11 Using Progression 1;
//+
Transfinite Surface {1};


//+
Recombine Surface {1};
//+
Physical Surface(5) = {1};
//+
Physical Curve(2) = {4};
//+
Physical Curve(3) = {2};
//+
Physical Curve(4) = {1, 3};
