#ifndef HEAT_EQ_IC_BC_HPP
#define HEAT_EQ_IC_BC_HPP



#include "../../../src/fem/fem.hpp"



namespace GALES{




  template<int dim>
  class heat_eq_ic_bc : public base_ic_bc<dim>  
  {
    using nd_type = node<dim>;
    using vec = boost::numeric::ublas::vector<double>;

    public : 
    



    void body_force(vec& gravity) const
    {
      gravity[0] = 0.0;      
      gravity[1] = 0.0;  
    }


 
 
 
 
    //---------------------   IC  ------------------------------------------
    double initial_T(const nd_type &nd)const {return 0.0;}


 
     //------------set dirichlet bc------------------------------------      
    auto dirichlet_T(const nd_type &nd) const 
    {
      if(nd.flag() == 2)      return std::make_pair(true, 1.0);
      if(nd.flag() == 3)      return std::make_pair(true, 0.0);      

      return std::make_pair(false,0.0);
    }


    //--------------set Neummann bc-------------------------------------
    auto neumann_q1(const std::vector<int>& bd_nodes, int side_flag) const 
    { 
      return std::make_pair(false,0.0);
    }

    auto neumann_q2(const std::vector<int>& bd_nodes, int side_flag) const 
    {
      return std::make_pair(false,0.0); 
    }


  };


}


#endif
