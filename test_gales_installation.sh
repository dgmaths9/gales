#!/bin/bash




check_file()
{
  FILE=$1
  if [ ! -f "$FILE" ]; then
      echo "Error: $FILE does not exist."
      exit 1
  fi
}






echo -e "\n\n--------test 1 --------------\n" 
cd tpl
echo -e " -------- compiling solwcad --------------" 
./build_solwcad_mesh_tools.sh
cd solwcad
check_file "solwcad_p"
check_file "solwcad_pT"
echo -e " -------- done--------------" 
cd ../..
echo -e "\n-------- test 1 passed successfully --------------\n\n\n\n" 





echo -e "\n\n--------test 2 --------------\n" 
cd sim/solid_es/annulus
echo -e " -------- generating simulation executable --------------" 
./build
check_file "executable"
echo -e "------------done --------------" 

cd input
echo -e "--------generating mesh by gmsh--------------" 
gmsh mesh.geo -2
check_file "mesh.msh"
gales_mesh  2
check_file "mesh_2core.txt"
cd ..
echo -e "-------- done --------------" 


sed -i -e "s/.*solid_mesh_file.*/solid_mesh_file        mesh_2core.txt/" setup.txt


echo -e " -------- running simulation in parallel with 2 cores --------------" 
mpirun -np 2 ./executable
echo -e " -------- simulation finished--------------" 


check_file results/solid/u/0
check_file results/solid/u/1
echo -e "\n-------- test 2 passed successfully --------------\n\n\n\n" 


echo -e "\n\n -------- Gales is installed!!!  --------------\n\n\n\n" 


      


